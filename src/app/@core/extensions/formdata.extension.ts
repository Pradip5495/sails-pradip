import { IJson } from '@types';

FormData.prototype.appendAll = function appendAll(dataArray: IJson[]): FormData {
  for (const dataKey in dataArray) {
    if (dataArray[dataKey]) {
      this.append(dataKey, dataArray[dataKey]);
    }
  }
  return this;
};
