Object.defineProperty(Array.prototype, 'chunk', {
  value(chunkSize: number) {
    const R = [];
    for (let i = 0; i < this.length; i += chunkSize) {
      R.push(this.slice(i, i + chunkSize));
    }
    return R;
  },
});
