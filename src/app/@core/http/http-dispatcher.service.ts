import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Logger } from '../logger.service';
import { environment } from '@env/environment';

const log = new Logger('HttpDispatcherService');

interface IHttpDispatchOptions {
  acceptNullResponse?: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class HttpDispatcherService {
  options: IHttpDispatchOptions;

  private static handleError(error: HttpErrorResponse) {
    if (!environment.production) {
      if (error.error instanceof ErrorEvent) {
        // Log client-side or network error
        log.error('An error occurred:', error.error.message);
      } else {
        // Log server-side error [The response body may contain clues as to what went wrong]
        log.error(`Backend returned code '${error.status}', ` + ` with message: '${error.error.message}'`);
      }
    }

    return throwError(
      // return a user-facing error message, pipe backend messages with static fallback // FIXME move to CONSTANTS
      error.error.message ? error.error.message : 'Something bad happened; please try again later.'
    );
  }

  constructor(private readonly httpClient: HttpClient) {}

  // TODO temp disabled Cache .cache()

  get<T>(url: string, options?: IHttpDispatchOptions): Observable<T> {
    return this.httpClient.get<T>(url).pipe(
      map((response) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        const altDataParam = 'payload'; // FIXME post key update from backend

        /**
         * return empty array in case of 204 (No Content) response
         */
        if (options?.acceptNullResponse && response === null) {
          return [];
        }

        return response[statusCode] && response[statusCode] === 200
          ? response.hasOwnProperty(altDataParam)
            ? response[altDataParam]
            : response[dataParam]
          : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }),
      catchError(HttpDispatcherService.handleError)
    );
  }

  post<T>(url: string, body: any): Observable<T> {
    return this.httpClient.post<T>(url, body).pipe(
      tap((responseData: any) => {
        return responseData;
      }),
      catchError(HttpDispatcherService.handleError)
    );
  }

  put<T>(url: string, body: any): Observable<T> {
    return this.httpClient.put<T>(url, body).pipe(
      tap((responseData: any) => {
        return responseData;
      }),
      catchError(HttpDispatcherService.handleError)
    );
  }

  delete<T>(url: string): Observable<T> {
    return this.httpClient.delete<T>(url).pipe(
      tap((responseData: any) => {
        return responseData;
      }),
      catchError(HttpDispatcherService.handleError)
    );
  }
}
