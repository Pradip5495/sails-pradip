import { Observable, of } from 'rxjs';

import { LoginContext } from './authentication.service';
import { Credentials } from './credentials.service';

export class MockAuthenticationService {
  credentials: Credentials | null = {
    username: 'test',
    password: '123456',
    domain: 'test',
  };

  login(context: LoginContext): Observable<Credentials> {
    // TODO Return Mock Login
    return;
  }

  logout(): Observable<boolean> {
    this.credentials = null;
    return of(true);
  }
}
