import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Credentials, CredentialsService } from './credentials.service';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';

const routes = {
  login: () => `/login`,
};

export interface LoginContext {
  username: string;
  password: string;
  domain: string;
  remember?: boolean;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private httpClient: HttpClient, private credentialsService: CredentialsService) {}

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginContext): Observable<Credentials> {
    const data = {
      username: context.username,
      password: context.password,
      domain: context.domain,
    };
    return this.httpClient.post<LoginContext>(routes.login(), data).pipe(
      tap((responseData: LoginContext) => {
        const statusCode = 'statusCode';
        const tokenParam = 'token';
        if (responseData[statusCode] === 200) {
          this.credentialsService.setCredentials(responseData[tokenParam], context.remember);
          return responseData[tokenParam];
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.setCredentials();
    return of(true);
  }
}
