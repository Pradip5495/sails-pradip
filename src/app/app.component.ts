import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, ResolveEnd, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { merge } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { environment } from '@env/environment';
import { Logger, untilDestroyed } from '@core';
import { I18nService } from '@app/i18n';
import { IconService, UserService } from '@shared';
import { SidenavStoreService } from './shell/sidenav/sidenav-store.service';

const log = new Logger('App');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private translateService: TranslateService,
    private i18nService: I18nService,
    private iconService: IconService,
    private userService: UserService,
    private leftSidenavStore: SidenavStoreService
  ) {}

  ngOnInit() {
    // Setup logger
    if (environment.production) {
      Logger.enableProductionMode();
    }
    this.functionToMaintainSideNavState();

    log.debug('init');

    // Setup translations
    this.i18nService.init(environment.defaultLanguage, environment.supportedLanguages);

    // Setup svg icon registration
    this.iconService.registerIcons();

    const onNavigationEnd = this.router.events.pipe(filter((event) => event instanceof NavigationEnd));

    // Change page title on navigation or language change, based on route data
    merge(this.translateService.onLangChange, onNavigationEnd)
      .pipe(
        map(() => {
          let route = this.activatedRoute;
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        filter((route) => route.outlet === 'primary'),
        switchMap((route) => route.data),
        untilDestroyed(this)
      )
      .subscribe((event) => {
        const title = event.title;
        if (title) {
          this.titleService.setTitle(this.translateService.instant(title));
        }
      });
  }

  functionToMaintainSideNavState() {
    this.router.events.pipe(untilDestroyed(this)).subscribe((event) => {
      if (event instanceof ResolveEnd) {
        try {
          /* Fetching the Menu from Local Storage */
          const currentUserData = this.userService.currentUser;
          let currentUserMenuItems: any;

          // Getting the URL
          const navigatingUrl = event.urlAfterRedirects;

          // Basically Only Near-Miss, Incident and Admin Menus had SideNav
          if (
            navigatingUrl.toLowerCase().includes('near-miss') ||
            navigatingUrl.toLowerCase().includes('incident') ||
            navigatingUrl.toLowerCase().includes('admin') ||
            navigatingUrl.toLowerCase().includes('audit')
          ) {
            const moduleRoutes = {
              Audit: currentUserData.myMenu.auditRoutes,
              Incident: currentUserData.myMenu.incidentRoutes,
              Admin: currentUserData.myMenu.adminRoutes,
            };
            const activeModule = this.userService.getSelectedModule();
            currentUserMenuItems = moduleRoutes[activeModule] || [];
            // Identify the Create or Update Route
            const isCreateOrUpdateRoute =
              navigatingUrl.includes('create') ||
              navigatingUrl.includes('form') ||
              navigatingUrl.includes('update') ||
              navigatingUrl.includes('edit') ||
              navigatingUrl.includes('new') ||
              navigatingUrl.includes('modify');

            let sideNavMenu;
            if (!isCreateOrUpdateRoute) {
              sideNavMenu = currentUserMenuItems.find((value: any) => {
                return navigatingUrl.indexOf(value.route) > 0;
              });
              this.leftSidenavStore.navItems = sideNavMenu ? sideNavMenu.children : [];
            } else {
              this.leftSidenavStore.navItems = [];
            }
          } else {
            this.leftSidenavStore.navItems = [];
          }
        } catch (ex) {}
      }
    });
  }

  ngOnDestroy() {
    this.i18nService.destroy();
  }
}
