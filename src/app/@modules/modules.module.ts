import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ModuleRegistry } from '@ag-grid-community/core';
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model';
import { SideBarModule } from '@ag-grid-enterprise/side-bar';
import { MenuModule } from '@ag-grid-enterprise/menu';
import { ColumnsToolPanelModule } from '@ag-grid-enterprise/column-tool-panel';
import { ExcelExportModule } from '@ag-grid-enterprise/excel-export';
import { ClipboardModule } from '@ag-grid-enterprise/clipboard';
import { ModulesRoutingModule } from './modules-routing.module';
import { AuditInspectionsModule } from './audit-inspections/audit-inspections.module';
import { MasterDetailModule } from '@ag-grid-enterprise/master-detail';

ModuleRegistry.registerModules([
  ClientSideRowModelModule,
  SideBarModule,
  MenuModule,
  ColumnsToolPanelModule,
  ExcelExportModule,
  ClipboardModule,
  MasterDetailModule,
]);

@NgModule({
  declarations: [],
  imports: [CommonModule, ModulesRoutingModule, AuditInspectionsModule],
  exports: [],
  providers: [DatePipe],
})
export class ModulesModule {}
