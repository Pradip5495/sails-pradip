import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UserService, RoleAccessService } from '@shared/service';
import { NotificationService } from '@shared/common-service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UpdateUser } from '@shared/models';
import { Logger } from '@core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '@env/environment';

const env = environment;

const log = new Logger('Profile-Form');

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  profileFormGroup: FormGroup;
  overViewFormGroup: FormGroup;
  isLoading = false;

  public countryCode: any = 1;
  public phonenumber: any;
  isNewProfile = false;
  public imageUrl: any = '/assets/user.png';
  fileToUpload: File = null;
  error: string | undefined;
  updateUser = new UpdateUser();
  public title: any;
  public dataId: any;
  profilevalue: any;
  files: any = [];
  public allFiles: any = [];
  public images: any = [];

  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();
  userId: any;
  designationId: any;
  roleId: any;

  constructor(
    private formBuilder: FormBuilder,
    private readonly roleAccessService: RoleAccessService,
    private readonly userService: UserService,
    private notifyService: NotificationService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.profileFormGroup = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      countryCode: ['', Validators.required],
      addressLine1: ['', Validators.required],
      addressLine2: [''],
      addressLine3: [''],
      phone: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
      city: ['', Validators.required],
      state: ['', Validators.required],
      country: ['', Validators.required],
      zipcode: ['', Validators.required],
      files: ['', Validators.required],
      role: new FormControl({ value: null, disabled: true }, Validators.required),
      designation: new FormControl({ value: null, disabled: true }, Validators.required),
      isActive: ['', Validators.required],
    });

    const paramValue = 'value';
    if (this.route.params[paramValue].id) {
      this.dataId = this.route.params[paramValue].id;
      this.getUsersById(this.dataId);
    }
    this.designationTypeList();
    this.roleTypeList();
  }

  onFileChange(event: any) {
    log.debug('onFileChange', event);
    if (event.addedFiles) {
      this.imageUrl = '';
      this.files = [];
      this.files.push(event.addedFiles[0]);
    }
  }

  designationTypeList() {
    this.userService.getDesignations().subscribe((data) => {
      this.designationId = data;
      log.debug('des', this.designationId);
    });
  }

  roleTypeList() {
    this.roleAccessService.getRoles().subscribe((data) => {
      this.roleId = data;
    });
  }

  getUsersById(params: any) {
    log.debug('usersId: ', params);
    this.userService.getUsers().subscribe((data) => {
      log.debug('responses', data);
      this.profilevalue = data;

      log.debug(this.profilevalue);
      for (const i of this.profilevalue) {
        if (i.userId === params) {
          this.updateUser = i;
          log.debug('profileUpdate', this.updateUser);
          this.patchUserForm();
        }
      }
    });
  }

  patchUserForm() {
    this.profileFormGroup.patchValue({
      firstname: this.updateUser.firstname,
      lastname: this.updateUser.lastname,
      countryCode: this.updateUser.countryCode,
      addressLine1: this.updateUser.addressLine1,
      addressLine2: this.updateUser.addressLine2,
      addressLine3: this.updateUser.addressLine3,
      phone: this.updateUser.phone,
      city: this.updateUser.city,
      state: this.updateUser.state,
      country: this.updateUser.country,
      zipcode: this.updateUser.zipcode,
      role: this.updateUser.roleId,
      profilePic: null,
      designation: this.updateUser.designationId,
    });
    this.imageUrl = env.filePath + '/' + this.updateUser.profilePic;
  }

  get getUserNameInitials(): string {
    const currentUser = this.userService.currentUser;
    const name = currentUser.firstname + ' ' + currentUser.lastname;
    return name;
  }

  onSubmit() {
    this.profileFormGroup.value.countryCode = this.countryCode;
    const formData: any = new FormData();

    // const data = this.profileFormGroup.value;
    formData.append('firstname', this.profileFormGroup.value.firstname);
    formData.append('lastname', this.profileFormGroup.value.lastname);
    formData.append('countryCode', this.profileFormGroup.value.countryCode);
    formData.append('addressLine1', this.profileFormGroup.value.addressLine1);
    formData.append('addressLine2', this.profileFormGroup.value.addressLine2);
    formData.append('addressLine3', this.profileFormGroup.value.addressLine3);

    formData.append('phone', this.profileFormGroup.value.phone);
    formData.append('city', this.profileFormGroup.value.city);
    formData.append('state', this.profileFormGroup.value.state);
    formData.append('country', this.profileFormGroup.value.country);
    formData.append('zipcode', this.profileFormGroup.value.zipcode);
    formData.append('role', this.profileFormGroup.value.role);
    formData.append('profilePic', this.files[0]);
    formData.append('designation', this.profileFormGroup.value.designation);
    formData.append('isActive', this.profileFormGroup.value.isActive);

    this.userService.updateUser(this.dataId, formData).subscribe(
      (response) => {
        log.debug('responsed: ', response);
        const message = 'Profile Updated successfully!';
        this.notifyService.showSuccess(message, this.title);
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
        // this._snackBar.open(message, '', {
        //   duration: 4000
        // });
      }
    );
  }

  overviewReset() {
    this.profileFormGroup.reset();
  }

  onCountryChange(country: any) {
    this.countryCode = country.dialCode;
    // alert(this.countryCode);
    return this.countryCode;
  }

  getNumber(event: any) {
    this.phonenumber = event.placeholder;
    // alert(this.phonenumber.length);
  }
  telInputObject(event: any) {}

  onError(obj: any) {}
}
