import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from '@app/i18n';
import { ResetPasswordComponent } from '@modules/reset-password/reset-password.component';

const routes: Routes = [
  {
    path: ':token',
    component: ResetPasswordComponent,
    data: { title: extract('Reset Password') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResetPasswordRoutingModule {}
