import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificationService } from '@shared/common-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Logger } from '@core';
import { MustMatch } from '@modules/activation/must-match.validator';
import { ForgotPasswordService } from '@shared/service';

const log = new Logger('Reset-Password');

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
  token: any;
  isLoading = false;
  error: string | undefined;
  resetPassword: FormGroup;
  public title = 'Forgot-Password';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private notifyService: NotificationService,
    private formBuilder: FormBuilder,
    private forgotPasswordService: ForgotPasswordService
  ) {}

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      this.token = params.token;
      log.debug('reset Password Token', +'\n' + this.token);
    });
    this.createForm();
  }

  setPassword() {
    this.isLoading = true;
    const data = this.resetPassword.value;
    const formData = { token: this.token, password: data.password };
    log.debug('resetPasswordData', formData);
    this.forgotPasswordService.resetPassword(formData).subscribe(
      (response) => {
        this.notifyService.showSuccess(response.message, this.title);
        this.isLoading = false;
        this.router.navigate(['/login']);
      },
      (error) => {
        this.error = error;
        this.isLoading = false;
        this.notifyService.showError(error.message, this.title);
      }
    );
  }

  private createForm() {
    this.resetPassword = this.formBuilder.group(
      {
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', Validators.required],
      },
      {
        validator: MustMatch('password', 'confirmPassword'),
      }
    );
  }
}
