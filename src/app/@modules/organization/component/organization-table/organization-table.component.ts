import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Logger } from '@core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { OrganizationService } from '../../organization.service';
import { NotificationService } from '@shared/common-service/notification.service';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
const log = new Logger('OrganizationTableComponent');

@Component({
  selector: 'app-organization-table',
  templateUrl: './organization-table.component.html',
  styleUrls: ['./organization-table.component.scss'],
})
export class OrganizationTableComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  displayedColumns: string[] = ['name', 'email', 'phone', 'city', 'state', 'country', 'zipcode', 'actions'];
  organizationData: any = [];
  dataSource: any;
  allOrganizationData: any;
  isLoading = false;
  noRecord = false;
  error: string | undefined;
  public title = 'Organization';
  public icons: any;
  isdataSource = false;
  public rowData: any;

  public pageSize = 10;
  public deleteData: any;
  public currentPage = 0;
  public totalSize = 0;
  public array: any;
  public sideBar: any;
  public id: any;
  public formData: any = [];
  editType = '';
  domLayout = 'autoHeight';
  columnDefs = [
    {
      headerName: 'NAME',
      field: 'name',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'EMAIL',
      field: 'email',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'PHONE',
      field: 'phone',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    // 'name', 'email', 'phone', 'city', 'state', 'country', 'zipcode', 'actions']
    {
      headerName: 'CITY',
      field: 'city',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'STATE',
      field: 'state',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'COUNTRY',
      field: 'country',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'ZIP CODE',
      field: 'zipcode',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'ACTION',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deleteOrganization.bind(this),
        onClickEdit: this.editOrganizationClick.bind(this),
        hideDetailsButton: true,
      },
      filter: 'agTextColumnFilter',
    },
  ];
  headerHeight: 100;
  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
  };
  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };

  constructor(
    private router: Router,
    private organizationService: OrganizationService,
    private notifyService: NotificationService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.getAllOrganizationList();
    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };
  }
  onGridReady(params: any) {
    log.debug('reached');
    this.organizationService.getAllOrganizationList().subscribe(
      (response) => {
        log.debug('Users Data', response);
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
      },
      (error) => {
        this.notifyService.showError(error);
      }
    );
  }

  getAllOrganizationList() {
    this.organizationService.getAllOrganizationList().subscribe(
      (response) => {
        this.organizationData = response;
        if (!response) {
          this.noRecord = true;
          const message = 'No Record Found!';
          this.notifyService.showError(message, this.title);
        }
        this.dataSource = new MatTableDataSource(this.organizationData);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.array = response;
        this.totalSize = this.array.length;
        this.iterator();
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  /**
   * Delete  All Organization Data
   *
   */
  deleteOrganization(params: any) {
    this.organizationService.deleteOrganization(params.rowData.TenantId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.getAllOrganizationList();
        } else {
          const message = 'Record No Deleted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }
  editOrganizationClick(element: any) {
    log.debug('element:', element);
    log.debug('router', element.rowData.TenantId);
    log.debug('link', this.router);
    this.id = element.rowData.TenantId;
    this.formData = element.rowData;
    // this.router.navigate([`/organization/${this.id}/edit`]);
    this.router.navigate([`/organization/${this.id}/edit`], { state: this.formData, skipLocationChange: false });
  }

  public handlePage(e: PageEvent) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }
  onAddForm() {
    this.router.navigateByUrl('/organization/new-organization-form').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.dataSource = part;
  }
}
