import { Component, OnInit } from '@angular/core';
import { OrganizationService } from '../../organization.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Logger } from '@core';
import { CacheServiceService } from '@shared/common-service/cache-service.service';
import { NotificationService } from '@shared/common-service/notification.service';

const log = new Logger('Organization-Form');

@Component({
  selector: 'app-organization-update-form',
  templateUrl: './organization-update-form.component.html',
  styleUrls: ['./organization-update-form.component.scss'],
})
export class OrganizationUpdateFormComponent implements OnInit {
  organizationFormGroup: FormGroup;
  overViewFormGroup: FormGroup;
  error: string | undefined;
  public tenantId: any = [];
  public tuid: any;
  public countrysCode: any;
  public phonenumber: any;
  public code: any;
  public countryCode: any;
  public resData: any;
  public organizationData: any;
  public orgData: any;
  public orgArray: any = [];
  public formData: any = [];
  public orgGetData: any;
  public title = 'Organization';
  hasError: boolean;
  color = 'primary';
  check = false;
  constructor(
    private formBuilder: FormBuilder,
    private cacheServiceService: CacheServiceService,
    private route: ActivatedRoute,
    private router: Router,
    private organizationService: OrganizationService,
    private notifyService: NotificationService
  ) {}

  get f() {
    return this.organizationFormGroup.controls;
  }

  ngOnInit() {
    this.organizationFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      organisationDomain: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      addressLine1: ['', Validators.required],
      addressLine2: [''],
      addressLine3: [''],
      phone: ['', [Validators.required]],
      city: ['', Validators.required],
      state: ['', Validators.required],
      country: ['', Validators.required],
      zipcode: ['', Validators.required],
      isActive: ['', Validators.required],
    });
    log.debug('route data:', this.route.params);
    const paramValue = 'value';
    if (this.route.params[paramValue].id) {
      this.tenantId = this.route.params[paramValue].id;
      log.debug('route data:', this.route.params);
      log.debug('id::', this.tenantId);
      this.formData = history.state;
      log.debug('formData:', this.formData);
      this.getOrganizationById(this.formData);
    }
  }

  getOrganizationById(params: any) {
    log.debug('details: ', params);
    this.orgData = params;
    this.orgGetData = params;
    log.debug('orgData details', this.orgData);
    log.debug('orgGetData details', this.orgGetData);
    this.setOrgFormData();
  }

  setOrgFormData() {
    log.debug('orgDataa:', this.orgData);
    this.organizationFormGroup.value.countryCode = this.orgData.countryCode;
    this.organizationFormGroup.patchValue(this.orgData);
    log.debug('organizationFormGroup', this.organizationFormGroup);
    this.setFormData();
  }

  setFormData() {
    const orgFormData = this.orgData;

    orgFormData.forEach((element: any) => {
      this.orgArray.push(element.TenantId);
      log.debug('orgArray', this.orgArray);
    });

    this.organizationFormGroup.get('TenantId').patchValue(this.orgArray);
    this.organizationFormGroup.patchValue(this.orgData);
  }

  onSubmit() {
    this.cacheServiceService.setOrganizationTree('');
    this.organizationFormGroup.value.countryCode = this.orgData.countryCode;
    const data = this.organizationFormGroup.value;
    log.debug('show data:', data);

    const reqData = {
      name: this.organizationFormGroup.value.name,
      organisationDomain: this.organizationFormGroup.value.organisationDomain,
      email: this.organizationFormGroup.value.email,
      phone: this.organizationFormGroup.value.phone,
      countryCode: this.organizationFormGroup.value.countryCode,
      addressLine1: this.organizationFormGroup.value.addressLine1,
      addressLine2: this.organizationFormGroup.value.addressLine2,
      addressLine3: this.organizationFormGroup.value.addressLine3,
      city: this.organizationFormGroup.value.city,
      state: this.organizationFormGroup.value.state,
      country: this.organizationFormGroup.value.country,
      zipcode: this.organizationFormGroup.value.zipcode,
      isActive: this.organizationFormGroup.value.isActive,
    };

    log.debug('reqData', reqData);
    this.organizationService.updateOrganization(reqData, this.tenantId).subscribe(
      (response) => {
        log.debug('response: ', response);
        this.organizationData = response;

        if (this.organizationData.statusCode === 200) {
          const message = 'Organization submitted successfully!';
          this.notifyService.showSuccess(message, this.title);
          this.overviewReset();
          this.router.navigate(['/organization']);
        } else {
          if (this.organizationData.statusText === 'Conflict') {
            log.debug('conflict error');
            log.debug('message', this.organizationData.error.message);
            const message = this.organizationData.error.message;
            this.notifyService.showError(message, this.title);
          } else {
            const message = this.organizationData.error.message;
            this.notifyService.showError(message, this.title);
          }
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  overviewReset() {
    this.organizationFormGroup.reset();
  }

  onCountryChange(country: any) {
    //  console.log('countrycodes234234', { country });
    log.debug('countrycodes:', country);
    this.countryCode = country.dialCode;
    return this.countryCode;
  }

  getNumber(event: any) {
    this.phonenumber = event;
    log.debug('event', event);
  }
  telInputObject(event: any) {
    log.debug('countrycodes:', event);
    this.code = event.s.iso2;
    log.debug('code:', this.code);
    event.setDialCode(this.countryCode);
    event.setCountry(this.countryCode);
  }
  onError(obj: any) {
    this.hasError = obj;
    // console.log('countrycodes234234', { obj });
    // log.debug('hasError:', this.hasError);
    return obj;
  }
}
