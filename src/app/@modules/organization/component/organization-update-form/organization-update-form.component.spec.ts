import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationUpdateFormComponent } from './organization-update-form.component';

describe('OrganizationUpdateFormComponent', () => {
  let component: OrganizationUpdateFormComponent;
  let fixture: ComponentFixture<OrganizationUpdateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrganizationUpdateFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
