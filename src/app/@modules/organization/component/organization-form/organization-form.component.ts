import { Component, OnInit, ViewChild } from '@angular/core';
import { OrganizationService } from '../../organization.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service/notification.service';
import { FormlyFieldConfig } from '@formly';
import { ICustomFormlyConfig } from '@types';
import { TelInputTypeComponent } from '@formly/types/tel-input/tel-input-type.component';

const log = new Logger('Organization-Form');

@Component({
  selector: 'app-organization-form',
  templateUrl: './organization-form.component.html',
  styleUrls: ['./organization-form.component.scss'],
})
export class OrganizationFormComponent implements OnInit {
  @ViewChild('ng2TelInput') ng2TelInput: TelInputTypeComponent;

  organizationFormGroup: FormGroup = new FormGroup({});
  overViewFormGroup: FormGroup;
  error: string | undefined;
  public TenantId: any;
  public organizationData: any;
  public arr: any = [];
  public countryCode: any;
  public phonenumber: any;
  hasError: boolean;
  public organizationTree: any;
  public title = 'Organization';

  model = {};
  fields: FormlyFieldConfig[] = [
    {
      template: ' <div><b>Overview</b><br/><br/><strong>BASIC DETAILS:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'name',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Name',
            placeholder: 'Name',
            required: true,
            throwError: 'Name is required',
          },
        },

        {
          key: 'organisationDomain',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Domain',
            placeholder: 'Domain',
            required: true,
            throwError: 'Domain is required',
          },
        },
      ],
    },
    {
      template: ' <div><strong>CONTACT DETAILS:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'email',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Email',
            placeholder: 'Email',
            required: true,
          },
          validators: {
            email: {
              expression: (c: any) => /\S+@\S+\.\S+/.test(c.value),
              message: (error: any, field: FormlyFieldConfig) =>
                `"${field.formControl.value}" is not a valid Email Address`,
            },
          },
        },

        {
          key: 'phone',
          type: 'tel-input',
          className: 'flex-2',
          templateOptions: {
            label: 'Phone',
            placeholder: 'Phone',
            required: true,
            onError: this.onError.bind(this),
            onCountryChange: this.onCountryChange.bind(this),
            getNumber: this.getNumber.bind(this),
            telInputObject: this.telInputObject.bind(this),
          },
          modelOptions: {
            updateOn: 'blur',
          },
          validators: {
            phone: {
              expression: (c: FormControl) => {
                // console.log({ phoneNUmber: c.value, errors: c.errors, c });
                return !this.hasError;
              },
              message: (error: any, field: FormlyFieldConfig) => {
                //  console.log({ error, field });
                return `${field.formControl.value} is not a valid Phone number`;
              },
            },
          },
        },
      ],
    },
    {
      template: ' <div><strong>ADDRESS DETAILS:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'addressLine1',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Address Line 1',
            placeholder: 'Address Line 1',
            required: true,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'addressLine2',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Address Line 2',
            placeholder: 'Address Line 2',
            required: false,
          },
        },

        {
          key: 'addressLine3',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Address Line 3',
            placeholder: 'Address Line 3',
            required: false,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'city',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'City',
            placeholder: 'City',
            required: true,
          },
        },

        {
          key: 'state',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'State',
            placeholder: 'State',
            required: true,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'country',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Country',
            placeholder: 'Country',
            required: true,
          },
        },

        {
          key: 'zipcode',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'ZipCode',
            placeholder: 'ZipCode',
            required: true,
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      config: {
        model: this.model,
        fields: this.fields,
        options: {},
        form: this.organizationFormGroup,
      },
      onSave: () => this.onSubmit(),
      onDiscard: () => this.overviewReset(),
    },
  ];

  constructor(
    private organizationService: OrganizationService,
    private router: Router,
    private notifyService: NotificationService
  ) {}

  get f() {
    return this.organizationFormGroup.controls;
  }

  ngOnInit() {}

  onSubmit() {
    log.debug('countryCode', this.countryCode);
    this.organizationFormGroup.value.countryCode = this.countryCode;
    const data = this.organizationFormGroup.value;
    log.debug('countryCode', this.countryCode);
    log.debug('submit reached');
    log.debug('data', data);

    this.organizationService.createOrganization(data).subscribe(
      (response) => {
        log.debug('response: ', response);
        this.organizationData = response;
        if (this.organizationData.statusCode === 201) {
          const message = 'Organization submitted successfully!';
          this.notifyService.showSuccess(message, this.title);
          this.overviewReset();
          this.router.navigate(['/organization']);
        } else {
          if (this.organizationData.statusText === 'Conflict') {
            log.debug('conflict error');
            log.debug('message', this.organizationData.error.message);
            const message = this.organizationData.error.message;
            this.notifyService.showError(message, this.title);
          } else {
            const message = this.organizationData.error.message;
            this.notifyService.showError(message, this.title);
          }
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }
  overviewReset() {
    this.organizationFormGroup.reset();
  }

  onCountryChange(country: any) {
    //  console.log('countrycodes234234', { country });
    //  log.debug('countrycodes:', country);
    this.countryCode = country.dialCode;
    return this.countryCode;
  }

  getNumber(event: any) {
    this.phonenumber = event;
    log.debug('event', event);
  }
  telInputObject(event: any) {
    log.debug('countrycodes:', event);
  }
  onError(obj: any) {
    this.hasError = obj;
    // console.log('countrycodes234234', { obj });
    // log.debug('hasError:', this.hasError);
    return obj;
  }
}
