import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '@shared';
import { MaterialModule } from '@app/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { OrganizationComponent } from './organization.component';
import { OrganizationTableComponent } from './component/organization-table/organization-table.component';
import { OrganizationFormComponent } from './component/organization-form/organization-form.component';
import { OrganizationUpdateFormComponent } from './component/organization-update-form/organization-update-form.component';
import { OrganizationRoutingModule } from '@modules/organization/organization-routing.module';
import { MatInputModule } from '@angular/material/input';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { FormlyModule } from '@formly';

@NgModule({
  declarations: [
    OrganizationComponent,
    OrganizationTableComponent,
    OrganizationFormComponent,
    OrganizationUpdateFormComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    MatInputModule,
    ReactiveFormsModule,
    NgxIntlTelInputModule,
    Ng2TelInputModule,
    OrganizationRoutingModule,
    FormlyModule,
  ],
})
export class OrganizationModule {}
