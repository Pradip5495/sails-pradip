import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class OrganizationService {
  constructor(private httpClient: HttpClient) {}

  /**
   *  Get All Related Incident Id's
   */
  getAllOrganizationList(): Observable<Response> {
    const API_URL = `/tenant`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   * Submit Organization  Data
   */
  createOrganization(data: any): Observable<Response> {
    return this.httpClient.post('/tenant', data).pipe(
      tap((responseData: any) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        if (responseData[statusCode] === 200) {
          return responseData[dataParam];
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }

  /**
   * Update Organization Data
   */
  updateOrganization(data: any, tuid: any): Observable<Response> {
    const API_URL = `/tenant/${tuid}`;
    return this.httpClient.put(API_URL, data).pipe(
      tap((responseData: any) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        if (responseData[statusCode] === 200) {
          return responseData[dataParam];
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }

  /**
   * Delete Organization Data
   */
  deleteOrganization(tuid: any): Observable<Response> {
    const API_URL = `/tenant/${tuid}`;
    return this.httpClient.delete(API_URL).pipe(
      tap((responseData: any) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        if (responseData[statusCode] === 200) {
          return responseData[dataParam];
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }
}
