import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { extract } from '@app/i18n';

import { OrganizationComponent } from './organization.component';
import { OrganizationFormComponent } from './component/organization-form/organization-form.component';
import { OrganizationTableComponent } from './component/organization-table/organization-table.component';
import { OrganizationUpdateFormComponent } from './component/organization-update-form/organization-update-form.component';

const routes: Routes = [
  {
    path: '',
    component: OrganizationComponent,
    children: [
      { path: '', component: OrganizationTableComponent },
      { path: 'new-organization-form', component: OrganizationFormComponent },
      { path: ':id/edit', component: OrganizationUpdateFormComponent },
    ],
    data: { title: extract('organization') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class OrganizationRoutingModule {}
