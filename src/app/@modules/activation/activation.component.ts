import { Component, OnInit, ViewChild, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service/notification.service';
import { ActivationService } from '@shared';
import { MatStepper } from '@angular/material/stepper';
import { MustMatch } from './must-match.validator';
import { SearchCountryField, TooltipLabel, CountryISO } from 'ngx-intl-tel-input';

import { PasswordStrengthValidator } from './password-strength.validator';

const log = new Logger('Tenant-Form');

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.css'],
})
export class ActivationComponent implements OnInit {
  @Input() ng2TelInputOptions: any;
  @Output() hasError: EventEmitter<boolean> = new EventEmitter();
  @Output() ng2TelOutput: EventEmitter<any> = new EventEmitter();
  @Output() intlTelInputObject: EventEmitter<any> = new EventEmitter();
  ngTelInput: any;

  get f() {
    return this.tenantActivationFormGroup.controls;
  }
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];

  phoneForm = new FormGroup({
    phone: new FormControl(undefined, [Validators.required]),
  });

  @ViewChild('stepper') stepper: MatStepper;
  tenantActivationFormGroup: FormGroup;
  error: string | undefined;
  token: any;
  domain: any;
  public title = 'User-Registeration';
  public organizationData: any;
  public fielderror: any;
  public countryCode: any = 1;
  public phonenumber: any;
  public placeholder: any;
  public activeIndex: any = 1;
  public specialCharacters: any;
  public lowercase: any;
  hide = true;
  hide2 = true;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private activationService: ActivationService,
    private notifyService: NotificationService
  ) {}

  @HostListener('blur') onBlur() {
    const isInputValid: boolean = this.isInputValid();
    if (isInputValid) {
      const telOutput = this.ngTelInput.intlTelInput('getNumber');
      this.hasError.emit(isInputValid);
      this.ng2TelOutput.emit(telOutput);
    } else {
      this.hasError.emit(isInputValid);
    }
  }

  isInputValid(): boolean {
    return this.ngTelInput.intlTelInput('isValidNumber') ? true : false;
  }

  changePreferredCountries() {
    this.preferredCountries = [CountryISO.India, CountryISO.Canada];
  }

  ngOnInit() {
    this.specialCharacters = /(?=.*[$@$!%*?&]).{8,}/;
    this.lowercase = /(?=.*[a-z]).{8,}/;
    this.route.params.forEach((params: Params) => {
      this.token = params.token;
      log.debug(this.token, +'\n' + this.token);
    });

    this.route.paramMap.subscribe((params) => {
      log.debug(params);
    });

    this.route.queryParams.subscribe((params) => {
      log.debug(params.token);
    });
    this.tenantActivationFormGroup = this.formBuilder.group(
      {
        username: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
        password: [
          '',
          [Validators.required, PasswordStrengthValidator, Validators.maxLength(16), Validators.minLength(8)],
        ],

        cpassword: ['', Validators.required],
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        phone: ['', [Validators.required]],
        addressLine1: ['', Validators.required],
        addressLine2: [''],
        addressLine3: [''],
        city: ['', Validators.required],
        state: ['', Validators.required],
        country: ['', Validators.required],
        countryCode: [''],
        zipcode: ['', Validators.required],
        token: [this.token],
      },
      {
        validator: MustMatch('password', 'cpassword'),
      }
    );
  }

  onSubmit() {
    this.tenantActivationFormGroup.value.countryCode = this.countryCode;
    const data = this.tenantActivationFormGroup.value;
    log.debug('activation', data);

    this.activationService.createActivation(data).subscribe(
      (response) => {
        this.organizationData = response;
        if (this.organizationData.statusCode === 200) {
          const message = 'Tenant Created successfully!';
          this.notifyService.showSuccess(message, this.title);
          this.overviewReset();
          this.router.navigate(['/login']);
        } else {
          const message = this.organizationData.message;
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  move(index: number) {
    this.stepper.selectedIndex = index;
    this.activeIndex = index + 1;
  }

  overviewReset() {
    this.tenantActivationFormGroup.reset();
  }

  onCountryChange(country: any) {
    this.countryCode = country.dialCode;
    return this.countryCode;
  }

  getNumber(event: any) {
    log.debug('show events:', event);
    this.phonenumber = event;
    log.debug('show events3:', this.phonenumber);
  }

  telInputObject(event: any) {
    log.debug('show events2:', this.intlTelInputObject);
  }

  onError(obj: any) {
    this.hasError = obj;
    log.debug('hasError', obj);
    return obj;
  }
}
