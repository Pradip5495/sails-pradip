import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '@shared';
import { MaterialModule } from '@app/material.module';
import { ActivationRoutingModule } from './activation-routing.module';
import { ActivationComponent } from './activation.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Ng2TelInputModule } from 'ng2-tel-input';
// import { MatFormFieldModule, MatInputModule } from '@app/material.module';

@NgModule({
  declarations: [ActivationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    SharedModule,
    MatFormFieldModule,
    MatInputModule,
    FlexLayoutModule,
    MaterialModule,
    NgxIntlTelInputModule,
    Ng2TelInputModule,
    ActivationRoutingModule,
  ],
})
export class ActivationModule {}
