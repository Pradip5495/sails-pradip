import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivationComponent } from './activation.component';
import { extract } from '@app/i18n';

const routes: Routes = [
  {
    path: ':token',
    component: ActivationComponent,
    data: { title: extract('Activation') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class ActivationRoutingModule {}
