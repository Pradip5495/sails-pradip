import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '@shared';
import { MaterialModule } from '@app/material.module';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { AuthenticationGuard, AuthenticationService } from '@core/auth';
import { FlipModule } from 'ngx-flip';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    SharedModule,
    FlipModule,
    FlexLayoutModule,
    MaterialModule,
    LoginRoutingModule,
  ],
  providers: [AuthenticationGuard, AuthenticationService],
  declarations: [LoginComponent],
})
export class LoginModule {}
