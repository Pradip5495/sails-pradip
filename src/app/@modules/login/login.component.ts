import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { I18nService } from '@app/i18n';
import { Logger, untilDestroyed } from '@core';
import { AuthenticationService, CredentialsService } from '@core/auth';
import { environment } from '@env/environment';
import { UserService } from '@shared';
import { NotificationService } from '@shared/common-service';
import { ForgotPasswordService, Icons } from '@shared/service';
import { finalize } from 'rxjs/operators';

const log = new Logger('Login');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  version: string | null = environment.version;
  error: string | undefined;
  loginForm!: FormGroup;
  forgotPassword!: FormGroup;
  isLoading = false;
  hideForm = false;
  showMessage = false;
  flip = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private i18nService: I18nService,
    private authenticationService: AuthenticationService,
    private credentialsService: CredentialsService,
    private userService: UserService,
    private _snackBar: MatSnackBar,
    private forgotPasswordService: ForgotPasswordService,
    private notifyService: NotificationService
  ) {
    this.createForm();
    if (this.credentialsService.isAuthenticated()) {
      this.router.navigate(['/']);
    }
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  ngOnInit() {
    this.createForm();
  }

  ngOnDestroy() {}

  login() {
    this.isLoading = true;
    const login$ = this.authenticationService.login(this.loginForm.value);
    login$
      .pipe(
        finalize(() => {
          this.loginForm.markAsPristine();
          this.isLoading = false;
        }),
        untilDestroyed(this)
      )
      .subscribe(
        (credentials) => {
          if (!(credentials instanceof HttpErrorResponse)) {
            this.userService.getProfile().subscribe(
              (response: any) => {
                log.debug(`${response} successfully logged in`);
                // TODO use MyProfile's menu
                const myMenu: any = {
                  incidentRoutes: [
                    {
                      displayName: 'Dashboard',
                      iconName: 'stats',
                      route: 'incident/investigation-stats',
                      children: [],
                    },
                    {
                      displayName: 'Near Miss',
                      iconName: 'near-miss',
                      route: 'incident/near-miss',
                      children: [
                        {
                          displayName: 'All Reports',
                          iconName: Icons.APPS_WHITE,
                          route: 'incident/near-miss',
                          children: null,
                        },
                        {
                          displayName: 'Un-Safe Condition',
                          iconName: Icons.STOP,
                          route: 'incident/near-miss/filter/Unsafe Condition',
                          children: null,
                        },
                        {
                          displayName: 'Un-Safe Act',
                          iconName: Icons.WARNING_WHITE,
                          route: 'incident/near-miss/filter/Unsafe Act',
                          children: null,
                        },
                        {
                          displayName: 'Near Miss',
                          iconName: Icons.NEAR_MISS,
                          route: 'incident/near-miss/filter/near-miss',
                          children: null,
                        },
                        {
                          displayName: 'Significant Near Miss',
                          iconName: Icons.INCIDENT,
                          route: 'incident/near-miss/filter/Significant Near Miss',
                          children: null,
                        },
                      ],
                    },
                    {
                      displayName: 'Incident',
                      iconName: 'incident',
                      route: 'incident/incident',
                      children: [
                        {
                          displayName: 'All Reports',
                          iconName: Icons.APPS_WHITE,
                          route: 'incident/incident',
                          children: null,
                        },
                        {
                          displayName: 'Incident General',
                          iconName: Icons.INCIDENT,
                          route: 'incident/incident/filter/general',
                          children: null,
                        },
                        {
                          displayName: 'Injuries',
                          iconName: Icons.INJURIES_ACCIDENT,
                          route: 'incident/incident/filter/injuries',
                          children: null,
                        },
                        {
                          displayName: 'Equipment Failure',
                          iconName: Icons.INCIDENT,
                          route: 'incident/incident/filter/eqptfailure',
                          children: null,
                        },
                      ],
                    },
                    {
                      displayName: 'Fleet Notification',
                      iconName: 'notification',
                      route: 'incident/fleet-notification',
                      children: [],
                    },
                    {
                      displayName: 'Third Party Sharing',
                      iconName: Icons.SHARING,
                      route: 'incident/third-party-sharing',
                      children: [],
                    },
                    {
                      displayName: 'Lesson Learnt',
                      iconName: 'lesson-learnt',
                      route: 'incident/lessons-learnt',
                      children: [],
                    },
                    {
                      displayName: 'Investigators',
                      iconName: 'investigator',
                      route: 'incident/investigators',
                      children: [],
                    },
                    {
                      displayName: 'Report',
                      iconName: 'reports',
                      route: 'incident/investigation-report',
                      children: [],
                    },
                  ],
                  auditRoutes: [
                    {
                      displayName: 'Dashboard',
                      iconName: 'stats',
                      route: 'audit/dashboard',
                      children: [
                        {
                          displayName: 'Summary',
                          iconName: Icons.APPS_WHITE,
                          route: 'audit/dashboard/summary',
                          children: null,
                        },
                      ],
                    },
                    {
                      displayName: 'Planning',
                      iconName: 'lesson-learnt',
                      route: 'audit/planning-approvals',
                      children: [
                        {
                          displayName: 'Plan',
                          iconName: Icons.CONTRACT,
                          route: 'audit/planning-approvals/plan',
                          children: null,
                        },
                        {
                          displayName: 'Calandar',
                          iconName: Icons.CALENDAR,
                          route: 'audit/planning-approvals/calender',
                          children: null,
                        },
                        {
                          displayName: 'Timeline',
                          iconName: Icons.TIMELINE,
                          route: 'audit/planning-approvals/timeline',
                          children: null,
                        },
                        {
                          displayName: 'Reminders',
                          iconName: Icons.REMINDER,
                          route: 'audit/planning-approvals/reminder',
                          children: null,
                        },
                      ],
                    },
                    {
                      displayName: 'Preparation',
                      iconName: 'check-list',
                      route: 'audit/preparation',
                      children: [],
                    },
                    {
                      displayName: 'Inspection Request',
                      iconName: 'notification',
                      route: 'audit/inspection-request',
                      children: [
                        {
                          displayName: 'New Request',
                          iconName: Icons.APPS_WHITE,
                          route: 'audit/inspection-request/request',
                          children: null,
                        },
                        {
                          displayName: 'Closeout Status',
                          iconName: Icons.APPS_WHITE,
                          route: 'audit/inspection-request/closeout-status',
                          children: null,
                        },
                      ],
                    },
                    {
                      displayName: 'Inspectors',
                      iconName: 'investigator',
                      route: 'audit/inspector/list',
                      children: [],
                    },
                    {
                      displayName: 'Inspections',
                      iconName: 'data-entry',
                      route: 'audit/inspections',
                      children: [
                        {
                          displayName: 'Inspections',
                          iconName: Icons.APPS_WHITE,
                          route: 'audit/inspections',
                          children: null,
                        },
                        {
                          displayName: 'Summary',
                          iconName: Icons.APPS_WHITE,
                          route: 'audit/inspections/summary',
                          children: null,
                        },
                      ],
                    },
                    {
                      displayName: 'Report',
                      iconName: 'reports',
                      route: 'incident/investigation-report',
                      children: [],
                    },
                  ],
                  adminRoutes: [
                    {
                      displayName: 'Admin Area',
                      iconName: Icons.REPAIR_TOOL,
                      route: 'admin',
                      children: [
                        {
                          displayName: 'Admin Dashboard',
                          iconName: Icons.APPS_WHITE,
                          route: 'admin/dashboard',
                          children: null,
                        },
                        {
                          displayName: 'Vessel Management',
                          iconName: Icons.SEA_SHIP_WITH_CONTAINERS,
                          route: 'admin/vessel',
                          children: null,
                        },
                        {
                          displayName: 'Custom Data Field Management',
                          iconName: Icons.SEA_SHIP_WITH_CONTAINERS,
                          route: 'admin/custom-data-fields/task',
                          children: null,
                        },
                        {
                          displayName: 'Fleet',
                          iconName: Icons.SEA_SHIP_WITH_CONTAINERS,
                          route: 'admin/fleet',
                          children: null,
                        },
                        {
                          displayName: 'Additional Group',
                          iconName: Icons.SEA_SHIP_WITH_CONTAINERS,
                          route: 'admin/additional-group',
                          children: null,
                        },
                        {
                          displayName: 'Vessel Owner',
                          iconName: Icons.SEA_SHIP_WITH_CONTAINERS,
                          route: 'admin/vessel-owner',
                          children: null,
                        },
                        {
                          displayName: 'Port',
                          iconName: Icons.SEA_SHIP_WITH_CONTAINERS,
                          route: 'admin/port',
                          children: null,
                        },
                        {
                          displayName: 'User Management',
                          iconName: Icons.INVESTIGATOR,
                          route: 'admin/user',
                          children: null,
                        },
                        {
                          displayName: 'Access Control',
                          iconName: Icons.REPAIR_TOOL,
                          route: 'admin/access-control',
                          children: null,
                        },
                      ],
                    },
                  ],
                  organizationRoutes: [
                    {
                      displayName: 'Organization',
                      iconName: Icons.ORGANIZATION,
                      route: 'organization',
                      children: [],
                    },
                  ],
                };
                response.myMenu = myMenu;
                const resp = JSON.stringify(response);
                this.userService.setProfile(resp);
                this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
              },
              (error: string) => {
                log.debug(`user profile error: ${error}`);
                this.error = error;
                const message = 'Error in Login';
                this._snackBar.open(message, '', {
                  duration: 40000,
                });
                this.router.navigate(['/login']);
              }
            );
          } else {
            const message = 'Error in Login';
            this._snackBar.open(message, '', {
              duration: 40000,
            });
            this.router.navigate(['/login']);
          }
        },
        (error) => {
          log.debug(`Login error: ${error}`);
          this.error = error;
          const message = 'Error in Login';
          this._snackBar.open(message, '', {
            duration: 40000,
          });
          this.router.navigate(['/login']);
        }
      );
  }

  getPasswordLink() {
    this.isLoading = true;
    const forgotPasswordData = this.forgotPassword.value;
    log.debug('forgotPasswordData', forgotPasswordData);
    this.forgotPasswordService.forgotPassword(forgotPasswordData).subscribe(
      (response) => {
        this.notifyService.showSuccess(response.message);
        this.isLoading = false;
        this.hideForm = true;
        this.showMessage = true;
      },
      (error) => {
        log.debug('response', error);
        this.error = error;
        this.isLoading = false;
        this.notifyService.showError(this.error);
      }
    );
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  rotate() {
    this.flip = !this.flip;
    this.hideForm = false;
    this.showMessage = false;
    this.error = null;
    this.forgotPassword.reset();
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      domain: ['', Validators.required],
      remember: true,
    });

    this.forgotPassword = this.formBuilder.group({
      username: ['', Validators.required],
      domain: ['', Validators.required],
    });
  }
}
