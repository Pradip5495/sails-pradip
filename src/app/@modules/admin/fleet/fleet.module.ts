import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FleetListComponent } from './component/fleet-list/fleet-list.component';
import { FleetComponent } from './fleet.component';
import { FleetRoutingModule } from './fleet-routing.module';

import { SharedModule } from '@shared';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { FormlyModule } from '@formly';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [FleetListComponent, FleetComponent],
  imports: [
    CommonModule,
    FleetRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    SharedModule,
    MatFormFieldModule,
    FlexLayoutModule,
    MatDialogModule,
    MaterialModule,
    FormlyModule,
  ],
})
export class FleetModule {}
