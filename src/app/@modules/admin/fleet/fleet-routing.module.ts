import { FleetListComponent } from './component/fleet-list/fleet-list.component';
import { NgModule } from '@angular/core';
import { extract } from '@app/i18n';
import { RouterModule, Routes } from '@angular/router';
import { FleetComponent } from './fleet.component';

// Vessels Child Routes
const children: Routes = [
  {
    path: '',
    component: FleetListComponent,
    data: { title: extract('All Fleet') },
  },
];

// Route Config for Vessels Owner within Admin Routing
const routes: Routes = [
  {
    path: '',
    component: FleetComponent,
    children,
    data: { title: extract('Fleet') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class FleetRoutingModule {}
