import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { AgGridAngular } from '@ag-grid-community/angular';
import { VesselService } from '@shared/service/vessel.service';
import { UserService } from '@shared/service/user.service';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { CellValidationComponent } from '@shared/component/cell-validation/cell-validation.component';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { IJson } from '@types';

const log = new Logger('FleetListComponent');

@Component({
  selector: 'app-fleet-list',
  templateUrl: './fleet-list.component.html',
  styleUrls: ['./fleet-list.component.scss'],
})
export class FleetListComponent implements OnInit {
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();
  @ViewChild('updatedialog', { static: true }) _updateDialogContent: any;
  @ViewChild('newdialog', { static: true }) _addDialogContent: any;
  @ViewChild('addVesseldialog', { static: true }) _vesselContent: any;
  gridOptions: any;

  get f() {
    return this.fleetFormGroup.controls;
  }

  fleetFormGroup: FormGroup;
  fleetUpdateFormGroup: FormGroup;
  fleetVesselFormGroup: FormGroup;
  vesselDetails: FormArray;

  vesselfleetData: any;
  public fleetData: any = [];
  public suppressRowDeselection: any;
  public gridApi: any;
  public gridColumnApi: any;
  public rowSelection: any;
  public sortingOrder: any;
  public fleetManagerId: any;
  public fleetManagerUpdateId: any;
  public selectedValue: any;
  public fleetId: any;
  public active: any;
  public noVesselMessage: any = false;
  // public type: string = 'unassigned';
  public addVesselDetail: any = [];
  updatingFleet = false;
  public fleetUpdateDetails: any = [];
  public updataListed: any = [];
  public checkboxVessel: any = [];
  public vesselIdFleet: any = [];
  public checkedVessel: any = [];

  selected = 'option';
  selectedItem: string;

  animal: string;
  name: string;

  @ViewChild('agGrid') agGrid: AgGridAngular;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  // Notify parent for Page Metadata Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  public array: any;
  public length = 100;
  public rowData: any;
  public sideBar: any;
  public id: any;
  public icons: any;
  public columnName: any;
  public vesselId: any;
  rowDataClicked1 = {};
  deleteData: any;
  title: string;
  error: string | undefined;
  public vessel: any;
  public manager: any = [];
  public vesselArray: any = [];
  public managerWithId: any = [];
  public vesselIdArray: any = [];
  public rowList: any = [];
  displayDialog = false;
  displayUpdateDialog = false;
  displayVesselDialog = false;
  checked = 'true';
  public type: any = 'true';
  public vesselFleet: any = [];

  public formData: any = [];
  domLayout = 'autoHeight';
  vesselTypeValue: any = [];
  vesselTypeValueWithId: any = [];
  vesselOwnerValue: any = [];
  vesselOwnerValueWithId: any = [];
  fleetNameValue: any = [];
  fleetNameValueWithId: any = [];
  portNameValue: any = [];
  portNameValueWithId: any = [];
  fleetManagerValue: any = [];
  fleetManagerValueWithId: any = [];
  vesselValue: any = [];
  vesselValueWithId: any = [];
  vesselsList: any = [];
  showVessels: any = [];

  editType = '';

  columnDefs = [
    {
      headerName: 'Fleet Name',
      field: 'name',
      flex: 1,
      resizable: true,
      cellEditor: 'cellValidation',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Manager',
      field: 'managerFirstName',
      flex: 1,
      resizable: true,
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellEditorParams: (manager: any) => {
        return {
          values: this.manager,
        };
      },
    },
    {
      headerName: 'Actions',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deleteFleet.bind(this),
        onClickEdit: this.editItems.bind(this),
        hideDetailsButton: true,
      },
      filter: 'agTextColumnFilter',
    },
  ];

  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };
  paginationPageSize: number;

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    cellValidation: CellValidationComponent,
  };

  constructor(
    private readonly vesselService: VesselService,
    private readonly notificationService: NotificationService,
    private notifyService: NotificationService,
    private readonly userService: UserService,
    private formBuilder: FormBuilder
  ) {
    this.rowSelection = 'single';
    this.suppressRowDeselection = true;
  }

  onCellValueChanged(params: any) {
    log.debug('on cell value change function');
    const managerData = this.managerWithId.find((user: any) => user.firstname === params.data.managerFirstName);
    const reqData = {
      name: params.data.name,
      managerId: managerData.userId,
      isActive: params.data.isActive,
    };

    const handleResponse = (data: any) => {
      this.notificationService.showSuccess(data.message);
      this.onGridReady(params);
    };
    this.vesselService.updateVesselFleet(params.data.fleetId, reqData).subscribe(handleResponse, this.showError);
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    const handleResponse = (response: any) => {
      this.isdataSource = true;
      this.rowData = response;
    };
    this.vesselService.getVesselFleets().subscribe(handleResponse, this.showError);
  }

  onRowSelected(event: any) {
    if (!event.node.selected) {
      return;
    }

    log.debug('on row selected function', event);
    this.gridApi.getSelectedRows();
    this.showVessels = [];
    this.vesselIdFleet = [];
    this.checkboxVessel = [];
    this.addVesselDetail = event.data;
    this.vesselList(event.data.vesselDetails);
    log.debug('vessel List ', event.data.vesselDetails);
  }

  deleteFleet(params: any) {
    const handleResponse = (response: any) => {
      this.deleteData = response;
      this.notifyService.showSuccess(this.deleteData.message, this.title);
      this.onGridReady(params);
    };
    this.vesselService.deleteVesselFleet(params.rowData.fleetId).subscribe(handleResponse, this.showError);
  }

  fetchData() {
    this.vesselService.getVessels().subscribe((data) => (this.rowData = data));
  }

  onAddForm() {
    this.openDialog();
  }

  ngOnInit(): void {
    this.managerList();
    this.pageSubTitle.emit('ALL VESSELS');
    this.activePageIndex.emit(0);

    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };

    this.paginationPageSize = 25;

    this.fleetFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      managerId: ['', Validators.required],
      vesselId: ['', Validators.required],
    });

    this.fleetUpdateFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      managerId: ['', Validators.required],
      vesselDetails: this.formBuilder.array([]),
    });

    this.fleetVesselFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      managerId: ['', Validators.required],
    });
  }

  managerList() {
    const handleResponse = (data: any) => {
      this.managerWithId = data;
      log.debug('managerlist:', this.managerWithId);
      this.manager = data.map((item: any) => item.firstname);
    };
    this.userService.getUsers().subscribe(handleResponse, this.showError);
  }

  showVessel(isFiltersApplied: boolean = true) {
    this.vesselArray = [];
    const filters = new Map();
    if (isFiltersApplied) {
      filters.set('fleet', 'unassigned');
    }
    this.vesselService.getVessels(filters).subscribe(
      (data) => {
        this.vesselFleet = data;
        this.vesselArray = this.vesselFleet.map((fleet: IJson) => fleet);
        log.debug('vesselArray:', this.vesselArray);
        const message = this.vesselFleet.message;
        this.notifyService.showSuccess(message, this.title);
        this.onGridReady(this.vesselFleet);
      },
      (error) => {
        this.showError(error);
      }
    );
  }

  vesselList(event: any) {
    if (event === null) {
      return;
    }
    for (const i of event) {
      log.debug('showvessel',i.vessel);
      this.showVessels.push(i.vessel);
      this.vesselIdFleet.push(i.vesselId);
    }
  }

  onRowDataChanged($params: any) {}

  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map((node) => node.data);
    const selectedDataStringPresentation = selectedData.map((node) => node.make + ' ' + node.model).join(', ');
    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  }

  onSelectionChanged(event: any) {
    log.debug('on selection change function');
    this.vesselsList = [];
  }

  anyFunction() {
    log.debug('AnyFunction Called');
  }

  editItems(params: any) {
    log.debug('edit params', params);
    this.formData = params.rowData;
    this.openUpdateDialog(this.formData);
  }

  onSubmit() {
    this.vesselIdFleet = this.checkboxVessel.map((vessel: any) => vessel.vesselId);
    this.fleetFormGroup.value.vesselId = this.vesselIdFleet;

    const fleetData = this.fleetFormGroup.value;

    const handleResponse = (response: IJson) => {
      log.debug('response: ', response);
      this.fleetData = response;
      this.notifyService.showSuccess('Fleet submitted successfully!', this.title);
      this.overviewReset();
      this._addDialogContent.close();
      this.onGridReady(this.fleetData);
    };

    this.vesselService.createVesselFleet(fleetData).subscribe(handleResponse, this.showError);
  }

  onUpdateSubmit() {
    this.vesselIdArray = [];
    this.fleetManagerUpdateId = this.fleetUpdateFormGroup.value.managerId;
    this.fleetUpdateFormGroup.value.managerId = this.fleetManagerUpdateId;

    this.fleetUpdateFormGroup.value.vesselDetails.map((vessel: any) => {
      if (!(vessel.vessel === false)) {
        this.vesselIdArray.push(vessel.vesselId);
      } else {
        const index = this.showVessels.indexOf(vessel.vessel);
        log.debug('index of show vessel', index);
        this.showVessels.splice(index, 1);
      }
    });

    const reqData = {
      name: this.fleetUpdateFormGroup.value.name,
      managerId: this.fleetUpdateFormGroup.value.managerId,
      vesselId: this.vesselIdArray,
      isActive: this.active,
    };

    this.updatingFleet = true;

    const handleResponse = (response: any) => {
      this.updatingFleet = false;
      this.notifyService.showSuccess('Fleet Updated successfully!');
      this.overviewUpdateReset();
      this.onGridReady(this.fleetData);
      this._updateDialogContent.close();
    };

    this.vesselService.updateVesselFleet(this.fleetId, reqData).subscribe(handleResponse, (error) => {
      this.updatingFleet = false;
      this.showError(error);
    });
  }

  onVesselSubmit() {
    this.checkboxVessel.map((vessel: any) => {
      this.showVessels.push(vessel.vessel);
      this.vesselIdFleet.push(vessel.vesselId);
    });

    const reqData = {
      name: this.addVesselDetail.name,
      managerId: this.addVesselDetail.managerId,
      vesselId: this.vesselIdFleet,
      isActive: this.addVesselDetail.isActive,
    };

    const handleResponse = (response: any) => {
      this.updatingFleet = false;
      log.debug('response data: ', response);
      this.notifyService.showSuccess('Vessel Added Successfully!');
      this.onGridReady(this.fleetData);
      this._vesselContent.close();
    };

    this.vesselService.updateVesselFleet(this.addVesselDetail.fleetId, reqData).subscribe(handleResponse, (error) => {
      this.updatingFleet = false;
      this.showError(error);
    });
  }

  overviewVesselReset() {
    this.fleetVesselFormGroup.reset();
  }

  overviewUpdateReset() {
    this.fleetUpdateFormGroup.reset();
  }

  overviewReset() {
    this.fleetFormGroup.reset();
  }

  discard(event: any) {
    this.displayDialog = false;
  }

  getVesselFormArray() {
    return this.fleetUpdateFormGroup.controls.vesselDetails as FormArray;
  }

  openDialog(): void {
    this.displayDialog = true;
    this.showVessel();
  }

  openUpdateDialog(params: any): void {
    log.debug('open update dialog params:', params);
    this.updataListed = [];
    this.checkboxVessel = [];

    this.displayUpdateDialog = true;

    this.fleetId = params.fleetId;
    this.active = params.isActive;
    this.fleetUpdateDetails = params.vesselDetails;

    this.getVesselFormArray().clear();

    if (params.vesselDetails) {
      params.vesselDetails.map((vessel: any) => {
        this.getVesselFormArray().push(this.formBuilder.group(vessel));
      });
    }

    this.fleetUpdateFormGroup.patchValue({
      name: params.name,
      managerId: params.managerId,
      vesselDetails: [params.vesselDetails],
    });
  }

  openVesselDialog(): void {
    this.displayVesselDialog = true;
    this.fleetVesselFormGroup.patchValue({
      name: this.addVesselDetail.name,
      managerId: this.addVesselDetail.managerFirstName,
    });
    this.showVessel();
  }

  toggle(event: any) {
    this.checkboxVessel.push(event);
    log.debug('toggle is cliecked ', event);
  }

  showError = (message: string) => {
    this.notificationService.showError(message);
  };
}
