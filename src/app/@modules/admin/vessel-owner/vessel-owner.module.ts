import { VesselOwnerRoutingModule } from './vessel-owner-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VesselOwnerListComponent } from './component/vessel-owner-list/vessel-owner-list.component';
import { VesselOwnerComponent } from './vessel-owner.component';

import { SharedModule } from '@shared';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { FormlyModule } from '@formly';
import { NewVesselOwnerComponent } from './component/new-vessel-owner/new-vessel-owner.component';
import { VesselOwnerEditComponent } from './component/vessel-owner-edit/vessel-owner-edit.component';

@NgModule({
  declarations: [VesselOwnerListComponent, VesselOwnerComponent, NewVesselOwnerComponent, VesselOwnerEditComponent],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    VesselOwnerRoutingModule,
    FormlyModule,
  ],
})
export class VesselOwnerModule {}
