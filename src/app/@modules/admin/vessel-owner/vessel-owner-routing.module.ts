import { VesselOwnerListComponent } from './component/vessel-owner-list/vessel-owner-list.component';
import { VesselOwnerComponent } from './vessel-owner.component';
import { NgModule } from '@angular/core';
import { extract } from '@app/i18n';
import { RouterModule, Routes } from '@angular/router';
import { NewVesselOwnerComponent } from './component';
import { VesselOwnerEditComponent } from './component/vessel-owner-edit/vessel-owner-edit.component';

// Vessels Child Routes
const children: Routes = [
  {
    path: '',
    component: VesselOwnerListComponent,
    data: { title: extract('All Vessel Owner') },
  },
  {
    path: 'create',
    component: NewVesselOwnerComponent,
    data: { title: extract('New Vessel Owner') },
  },
  {
    path: ':id/edit',
    component: VesselOwnerEditComponent,
    data: { title: extract('Edit Vessel Owner') },
  },
];

// Route Config for Vessels Owner within Admin Routing
const routes: Routes = [
  {
    path: '',
    component: VesselOwnerComponent,
    children,
    data: { title: extract('Vessel Owner') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class VesselOwnerRoutingModule {}
