import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselOwnerComponent } from './vessel-owner.component';

describe('VesselOwnerComponent', () => {
  let component: VesselOwnerComponent;
  let fixture: ComponentFixture<VesselOwnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VesselOwnerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VesselOwnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
