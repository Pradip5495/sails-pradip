import { Router } from '@angular/router';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { VesselService } from '@shared/service';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { AgGridAngular } from '@ag-grid-community/angular';
import { StatusRendererComponent } from '@shared/component/status-renderer/status-renderer.component';
import { NewRowButtonsComponent } from '@shared/component/new-row-buttons/new-row-buttons.component';
import { CellValidationComponent } from '@shared/component/cell-validation/cell-validation.component';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';

const log = new Logger('ListVesselComponent');

@Component({
  selector: 'app-vessel-owner-list',
  templateUrl: './vessel-owner-list.component.html',
  styleUrls: ['./vessel-owner-list.component.scss'],
})
export class VesselOwnerListComponent implements OnInit {
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();
  public gridApi: any;
  public gridColumnApi: any;
  public sortingOrder: any;
  public domLayout: any = 'autoHeight';

  @ViewChild('agGrid') agGrid: AgGridAngular;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  // Notify parent for Page Metadata Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  public array: any;
  public length = 100;
  public rowData: any;
  public sideBar: any;
  public icons: any;
  public columnName: any;
  public vesselId: any;
  public formData: any = [];
  public id: any;
  rowDataClicked1 = {};
  deleteData: any;
  title: string;
  error: string | undefined;

  vesselTypeValue: any = [];
  vesselTypeValueWithId: any = [];
  vesselOwnerValue: any = [];
  vesselOwnerValueWithId: any = [];
  fleetNameValue: any = [];
  fleetNameValueWithId: any = [];
  portNameValue: any = [];
  portNameValueWithId: any = [];
  fleetManagerValue: any = [];
  fleetManagerValueWithId: any = [];

  editType = '';

  columnDefs = [
    {
      headerName: 'Vessel Owner',
      field: 'name',
      flex: 1,
      resizable: true,
      cellEditor: 'cellValidation',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'Address',
      field: 'address',
      flex: 1,
      resizable: true,
      cellClass: ['my-class1'],
      cellEditor: 'cellValidation',
      editable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Email',
      field: 'email',
      cellEditor: 'cellValidation',
      editable: true,
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Phone',
      field: 'phone',
      editable: true,
      cellEditor: 'cellValidation',
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Countrycode',
      field: 'countryCode',
      flex: 1,
      cellEditor: 'cellValidation',
      resizable: true,
      editable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Company',
      field: 'company',
      flex: 1,
      cellEditor: 'cellValidation',
      resizable: true,
      editable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Actions',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deleteVesselOwner.bind(this),
        onClickEdit: this.editItems.bind(this),
      },
      filter: 'agTextColumnFilter',
    },
  ];

  headerHeight: 100;

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    statusRendererComponents: StatusRendererComponent,
    newRowButtonsComponent: NewRowButtonsComponent,
    cellValidation: CellValidationComponent,
  };

  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };
  paginationPageSize: number;

  constructor(
    private readonly vesselService: VesselService,
    private readonly notificationService: NotificationService,
    private notifyService: NotificationService,
    private router: Router
  ) {}

  onCellValueChanged(params: any) {
    this.vesselService.updateVesselOwner(params.data.vesselOwnerId, params.data).subscribe(
      (data) => {
        this.notificationService.showSuccess(data.message);
        log.debug('updated data:', params.data);
        this.onGridReady(params);
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
        log.debug('not updated', params.data);
      }
    );
  }

  onBtnClick1(e: any) {
    this.rowDataClicked1 = e.rowData;
    alert(this.rowDataClicked1);
  }

  onGridReady(params: any) {
    log.debug('reached');
    log.debug('griddata:', params);
    this.vesselService.getVesselOwners().subscribe(
      (response) => {
        log.debug('Task Data', response);
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
        // this.rowData = this.rowDataTask
        log.debug('rowDatatask2:', this.rowData);
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  deleteVesselOwner(params: any) {
    this.vesselService.deleteVesselOwner(params.rowData.vesselOwnerId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record No Deteted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  fetchData() {
    this.vesselService.getVessels().subscribe((data) => {
      this.rowData = data;
    });
  }

  onAddForm() {
    this.router.navigateByUrl('/admin/vessel-owner/create').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  ngOnInit(): void {
    this.pageSubTitle.emit('ALL VESSELS');
    this.activePageIndex.emit(0);

    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };

    this.paginationPageSize = 5;

    this.sideBar = {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'columns',
          iconKey: 'columns',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
          },
        },
        {
          id: 'customStats',
          labelDefault: 'Vessels',
          labelKey: 'customStats',
          iconKey: 'custom-stats',
          filter: true,
          toolPanel: 'customStatsToolPanel',
        },
      ],
      filter: true,
      position: 'right',
    };
  }

  onRowDataChanged($params: any) {}

  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map((node) => node.data);
    const selectedDataStringPresentation = selectedData.map((node) => node.make + ' ' + node.model).join(', ');
    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  }

  /**
   * @deprecated only for example purpose to copy in other modules.
   */
  anyFunction() {
    log.debug('AnyFunction Called');
  }

  editItems(params: any) {
    this.id = params.rowData.vesselOwnerId;
    this.formData = params.rowData;
    this.router.navigate([`/admin/vessel-owner/${this.id}/edit`], { state: this.formData, skipLocationChange: false });
  }
}
