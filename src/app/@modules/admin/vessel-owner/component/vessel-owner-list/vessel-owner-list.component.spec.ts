import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselOwnerListComponent } from './vessel-owner-list.component';

describe('VesselOwnerListComponent', () => {
  let component: VesselOwnerListComponent;
  let fixture: ComponentFixture<VesselOwnerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VesselOwnerListComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VesselOwnerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
