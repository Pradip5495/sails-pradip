import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVesselOwnerComponent } from './new-vessel-owner.component';

describe('NewVesselOwnerComponent', () => {
  let component: NewVesselOwnerComponent;
  let fixture: ComponentFixture<NewVesselOwnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewVesselOwnerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVesselOwnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
