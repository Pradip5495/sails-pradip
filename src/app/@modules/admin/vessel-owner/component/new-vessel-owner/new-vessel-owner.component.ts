import { VesselService } from '@shared/service/vessel.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NotificationService } from '@shared/common-service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { FormlyFieldConfig } from '@formly';
import { Logger } from '@core';
import { ICustomFormlyConfig } from '@types';

const log = new Logger('AddOwnerComponent');
@Component({
  selector: 'app-new-vessel-owner',
  templateUrl: './new-vessel-owner.component.html',
  styleUrls: ['./new-vessel-owner.component.scss'],
})
export class NewVesselOwnerComponent implements OnInit {
  public countryCode: any;
  public phonenumber: any;
  public title = 'Vessel Owner';
  public vesselOwnerData: any;
  hasError: boolean;

  // Notify parent for Action and Title Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  VesselFormGroup: FormGroup = new FormGroup({});

  model = {};

  fields: FormlyFieldConfig[] = [
    {
      template: ' <div><strong>Basic Details:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'name',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Username',
            placeholder: 'Enter User Name',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'company',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Company',
            placeholder: 'Enter Company Name',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      template: '<div><strong>Contact:</strong></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'email',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Email',
            placeholder: 'email',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'phone',
          type: 'tel-input',
          className: 'flex-1',
          templateOptions: {
            label: 'Phone',
            placeholder: 'Phone',
            required: true,
            onError: this.onError.bind(this),
            onCountryChange: this.onCountryChange.bind(this),
            getNumber: this.getNumber.bind(this),
            telInputObject: this.telInputObject.bind(this),
          },
          modelOptions: {
            updateOn: 'blur',
          },
          validators: {
            phone: {
              expression: (c: FormControl) => {
                // console.log({ phoneNUmber: c.value, errors: c.errors, c });
                return !this.hasError;
              },
              message: (error: any, field: FormlyFieldConfig) => {
                //  console.log({ error, field });
                return `${field.formControl.value} is not a valid Phone number`;
              },
            },
          },
        },
      ],
    },

    {
      template: '<div><strong>Dimension:</strong></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'address',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Address',
            placeholder: 'Enter address',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      config: {
        form: this.VesselFormGroup,
        model: this.model,
        options: {},
        fields: this.fields,
      },
      onSave: () => this.onSubmit(),
      onDiscard: () => this.overviewReset(),
    },
  ];

  constructor(
    private notifyService: NotificationService,
    private readonly vesselService: VesselService,
    private router: Router,
    private readonly notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.pageSubTitle.emit('NEW USER');
    this.activePageIndex.emit(1);
  }

  onSubmit() {
    this.VesselFormGroup.value.countryCode = this.countryCode;
    const data = this.VesselFormGroup.value;
    log.debug('Submit Create user Request', data);
    this.vesselService.createVesselOwner(data).subscribe(
      (response) => {
        log.debug('response: ', response);
        this.vesselOwnerData = response;
        if (this.vesselOwnerData.statusCode === 201) {
          const message = 'Vessel Owner submitted successfully!';
          this.notifyService.showSuccess(message, this.title);
          this.overviewReset();
          this.router.navigate(['/admin/vessel-owner']);
        } else {
          const message = this.vesselOwnerData.error.message;
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
        log.debug('not updated', data);
      }
    );
  }

  overviewReset() {
    this.VesselFormGroup.reset();
  }

  onCountryChange(country: any) {
    //  console.log('countrycodes234234', { country });
    //  log.debug('countrycodes:', country);
    this.countryCode = country.dialCode;
    return this.countryCode;
  }

  getNumber(event: any) {
    this.phonenumber = event;
    log.debug('event', event);
  }
  telInputObject(event: any) {
    log.debug('countrycodes:', event);
  }
  onError(obj: any) {
    this.hasError = obj;
    // console.log('countrycodes234234', { obj });
    // log.debug('hasError:', this.hasError);
    return obj;
  }
  navigateBack() {
    this.router.navigateByUrl('/admin/vessel-owner').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }
}
