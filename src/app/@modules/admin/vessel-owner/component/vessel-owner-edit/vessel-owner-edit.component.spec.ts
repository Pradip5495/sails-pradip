import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselOwnerEditComponent } from './vessel-owner-edit.component';

describe('VesselOwnerEditComponent', () => {
  let component: VesselOwnerEditComponent;
  let fixture: ComponentFixture<VesselOwnerEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VesselOwnerEditComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VesselOwnerEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
