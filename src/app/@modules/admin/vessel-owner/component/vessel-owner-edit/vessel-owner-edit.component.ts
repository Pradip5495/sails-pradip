import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { VesselService } from '@shared/service';
import { NotificationService } from '@shared/common-service';
import { FormGroup, FormControl } from '@angular/forms';
import { UpdateUser } from '@shared/models';
import { FormlyFieldConfig, FormlyFormOptions } from '@formly';
import { Logger } from '@core';
import { ActivatedRoute, Router } from '@angular/router';
import { ICustomFormlyConfig } from '@types';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';

const log = new Logger('EditOwnerComponent');

@Component({
  selector: 'app-vessel-owner-edit',
  templateUrl: './vessel-owner-edit.component.html',
  styleUrls: ['./vessel-owner-edit.component.scss'],
})
export class VesselOwnerEditComponent implements OnInit {
  error: string | undefined;
  public title: any;
  public showDetails: any;
  public formData: any = [];
  public countryCode: any;
  public phonenumber: any;
  public vesselOwnerData: any;
  hasError: boolean;

  // state$: Observable<object>;

  @ViewChild('details') details: CustomizedCellComponent;
  @Output() userDetails: CustomizedCellComponent;
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  form = new FormGroup({});
  public dataId: any;
  updateUser = new UpdateUser();
  options: FormlyFormOptions = {
    formState: {
      limitDate: true,
      today: new Date(),
    },
  };
  fields: FormlyFieldConfig[] = [
    {
      template: ' <div><strong>Basic Details:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'name',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Username',
            placeholder: 'Enter User Name',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'company',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Company',
            placeholder: 'Enter Company Name',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      template: '<div><strong>Contact:</strong></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'email',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Email',
            placeholder: 'email',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'phone',
          type: 'tel-input',
          className: 'flex-1',
          templateOptions: {
            label: 'Phone',
            placeholder: 'Phone',
            required: true,
            onError: this.onError.bind(this),
            onCountryChange: this.onCountryChange.bind(this),
            getNumber: this.getNumber.bind(this),
            telInputObject: this.telInputObject.bind(this),
          },
          modelOptions: {
            updateOn: 'blur',
          },
          validators: {
            phone: {
              expression: (c: FormControl) => {
                // console.log({ phoneNUmber: c.value, errors: c.errors, c });
                return !this.hasError;
              },
              message: (error: any, field: FormlyFieldConfig) => {
                //  console.log({ error, field });
                return `${field.formControl.value} is not a valid Phone number`;
              },
            },
          },
        },
      ],
    },

    {
      template: '<div><strong>Dimension:</strong></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'address',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Address',
            placeholder: 'Enter address',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      config: {
        form: this.form,
        model: this.updateUser,
        options: this.options,
        fields: this.fields,
      },
      onSave: this.onSubmit.bind(this),
      onDiscard: () => this.form.reset(),
    },
  ];

  constructor(
    private notifyService: NotificationService,
    private readonly vesselService: VesselService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.pageSubTitle.emit('UPDATE PORT');
    this.activePageIndex.emit(1);
    log.debug('route details:', this.route);

    const paramValue = 'value';
    if (this.route.params[paramValue].id) {
      this.formData = {};
      this.dataId = this.route.params[paramValue].id;

      if (this.route.params[paramValue].id === history.state.vesselOwnerId) {
        this.formData = history.state;
      }

      this.form.patchValue(this.formData);
      this.getUsersById(this.formData);
    }
  }

  getUsersById(params: any) {
    log.debug('usersData: ', params);
    this.form.patchValue(params);
  }

  onSubmit(params: any) {
    log.debug('details:', params);
    log.debug('params submit data:', params.vesselOwnerId);
    this.form.value.countryCode = this.countryCode;
    const data = this.form.value;
    log.debug('data:', data);
    this.vesselService.updateVesselOwner(this.dataId, params).subscribe(
      (response) => {
        log.debug('response data: ', response);
        this.notifyService.showSuccess('Vessel Owner Updated successfully!');
        if (response) {
          this.router.navigate(['/admin/vessel-owner']);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  onCountryChange(country: any) {
    //  console.log('countrycodes234234', { country });
    //  log.debug('countrycodes:', country);
    this.countryCode = country.dialCode;
    return this.countryCode;
  }

  getNumber(event: any) {
    this.phonenumber = event;
    log.debug('event', event);
  }
  telInputObject(event: any) {
    log.debug('countrycodes:', event);
  }
  onError(obj: any) {
    this.hasError = obj;
    // console.log('countrycodes234234', { obj });
    // log.debug('hasError:', this.hasError);
    return obj;
  }
  navigateBack() {
    this.router.navigateByUrl('/admin/vessel-owner').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }
}
