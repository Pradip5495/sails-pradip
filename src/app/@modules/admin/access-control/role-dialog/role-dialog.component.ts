import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@formly';
import { ICustomFormlyConfig } from '@types';

@Component({
  selector: 'app-role-dialog',
  templateUrl: './role-dialog.component.html',
  styleUrls: ['./role-dialog.component.css'],
})
export class RoleDialogComponent implements OnInit {
  roleForm = new FormGroup({});
  model = { role: '' };
  fields: FormlyFieldConfig[] = [
    {
      key: 'role',
      id: 'roleField',
      type: 'input',
      templateOptions: {
        label: 'Role',
        placeholder: 'Enter Role',
        required: true,
      },
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      config: {
        model: this.model,
        fields: this.fields,
        options: {},
        form: this.roleForm,
      },
      legacyForm: true,
    },
  ];

  constructor(public dialogRef: MatDialogRef<RoleDialogComponent>) {
    // this.roleForm = fb.group({
    //   role: ['', Validators.required],
    // });
  }

  ngOnInit(): void {}

  save() {
    this.dialogRef.close(this.roleForm.value);
  }

  close() {
    this.dialogRef.close();
  }
}
