import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessControlRoutingModule } from './access-control-routing.module';
import { AccessControlComponent } from './access-control.component';
import { MaterialModule } from '@app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { RoleDialogComponent } from './role-dialog/role-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormlyModule } from '@formly';

@NgModule({
  declarations: [AccessControlComponent, RoleDialogComponent],
  imports: [
    CommonModule,
    AccessControlRoutingModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MaterialModule,
    MatDialogModule,
    FormlyModule,
  ],
})
export class AccessControlModule {}
