import { Component, OnInit } from '@angular/core';
import { RoleAccessService } from '@shared/service';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { RoleDialogComponent } from '@modules/admin/access-control/role-dialog/role-dialog.component';

const log = new Logger('AccessControlComponent');

/**
 * MENU data with nested structure.
 * Each node has a name and an optional list of children.
 */
interface MenuNode {
  menuName: string;
  view: any;
  menuId: string;
  edit: any;
  route?: any;
  create: any;
  delete: any;
  iconName?: string;
  isActive?: any;
  displayName?: string;
  children?: MenuNode[];
}

/** Flat node with expandable and level information */
interface FlatNode {
  expandable: boolean;
  menuName: string;
  level: number;
}

@Component({
  selector: 'app-access-control',
  templateUrl: './access-control.component.html',
  styleUrls: ['./access-control.component.css'],
})
export class AccessControlComponent implements OnInit {
  public roles: any = [];
  public childData: any;
  public i = 1;
  public isDataSource: any = false;
  activeElement: any = '';
  accessForm: FormGroup;

  treeControl = new FlatTreeControl<FlatNode>(
    (node) => node.level,
    (node) => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    AccessControlComponent._transformer,
    (node) => node.level,
    (node) => node.expandable,
    (node) => node.children
  );

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  // FIXME remove Static declaration using Private member function with runtime pre initialization.
  private static _transformer = (node: MenuNode, level: number) => {
    return {
      expandable: !!node.children && node.children[0] !== null,
      menuName: node.menuName,
      view: node.view,
      edit: node.edit,
      delete: node.delete,
      create: node.create,
      menuId: node.menuId,
      level,
    };
  };

  constructor(
    private readonly roleAccessService: RoleAccessService,
    private readonly notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.getRoles();
  }

  hasChild = (_: number, node: FlatNode) => node.expandable;

  getRoles(): void {
    this.roleAccessService.getRoles().subscribe(
      (response) => {
        log.debug('Role Data', response);
        this.roles = response;
        this.activeElement = this.roles[0].roleId;
        if (this.roles[0].roleId) {
          this.getRoleAccess(this.activeElement);
        }
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  getRoleAccess(roleId: string): void {
    this.roleAccessService.getAccess(roleId).subscribe(
      (response) => {
        log.debug('Role Access Data', response);
        this.isDataSource = true;
        this.dataSource.data = response;
        this.createForm();
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  getAccessInfo(roles: any): void {
    log.debug('clicked', roles);
    this.activeElement = roles.roleId;
    this.getRoleAccess(roles.roleId);
  }

  createForm(): void {
    this.accessForm = this.formBuilder.group({
      roleId: [this.activeElement],
      accessDetails: new FormArray([]),
    });
    let child: any = [];
    const menuData = this.dataSource.data.map((menuNode) => {
      // TODO read child menu data
      const childData = menuNode.children.map((childNode) => {
        return childNode !== null
          ? this.formBuilder.group({
              menuName: [childNode.menuName],
              menuId: [childNode.menuId],
              view: [childNode.view],
              edit: [childNode.edit],
              create: [childNode.create],
              del: [childNode.delete],
            })
          : false;
      });
      // TODO Concat Child Data to set on form
      if (childData[0] !== false) {
        child = [].concat(child, childData);
      }
      return this.formBuilder.group({
        menuName: [menuNode.menuName],
        menuId: [menuNode.menuId],
        view: [menuNode.view],
        edit: [menuNode.edit],
        create: [menuNode.create],
        del: [menuNode.delete],
      });
    });
    // TODO set Form Data
    const menus = [].concat(menuData, child);
    const menuFormArray: FormArray = this.formBuilder.array(menus);
    this.accessForm.setControl('accessDetails', menuFormArray);
  }

  onSubmitForm(): void {
    log.debug('Request Submit', this.accessForm.value);
    this.roleAccessService.createAccess(this.accessForm.value).subscribe(
      (data) => {
        this.getRoleAccess(this.accessForm.value.roleId);
        this.notificationService.showSuccess('Role Access created successfully');
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
      }
    );
  }

  // TODO Handle Checkbox select unselect events
  onChangeAccess(node: any, $event: any, key: string) {
    const accessFormArray = this.accessForm.controls.accessDetails as FormArray;
    let i = 0;
    let index = 0;
    // TODO to update RWX permission
    const data = this.dataSource.data.map((parentMenu) => {
      if (node.menuId === parentMenu.menuId) {
        let menu;
        if (key === 'view') {
          menu =
            $event.checked === true
              ? {
                  route: parentMenu.route,
                  iconName: parentMenu.iconName,
                  isActive: parentMenu.isActive,
                  displayName: parentMenu.displayName,
                  menuName: parentMenu.menuName,
                  menuId: parentMenu.menuId,
                  children: parentMenu.children,
                  view: true,
                  edit: 0,
                  delete: 0,
                  create: 0,
                }
              : {
                  route: parentMenu.route,
                  iconName: parentMenu.iconName,
                  isActive: parentMenu.isActive,
                  displayName: parentMenu.displayName,
                  menuName: parentMenu.menuName,
                  menuId: parentMenu.menuId,
                  children: parentMenu.children,
                  view: 0,
                  edit: 0,
                  delete: 0,
                  create: 0,
                };
        }

        if (key === 'create') {
          menu =
            $event.checked === true
              ? {
                  route: parentMenu.route,
                  iconName: parentMenu.iconName,
                  isActive: parentMenu.isActive,
                  displayName: parentMenu.displayName,
                  menuName: parentMenu.menuName,
                  menuId: parentMenu.menuId,
                  children: parentMenu.children,
                  view: true,
                  edit: 0,
                  delete: 0,
                  create: true,
                }
              : {
                  route: parentMenu.route,
                  iconName: parentMenu.iconName,
                  isActive: parentMenu.isActive,
                  displayName: parentMenu.displayName,
                  menuName: parentMenu.menuName,
                  menuId: parentMenu.menuId,
                  children: parentMenu.children,
                  view: true,
                  edit: 0,
                  delete: 0,
                  create: 0,
                };
        }
        if (key === 'edit') {
          menu =
            $event.checked === true
              ? {
                  route: parentMenu.route,
                  iconName: parentMenu.iconName,
                  isActive: parentMenu.isActive,
                  displayName: parentMenu.displayName,
                  menuName: parentMenu.menuName,
                  menuId: parentMenu.menuId,
                  children: parentMenu.children,
                  view: true,
                  edit: true,
                  delete: 0,
                  create: true,
                }
              : {
                  route: parentMenu.route,
                  iconName: parentMenu.iconName,
                  isActive: parentMenu.isActive,
                  displayName: parentMenu.displayName,
                  menuName: parentMenu.menuName,
                  menuId: parentMenu.menuId,
                  children: parentMenu.children,
                  view: true,
                  edit: 0,
                  delete: 0,
                  create: true,
                };
        }
        if (key === 'del') {
          menu =
            $event.checked === true
              ? {
                  route: parentMenu.route,
                  iconName: parentMenu.iconName,
                  isActive: parentMenu.isActive,
                  displayName: parentMenu.displayName,
                  menuName: parentMenu.menuName,
                  menuId: parentMenu.menuId,
                  children: parentMenu.children,
                  view: true,
                  edit: true,
                  delete: true,
                  create: true,
                }
              : {
                  route: parentMenu.route,
                  iconName: parentMenu.iconName,
                  isActive: parentMenu.isActive,
                  displayName: parentMenu.displayName,
                  menuName: parentMenu.menuName,
                  menuId: parentMenu.menuId,
                  children: parentMenu.children,
                  view: true,
                  edit: true,
                  delete: 0,
                  create: true,
                };
        }
        return menu;
      } else {
        const childData = parentMenu.children.map((childMenu) => {
          if (childMenu === null) {
            return childMenu;
          }
          if (childMenu.menuId === node.menuId) {
            let menu;
            if (key === 'view') {
              menu =
                $event.checked === true
                  ? {
                      route: childMenu.route,
                      iconName: childMenu.iconName,
                      isActive: childMenu.isActive,
                      displayName: childMenu.displayName,
                      menuName: childMenu.menuName,
                      menuId: childMenu.menuId,
                      view: true,
                      edit: 0,
                      delete: 0,
                      create: 0,
                    }
                  : {
                      route: childMenu.route,
                      iconName: childMenu.iconName,
                      isActive: childMenu.isActive,
                      displayName: childMenu.displayName,
                      menuName: childMenu.menuName,
                      menuId: childMenu.menuId,
                      view: 0,
                      edit: 0,
                      delete: 0,
                      create: 0,
                    };
            }

            if (key === 'create') {
              menu =
                $event.checked === true
                  ? {
                      route: childMenu.route,
                      iconName: childMenu.iconName,
                      isActive: childMenu.isActive,
                      displayName: childMenu.displayName,
                      menuName: childMenu.menuName,
                      menuId: childMenu.menuId,
                      view: true,
                      edit: 0,
                      delete: 0,
                      create: true,
                    }
                  : {
                      route: childMenu.route,
                      iconName: childMenu.iconName,
                      isActive: childMenu.isActive,
                      displayName: childMenu.displayName,
                      menuName: childMenu.menuName,
                      menuId: childMenu.menuId,
                      view: true,
                      edit: 0,
                      delete: 0,
                      create: 0,
                    };
            }
            if (key === 'edit') {
              menu =
                $event.checked === true
                  ? {
                      route: childMenu.route,
                      iconName: childMenu.iconName,
                      isActive: childMenu.isActive,
                      displayName: childMenu.displayName,
                      menuName: childMenu.menuName,
                      menuId: childMenu.menuId,
                      view: true,
                      edit: true,
                      delete: 0,
                      create: true,
                    }
                  : {
                      route: childMenu.route,
                      iconName: childMenu.iconName,
                      isActive: childMenu.isActive,
                      displayName: childMenu.displayName,
                      menuName: childMenu.menuName,
                      menuId: childMenu.menuId,
                      view: true,
                      edit: 0,
                      delete: 0,
                      create: true,
                    };
            }
            if (key === 'del') {
              menu =
                $event.checked === true
                  ? {
                      route: childMenu.route,
                      iconName: childMenu.iconName,
                      isActive: childMenu.isActive,
                      displayName: childMenu.displayName,
                      menuName: childMenu.menuName,
                      menuId: childMenu.menuId,
                      view: true,
                      edit: true,
                      delete: true,
                      create: true,
                    }
                  : {
                      route: childMenu.route,
                      iconName: childMenu.iconName,
                      isActive: childMenu.isActive,
                      displayName: childMenu.displayName,
                      menuName: childMenu.menuName,
                      menuId: childMenu.menuId,
                      view: true,
                      edit: true,
                      delete: 0,
                      create: true,
                    };
            }
            return menu;
          } else {
            return childMenu;
          }
        });
        return {
          route: parentMenu.route,
          iconName: parentMenu.iconName,
          isActive: parentMenu.isActive,
          displayName: parentMenu.displayName,
          menuName: parentMenu.menuName,
          menuId: parentMenu.menuId,
          view: parentMenu.view,
          edit: parentMenu.edit,
          delete: parentMenu.delete,
          create: parentMenu.create,
          children: childData,
        };
      }
    });
    this.dataSource.data = data;

    const indexArray = accessFormArray.value.map((menuNode: any) => {
      i++;
      if (node.menuId === menuNode.menuId) {
        return i ? i - 1 : i;
      }
      return false;
    });

    for (const x of indexArray) {
      if (x) {
        index = x;
      }
    }

    switch (key) {
      case 'view':
        if ($event.checked) {
          accessFormArray.value[index].view = $event.checked;
        } else {
          accessFormArray.value[index].view = false;
          accessFormArray.value[index].create = false;
          accessFormArray.value[index].edit = false;
          accessFormArray.value[index].del = false;
        }
        return accessFormArray;
      case 'create':
        if ($event.checked) {
          accessFormArray.value[index].view = $event.checked;
          accessFormArray.value[index].create = $event.checked;
        } else {
          accessFormArray.value[index].create = false;
          accessFormArray.value[index].edit = false;
          accessFormArray.value[index].del = false;
        }
        return accessFormArray;
      case 'edit':
        if ($event.checked) {
          accessFormArray.value[index].view = $event.checked;
          accessFormArray.value[index].create = $event.checked;
          accessFormArray.value[index].edit = $event.checked;
        } else {
          accessFormArray.value[index].edit = false;
          accessFormArray.value[index].del = false;
        }
        return accessFormArray;
      case 'del':
        if ($event.checked) {
          accessFormArray.value[index].view = $event.checked;
          accessFormArray.value[index].create = $event.checked;
          accessFormArray.value[index].edit = $event.checked;
          accessFormArray.value[index].del = $event.checked;
        } else {
          accessFormArray.value[index].del = false;
        }
        return accessFormArray;
      default:
        return false;
    }
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    const dialogRef = this.dialog.open(RoleDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      (data) => {
        if (data) {
          this.createRole(data);
        }
      },
      (error) => {
        log.error(error);
      }
    );
  }

  createRole(data: any): void {
    this.roleAccessService.createRole(data).subscribe(
      (res) => {
        this.getRoles();
        this.notificationService.showSuccess('Role created successfully');
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
      }
    );
  }
}
