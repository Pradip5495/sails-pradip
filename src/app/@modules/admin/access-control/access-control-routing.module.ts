import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessControlComponent } from '@modules/admin/access-control/access-control.component';
import { extract } from '@app/i18n';

const routes: Routes = [
  {
    path: '',
    component: AccessControlComponent,
    data: { title: extract('Role Access') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccessControlRoutingModule {}
