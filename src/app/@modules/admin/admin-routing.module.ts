import { VesselOwnerModule } from '@modules/admin/vessel-owner';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { extract } from '@app/i18n';
import { AdminComponent } from '@modules/admin/admin.component';
import { AdminDashboardComponent } from '@modules/admin/component';
import { PortModule } from './port';

// Admin Child Routes
const children: Routes = [
  { path: '', redirectTo: '/admin/dashboard', pathMatch: 'full' },
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
      {
        path: 'dashboard',
        component: AdminDashboardComponent,
        data: { title: extract('Admin Dashboard') },
      },
      {
        path: 'vessel',
        loadChildren: () => import('./vessel-management').then((m) => m.VesselManagementModule),
      },
      {
        path: 'user',
        loadChildren: () => import('./user-management').then((m) => m.UserManagementModule),
      },
      {
        path: 'access-control',
        loadChildren: () => import('./access-control').then((m) => m.AccessControlModule),
      },
      {
        path: 'custom-data-fields',
        loadChildren: () => import('./custom-data-field-management').then((m) => m.CustomDataFieldManagementModule),
      },
      {
        path: 'vessel-owner',
        loadChildren: () => VesselOwnerModule,
      },
      {
        path: 'port',
        loadChildren: () => PortModule,
      },
      {
        path: 'fleet',
        loadChildren: () => import('./fleet').then((m) => m.FleetModule),
      },
      {
        path: 'additional-group',
        loadChildren: () => import('./additional-group').then((m) => m.AdditionalGroupModule),
      },
    ],
    data: { title: extract('Admin Area') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(children)],
  exports: [RouterModule],
  providers: [],
})
export class AdminRoutingModule {}
