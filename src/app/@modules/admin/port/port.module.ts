import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortListComponent } from './component/port-list/port-list.component';
import { PortRoutingModule } from './port-routing.module';
import { PortComponent } from './port.component';

import { SharedModule } from '@shared';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { FormlyModule } from '@formly';
import { NewPortComponent } from './component/new-port/new-port.component';
import { PortEditComponent } from './component/port-edit/port-edit.component';

@NgModule({
  declarations: [PortListComponent, PortComponent, NewPortComponent, PortEditComponent],
  imports: [
    CommonModule,
    PortRoutingModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    FormlyModule,
  ],
})
export class PortModule {}
