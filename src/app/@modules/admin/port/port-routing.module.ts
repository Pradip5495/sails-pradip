import { PortListComponent } from './component/port-list/port-list.component';
import { PortComponent } from './port.component';
import { NgModule } from '@angular/core';
import { extract } from '@app/i18n';
import { RouterModule, Routes } from '@angular/router';
import { NewPortComponent } from './component/new-port/new-port.component';
import { PortEditComponent } from './component/port-edit/port-edit.component';

// Vessels Child Routes
const children: Routes = [
  {
    path: '',
    component: PortListComponent,
    data: { title: extract('All Port') },
  },
  {
    path: 'create',
    component: NewPortComponent,
    data: { title: extract('New Port') },
  },
  {
    path: ':id/edit',
    component: PortEditComponent,
    data: { title: extract('Edit Port') },
  },
];

// Route Config for Vessels Owner within Admin Routing
const routes: Routes = [
  {
    path: '',
    component: PortComponent,
    children,
    data: { title: extract('Port') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class PortRoutingModule {}
