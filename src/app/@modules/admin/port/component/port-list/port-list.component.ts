import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { AgGridAngular } from '@ag-grid-community/angular';
import { VesselService } from '@shared/service/vessel.service';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { CellValidationComponent } from '@shared/component/cell-validation/cell-validation.component';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';

const log = new Logger('ListVesselComponent');

@Component({
  selector: 'app-port-list',
  templateUrl: './port-list.component.html',
  styleUrls: ['./port-list.component.scss'],
})
export class PortListComponent implements OnInit {
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  public gridApi: any;
  public gridColumnApi: any;
  public sortingOrder: any;
  public domLayout: any = 'autoHeight';

  @ViewChild('agGrid') agGrid: AgGridAngular;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  // Notify parent for Page Metadata Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  public array: any;
  public length = 100;
  public rowData: any;
  public sideBar: any;
  public icons: any;
  public columnName: any;
  public id: any;
  public vesselId: any;
  rowDataClicked1 = {};
  deleteData: any;
  title: string;
  error: string | undefined;
  public manager: any = [];
  public managerWithId: any = [];
  public formData: any = [];

  vesselTypeValue: any = [];
  vesselTypeValueWithId: any = [];
  vesselOwnerValue: any = [];
  vesselOwnerValueWithId: any = [];
  fleetNameValue: any = [];
  fleetNameValueWithId: any = [];
  portNameValue: any = [];
  portNameValueWithId: any = [];
  fleetManagerValue: any = [];
  fleetManagerValueWithId: any = [];

  editType = '';

  columnDefs = [
    {
      headerName: 'Port',
      field: 'name',
      flex: 1,
      resizable: true,
      cellEditor: 'cellValidation',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'Latitude',
      field: 'latitude',
      flex: 1,
      resizable: true,
      cellEditor: 'cellValidation',
      editable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agNumberColumnFilter',
    },
    {
      headerName: 'Longitude',
      field: 'longitude',
      editable: true,
      flex: 1,
      resizable: true,
      cellEditor: 'cellValidation',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agNumberColumnFilter',
    },
    {
      headerName: 'Country',
      field: 'country',
      editable: true,
      cellEditor: 'cellValidation',
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Actions',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deletePort.bind(this),
        onClickEdit: this.editItems.bind(this),
      },
      filter: 'agTextColumnFilter',
    },
  ];

  defaultColDef = {
    enableValue: true,
    sortable: true,
    filter: true,
  };

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    cellValidation: CellValidationComponent,
  };

  paginationPageSize: number;

  constructor(
    private readonly vesselService: VesselService,
    private readonly notificationService: NotificationService,
    private notifyService: NotificationService,
    private router: Router
  ) {}

  onGridReady(params: any) {
    log.debug('reached');
    log.debug('griddata:', params);
    this.vesselService.getVesselPorts().subscribe(
      (response) => {
        log.debug('Task Data', response);
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
        // this.rowData = this.rowDataTask
        log.debug('rowDatatask2:', this.rowData);
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  deletePort(params: any) {
    log.debug('deltdata:', params);
    this.vesselService.deleteVesselPort(params.rowData.portId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record No Deteted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  onCellValueChanged(params: any) {
    this.vesselService.updateVesselPort(params.data.portId, params.data).subscribe(
      (data) => {
        this.notificationService.showSuccess(data.message);
        log.debug('updated data:', params.data);
        this.onGridReady(params);
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
        log.debug('not updated', params.data);
      }
    );
  }

  fetchData() {
    this.vesselService.getVessels().subscribe((data) => {
      this.rowData = data;
    });
  }

  onAddForm() {
    this.router.navigateByUrl('/admin/port/create').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  ngOnInit(): void {
    this.pageSubTitle.emit('ALL PORTS');
    this.activePageIndex.emit(0);

    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };

    this.paginationPageSize = 5;

    this.sideBar = {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'columns',
          iconKey: 'columns',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
          },
        },
        {
          id: 'customStats',
          labelDefault: 'Vessels',
          labelKey: 'customStats',
          iconKey: 'custom-stats',
          filter: true,
          toolPanel: 'customStatsToolPanel',
        },
      ],
      filter: true,
      position: 'right',
    };
  }

  onRowDataChanged($params: any) {}

  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map((node) => node.data);
    const selectedDataStringPresentation = selectedData.map((node) => node.make + ' ' + node.model).join(', ');
    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  }

  /**
   * @deprecated only for example purpose to copy in other modules.
   */
  anyFunction() {
    log.debug('AnyFunction Called');
  }

  editItems(params: any) {
    this.id = params.rowData.portId;
    this.formData = params.rowData;
    // this.router.navigate([`/admin/user/${this.id}/edit`]);

    this.router.navigate([`/admin/port/${this.id}/edit`], { state: this.formData, skipLocationChange: false });
  }
}
