import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { VesselService } from '@shared/service';
import { NotificationService } from '@shared/common-service';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FormlyFieldConfig } from '@formly';
import { Logger } from '@core';
import { ICustomFormlyConfig } from '@types';

const log = new Logger('AddOwnerComponent');

@Component({
  selector: 'app-new-port',
  templateUrl: './new-port.component.html',
  styleUrls: ['./new-port.component.scss'],
})
export class NewPortComponent implements OnInit {
  public countryCode: any;
  public phonenumber: any;
  public title = 'Port';
  public vesselOwnerData: any;
  public portData: any = [];
  hasError: boolean;

  // Notify parent for Action and Title Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  PortFormGroup: FormGroup = new FormGroup({});

  model = {};

  fields: FormlyFieldConfig[] = [
    {
      template: ' <div><strong>Basic Details:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'name',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Username',
            placeholder: 'Enter User Name',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'country',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Country',
            placeholder: 'Enter Country',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'latitude',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Latitude',
            placeholder: 'latitude',
            required: true,
            throwError: 'Field is required',
          },
        },

        {
          key: 'longitude',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Longitude',
            placeholder: 'Enter Longitude',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      config: {
        form: this.PortFormGroup,
        model: this.model,
        options: {},
        fields: this.fields,
      },
      onSave: () => this.onSubmit(),
      onDiscard: () => this.overviewReset(),
    },
  ];

  constructor(
    private notifyService: NotificationService,
    private readonly vesselService: VesselService,
    private router: Router,
    private readonly notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.pageSubTitle.emit('NEW PORT');
    this.activePageIndex.emit(1);
  }

  onSubmit() {
    this.PortFormGroup.value.countryCode = this.countryCode;
    const data = this.PortFormGroup.value;
    log.debug('Submit Create user Request', data);
    this.vesselService.createVesselPort(data).subscribe(
      (response) => {
        log.debug('response: ', response);
        this.portData = response;
        if (this.portData.statusCode === 201) {
          const message = 'Port submitted successfully!';
          this.notifyService.showSuccess(message, this.title);
          this.overviewReset();
          this.router.navigate(['/admin/port']);
        } else {
          const message = this.vesselOwnerData.error.message;
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
        log.debug('not updated', data);
      }
    );
  }

  navigateBack() {
    this.router.navigateByUrl('/admin/port').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  overviewReset() {
    this.PortFormGroup.reset();
  }
}
