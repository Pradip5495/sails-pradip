import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { VesselService } from '@shared/service';
import { NotificationService } from '@shared/common-service';
import { FormGroup } from '@angular/forms';
import { UpdateUser } from '@shared/models';
import { FormlyFieldConfig, FormlyFormOptions } from '@formly';
import { Logger } from '@core';
import { ActivatedRoute, Router } from '@angular/router';
import { ICustomFormlyConfig } from '@types';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';

const log = new Logger('EditPortComponent');

@Component({
  selector: 'app-port-edit',
  templateUrl: './port-edit.component.html',
  styleUrls: ['./port-edit.component.scss'],
})
export class PortEditComponent implements OnInit {
  error: string | undefined;
  public title: any;
  public showDetails: any;
  public formData: any = [];

  // state$: Observable<object>;

  @ViewChild('details') details: CustomizedCellComponent;
  @Output() userDetails: CustomizedCellComponent;
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  form = new FormGroup({});
  public dataId: any;
  updateUser = new UpdateUser();
  options: FormlyFormOptions = {
    formState: {
      limitDate: true,
      today: new Date(),
    },
  };
  fields: FormlyFieldConfig[] = [
    {
      template: ' <div><strong>Basic Details:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'name',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Username',
            placeholder: 'Enter User Name',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'country',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Country',
            placeholder: 'Enter Country',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'latitude',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Latitude',
            placeholder: 'latitude',
            required: true,
            throwError: 'Field is required',
          },
        },

        {
          key: 'longitude',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Longitude',
            placeholder: 'Enter Longitude',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      config: {
        form: this.form,
        model: this.updateUser,
        options: this.options,
        fields: this.fields,
      },
      onSave: this.onSubmit.bind(this),
      onDiscard: () => this.form.reset(),
    },
  ];

  constructor(
    private notifyService: NotificationService,
    private readonly vesselService: VesselService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.pageSubTitle.emit('UPDATE PORT');
    this.activePageIndex.emit(1);
    log.debug('route details:', this.route);

    const paramValue = 'value';
    if (this.route.params[paramValue].id) {
      this.formData = {};
      this.dataId = this.route.params[paramValue].id;

      if (this.route.params[paramValue].id === history.state.portId) {
        this.formData = history.state;
      }

      this.form.patchValue(this.formData);
      this.getUsersById(this.formData);
    }
  }

  getUsersById(params: any) {
    log.debug('usersData: ', params);
    this.form.patchValue(params);
  }

  onSubmit(params: any) {
    log.debug('details:', params);
    log.debug('params submit data:', params.designationId);
    this.vesselService.updateVesselPort(this.dataId, params).subscribe(
      (response) => {
        log.debug('response data: ', response);
        this.notifyService.showSuccess('Port Updated successfully!');
        if (response) {
          this.router.navigate(['/admin/port']);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  navigateBack() {
    this.router.navigateByUrl('/admin/port').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }
}
