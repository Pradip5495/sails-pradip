import { ThirdPartyTypeComponent } from './component/third-party-type/third-party-type.component';
import { NationalityComponent } from './component/nationality/nationality.component';
import { BackgroundConditionComponent } from './component/background-condition/background-condition.component';
import { InjuryOccuredByComponent } from './component/injury-occured-by/injury-occured-by.component';
import { BodyPartAffectedComponent } from './component/body-part-affected/body-part-affected.component';
import { EventImpactTypeComponent } from './component/event-impact-type/event-impact-type.component';
import { VesselTypeComponent } from './component/vessel-type/vessel-type.component';
import { EquipmentDataTableComponent } from './component/equipment/equipment-data-table/equipment-data-table.component';
import { RankDataTableComponent } from './component/rank/rank-data-table/rank-data-table.component';
import { LocationDataTableComponent } from './component/location/location-data-table/location-data-table.component';
import { TaskDataTableComponent } from './component/task/task-data-table/task-data-table.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { extract } from '@app/i18n';
import { CustomDataFieldManagementComponent } from '@modules/admin/custom-data-field-management/custom-data-field-management.component';
import { AgGridModule } from '@ag-grid-community/angular';
import {
  CategoryDataTableComponent,
  AnalysisSimilarIncidentComponent,
  InjuryCategoryComponent,
  LessonLearntTypeComponent,
} from './component';
import { CompanyDataTableComponent } from '@modules/admin/custom-data-field-management/component/company/company-data-table/company-data-table.component';

const children: Routes = [
  {
    path: 'task',
    component: TaskDataTableComponent,
    data: { title: extract('Tasks') },
  },
  {
    path: 'location',
    component: LocationDataTableComponent,
    data: { title: extract('Locations') },
  },
  {
    path: 'rank',
    component: RankDataTableComponent,
    data: { title: extract('Ranks') },
  },
  {
    path: 'equipment',
    component: EquipmentDataTableComponent,
    data: { title: extract('Equipments') },
  },
  {
    path: 'equipment-category',
    component: CategoryDataTableComponent,
    data: { title: extract('Equipment Categorys') },
  },
  {
    path: 'company',
    component: CompanyDataTableComponent,
    data: { title: extract('Company') },
  },
  {
    path: 'vessel-type',
    component: VesselTypeComponent,
    data: { title: extract('Vessel Type') },
  },
  {
    path: 'event-impact-type',
    component: EventImpactTypeComponent,
    data: { title: extract('Event Impact Type') },
  },
  {
    path: 'body-part-affected',
    component: BodyPartAffectedComponent,
    data: { title: extract('Body Part Affected') },
  },
  {
    path: 'injury-occured-by',
    component: InjuryOccuredByComponent,
    data: { title: extract('Injury Occured By') },
  },
  {
    path: 'background-condition',
    component: BackgroundConditionComponent,
    data: { title: extract('Background Condition') },
  },
  {
    path: 'analysis-similar-incident',
    component: AnalysisSimilarIncidentComponent,
    data: { title: extract('Analysis of Similar Incident') },
  },
  {
    path: 'injury-category',
    component: InjuryCategoryComponent,
    data: { title: extract('Injury Category') },
  },
  {
    path: 'nationality',
    component: NationalityComponent,
    data: { title: extract('Nationality') },
  },
  {
    path: 'third-party-type',
    component: ThirdPartyTypeComponent,
    data: { title: extract('ThirdPartyType') },
  },
  {
    path: 'lesson-learnt-type',
    component: LessonLearntTypeComponent,
    data: { title: extract('Lesson Learnt Type') },
  },
];

// Route Config for Vessels Routes within Admin Routing
const routes: Routes = [
  {
    path: '',
    component: CustomDataFieldManagementComponent,
    children,
    data: { title: extract('Custom Data Field Management') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, AgGridModule],
  providers: [],
})
export class CustomDataFieldRoutingModule {}
