import { AddNewDataService } from '@shared/service/add-new-data.service';
import { Router } from '@angular/router';
import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Logger } from '@core';

const log = new Logger('CustomFieldData');

@Component({
  selector: 'app-custom-data-field-management',
  templateUrl: './custom-data-field-management.component.html',
  styleUrls: ['./custom-data-field-management.component.scss'],
})
export class CustomDataFieldManagementComponent implements OnInit, AfterViewInit {
  public routerData: any;
  public activeTab: any;
  public taskactive: any = false;
  public rankactive: any = false;
  public locationactive: any = false;
  public equipmentactive: any = false;
  public categoryactive: any = false;
  // public active : any = 0;

  pageSubTitle: any;
  activePageIndex = 0;
  componentRef: any;

  navLinks: any[];
  activeLinkIndex = -1;

  constructor(
    private readonly changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private addNewDataService: AddNewDataService
  ) {
    this.navLinks = [
      {
        label: 'Task',
        link: './task',
        index: 0,
      },
      {
        label: 'Location',
        link: './location',
        index: 1,
      },
      {
        label: 'Rank',
        link: './rank',
        index: 2,
      },
      {
        label: 'Equipment',
        link: './equipment',
        index: 3,
      },
      {
        label: 'Equipment Category',
        link: './equipment-category',
        index: 4,
      },
      {
        label: 'Company',
        link: './company',
        index: 5,
      },
      {
        label: 'Vessel Type',
        link: './vessel-type',
        index: 6,
      },
      {
        label: 'Event Impact Type',
        link: './event-impact-type',
        index: 7,
      },
      {
        label: 'Body Part Affected',
        link: './body-part-affected',
        index: 8,
      },
      {
        label: 'Injury Occured By',
        link: './injury-occured-by',
        index: 9,
      },
      {
        label: 'Background Condition',
        link: './background-condition',
        index: 10,
      },
      {
        label: 'Analysis of Similar Incident',
        link: './analysis-similar-incident',
        index: 11,
      },
      {
        label: 'Injury Category',
        link: './injury-category',
        index: 12,
      },
      {
        label: 'Nationality',
        link: './nationality',
        index: 13,
      },
      {
        label: 'Third Party Type',
        link: './third-party-type',
        index: 14,
      },
      {
        label: 'Lesson Learnt Type',
        link: './lesson-learnt-type',
        index: 15,
      },
    ];
  }

  ngOnInit(): any {
    this.router.events.subscribe((res) => {
      this.routerData = res;
      this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find((tab) => tab.link === '.' + this.router.url));
    });

    // TODO Add Page sub title and Active Tab Selection by url
    log.debug('url', this.router.url);
    switch (this.router.url) {
      case '/admin/custom-data-fields/task':
        this.pageSubTitle = 'Task';
        this.activeTab = 'Task';
        return true;
      case '/admin/custom-data-fields/rank':
        this.pageSubTitle = 'Rank';
        this.activeTab = 'Rank';
        return true;
      case '/admin/custom-data-fields/location':
        this.pageSubTitle = 'Location';
        this.activeTab = 'Location';
        return true;
      case '/admin/custom-data-fields/equipment':
        this.pageSubTitle = 'Equipment';
        this.activeTab = 'Equipment';
        return true;
      case '/admin/custom-data-fields/equipment-category':
        this.pageSubTitle = 'Equipment Category';
        this.activeTab = 'Equipment Category';
        return true;
      case '/admin/custom-data-fields/company':
        this.pageSubTitle = 'Company';
        this.activeTab = 'Company';
        return true;
      case '/admin/custom-data-fields/vessel-type':
        this.pageSubTitle = 'Vessel Type';
        this.activeTab = 'Vessel Type';
        return true;
      case '/admin/custom-data-fields/event-impact-type':
        this.pageSubTitle = 'Event Impact Type';
        this.activeTab = 'Event Impact Type';
        return true;
      case '/admin/custom-data-fields/body-part-affected':
        this.pageSubTitle = 'Body Part Affected';
        this.activeTab = 'Body Part Affected';
        return true;
      case '/admin/custom-data-fields/injury-occured-by':
        this.pageSubTitle = 'Injury Occured By';
        this.activeTab = 'Injury Occured By';
        return true;
      case '/admin/custom-data-fields/background-condition':
        this.pageSubTitle = 'Background Condition';
        this.activeTab = 'Background Condition';
        return true;
      case '/admin/custom-data-fields/analysis-similar-incident':
        this.pageSubTitle = 'Analysis of Similar Incident';
        this.activeTab = 'Analysis of Similar Incident';
        return true;
      case '/admin/custom-data-fields/injury-category':
        this.pageSubTitle = 'Injury Category';
        this.activeTab = 'Injury Category';
        return true;
      case '/admin/custom-data-fields/nationality':
        this.pageSubTitle = 'Nationality';
        this.activeTab = 'Nationality';
        return true;
      case '/admin/custom-data-fields/third-party-type':
        this.pageSubTitle = 'Third Party Type';
        this.activeTab = 'Third Party Type';
        return true;
      case '/admin/custom-data-fields/lesson-learnt-type':
        this.pageSubTitle = 'Lesson Learnt Type';
        this.activeTab = 'Lesson Learnt Type';
        return true;

      default:
        return false;
    }
  }

  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges();
  }

  onActivate(componentReference: any) {
    this.componentRef = componentReference;

    // Setup Page SubTitle Subscription
    this.componentRef.pageSubTitle.subscribe((pageSubTitle: string) => {
      this.changeDetectorRef.detectChanges();
    });

    // Setup Page Index Subscription
    this.componentRef.activePageIndex.subscribe((activePageIndex: number) => {
      this.activePageIndex = activePageIndex;
      this.changeDetectorRef.detectChanges();
    });
  }

  addData(params: string) {
    // TODO Get Add Data Event call & forword to specific Event Handler
    switch (params) {
      case 'Task':
        this.addNewDataService.sendClickEventTask();
        return true;
      case 'Rank':
        this.addNewDataService.sendClickEventRank();
        return true;
      case 'Location':
        this.addNewDataService.sendClickEventLocation();
        return true;
      case 'Equipment':
        this.addNewDataService.sendClickEventEquipment();
        return true;
      case 'Equipment Category':
        this.addNewDataService.sendClickEventCategory();
        return true;
      case 'Company':
        this.addNewDataService.sendClickEventCompany();
        return true;
      case 'Vessel Type':
        this.addNewDataService.sendClickEventVesselType();
        return true;
      case 'Vessel Owner':
        this.addNewDataService.sendClickEventVesselOwner();
        return true;
      case 'Fleet':
        this.addNewDataService.sendClickEventFleet();
        return true;
      case 'Port':
        this.addNewDataService.sendClickEventPort();
        return true;
      case 'Event Impact Type':
        this.addNewDataService.sendClickEventImpactType();
        return true;
      case 'Body Part Affected':
        this.addNewDataService.sendClickEventBodyPartAffected();
        return true;
      case 'Injury Occured By':
        this.addNewDataService.sendClickEventInjuryOccuredBy();
        return true;
      case 'Background Condition':
        this.addNewDataService.sendClickEventBackgroundCondition();
        return true;
      case 'Analysis of Similar Incident':
        this.addNewDataService.sendClickEventAnalysisSimilarIncident();
        return true;
      case 'Injury Category':
        this.addNewDataService.sendClickEventInjuryCategory();
        return true;
      case 'Nationality':
        this.addNewDataService.sendClickEventNationality();
        return true;
      case 'Third Party Type':
        this.addNewDataService.sendClickEventThirdPartyType();
        return true;
      case 'Lesson Learnt Type':
        this.addNewDataService.sendClickEventLearntLessonType();
        return true;

      default:
        return false;
    }
  }

  onTabClick(params: any) {
    this.pageSubTitle = params.label;
    this.activeTab = params.label;
  }
}
