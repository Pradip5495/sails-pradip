import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomDataFieldManagementComponent } from './custom-data-field-management.component';

describe('CustomDataFieldManagementComponent', () => {
  let component: CustomDataFieldManagementComponent;
  let fixture: ComponentFixture<CustomDataFieldManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomDataFieldManagementComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomDataFieldManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
