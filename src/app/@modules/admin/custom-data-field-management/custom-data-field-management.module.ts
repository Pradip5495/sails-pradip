import { CustomDataFieldRoutingModule } from './custom-data-field-routing.module';
import { FormsModule } from '@angular/forms';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomDataFieldManagementComponent } from './custom-data-field-management.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { TaskDataTableComponent } from './component/task/task-data-table/task-data-table.component';
import { LocationDataTableComponent } from './component/location/location-data-table/location-data-table.component';
import { RankDataTableComponent } from './component/rank/rank-data-table/rank-data-table.component';
import { EquipmentDataTableComponent } from './component/equipment/equipment-data-table/equipment-data-table.component';
import { CategoryDataTableComponent } from './component/equipment-category/category-data-table/category-data-table.component';
import { CompanyDataTableComponent } from './component/company/company-data-table/company-data-table.component';
import { VesselTypeComponent } from './component/vessel-type/vessel-type.component';
import { EventImpactTypeComponent } from './component/event-impact-type/event-impact-type.component';
import { BodyPartAffectedComponent } from './component/body-part-affected/body-part-affected.component';
import { InjuryOccuredByComponent } from './component/injury-occured-by/injury-occured-by.component';
import { BackgroundConditionComponent } from './component/background-condition/background-condition.component';
import { AnalysisSimilarIncidentComponent } from './component/analysis-similar-incident/analysis-similar-incident.component';
import { InjuryCategoryComponent } from './component/injury-category/injury-category.component';
import { NationalityComponent } from './component/nationality/nationality.component';
import { LessonLearntTypeComponent } from './component/lesson-learnt-type/lesson-learnt-type.component';
import { CellValidationComponent } from '../../../@shared/component/cell-validation/cell-validation.component';
import { ThirdPartyTypeComponent } from './component/third-party-type/third-party-type.component';

@NgModule({
  declarations: [
    CustomDataFieldManagementComponent,
    TaskDataTableComponent,
    LocationDataTableComponent,
    RankDataTableComponent,
    EquipmentDataTableComponent,
    CategoryDataTableComponent,
    CompanyDataTableComponent,
    VesselTypeComponent,
    EventImpactTypeComponent,
    BodyPartAffectedComponent,
    InjuryOccuredByComponent,
    BackgroundConditionComponent,
    AnalysisSimilarIncidentComponent,
    InjuryCategoryComponent,
    NationalityComponent,
    LessonLearntTypeComponent,
    CellValidationComponent,
    ThirdPartyTypeComponent,
  ],
  imports: [
    CommonModule,
    CustomDataFieldRoutingModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    FormsModule,
  ],
})
export class CustomDataFieldManagementModule {}
