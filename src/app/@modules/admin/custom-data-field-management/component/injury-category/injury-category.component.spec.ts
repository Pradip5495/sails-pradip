import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InjuryCategoryComponent } from './injury-category.component';

describe('InjuryCategoryComponent', () => {
  let component: InjuryCategoryComponent;
  let fixture: ComponentFixture<InjuryCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InjuryCategoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InjuryCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
