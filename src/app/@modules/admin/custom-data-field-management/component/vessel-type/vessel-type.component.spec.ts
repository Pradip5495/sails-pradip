import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselTypeComponent } from './vessel-type.component';

describe('VesselTypeComponent', () => {
  let component: VesselTypeComponent;
  let fixture: ComponentFixture<VesselTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VesselTypeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VesselTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
