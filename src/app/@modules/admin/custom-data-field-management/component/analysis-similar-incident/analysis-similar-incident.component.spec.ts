import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisSimilarIncidentComponent } from './analysis-similar-incident.component';

describe('AnalysisSimilarIncidentComponent', () => {
  let component: AnalysisSimilarIncidentComponent;
  let fixture: ComponentFixture<AnalysisSimilarIncidentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AnalysisSimilarIncidentComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisSimilarIncidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
