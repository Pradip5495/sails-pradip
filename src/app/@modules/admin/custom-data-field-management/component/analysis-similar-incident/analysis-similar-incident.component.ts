import { CellValidationComponent } from '../../../../../@shared/component/cell-validation/cell-validation.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { GridApi, ColumnApi } from '@ag-grid-community/core';
import { Subscription } from 'rxjs';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { NewRowButtonsComponent } from '@shared/component/new-row-buttons/new-row-buttons.component';
import { StatusRendererComponent } from '@shared/component/status-renderer/status-renderer.component';
import { InjuryService } from '@shared/service/injury.service';
import { AgGridComponent } from '@shared/component/ag-grid/ag-grid.component';
import { AddNewDataService } from '@shared/service/add-new-data.service';

const log = new Logger('DataFieldTable');

@Component({
  selector: 'app-analysis-similar-incident',
  templateUrl: './analysis-similar-incident.component.html',
  styleUrls: ['./analysis-similar-incident.component.css'],
})
export class AnalysisSimilarIncidentComponent implements OnInit {
  // gridApi and columnApi
  public api: GridApi;
  public columnApi: ColumnApi;
  // public gridOptions: GridOptions

  public gridApi: any;
  public gridColumnApi: any;

  public sortingOrder: any;
  public editType: any;
  public rowData: any = [];
  public arrayData: any = [];
  public sideBar: any;
  public icons: any;
  public newItems: any;
  public res: any;
  public newData: any;
  public newCount: 1;
  public message: any;
  public rowlength: any;
  public cnt: any;

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  clickEventsubscription: Subscription;
  domLayout = 'autoHeight';

  error: string | undefined;

  @ViewChild('agGrid') agGrid: AgGridComponent;

  columnDefs = [
    {
      headerName: 'Questions',
      field: 'questions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      width: 750,
      cellEditor: 'cellValidation',
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'Actions',
      field: 'actions',
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellRendererSelector: (params: any) =>
        params.data.questions === 'Enter new Question'
          ? { component: 'newRowButtonsComponent' }
          : { component: 'customizedBtnCell' },
      cellRendererParams: {
        onClick: this.deleteQuestion.bind(this),
        onClickEdit: this.updateItems.bind(this),
        onSaveClick: this.createQuestion.bind(this),
        onCancelClick: this.removeQuestion.bind(this),
      },
    },
  ];

  defaultColDef = {
    enableValue: true,
    sortable: true,
    filter: true,
  };

  deleteData: any;
  title: string;

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    statusRendererComponents: StatusRendererComponent,
    newRowButtonsComponent: NewRowButtonsComponent,
    cellValidation: CellValidationComponent,
  };

  paginationPageSize: number;

  constructor(
    private readonly injuryService: InjuryService,
    private readonly notificationService: NotificationService,
    private notifyService: NotificationService,
    private addNewDataService: AddNewDataService
  ) {
    this.clickEventsubscription = this.addNewDataService.getClickEventAnalysisSimilarIncident().subscribe(() => {
      this.addNewData();
    });
  }

  onGridReady(params: any) {
    log.debug('reached');
    log.debug('griddata:', params);
    this.injuryService.getAnalysisOfSimilarIncidents().subscribe(
      (response) => {
        log.debug('Task Data', response);
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
        // this.rowData = this.rowDataTask
        log.debug('rowDatatask2:', this.rowData);
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }
  onCellValueChanged(params: any) {
    log.debug('param:', params);
    if (params.oldValue === 'Enter new Question') {
      this.injuryService.createAnalysisOfSimilarIncidents(params.data).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    } else {
      const rebody = {
        questions: params.data.questions,
      };
      this.injuryService.updateAnalysisOfSimilarIncidents(params.data.analysisOfSimilarIncidentId, rebody).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    }
  }

  deleteQuestion(params: any) {
    log.debug('deltdata:', params);
    this.injuryService.deleteAnalysisOfSimilarIncidents(params.rowData.analysisOfSimilarIncidentId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record No Deteted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  updateItems(params: any) {
    log.debug('paramsupdate:', params);
    params.api.setFocusedCell(params.rowIndex, 'actions');
    params.api.startEditingCell({
      rowIndex: params.rowIndex,
      colKey: 'actions',
    });
  }

  createQuestion(params: any) {
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
    this.fetchData();
  }

  removeQuestion(params: any) {
    log.debug('cancel:', params);
    this.agGrid.agGrid.rowData.splice(0, 0);
    this.agGrid.agGrid.api.setRowData(this.agGrid.agGrid.rowData);
    this.fetchData();
  }

  updatetoggle(params: any) {
    log.debug('toggle:', params);
    params.rowData.isActive = params.event.checked;
    this.injuryService
      .updateAnalysisOfSimilarIncidents(params.rowData.analysisOfSimilarIncidentId, params.rowData)
      .subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.rowData);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.rowData);
        }
      );
  }

  addNewData() {
    this.newItems = this.createNewRowData();
    this.agGrid.agGrid.api.insertItemsAtIndex(0, [this.newItems]);
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
  }
  fetchData() {
    this.injuryService.getAnalysisOfSimilarIncidents().subscribe((data) => {
      this.rowData = data;
    });
  }

  getRowNodeId(data: any) {
    return data.id;
  }

  createNewRowData() {
    this.newData = {
      questions: 'Enter new Question',
    };
    this.newCount++;
    return this.newData;
  }

  ngOnInit(): void {
    this.editType = 'fullRow';
    this.icons = {};

    this.paginationPageSize = 6;
  }
}
