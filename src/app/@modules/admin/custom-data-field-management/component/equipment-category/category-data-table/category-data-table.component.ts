import { NewRowButtonsComponent } from '@shared/component/new-row-buttons/new-row-buttons.component';
import { StatusRendererComponent } from '@shared/component/status-renderer/status-renderer.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { CustomDataFieldService } from '@shared/service/custom-data-field.service';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { AddNewDataService } from '@shared/service/add-new-data.service';
import { Subscription } from 'rxjs';
import { AgGridComponent } from '@shared/component/ag-grid/ag-grid.component';
import { CellValidationComponent } from '@shared/component/cell-validation/cell-validation.component';

const log = new Logger('EquipmentDataFieldTable');

@Component({
  selector: 'app-category-data-table',
  templateUrl: './category-data-table.component.html',
  styleUrls: ['./category-data-table.component.css'],
})
export class CategoryDataTableComponent implements OnInit {
  public gridApi: any;
  public gridColumnApi: any;
  public sortingOrder: any;
  public rowData: any = [];
  public updatedrowData: any = [];
  public arrayData: any = [];
  public sideBar: any;
  public icons: any;
  public newItems: any = [];
  public res: any;
  public newData: any = [];
  public newCount: 1;
  public message: any;
  public rowlength: any;
  public cnt: any;
  public catname: any;

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  deleteData: any;
  title: string;
  error: string | undefined;
  clickEventsubscription: Subscription;
  domLayout = 'autoHeight';

  @ViewChild('agGrid') agGrid: AgGridComponent;

  columnDefs = [
    {
      headerName: 'Equipment Category',
      field: 'categoryName',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      width: 750,
      cellEditor: 'cellValidation',
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },

    {
      headerName: 'Active',
      field: 'isActive',
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellRenderer: 'statusRendererComponents',
      cellRendererParams: {
        toggle: this.updatetoggle.bind(this),
      },
    },
    {
      headerName: 'Actions',
      field: 'actions',
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellRendererSelector: (params: any) =>
        params.data.categoryName === 'Enter new Category Name'
          ? { component: 'newRowButtonsComponent' }
          : { component: 'customizedBtnCell' },
      cellRendererParams: {
        onClick: this.deleteCategory.bind(this),
        onClickEdit: this.updateItems.bind(this),
        onSaveClick: this.createCategory.bind(this),
        onCancelClick: this.removeCategory.bind(this),
      },
    },
  ];

  defaultColDef = {
    enableValue: true,
    sortable: true,
    filter: true,
  };

  editType = 'fullRow';

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    statusRendererComponents: StatusRendererComponent,
    newRowButtonsComponent: NewRowButtonsComponent,
    cellValidation: CellValidationComponent,
  };

  paginationPageSize: number;

  constructor(
    private readonly customDataFieldService: CustomDataFieldService,
    private readonly notificationService: NotificationService,
    private notifyService: NotificationService,
    private addNewDataService: AddNewDataService
  ) {
    this.clickEventsubscription = this.addNewDataService.getClickEventCategory().subscribe(() => {
      this.addNewData();
    });
  }

  deleteCategory(params: any) {
    this.customDataFieldService.deleteCategory(params.rowData.equipmentCategoryId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.onGridReady(params);
        } else {
          const message = 'Record No Deteted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  updateItems(params: any) {
    params.api.setFocusedCell(params.rowIndex, 'actions');
    params.api.startEditingCell({
      rowIndex: params.rowIndex,
      colKey: 'actions',
    });
  }

  createCategory(params: any) {
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
    this.fetchData();
  }

  removeCategory(params: any) {
    log.debug('cancel:', params);
    this.agGrid.agGrid.rowData.splice(0, 0);
    this.agGrid.agGrid.api.setRowData(this.agGrid.agGrid.rowData);
    // this.fetchData();
  }

  updatetoggle(params: any) {
    log.debug('paramstoggle:', params);
    params.rowData.isActive = params.event.checked;
    log.debug('paramstoggleevent:', params.rowData);
    this.customDataFieldService.updateCategory(params.rowData.equipmentCategoryId, params.rowData).subscribe(
      (data) => {
        this.notificationService.showSuccess(data.message);
        log.debug('updated data:', params.rowData);
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
        log.debug('not updated', params.rowData);
      }
    );
  }

  addNewData() {
    this.newItems = this.createNewRowData();
    this.agGrid.agGrid.api.insertItemsAtIndex(0, [this.newItems]);
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
  }
  fetchData() {
    this.customDataFieldService.getCategorys().subscribe((data) => {
      this.rowData = data;
    });
  }

  getRowNodeId(data: any) {
    log.debug('rowid:', data);
    return data.id;
  }

  createNewRowData() {
    this.newData = {
      categoryName: 'Enter new Category Name',
      isActive: true,
    };
    log.debug('newData:', this.newData);
    this.newCount++;
    return this.newData;
  }

  onGridReady(params: any) {
    log.debug('reached');
    this.customDataFieldService.getCategorys().subscribe(
      (response) => {
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  onCellValueChanged(params: any) {
    if (params.oldValue === 'Enter new Category Name') {
      this.customDataFieldService.createCategory(params.data).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    } else {
      this.customDataFieldService.updateCategory(params.data.equipmentCategoryId, params.data).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    }
  }

  ngOnInit(): void {
    this.icons = {};

    this.paginationPageSize = 6;
  }
}
