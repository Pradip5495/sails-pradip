import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdPartyTypeComponent } from './third-party-type.component';

describe('ThirdPartyTypeComponent', () => {
  let component: ThirdPartyTypeComponent;
  let fixture: ComponentFixture<ThirdPartyTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThirdPartyTypeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPartyTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
