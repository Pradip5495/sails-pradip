import { Component, OnInit, ViewChild } from '@angular/core';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';

import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { StatusRendererComponent } from '@shared/component/status-renderer/status-renderer.component';

import { Subscription } from 'rxjs';
import { AddNewDataService } from '@shared/service/add-new-data.service';
import { CustomDataFieldService } from '@shared/service/custom-data-field.service';
import { NewRowButtonsComponent } from '@shared/component/new-row-buttons/new-row-buttons.component';
import { AgGridComponent } from '@shared/component/ag-grid/ag-grid.component';
import { CellValidationComponent } from '@shared/component/cell-validation/cell-validation.component';

const log = new Logger('DataFieldTable');

@Component({
  selector: 'app-location-data-table',
  templateUrl: './location-data-table.component.html',
  styleUrls: ['./location-data-table.component.css'],
})
export class LocationDataTableComponent implements OnInit {
  public gridApi: any;
  public gridColumnApi: any;
  public sortingOrder: any;
  public rowData: any = [];
  public arrayData: any = [];
  public sideBar: any;
  public icons: any;
  public newItems: any;
  public res: any;
  public newData: any;
  public newCount: 1;
  public message: any;
  public rowlength: any;
  public cnt: any;

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  deleteData: any;
  title: string;
  error: string | undefined;
  clickEventsubscription: Subscription;
  domLayout = 'autoHeight';

  @ViewChild('agGrid') agGrid: AgGridComponent;

  columnDefs = [
    {
      headerName: 'Location',
      field: 'location',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      width: 750,
      cellEditor: 'cellValidation',
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'Active',
      field: 'isActive',
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellRenderer: 'statusRendererComponents',
      cellRendererParams: {
        toggle: this.updatetoggle.bind(this),
      },
    },
    {
      headerName: 'Actions',
      field: 'actions',
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellRendererSelector: (params: any) =>
        params.data.location === 'Enter new Location Name'
          ? { component: 'newRowButtonsComponent' }
          : { component: 'customizedBtnCell' },
      cellRendererParams: {
        onClick: this.deleteLocation.bind(this),
        onClickEdit: this.updateItems.bind(this),
        onSaveClick: this.createLocation.bind(this),
        onCancelClick: this.removeLocation.bind(this),
      },
    },
  ];

  defaultColDef = {
    enableValue: true,
    sortable: true,
    filter: true,
  };

  editType = 'fullRow';

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    statusRendererComponents: StatusRendererComponent,
    newRowButtonsComponent: NewRowButtonsComponent,
    cellValidation: CellValidationComponent,
  };

  paginationPageSize: number;

  constructor(
    private readonly customDataFieldService: CustomDataFieldService,
    private readonly notificationService: NotificationService,
    private notifyService: NotificationService,
    private addNewDataService: AddNewDataService
  ) {
    this.clickEventsubscription = this.addNewDataService.getClickEventLocation().subscribe(() => {
      this.addNewDataLocation();
    });
  }

  deleteLocation(params: any) {
    this.customDataFieldService.deleteLocation(params.rowData.locationId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record No Deteted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  updateItems(params: any) {
    params.api.setFocusedCell(params.rowIndex, 'actions');
    params.api.startEditingCell({
      rowIndex: params.rowIndex,
      colKey: 'actions',
    });
  }

  createLocation(params: any) {
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
    this.fetchData();
  }

  removeLocation(params: any) {
    log.debug('cancel:', params);
    this.agGrid.agGrid.rowData.splice(0, 0);
    this.agGrid.agGrid.api.setRowData(this.agGrid.agGrid.rowData);
    // this.fetchData();
  }

  updatetoggle(params: any) {
    params.rowData.isActive = params.event.checked;
    this.customDataFieldService.updateLocation(params.rowData.locationId, params.rowData).subscribe(
      (data) => {
        this.notificationService.showSuccess(data.message);
        this.fetchData();
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
      }
    );
  }

  addNewDataLocation() {
    this.newItems = this.createNewRowData();
    this.agGrid.agGrid.api.insertItemsAtIndex(0, [this.newItems]);
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
  }

  fetchData() {
    this.customDataFieldService.getLocations().subscribe((data) => {
      this.rowData = data;
    });
  }

  getRowNodeId(data: any) {
    log.debug('rowid:', data);
    return data.id;
  }

  createNewRowData() {
    this.newData = {
      location: 'Enter new Location Name',
      isActive: true,
    };
    log.debug('newData:', this.newData);
    this.newCount++;
    return this.newData;
  }

  onGridReady(params: any) {
    this.customDataFieldService.getLocations().subscribe(
      (response) => {
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  onCellValueChanged(params: any) {
    if (params.oldValue === 'Enter new Location Name') {
      this.customDataFieldService.createLocation(params.data).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    } else {
      this.customDataFieldService.updateLocation(params.data.locationId, params.data).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    }
  }

  ngOnInit(): void {
    this.icons = {};

    this.paginationPageSize = 6;
  }
}
