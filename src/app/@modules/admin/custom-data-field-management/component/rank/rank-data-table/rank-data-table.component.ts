import { Component, OnInit, ViewChild } from '@angular/core';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { CustomDataFieldService } from '@shared/service/custom-data-field.service';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { StatusRendererComponent } from '@shared/component/status-renderer/status-renderer.component';
import { Subscription } from 'rxjs';
import { AddNewDataService } from '@shared/service';
import { NewRowButtonsComponent } from '@shared/component/new-row-buttons/new-row-buttons.component';
import { AgGridComponent } from '@shared/component/ag-grid/ag-grid.component';
import { GridApi, ColumnApi } from '@ag-grid-community/core';
import { CellValidationComponent } from '@shared/component/cell-validation/cell-validation.component';

const log = new Logger('RankDataFieldTable');

@Component({
  selector: 'app-rank-data-table',
  templateUrl: './rank-data-table.component.html',
  styleUrls: ['./rank-data-table.component.css'],
})
export class RankDataTableComponent implements OnInit {
  public api: GridApi;
  public columnApi: ColumnApi;

  public gridApi: any;
  public gridColumnApi: any;
  public sortingOrder: any;
  public rowData: any = [];
  public arrayData: any = [];
  public sideBar: any;
  public icons: any;
  public newItems: any;
  public res: any;
  public newData: any;
  public newCount: 1;
  public message: any;
  public rowlength: any;
  public cnt: any;

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  deleteData: any;
  clickEventsubscription: Subscription;

  domLayout = 'autoHeight';

  title: string;
  error: string | undefined;

  @ViewChild('agGrid') agGrid: AgGridComponent;

  columnDefs = [
    {
      headerName: 'Rank',
      field: 'rank',
      width: 750,
      cellEditor: 'cellValidation',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'Active',
      field: 'isActive',
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellRenderer: 'statusRendererComponents',
      cellRendererParams: {
        toggle: this.updatetoggle.bind(this),
      },
    },
    {
      headerName: 'Actions',
      field: 'actions',
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellRendererSelector: (params: any) =>
        params.data.rank === 'Enter new Rank Name'
          ? { component: 'newRowButtonsComponent' }
          : { component: 'customizedBtnCell' },
      cellRendererParams: {
        onClick: this.deleteRank.bind(this),
        onClickEdit: this.updateItems.bind(this),
        onSaveClick: this.createRank.bind(this),
        onCancelClick: this.removeRank.bind(this),
      },
    },
  ];

  defaultColDef = {
    enableValue: true,
    sortable: true,
    filter: true,
  };

  paginationPageSize: number;

  editType = 'fullRow';

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    statusRendererComponents: StatusRendererComponent,
    newRowButtonsComponent: NewRowButtonsComponent,
    cellValidation: CellValidationComponent,
  };

  constructor(
    private readonly customDataFieldService: CustomDataFieldService,
    private readonly notificationService: NotificationService,
    private notifyService: NotificationService,
    private addNewDataService: AddNewDataService
  ) {
    this.clickEventsubscription = this.addNewDataService.getClickEventRank().subscribe(() => {
      this.addNewDataRank();
    });
  }

  onGridReady(params: any) {
    this.customDataFieldService.getRanks().subscribe(
      (response) => {
        log.debug('Location Data', response);
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  onCellValueChanged(params: any) {
    if (params.oldValue === 'Enter new Rank Name') {
      this.customDataFieldService.createRank(params.data).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    } else {
      this.customDataFieldService.updateRank(params.data.rankId, params.data).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    }
  }

  deleteRank(params: any) {
    this.customDataFieldService.deleteRank(params.rowData.rankId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record No Deteted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  updateItems(params: any) {
    params.api.setFocusedCell(params.rowIndex, 'actions');
    params.api.startEditingCell({
      rowIndex: params.rowIndex,
      colKey: 'actions',
    });
  }

  createRank(params: any) {
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
    this.fetchData();
  }

  removeRank(params: any) {
    log.debug('cancel:', params);
    this.agGrid.agGrid.rowData.splice(0, 0);
    this.agGrid.agGrid.api.setRowData(this.agGrid.agGrid.rowData);
    // this.fetchData();
  }

  updatetoggle(params: any) {
    params.rowData.isActive = params.event.checked;
    this.customDataFieldService.updateRank(params.rowData.rankId, params.rowData).subscribe(
      (data) => {
        this.notificationService.showSuccess(data.message);
        this.fetchData();
        log.debug('updated data:', params.rowData);
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
        log.debug('not updated', params.rowData);
      }
    );
  }

  addNewDataRank() {
    this.newItems = this.createNewRowData();
    this.agGrid.agGrid.api.insertItemsAtIndex(0, [this.newItems]);
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
  }

  fetchData() {
    this.customDataFieldService.getRanks().subscribe((data) => {
      this.rowData = data;
    });
  }

  getRowNodeId(data: any) {
    log.debug('rowid:', data);
    return data.id;
  }

  createNewRowData() {
    this.newData = {
      rank: 'Enter new Rank Name',
      isActive: true,
    };
    log.debug('newData:', this.newData);
    this.newCount++;
    return this.newData;
  }

  ngOnInit(): void {
    this.icons = {};

    this.paginationPageSize = 6;
  }
}
