import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankDataTableComponent } from './rank-data-table.component';

describe('RankDataTableComponent', () => {
  let component: RankDataTableComponent;
  let fixture: ComponentFixture<RankDataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RankDataTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RankDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
