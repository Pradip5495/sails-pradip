import { Component, OnInit, ViewChild } from '@angular/core';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { GridApi, ColumnApi } from '@ag-grid-community/core';

import { Subscription } from 'rxjs';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { NewRowButtonsComponent } from '@shared/component/new-row-buttons/new-row-buttons.component';
import { StatusRendererComponent } from '@shared/component/status-renderer/status-renderer.component';
import { AgGridComponent } from '@shared/component/ag-grid/ag-grid.component';
import { AddNewDataService } from '@shared/service/add-new-data.service';
import { LessonTypeService } from '@shared/service/lesson-type.service';
import { CellValidationComponent } from '@shared/component/cell-validation/cell-validation.component';

const log = new Logger('DataFieldTable');

@Component({
  selector: 'app-lesson-learnt-type',
  templateUrl: './lesson-learnt-type.component.html',
  styleUrls: ['./lesson-learnt-type.component.css'],
})
export class LessonLearntTypeComponent implements OnInit {
  // gridApi and columnApi
  public api: GridApi;
  public columnApi: ColumnApi;
  // public gridOptions: GridOptions

  public gridApi: any;
  public gridColumnApi: any;

  public sortingOrder: any;
  public editType: any;
  public rowData: any = [];
  public arrayData: any = [];
  public sideBar: any;
  public icons: any;
  public newItems: any;
  public res: any;
  public newData: any;
  public newCount: 1;
  public message: any;
  public rowlength: any;
  public cnt: any;

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  clickEventsubscription: Subscription;
  domLayout = 'autoHeight';

  error: string | undefined;

  @ViewChild('agGrid') agGrid: AgGridComponent;

  columnDefs = [
    {
      headerName: 'Lesson Type',
      field: 'lessonType',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      width: 750,
      cellEditor: 'cellValidation',
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'Actions',
      field: 'actions',
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellRendererSelector: (params: any) =>
        params.data.lessonType === 'Enter new Lesson Type'
          ? { component: 'newRowButtonsComponent' }
          : { component: 'customizedBtnCell' },
      cellRendererParams: {
        onClick: this.deleteLessonsLearntType.bind(this),
        onClickEdit: this.updateItems.bind(this),
        onSaveClick: this.createLessonsLearntType.bind(this),
        onCancelClick: this.removeLessonsLearntType.bind(this),
      },
    },
  ];

  defaultColDef = {
    enableValue: true,
    sortable: true,
    filter: true,
  };

  deleteData: any;
  title: string;

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    statusRendererComponents: StatusRendererComponent,
    newRowButtonsComponent: NewRowButtonsComponent,
    cellValidation: CellValidationComponent,
  };

  paginationPageSize: number;

  constructor(
    private readonly lessonTypeService: LessonTypeService,
    private readonly notificationService: NotificationService,
    private notifyService: NotificationService,
    private addNewDataService: AddNewDataService
  ) {
    this.clickEventsubscription = this.addNewDataService.getClickEventLessonLearntType().subscribe(() => {
      this.addNewData();
    });
  }

  onGridReady(params: any) {
    log.debug('reached');
    log.debug('griddata:', params);
    this.lessonTypeService.getLessonlearntType().subscribe(
      (response) => {
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }
  onCellValueChanged(params: any) {
    log.debug('param:', params);
    if (params.oldValue === 'Enter new Lesson Type') {
      this.lessonTypeService.createLessonlearntType(params.data).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    } else {
      this.lessonTypeService.updateLessonlearntType(params.data.lessonsLearntTypeId, params.data).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    }
  }

  deleteLessonsLearntType(params: any) {
    log.debug('deltdata:', params);
    this.lessonTypeService.deleteLessonlearntType(params.rowData.lessonsLearntTypeId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record No Deteted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  updateItems(params: any) {
    log.debug('paramsupdate:', params);
    params.api.setFocusedCell(params.rowIndex, 'actions');
    params.api.startEditingCell({
      rowIndex: params.rowIndex,
      colKey: 'actions',
    });
  }

  createLessonsLearntType(params: any) {
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
    this.fetchData();
  }

  removeLessonsLearntType(params: any) {
    log.debug('cancel:', params);
    this.agGrid.agGrid.rowData.splice(0, 0);
    this.agGrid.agGrid.api.setRowData(this.agGrid.agGrid.rowData);
    this.fetchData();
  }

  updatetoggle(params: any) {
    log.debug('toggle:', params);
    params.rowData.isActive = params.event.checked;
    this.lessonTypeService.updateLessonlearntType(params.rowData.lessonsLearntTypeId, params.rowData).subscribe(
      (data) => {
        this.notificationService.showSuccess(data.message);
        log.debug('updated data:', params.rowData);
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
        log.debug('not updated', params.rowData);
      }
    );
  }

  addNewData() {
    this.newItems = this.createNewRowData();
    this.agGrid.agGrid.api.insertItemsAtIndex(0, [this.newItems]);
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
  }
  fetchData() {
    this.lessonTypeService.getLessonlearntType().subscribe((data) => {
      this.rowData = data;
    });
  }

  getRowNodeId(data: any) {
    return data.id;
  }

  createNewRowData() {
    this.newData = {
      lessonType: 'Enter new Lesson Type',
    };
    this.newCount++;
    return this.newData;
  }

  ngOnInit(): void {
    this.editType = 'fullRow';
    this.icons = {};

    this.paginationPageSize = 6;
  }
}
