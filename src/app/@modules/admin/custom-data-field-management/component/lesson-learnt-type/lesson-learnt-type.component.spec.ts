import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonLearntTypeComponent } from './lesson-learnt-type.component';

describe('LessonLearntTypeComponent', () => {
  let component: LessonLearntTypeComponent;
  let fixture: ComponentFixture<LessonLearntTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LessonLearntTypeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonLearntTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
