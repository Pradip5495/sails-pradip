import { Component, OnInit, ViewChild } from '@angular/core';

import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { StatusRendererComponent } from '@shared/component/status-renderer/status-renderer.component';
import { Subscription } from 'rxjs';
import { AddNewDataService } from '@shared/service/add-new-data.service';
import { CustomDataFieldService } from '@shared/service/custom-data-field.service';
import { NewRowButtonsComponent } from '@shared/component/new-row-buttons/new-row-buttons.component';
import { AgGridComponent } from '@shared/component/ag-grid/ag-grid.component';
import { CellValidationComponent } from '@shared/component/cell-validation/cell-validation.component';

const log = new Logger('CompanyDataFields');
@Component({
  selector: 'app-company-data-table',
  templateUrl: './company-data-table.component.html',
  styleUrls: ['./company-data-table.component.css'],
})
export class CompanyDataTableComponent implements OnInit {
  public gridApi: any;
  public gridColumnApi: any;
  public sortingOrder: any;
  public rowData: any = [];
  public arrayData: any = [];
  public sideBar: any;
  public icons: any;
  public newItems: any;
  public res: any;
  public newData: any;
  public newCount: 1;
  public message: any;
  public rowlength: any;
  public cnt: any;

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  deleteData: any;
  title: string;
  error: string | undefined;
  clickEventsubscription: Subscription;
  domLayout = 'autoHeight';

  @ViewChild('agGrid') agGrid: AgGridComponent;

  columnDefs = [
    {
      headerName: 'Company',
      field: 'companyName',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      width: 750,
      cellEditor: 'cellValidation',
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'Actions',
      field: 'actions',
      flex: 1,
      resizable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellRendererSelector: (params: any) =>
        params.data.companyName === 'Enter New Company Name'
          ? { component: 'newRowButtonsComponent' }
          : { component: 'customizedBtnCell' },
      cellRendererParams: {
        onClick: this.deleteCompany.bind(this),
        onClickEdit: this.updateItems.bind(this),
        onSaveClick: this.createComapany.bind(this),
        onCancelClick: this.removeComapany.bind(this),
      },
    },
  ];

  defaultColDef = {
    enableValue: true,
    sortable: true,
    filter: true,
  };

  editType = 'fullRow';

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    statusRendererComponents: StatusRendererComponent,
    newRowButtonsComponent: NewRowButtonsComponent,
    cellValidation: CellValidationComponent,
  };

  paginationPageSize: number;

  constructor(
    private readonly customDataFieldService: CustomDataFieldService,
    private readonly notificationService: NotificationService,
    private notifyService: NotificationService,
    private addNewDataService: AddNewDataService
  ) {
    this.clickEventsubscription = this.addNewDataService.getClickEventCompany().subscribe(() => {
      this.addNewDataCompany();
    });
  }

  deleteCompany(params: any) {
    this.customDataFieldService.deleteCompany(params.rowData.companyId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record No Deteted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  updateItems(params: any) {
    params.api.setFocusedCell(params.rowIndex, 'actions');
    params.api.startEditingCell({
      rowIndex: params.rowIndex,
      colKey: 'actions',
    });
  }

  createComapany(params: any) {
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
    this.fetchData();
  }

  removeComapany(params: any) {
    log.debug('cancel:', params);
    this.agGrid.agGrid.rowData.splice(0, 0);
    this.agGrid.agGrid.api.setRowData(this.agGrid.agGrid.rowData);
    // this.fetchData();
  }

  updatetoggle(params: any) {
    params.rowData.isActive = params.event.checked;
    params.rowData.companyName = params.rowData.companyName;
    this.customDataFieldService.updateCompany(params.rowData.companyId, params.rowData).subscribe(
      (data) => {
        this.notificationService.showSuccess(data.message);
        this.fetchData();
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
      }
    );
  }

  addNewDataCompany() {
    this.newItems = this.createNewRowData();
    this.agGrid.agGrid.api.insertItemsAtIndex(0, [this.newItems]);
    this.agGrid.agGrid.api.setFocusedCell(0, 'actions');
    this.agGrid.agGrid.api.startEditingCell({
      rowIndex: 0,
      colKey: 'actions',
    });
  }

  fetchData() {
    this.customDataFieldService.getCompanys().subscribe((data) => {
      this.rowData = data;
    });
  }

  getRowNodeId(data: any) {
    log.debug('rowid:', data);
    return data.id;
  }

  createNewRowData() {
    this.newData = {
      companyName: 'Enter New Company Name',
      isActive: true,
    };
    log.debug('newData:', this.newData);
    this.newCount++;
    return this.newData;
  }

  onGridReady(params: any) {
    this.customDataFieldService.getCompanys().subscribe(
      (response) => {
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  onCellValueChanged(params: any) {
    if (params.oldValue === 'Enter New Company Name') {
      this.customDataFieldService.createCompany(params.data).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    } else {
      this.customDataFieldService.updateCompany(params.data.companyId, params.data).subscribe(
        (data) => {
          this.notificationService.showSuccess(data.message);
          log.debug('updated data:', params.data);
          this.onGridReady(params);
        },
        (error) => {
          log.error(error);
          this.notificationService.showError(error);
          log.debug('not updated', params.data);
        }
      );
    }
  }

  ngOnInit(): void {
    this.icons = {};

    this.paginationPageSize = 6;
  }
}
