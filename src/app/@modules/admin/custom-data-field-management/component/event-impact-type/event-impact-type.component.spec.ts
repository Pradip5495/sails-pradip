import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventImpactTypeComponent } from './event-impact-type.component';

describe('EventImpactTypeComponent', () => {
  let component: EventImpactTypeComponent;
  let fixture: ComponentFixture<EventImpactTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventImpactTypeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventImpactTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
