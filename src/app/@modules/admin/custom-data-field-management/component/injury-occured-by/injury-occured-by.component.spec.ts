import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InjuryOccuredByComponent } from './injury-occured-by.component';

describe('InjuryOccuredByComponent', () => {
  let component: InjuryOccuredByComponent;
  let fixture: ComponentFixture<InjuryOccuredByComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InjuryOccuredByComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InjuryOccuredByComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
