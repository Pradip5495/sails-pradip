import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyPartAffectedComponent } from './body-part-affected.component';

describe('BodyPartAffectedComponent', () => {
  let component: BodyPartAffectedComponent;
  let fixture: ComponentFixture<BodyPartAffectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BodyPartAffectedComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyPartAffectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
