import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackgroundConditionComponent } from './background-condition.component';

describe('BackgroundConditionComponent', () => {
  let component: BackgroundConditionComponent;
  let fixture: ComponentFixture<BackgroundConditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BackgroundConditionComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackgroundConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
