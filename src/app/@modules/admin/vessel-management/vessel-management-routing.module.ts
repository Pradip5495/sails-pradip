import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { extract } from '@app/i18n';
import { VesselManagementComponent } from '@modules/admin/vessel-management/vessel-management.component';
import {
  AddVesselComponent,
  ListVesselComponent,
  ViewVesselComponent,
} from '@modules/admin/vessel-management/component';

// Vessels Child Routes
const children: Routes = [
  {
    path: '',
    component: ListVesselComponent,
    data: { title: extract('All Vessels') },
  },
  {
    path: 'create',
    component: AddVesselComponent,
    data: { title: extract('New Vessel') },
  },
  {
    path: ':id',
    component: ViewVesselComponent,
    data: { title: extract('Vessel Details') },
  },
];

// Route Config for Vessels Routes within Admin Routing
const routes: Routes = [
  {
    path: '',
    component: VesselManagementComponent,
    children,
    data: { title: extract('Vessels Management') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class VesselManagementRoutingModule {}
