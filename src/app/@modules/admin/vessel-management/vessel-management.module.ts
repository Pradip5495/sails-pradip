import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VesselManagementComponent } from './vessel-management.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { VesselManagementRoutingModule } from './vessel-management-routing.module';
import {
  AddVesselComponent,
  ListVesselComponent,
  VesselDatalistComponent,
  ViewVesselComponent,
} from '@modules/admin/vessel-management/component';
import { FormlyModule } from '@formly';

@NgModule({
  declarations: [
    VesselManagementComponent,
    ListVesselComponent,
    AddVesselComponent,
    ViewVesselComponent,
    VesselDatalistComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    VesselManagementRoutingModule,
    FormlyModule,
  ],
})
export class VesselManagementModule {}
