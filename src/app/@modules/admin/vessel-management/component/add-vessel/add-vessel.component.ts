import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { VesselService } from '@shared/service';
import { NotificationService } from '@shared/common-service';
import { FormGroup, FormControl } from '@angular/forms';
import { CreateVesselModel } from '@shared/models';
import { FormlyFieldConfig, FormlyFormOptions } from '@formly';
import { Logger } from '@core';
import { ICustomFormlyConfig } from '@types';
import { Router } from '@angular/router';

const log = new Logger('AddVesselComponent');

@Component({
  selector: 'app-add-vessel',
  templateUrl: './add-vessel.component.html',
  styleUrls: ['./add-vessel.component.scss'],
})
export class AddVesselComponent implements OnInit {
  @ViewChild('newdialog', { static: true }) _addDialogContent: any;
  @ViewChild('portdialog', { static: true }) _addPortDialogContent: any;
  @ViewChild('vesseltypedialog', { static: true }) _addVesselTypeDialogContent: any;

  public name: any;
  public email: any;
  public length: any;
  public breadth: any;
  public yearBuilt: any;
  public dom: any;
  public hullType: any;
  public owner: any = [];
  public capacity: any;
  public grossCapacity: any;
  public weightSummer: any;
  public draftSummer: any;
  public port: any;
  public deliveryDate: any;
  public completionDate: any;
  public registryPort: any;
  public ownerDetails: any;
  public secondaryEmail: any;
  public vesselType: any;
  public phone: any;
  public address: any;
  public countryCode: any;
  public ownerEmail: any;
  public company: any;
  public country: any;
  public nameOfContactPerson: any;
  public phonenumber: any;
  public vesselOwnerData: any;
  public vesselOwnerId: any;
  public vesselportId: any;
  public vesselTypeId: any;

  displayDialog = false;
  displayPortDialog = false;
  displayVesselTypeDialog = false;

  hasError: boolean;
  fileContent: any;
  fileData: any = [];
  ownerData: any = [];
  vesselData: any = [];
  portData: any = [];
  vesseltypeData: any = [];

  // Notify parent for Action and Title Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  overviewModel = {};
  vesselform = new FormGroup({});
  // ownerFormGroup: FormGroup;
  model = new CreateVesselModel();
  options: FormlyFormOptions = {
    formState: {
      limitDate: true,
      today: new Date(),
    },
  };

  fields: FormlyFieldConfig[] = [
    {
      template: ' <div><strong>Please attach xml here</strong></div>',
    },
    {
      fieldGroupClassName: '',
      fieldGroup: [
        {
          key: 'files',
          className: 'flex-1',
          type: 'sailfile',
          templateOptions: {
            preview: true,
            id: 'overview',
            accept: 'application/xml',
            onFileChange: this.onFilesUpload.bind(this),
          },
        },
      ],
    },

    {
      template: ' <div><strong>Basic Details:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'vessel',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Name',
            placeholder: 'Enter Vessel Name',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'vesselTypeId',
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Type of Vessel',
            options: this.vesselService.getVesselTypes(),
            valueProp: 'vesselTypeId',
            labelProp: 'vesselType',
            required: true,
            addonRight: {
              title: 'ADD',
              icon: 'add',
              onClick: (to: any, component: any, event: any) => {
                event.stopPropagation();
                this.openVesselTypeDialog(this.vesseltypeform);
              },
            },
          },
        },
        {
          key: 'portId',
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Port',
            options: this.vesselService.getVesselPorts(),
            valueProp: 'portId',
            labelProp: 'name',
            required: true,
            addonRight: {
              title: 'ADD',
              icon: 'add',
              onClick: (to: any, component: any, event: any) => {
                event.stopPropagation();
                this.openPortDialog(this.portform);
              },
            },
          },
        },
        {
          key: 'deliveryDate',
          type: 'datepicker',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Delivery Date',
            placeholder: 'Enter Delivery Date',
            required: false,
          },
          expressionProperties: {
            'templateOptions.min': `formState.limitDate ? this.today : null`,
          },
        },
      ],
    },

    // {
    //   template: '<div><strong>Contact:</strong></div><hr/>',
    // },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'vesselOwnerId',
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Owner',
            options: this.vesselService.getVesselOwners(),
            valueProp: 'vesselOwnerId',
            placeholder: 'Select Owner',
            labelProp: 'name',
            required: true,
            addonRight: {
              title: 'ADD',
              icon: 'add',
              onClick: (to: any, component: any, event: any) => {
                event.stopPropagation();
                this.openDialog(this.ownerform);
              },
            },
          },
        },
        {
          key: 'masterEmail',
          type: 'textarea',
          className: 'flex-2',
          templateOptions: {
            label: 'Email',
            placeholder: 'Enter Primary Email Address',
            required: false,
          },
        },
      ],
    },
    // {
    //   template: '<div><strong>Dimension:</strong></div><hr/>',
    // },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'imoNumber',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Enter Imo Number',
            placeholder: 'Enter Imo Number',
            required: false,
          },
        },
        {
          key: 'hullType',
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Hull Type',
            placeholder: 'Select Hull Type',
            options: [{ hullType: 'Single hull' }, { hullType: 'Double hull' }],
            valueProp: 'hullType',
            labelProp: 'hullType',
            required: false,
          },
        },
        {
          key: 'yearBuilt',
          type: 'datepicker',
          className: 'flex-2',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Enter Year Built',
            placeholder: 'Enter Year Built',
            required: false,
          },
          expressionProperties: {
            'templateOptions.min': `formState.limitDate ? this.today : null`,
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      config: {
        model: this.model,
        fields: this.fields,
        options: this.options,
        form: this.vesselform,
      },
      onSave: this.onSubmit.bind(this),
      onDiscard: () => this.vesselform.reset(),
    },
  ];

  // #region Vessel Owner formly form
  ownerform = new FormGroup({});

  ownerModel = {};

  ownerFields: FormlyFieldConfig[] = [
    {
      template: ' <div><strong>Basic Details:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'name',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Username',
            placeholder: 'Enter User Name',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'company',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Company',
            placeholder: 'Enter Company Name',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      template: '<div><strong>Contact:</strong></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'email',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Email',
            placeholder: 'email',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'nameOfContactPerson',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Contact Person',
            placeholder: 'Contact Person',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'phone',
          type: 'tel-input',
          className: 'flex-1',
          templateOptions: {
            label: 'Phone',
            placeholder: 'Phone',
            required: true,
            onError: this.onError.bind(this),
            onCountryChange: this.onCountryChange.bind(this),
            getNumber: this.getNumber.bind(this),
            telInputObject: this.telInputObject.bind(this),
          },
          modelOptions: {
            updateOn: 'blur',
          },
          validators: {
            phone: {
              expression: (c: FormControl) => {
                // console.log({ phoneNUmber: c.value, errors: c.errors, c });
                return !this.hasError;
              },
              message: (error: any, field: FormlyFieldConfig) => {
                //  console.log({ error, field });
                return `${field.formControl.value} is not a valid Phone number`;
              },
            },
          },
        },
      ],
    },

    {
      template: '<div><strong>Dimension:</strong></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'address',
          type: 'textarea',
          className: 'flex-1',
          templateOptions: {
            label: 'Address',
            placeholder: 'Enter address',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
  ];

  formlyConfigAdd: ICustomFormlyConfig[] = [
    {
      config: {
        form: this.ownerform,
        model: this.ownerModel,
        options: {},
        fields: this.ownerFields,
      },
      legacyForm: true,
      onSave: () => this.onOwnerSubmit(),
      onDiscard: () => this.overviewOwnerReset(),
    },
  ];

  // #endregion Vessel Owner formly form

  // #region Vessel Port formly form
  portform = new FormGroup({});

  portModel = {};

  portFields: FormlyFieldConfig[] = [
    {
      template: ' <div><strong>Basic Details:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'name',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Username',
            placeholder: 'Enter User Name',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'country',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Country',
            placeholder: 'Enter Country',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'latitude',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Latitude',
            placeholder: 'latitude',
            required: true,
            throwError: 'Field is required',
          },
        },

        {
          key: 'longitude',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Longitude',
            placeholder: 'Enter Longitude',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
  ];

  formlyConfigPort: ICustomFormlyConfig[] = [
    {
      config: {
        form: this.portform,
        model: this.portModel,
        options: {},
        fields: this.portFields,
      },
      legacyForm: true,
      onSave: () => this.onPortSubmit(),
      onDiscard: () => this.overviewPortReset(),
    },
  ];
  // #endregion Vessel Port formly form

  // #region Vessel Type formly form
  vesseltypeform = new FormGroup({});

  vesseltypeModel = {};

  vesseltypeFields: FormlyFieldConfig[] = [
    {
      template: ' <div><strong>Basic Details:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'vesselType',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Username',
            placeholder: 'Enter User Name',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
  ];

  formlyConfigVesselType: ICustomFormlyConfig[] = [
    {
      config: {
        form: this.vesseltypeform,
        model: this.vesseltypeModel,
        options: {},
        fields: this.vesseltypeFields,
      },
      legacyForm: true,
      onSave: () => this.onVesselTypeSubmit(),
      onDiscard: () => this.overviewVesselTypeReset(),
    },
  ];

  // #endregion Vessel Type formly form

  constructor(
    private notifyService: NotificationService,
    private readonly vesselService: VesselService,
    private router: Router,
    private readonly notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.pageSubTitle.emit('NEW VESSEL');
    this.activePageIndex.emit(1);
  }

  onAddForm() {
    this.router.navigateByUrl('/admin/vessel').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  onFilesUpload(fileList: FileList) {
    this.fileData.push(fileList);
    const file = fileList[0];
    const fileReader: FileReader = new FileReader();
    const self = this;
    fileReader.onloadend = (x) => {
      this.fileContent = fileReader.result;
      const parser = new DOMParser();
      this.dom = parser.parseFromString(self.fileContent, 'application/xml');

      for (const i of this.dom.getElementsByTagName('Response')) {
        const responseData = i.getAttribute('ctrl');

        if (responseData === '47563F96-482E-4CE8-83F5-97499B5CE7BA') {
          this.email = i.getElementsByTagName('ResponseString')[0].textContent;
        } else if (responseData === 'BAA19B27-092D-4442-B5A8-7EBA8197B1A5') {
          this.secondaryEmail = i.getElementsByTagName('ResponseString')[0].textContent;
        } else if (responseData === '8BA60DAE-CB2B-4979-A659-4CE5262C6C57') {
          this.length = i.getElementsByTagName('ResponseDecimal')[0].textContent;
        } else if (responseData === 'B45AAA7A-7D1C-46F3-BC91-774A3A529E47') {
          this.breadth = i.getElementsByTagName('ResponseDecimal')[0].textContent;
        } else if (responseData === 'D8FCBA5E-F08D-44E5-9CE4-1AA167D90C06') {
          this.hullType = i.getElementsByTagName('ResponseString')[0].textContent;
        } else if (responseData === '35455E28-9FDF-4AE4-A4A4-BFF5F9434B8B') {
          this.yearBuilt = i.getElementsByTagName('ResponseDate')[0].textContent;
        } else if (responseData === '61BBAA8F-D720-4CFB-BE7D-D0ED441799A1') {
          this.ownerDetails = i.getElementsByTagName('ResponseString')[0].textContent;
        } else if (responseData === '886DC938-F943-46B6-ADEC-B3DD5620A3D4') {
          this.phone = i.getElementsByTagName('ResponseString')[0].textContent;
        } else if (responseData === 'FA2AEB74-283F-4667-A44B-E953C72A4F38') {
          this.country = i.getElementsByTagName('ResponseString')[0].textContent;
        } else if (responseData === '886DC938-F943-46B6-ADEC-B3DD5620A3D4') {
          this.countryCode = i.getElementsByTagName('ResponseString')[0];
        } else if (responseData === '6D4C11BD-FF67-49AD-ACBF-EAA748D99CAD') {
          this.address = i.getElementsByTagName('ResponseMemo')[0].textContent;
        } else if (responseData === 'D0BE496C-A2FF-47F4-9C6C-C670A726D87F') {
          this.nameOfContactPerson = i.getElementsByTagName('ResponseString')[0].textContent;
        } else if (responseData === 'A0E7A413-25CE-4667-9F64-732076A679EA') {
          this.ownerEmail = i.getElementsByTagName('ResponseString')[0].textContent;
        } else if (responseData === '4E8F4A53-52CF-44DD-BE55-FF10B7723ED1') {
          this.capacity = i.getElementsByTagName('ResponseDecimal')[0].textContent;
        } else if (responseData === '9DE8C20A-6E95-4D73-9A2F-A461BA74A18A') {
          this.grossCapacity = i.getElementsByTagName('ResponseDecimal')[0].textContent;
        } else if (responseData === '99E73D52-40CD-4019-A701-FC067FBE400A') {
          this.deliveryDate = i.getElementsByTagName('ResponseDate')[0].textContent;
        } else if (responseData === 'FE52BFAD-BE94-414E-9E6E-CAE0A2B035F6') {
          this.registryPort = i.getElementsByTagName('ResponseString')[0].textContent;
        } else if (responseData === 'DF0F71B2-4F79-412E-A252-434BE5987826') {
          this.vesselType = i.getElementsByTagName('ResponseString')[0].textContent;
        }
      }

      for (const i of this.dom.getElementsByTagName('CellResponse')) {
        const cellResponseData = i.getAttribute('colId');
        if (cellResponseData === 'D94620B0-3418-43E3-B901-4AD766087D46') {
          this.weightSummer = i.getElementsByTagName('ResponseDecimal')[0].textContent;
        } else if (cellResponseData === '2C61793D-192F-4051-8670-283D732438AB') {
          this.draftSummer = i.getElementsByTagName('ResponseDecimal')[0].textContent;
        }
      }

      this.ownerId(this.ownerDetails);
      this.portId(this.registryPort);
      this.vesselTypesId(this.vesselType);

      const reqData = {
        name: this.ownerDetails,
        phone: this.phone,
        country: this.country,
        countryCode: this.countryCode = '+971',
        email: this.ownerEmail,
        nameOfContactPerson: this.nameOfContactPerson,
        address: this.nameOfContactPerson,
      };

      this.owner.push(reqData);

      log.debug('ownerdet', this.owner);

      this.vesselform.patchValue({
        vessel: this.dom.getElementsByTagName('Header')[0].getElementsByTagName('Vessel')[0].getAttribute('name'),
        imoNumber: this.dom.getElementsByTagName('Header')[0].getElementsByTagName('Vessel')[0].getAttribute('id'),
        masterEmail: this.email,
        lengthOverall: this.length,
        extremeBreadth: this.breadth,
        hullType: this.hullType,
        yearBuilt: this.yearBuilt,
        portId: this.vesselportId,
        vesselOwnerId: this.owner,
        netRegisterTonnage: this.capacity,
        grossTonnage: this.grossCapacity,
        deadWeightSummer: this.weightSummer,
        draftSummer: this.draftSummer,
        registryPort: this.registryPort,
        deliveryDate: this.deliveryDate,
        vesselType: this.vesselType,
        completionDateOfHVPQ: this.dom
          .getElementsByTagName('Header')[0]
          .getElementsByTagName('Document')[0]
          .getAttribute('exported'),
        ownerDetails: this.owner,
        secondaryEmail: this.secondaryEmail,
      });
    };
    fileReader.readAsText(file);
  }

  openDialog(params: any): void {
    this.displayDialog = true;
  }

  ownerId(event: any) {
    const handleResponse = (data: any) => {
      this.ownerData.push(data);
      this.ownerData[0].map((param: any) => {
        if (param.name === event) {
          this.vesselOwnerId = param.vesselOwnerId;
        }
        this.vesselform.patchValue({
          vesselOwnerId: this.vesselOwnerId,
        });
      });
    };
    this.vesselService.getVesselOwners().subscribe(handleResponse, (error) => {
      this.showError(error);
    });
  }

  portId(event: any) {
    this.vesselService.getVesselPorts().subscribe(
      (data) => {
        log.debug('name:', data);
        this.portData.push(data);
        log.debug('portData', this.portData);
        this.portData[0].map((param: any) => {
          log.debug('show port row', param);
          if (param.name === event) {
            this.vesselportId = param.portId;
            log.debug('vesselportId', this.vesselportId);
          }
          this.vesselform.patchValue({
            portId: this.vesselportId,
          });
        });
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  vesselTypesId(event: any) {
    log.debug('vessel type events:', event);
    this.vesselService.getVesselTypes().subscribe(
      (data) => {
        log.debug('vesselTypeId:', data);
        this.vesseltypeData.push(data);
        log.debug('vesseltypeData', this.vesseltypeData);
        this.vesseltypeData[0].map((param: any) => {
          log.debug('show vesselTypeId row', param);
          if (param.vesselType === event) {
            this.vesselTypeId = param.vesselTypeId;
            log.debug('vesselTypeId', this.vesselTypeId);
          }
          this.vesselform.patchValue({
            vesselTypeId: this.vesselTypeId,
          });
        });
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
      }
    );
  }

  onSubmit(vessel: CreateVesselModel) {
    log.debug('Submit Create Vessel Request', JSON.stringify(vessel));

    const optionalFields = {
      lengthOverall: this.length,
      secondaryEmail: this.secondaryEmail,
      netRegisterTonnage: this.capacity,
      grossTonnage: this.grossCapacity,
      deadWeightSummer: this.weightSummer,
      draftSummer: this.draftSummer,
      extremeBreadth: this.breadth,
    };

    vessel = { ...vessel, ...optionalFields };
    log.debug('vessel data:', vessel);

    log.debug('vesselData', this.vesselData);
    const handleResponse = (data: any) => {
      this.notificationService.showSuccess(data.message);
      this.vesselform.reset();
      this.router.navigate(['/admin/vessel']);
    };
    this.vesselService.createVessel(vessel).subscribe(handleResponse, (error) => {
      this.showError(error);
    });
  }

  onOwnerSubmit() {
    this.ownerform.value.countryCode = this.countryCode = '+971';
    const data = this.ownerform.value;
    log.debug('Submit Create user Request', data);
    const handleResponse = (response: any) => {
      this.vesselOwnerData = response;
      this.notifyService.showSuccess('Vessel Owner submitted successfully!');
      this._addDialogContent.close();
    };

    this.vesselService.createVesselOwner(data).subscribe(handleResponse, (error) => {
      this.showError(error);
    });
  }

  onPortSubmit() {
    this.portform.value.countryCode = this.countryCode = '+971';
    const data = this.portform.value;
    log.debug('Submit Create user Request', data);

    const handleResponse = (response: any) => {
      log.debug('response: ', response);
      this.portData = response;
      this.notifyService.showSuccess('Port Added Successfully!');
      this._addPortDialogContent.close();
    };
    this.vesselService.createVesselPort(data).subscribe(handleResponse, (error) => {
      this.showError(error);
    });
  }

  onVesselTypeSubmit() {
    const data = this.vesseltypeform.value;
    log.debug('Submit Creating user Request', data);
    const handleResponse = (response: any) => {
      log.debug('response: ', response);
      this.vesseltypeData = response;
      this.notifyService.showSuccess('Vessel Type Added Successfully!');
      this._addVesselTypeDialogContent.close();
    };
    this.vesselService.createVesselType(data).subscribe(handleResponse, (error) => {
      this.showError(error);
    });
  }

  overviewOwnerReset() {
    this.vesselform.reset();
  }

  overviewPortReset() {
    this.portform.reset();
  }

  overviewVesselTypeReset() {
    this.vesseltypeform.reset();
  }

  onOpenDialog(params: any) {
    this.ownerform.patchValue({
      name: this.ownerDetails,
      phone: this.phone,
      company: this.company,
      country: this.country,
      countryCode: this.countryCode,
      email: this.ownerEmail,
      nameOfContactPerson: this.nameOfContactPerson,
      address: this.address,
    });
  }

  openPortDialog(params: any) {
    this.displayPortDialog = true;
  }

  openVesselTypeDialog(params: any) {
    this.displayVesselTypeDialog = true;
  }

  onPortDialog(params: any) {
    this.portform.patchValue({
      name: this.registryPort,
    });
  }

  onVesselTypeDialog(params: any) {
    this.vesseltypeform.patchValue({
      vesselType: this.vesselType,
    });
  }

  onCountryChange(country: any) {
    this.countryCode = country.dialCode;
    return this.countryCode;
  }

  getNumber(event: any) {
    this.phonenumber = event;
    log.debug('event', event);
  }
  telInputObject(event: any) {
    log.debug('countrycodes:', event);
  }
  onError(obj: any) {
    this.hasError = obj;
    return obj;
  }

  showError = (message: string) => {
    this.notificationService.showError(message);
  };
}
