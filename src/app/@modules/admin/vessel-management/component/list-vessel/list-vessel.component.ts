import { Router } from '@angular/router';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { VesselService } from '@shared/service';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { AgGridAngular } from '@ag-grid-community/angular';
import { VesselSidebarComponent } from '@shared/component/vessel-sidebar/vessel-sidebar.component';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';

const log = new Logger('ListVesselComponent');

@Component({
  selector: 'app-list-vessel',
  templateUrl: './list-vessel.component.html',
  styleUrls: ['./list-vessel.component.scss'],
})
export class ListVesselComponent implements OnInit {
  public gridApi: any;
  public gridColumnApi: any;
  public sortingOrder: any;

  @ViewChild('agGrid') agGrid: AgGridAngular;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  // Notify parent for Page Metadata Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  public array: any;
  public length = 100;
  public rowData: any;
  public sideBar: any;
  public icons: any;
  public columnName: any;
  public vesselId: any;
  rowDataClicked1 = {};
  deleteData: any;
  title: string;
  error: string | undefined;

  vesselTypeValue: any = [];
  vesselTypeValueWithId: any = [];
  vesselOwnerValue: any = [];
  vesselOwnerValueWithId: any = [];
  fleetNameValue: any = [];
  fleetNameValueWithId: any = [];
  portNameValue: any = [];
  portNameValueWithId: any = [];
  fleetManagerValue: any = [];
  fleetManagerValueWithId: any = [];

  editType = '';
  domLayout = 'autoHeight';
  columnDefs = [
    {
      headerName: 'Name',
      field: 'vessel',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'Type',
      field: 'vesselType',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellEditorParams: (vesselTypevalue: any) => {
        return {
          values: this.vesselTypeValue,
        };
      },
    },
    {
      headerName: 'Owner',
      field: 'vesselOwnerName',
      cellEditor: 'agSelectCellEditor',
      editable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellEditorParams: (vesselOwnerValue: any) => {
        vesselOwnerValue = this.vesselOwnerValue;
        return {
          values: vesselOwnerValue,
        };
      },
    },
    {
      headerName: 'Fleet',
      field: 'fleetName',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellEditorParams: (fleetNameValue: any) => {
        fleetNameValue = this.fleetNameValue;
        return {
          values: fleetNameValue,
        };
      },
    },
    {
      headerName: 'Port',
      field: 'portName',
      editable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      cellEditor: 'agSelectCellEditor',
      sortable: true,
      filter: 'agTextColumnFilter',
      cellEditorParams: (portNameValue: any) => {
        portNameValue = this.portNameValue;
        log.debug('portdata:', portNameValue);
        return {
          values: portNameValue,
        };
      },
    },
    {
      headerName: 'Email',
      field: 'masterEmail',
      editable: true,
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Actions',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deleteVessel.bind(this),
        onClickEdit: this.editItems.bind(this),
        hideDetailsButton: true,
      },
      filter: 'agTextColumnFilter',
    },
  ];

  headerHeight: 100;

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    customStatsToolPanel: VesselSidebarComponent,
  };

  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };
  paginationPageSize: number;

  constructor(
    private readonly vesselService: VesselService,
    private readonly notificationService: NotificationService,
    private notifyService: NotificationService,
    private router: Router
  ) {}

  onCellValueChanged(params: any) {
    const vesselTypeData = this.vesselTypeValueWithId.find(
      (vesselType: any) => vesselType.vesselType === params.data.vesselType
    );

    const vesselOwnerData = this.vesselOwnerValueWithId.find(
      (vesselOwner: any) => vesselOwner.name === params.data.vesselOwnerName
    );

    const portData = this.portNameValueWithId.find((port: any) => port.name === params.data.portName);

    const fleetData = this.fleetNameValueWithId.find((fleet: any) => fleet.name === params.data.fleetName);

    log.debug('this.vesselOwner-----', vesselOwnerData);
    log.debug('veseeleparams:', params);
    const reqData = {
      country: params.data.country,
      deadWeightSummer: params.data.deadWeightSummer,
      deliveryDate: params.data.deliveryDate,
      draftSummer: params.data.draftSummer,
      extremeBreadth: params.data.extremeBreadth,
      fleetManagerName: params.data.fleetManagerName,
      fleetName: params.data.fleetName,
      grossTonnage: params.data.grossTonnage,
      isActive: params.data.isActive,
      lengthOverall: params.data.lengthOverall,
      masterEmail: params.data.masterEmail,
      netRegisterTonnage: params.data.netRegisterTonnage,
      portName: params.data.portName,
      secondaryEmail: params.data.secondaryEmail,
      userDetails: params.data.userDetails,
      vessel: params.data.vessel,
      vesselId: params.data.vesselId,
      vesselOwnerName: params.data.vesselOwnerName,
      vesselType: params.data.vesselType,
      vesselTypeId: vesselTypeData.vesselTypeId,
      fleetId: fleetData.fleetId,
      portId: portData.portId,
      vesselOwnerId: vesselOwnerData.vesselOwnerId,
      fleetManagerId: params.data.fleetManagerId,
      vesselUserId: params.data.vesselUserId,
    };
    this.vesselService.updateVessel(params.data.vesselId, reqData).subscribe(
      (data) => {
        this.notificationService.showSuccess(data.message);
        log.debug('updated data:', params.data);
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
        log.debug('not updated', params.data);
      }
    );
  }

  onBtnClick1(e: any) {
    this.rowDataClicked1 = e.rowData;
    alert(this.rowDataClicked1);
  }

  onGridReady(params: any) {
    log.debug('reached');
    this.vesselService.getVessels().subscribe(
      (response) => {
        log.debug('Vessels Data', response);
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  deleteVessel(params: any) {
    this.vesselService.deleteVessel(params.rowData.vesselId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record No Deteted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  fetchData() {
    this.vesselService.getVessels().subscribe((data) => {
      this.rowData = data;
    });
  }

  onAddForm() {
    this.router.navigateByUrl('/admin/vessel/create').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  ngOnInit(): void {
    this.pageSubTitle.emit('ALL VESSELS');
    this.activePageIndex.emit(0);

    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };

    this.paginationPageSize = 20;

    this.vesselTypeList();
    this.getvesselOwner();
    this.getfleetNameValue();
    this.getportNameValue();
    this.getfleetManagerValue();
  }

  vesselTypeList() {
    this.vesselService.getVesselTypes().subscribe(
      (data) => {
        this.vesselTypeValueWithId = data;
        for (const i of data) {
          this.vesselTypeValue.push(i.vesselType);
        }
      },
      (error) => {
        log.error(error);
      }
    );
  }

  getvesselOwner() {
    this.vesselService.getVesselOwners().subscribe(
      (data) => {
        this.vesselOwnerValueWithId = data;
        for (const i of data) {
          this.vesselOwnerValue.push(i.name);
        }
      },
      (error) => {
        log.error(error);
      }
    );
  }

  getfleetNameValue() {
    this.vesselService.getVesselFleets().subscribe(
      (data) => {
        this.fleetNameValueWithId = data;
        for (const i of data) {
          this.fleetNameValue.push(i.name);
        }
      },
      (error) => {
        log.error(error);
      }
    );
  }

  getportNameValue() {
    this.vesselService.getVesselPorts().subscribe(
      (data) => {
        this.portNameValueWithId = data;
        for (const i of data) {
          this.portNameValue.push(i.name);
        }
      },
      (error) => {
        log.error(error);
      }
    );
  }

  getfleetManagerValue() {
    this.vesselService.getVesselFleets().subscribe(
      (data) => {
        this.fleetManagerValueWithId = data;
        for (const i of data) {
          this.fleetManagerValue.push(i.managerFirstName);
        }
      },
      (error) => {
        log.error(error);
      }
    );
  }

  onRowDataChanged($params: any) {}

  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map((node) => node.data);
    const selectedDataStringPresentation = selectedData.map((node) => node.make + ' ' + node.model).join(', ');
    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  }

  /**
   * @deprecated only for example purpose to copy in other modules.
   */
  anyFunction() {
    log.debug('AnyFunction Called');
  }

  editItems() {
    this.router.navigate(['/admin/vessel/update']);
  }
}
