import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-view-vessel',
  templateUrl: './view-vessel.component.html',
  styleUrls: ['./view-vessel.component.scss'],
})
export class ViewVesselComponent implements OnInit {
  // Notify parent for Action and Title Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {
    this.pageSubTitle.emit('VIEW VESSEL');
    this.activePageIndex.emit(2);
  }
}
