import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselDatalistComponent } from './vessel-datalist.component';

describe('VesselDatalistComponent', () => {
  let component: VesselDatalistComponent;
  let fixture: ComponentFixture<VesselDatalistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VesselDatalistComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VesselDatalistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
