import { Component, OnInit } from '@angular/core';
import { IToolPanel, IToolPanelParams } from '@ag-grid-community/core';
import { VesselService } from '@shared/service';
import { Logger } from '@core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@formly';
import { MatCheckboxChange } from '@angular/material/checkbox';

const log = new Logger('vessel-datalist');

@Component({
  selector: 'app-vessel-datalist',
  templateUrl: './vessel-datalist.component.html',
  styleUrls: ['./vessel-datalist.component.css'],
})
export class VesselDatalistComponent implements OnInit, IToolPanel {
  form = new FormGroup({});
  model: any = {};
  fields: FormlyFieldConfig[] = [
    {
      key: 'Input',
      type: 'input',
      templateOptions: {
        label: 'Input',
        placeholder: 'Placeholder',
        description: 'Description',
        required: true,
      },
    },
  ];
  // vessels: VesselModel[] = [];
  selected3: any[] = [];
  public vessels: any;

  constructor(private readonly vesselService: VesselService) {}

  ngOnInit(): void {}

  agInit(params: IToolPanelParams): void {
    const myVesselFilter = new Map<string, string>().set('myvessel', 'true');
    this.vesselService.getVessels(myVesselFilter).subscribe((vessels) => {
      log.debug('vesselid:', vessels);
      this.vessels = vessels;
    });
  }

  refresh(): void {
    throw new Error('Method not implemented.');
  }
  // public selected3:  Array<any>;
  toggle(vessel: string, event: MatCheckboxChange) {
    if (event.checked) {
      this.selected3.push(vessel);
    } else {
      const index = this.selected3.indexOf(vessel);
      if (index >= 0) {
        this.selected3.splice(index, 1);
      }
    }
  }
  exists(vessel: any) {
    return this.selected3.indexOf(vessel) > -1;
  }

  isIndeterminate() {
    return this.selected3.length > 0 && !this.isChecked();
  }

  isChecked() {
    return this.selected3.length === this.vessels.length;
  }

  toggleAll(event: MatCheckboxChange) {
    if (event.checked) {
      this.vessels.forEach((row: any) => {
        // console.log('checked row', row);
        this.selected3.push(row);
      });

      // console.log('checked here');
    } else {
      // console.log('checked false');
      this.selected3.length = 0;
    }
  }
}
