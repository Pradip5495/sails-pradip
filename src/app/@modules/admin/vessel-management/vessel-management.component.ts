import { AfterViewInit, ChangeDetectorRef, Component } from '@angular/core';

@Component({
  selector: 'app-vessel-management',
  templateUrl: './vessel-management.component.html',
  styleUrls: ['./vessel-management.component.scss'],
})
export class VesselManagementComponent implements AfterViewInit {
  // Active Page Metadata
  activePageIndex = 0;

  // Router Outlet Child Component Reference for API Access
  componentRef: any;

  constructor(private readonly changeDetectorRef: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges();
  }

  onActivate(componentReference: any) {
    this.componentRef = componentReference;

    // Setup Page Index Subscription
    this.componentRef.activePageIndex.subscribe((activePageIndex: number) => {
      this.activePageIndex = activePageIndex;
      this.changeDetectorRef.detectChanges();
    });
  }
}
