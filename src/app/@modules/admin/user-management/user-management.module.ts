import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserManagementRoutingModule } from './user-management-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { UserManagementComponent } from './user-management.component';

import { AddUserComponent } from './component/add-user/add-user.component';
import { ListUserComponent } from './component/list-user/list-user.component';
import { ViewUserComponent } from './component/view-user/view-user.component';
import { FormlyModule } from '@formly';
import { EditUserComponent } from '@modules/admin/user-management/component';

@NgModule({
  declarations: [AddUserComponent, ListUserComponent, ViewUserComponent, EditUserComponent, UserManagementComponent],
  imports: [
    CommonModule,
    UserManagementRoutingModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    FormlyModule,
  ],
})
export class UserManagementModule {}
