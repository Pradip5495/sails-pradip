import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { extract } from '@app/i18n';
import { UserManagementComponent } from '@modules/admin/user-management/user-management.component';
import { AddUserComponent, EditUserComponent, ListUserComponent } from '@modules/admin/user-management/component';

// Vessels Child Routes
const children: Routes = [
  {
    path: '',
    component: ListUserComponent,
    data: { title: extract('List User') },
  },
  {
    path: 'create',
    component: AddUserComponent,
    data: { title: extract('New User') },
  },
  {
    path: ':id/edit',
    component: EditUserComponent,
    data: { title: extract('New User') },
  },
];

// Route Config for Vessels Routes within Admin Routing
const routes: Routes = [
  {
    path: '',
    component: UserManagementComponent,
    children,
    data: { title: extract('Users Management') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class UserManagementRoutingModule {}
