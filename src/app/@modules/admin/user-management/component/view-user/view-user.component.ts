import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css'],
})
export class ViewUserComponent implements OnInit {
  // Notify parent for Action and Title Setup.
  // @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  // @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {
    // this.pageSubTitle.emit('VIEW VESSEL');
    // this.activePageIndex.emit(2);
  }
}
