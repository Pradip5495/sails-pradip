import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { UserService, RoleAccessService } from '@shared/service';
import { MatTableDataSource } from '@angular/material/table';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { VesselSidebarComponent } from '@shared/component/vessel-sidebar/vessel-sidebar.component';

const log = new Logger('ListUserComponent');

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss'],
})
export class ListUserComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  // Notify parent for Page Metadata Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  displayedColumns: string[] = [
    'firstname',
    'lastname',
    'phone',
    // 'countryCode',
    // 'addressLine1',
    // 'addressLine2',
    // 'addressLine3',
    'city',
    'state',
    'country',
    'zipcode',
    'role',
    'designation',
    'actions',
  ];

  dataSource: any;
  isLoading = false;
  isdataSource = false;

  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  public array: any;
  public length = 100;
  public pageSizeOptions: number[] = [5, 10, 25, 100];
  public deleteData: any;
  public columnName: any;
  public rowData: any;
  public id: any;

  public sideBar: any;
  public title = 'Users';
  public icons: any;
  public userId: any;
  public roleTypeValue: any = [];
  public desTypeValue: any = [];
  public roleTypeValueWithId: any = [];
  public desTypeValueWithId: any = [];
  public formData: any = [];

  error: string | undefined;

  editType = '';
  domLayout = 'autoHeight';

  columnDefs = [
    {
      headerName: 'First Name',
      field: 'firstname',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'Last Name',
      field: 'lastname',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Phone',
      field: 'phone',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'City',
      field: 'city',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'State',
      field: 'state',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Country',
      field: 'country',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'ZipCode',
      field: 'zipcode',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Role',
      field: 'role',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellEditorParams: (roleTypeValue: any) => {
        return {
          values: this.roleTypeValue,
        };
      },
    },
    {
      headerName: 'Designation',
      field: 'designation',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellEditorParams: (desTypeValue: any) => {
        return {
          values: this.desTypeValue,
        };
      },
    },
    {
      headerName: 'Actions',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.onDelete.bind(this),
        onClickEdit: this.editItems.bind(this),
        hideDetailsButton: true,
      },
      filter: 'agTextColumnFilter',
    },
  ];

  headerHeight: 100;

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    customStatsToolPanel: VesselSidebarComponent,
  };

  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };

  paginationPageSize: number;

  constructor(
    private readonly userService: UserService,
    private readonly notificationService: NotificationService,
    private readonly roleAccessService: RoleAccessService,
    private router: Router
  ) {}

  onCellValueChanged(params: any) {
    log.debug('columnname', params.column.getId());
    log.debug('params data', params);
    log.debug('column', params.value);

    const roleTypeData = this.roleTypeValueWithId.find((roleType: any) => roleType.role === params.data.role);
    log.debug('inrole', roleTypeData);

    const desTypeData = this.desTypeValueWithId.find((desType: any) => desType.designation === params.data.designation);

    const reqData = {
      addressLine1: params.data.addressLine1,
      addressLine2: params.data.addressLine2,
      addressLine3: params.data.addressLine3,
      city: params.data.city,
      country: params.data.country,
      countryCode: params.data.countryCode,
      designation: params.data.designation,
      designationId: desTypeData.designationId,

      firstname: params.data.firstname,
      isActive: params.data.isActive,
      lastname: params.data.lastname,
      phone: params.data.phone,
      role: params.data.role,
      roleId: roleTypeData.roleId,

      state: params.data.state,
      userId: params.data.userId,
      zipcode: params.data.zipcode,
    };

    this.userService.updateUser(params.data.userId, reqData).subscribe(
      (data) => {
        this.notificationService.showSuccess(data.message);
        log.debug('updated data:', params.data);
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
        log.debug('not updated', params.data);
      }
    );
  }

  onGridReady(params: any) {
    log.debug('reached');
    this.userService.getUsers().subscribe(
      (response) => {
        log.debug('Users Data', response);
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  onDelete(params: any) {
    this.userService.deleteUser(params.rowData.userId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notificationService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record Not Deleted';
          this.notificationService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notificationService.showError(message, this.title);
      }
    );
  }

  fetchData() {
    this.userService.getUsers().subscribe((data) => {
      this.rowData = data;
    });
  }

  onAddForm() {
    this.router.navigateByUrl('/admin/user/create').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  ngOnInit(): void {
    this.paginationPageSize = 5;
    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };
    this.roleTypeList();
    this.desTypeList();

    this.pageSubTitle.emit('ALL USERS');
    this.activePageIndex.emit(0);
    this.getAllUsers();

    this.sideBar = {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'columns',
          iconKey: 'columns',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
          },
        },
        {
          id: 'customStats',
          labelDefault: 'Vessels',
          labelKey: 'customStats',
          iconKey: 'custom-stats',
          filter: true,
          toolPanel: 'customStatsToolPanel',
        },
      ],
      filter: true,
      hiddenByDefault: true,
      position: 'right',
    };
  }

  roleTypeList() {
    this.roleAccessService.getRoles().subscribe(
      (data) => {
        this.roleTypeValueWithId = data;
        for (const i of data) {
          this.roleTypeValue.push(i.role);
          log.debug('role', i);
        }
      },
      (error) => {
        log.error(error);
      }
    );
  }

  desTypeList() {
    this.userService.getDesignations().subscribe(
      (data) => {
        this.desTypeValueWithId = data;
        for (const i of data) {
          this.desTypeValue.push(i.designation);
          log.debug('designation', i);
        }
      },
      (error) => {
        log.error(error);
      }
    );
  }

  getAllUsers() {
    this.userService.getUsers().subscribe(
      (response) => {
        log.debug('Users Data', response);
        if (!response) {
          this.isdataSource = true;
        }

        this.dataSource = new MatTableDataSource(response);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.array = response;
        this.totalSize = this.array.length;
        this.iterator();
      },
      (error) => {
        this.notificationService.showError(error);
      }
    );
  }

  public handlePage(e: PageEvent) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  editItems(params: any) {
    log.debug('routers', params.rowData.userId);
    log.debug('link', params.rowData);
    log.debug('link', this.router);
    this.id = params.rowData.userId;
    this.formData = params.rowData;
    // this.router.navigate([`/admin/user/${this.id}/edit`]);

    this.router.navigate([`/admin/user/${this.id}/edit`], { state: this.formData, skipLocationChange: false });
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    this.dataSource = this.array.slice(start, end);
  }
}
