import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { UserService, RoleAccessService } from '@shared/service';
import { NotificationService } from '@shared/common-service';
import { FormGroup } from '@angular/forms';
import { UpdateUser } from '@shared/models';
import { FormlyFieldConfig, FormlyFormOptions } from '@formly';
import { Logger } from '@core';
import { ActivatedRoute, Router } from '@angular/router';
import { ICustomFormlyConfig } from '@types';
import { CustomizedCellComponent } from '@app/@shared/component/customized-cell/customized-cell.component';

const log = new Logger('EditUserComponent');
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
})
export class EditUserComponent implements OnInit {
  error: string | undefined;
  public title: any;
  public showDetails: any;
  public formData: any = [];

  // state$: Observable<object>;

  @ViewChild('details') details: CustomizedCellComponent;
  @Output() userDetails: CustomizedCellComponent;
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  form = new FormGroup({});
  public dataId: any;
  updateUser = new UpdateUser();
  options: FormlyFormOptions = {
    formState: {
      limitDate: true,
      today: new Date(),
    },
  };
  fields: FormlyFieldConfig[] = [
    {
      template: ' <div><strong>Basic Details:</strong></div><hr />',
    },

    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'firstname',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'First Name',
            placeholder: 'Enter User first name',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'lastname',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Last Name',
            placeholder: 'Enter User last name',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      template: '<div><strong>Contact:</strong></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'countryCode',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Country Code',
            placeholder: 'Enter User country Code ',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'phone',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Phone',
            placeholder: 'Enter phone',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'addressLine1',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Address Line 1',
            placeholder: 'Enter address Line 1 ',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'addressLine2',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Address Line 2',
            placeholder: 'Enter address Line 2',
            throwError: 'Field is required',
          },
        },
        {
          key: 'addressLine3',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Address Line 3',
            placeholder: 'Enter address Line 3',
            throwError: 'Field is required',
          },
        },
      ],
    },

    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'city',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'City',
            placeholder: 'Enter city',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'state',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'State',
            placeholder: 'Enter state',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'country',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Country',
            placeholder: 'Enter country',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'zipcode',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'zipcode',
            placeholder: 'Enter zipcode',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      template: '<div><strong>Dimension:</strong></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'roleId',
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Role',
            options: this.roleAccessService.getRoles(),
            valueProp: 'roleId',
            labelProp: 'role',
            required: true,
          },
        },
        {
          key: 'designationId',
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Designation',
            options: this.userService.getDesignations(),
            valueProp: 'designationId',
            labelProp: 'designation',
            required: true,
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      config: {
        form: this.form,
        model: this.updateUser,
        options: this.options,
        fields: this.fields,
      },
      onSave: this.onSubmit.bind(this),
      onDiscard: () => this.form.reset(),
    },
  ];

  constructor(
    private readonly roleAccessService: RoleAccessService,
    private readonly userService: UserService,
    private notifyService: NotificationService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  onAddForm() {
    this.router.navigateByUrl('/admin/user').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  ngOnInit(): void {
    this.pageSubTitle.emit('UPDATE USER');
    this.activePageIndex.emit(1);
    log.debug('route details:', this.route);

    const paramValue = 'value';
    if (this.route.params[paramValue].id) {
      this.formData = {};
      this.dataId = this.route.params[paramValue].id;

      if (this.route.params[paramValue].id === history.state.userId) {
        this.formData = history.state;
      }

      log.debug('formdata:', this.formData);

      this.form.patchValue(this.formData);
      this.getUsersById(this.formData);
    }
  }

  getUsersById(params: any) {
    log.debug('usersData: ', params);
    this.form.patchValue(params);
  }

  onSubmit(params: any) {
    log.debug('details:', params);
    log.debug('params submit data:', params.designationId);
    this.userService.updateUser(this.dataId, params).subscribe(
      (response) => {
        log.debug('response data: ', response);
        this.notifyService.showSuccess('User Updated successfully!');
        if (response) {
          this.router.navigate(['/admin/user']);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }
}
