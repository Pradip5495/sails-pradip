import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserService, RoleAccessService } from '@shared/service';
import { NotificationService } from '@shared/common-service';
import { FormGroup } from '@angular/forms';
import { UserModel } from '@shared/models';
import { FormlyFieldConfig, FormlyFormOptions } from '@formly';
import { Logger } from '@core';
import { ICustomFormlyConfig } from '@types';
import { Router } from '@angular/router';

const log = new Logger('AddUerComponent');

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  // Notify parent for Action and Title Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  form = new FormGroup({});
  model = new UserModel();
  options: FormlyFormOptions = {
    formState: {
      limitDate: true,
      today: new Date(),
    },
  };
  fields: FormlyFieldConfig[] = [
    {
      template: ' <div><strong>Basic Details:</strong></div><hr />',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'username',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Username',
            placeholder: 'Enter User Name',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'password',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Password',
            type: 'password',
            placeholder: 'Enter password',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'firstname',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'First Name',
            placeholder: 'Enter User first name',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'lastname',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Last Name',
            placeholder: 'Enter User last name',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      template: '<div><strong>Contact:</strong></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'countryCode',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Country Code',
            placeholder: 'Enter User country Code ',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'phone',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Phone',
            placeholder: 'Enter Phone',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'addressLine1',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Address Line 1',
            placeholder: 'Enter address Line 1 ',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'addressLine2',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Address Line 2',
            placeholder: 'Enter address Line 2',
            throwError: 'Field is required',
          },
        },
        {
          key: 'addressLine3',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Address Line 3',
            placeholder: 'Enter address Line 3',
            throwError: 'Field is required',
          },
        },
      ],
    },

    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'city',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'City',
            placeholder: 'Enter city',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'state',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'State',
            placeholder: 'Enter state',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'country',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Country',
            placeholder: 'country',
            required: true,
            throwError: 'Field is required',
          },
        },
        {
          key: 'zipcode',
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Zipcode',
            placeholder: 'Enter zipcode',
            required: true,
            throwError: 'Field is required',
          },
        },
      ],
    },
    {
      template: '<div><strong>Dimension:</strong></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'roleId',
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Role',
            options: this.roleAccessService.getRoles(),
            valueProp: 'roleId',
            labelProp: 'role',
            required: true,
          },
        },
        {
          key: 'designationId',
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Designation',
            options: this.userService.getDesignations(),
            valueProp: 'designationId',
            labelProp: 'designation',
            required: true,
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      config: {
        form: this.form,
        model: this.model,
        options: this.options,
        fields: this.fields,
      },
      onSave: this.onSubmit.bind(this),
      onDiscard: () => this.form.reset(),
    },
  ];

  constructor(
    // private readonly vesselService: VesselService,
    private readonly roleAccessService: RoleAccessService,
    private readonly userService: UserService,
    private readonly notificationService: NotificationService,
    private router: Router
  ) {}

  onAddForm() {
    this.router.navigateByUrl('/admin/user').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  ngOnInit(): void {
    this.pageSubTitle.emit('NEW USER');
    this.activePageIndex.emit(1);
  }

  onSubmit(user: UserModel) {
    log.debug('Submit Create user Request', JSON.stringify(user));
    this.userService.createUser(user).subscribe(
      (data) => {
        this.notificationService.showSuccess('User created !');
      },
      (error) => {
        log.error(error);
        this.notificationService.showError(error);
      }
    );
  }
}
