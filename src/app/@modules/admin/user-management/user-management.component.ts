import { AfterViewInit, ChangeDetectorRef, Component } from '@angular/core';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss'],
})
export class UserManagementComponent implements AfterViewInit {
  activePageIndex = 0;

  componentRef: any;

  constructor(private readonly changeDetectorRef: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges();
  }

  onActivate(componentReference: any) {
    this.componentRef = componentReference;

    // Setup Page Index Subscription
    this.componentRef.activePageIndex.subscribe((activePageIndex: number) => {
      this.activePageIndex = activePageIndex;
      this.changeDetectorRef.detectChanges();
    });
  }
}
