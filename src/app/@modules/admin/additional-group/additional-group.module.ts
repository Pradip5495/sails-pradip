import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdditionalGroupComponent } from './additional-group.component';
import { AdditionalGroupRoutingModule } from './additional-group-routing.module';
import { ListAdditionalGroupComponent } from './component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { FormlyModule } from '@formly';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AgGridModule } from '@ag-grid-community/angular';

@NgModule({
  declarations: [AdditionalGroupComponent, ListAdditionalGroupComponent],
  imports: [
    CommonModule,
    AdditionalGroupRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    SharedModule,
    MatFormFieldModule,
    FlexLayoutModule,
    MatDialogModule,
    MaterialModule,
    FormlyModule,
    AgGridModule,
  ],
})
export class AdditionalGroupModule {}
