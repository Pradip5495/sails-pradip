import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdditionalGroupComponent } from './additional-group.component';
import { extract } from '@app/i18n';
import { ListAdditionalGroupComponent } from './component/list-additional-group/list-additional-group.component';

const children: Routes = [
  {
    path: '',
    component: ListAdditionalGroupComponent,
    data: { title: extract('List Additional Group') },
  },
];

const routes: Routes = [
  {
    path: '',
    component: AdditionalGroupComponent,
    children,
    data: { title: extract('Additional Group Management') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdditionalGroupRoutingModule {}
