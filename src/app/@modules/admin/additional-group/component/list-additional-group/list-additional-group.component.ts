import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { AgGridAngular } from '@ag-grid-community/angular';
import { VesselService } from '@shared/service/vessel.service';
import { UserService } from '@shared/service/user.service';
import { Logger } from '@core';
import { NotificationService } from '@shared/common-service';
import { CellValidationComponent } from '@shared/component/cell-validation/cell-validation.component';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { AdditionalGroupService } from '@shared/service/additional-group.service';
import { IJson } from '@types';
import { relativeTimeRounding } from 'moment';

const log = new Logger('AdditionalGroupListComponent');

@Component({
  selector: 'app-list-additional-group',
  templateUrl: './list-additional-group.component.html',
  styleUrls: ['./list-additional-group.component.scss'],
})
export class ListAdditionalGroupComponent implements OnInit {
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();
  @ViewChild('updatedialog', { static: true }) _updatedialogContent: any;
  @ViewChild('newdialog', { static: true }) _addDialogContent: any;
  @ViewChild('addVesseldialog', { static: true }) _vesselContent: any;
  listData: any;
  groupName: any;
  groupUserId: any;

  get f() {
    return this.additionalFormGroup.controls;
  }

  additionalFormGroup: FormGroup;
  additionalUpdateFormGroup: FormGroup;
  additionalVesselFormGroup: FormGroup;
  vessel: FormArray;

  public vesselGroupData: any;
  public additionalData: any = [];
  public currentVessel: any = [];
  public gridApi: any;
  public gridColumnApi: any;
  public userIdGroup: any;
  public rowSelection: any;
  public sortingOrder: any;
  public additionalGroupUserId: any;
  public additionalGroupVesselId: any;
  public additionalGroupUserUpdateId: any;
  public selectedValue: any;
  public additionalGroupId: any;
  public active: any;
  public noVesselMessage: any = false;
  public addVesselDetail: any = [];
  updatingAdditionalGroup = false;
  public additionalUpdateDetails: any = [];
  public checkboxVessel: any = [];
  public vesselIdAdditional: any = [];
  public Array3: any = [];
  public reqVessel: any = [];
  vesselArray1: any = [];
  vesselArray2: any = [];
  newArray: any = [];
  demoVessel1: any = [];

  selected = 'option';
  selectedItem: string;

  animal: string;
  name: string;

  @ViewChild('agGrid') agGrid: AgGridAngular;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  // Notify parent for Page Metadata Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  public array: any;
  public length = 100;
  public rowData: any;
  public sideBar: any;
  public id: any;
  public icons: any;
  public columnName: any;
  public vesselId: any = [];
  rowDataClicked1 = {};
  deleteData: any;
  title: string;
  error: string | undefined;
  public user: any = [];
  vesselArray: any = [];
  public vesselListArray: any = [];
  public userWithId: any = [];
  public vesselIdArray: any = [];
  public vesselIdArray1: any = [];
  public rowList: any = [];
  displayDialog = false;
  displayUpdateDialog = false;
  displayVesselDialog = false;
  checked = 'true';
  public type: any = 'true';
  public vesselAdditionalGroup: any = [];

  public formData: any = [];
  domLayout = 'autoHeight';
  vesselValue: any = [];
  vesselValueWithId: any = [];
  vesselsList: any = [];
  showVessels: any = [];
  demoVessels: any = [];

  editType = '';

  columnDefs = [
    {
      headerName: 'Name',
      field: 'name',
      flex: 1,
      resizable: true,
      cellEditor: 'cellValidation',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Users',
      field: 'userName',
      flex: 1,
      resizable: true,
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      cellEditorParams: (user: any) => {
        return {
          values: this.user,
        };
      },
    },
    {
      headerName: 'Actions',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deleteAdditionalGroup.bind(this),
        onClickEdit: this.editItems.bind(this),
      },
      filter: 'agTextColumnFilter',
    },
  ];

  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };
  paginationPageSize: number;

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    cellValidation: CellValidationComponent,
  };

  constructor(
    private readonly vesselService: VesselService,
    private readonly notificationService: NotificationService,
    private notifyService: NotificationService,
    private readonly userService: UserService,
    private readonly additionalGroupService: AdditionalGroupService,
    private formBuilder: FormBuilder
  ) {
    this.rowSelection = 'single';
  }

  onCellValueChanged(params: any) {
    const userData = this.userWithId.find((user: any) => user.firstname + ' ' + user.lastname === params.data.userName);

    this.reqVessel = params.data.vessel.map((vessel: any) => vessel.vesselId);

    const reqData = {
      name: params.data.name,
      userId: userData.userId,
      vesselId: this.reqVessel,
    };

    const handleResponse = (data: any) => {
      this.notificationService.showSuccess(data.message);
      this.onGridReady(params);
    };

    this.additionalGroupService
      .updateAdditionalGroup(params.data.additionalGroupId, reqData)
      .subscribe(handleResponse, this.showError);
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const handleResponse = (response: any) => {
      this.rowData = response;
    };
    this.additionalGroupService.getAdditionalGroup().subscribe(handleResponse, this.showError);
  }

  onRowSelected(event: any) {
    if (!event.node.selected) {
      return;
    }
    this.showVessels = [];
    this.vesselIdAdditional = [];
    this.checkboxVessel = [];
    this.addVesselDetail = event.data;
    this.vesselList(event.data.vessel);
  }

  deleteAdditionalGroup(params: any) {
    this.additionalGroupService.deleteAdditionalGroup(params.rowData.additionalGroupId).subscribe((response) => {
      this.deleteData = response;
      const message = this.deleteData.message;
      this.notifyService.showSuccess(message, this.title);
      this.onGridReady(params);
    }, this.showError);
  }

  onAddForm() {
    this.openDialog();
  }

  ngOnInit(): void {
    this.userList();
    this.activePageIndex.emit(0);
    this.showVessel();

    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };

    this.paginationPageSize = 7;

    this.additionalFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      userId: new FormControl(['', Validators.required]),
      vesselId: ['', Validators.required],
    });

    this.additionalUpdateFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      userId: new FormControl(['', Validators.required]),
      vessel: this.formBuilder.array([]),
    });

    this.additionalVesselFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      userId: new FormControl(['', Validators.required]),
    });
  }

  userList() {
    this.userService.getUsers().subscribe(
      (data) => {
        this.userWithId = data;
        for (const i of data) {
          this.user.push(i.firstname + ' ' + i.lastname);
        }
      },
      (error) => {
        log.error(error);
      }
    );
  }

  showVessel(isFiltersApplied: boolean = true) {
    this.vesselArray1 = [];
    this.vesselArray2 = [];
    this.newArray = [];
    this.vesselArray = [];
    
    const filters = new Map();
    if (isFiltersApplied) {
      filters.set('myvessel', 'true');
    }

    //  this.vesselArray = this.vesselAdditionalGroup.map((test: IJson) => test.vessel);
    //   log.debug('vesselaeeattta',this.vesselArray)

    //   this.vesselArray1 = this.showVessels;
    //     log.debug('vesselArray1:', this.vesselArray1);

    //     this.newArray = this.vesselArray.filter((data: any)=>{
    //       log.debug('data',data);
    //       return !this.vesselArray1.includes(data);
    //     })
    //     log.debug('newArray',this.newArray)
        

    this.vesselService.getVessels(filters).subscribe((data) => {
      this.vesselAdditionalGroup = data;

      for (const i of this.vesselAdditionalGroup) {
        log.debug('gfcgfc', i);
        this.vesselArray.push(i);
      }

      this.demoVessels = this.vesselArray.map((test: IJson) => test.vessel);
      
      log.debug('demovessels',this.demoVessels);
      log.debug('vesselArray',this.vesselArray);

      this.demoVessel1 = this.showVessels;
      log.debug('showdemo',this.demoVessel1);
      log.debug('showvessel',this.showVessels);

      // this.newArray = this.demoVessels.filter((data: any)=>{
      //         log.debug('data',data);
      //         return !this.demoVessel1.includes(data);
      //       })
      //       log.debug('newArray',this.newArray)

      

      // this.demoVessel1.some((data1: any) => {
      //   log.debug('dfgfdg',data1)
      //   this.demoVessels.filter((item:any)=>{
      //     log.debug('item',item)
      //     if (!(data1 === item.vessel)){
      //       log.debug('data1',item.vessel);
      //       this.newArray.push(item.vessel)
      //       const array = this.newArray;
      //       log.debug('array',array);
            
      //     }
      //   })
      // })



      // for(const i of this.demoVessel1){
      //   log.debug('index',i);
      //   const demo = i;
      //   for(const j of this.demoVessels){
      //     log.debug('indexvessel',j);
      //     if(!(demo === j)){
      //       log.debug('demosaasa',demo);
      //      this.newArray.push(j);
      //     }
      //   }
      // }
      
     
      this.onGridReady(this.vesselAdditionalGroup);
    }, this.showError);
  }

  vesselList(event: any) {
    if (event === null) {
      return;
    }
    for (const i of event) {
      log.debug('iiiii',i)
      this.showVessels.push(i.vessel);
     
      this.vesselIdAdditional.push(i.vesselId);
    }
  }

  onRowDataChanged($params: any) {}

  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map((node) => node.data);
    const selectedDataStringPresentation = selectedData.map((node) => node.make + ' ' + node.model).join(', ');
    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  }

  onSelectionChanged(event: any) {
    this.vesselsList = [];
  }

  anyFunction() {
    log.debug('AnyFunction Called');
  }

  editItems(params: any) {
    this.formData = params.rowData;
    this.openUpdateDialog(this.formData);
  }

  onSubmit() {
    this.vesselIdAdditional = this.checkboxVessel.map((vessel: any) => vessel.vesselId);
    this.additionalFormGroup.value.vesselId = this.vesselIdAdditional;

    this.additionalFormGroup.value.vesselId = this.vesselIdAdditional;

    const data = this.additionalFormGroup.value;

    const handleResponse = (response: any) => {
      this.additionalData = response;
      this.notifyService.showSuccess('Additional group created successfully!', this.title);
      this.overviewReset();
      this._addDialogContent.close();
      this.onGridReady(this.additionalData);
    };

    this.additionalGroupService.createAdditionalGroup(data).subscribe(handleResponse, this.showError);
  }

  onUpdateSubmit() {
    log.debug('updated data:', this.additionalUpdateFormGroup.value);

    const vessels = [...this.additionalUpdateFormGroup.value.vessel];
    log.debug('updatevessle', vessels);

    this.showVessels = [
      ...vessels
        .filter((vessel: any) => {
          return vessel.vesselStatus;
        })
        .map((vessel: any) => {
          return vessel.vessel;
        }),
    ];

    const newVessels = [
      ...vessels
        .filter((vessel: any) => {
          return vessel.vesselStatus;
        })
        .map((vessel: any) => {
          return vessel.vesselId;
        }),
    ];

    const reqData = {
      name: this.additionalUpdateFormGroup.value.name,
      userId: this.additionalUpdateFormGroup.value.userId,
      vesselId: newVessels,
    };
    log.debug('vessedata', this.vesselListArray);

    log.debug('reqData:', reqData);

    this.updatingAdditionalGroup = true;

    const handleResponse = (response: any) => {
      log.debug('response', response);
      this.updatingAdditionalGroup = false;
      this.notifyService.showSuccess('Additional Group Updated successfully!');
      this.overviewUpdateReset();
      this.onGridReady(this.additionalData);
      this._updatedialogContent.close();
    };

    this.additionalGroupService
      .updateAdditionalGroup(this.additionalGroupId, reqData)
      .subscribe(handleResponse, (error) => {
        this.updatingAdditionalGroup = false;
        this.showError(error);
      });
  }

  onVesselSubmit() {
    this.checkboxVessel.map((vessel: any) => {
      this.showVessels.push(vessel.vessel);
      this.vesselIdAdditional.push(vessel.vesselId);
    });

    const reqData = {
      name: this.addVesselDetail.name,
      userId: this.addVesselDetail.userId,
      vesselId: this.vesselIdAdditional,
    };

    const handleResponse = (response: any) => {
      this.updatingAdditionalGroup = false;
      this.notifyService.showSuccess('Vessel Added Successfully!');
      this.onGridReady(this.additionalData);
      this._vesselContent.close();
    };

    this.additionalGroupService
      .updateAdditionalGroup(this.addVesselDetail.additionalGroupId, reqData)
      .subscribe(handleResponse, (error) => {
        this.updatingAdditionalGroup = false;
        this.showError(error);
      });
  }

  overviewVesselReset() {
    this.additionalVesselFormGroup.reset();
  }

  overviewUpdateReset() {
    this.additionalUpdateFormGroup.reset();
  }

  overviewReset() {
    this.additionalFormGroup.reset();
  }

  openDialog(): void {
    this.displayDialog = true;
    this.showVessel();
  }

  discard(event: any) {
    this.displayDialog = false;
  }

  getVesselFormArray() {
    return this.additionalUpdateFormGroup.controls.vessel as FormArray;
  }

  openUpdateDialog(params: any): void {
    this.checkboxVessel = [];
    this.additionalGroupId = params.additionalGroupId;
    this.additionalUpdateDetails = params.vessel;

    this.getVesselFormArray().clear();

    if (params.vessel) {
      params.vessel.map((vessel: any) => {
        log.debug('vessel1', vessel);
        const vesselData = {
          ...vessel,
          vesselStatus: true,
        };
        log.debug('vessel', vessel);
        this.getVesselFormArray().push(this.formBuilder.group(vesselData));
      });
    }

    this.displayUpdateDialog = true;

    this.additionalUpdateFormGroup.patchValue({
      name: params.name,
      userId: params.userId,
      vessel: [params.vessel],
    });
  }

  openVesselDialog(): void {
    this.displayVesselDialog = true;
    this.additionalVesselFormGroup.patchValue({
      name: this.addVesselDetail.name,
      userId: this.addVesselDetail.userName,
    });
    this.showVessel();
  }

  toggle(selectedVessel: any, checkbox?: any) {
    const checkBoxStatus = checkbox.checked;
    log.debug('togle', selectedVessel, checkbox);
    if (!checkBoxStatus) {
      checkbox.checked = true;
      this.checkboxVessel.push(selectedVessel);
    } else {
      checkbox.checked = false;
      this.checkboxVessel = this.checkboxVessel.filter((vessel: any) => vessel.vesselId !== selectedVessel.vesselId);
    }
  }

  checkboxClick(event: any) {
    event.preventDefault();
  }
  showError = (message: string) => {
    this.notificationService.showError(message);
  };
}
