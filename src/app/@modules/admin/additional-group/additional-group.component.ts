import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-additional-group',
  templateUrl: './additional-group.component.html',
  styleUrls: ['./additional-group.component.scss'],
})
export class AdditionalGroupComponent implements AfterViewInit {
  // Active Page Metadata
  activePageIndex = 0;

  // Router Outlet Child Component Reference for API Access
  componentRef: any;

  constructor(private readonly changeDetectorRef: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges();
  }

  onActivate(componentReference: any) {
    this.componentRef = componentReference;

    // Setup Page Index Subscription
    this.componentRef.activePageIndex.subscribe((activePageIndex: number) => {
      this.activePageIndex = activePageIndex;
      this.changeDetectorRef.detectChanges();
    });
  }
}
