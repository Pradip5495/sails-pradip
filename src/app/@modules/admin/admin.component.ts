import { Component, OnInit } from '@angular/core';
import { SidenavStoreService } from '@app/shell/sidenav/sidenav-store.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  constructor(private readonly sidenavStore: SidenavStoreService) {}

  ngOnInit(): void {
    // this.sidenavStore.hasSideNav = true;
    this.sidenavStore.hasMyVessels = false; // Disable MyVessels Filter
  }
}
