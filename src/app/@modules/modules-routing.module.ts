import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShellComponent } from '@app/shell/shell.component';
import { AuthenticationGuard } from '@core/auth';

const moduleRoutes: Routes = [
  { path: '', redirectTo: 'indicent', pathMatch: 'full' },
  {
    path: '',
    component: ShellComponent,
    children: [
      {
        path: 'incident',
        loadChildren: () =>
          import('@modules/incident-investigation/incident-investigation.module').then(
            (m) => m.IncidentInvestigationModule
          ),
      },
      { path: 'home', loadChildren: () => import('@modules/home').then((m) => m.HomeModule) },
      { path: 'admin', loadChildren: () => import('@modules/admin').then((m) => m.AdminModule) },
      {
        path: 'reset-password',
        loadChildren: () => import('@modules/reset-password/reset-password.module').then((m) => m.ResetPasswordModule),
      },
      {
        path: 'activation',
        loadChildren: () => import('@modules/activation/activation.module').then((m) => m.ActivationModule),
      },
      { path: 'profile', loadChildren: () => import('@modules/profile').then((m) => m.ProfileModule) },
      { path: 'organization', loadChildren: () => import('@modules/organization').then((m) => m.OrganizationModule) },
      { path: 'audit', loadChildren: () => import('@modules/audit-inspections').then((m) => m.AuditInspectionsModule) },
    ],
    canActivate: [AuthenticationGuard],
    // Reuse ShellComponent instance when navigating between child views
    // data: { reuse: true },
  },
  { path: '**', redirectTo: '/home' },
];

@NgModule({
  imports: [RouterModule.forChild(moduleRoutes)],
  exports: [RouterModule],
})
export class ModulesRoutingModule {}
