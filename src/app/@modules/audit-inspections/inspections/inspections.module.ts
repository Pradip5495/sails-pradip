import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { RouterModule } from '@angular/router';
import { InspectionsComponent } from './inspections.component';
import { InspectionsTableComponent } from './component/inspections-table/inspections-table.component';
import { MatDialogModule } from '@angular/material/dialog';
import { InspectionSummaryComponent } from './component/inspection-summary/inspection-summary.component';
import { InspectionsRoutingModule } from './inspections-routing.module';
import { MatIconModule } from '@angular/material/icon';
import { InspectionsFormComponent } from './component/inspections-form/inspections-form.component';
import { FormlyModule } from '@formly';

@NgModule({
  declarations: [InspectionsComponent, InspectionsTableComponent, InspectionSummaryComponent, InspectionsFormComponent],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    FormlyModule,
    MatDialogModule,
    MatIconModule,
    InspectionsRoutingModule,
  ],
})
export class InspectionsModule {}
