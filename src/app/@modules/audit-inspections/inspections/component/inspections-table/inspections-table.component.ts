import { Component, OnInit } from '@angular/core';
import { CustomizedCellComponent } from '@app/@shared/component/customized-cell/customized-cell.component';
import { GridOptions } from '@ag-grid-community/core';
import { IJson } from '@types';
import { rows, columns } from './inspections-table.cell-data';
import { ProgressBarCellRendererComponent, ButtonCellRendererComponent } from '@shared/component';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-inspections-table',
  templateUrl: './inspections-table.component.html',
  styleUrls: ['./inspections-table.component.scss'],
})
export class InspectionsTableComponent implements OnInit {
  icons: IJson;
  gridOptions: GridOptions;
  columnDefs = columns;
  rowData = rows;
  domLayout = 'autoHeight';
  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    progressBar: ProgressBarCellRendererComponent,
    button: ButtonCellRendererComponent,
  };
  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };

  newInspectionsDialog = false;

  dataEntryInspectionsDialog = false;

  newInspectionsFormDialog = false;

  viewInspectionDialog = false;

  dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);

  displayedColumns: string[] = ['number', 'summary', 'status'];

  constructor() {}

  ngOnInit() {
    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
      sortUnSort: '<span class="ag-icon ag-icon-desc"></span>',
    };
    this.gridOptions = {
      getRowHeight: (params: any) => {
        return params.node.group ? 70 : null;
      },
      rowSelection: 'multiple',
      groupSelectsChildren: true,
      groupSelectsFiltered: true,
      suppressAggFuncInHeader: true,
      suppressRowClickSelection: false,
      getRowStyle: (params: any) => {
        const node = params.node;
        if (node.group) {
          return null;
        }
        return {
          background: node.rowIndex % 2 === 0 ? '#f2f2f8' : '#ecedf4',
        };
      },
      autoGroupColumnDef: {
        headerName: 'Vessel',
        field: 'group',
        width: 300,
        cellRenderer: 'agGroupCellRenderer',
      },
      groupRowRendererParams: {
        suppressCount: true,
      },

      getNodeChildDetails: function getNodeChildDetails(rowItem: any) {
        if (rowItem.participants) {
          return {
            group: true,
            children: rowItem.participants,
            key: rowItem.group,
          };
        } else {
          return null;
        }
      },
      onGridReady(params: any) {},
    };
  }

  onMonthFilterChange(event: any) {
    // console.log(event.value);
  }

  onSubmit(value: any) {}
}

export interface Element {
  number: string;
  summary: string;
  status: string;
}

const ELEMENT_DATA: Element[] = [
  {
    number: '01',
    summary:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum pharetra tortor, ut iaculis lacus lobortis sed. Vestibulum sed neque eget erat sollicitudin lobortis',
    status: 'VL',
  },
  {
    number: '02',
    summary:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum pharetra tortor, ut iaculis lacus lobortis sed. Vestibulum sed neque eget erat sollicitudin lobortis',
    status: 'L',
  },
  {
    number: '03',
    summary:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum pharetra tortor, ut iaculis lacus lobortis sed. Vestibulum sed neque eget erat sollicitudin lobortis',
    status: 'VL',
  },
  {
    number: '01',
    summary:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum pharetra tortor, ut iaculis lacus lobortis sed. Vestibulum sed neque eget erat sollicitudin lobortis',
    status: 'L',
  },
  {
    number: '01',
    summary:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum pharetra tortor, ut iaculis lacus lobortis sed. Vestibulum sed neque eget erat sollicitudin lobortis',
    status: 'VL',
  },
  {
    number: '02',
    summary:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum pharetra tortor, ut iaculis lacus lobortis sed. Vestibulum sed neque eget erat sollicitudin lobortis',
    status: 'VL',
  },
  {
    number: '01',
    summary:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum pharetra tortor, ut iaculis lacus lobortis sed. Vestibulum sed neque eget erat sollicitudin lobortis',
    status: 'L',
  },
  {
    number: '02',
    summary:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum pharetra tortor, ut iaculis lacus lobortis sed. Vestibulum sed neque eget erat sollicitudin lobortis',
    status: 'L',
  },
];
