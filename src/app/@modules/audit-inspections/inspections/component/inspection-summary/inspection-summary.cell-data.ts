export const columns = [
  {
    headerName: 'DATE INSPECTION',
    field: 'date',
    headerCheckboxSelectionFilteredOnly: true,
    unSortIcon: true,
    suppressMenu: true,
    headerTooltip: 'Date Inspection',
    flex: 0.7,
  },
  {
    headerName: 'OIL MAJOR',
    field: 'oilMajor',
    width: 100,
    flex: 1,
    headerTooltip: 'Oil Major',
  },
  {
    headerName: 'PORT',
    field: 'port',
    unSortIcon: true,
    flex: 0.8,
    headerTooltip: 'Port',
  },
  {
    headerName: 'NO. OF OBS',
    field: 'obsno',
    width: 148,
    headerTooltip: 'No. of OBS',
    flex: 1,
  },
];

export const rows = [
  {
    group: 'Vessel 10',
    inspection: { label: 'SIRE', tag: 'Equinor' },
    status: 21,
    date: '04 Aug 2019',
    port: 'Mumbai',
  },
  {
    group: 'Vessel 11',
    inspection: { label: 'SIRE', tag: 'Equinor' },
    status: 43,
    date: '04 Aug 2019',
    port: 'Mumbai',
  },
  {
    group: 'Vessel 12',
    inspection: { label: 'SIRE', tag: 'Equinor' },
    status: 75,
    date: '04 Aug 2019',
    port: 'Mumbai',
  },
  {
    group: 'Vessel 12',
    inspection: { label: 'SIRE', tag: 'Equinor' },
    status: 100,
    date: '04 Aug 2019',
    port: 'Mumbai',
  },
  {
    group: 'Vessel 13',
    inspection: { label: 'SIRE', tag: 'Equinor' },
    status: 95,
    date: '04 Aug 2019',
    port: 'Mumbai',
  },
];
