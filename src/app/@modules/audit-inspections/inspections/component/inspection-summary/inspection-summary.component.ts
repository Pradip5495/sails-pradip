import { Component, OnInit } from '@angular/core';
import { CustomizedCellComponent } from '@app/@shared/component/customized-cell/customized-cell.component';
import { GridOptions } from '@ag-grid-community/core';
import { IJson } from '@types';
import { rows, columns } from './inspection-summary.cell-data';
import { ProgressBarCellRendererComponent, ButtonCellRendererComponent } from '@shared/component';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-inspection-summary',
  templateUrl: './inspection-summary.component.html',
  styleUrls: ['./inspection-summary.component.scss'],
})
export class InspectionSummaryComponent implements OnInit {
  icons: IJson;
  gridOptions: GridOptions;
  columnDefs = columns;
  rowData = rows;
  domLayout = 'autoHeight';
  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    progressBar: ProgressBarCellRendererComponent,
    button: ButtonCellRendererComponent,
  };
  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };

  newInspectionsDialog = false;

  dataSource: any[] = [
    {
      inspection: '............',
      port: '.............',
      date: '.............',
    },
  ];
  displayedColumns: string[] = ['inspection', 'port', 'date'];

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
      sortUnSort: '<span class="ag-icon ag-icon-desc"></span>',
    };
    this.gridOptions = {
      getRowHeight: (params: any) => {
        return params.node.group ? 70 : null;
      },
      rowSelection: 'multiple',
      groupSelectsChildren: true,
      groupSelectsFiltered: true,
      suppressAggFuncInHeader: true,
      suppressRowClickSelection: false,
      getRowStyle: (params: any) => {
        const node = params.node;
        if (node.group) {
          return null;
        }
        return {
          background: node.rowIndex % 2 === 0 ? '#f2f2f8' : '#ecedf4',
        };
      },
      autoGroupColumnDef: {
        headerName: 'Vessel',
        field: 'group',
        width: 300,
        cellRenderer: 'agGroupCellRenderer',
      },
      groupRowRendererParams: {
        suppressCount: true,
      },

      getNodeChildDetails: function getNodeChildDetails(rowItem: any) {
        if (rowItem.participants) {
          return {
            group: true,
            children: rowItem.participants,
            key: rowItem.group,
          };
        } else {
          return null;
        }
      },
      onGridReady(params: any) {},
    };
  }

  onMonthFilterChange(event: any) {
    // console.log(event.value);
  }

  onSubmit(value: any) {}

  onCreateForm() {
    this.router.navigate(['../create'], { relativeTo: this.activatedRoute });
  }
}
