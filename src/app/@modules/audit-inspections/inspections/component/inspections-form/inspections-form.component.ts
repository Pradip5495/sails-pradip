import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@formly';
import { ICustomFormlyConfig } from '@types';
import { UserService } from '@shared';

@Component({
  selector: 'app-fleet-notification-form',
  templateUrl: './inspections-form.component.html',
  styleUrls: ['./inspections-form.component.scss'],
})
export class InspectionsFormComponent implements OnInit, OnDestroy {
  inspectionFormKeys = {
    port: 'Port',
    oilMajor: 'Oil Major',
    dateOfInspection: 'Date of Inspection',

    sireReportNo: 'SIRE Report No.',
    inspector: 'Inspector',
    reportRecd: 'Report Recd',

    inspStatus: 'Insp. Status',
    invoiceRecd: 'Invoice Recd',
    commentsUpload: 'Comments Upload',
    reminder: 'Reminder',

    positiveScreening: 'Positive Screening',
    invoicePaid: 'Invoice Paid',
  };
  inspectionForm = new FormGroup({});
  inspectionModel = {};
  inspectionOptions: FormlyFormOptions = {
    formState: {
      users: this.userService.getUsers(),
    },
  };
  inspectionFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          fieldGroupClassName: 'vertical-form-group',
          fieldGroup: [
            {
              className: 'form-group-title',
              template: '<div>Basic</div>',
            },
            {
              key: this.inspectionFormKeys.port,
              type: 'input',
              className: '',
              templateOptions: {
                label: this.inspectionFormKeys.port,
                placeholder: 'Enter port number',
                required: true,
              },
            },
            {
              key: this.inspectionFormKeys.sireReportNo,
              type: 'input',
              className: '',
              templateOptions: {
                label: this.inspectionFormKeys.sireReportNo,
                placeholder: 'Enter SIRE report number',
                required: false,
              },
            },
          ],
        },

        {
          className: 'flex-1',
          fieldGroupClassName: 'vertical-form-group',
          fieldGroup: [
            {
              className: 'form-group-title',
              template: '<div>Inspecting Party</div>',
            },
            {
              key: this.inspectionFormKeys.oilMajor,
              type: 'input',
              className: '',
              templateOptions: {
                label: this.inspectionFormKeys.oilMajor,
                placeholder: 'Enter oil major number',
                required: true,
              },
            },
            {
              key: this.inspectionFormKeys.inspector,
              type: 'input',
              className: '',
              templateOptions: {
                label: this.inspectionFormKeys.inspector,
                placeholder: 'Enter inspector name',
                required: false,
              },
            },
          ],
        },

        {
          className: 'flex-1',
          fieldGroupClassName: 'vertical-form-group',
          fieldGroup: [
            {
              className: 'form-group-title',
              template: '<div>Date</div>',
            },
            {
              className: 'flex-1',
              type: 'matdatepicker',
              key: this.inspectionFormKeys.dateOfInspection,
              defaultValue: '',
              templateOptions: {
                label: this.inspectionFormKeys.dateOfInspection,
                appearance: 'outline',
                placeholder: 'Select date of inspection',
                datepickerOptions: {
                  // max: new Date(),
                },
              },
            },
            {
              key: this.inspectionFormKeys.reportRecd,
              type: 'input',
              className: '',
              templateOptions: {
                label: this.inspectionFormKeys.reportRecd,
                placeholder: 'Enter report received',
                required: false,
              },
            },
            {
              key: this.inspectionFormKeys.commentsUpload,
              type: 'input',
              className: '',
              templateOptions: {
                label: this.inspectionFormKeys.commentsUpload,
                placeholder: 'Enter comments upload',
                required: false,
              },
            },
            {
              key: this.inspectionFormKeys.reminder,
              type: 'input',
              className: '',
              templateOptions: {
                label: this.inspectionFormKeys.reminder,
                placeholder: 'Enter reminder',
                required: false,
              },
            },
          ],
        },
      ],
    },

    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          fieldGroupClassName: 'vertical-form-group',
          fieldGroup: [
            {
              className: 'form-group-title',
              template: '<div>Progress & Result</div>',
            },
            {
              key: this.inspectionFormKeys.inspStatus,
              type: 'input',
              className: '',
              templateOptions: {
                label: this.inspectionFormKeys.inspStatus,
                placeholder: 'Enter inspection status',
                required: false,
              },
            },
            {
              key: this.inspectionFormKeys.positiveScreening,
              type: 'select',
              defaultValue: '',
              templateOptions: {
                label: this.inspectionFormKeys.positiveScreening,
                placeholder: 'Select positive screening',
                appearance: 'outline',
                required: false,
                options: [],
                valueProp: 'positiveScreening',
                labelProp: 'positiveScreening',
              },
            },
          ],
        },

        {
          className: 'flex-1',
          fieldGroupClassName: 'vertical-form-group',
          fieldGroup: [
            {
              className: 'form-group-title',
              template: '<div>Invoice</div>',
            },
            {
              key: this.inspectionFormKeys.invoiceRecd,
              type: 'input',
              className: '',
              templateOptions: {
                label: this.inspectionFormKeys.invoiceRecd,
                placeholder: 'Enter invoice received',
                required: false,
              },
            },
            {
              key: this.inspectionFormKeys.invoicePaid,
              type: 'input',
              className: '',
              templateOptions: {
                label: this.inspectionFormKeys.invoicePaid,
                placeholder: 'Enter invoice paid',
                required: false,
              },
            },
          ],
        },
      ],
    },
  ];

  observationKey = {
    viqRef: 'VIQ Ref',
    initialRisk: 'initial Risk',
    finalRisk: 'final Risk',

    viqQuestions: 'VIQ Questions',
    observation: 'Observation',
    operatorComments: 'Operator Comments',

    dueDate: 'Due Date',
    dateClosed: 'Date Closed',

    status: 'Status',

    responsibility: 'Responsibility',
  };
  observationForm = new FormGroup({});
  observationModel = {};
  observationOptions: FormlyFormOptions = {};
  observationFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.observationKey.viqRef,
          type: 'select',
          defaultValue: '',
          templateOptions: {
            label: this.observationKey.viqRef,
            placeholder: 'Select VIQ Ref',
            appearance: 'outline',
            required: false,
            options: [],
            valueProp: 'positiveScreening',
            labelProp: 'positiveScreening',
          },
        },
        {
          key: this.observationKey.initialRisk,
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: this.observationKey.initialRisk,
            placeholder: 'Enter initial risk',
            required: false,
          },
        },
        {
          key: this.observationKey.finalRisk,
          type: 'select',
          defaultValue: '',
          templateOptions: {
            label: this.observationKey.finalRisk,
            placeholder: 'Select final risk',
            appearance: 'outline',
            required: false,
            options: [],
            valueProp: 'positiveScreening',
            labelProp: 'positiveScreening',
          },
        },
      ],
    },

    {
      fieldGroup: [
        {
          key: this.observationKey.viqQuestions,
          type: 'textarea',
          templateOptions: {
            appearance: 'outline',
            label: this.observationKey.viqQuestions,
            placeholder: '',
            rows: 5,
          },
        },

        {
          key: this.observationKey.observation,
          type: 'textarea',
          templateOptions: {
            appearance: 'outline',
            label: this.observationKey.observation,
            placeholder: '',
            rows: 5,
          },
        },

        {
          key: this.observationKey.operatorComments,
          type: 'textarea',
          templateOptions: {
            appearance: 'outline',
            label: this.observationKey.operatorComments,
            placeholder: '',
            rows: 5,
          },
        },
      ],
    },

    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'matdatepicker',
          key: this.observationKey.dueDate,
          defaultValue: '',
          templateOptions: {
            label: this.observationKey.dueDate,
            appearance: 'outline',
            placeholder: 'Select due date',
            datepickerOptions: {
              // max: new Date(),
            },
          },
        },
        {
          key: this.observationKey.dateClosed,
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: this.observationKey.dateClosed,
            placeholder: 'Enter date closed',
            required: false,
          },
        },
        {
          key: this.observationKey.status,
          type: 'radio',
          templateOptions: {
            className: 'flex-1',
            required: true,
            options: [
              {
                value: '1',
                label: 'Open',
              },
              {
                value: '2',
                label: 'Close',
              },
              {
                value: '3',
                label: 'Dry Dock',
              },
            ],
          },
        },
      ],
    },
  ];

  communicationFileKey = {
    communication: 'Communication',
    status: 'Status',
  };
  communicationForm = new FormGroup({});
  communicationModel = {};
  communicationOptions: FormlyFormOptions = {};
  communicationFields: FormlyFieldConfig[] = [
    {
      key: this.communicationFileKey.communication,
      type: 'input',
      className: '',
      templateOptions: {
        label: this.communicationFileKey.communication,
        placeholder: 'Enter communication details',
        required: true,
      },
    },
    {
      template: '<hr />',
    },
    {
      key: this.communicationFileKey.status,
      type: 'select',
      defaultValue: '',
      className: 'flex-1',
      templateOptions: {
        label: this.communicationFileKey.status,
        placeholder: 'Select status',
        appearance: 'outline',
        required: false,
        options: [],
        valueProp: 'positiveScreening',
        labelProp: 'positiveScreening',
      },
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      shortName: 'Inspection',
      title: 'Inspection Details',
      subTitle: 'Enter Inspection Data',
      config: {
        model: this.inspectionModel,
        fields: this.inspectionFields,
        options: this.inspectionOptions,
        form: this.inspectionForm,
      },
      discard: true,
      onDiscard: () => this.inspectionForm.reset(),
    },
    {
      shortName: 'Observation',
      title: 'Observation Details',
      subTitle: 'Enter Observation Data',
      config: {
        model: this.observationModel,
        fields: this.observationFields,
        options: this.observationOptions,
        form: this.observationForm,
      },
      discard: true,
      onDiscard: () => this.observationForm.reset(),
    },
    {
      shortName: 'Communication',
      title: 'Communication Details',
      subTitle: 'Enter Communication Data',
      config: {
        model: this.communicationModel,
        fields: this.communicationFields,
        options: this.communicationOptions,
        form: this.communicationForm,
      },
      discard: true,
      onDiscard: () => this.communicationForm.reset(),
    },
  ];

  constructor(private userService: UserService) {}

  ngOnInit() {}

  ngOnDestroy() {}
}
