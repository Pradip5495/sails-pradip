import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InspectionSummaryComponent } from './component/inspection-summary/inspection-summary.component';
import { InspectionsComponent } from './inspections.component';
import { InspectionsTableComponent } from './component/inspections-table/inspections-table.component';
import { InspectionsFormComponent } from './component/inspections-form/inspections-form.component';

const inspectionsRoutes: Routes = [
  {
    path: '',
    component: InspectionsComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      {
        path: 'list',
        component: InspectionsTableComponent,
      },
      {
        path: 'summary',
        component: InspectionSummaryComponent,
      },
      {
        path: 'create',
        component: InspectionsFormComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(inspectionsRoutes)],
  exports: [RouterModule],
})
export class InspectionsRoutingModule {}
