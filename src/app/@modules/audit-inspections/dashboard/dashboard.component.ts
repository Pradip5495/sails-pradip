import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'audit-dashboard',
  template: ` <router-outlet></router-outlet> `,
})
export class AuditDashboardComponent implements OnInit {
  constructor(private pageTitle: Title) {}

  ngOnInit() {
    this.pageTitle.setTitle('Audit Stastics / Dashboard');
  }
}
