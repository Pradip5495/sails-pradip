import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'audit-summary-dashboard',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuditSummaryDashboardComponent {
  ids = ['gauge-1', 'gauge-2', 'gauge-3', 'gauge-4', 'gauge-5', 'gauge-6'];
  constructor() {}
}
