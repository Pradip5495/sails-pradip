export default (props: any = {}) => {
  return {
    chart: {
      type: 'gauge',
      plotBackgroundColor: null,
      plotBackgroundImage: null,
      plotBorderWidth: 0,
      plotShadow: false,
      marginBottom: 0,
      spacingBottom: 0,
    },

    credits: {
      enabled: false,
    },

    title: {
      text: props.title,
      margin: -50,
      y: 40,
    },

    pane: {
      startAngle: -90,
      endAngle: 90,
      background: [
        {
          backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
            stops: [
              [0, '#FFF'],
              [1, '#333'],
            ],
          },
          borderWidth: 0,
          outerRadius: '109%',
        },
        {
          backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
            stops: [
              [0, '#333'],
              [1, '#FFF'],
            ],
          },
          borderWidth: 0,
          outerRadius: '107%',
        },
        {
          // default background
        },
        {
          backgroundColor: '#DDD',
          borderWidth: 0,
          outerRadius: '105%',
          innerRadius: '103%',
          shape: 'arc',

          // innerRadius: '50%',
          //   outerRadius: '100%',
        },
      ],
    },

    // the value axis
    yAxis: {
      min: 0,
      max: 200,

      innerRadius: '0%',

      minorTickInterval: 'auto',
      minorTickWidth: 1,
      minorTickLength: 10,
      minorTickPosition: 'inside',
      minorTickColor: '#666',

      tickPixelInterval: 30,
      tickWidth: 2,
      tickPosition: 'inside',
      tickLength: 10,
      tickColor: '#666',
      labels: {
        step: 2,
        rotation: 'auto',
      },
      title: {
        text: 'km/h',
      },
      plotBands: [
        {
          from: 0,
          to: 120,
          color: '#55BF3B', // green
        },
        {
          from: 120,
          to: 160,
          color: '#DDDF0D', // yellow
        },
        {
          from: 160,
          to: 200,
          color: '#DF5353', // red
        },
      ],
    },

    series: [
      {
        name: 'Speed',
        data: [props.data],
        tooltip: {
          valueSuffix: ' km/h',
        },
      },
    ],
  } as any;
};
