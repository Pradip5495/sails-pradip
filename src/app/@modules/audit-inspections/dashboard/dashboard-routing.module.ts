import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditSummaryDashboardComponent } from './component/summary/summary.component';
import { AuditDashboardComponent } from './dashboard.component';

const dashboardRoutes: Routes = [
  {
    path: '',
    component: AuditDashboardComponent,
    children: [
      { path: '', redirectTo: 'summary', pathMatch: 'full' },
      {
        path: 'summary',
        component: AuditSummaryDashboardComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dashboardRoutes)],
  exports: [RouterModule],
})
export class AuditDashboardRoutingModule {}
