import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditSummaryDashboardComponent } from './component/summary/summary.component';
import { AuditDashboardComponent } from './dashboard.component';
import { AuditDashboardRoutingModule } from './dashboard-routing.module';
import { RouterModule } from '@angular/router';
import { AuditHighchartsService } from './highcharts.service';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { ChartsModule } from '@shared/component';

@NgModule({
  declarations: [AuditSummaryDashboardComponent, AuditDashboardComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    MatCardModule,
    FlexLayoutModule,
    AuditDashboardRoutingModule,
    ChartsModule,
  ],
  providers: [AuditHighchartsService],
})
export class AuditDashboardModule {}
