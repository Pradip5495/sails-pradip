import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@app/@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@formly';
import { PlanningApprovalsRoutingModule } from './planning-approvals-routing.module';
import { PlanningApprovalsComponent } from './planning-approvals.component';
import { AuditPlanComponent } from './components/audit-plan/audit-plan.component';
import { CustomAgGridCellComponent, CustomUiAgGridCellComponent } from './shared';
import { CalenderComponent } from './components/calender/calender.component';
import { AddPlanComponent } from './shared/add-plan/add-plan.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { RemindersComponent } from './components/reminders/reminders.component';
import { AddReminderComponent } from './shared/add-reminder/add-reminder.component';
import { RemindersAgGridCellComponent } from './shared/reminders-ag-grid-cell/reminders-ag-grid-cell.component';
import { CalenderViewComponent } from './shared/calender-view/calender-view.component';
import { RemindersVesselsAgGridCellComponent } from './shared/reminders-vessels-ag-grid-cell/reminders-vessels-ag-grid-cell.component';

FullCalendarModule.registerPlugins([
  // register FullCalendar plugins
  dayGridPlugin,
]);
@NgModule({
  declarations: [
    PlanningApprovalsComponent,
    AuditPlanComponent,
    CustomAgGridCellComponent,
    CustomUiAgGridCellComponent,
    CalenderComponent,
    AddPlanComponent,
    TimelineComponent,
    RemindersComponent,
    AddReminderComponent,
    RemindersAgGridCellComponent,
    CalenderViewComponent,
    RemindersVesselsAgGridCellComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormlyModule,
    NgApexchartsModule,
    PlanningApprovalsRoutingModule,
    FullCalendarModule,
  ],
})
export class PlanningApprovalsModule {}
