import { Injectable } from '@angular/core';
import { HttpDispatcherService } from '@core';
import { PlanningModel } from '@shared/models/planning-model.model';

const routes = {
  getAll: (filters?: Map<string, string>) => `/inspection/planning?${filters.toSanitizedURLFilters()}`,
  save: () => `/inspection/planning`,
  // getCheckList: (inspectionId: string, chapterId: string) => `/preparation/checklist/${inspectionId}/${chapterId}`,
  // saveChecklist: () => `/preparation/checklist`,
  // deleteChecklistAttachment: (id: string) => `/preparation/checklist/attachment/${id}`,
  // approveChecklist: () => `/preparation/checklist/approve`,
  // getHistory: (checklistid: string) => `/preparation/checklist/history/${checklistid}`,
};

@Injectable({
  providedIn: 'root',
})
export class PlanningService {
  constructor(private httpDispatch: HttpDispatcherService) {}

  save(planningData: PlanningModel) {
    return this.httpDispatch.post(routes.save(), planningData);
  }

  getPlanningList(filters?: Map<string, string>) {
    return this.httpDispatch.get(routes.getAll(filters), { acceptNullResponse: true });
  }
}
