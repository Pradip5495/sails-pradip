import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanningApprovalsComponent } from './planning-approvals.component';
import { AuditPlanComponent } from './components/audit-plan/audit-plan.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { CalenderComponent } from './components/calender/calender.component';
import { RemindersComponent } from './components/reminders/reminders.component';

const planningApprovalsRoutes: Routes = [
  {
    path: '',
    component: PlanningApprovalsComponent,
    children: [
      { path: '', redirectTo: 'plan', pathMatch: 'full' },
      { path: 'plan', component: AuditPlanComponent },
      { path: 'timeline', component: TimelineComponent },
      { path: 'calender', component: CalenderComponent },
      { path: 'reminder', component: RemindersComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(planningApprovalsRoutes)],
  exports: [RouterModule],
})
export class PlanningApprovalsRoutingModule {}
