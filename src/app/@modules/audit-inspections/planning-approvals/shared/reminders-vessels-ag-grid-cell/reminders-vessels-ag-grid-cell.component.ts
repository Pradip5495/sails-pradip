import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'app-reminders-vessels-ag-grid-cell',
  templateUrl: './reminders-vessels-ag-grid-cell.component.html',
  styleUrls: ['./reminders-vessels-ag-grid-cell.component.css'],
})
export class RemindersVesselsAgGridCellComponent implements ICellRendererAngularComp {
  public params: any;
  public reminderData: any;

  agInit(params: any): void {
    this.params = params;
    this.reminderData = params.data;
  }

  refresh(): boolean {
    return false;
  }

  onCreate(params: any, reminderData: any) {
    if (this.params.onCreate) {
      reminderData = {
        ...reminderData,
      };
      this.params.onCreate(params, reminderData);
    }
  }
}
