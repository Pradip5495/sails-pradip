import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemindersVesselsAgGridCellComponent } from './reminders-vessels-ag-grid-cell.component';

describe('RemindersVesselsAgGridCellComponent', () => {
  let component: RemindersVesselsAgGridCellComponent;
  let fixture: ComponentFixture<RemindersVesselsAgGridCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RemindersVesselsAgGridCellComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemindersVesselsAgGridCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
