import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomUiAgGridCellComponent } from './custom-ui-ag-grid-cell.component';

describe('CustomUiAgGridCellComponent', () => {
  let component: CustomUiAgGridCellComponent;
  let fixture: ComponentFixture<CustomUiAgGridCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomUiAgGridCellComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomUiAgGridCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
