import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'app-custom-ui-ag-grid-cell',
  template: ` <span> {{ params.valueFormatted.dateOfInspection | localTimezone }} </span>
    <br />
    <span class="danger-text mr-10"> {{ params.valueFormatted.oilMajor || '-' }} </span>
    <span class="bold-text"> {{ params.valueFormatted.port || '-' }} </span>`,
  styleUrls: ['./custom-ui-ag-grid-cell.component.scss'],
})
export class CustomUiAgGridCellComponent implements ICellRendererAngularComp, OnInit {
  params: any;
  label: any;

  ngOnInit(): void {}

  agInit(params: any) {
    this.params = params;
    this.label = this.params.label || null;
  }

  refresh(params: any): boolean {
    return true;
  }
}
