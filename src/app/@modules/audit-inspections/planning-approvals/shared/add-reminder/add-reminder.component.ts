import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { ICustomFormlyConfig, IJson } from '@types';
import { ReminderModel } from '@shared/models';
import { OilMajorService, ReminderService, VesselService } from '@shared/service';
import { NotificationService } from '@shared/common-service';
import * as moment from 'moment';
import { isEmpty } from 'lodash';

@Component({
  selector: 'app-add-reminder',
  templateUrl: './add-reminder.component.html',
  styleUrls: ['./add-reminder.component.scss', '../add-plan/add-plan.component.scss'],
})
export class AddReminderComponent implements OnInit, OnChanges {
  @Output() closeDialog = new EventEmitter<any>();

  @Input() reminder: any;
  @Input() isEdit: boolean;

  inspectionType: any;
  vesselsList: any = [];
  vesselId: string = null;
  oilMajorId: string = null;
  previousInspectionDate: string = null;
  reminderId: string;

  //#region FormlyForm Tegion Starts
  reminderFormKeys = {
    oilMajorId: 'oilMajorId',
    reminder: 'reminder',
    recurring: 'recurring',
  };
  reminderForm = new FormGroup({});
  reminderModel: IJson = {};
  reminderOptions: FormlyFormOptions = {};
  reminderFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: this.reminderFormKeys.oilMajorId,
          type: 'select',
          defaultValue: '',
          templateOptions: {
            label: 'OIL MAJOR',
            placeholder: 'Select',
            appearance: 'outline',
            required: true,
            options: this.oilMajorService.getOilMajors(),
            valueProp: 'oilMajorId',
            labelProp: 'fullName',
            change: (field: FormlyFieldConfig, event: any) => {
              this.oilMajorId = event.value;

              if (this.vesselId != null && this.oilMajorId != null) {
                this.getPreviousInspectionData(this.vesselId, this.oilMajorId);
              }
            },
          },
        },
        {
          className: 'flex-1 margin-right',
          key: this.reminderFormKeys.reminder,
          type: 'matdatepicker',
          defaultValue: '',
          templateOptions: {
            label: 'REMINDER',
            placeholder: 'REMINDER',
            appearance: 'outline',
            required: true,
          },
        },
        {
          className: 'flex-1',
          key: this.reminderFormKeys.recurring,
          type: 'select',
          defaultValue: '',
          templateOptions: {
            label: 'RECURRING',
            placeholder: 'Select Months',
            appearance: 'outline',
            required: false,
            options: [
              { value: null, month: 'NON RECURRING' },
              { value: 1, month: 1 },
              { value: 2, month: 2 },
              { value: 3, month: 3 },
              { value: 4, month: 4 },
              { value: 5, month: 5 },
              { value: 6, month: 6 },
              { value: 7, month: 7 },
              { value: 8, month: 8 },
              { value: 9, month: 9 },
              { value: 10, month: 10 },
              { value: 11, month: 11 },
              { value: 12, month: 12 },
            ],
            valueProp: 'value',
            labelProp: 'month',
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig = {
    config: {
      model: this.reminderModel,
      fields: this.reminderFields,
      options: this.reminderOptions,
      form: this.reminderForm,
    },
    onSave: this.onSubmit.bind(this),
    onDiscard: () => this.closeDialog.emit(null),
  };
  //#endregion FormlyForm Region Ends

  constructor(
    private readonly reminderService: ReminderService,
    private readonly notificationService: NotificationService,
    private readonly oilMajorService: OilMajorService,
    private readonly vesselService: VesselService,
    private formBuilder: FormBuilder
  ) {
    this.reminderForm = this.formBuilder.group({
      vesselId: [''],
      inspectionType: [''],
    });
  }

  ngOnInit(): void {
    this.getVessels();
    this.previousInspectionDate = null;
  }

  ngOnChanges(changes: SimpleChanges) {
    // Check if form is for edit/update mode
    if (changes.isEdit) {
      this.isEdit = changes.isEdit.currentValue;
    }

    // To get reminder Id from @Input
    // if reminder exist, consider form is for updating data
    if (changes.reminder && changes.reminder.currentValue) {
      if (this.isEdit) {
        this.editReminder(this.reminder);
      }
    }

    if (!this.isEdit && this.reminder) {
      this.createReminderWithVessel(this.reminder);
    }
  }

  // Get previous/last inspection date as per the vessel and oil major
  selectionChangedVessel(event: any) {
    this.vesselId = event.value;

    if (this.vesselId != null && this.oilMajorId != null) {
      this.getPreviousInspectionData(this.vesselId, this.oilMajorId);
    }
  }

  // Create/Update reminder
  onSubmit(reminder: ReminderModel) {
    // Check if dialog is in edit mode--
    // if yes, call update method
    // else continue with create new
    if (this.isEdit) {
      this.updateReminder(reminder);
      return true;
    }

    const reminderData = {
      ...reminder,
      ...this.reminderForm.value,
    };

    if (!reminderData.recurring) {
      delete reminderData.recurring;
    }

    reminderData.inspectionType = this.getInspectionTypeValue(reminderData);

    this.reminderService.createReminder(reminderData).subscribe(
      (data) => {
        this.closeDialog.emit(null);
        this.notificationService.showSuccess(data.message, 'Success!');
      },
      (error) => {
        this.notificationService.showError(error, 'Error!');
      }
    );
  }

  // Open dialog with prefilled vesselId to create
  private createReminderWithVessel(reminderData: IJson) {
    this.reminderForm.patchValue({
      vesselId: reminderData.vesselId,
    });
  }

  // Open dialog with prefilled data to update
  private editReminder(reminderData: any) {
    this.reminderForm.patchValue({
      vesselId: reminderData.vesselId,
      inspectionType: reminderData.inspectionType === 'NON-SIRE',
    });

    this.formlyConfig.config.form.patchValue({
      lastInspected: reminderData.lastInspected,
      oilMajor: reminderData.oilMajor,
      oilMajorId: reminderData.oilMajorId,
      recurring: reminderData.recurring,
      reminder: moment.utc(reminderData.reminder).local().toDate(),
      reminderId: reminderData.reminderId,
    });

    this.reminderId = reminderData.reminderId;
    // this.isEdit = true;
  }

  // Post updated data to API
  private updateReminder(reminderData: ReminderModel) {
    if (!this.reminderId) {
      this.notificationService.showError('Reminder not found', 'Error!');
      return false;
    }

    reminderData.inspectionType = this.getInspectionTypeValue(reminderData);

    this.reminderService.updateReminder(this.reminderId, reminderData).subscribe(
      (data) => {
        this.closeDialog.emit(null);
        this.notificationService.showSuccess(data.message, 'Success!');
      },
      (error) => {
        this.notificationService.showError(error, 'Error!');
      }
    );

    this.isEdit = false;
  }

  // Get list of available Vessels
  private getVessels() {
    this.vesselService.getVessels().subscribe(
      (response) => {
        this.vesselsList = response;
      },
      (error) => {
        this.notificationService.showError(error, 'Error!');
      }
    );
  }

  // Get previous/last inspection date
  private getPreviousInspectionData(vesselId: string, oilMajorId: string) {
    // Check if vesselId (string) is available
    if (isEmpty(vesselId)) {
      return false;
    }

    this.reminderService.getPreviousInspection(vesselId, oilMajorId).subscribe(
      (response: any) => {
        this.previousInspectionDate = response[0]?.lastInspectionDate;
      },
      (error) => {
        this.notificationService.showError(error, 'Error!');
      }
    );
  }

  // Set Inspection Type Value (String)
  private getInspectionTypeValue(data: any) {
    if (data.inspectionType) {
      return 'NON-SIRE';
    } else {
      return 'SIRE';
    }
  }
}
