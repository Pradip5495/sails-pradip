import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomAgGridCellComponent } from './custom-ag-grid-cell.component';

describe('CustomAgGridCellComponent', () => {
  let component: CustomAgGridCellComponent;
  let fixture: ComponentFixture<CustomAgGridCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomAgGridCellComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomAgGridCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
