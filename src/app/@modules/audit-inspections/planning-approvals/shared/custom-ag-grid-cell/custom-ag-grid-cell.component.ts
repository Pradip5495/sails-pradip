import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-custom-ag-grid-cell',
  templateUrl: './custom-ag-grid-cell.component.html',
  styleUrls: ['./custom-ag-grid-cell.component.scss'],
})
export class CustomAgGridCellComponent implements ICellRendererAngularComp, OnInit {
  params: any;
  label: any;
  interval = '6';
  inspectionDate: any;
  status: string;

  ngOnInit(): void {}

  agInit(params: any) {
    this.params = params;
    this.label = this.params.label || null;

    if (this.params.valueFormatted.interval) {
      this.interval = '' + this.params.valueFormatted.interval / 30;
    }

    this.status = params.data.status;

    this.inspectionDate = this.params.valueFormatted.dateOfInspection
      ? moment(this.params.valueFormatted.dateOfInspection).add(this.params.valueFormatted.interval, 'days')
      : '';

    if (this.inspectionDate) {
      this.status = this.getStatus();
    }
  }

  getStatus() {
    const dateDiff = moment(this.inspectionDate).diff(moment(new Date()), 'days');

    if (dateDiff > 60) {
      return 'green';
    }

    if (dateDiff > 30) {
      return 'orange';
    }

    if (dateDiff > 0) {
      return 'red';
    }
  }

  refresh(params: any): boolean {
    return true;
  }
}
