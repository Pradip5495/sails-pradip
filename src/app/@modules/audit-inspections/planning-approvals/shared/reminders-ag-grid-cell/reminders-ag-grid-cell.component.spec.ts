import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemindersAgGridCellComponent } from './reminders-ag-grid-cell.component';

describe('RemindersAgGridCellComponent', () => {
  let component: RemindersAgGridCellComponent;
  let fixture: ComponentFixture<RemindersAgGridCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RemindersAgGridCellComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemindersAgGridCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
