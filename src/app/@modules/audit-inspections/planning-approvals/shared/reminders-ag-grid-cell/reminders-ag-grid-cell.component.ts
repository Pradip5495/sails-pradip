import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-reminders-grid-cell',
  templateUrl: './reminders-ag-grid-cell.component.html',
  styleUrls: ['./reminders-ag-grid-cell.component.scss'],
})
export class RemindersAgGridCellComponent implements ICellRendererAngularComp {
  public params: any;
  public inspectionData: any;

  dateFormat = 'YYYY-DD-MM';
  today = moment().format(this.dateFormat);

  agInit(params: any): void {
    this.params = params;
    this.inspectionData = params.data.inspectionType;
  }

  refresh(): boolean {
    return false;
  }

  getIsoDate(date: string) {
    return moment.utc(date).local().format(this.dateFormat);
  }

  onEdit(params: any, reminderData: any, inspectionType: string) {
    if (this.params.onEdit) {
      reminderData = {
        ...reminderData,
        inspectionType,
      };
      this.params.onEdit(params, reminderData);
    }
  }

  onDelete(params: any, reminderData: any, inspectionType: string) {
    if (this.params.onDelete) {
      reminderData = {
        ...reminderData,
        inspectionType,
      };
      this.params.onDelete(params, reminderData);
    }
  }
}
