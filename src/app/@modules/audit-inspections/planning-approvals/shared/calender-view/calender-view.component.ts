import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { CalendarOptions, FullCalendarComponent } from '@fullcalendar/angular';
import timeGridPlugin from '@fullcalendar/timegrid';
import * as moment from 'moment';
@Component({
  selector: 'app-calender-view',
  templateUrl: './calender-view.component.html',
  styleUrls: ['./calender-view.component.scss'],
})
export class CalenderViewComponent implements OnInit {
  @ViewChild('calender2') calendarComponent2: FullCalendarComponent;
  @ViewChild('calender3') calendarComponent3: FullCalendarComponent;

  @Input() id: any;

  displayCalanderDialog = false;
  calenderApi: any;
  nextMonth: any;
  prevMonth: any;
  showCalender = false;
  header_background: any;
  tableData = [
    {
      oilMajor: 'Shell',
      port: 'Mumbai',
    },
  ];
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    plugins: [timeGridPlugin],
    headerToolbar: false,
    events: [],
    eventClick: (info: any) => {
      this.displayCalanderDialog = false;
      setTimeout(() => {
        this.header_background = info.event.backgroundColor;
        this.displayCalanderDialog = true;
      }, 10);
    },
  };
  constructor() {}

  ngOnInit(): void {
    setTimeout(() => {
      if (this.id === 'cal2') {
        document.getElementById('next').click();
      } else if (this.id === 'cal1') {
        document.getElementById('previous').click();
      }
    }, 100);
  }

  getPreviousMonth() {
    this.calendarComponent3.getApi().prev();
    const prev = moment(this.calendarComponent3.getApi().view.currentStart);
    this.prevMonth = prev.format('MMMM YYYY');
  }

  getNextMonth() {
    this.calendarComponent2.getApi().next();
    const start = moment(this.calendarComponent2.getApi().view.currentStart);
    this.nextMonth = start.format('MMMM YYYY');
  }

  callNextMonthFromOther() {
    this.calendarComponent3.getApi().next();
    const start = moment(this.calendarComponent3.getApi().view.currentStart);
    this.prevMonth = start.format('MMMM YYYY');
  }

  AfterViewInit(): void {
    this.calenderApi = this.calendarComponent2.getApi();
    this.calendarComponent3.getApi();
  }
}
