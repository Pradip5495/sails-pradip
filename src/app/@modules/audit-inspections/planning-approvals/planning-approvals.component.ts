import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-planning-approvals',
  templateUrl: './planning-approvals.component.html',
  styleUrls: ['./planning-approvals.component.scss'],
})
export class PlanningApprovalsComponent implements OnInit {
  constructor(private pageTitle: Title) {}

  ngOnInit() {
    this.pageTitle.setTitle('Audit Planning');
  }
}
