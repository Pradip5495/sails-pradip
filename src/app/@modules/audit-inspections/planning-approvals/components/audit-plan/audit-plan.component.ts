import { ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { CustomizedCellComponent } from '@shared';
import { untilDestroyed } from '@core';
import { CustomAgGridCellComponent, CustomUiAgGridCellComponent } from '../../shared';
import { AgCellButtonComponent } from '@shared/component/ag-cell-button/ag-cell-button.component';
import { CustomHeaderSelectComponent } from '@shared/component/custom-header-select/custom-header-select.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { VesselModel } from '@shared/models';
import { ICustomFormlyConfig, IJson } from '@types';
import { MatSelectChange } from '@angular/material/select';
import { catchError } from 'rxjs/operators';
import { CustomFormlyFieldConfig } from '@formly/fields/material/custom-field-config';
import { VesselService } from '@shared/service';
import { AuditInspectionService } from '@modules/audit-inspections/audit-inspections.service';
import { NotificationService } from '@shared/common-service';
import { PlanningService } from '../../planning.service';
import { NearMissService } from '@modules/incident-investigation/near-miss/share/near-miss.service';
import { GridOptions } from '@ag-grid-community/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DateToLocalPipe } from '@shared/pipes/date-to-local.pipe';
import { ButtonCellRendererComponent } from '@shared/component';

@Component({
  selector: 'app-audit-plan',
  templateUrl: './audit-plan.component.html',
  styleUrls: ['./audit-plan.component.scss'],
  providers: [DateToLocalPipe],
})
export class AuditPlanComponent implements OnInit, OnDestroy {
  //#region Ag Grid Properties Start
  headerScaleValue = 180;
  $headerScaleValue: Subject<any> = new Subject<any>();

  columnDefs = [
    {
      headerName: '',
      headerClass: 'group-header-custom-light border-right-1px',
      children: [
        {
          headerName: 'VESSEL',
          field: 'vesselName',
          cellClass: 'bold-text',
          width: 120,
          filter: 'agTextColumnFilter',
          menuTabs: ['generalMenuTab', 'filterMenuTab'],
        },
      ],
    },
    {
      headerName: 'HISTORY',
      headerClass: 'group-header-custom-light border-right-1px',
      children: [
        {
          headerName: 'LAST',
          width: 160,
          cellClass: 'custom-line-height',
          cellRenderer: 'customHtml',
          suppressSizeToFit: true,
          valueFormatter: (params: IJson) => {
            return params.data.history.length > 0 ? params.data.history[0] : {};
          },
        },
        {
          headerName: '2nd LAST',
          field: 'eNoc',
          width: 150,
          cellClass: 'custom-line-height',
          cellRenderer: 'customHtml',
          columnGroupShow: 'open',
          valueFormatter: (params: IJson) => {
            return params.data.history.length > 1 ? params.data.history[1] : {};
          },
        },
        {
          headerName: '3rd LAST',
          width: 150,
          cellClass: 'custom-line-height',
          cellRenderer: 'customHtml',
          columnGroupShow: 'open',
          valueFormatter: (params: IJson) => {
            return params.data.history.length > 2 ? params.data.history[2] : {};
          },
        },
      ],
    },
    {
      headerName: 'NEXT DUE INTERVAL',
      headerClass: 'group-header-custom-light border-right-1px',
      children: [
        {
          headerComponent: 'selectHeader',
          headerComponentParams: {
            defaultValue: this.headerScaleValue,
            onChange(params: any) {},
            onDefaultValue: function (params: any) {
              return this.$headerScaleValue;
            }.bind(this),
          },
          cellClass: 'custom-line-height-16px',
          headerName: '+12 Months',
          field: 'scaleData',
          width: 220,
          cellRenderer: 'scaleGridComponent',
          resizable: true,
          valueFormatter: (params: IJson) => {
            return { dateOfInspection: params.data.dateOfInspection, interval: params.data.interval };
          },
        },
      ],
    },
    {
      headerName: 'PLANNING',
      headerClass: 'group-header-custom-light border-right-1px',
      children: [
        {
          headerName: 'PORT',
          field: 'portName',
          cellClass: 'bold-text',
          width: 110,
          filter: 'agNumberColumnFilter',
          menuTabs: ['generalMenuTab', 'filterMenuTab'],
        },
        {
          headerName: 'DATE',
          field: 'dateOfInspection',
          width: 110,
          filter: 'agNumberColumnFilter',
          menuTabs: ['generalMenuTab', 'filterMenuTab'],
          valueFormatter: (params: IJson) => {
            return this.localDateTimeZone.transform(params.value);
          },
        },
        {
          headerName: 'OIL MAJOR',
          field: 'oilMajorName',
          width: 148,
          filter: 'agNumberColumnFilter',
          menuTabs: ['generalMenuTab', 'filterMenuTab'],
        },
      ],
    },
    {
      headerName: 'REMARKS',
      headerClass: 'group-header-custom-light',
      children: [
        {
          headerName: 'COMMENTS',
          field: 'comments',
          width: 120,
          filter: 'agNumberColumnFilter',
          menuTabs: ['generalMenuTab', 'filterMenuTab'],
        },
        {
          headerName: '',
          // width: 50,
          // flex: 1,
          cellRenderer: 'customizedBtnCell',
          cellRendererParams: {
            onClick(params: any) {},
            onClickEdit: (params: any) => {
              if (params.rowData.inspectionMasterId) {
                this.inspectionMasterId = params.rowData.inspectionMasterId;
              }
              this.editPlanningData = params.rowData;
              this.onAddForm();
            },
            hideDetailsButton: true,
            hideDeleteIcon: true,
          },
          hide: true,
        },
        {
          headerName: '',
          // flex: 0.5,
          width: 100,
          cellRenderer: 'button',
          cellRendererParams: {
            label: 'INITIATE',
            className: 'prep-action-btn',
            svgIcon: 'return_arrow',
            onClick: (params: any) => {
              const { inspectionMasterId, oilMajorId, portId, vesselId, inspectionTypeId } = params.data;
              const preparationData = btoa(
                JSON.stringify({
                  inspectionMasterId,
                  oilMajorId,
                  portId,
                  vesselId,
                  inspectionTypeId,
                })
              );
              this.router.navigate([`../../preparation/list/${preparationData}`], { relativeTo: this.activatedRoute });
            },
          },
          cellStyle: (params: any) => {
            return { padding: '10px' };
          },
        },
      ],
    },
  ];

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    customHtml: CustomUiAgGridCellComponent,
    scaleGridComponent: CustomAgGridCellComponent,
    cellButton: AgCellButtonComponent,
    selectHeader: CustomHeaderSelectComponent,
    button: ButtonCellRendererComponent,
  };

  defaultColDef = {
    resizable: true,
    enableValue: true,
    autoHeight: true,
    sortable: true,
    filter: true,
  };

  paginationPageSize = 10;

  headerHeight = 45;

  rowData: any[] = [];

  gridApi: any;

  inspectionMasterId: string;
  editPlanningData: IJson;

  //#endregion AG Grid Properties End

  //#region Filter Form Declarations Region Start

  filterFormKeys = {
    port: 'port',
    date: 'date',
    auditorScope: 'auditorScope',
    auditType: 'auditType',
    remarks: 'remarks',
  };
  filterForm = new FormGroup({});
  filterModel = {};
  filterOptions: FormlyFormOptions = {};
  filterFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: this.filterFormKeys.port,
          type: 'autocomplete',
          defaultValue: '',
          templateOptions: {
            label: 'Vessel',
            placeholder: 'PORT',
            appearance: 'outline',
            required: true,
            change: (field: FormlyFieldConfig, event: any) => {},
            filter: (term: string) => of(term ? this.filterVesselsArray(term) : this.vesselsArray.slice()),
          },
        },
        {
          className: 'flex-1',
          key: this.filterFormKeys.date,
          type: 'matdatepicker',
          defaultValue: '',
          templateOptions: {
            label: 'PLANNED DATE',
            placeholder: 'DATE',
            appearance: 'outline',
            required: true,
            change: (field: FormlyFieldConfig, event: any) => {},
          },
        },
        {
          className: 'flex-1',
          key: this.filterFormKeys.auditType,
          type: 'input',
          defaultValue: '',
          templateOptions: {
            label: 'Planned Port',
            placeholder: 'Planned Port',
            appearance: 'outline',
            required: true,
            change: (field: FormlyFieldConfig, event: any) => {},
          },
        },
      ],
    },
  ];

  //#endregion Filter From Declarations Region Ends

  gridOptions: GridOptions;

  @Output() closeDialog = new EventEmitter<any>();

  displayDialog = false;

  vesselList: VesselModel[] = [];
  auditUtilityList: IJson[] = [];
  inspectionTypeList: IJson[] = [];
  portList: IJson[] = [];

  dynamicFormFields: IJson[] = [];
  commonFormFields = ['vesselId', 'inspectionTypeId', 'inspectionType', 'portId', 'dateOfInspection'];

  vesselsArray = ['Vessel 1', 'Vessel 2', 'Vessel 3'];

  planningForm = new FormGroup({
    vesselId: new FormControl('', [Validators.required]),
    inspectionTypeId: new FormControl('', [Validators.required]),
    inspectionType: new FormControl('', [Validators.required]),
  });

  planningModel = {};
  planningOptions: FormlyFormOptions = {};
  planningFields: FormlyFieldConfig[] = [];

  formlyConfig: ICustomFormlyConfig = {
    cache: false,
    id: 'audit-preparation',
    legacyForm: true,
    config: {
      model: this.planningModel,
      fields: this.planningFields,
      options: this.planningOptions,
      form: this.planningForm,
    },
    onSave: () => {},
    discard: true,
    onDiscard: () => this.closeDialog.emit(null),
  };

  loadingUtilityData = false;
  savingPlanningForm = false;

  selectedUtility: IJson;

  constructor(
    private vesselService: VesselService,
    private auditService: AuditInspectionService,
    private notifyService: NotificationService,
    private cdr: ChangeDetectorRef,
    private planningService: PlanningService,
    private nearMissService: NearMissService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private localDateTimeZone: DateToLocalPipe
  ) {
    this.gridOptions = {
      suppressContextMenu: true,
    };
  }

  filterVesselsArray(name: string) {
    return this.vesselsArray.filter((state: any) => state.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  ngOnInit(): void {
    this.setFormFields();
    this.addFormFields();
    this.getAuditUtilities();
  }

  onSireChanged(selectedUtility: any) {
    if (selectedUtility) {
      this.selectedUtility = selectedUtility;
      this.headerScaleValue = this.selectedUtility.interval;
      this.$headerScaleValue.next(this.headerScaleValue);
      this.getPlanningList(this.selectedUtility.inspectionTypeId);
    }
  }

  getAuditUtilities() {
    this.auditService
      .getUtility()
      .pipe(untilDestroyed(this))
      .subscribe((response: any) => {
        const utilities = response.filter((utility: any) => utility.planType !== 'surprise');
        this.auditUtilityList = utilities;
        this.selectedUtility = this.auditUtilityList[0];
        this.headerScaleValue = this.selectedUtility.interval;
        this.$headerScaleValue.next(this.headerScaleValue);
        this.getPlanningList(this.selectedUtility.inspectionTypeId);
      });
  }

  onAddForm() {
    this.loadingUtilityData = true;
    this.removeDynamicForms();
    this.addFormFields();
    this.vesselService
      .getVessels()
      .pipe(untilDestroyed(this))
      .subscribe((vessels: VesselModel[]) => {
        this.loadingUtilityData = false;
        this.displayDialog = true;
        this.vesselList = vessels;
        this.planningForm.patchValue({
          vesselId: this.vesselList[0].vesselId,
          inspectionTypeId: this.auditUtilityList[0].inspectionTypeId,
          inspectionType: this.auditUtilityList[0].type,
        });
        this.setSubInspectionTypeField(this.auditUtilityList[0].subInspectionType);
        this.addFormFields();
      });
  }

  //#region AG grid Functions Regions Start

  onSubmit(value: any) {}

  resetFilter() {}

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  }

  //#region AG grid Functions Regions Ends

  onInspectionTypeChanged(event: MatSelectChange) {
    const selectedInspectionType = this.auditUtilityList.filter((item) => item.inspectionTypeId === event.value);
    if (selectedInspectionType.length === 1) {
      this.removeUncommonFormGroupControls();
      this.removeDynamicForms();

      this.planningForm.patchValue({ inspectionType: selectedInspectionType[0].type });

      if (selectedInspectionType[0].subInspectionType && selectedInspectionType[0].subInspectionType.length > 0) {
        this.setSubInspectionTypeField(selectedInspectionType[0].subInspectionType);
      } else {
        this.removeSubInspectionType();
        this.setAttributeFormFields(selectedInspectionType[0].inspectionAttributesDetails);
      }
      this.addFormFields();
    }
  }

  setFormFields() {
    this.setPortFormField().subscribe((portList) => {
      this.dynamicFormFields.push({
        className: 'flex-1',
        key: 'portId',
        type: 'select',
        defaultValue: this.planningForm.value.portId || '',
        templateOptions: {
          label: 'PORT',
          placeholder: 'PORT',
          appearance: 'outline',
          options: portList,
          valueProp: 'portId',
          labelProp: 'name',
        },
      });
      this.setDateFormField();
      this.setIntervalFormField();
      this.setCommentFormField();
    });
  }

  setPortFormField() {
    return this.nearMissService.getPortList();
  }

  setDateFormField() {
    this.dynamicFormFields.push({
      className: 'flex-1',
      key: 'dateOfInspection',
      type: 'matdatepicker',
      defaultValue: this.planningForm.value.dateOfInspection || '',
      templateOptions: {
        label: 'Inspection Date',
        placeholder: 'Inspection Date',
        appearance: 'outline',
        datepickerOptions: {
          min: new Date(),
        },
      },
    });
  }

  setIntervalFormField() {
    this.dynamicFormFields.push({
      className: 'flex-1',
      key: 'interval',
      type: 'select',
      defaultValue: this.planningForm.value.interval || '',
      templateOptions: {
        label: 'Interval',
        placeholder: 'Interval',
        appearance: 'outline',
        options: [
          {
            value: 90,
            label: '+3',
          },
          {
            value: 180,
            label: '+6',
          },
          {
            value: 270,
            label: '+9',
          },
          {
            value: 360,
            label: '+12',
          },
        ],
        valueProp: 'value',
        labelProp: 'label',
      },
    });
  }

  setCommentFormField() {
    this.dynamicFormFields.push({
      className: 'flex-1',
      key: 'comments',
      type: 'textarea',
      defaultValue: this.planningForm.value.comments || '',
      templateOptions: {
        label: 'Comments',
        placeholder: 'Comments',
      },
    });
  }

  removeUncommonFormGroupControls() {
    for (const control of Object.keys(this.planningForm.controls)) {
      if (!this.commonFormFields.includes(control)) {
        this.planningForm.removeControl(control);
      }
    }
  }

  removeDynamicForms() {
    this.dynamicFormFields = this.dynamicFormFields.filter((formField) => formField.formKey !== 'dynamicForm');
  }

  setSubInspectionTypeField(items: IJson[] = []) {
    this.removeSubInspectionType();
    this.dynamicFormFields.push({
      className: 'flex-1',
      key: 'subInspectionTypeId',
      type: 'matselect',
      defaultValue: this.planningForm.value.subInspectionTypeId,
      templateOptions: {
        label: 'Sub Inspection Type',
        placeholder: 'Sub Inspection Type',
        appearance: 'outline',
        required: true,
        options: items,
        valueProp: 'inspectionTypeId',
        labelProp: 'type',
        onChange: (field: FormlyFieldConfig, event: IJson, item: IJson) => {
          this.planningForm.patchValue({
            inspectionType: item.type,
            subInspectionTypeId: item.inspectionTypeId,
          });
          this.setAttributeFormFields(item.inspectionAttributesDetails);
        },
      },
    });
  }

  setAttributeFormFields(attrArray: IJson[]) {
    this.dynamicFormFields = this.dynamicFormFields.filter((field) => field.formKey !== 'dynamicForm');

    const dataSourceCalls: Observable<any>[] = [];
    const dataSourceItems: string[] = [];
    attrArray.forEach((attr: IJson) => {
      dataSourceCalls.push(this.getDataSourceData(attr.inspectionAttributeId).pipe(catchError((err) => of([]))));
      dataSourceItems.push(attr.dataSource);
    });

    forkJoin(dataSourceCalls).subscribe((dataSourceResponse) => {
      attrArray.forEach((attr: IJson, index: number) => {
        // TODO: refactor this later
        if (attr.inspectionAttributeId === 'aa1da347-cafd-4b06-993f-74511d969958') {
          attr.bindTo = 'designationId';
          attr.bindKey = 'designation';
        }

        this.dynamicFormFields.push({
          formKey: 'dynamicForm',
          className: 'flex-1',
          key: attr.attributeKey,
          type: attr.dataType === 'User Input List' || attr.dataType === 'select' ? 'matselect' : 'input',
          defaultValue: this.planningForm.value[attr.dataSource] || '',
          templateOptions: {
            label: attr.labelName,
            placeholder: attr.labelName,
            appearance: 'outline',
            readonly: attr.dataType === 'text',
            required: attr.isMandatory === 1,
            options: dataSourceResponse[index],
            valueProp: 'value',
            labelProp: 'key',
            multiple: attr.isMultiSelectAllow === 1,
            onChange: (field: CustomFormlyFieldConfig, event: IJson, item: IJson) => {
              if (attr.bindTo && attr.bindKey) {
                field.form.patchValue({
                  [attr.bindTo]: field.fieldObject[attr.bindKey],
                });
              }
            },
          },
        });
      });
      this.addFormFields();
    });
  }

  addFormFields() {
    const formFields: IJson[] = [];
    const fields = this.dynamicFormFields.chunk(2);

    this.planningForm.clearValidators();

    fields.forEach((innerfields: IJson[]) => {
      const groupFields: FormlyFieldConfig = {
        fieldGroupClassName: 'display-flex',
        fieldGroup: [],
      };

      innerfields.forEach((field: IJson) => {
        groupFields.fieldGroup.push(field);
      });

      formFields.push(groupFields);
    });

    this.planningFields = [...formFields];

    this.formlyConfig.config.fields = [...this.planningFields];

    this.cdr.detectChanges();
  }

  removeSubInspectionType() {
    this.dynamicFormFields = this.dynamicFormFields.filter((formField) => formField.key !== 'subInspectionTypeId');
  }

  getDataSourceData(inspectionAttributeId: string) {
    return this.auditService.getDataSourceData(inspectionAttributeId);
  }

  discard(event: any) {
    this.displayDialog = false;
  }

  save() {
    this.planningForm.markAllAsTouched();
    if (this.planningForm.invalid) {
      return;
    }

    const { comments, interval, ...inspectionMasterData } = this.planningForm.value;

    if (inspectionMasterData.subInspectionTypeId) {
      inspectionMasterData.inspectionTypeId = inspectionMasterData.subInspectionTypeId;
      delete inspectionMasterData.subInspectionTypeId;
    }

    if (!inspectionMasterData.state) {
      inspectionMasterData.state = 'Todo';
    }

    if (this.inspectionMasterId) {
      inspectionMasterData.inspectionMasterId = this.inspectionMasterId;
      inspectionMasterData.state = 'Planning Approved';
    }

    const preparationData = {
      inspectionMaster: inspectionMasterData,
      comments,
      interval,
    };

    this.savingPlanningForm = true;

    this.planningService
      .save(preparationData)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response: IJson) => {
          this.savingPlanningForm = false;
          this.displayDialog = false;
          this.notifyService.showSuccess(response.message);
          this.inspectionMasterId = '';
          this.getPlanningList(this.selectedUtility.inspectionTypeId);
        },
        (error) => {
          this.displayDialog = false;
          this.savingPlanningForm = false;
          this.inspectionMasterId = '';
          this.notifyService.showError(error);
        }
      );

    this.planningForm.markAllAsTouched();
  }

  getPlanningList(inspectionTypeId: string) {
    const filterData = new Map();

    filterData.set('inspectionType', inspectionTypeId);
    filterData.set('myVessel', true);
    this.gridOptions.api.showLoadingOverlay();
    this.planningService
      .getPlanningList(filterData)
      .pipe(untilDestroyed(this))
      .subscribe(
        (preparationList: IJson[]) => {
          this.rowData = preparationList;
          this.gridOptions.api.hideOverlay();
        },
        (error: any) => {
          this.rowData = [];
          this.gridOptions.api.hideOverlay();
        }
      );
  }

  getFormattedPreparationList(preparationList: IJson[]) {
    return preparationList.map((prepItem) => {
      prepItem.action = {
        disabled: prepItem.isRequested === 0,
      };
      prepItem.tasksCompleted = prepItem.progress.checkListCompletion;
      prepItem.timeElapsed = prepItem.progress.timeScale;
      return prepItem;
    });
  }

  ngOnDestroy() {}
}
