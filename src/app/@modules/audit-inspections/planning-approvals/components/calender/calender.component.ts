import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CalendarOptions, FullCalendarComponent } from '@fullcalendar/angular';
import timeGridPlugin from '@fullcalendar/timegrid';
import { ICustomFormlyConfig, IJson } from '@types';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { CalenderViewComponent } from '../../shared/calender-view/calender-view.component';
import { AuditInspectionService } from '@modules/audit-inspections/audit-inspections.service';
import { take } from 'rxjs/operators';
import { untilDestroyed } from '@core';
import { PlanningService } from '../../planning.service';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import listPlugin from '@fullcalendar/list';

@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.scss'],
})
export class CalenderComponent implements OnInit, OnDestroy {
  @ViewChild('calendar')
  calendarComponent: FullCalendarComponent;

  formlyConfig: ICustomFormlyConfig[];
  displayDialog = false;
  displayCalanderDialog = false;
  title: any;

  selectedPlanData: IJson;

  calendarView = {
    month: true,
    threeMonth: false,
    year: false,
  };

  auditUtilityList: Array;
  selectedUtility: IJson;

  headerClass: string;

  tableData = [
    {
      oilMajor: 'Shell',
      port: 'Mumbai',
    },
  ];

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    plugins: [timeGridPlugin],
    headerToolbar: false,
    events: [],
    views: {
      dayGridMonth: {
        showNonCurrentDates: false,
        fixedWeekCount: false,
      },
    },
    eventClick: (info: any) => {
      this.displayCalanderDialog = false;
      if (info) {
        if (info.event._def.hasOwnProperty('extendedProps')) {
          this.selectedPlanData = info.event.extendedProps.planningData;
        }
      }
      setTimeout(() => {
        this.headerClass = info.event.classNames.length > 0 ? info.event.classNames[0] : '';
        this.displayCalanderDialog = true;
      }, 10);
    },
  };

  calendarOptions1: CalendarOptions = {
    initialView: 'dayGridMonth',
    plugins: [timeGridPlugin],
    headerToolbar: false,
    events: [],
    eventClick: (info: any) => {
      this.displayCalanderDialog = false;
      if (info) {
        if (info.event._def.hasOwnProperty('extendedProps')) {
          this.selectedPlanData = info.event.extendedProps.planningData;
        }
      }
      setTimeout(() => {
        this.headerClass = info.event.classNames.length > 0 ? info.event.classNames[0] : '';
        this.displayCalanderDialog = true;
      }, 10);
    },
  };
  yearCalendarOptions: CalendarOptions = {
    initialView: 'listMonth',
    plugins: [listPlugin, timeGridPlugin],
    headerToolbar: false,
    views: {
      listMonth: {
        type: 'dayGrid',
        duration: { months: 2 },
      },
    },
    events: 'https://fullcalendar.io/demo-events.json?single-day&for-resource-timeline',
    eventClick: (info: any) => {
      this.displayCalanderDialog = false;
      if (info) {
        if (info.event._def.hasOwnProperty('extendedProps')) {
          this.selectedPlanData = info.event.extendedProps.planningData;
        }
      }
      setTimeout(() => {
        this.headerClass = info.event.classNames.length > 0 ? info.event.classNames[0] : '';
        this.displayCalanderDialog = true;
      }, 10);
    },
  };

  current_month: any;
  calenderViewComp: any;

  startDate: string;
  endDate: string;

  status = {
    green: {
      className: 'green-header',
      backgroundColor: '#27a921',
      borderColor: '#27a921',
    },
    orange: {
      backgroundColor: '#f5a52e',
      className: 'orange-header',
      borderColor: '#f5a52e',
    },
    red: {
      backgroundColor: '#e54f60',
      className: 'red-header',
      borderColor: '#e54f60',
    },
  };

  constructor(
    private datePipe: DatePipe,
    private auditService: AuditInspectionService,
    private planningService: PlanningService
  ) {}

  ngOnInit(): void {
    this.title = this.datePipe.transform(new Date(), 'MMMM yyyy');
    this.calenderViewComp = new CalenderViewComponent();

    this.getAuditUtilities();
  }

  previous() {
    this.calendarComponent.getApi().prev();
    this.getMonth();
  }

  next() {
    this.calendarComponent.getApi().next();
    this.getMonth();
    if (this.calendarView.threeMonth) {
      this.calenderViewComp.getNextMonth();
      this.calenderViewComp.callNextMonthFromOther();
    }
  }
  getMonth() {
    const start = moment(this.calendarComponent.getApi().view.currentStart);

    this.startDate = start.startOf('month').format('YYYY-MM-DD');
    this.endDate = start.endOf('month').format('YYYY-MM-DD');

    this.title = start.format('MMMM YYYY');

    this.getPlanningList(this.selectedUtility.value, this.startDate, this.endDate);
  }
  getCurrentMonth() {
    this.calendarView.threeMonth = false;
    this.calendarView.month = true;
    this.calendarView.year = false;
  }
  getThreeMonth() {
    this.calendarView.threeMonth = true;
    this.calendarView.month = false;
    this.calendarView.year = false;
  }

  getYear() {
    this.calendarView.threeMonth = false;
    this.calendarView.month = false;
    this.calendarView.year = true;
  }

  today() {
    this.calendarComponent.getApi().today();
    this.getMonth();
  }

  AfterViewInit(): void {
    this.calendarComponent.getApi();
  }

  discard(event: any) {
    this.displayDialog = false;
  }

  getAuditUtilities() {
    this.auditService
      .getUtility()
      .pipe(take(1))
      .subscribe((response: any) => {
        const utilities = response;
        this.auditUtilityList = utilities
          .filter((utility: any) => utility.planType !== 'surprise')
          .map((utility: any) => {
            const customizedUtility = {
              value: utility.inspectionTypeId,
              label: utility.type,
            };
            return customizedUtility;
          });
        this.selectedUtility = this.auditUtilityList[0];
        this.getMonth();
      });
  }

  onSireChanged(selectedUtility: any) {
    if (selectedUtility) {
      this.selectedUtility = selectedUtility;
      this.getMonth();
    }
  }

  getPlanningList(inspectionTypeId: string, startDate: string, endDate: string) {
    const filterData = new Map();

    filterData.set('inspectionType', inspectionTypeId);
    filterData.set('myVessel', true);
    filterData.set('startDate', startDate);
    filterData.set('endDate', endDate);

    this.planningService
      .getPlanningList(filterData)
      .pipe(untilDestroyed(this))
      .subscribe(
        (planningList: IJson[]) => {
          planningList.forEach((plan) => {
            this.calendarComponent.getApi().removeAllEvents();
            if (plan.dateOfInspection) {
              const status = this.status[this.getStatus(plan.dateOfInspection)];

              let eventData = {
                title: plan.vesselName,
                start: moment(plan.dateOfInspection).format('YYYY-MM-DD'),
                planningData: plan,
              };

              if (status) {
                eventData = {
                  ...eventData,
                  ...status,
                };
              }

              setTimeout(() => this.calendarComponent.getApi().addEvent(eventData), 0);
            }
          });
        },
        (error: any) => {
          this.calendarComponent.getApi().removeAllEvents();
        }
      );
  }

  onCalendarViewChange(event: MatButtonToggleChange) {
    if (event.value === 'month') {
      this.getCurrentMonth();
    }

    if (event.value === '3_month') {
      this.getThreeMonth();
    }

    if (event.value === 'year') {
      this.getYear();
    }
  }

  getStatus(inspectionDate: string) {
    const dateDiff = moment(inspectionDate).diff(moment(new Date()), 'days');

    if (dateDiff > 60) {
      return 'green';
    }

    if (dateDiff > 30) {
      return 'orange';
    }

    if (dateDiff > 0) {
      return 'red';
    }

    return null;
  }

  getTableOddCellClass(index: number, headerClass: string) {
    switch (headerClass) {
      case 'red-header':
        return index % 2 === 0 ? 'odd-table-cell-red' : '';

      case 'green-header':
        return index % 2 === 0 ? 'odd-table-cell-green' : '';

      case 'orange-header':
        return index % 2 === 0 ? 'odd-table-cell-orange' : '';

      default:
        return index % 2 === 0 ? 'odd-table-cell-blue' : '';
    }
  }

  ngOnDestroy() {}
}
