import { Component, OnInit } from '@angular/core';
import { NotificationService } from '@shared/common-service';
import { ReminderService } from '@shared/service';
import { IJson } from '@types';
import * as _ from 'lodash';
import { RemindersAgGridCellComponent } from '../../shared/reminders-ag-grid-cell/reminders-ag-grid-cell.component';
import { RemindersVesselsAgGridCellComponent } from '../../shared/reminders-vessels-ag-grid-cell/reminders-vessels-ag-grid-cell.component';

@Component({
  selector: 'app-reminders',
  templateUrl: './reminders.component.html',
  styleUrls: ['./reminders.component.scss'],
})
export class RemindersComponent implements OnInit {
  public isLoading = false;
  // TODO: add support of observable input
  headerSelectOptions = [
    { label: 'SIRE', value: 'SIRE' },
    { label: 'NON-SIRE', value: 'NON-SIRE' },
  ];
  headerSelection = 'SIRE';

  //#region Ag Grid Properties Start

  selectedReminder: IJson;
  reminderEdit: IJson;
  _editReminder: boolean;
  editType: any = null;
  displayDialog: boolean;

  //#region Ag Grid Properties Start
  columnDefs = [
    {
      headerName: 'VESSELS',
      field: 'vessel',
      cellClass: 'bold-text',
      rowSpan: (params: any) => {
        if (params.data.spanLength) {
          return +params.data.spanLength;
        }
      },
      filter: 'agTextColumnFilter',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'vesselWithBellCell',
      cellRendererParams: {
        onCreate: (params: any, reminderData: IJson) => {
          this.selectedReminder = reminderData;
          this.openCreateForm();
        },
      },
    },
    {
      headerName: 'OIL MAJOR',
      field: 'oilMajor',
      cellRenderer: 'spannedCells',
      cellRendererParams: {
        onEdit: (params: any, rowData: any) => {
          rowData = {
            ...rowData,
            vesselId: params.data.vesselId,
          };
          this.openEditForm(rowData);
        },

        onDelete: (params: any, rowData: any) => {
          this.deleteReminder(rowData);
        },
      },
      flex: 1,
      resizable: true,
      colSpan(params: any) {
        const groupData = params.data;
        if (groupData.group) {
          return 4;
        } else {
          return 1;
        }
      },
      cellStyle(params: any) {
        return { paddingLeft: '3px', paddingRight: '3px' };
      },
    },
    {
      headerName: 'LAST INSPECTED',
      field: 'lastInspected',
      flex: 1,
      resizable: true,
      cellClass: '',
    },
    {
      headerName: 'REMINDER',
      field: 'reminder',
      flex: 1,
      resizable: true,
      cellClass: '',
    },
    {
      headerName: 'ACTION',
      width: 100,
    },
  ];

  frameworkComponents = {
    spannedCells: RemindersAgGridCellComponent,
    vesselWithBellCell: RemindersVesselsAgGridCellComponent,
  };

  headerHeight = 40;
  rowData: any[] = [];
  rowDataFilter: any = [];
  gridApi: any;
  gridColumnApi: any;
  //#endregion AG Grid Properties End

  constructor(private reminderService: ReminderService, private readonly notificationService: NotificationService) {}

  ngOnInit() {}

  //#region AG grid Functions Regions Start
  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.getReminders();
    this.gridApi.sizeColumnsToFit();
  }

  getRowHeight(params: any) {
    const height = 46;
    return params.data.spanLength * height;
  }
  //#region AG grid Functions Regions Ends

  // Form to create new reminder
  openCreateForm() {
    this.displayDialog = false;
    this._editReminder = false;

    setTimeout(() => {
      this.displayDialog = true;
    }, 10);
  }

  // Form to edit reminder
  openEditForm(rowData: any) {
    this.selectedReminder = rowData;
    this.displayDialog = true;
    this._editReminder = true;
  }

  // Close dialog
  discard(event: any) {
    this.displayDialog = false;
  }

  // On dialog close, get all reminders
  onDialogClose() {
    this._editReminder = false;
    this.displayDialog = false;
    this.reminderEdit = undefined;
    // Update the data after creating or updating record
    this.getReminders();
  }

  onDialogOpen(event: any) {
    this.reminderEdit = this.selectedReminder;
  }

  headerSelectValue(selectionValue: any) {
    this.headerSelection = selectionValue.value;

    this.rowData = JSON.parse(JSON.stringify(this.rowDataFilter));

    let filteredData = [];
    filteredData = _.forEach(this.rowData, (row: any) => {
      const foundData = _.find(row.inspectionType, { inspectionType: selectionValue.value });
      row.inspectionType = foundData !== undefined ? [foundData] : [];
    });

    filteredData = _.filter(filteredData, (f) => {
      return f.inspectionType.length !== 0;
    });

    this.rowData = filteredData;
  }

  // Get all reminders list
  private getReminders() {
    this.isLoading = true;

    this.reminderService.getReminders().subscribe(
      (response) => {
        this.rowData = response;
        this.rowDataFilter = JSON.parse(JSON.stringify(this.rowData));
        this.isLoading = false;
        this.headerSelectValue({ value: this.headerSelection });
      },
      (error) => {
        this.notificationService.showError(error, 'Error!');
      }
    );
  }

  // Delete reminder
  private deleteReminder(rowData: any) {
    this.reminderService.deleteReminder(rowData.reminderId).subscribe(
      (response) => {
        if (response.statusCode === 200) {
          const message = response.message;
          this.notificationService.showSuccess(message, 'Success!');
          this.getReminders();
        } else {
          const message = 'Record not deteted';
          this.notificationService.showError(message, 'Error!');
        }
      },
      (error) => {
        const message = error;
        this.notificationService.showError(message, 'Error!');
      }
    );
  }
}
