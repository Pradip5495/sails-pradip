import { Injectable } from '@angular/core';
import { HttpDispatcherService } from '@core';

const routes = {
  getAll: (query: string) => `/planning/timeline?${query}`,
};

@Injectable({
  providedIn: 'root',
})
export class TimelineService {
  constructor(private httpDispatch: HttpDispatcherService) {}

  getTimeline(inspectionType: string, myVessel: boolean, startDate: string, endDate: string, subType: string = null) {
    let params = `inspectionType=${inspectionType}&myVessel=${myVessel}&startDate=${startDate}&endDate=${endDate}`;
    if (subType) {
      params = params + `&subType=${subType}`;
    }
    return this.httpDispatch.get(routes.getAll(params), { acceptNullResponse: true });
  }
}
