import { Component, OnInit } from '@angular/core';

import { DatePipe } from '@angular/common';
import { IJson } from '@types';
import { FormControl } from '@angular/forms';
import { AuditInspectionService } from '@modules/audit-inspections/audit-inspections.service';
import { take } from 'rxjs/operators';
import { TimelineService } from './timeline.service';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['../calender/calender.component.scss', './timeline.component.scss'],
})
export class TimelineComponent implements OnInit {
  timelineDurationInMonths: 1 | 3 | 12;
  displayDialog = false;
  displaySelect = true;
  displayMonthSelect = true;
  displayYearCalander = false;
  // todayDate: any;
  calanderArray: any = [];

  selectedInspectionDetails: IJson;
  selectedVesselDetails: IJson;

  superindentRadioSelection = ['Marine', 'Technical', 'All'];
  superindentRadioValue: FormControl = new FormControl('All');

  sireData: any = [
    {
      vessel: 'vessel 10',
      lastInspectionDate: '2020-04-06 06:42:05.047685',
      displayAlert: false,
      vesselData: [
        {
          badge: 'INS-24rd',
          badgeDate: '2020-3-24 00:00:00.000000',
          color: 'yellow',
          comments: null,
          vesselType: null,
          alarmDate: '2020-3-24 06:42:05.047685',
          dateOfInspection: '2020-3-24 00:00:00.000000',
          inspectionMasterId: '338d30f7-360f-4161-8ef1-69765a2fa17a',
        },
        {
          badge: 'PL-9th',
          badgeDate: '2020-12-12 00:00:00.000000',
          color: 'yellow',
          comments: null,
          vesselType: null,
          alarmDate: '2020-12-06 06:42:05.047685',
          dateOfInspection: '2020-11-1 06:29:52.000000',
          inspectionMasterId: '12f0a5fd-3a4e-48b9-8c9e-74363dd3cae6',
        },
      ],
    },
    {
      vessel: 'vessel 11',
      lastInspectionDate: '2020-04-06 06:42:05.047685',
      displayAlert: false,
      vesselData: [
        {
          badge: 'ASN-20th',
          badgeDate: '2020-12-20 00:00:00.000000',
          color: 'yellow',
          comments: null,
          vesselType: null,
          alarmDate: '2020-10-10 06:42:05.047685',
          dateOfInspection: '2020-12-30 00:00:00.000000',
          inspectionMasterId: '338d30f7-360f-4161-8ef1-69765a2fa17a',
        },
      ],
    },
    {
      vessel: 'vessel 12',
      lastInspectionDate: '2020-04-06 06:42:05.047685',
      displayAlert: false,
      vesselData: [
        {
          badge: 'INS-30th',
          badgeDate: '2020-11-30 00:00:00.000000',
          color: 'red',
          comments: null,
          vesselType: null,
          alarmDate: '2020-10-31 06:42:05.047685',
          dateOfInspection: '2020-12-1 00:00:00.000000',
          inspectionMasterId: '338d30f7-360f-4161-8ef1-69765a2fa17a',
        },
      ],
    },
    {
      vessel: 'vessel 13',
      lastInspectionDate: '',
      displayAlert: false,
      vesselData: [
        {
          badge: 'PA-15th',
          badgeDate: '2020-10-15 00:00:00.000000',
          color: 'black',
          comments: null,
          vesselType: null,
          alarmDate: '2020-10-10 06:42:05.047685',
          dateOfInspection: '2020-12-30 00:00:00.000000',
          inspectionMasterId: '338d30f7-360f-4161-8ef1-69765a2fa17a',
        },
      ],
    },
    {
      vessel: 'vessel 14',
      lastInspectionDate: '2020-04-06 06:42:05.047685',
      displayAlert: false,
      vesselData: [
        {
          badge: 'PA-23rd',
          badgeDate: '2020-12-12 00:00:00.000000',
          color: 'orange',
          comments: null,
          vesselType: null,
          alarmDate: '2020-12-12 06:42:05.047685',
          dateOfInspection: '2020-12-22 00:00:00.000000',
          inspectionMasterId: '338d30f7-360f-4161-8ef1-69765a2fa17a',
        },
      ],
    },
  ];

  superindentVesselData: any = [
    {
      vessel: 'vessel 10',
      lastInspectionDate: '2020-04-06 06:42:05.047685',
      displayAlert: false,
      vesselData: [
        {
          badge: 'INS-24rd',
          badgeDate: '2020-3-24 00:00:00.000000',
          color: 'yellow',
          comments: null,
          vesselType: 'M',
          alarmDate: '2020-3-24 06:42:05.047685',
          dateOfInspection: '2020-3-30 00:00:00.000000',
          inspectionMasterId: '338d30f7-360f-4161-8ef1-69765a2fa17a',
        },
        {
          badge: 'PL-9th',
          badgeDate: '2020-12-12 00:00:00.000000',
          color: 'yellow',
          comments: null,
          vesselType: 'T',
          alarmDate: '2020-12-06 06:42:05.047685',
          dateOfInspection: '2020-11-20 21:29:52.000000',
          inspectionMasterId: '12f0a5fd-3a4e-48b9-8c9e-74363dd3cae6',
        },
      ],
    },
    {
      vessel: 'vessel 11',
      lastInspectionDate: '2020-04-06 06:42:05.047685',
      displayAlert: false,
      vesselData: [
        {
          badge: 'ASN-20th',
          badgeDate: '2020-12-20 00:00:00.000000',
          color: 'yellow',
          comments: null,
          vesselType: null,
          alarmDate: '2020-10-10 06:42:05.047685',
          dateOfInspection: '2020-12-30 00:00:00.000000',
          inspectionMasterId: '338d30f7-360f-4161-8ef1-69765a2fa17a',
        },
      ],
    },
    {
      vessel: 'vessel 12',
      lastInspectionDate: '2020-04-06 06:42:05.047685',
      displayAlert: false,
      vesselData: [
        {
          badge: 'INS-30th',
          badgeDate: '2020-11-30 00:00:00.000000',
          color: 'red',
          comments: null,
          vesselType: null,
          alarmDate: '2020-10-30 06:42:05.047685',
          dateOfInspection: '2020-12-1 00:00:00.000000',
          inspectionMasterId: '338d30f7-360f-4161-8ef1-69765a2fa17a',
        },
      ],
    },
    {
      vessel: 'vessel 13',
      lastInspectionDate: '',
      displayAlert: false,
      vesselData: [
        {
          badge: 'PA-15th',
          badgeDate: '2020-10-15 00:00:00.000000',
          color: 'black',
          comments: null,
          vesselType: null,
          alarmDate: '2020-10-10 06:42:05.047685',
          dateOfInspection: '2020-12-30 00:00:00.000000',
          inspectionMasterId: '338d30f7-360f-4161-8ef1-69765a2fa17a',
        },
      ],
    },
    {
      vessel: 'vessel 14',
      lastInspectionDate: '2020-04-06 06:42:05.047685',
      displayAlert: false,
      vesselData: [
        {
          badge: 'PA-23rd',
          badgeDate: '2020-11-12 00:00:00.000000',
          color: 'orange',
          comments: null,
          vesselType: null,
          alarmDate: '2020-12-12 06:42:05.047685',
          dateOfInspection: '2020-12-22 00:00:00.000000',
          inspectionMasterId: '338d30f7-360f-4161-8ef1-69765a2fa17a',
        },
      ],
    },
  ];

  displayLoading: boolean;

  vesselsArray = ['Vessel 1', 'Vessel 2', 'Vessel 3'];

  todayDate1: Date = new Date();
  thisMonth: number;
  todaysDate: number;
  thisYear: number;

  auditUtilityList: Array;
  selectedUtility: IJson;

  constructor(
    private datePipe: DatePipe,
    private auditService: AuditInspectionService,
    private timelineService: TimelineService
  ) {
    this.todaysDate = this.todayDate1.getDate();
    this.thisMonth = this.todayDate1.getMonth() + 1;
    this.thisYear = this.todayDate1.getFullYear();
  }

  ngOnInit(): void {
    this.displayYearCalander = false;
    // this.todayDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

    this.getAuditUtilities();
    // this.getTimelineData();
  }

  getAuditUtilities() {
    this.auditService
      .getUtility()
      .pipe(take(1))
      .subscribe((response: any) => {
        const utilities = response;
        this.auditUtilityList = utilities
          .filter((utility: any) => utility.planType !== 'surprise')
          .map((utility: any) => {
            const customizedUtility = {
              value: utility.inspectionTypeId,
              label: utility.type,
            };
            return customizedUtility;
          });
        this.selectedUtility = this.auditUtilityList[0];
        this.setTimelineDuration(3);

        // this.getTimelineData(this.selectedUtility.value);
      });
  }

  // GET THE GANTT INTERNAL STATE

  onAddForm() {
    this.displayDialog = false;
    setTimeout(() => {
      this.displayDialog = true;
    }, 10);
  }

  showYear() {
    this.displayYearCalander = true;
  }
  showMonths() {
    this.displayYearCalander = false;
  }
  discard(event: any) {
    this.displayDialog = false;
  }

  getCalenderHeaderRows() {
    return this.timelineDurationInMonths === 12
      ? new Array(12)
      : this.timelineDurationInMonths === 3
      ? new Array(3)
      : new Array(1);
  }

  getMonthHeaderDivisions(index: number) {
    if (this.timelineDurationInMonths === 3) {
      if (index === 0) {
        if (this.todaysDate > 0 && this.todaysDate <= 7) {
          return '28%';
        } else if (this.todaysDate > 7 && this.todaysDate <= 14) {
          return '23%';
        } else if (this.todaysDate > 14 && this.todaysDate <= 21) {
          return '16.8%';
        } else if (this.todaysDate > 21 && this.todaysDate <= 31) {
          return '9.33%';
        }
      } else {
        if (this.todaysDate > 0 && this.todaysDate <= 7) {
          return '28%';
        } else if (this.todaysDate > 7 && this.todaysDate <= 14) {
          return '30.54%';
        } else if (this.todaysDate > 14 && this.todaysDate <= 21) {
          return '67.2%';
        } else if (this.todaysDate > 21 && this.todaysDate <= 31) {
          return '74.6%';
        }
      }
    } else {
      return this.timelineDurationInMonths === 12 ? '6.79%' : '84%';
    }
  }

  getMonthNameHeader(index: number) {
    let month = this.thisMonth + index;
    if (this.timelineDurationInMonths === 12) {
      month = index + 1;
    }

    if (this.timelineDurationInMonths === 3) {
      month = month > 12 ? month - 12 : month;
    }
    // if (month > 12) {
    //   month = month -12
    // }
    switch (month) {
      case 1:
        return 'Jan';

      case 2:
        return 'Feb';

      case 3:
        return 'Mar';

      case 4:
        return 'Apr';

      case 5:
        return 'May';

      case 6:
        return 'June';

      case 7:
        return 'July';

      case 8:
        return 'Aug';

      case 9:
        return 'Sep';

      case 10:
        return 'Oct';

      case 11:
        return 'Nov';

      case 12:
        return 'Dec';

      default:
        break;
    }
  }

  getTimelineDataRows() {
    if (this.timelineDurationInMonths === 3) {
      if (this.todaysDate > 0 && this.todaysDate <= 7) {
        return new Array(this.timelineDurationInMonths * 4);
      } else if (this.todaysDate > 7 && this.todaysDate <= 14) {
        return new Array(this.timelineDurationInMonths * 4 - 1);
      } else if (this.todaysDate > 14 && this.todaysDate <= 21) {
        return new Array(this.timelineDurationInMonths * 4 - 2);
      } else if (this.todaysDate > 21 && this.todaysDate <= 31) {
        return new Array(this.timelineDurationInMonths * 4 - 3);
      }
    } else {
      return this.timelineDurationInMonths === 12 ? new Array(12) : new Array(this.timelineDurationInMonths * 4);
    }
  }

  getMonthDataDivisions(index: number) {
    if (this.timelineDurationInMonths === 3) {
      if (this.todaysDate > 0 && this.todaysDate <= 7) {
        return '7%';
      } else if (this.todaysDate > 7 && this.todaysDate <= 14) {
        return '7.63%';
      } else if (this.todaysDate > 14 && this.todaysDate <= 21) {
        return '8.4%';
      } else if (this.todaysDate > 21 && this.todaysDate <= 31) {
        return '9.33%';
      }
    } else {
      return this.timelineDurationInMonths === 12 ? '6.58%' : '21%';
    }
    // return this.timelineDurationInMonths > 2 ? '7%' : '21%';
  }

  displayInspectionLabel(colIndex: number, valueDate: string) {
    if (!valueDate) {
      return false;
    }

    if (isNaN(Date.parse(valueDate))) {
      return;
    }

    // Received Data
    const convertToDate = new Date(valueDate);
    const date = convertToDate.getDate();
    const month = convertToDate.getMonth() + 1;
    const year = convertToDate.getFullYear();
    let index = colIndex;

    // Todays Data
    let thisMonth = this.thisMonth;
    let thisYear = this.thisYear;

    // Checking for Week
    if (this.timelineDurationInMonths === 1) {
      if (month === thisMonth) {
        if (date > 0 && date <= 7 && index === 0) {
          return true;
        } else if (date > 7 && date <= 14 && index === 1) {
          return true;
        } else if (date > 14 && date <= 21 && index === 2) {
          return true;
        } else if (date > 21 && date <= 31 && index === 3) {
          return true;
        }
      }
    }

    // Checking for 3 months
    if (this.timelineDurationInMonths === 3) {
      // Logic to get Next Months index define how many columns will first Month get based on todays date
      if (this.todaysDate > 0 && this.todaysDate <= 7) {
        thisMonth = index <= 3 ? this.thisMonth : index > 3 && index <= 7 ? this.thisMonth + 1 : this.thisMonth + 2;
      } else if (this.todaysDate > 7 && this.todaysDate <= 14) {
        thisMonth = index <= 2 ? this.thisMonth : index > 2 && index <= 6 ? this.thisMonth + 1 : this.thisMonth + 2;
      } else if (this.todaysDate > 14 && this.todaysDate <= 21) {
        thisMonth = index <= 1 ? this.thisMonth : index > 1 && index <= 5 ? this.thisMonth + 1 : this.thisMonth + 2;
      } else if (this.todaysDate > 21 && this.todaysDate <= 31) {
        thisMonth = index === 0 ? this.thisMonth : index > 0 && index <= 4 ? this.thisMonth + 1 : this.thisMonth + 2;
      }
      // thisMonth = index <= 3 ? this.thisMonth : (index > 3 && index <= 7) ? this.thisMonth + 1 : this.thisMonth + 2;

      // This is used to get the month of next year
      thisYear = thisMonth > 12 ? thisYear + 1 : thisYear;
      thisMonth = thisMonth > 12 ? thisMonth - 12 : thisMonth;

      // Resetting index to start for every 4 columns
      // index = index <= 3 ? index : (index > 3 && index <= 7) ? index-4 : index-8;

      // Every Month is divided in 4 parts, below logic will assign index of 0-3 for every month
      // for 1st month index may be 0-3, 0-2, 0-1 or 0 based on todays date
      if (this.todaysDate > 0 && this.todaysDate <= 7) {
        index = index <= 3 ? index : index > 3 && index <= 7 ? index - 4 : index - 8;
      } else if (this.todaysDate > 7 && this.todaysDate <= 14) {
        index = index <= 2 ? index : index > 2 && index <= 6 ? index - 3 : index - 7;
      } else if (this.todaysDate > 14 && this.todaysDate <= 21) {
        index = index <= 1 ? index : index > 1 && index <= 5 ? index - 2 : index - 6;
      } else if (this.todaysDate > 21 && this.todaysDate <= 31) {
        index = index === 0 ? index : index > 0 && index <= 4 ? index - 1 : index - 5;
      }

      if (month === thisMonth && year === thisYear) {
        if (this.todaysDate > 0 && this.todaysDate <= 7) {
          if (date > 0 && date <= 7 && index === 0) {
            return true;
          } else if (date > 7 && date <= 14 && index === 1) {
            return true;
          } else if (date > 14 && date <= 21 && index === 2) {
            return true;
          } else if (date > 21 && date <= 31 && index === 3) {
            return true;
          }
        } else if (this.todaysDate > 7 && this.todaysDate <= 14) {
          if (colIndex <= 2) {
            if (date > 7 && date <= 14 && index === 0) {
              return true;
            } else if (date > 14 && date <= 21 && index === 1) {
              return true;
            } else if (date > 21 && date <= 31 && index === 2) {
              return true;
            }
          } else {
            if (date > 0 && date <= 7 && index === 0) {
              return true;
            } else if (date > 7 && date <= 14 && index === 1) {
              return true;
            } else if (date > 14 && date <= 21 && index === 2) {
              return true;
            } else if (date > 21 && date <= 31 && index === 3) {
              return true;
            }
          }
        } else if (this.todaysDate > 14 && this.todaysDate <= 21) {
          if (colIndex <= 1) {
            if (date > 14 && date <= 21 && index === 0) {
              return true;
            } else if (date > 21 && date <= 31 && index === 1) {
              return true;
            }
          } else {
            if (date > 0 && date <= 7 && index === 0) {
              return true;
            } else if (date > 7 && date <= 14 && index === 1) {
              return true;
            } else if (date > 14 && date <= 21 && index === 2) {
              return true;
            } else if (date > 21 && date <= 31 && index === 3) {
              return true;
            }
          }
        } else if (this.todaysDate > 21 && this.todaysDate <= 31) {
          if (colIndex === 0) {
            if (date > 21 && date <= 31 && index === 0) {
              return true;
            }
          } else {
            if (date > 0 && date <= 7 && index === 0) {
              return true;
            } else if (date > 7 && date <= 14 && index === 1) {
              return true;
            } else if (date > 14 && date <= 21 && index === 2) {
              return true;
            } else if (date > 21 && date <= 31 && index === 3) {
              return true;
            }
          }
        }
      }
    }

    // Checking for 12 Months
    if (this.timelineDurationInMonths === 12) {
      // This code is commented because we have to display only this Year vessel data
      // thisMonth = thisMonth + index;
      // thisYear = thisMonth > 12 ? thisYear + 1 : thisYear;
      // thisMonth = thisMonth > 12 ? thisMonth -12 : thisMonth;

      if (month === index + 1 && year === thisYear) {
        return true;
      }
    }
  }

  clickOnInspection(inspectionData: IJson, vesselData: IJson) {
    this.displayDialog = true;
    this.selectedInspectionDetails = inspectionData;
    this.selectedVesselDetails = vesselData;
  }

  displayDateMarker(rowIndex: number) {
    if (this.timelineDurationInMonths === 3 && rowIndex === 0) {
      return true;
    } else if (this.timelineDurationInMonths === 12 && rowIndex === 0) {
      return true;
    } else {
      return false;
    }
  }

  getMarkerPosition(item: string) {
    const defaultValue = 16.7;
    const eachCell = 6.6;
    if (this.timelineDurationInMonths === 12) {
      const cellSpan = (this.thisMonth - 1) * eachCell;
      const spaceInsideCell = this.todaysDate * 0.2;

      if (item === 'arrow') {
        return defaultValue + cellSpan + spaceInsideCell - 0.11;
      }
      return defaultValue + cellSpan + spaceInsideCell;
    }
  }

  getVesselTypeLabelClass(vesselType: string) {
    if (!vesselType) {
      return;
    }
    const vesseltype = vesselType.toUpperCase();
    switch (vesseltype) {
      case 'T':
        return 'vessel-type-label-T';

      case 'M':
        return 'vessel-type-label-M';

      default:
        break;
    }
  }

  /**
   * This function returns position left value in percentage 0-70 (3 Months), 0-90(week), 0-70(year)
   * Which place icon from 0-7, 7-14, 14-21 and 21-31 in month calender and week calender
   */
  placeAlarmAndInspectionPosition(valueDate: string) {
    if (!valueDate) {
      return false;
    }

    if (isNaN(Date.parse(valueDate))) {
      return;
    }

    // Received Data
    const convertToDate = new Date(valueDate);
    let date = convertToDate.getDate();
    let multiplyFactor = 10;
    if (this.timelineDurationInMonths === 1) {
      if (date < 2) {
        return 1;
      } else if (date > 7 && date <= 14) {
        date = date - 5;
      } else if (date > 14 && date <= 21) {
        date = date - 12;
      } else if (date > 21 && date <= 31) {
        multiplyFactor = 7.5;
        date = date - 19;
      }
    }

    if (this.timelineDurationInMonths === 3) {
      if (date < 2) {
        return 1;
      } else if (date > 7 && date <= 14) {
        date = date - 7;
      } else if (date > 14 && date <= 21) {
        date = date - 14;
      } else if (date > 21 && date <= 31) {
        multiplyFactor = date === 31 ? 7.5 : 8;
        date = date - 21;
      }
    }

    if (this.timelineDurationInMonths === 12) {
      date = (date * 2.42) / 10;
    }

    return date * multiplyFactor;
  }

  /**
   * This function returns position right value in percentage 0-100 (3 Months)
   * Which place icon from 0-7, 7-14, 14-21 and 21-31 in month calender
   */
  placeProgressBar(valueDate: string) {
    if (!valueDate) {
      return false;
    }

    if (isNaN(Date.parse(valueDate))) {
      return;
    }

    // Received Data
    const convertToDate = new Date(valueDate);
    let date = convertToDate.getDate();
    const value = 100;
    let multiplicationFactor = 14.285;
    let valueToRemove: number;

    if (this.timelineDurationInMonths === 12) {
      // return 0;
    }

    if (this.timelineDurationInMonths === 3) {
      if (date > 0 && date <= 7) {
        valueToRemove = value - date * multiplicationFactor;
      } else if (date > 7 && date <= 14) {
        date = date - 7;
        valueToRemove = value - date * multiplicationFactor;
      } else if (date > 14 && date <= 21) {
        date = date - 14;
        valueToRemove = value - date * multiplicationFactor;
      } else if (date > 21 && date <= 31) {
        multiplicationFactor = 10;
        date = date - 21;
        valueToRemove = value - date * multiplicationFactor;
      }
      return valueToRemove;
    }
  }

  getSumOfProgressBarwidth(value1: any, value2: any) {
    return value1 + value2;
  }

  getMonthProgressWidth(progressBarNo: number, valueDate: string) {
    if (!valueDate) {
      return false;
    }

    if (isNaN(Date.parse(valueDate))) {
      return;
    }

    // Received Data
    const convertToDate = new Date(valueDate);
    const month = convertToDate.getMonth() + 1;

    if (progressBarNo === 1) {
      if (month === this.thisMonth) {
        return 0;
      }
      if (month - this.thisMonth === 1) {
        return 0;
      }
      if (month - this.thisMonth === 2) {
        return 400;
      }
    }

    if (progressBarNo === 2) {
      if (month === this.thisMonth) {
        return 0;
      }
      if (month - this.thisMonth === 1) {
        return 400;
      }
      if (month - this.thisMonth === 2) {
        return 400;
      }
    }

    if (progressBarNo === 3) {
      if (month === this.thisMonth) {
        return 400;
      }
      if (month - this.thisMonth === 1) {
        return 400;
      }
      if (month - this.thisMonth === 2) {
        return 400;
      }
    }
  }

  editTimeLine(rowData: IJson) {}

  getHeading() {
    if (this.timelineDurationInMonths === 3 || this.timelineDurationInMonths === 1) {
      return this.datePipe.transform(new Date(), 'MMMM yyyy');
    }
    if (this.timelineDurationInMonths === 12) {
      return this.datePipe.transform(new Date(), 'yyyy');
    }
  }

  setTimelineDuration(valueInMonths: 1 | 3 | 12) {
    this.timelineDurationInMonths = valueInMonths;
    this.getTimelineData(this.selectedUtility.value, false);
  }

  styleTimelineButton(valueInMonths: 1 | 3 | 12) {
    return this.timelineDurationInMonths === valueInMonths;
  }

  headerSelectValue(selectedUtility: any) {
    if (selectedUtility) {
      this.selectedUtility = selectedUtility;
      // this.headerSelection = selectedUtility.label;
      this.getTimelineData(selectedUtility.value);
    }
  }

  getTimelineData(selectedUtilityValue: string, resetTimeDuration: boolean = true) {
    this.displayLoading = true;

    if (resetTimeDuration) {
      this.timelineDurationInMonths = 3;
    }

    const getUserSpecificVessels = true;
    const date = new Date();
    const y = date.getFullYear();
    const m = date.getMonth();
    let startDate: string;
    let endDate: string;
    let subType: string;

    if (this.timelineDurationInMonths === 1) {
      startDate = this.datePipe.transform(new Date(y, m, 1), 'yyyy-MM-dd');
      endDate = this.datePipe.transform(new Date(y, m + 1, 0), 'yyyy-MM-dd');
    }

    if (this.timelineDurationInMonths === 3) {
      startDate = this.datePipe.transform(new Date(y, m, 1), 'yyyy-MM-dd');
      endDate = this.datePipe.transform(new Date(y, m + 3, 0), 'yyyy-MM-dd');
    }

    if (this.timelineDurationInMonths === 12) {
      startDate = this.datePipe.transform(new Date(y, 0, 1), 'yyyy-MM-dd');
      endDate = this.datePipe.transform(new Date(y, 12, 0), 'yyyy-MM-dd');
    }

    if (this.displayInspectionOrMarineSelection('SUPERINTENDENT INSPECTION')) {
      subType = this.superindentRadioValue.value;
    }

    // API Call
    this.timelineService
      .getTimeline(selectedUtilityValue, getUserSpecificVessels, startDate, endDate, subType)
      .subscribe((response) => {
        if (response) {
          this.calanderArray = response;
          this.displayLoading = false;
        }
      });
  }

  superindentValueChanged(event: any) {
    this.getTimelineData(this.selectedUtility.value, false);
  }

  displayInspectionOrMarineSelection(inspectionType: string) {
    if (this.selectedUtility) {
      return this.selectedUtility.label.toLowerCase() === inspectionType.toLowerCase();
    }
  }

  getDialogHeaderClass() {
    if (this.selectedInspectionDetails) {
      switch (this.selectedInspectionDetails.color) {
        case 'yellow':
          return 'yellow-header';
        case 'red':
          return 'red-header';
        case 'purple':
          return 'purple-header';
        case 'green':
          return 'green-header';
        case 'grey':
          return 'grey-header';
        case 'orange':
          return 'orange-header';

        default:
          return '';
      }
    }
  }
}
