import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningApprovalsComponent } from './planning-approvals.component';

describe('PlanningApprovalsComponent', () => {
  let component: PlanningApprovalsComponent;
  let fixture: ComponentFixture<PlanningApprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningApprovalsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningApprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
