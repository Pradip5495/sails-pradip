import { Injectable } from '@angular/core';
import { HttpDispatcherService } from '@core';

const routes = {
  utility: {
    get: () => `/utility`,
    getDataSourceData: (inspectionAttributeId: string) => `/utility/${inspectionAttributeId}`,
    getInspectionMaster: (inspectionMasterId: string) => `/utility/inspectionmaster/${inspectionMasterId}`,
  },
  auditCause: {
    get: (filters?: Map<string, string>) =>
      filters ? `/utility/auditcause?${filters.toSanitizedURLFilters()}` : `/utility/auditcause`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class AuditInspectionService {
  constructor(private httpDispatch: HttpDispatcherService) {}

  getUtility() {
    return this.httpDispatch.get(routes.utility.get(), { acceptNullResponse: true });
  }

  getAuditCause(filters?: Map<string, string>) {
    return this.httpDispatch.get(routes.auditCause.get(filters), { acceptNullResponse: true });
  }

  getDataSourceData(inspectionAttributeId: string) {
    return this.httpDispatch.get(routes.utility.getDataSourceData(inspectionAttributeId), { acceptNullResponse: true });
  }

  getInspectionMaster(inspectionMasterId: string) {
    return this.httpDispatch.get(routes.utility.getInspectionMaster(inspectionMasterId), { acceptNullResponse: true });
  }
}
