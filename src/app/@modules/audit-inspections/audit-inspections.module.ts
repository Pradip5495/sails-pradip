import { NgModule } from '@angular/core';
import { AuditInspectionsRoutingModule } from './audit-inspections-routing.module';
import { AuditInspectionsComponent } from './audit-inspections.component';
import { PreparationModule } from './preparation';
import { InspectionsModule } from './inspections';
import { AuditDashboardModule } from './dashboard';

@NgModule({
  declarations: [AuditInspectionsComponent],
  imports: [AuditInspectionsRoutingModule, PreparationModule, InspectionsModule, AuditDashboardModule],
})
export class AuditInspectionsModule {}
