import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InspectorTableComponent } from './inspector-table.component';

// import { PreparationTableComponent } from './preparation-table.component';

describe('InspectorTableComponent', () => {
  let component: InspectorTableComponent;
  let fixture: ComponentFixture<InspectorTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InspectorTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectorTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
