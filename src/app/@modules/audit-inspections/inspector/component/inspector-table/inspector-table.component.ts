import { Component, OnInit } from '@angular/core';
// import { GridOptions } from 'ag-grid-community';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-inspector-table',
  templateUrl: './inspector-table.component.html',
  styleUrls: ['./inspector-table.component.scss'],
})
export class InspectorTableComponent implements OnInit {
  domLayout: any;
  gridApi: any;
  editType = '';
  showGrid = true;
  showColumn = false;
  public icons: any;
  public paginationPageSize: any;

  gridColumnShowHide = true;
  displayDialog = false;
  displayProfileDialog = false;
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public elem: any;
  dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);
  public barChartLabels: Label[] = ['', '', ''];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins: any[];
  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: '' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: '' },
  ];

  displayedColumns: string[] = ['ship', 'oil_majors', 'port', 'date', 'obs'];
  public chartColors: Array<any> = [
    {
      backgroundColor: '#16569e',
    },
    {
      backgroundColor: '#b2c0ce',
    },
  ];
  count = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
  ];

  constructor() {}

  ngOnInit() {}

  ngGridClick($event: any) {
    this.showColumn = false;
    this.showGrid = $event;
  }
  ngColumnClick($event: any) {
    this.showGrid = false;
    this.showColumn = $event;
  }

  onAddForm() {
    this.displayProfileDialog = false;
    this.displayDialog = false;
    setTimeout(() => {
      this.displayDialog = true;
    }, 10);
  }

  profileClick() {
    this.displayDialog = false;
    this.displayProfileDialog = false;
    setTimeout(() => {
      this.displayProfileDialog = true;
    }, 10);
  }

  discard(event: any) {
    this.displayDialog = false;
  }
}
export interface Element {
  ship: string;
  oil_majors: string;
  port: string;
  date: string;
  obs: number;
}
const ELEMENT_DATA: Element[] = [
  { ship: 'Vessel 11', oil_majors: 'BP', port: 'MALACCA', date: '10 March 2017', obs: 9 },
  { ship: 'Vessel 12', oil_majors: 'PETRON', port: 'MALACCA', date: '10 March 2017', obs: 9 },
  { ship: 'Vessel 13', oil_majors: 'SHELL', port: 'MALACCA', date: '10 March 2017', obs: 9 },
  { ship: 'Vessel 14', oil_majors: 'PETRON', port: 'MALACCA', date: '10 March 2017', obs: 9 },
  { ship: 'Vessel 15', oil_majors: 'Boron', port: 'MALACCA', date: '10 March 2017', obs: 9 },
  { ship: 'Vessel 11', oil_majors: 'BP', port: 'MALACCA', date: '10 March 2017', obs: 9 },
  { ship: 'Vessel 15', oil_majors: 'PETRON', port: 'MALACCA', date: '10 March 2017', obs: 9 },
  { ship: 'Vessel 16', oil_majors: 'SHELL', port: 'MALACCA', date: '10 March 2017', obs: 9 },
  { ship: 'Vessel 11', oil_majors: 'BP', port: 'MALACCA', date: '10 March 2017', obs: 9 },
  { ship: 'Vessel 11', oil_majors: 'BP', port: 'MALACCA', date: '10 March 2017', obs: 9 },
];
