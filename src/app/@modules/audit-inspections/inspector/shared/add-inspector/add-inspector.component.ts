import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { ICustomFormlyConfig } from '@types';

@Component({
  selector: 'app-add-inspector',
  templateUrl: './add-inspector.component.html',
  styleUrls: ['./add-inspector.component.scss'],
})
export class AddInspectorComponent implements OnInit {
  @Output() closeDialog = new EventEmitter<any>();

  //#region FormlyForm Tegion Starts
  planFormKeys = {
    role: 'role',
    fname: 'fname',
    mname: 'mname',
    lname: 'lname',
    inscompany: 'inscompany',
    addnarration: 'addnarration',
    add_new: 'add_new',
  };
  planForm = new FormGroup({});
  planModel = {};
  planOptions: FormlyFormOptions = {};
  planFields: FormlyFieldConfig[] = [
    {
      key: this.planFormKeys.role,
      type: 'radio',
      templateOptions: {
        label: 'Role',
        required: true,
        options: [
          { value: 1, label: 'Inspector' },
          { value: 2, label: 'Auditor' },
        ],
      },
    },
    {
      key: this.planFormKeys.fname,
      type: 'input',
      defaultValue: '',
      templateOptions: {
        label: 'First Name',
        required: true,
        // options: [ 'Port 1', 'PORT 2'],
        change: (field: FormlyFieldConfig, event: any) => {},
      },
    },

    {
      key: this.planFormKeys.mname,
      type: 'input',
      defaultValue: '',
      templateOptions: {
        label: 'Middle Name',
        appearance: 'outline',
        required: true,
        change: (field: FormlyFieldConfig, event: any) => {},
      },
    },
    {
      key: this.planFormKeys.mname,
      type: 'input',
      defaultValue: '',
      templateOptions: {
        label: 'Last Name',
        appearance: 'outline',
        required: true,
        change: (field: FormlyFieldConfig, event: any) => {},
      },
    },
    {
      key: this.planFormKeys.inscompany,
      type: 'input',
      defaultValue: '',
      templateOptions: {
        appearance: 'outline',
        label: 'Inspecting Company',
        required: true,
        change: (field: FormlyFieldConfig, event: any) => {},
      },
    },
    // {
    //   key: this.planFormKeys.addnarration,
    //   type: 'custom',
    //   templateOptions: {
    //     label: 'Add Narration',
    //     change: (field: FormlyFieldConfig, event: any) => {},
    //   },
    // },
    {
      key: this.planFormKeys.add_new,
      type: 'input',
      defaultValue: '',
      templateOptions: {
        appearance: 'outline',
        label: 'Add New',
        required: true,
        // filter: (term: string) => of(term ? this.filterAuditTypeArray(term) : this.auditTypeArray.slice()),
      },
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      config: {
        model: this.planModel,
        fields: this.planFields,
        options: this.planOptions,
        form: this.planForm,
      },
      // onSave: () => {},
      // discard: true,
      // onDiscard: () => this.closeDialog.emit(null),
    },
  ];

  portArray = ['Port 1', 'PORT 2'];
  auditTypeArray = ['AUDIT 1', 'AUDIT 2'];

  //#endregion FormlyForm Region Ends

  constructor() {}

  /* This function is used by autocomplete field in Action section */
  filterPortArray(name: string) {
    return this.portArray.filter((state: any) => state.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  filterAuditTypeArray(name: string) {
    return this.auditTypeArray.filter((state: any) => state.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  ngOnInit(): void {}

  selectionChanged(event: any) {}
}
