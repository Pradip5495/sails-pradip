import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InspectorComponent } from './inspector.component';
import { InspectorTableComponent } from './component/inspector-table/inspector-table.component';

const inspectorRoutes: Routes = [
  {
    path: '',
    component: InspectorComponent,
    children: [
      { path: '', redirectTo: 'inspector', pathMatch: 'full' },
      {
        path: 'list',
        component: InspectorTableComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(inspectorRoutes)],
  exports: [RouterModule],
})
export class InspectorRoutingModule {}
