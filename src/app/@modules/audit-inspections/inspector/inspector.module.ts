import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { RouterModule } from '@angular/router';
// import { AgGridModule } from 'ag-grid-angular';
import { FormlyModule } from '@app/@formly';
import { ChartsModule } from 'ng2-charts';
import { InspectorRoutingModule } from './inspector-routing.module';
import { InspectorComponent } from './inspector.component';
import { InspectorTableComponent } from './component/inspector-table/inspector-table.component';
import { AddInspectorComponent } from './shared/add-inspector/add-inspector.component';

@NgModule({
  declarations: [InspectorComponent, InspectorTableComponent, AddInspectorComponent],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    RouterModule,
    ReactiveFormsModule,
    FormlyModule,
    ChartsModule,
    InspectorRoutingModule,
  ],
})
export class InspectorModule {}
