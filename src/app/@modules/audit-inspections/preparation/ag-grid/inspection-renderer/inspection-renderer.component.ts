import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { Component } from '@angular/core';
@Component({
  selector: 'inspection-label',
  template: `
    <div class="inspect__rcell">
      <label>{{ params.label }}</label>
      <div class="tag">
        <span>{{ params.tag }}</span>
      </div>
    </div>
  `,
  styles: [
    `
      .inspect__rcell {
        padding: 15px 15px 15px 0px;
        font-size: 14px;
      }

      .inspect__rcell label {
        color: #16569e;
        font-weight: bold;
      }

      div {
        line-height: normal;
      }

      .inspect__rcell div.tag {
        margin-top: 5px;
        font-size: 14px;
      }
    `,
  ],
})
export class InspectionRendererComponent implements ICellRendererAngularComp {
  params: any = {};

  agInit(params: any) {
    this.params = params.value;
  }

  refresh(params: any): boolean {
    return true;
  }
}
