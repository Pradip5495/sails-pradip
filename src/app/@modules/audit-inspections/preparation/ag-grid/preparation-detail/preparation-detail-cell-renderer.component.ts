import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { Component } from '@angular/core';
import { IJson } from '@types';

@Component({
  selector: 'app-preparation-detail-cell',
  templateUrl: './preparation-detail-cell-renderer.component.html',
  styleUrls: ['./preparation-detail-cell-renderer.component.scss'],
})
export class PreparationDetailCellRendererComponent implements ICellRendererAngularComp {
  list: IJson = [];

  agInit(params: any) {
    this.list = [...params.data.chapterData];
  }

  refresh(params: any) {
    return true;
  }

  getStatusClass(item: IJson) {
    if (item.completed.preparationCheckListCount === 0) {
      return { 'status-todo': true };
    }
    if (item.completed.preparationCheckListCount < item.completed.checkListTotQuestionCount) {
      return { 'status-inprogress': true };
    }
    if (item.completed.preparationCheckListCount === item.completed.checkListTotQuestionCount) {
      return { 'status-done': true };
    }
    const statusClasses = {
      'In Progress': 'status-inprogress',
      Done: 'status-done',
      'To Do': 'status-todo',
    };

    return { [statusClasses[status]]: true };
  }

  getStatus(item: IJson) {
    if (item.completed.preparationCheckListCount === 0) {
      return 'To Do';
    }
    if (item.completed.preparationCheckListCount < item.completed.checkListTotQuestionCount) {
      return 'In Progress';
    }
    if (item.completed.preparationCheckListCount === item.completed.checkListTotQuestionCount) {
      return 'Done';
    }
  }
}
