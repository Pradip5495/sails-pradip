import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { of } from 'rxjs';
import { ICustomFormlyConfig } from '@types';

@Component({
  selector: 'app-add-preparation',
  templateUrl: './add-preparation.component.html',
  styleUrls: ['./add-preparation.component.scss'],
})
export class AddPreparationComponent implements OnInit {
  @Output() closeDialog = new EventEmitter<any>();

  //#region FormlyForm Tegion Starts
  planFormKeys = {
    port: 'port',
    date: 'date',
    auditorScope: 'auditorScope',
    auditType: 'auditType',
    remarks: 'remarks',
  };

  planForm = new FormGroup({});
  planModel = {};
  planOptions: FormlyFormOptions = {};
  planFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: this.planFormKeys.port,
          type: 'autocomplete',
          defaultValue: '',
          templateOptions: {
            label: 'PORT',
            placeholder: 'PORT',
            appearance: 'outline',
            required: true,
            // options: [ 'Port 1', 'PORT 2'],
            change: (field: FormlyFieldConfig, event: any) => {},
            filter: (term: string) => of(term ? this.filterPortArray(term) : this.portArray.slice()),
          },
        },
        {
          className: 'flex-1',
          key: this.planFormKeys.date,
          type: 'matdatepicker',
          defaultValue: '',
          templateOptions: {
            label: 'DATE',
            placeholder: 'DATE',
            appearance: 'outline',
            required: true,
            change: (field: FormlyFieldConfig, event: any) => {},
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      cache: false,
      id: 'audit-preparation',
      legacyForm: true,
      config: {
        model: this.planModel,
        fields: this.planFields,
        options: this.planOptions,
        form: this.planForm,
      },
      onSave: () => {},
      discard: true,
      onDiscard: () => this.closeDialog.emit(null),
    },
  ];

  portArray = ['Port 1', 'PORT 2'];
  auditTypeArray = ['AUDIT 1', 'AUDIT 2'];

  //#endregion FormlyForm Region Ends

  constructor() {}

  /* This function is used by autocomplete field in Action section */
  filterPortArray(name: string) {
    return this.portArray.filter((state: any) => state.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  filterAuditTypeArray(name: string) {
    return this.auditTypeArray.filter((state: any) => state.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  ngOnInit(): void {}

  selectionChanged(event: any) {}

  discard() {
    this.planForm.reset();
    this.closeDialog.emit();
  }

  save() {
    this.planForm.markAllAsTouched();
  }
}
