import { AgGridModule } from '@ag-grid-community/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@app/material.module';
import { FormlyModule } from '@formly';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { InspectionRendererComponent } from './ag-grid/inspection-renderer/inspection-renderer.component';
import { PreparationDetailCellRendererComponent } from './ag-grid/preparation-detail/preparation-detail-cell-renderer.component';
import { PreparationDetailsComponent } from './component/preparation-details/preparation-details.component';
import { PreparationModalComponent } from './component/preparation-modal/preparation-modal.component';
import { PreparationTableComponent } from './component/preparation-table/preparation-table.component';
import { PreparationRoutingModule } from './preparation-routing.module';
import { PreparationComponent } from './preparation.component';
import { AddPreparationComponent } from './shared/add-preparation/add-preparation.component';

@NgModule({
  declarations: [
    PreparationComponent,
    PreparationTableComponent,
    PreparationModalComponent,
    PreparationDetailsComponent,
    PreparationDetailCellRendererComponent,
    AddPreparationComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,
    MatIconModule,
    PreparationRoutingModule,
    FormlyModule,
    AgGridModule.withComponents([InspectionRendererComponent]),
  ],
})
export class PreparationModule {}
