import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderService } from '@app/shell/header/header.service';
import { SidenavStoreService } from '@app/shell/sidenav/sidenav-store.service';
import { IJson } from '@types';
import { take } from 'rxjs/operators';
import { LocalStorageService, NotificationService } from '@shared/common-service';
import { PreparationService } from '../../preparation.service';
import { UserService } from '@shared';
import { environment } from '@env/environment';
import { DomSanitizer } from '@angular/platform-browser';

const env = environment;
export interface IPreparationDetails {
  sNo?: number;
  subGroups: IPreparationDetailsSubGroup[];
  border?: boolean;
}

interface IPreparationDetailsResponse {
  y?: boolean;
  n?: boolean;
  na?: boolean;
  ns?: boolean;
}

interface IRepeatLabel {
  value: number;
  label?: string;
}

interface IPreparationDetailsRepeat {
  x?: IRepeatLabel;
  y?: IRepeatLabel;
  z?: IRepeatLabel;
}

interface IPreparationDetailsSubGroup {
  subGroupId: string;
  itemToCheck: string;
  natureOfCheck: string[];
  commonFindings?: boolean; // If true, show eye icon
  risk?: string;
  repeat?: IPreparationDetailsRepeat;
  response?: IPreparationDetailsResponse;
  actions: {
    attachment?: boolean;
    chat?: boolean;
    schedule?: boolean;
  };
  assignee?: {
    label?: string;
  };
  approver?: {
    label?: string;
  };
  details?: { description: string }[];
  detailImage?: string;
}

@Component({
  selector: 'app-preparation-details',
  templateUrl: './preparation-details.component.html',
  styleUrls: [
    './preparation-details.component.scss',
    '../../../../../@shared/component/attachment/attachment.component.scss',
  ],
})
export class PreparationDetailsComponent implements OnInit, OnDestroy {
  selectedSubGroup: IPreparationDetailsSubGroup;

  showDialog = false;
  showHistoryDialog = false;
  savingChecklist = false;
  approvingChapter = false;
  fetchingHistory = false;

  stateData = ['inprogress', 'todo'];
  checkListStatusData = ['in-progress', 'todo', 'completed'];

  stateValue = '';
  userDesignationCode = '';

  uploadImageCount = 0;

  inspectionMasterId: string;
  defaultChapterId: string;
  selectedVessel: string;

  selectedCheckListIndex: number;

  selectedPreperationDetailsData: IJson;
  preparationAttachments: IJson = {};

  chapterDropdownData: Array<IJson>;
  checkListData: Array<IJson>;
  preparationChapterListData: Array<IJson>;
  attachments: IJson[] = [];
  historyList: IJson[] = [];

  showPreviewDialog: boolean;
  fileUrl: string;
  fileType: string;

  constructor(
    private readonly sidenavStore: SidenavStoreService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly headerService: HeaderService,
    private localStorageService: LocalStorageService,
    private preparationService: PreparationService,
    private notificationService: NotificationService,
    private userService: UserService,
    private domSanitizer: DomSanitizer
  ) {
    this.sidenavStore.hasSideNav = false;
    this.headerService.hasHeader = false;
  }

  ngOnInit() {
    this.setCurrentUserDesignationCode();
    this.activatedRoute.params.pipe(take(1)).subscribe((params: IJson) => {
      if (params.hasOwnProperty('inspectionMasterId') && params.hasOwnProperty('chapterId')) {
        this.inspectionMasterId = params.inspectionMasterId;
        this.defaultChapterId = params.chapterId;
      }
      this.getDataFromLocalStorage();
      if (this.inspectionMasterId && this.defaultChapterId) {
        this.getInspectionDetails(this.inspectionMasterId, this.defaultChapterId);
      }
    });
  }

  setCurrentUserDesignationCode() {
    this.userDesignationCode = this.userService.currentUser.designationCode;
  }
  currentPeriodClicked(date: any, index: number) {
    this.checkListData[index].dueDate = date.value;
  }

  getDataFromLocalStorage() {
    this.selectedPreperationDetailsData = this.localStorageService.get('preperation-data');
    if (this.selectedPreperationDetailsData) {
      if (!this.inspectionMasterId || !this.defaultChapterId) {
        this.defaultChapterId = this.selectedPreperationDetailsData.chapterData[0].chapterId;
        this.inspectionMasterId = this.selectedPreperationDetailsData.inspectionMasterId;
      }
      this.chapterDropdownData = this.selectedPreperationDetailsData.chapterData;
      this.selectedVessel = this.selectedPreperationDetailsData.vessel;
    }
  }

  getInspectionDetails(inspectionMasterId: string, chapterId: string) {
    this.preparationService
      .getPreparationCheckList(inspectionMasterId, chapterId)
      .pipe(take(1))
      .subscribe((checklistResponse: any) => {
        if (checklistResponse) {
          this.preparationChapterListData = checklistResponse;
          this.setFormData();
          // if (this.preparationChapterListData) {
          //   const checkListData = this.preparationChapterListData[0].checkListData;
          //   checkListData.map((value: any) => {
          //     value.natureOfCheck = value.natureOfCheck.replace('- ', '');
          //     value.natureOfCheck = value.natureOfCheck.split('\n-');
          //     value.attachments = [];
          //     value.attachmentFiles = [];
          //     value.status = value.status ? value.status : 'NA';
          //     value.findingEdit = false;
          //     value.dueDate = value.dueDate ? new Date(value.dueDate) : new Date();
          //   });
          //   this.setState(this.preparationChapterListData[0].state);
          //   this.checkListData = checkListData;
          // }
        }
      });

    // this.setFormData();
  }

  setFormData() {
    if (this.preparationChapterListData) {
      const checkListData = this.preparationChapterListData[0].checkListData;
      checkListData.map((value: any) => {
        value.natureOfCheck = value.natureOfCheck.replace('- ', '');
        value.natureOfCheck = value.natureOfCheck.split('\n-');
        value.attachments = [];
        value.attachmentFiles = [];
        value.status = value.status ? value.status : 'NA';
        value.findingEdit = false;
        value.dueDate = value.dueDate ? new Date(value.dueDate) : new Date();
      });
      this.setState(this.preparationChapterListData[0].state);
      this.checkListData = checkListData;
    }
  }

  onChapterChange(event: any) {
    if (event) {
      this.defaultChapterId = event.value;
      this.getInspectionDetails(this.inspectionMasterId, this.defaultChapterId);
    }
  }

  setState(state: string) {
    this.stateValue = state;
  }

  setCheckListStatus(state: string, i: number) {
    this.checkListData[i].checkListStatus = state;
  }

  getRiskClass(risk: string) {
    return risk === 'L' ? 'green' : risk === 'M' ? 'orange' : 'red';
  }

  getStatusBadgeClass(statusValue: string, badge: string) {
    if (!statusValue) {
      return 'badge-flat';
    }
    const status = statusValue;
    if (badge === 'y') {
      return status === 'YES' ? 'badge-success' : 'badge-flat';
    }

    if (badge === 'n') {
      return status === 'NO' ? 'red-bg' : 'badge-flat';
    }

    if (badge === 'na') {
      return status === 'NA' ? 'na-bg' : 'badge-flat';
    }
  }

  setStatusBadge(index: string, value: string) {
    this.checkListData[index].status = value;
  }

  onCancel(event: Event) {
    this.router.navigate(['../../../list'], { relativeTo: this.activatedRoute });
  }

  ngOnDestroy() {
    this.headerService.hasHeader = true;
  }

  showDetails(subGroup: IPreparationDetailsSubGroup) {
    // TODO: update this function later on with API
    this.showDialog = true; // subGroup.details?.length > 0;
    subGroup = {
      ...subGroup,
      details: [
        { description: 'Up to the date HVPQ not posted on OCIMF website.' },
        {
          description: 'Some relevant questions were not answered or marked \'Not Applicable\'',
        },
        {
          description: `A no. of entries were not accurate & didn\'t reflect the ship\'s fittings/conditions, -eg information regarding
        mooring ropes, STS, last Dry dock, ECDIS installation, cargo pipelines pressure test, no. of crew, unscheduled repairs, sloshing limits,
        details of technical operator (as compared to DOC) etc.`,
        },
        {
          description: 'Latest Edition of HVPQ not available on board.',
        },
      ],
      detailImage: '/assets/file.png',
    };

    this.selectedSubGroup = subGroup;
  }

  OnFileChange(files: File[], checkListItem: IJson) {
    if (files.length === 0) {
      return;
    }

    const image = { url: '' };
    const reader = new FileReader();
    reader.onload = (filedata) => {
      image.url = filedata.target.result + '';
      this.attachments.push(image);
    };
    reader.readAsDataURL(files[0]);
  }

  fileChangeForChecklist(files: any, index: number) {
    if (files.length === 0) {
      return;
    }
    const { name, type, size } = files[0] as File;
    const image = { name, type, size, url: '' };
    const reader = new FileReader();
    reader.onload = (filedata) => {
      image.url = filedata.target.result + '';
    };
    reader.readAsDataURL(files[0]);
    this.checkListData[index].attachments.push(image);
    this.checkListData[index].attachmentFiles.push(files[0]);
    this.uploadImageCount = 0;
  }

  selectedIndex(index: any) {
    if (this.uploadImageCount === 0) {
      this.selectedCheckListIndex = index;
    }
    this.uploadImageCount++;
  }

  saveChapterDetails(event: IJson) {
    this.savingChecklist = true;

    const checkListFormData = this.checkListData.map((checkList: IJson, index: number) => {
      const attachmentKeyName = 'attachmentFieldName';
      const closeDateKeyName = 'closeDate';

      const returnData = {
        checkListId: checkList.checklistId,
        status: checkList.status,
        dueDate: checkList.dueDate.toUTCString(),
        finding: checkList.finding ? checkList.finding : '',
        preparationChecklistId: checkList.preparationChecklistId,
        // TODO: update it later on after fixing attachment issues
        // attachmentFieldName: '',
        checkListStatus: checkList.checkListStatus,
        message: checkList.preparationChecklistId,
      };
      if (checkList.attachmentFiles.length > 0) {
        returnData[attachmentKeyName] = '';
      }
      if (checkList.closeDate) {
        returnData[closeDateKeyName] = checkList.closeDate;
      }
      // if (checkList.finding) {
      //   returnData['finding'] = checkList.finding;
      // };
      return returnData;
    });

    const formData = new FormData();
    formData.append('inspectionMasterId', this.inspectionMasterId);
    this.checkListData.forEach((checkList, index) => {
      if (checkList.attachmentFiles.length > 0) {
        checkList.attachmentFiles.forEach((image: any, index1: number) => {
          formData.append(`file${index}[${index1}]`, checkList.attachmentFiles[index1]);
        });
      }
    });
    checkListFormData.forEach((checkList, index) => {
      for (const key in checkList) {
        if (key === 'attachmentFieldName') {
          formData.append(`checkListData[${index}][attachmentFieldName]`, `file${index}`);
        } else {
          formData.append(`checkListData[${index}][${key}]`, checkList[key]);
        }
      }
    });

    // const preparationFormData = {
    //   inspectionMasterId: this.inspectionMasterId,
    //   checkListData: checkListFormData,
    // };

    const preparationResponse = (response: IJson) => {
      this.savingChecklist = false;
      this.notificationService.showSuccess(response.message);
      this.getInspectionDetails(this.inspectionMasterId, this.defaultChapterId);
    };

    const preparationError = (error: string) => {
      this.savingChecklist = false;
      this.notificationService.showError(error);
    };

    this.preparationService.saveChecklist(formData).subscribe(preparationResponse, preparationError);
  }

  approveSelectedChaper(event: IJson) {
    this.approvingChapter = true;
    const approveData = {
      viqChapterId: this.defaultChapterId,
      inspectionMasterId: this.inspectionMasterId,
    };

    const approveResponse = (response: IJson) => {
      this.approvingChapter = false;
      this.notificationService.showSuccess(response.message);
    };

    const approveError = (error: string) => {
      this.approvingChapter = false;
      this.notificationService.showError(error);
    };

    this.preparationService.approveChecklist(approveData).subscribe(approveResponse, approveError);
  }

  getReceivedImagePath(receivedImageData: string) {
    const indexOfPoint = receivedImageData.indexOf('.');
    let receivedFileType: string;
    if (indexOfPoint > -1) {
      receivedFileType = receivedImageData.slice(indexOfPoint + 1);
    }
    const imageTypeAssetMapping = {
      png: env.filePath + '/' + receivedImageData,
      jpg: env.filePath + '/' + receivedImageData,
      jpeg: env.filePath + '/' + receivedImageData,
      zip: '/assets/zip.png',
      ' ': '/assets/zip.png',
      pdf: '/assets/pdf.png',
    };
    return imageTypeAssetMapping[receivedFileType] || 'assets/file.png';
  }

  getFilterFile(imageType: any) {
    const imageTypeAssetMapping = {
      'image/png': imageType.url,
      'image/jpg': imageType.url,
      'image/jpeg': imageType.url,
      'application/x-zip-compressed': '/assets/zip.png',
      ' ': '/assets/zip.png',
      'application/pdf': '/assets/pdf.png',
    };
    return imageTypeAssetMapping[imageType.type] || 'assets/file.png';
  }

  /**
   * return image class based on file type
   */
  getImageStyleClass(imageType: any) {
    const imageTypeStyleMapping = {
      'image/png': 'upload-images',
      'image/jpg': 'upload-images',
      'image/jpeg': 'upload-images',
    };

    return imageTypeStyleMapping[imageType.type] || 'pdfimages';
  }

  deleteImage($event: any, attachment: any, mainIndex: any, attachmentIndex: number) {
    $event.preventDefault();
    try {
      this.checkListData[mainIndex].attachments.splice(attachmentIndex, 1);
      this.checkListData[mainIndex].attachmentFiles.splice(attachmentIndex, 1);
    } catch (ex) {}
  }

  deletePreviousImage(event: any, attachment: IJson, mainIndex: number, subIndex: number) {
    event.preventDefault();
    this.preparationService
      .deleteChecklistAttachment(attachment.auditAttachementId)
      .pipe(take(1))
      .subscribe((response: any) => {
        if (response) {
          if (response.statusCode === 200) {
            this.checkListData[mainIndex].preparationCheckListAttachment.splice(subIndex, 1);
          } else {
            this.notificationService.showError(response.message);
          }
        }
      });
  }

  getImagePath(receivedFileName: string) {
    return `${env.filePath}/${receivedFileName}`;
  }

  showPreview(receivedFileName: string) {
    const indexOfPoint = receivedFileName.indexOf('.');
    if (indexOfPoint > -1) {
      this.fileType = receivedFileName.slice(indexOfPoint + 1);
    }
    this.fileUrl = `${env.filePath}/${receivedFileName}`;

    if (this.fileType === 'json') {
      window.open(this.fileUrl, '_blank');
      return;
    }
    this.showPreviewDialog = true;
  }

  getfileUrl() {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(this.fileUrl);
  }

  openHistory(checkList: IJson) {
    this.fetchingHistory = true;
    this.preparationService.getHistory(checkList.checklistId).subscribe((historyList: IJson[]) => {
      this.historyList = historyList;
      this.showHistoryDialog = true;
      this.fetchingHistory = false;
    });
  }
}
