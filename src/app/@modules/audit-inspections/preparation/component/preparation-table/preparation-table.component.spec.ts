import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PreparationTableComponent } from './preparation-table.component';

describe('PreparationTableComponent', () => {
  let component: PreparationTableComponent;
  let fixture: ComponentFixture<PreparationTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PreparationTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
