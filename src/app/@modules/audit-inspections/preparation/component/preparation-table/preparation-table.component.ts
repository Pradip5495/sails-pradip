import { GridOptions } from '@ag-grid-community/core';
import { ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomizedCellComponent } from '@app/@shared/component/customized-cell/customized-cell.component';
import { untilDestroyed } from '@core';
import { AuditInspectionService } from '@modules/audit-inspections/audit-inspections.service';
import { NearMissService } from '@modules/incident-investigation/near-miss/share/near-miss.service';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { NotificationService, LocalStorageService } from '@shared/common-service';
import { ButtonCellRendererComponent, ProgressBarCellRendererComponent } from '@shared/component';
import { VesselModel } from '@shared/models';
import { VesselService } from '@shared/service';
import { ICustomFormlyConfig, IJson } from '@types';
import { forkJoin, Observable, of } from 'rxjs';
import { PreparationDetailCellRendererComponent } from '../../ag-grid/preparation-detail/preparation-detail-cell-renderer.component';
import { PreparationService } from '../../preparation.service';
import { columns } from './preparation-table.cell-data';

import { TwoProgressBarCellComponent } from '@shared/component/ag-grid/two-progress-bar-cell/two-progress-bar-cell.component';
import { catchError } from 'rxjs/operators';
import { CustomFormlyFieldConfig } from '@formly/fields/material/custom-field-config';
import { DateToLocalPipe } from '@shared/pipes/date-to-local.pipe';

@Component({
  selector: 'app-preparation-table',
  templateUrl: './preparation-table.component.html',
  styleUrls: ['./preparation-table.component.scss'],
  providers: [DateToLocalPipe],
})
export class PreparationTableComponent implements OnInit, OnDestroy {
  @Output() closeDialog = new EventEmitter<any>();

  icons: IJson;
  gridOptions: GridOptions;
  columnDefs = columns;
  rowData: IJson[] = [];

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    progressBar: ProgressBarCellRendererComponent,
    button: ButtonCellRendererComponent,
    detailCell: PreparationDetailCellRendererComponent,
    progressBar2: TwoProgressBarCellComponent,
  };
  defaultColDef = {
    flex: 1,
    sortable: true,
    filter: true,
  };

  newPreparationDialog = false;

  components = {
    vesselCellRenderer: this.getVesselCellRenderer(),
  };

  paginationPageSize = 10;
  vesselList: VesselModel[] = [];
  auditUtilityList: IJson[] = [];
  inspectionTypeList: IJson[] = [];
  portList: IJson[] = [];

  dynamicFormFields: IJson[] = [];
  commonFormFields = ['vesselId', 'inspectionTypeId', 'inspectionType', 'portId', 'dateOfInspection'];

  preparationForm = new FormGroup({
    vesselId: new FormControl('', [Validators.required]),
    inspectionTypeId: new FormControl('', [Validators.required]),
    inspectionType: new FormControl('', [Validators.required]),
  });
  preparationModel = {};
  preparationOptions: FormlyFormOptions = {};
  preparationFields: FormlyFieldConfig[] = [];

  formlyConfig: ICustomFormlyConfig = {
    cache: false,
    id: 'audit-preparation',
    legacyForm: true,
    config: {
      model: this.preparationModel,
      fields: this.preparationFields,
      options: this.preparationOptions,
      form: this.preparationForm,
    },
    onSave: () => {},
    discard: true,
    onDiscard: () => this.closeDialog.emit(null),
  };

  loadingUtilityData = false;
  savingPreparationForm = false;
  planningData: IJson = {};

  get suppressPaginationControls() {
    return this.rowData.length <= this.paginationPageSize;
  }

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private vesselService: VesselService,
    private auditService: AuditInspectionService,
    private nearMissService: NearMissService,
    private cdr: ChangeDetectorRef,
    private preparationService: PreparationService,
    private notifyService: NotificationService,
    private localStorageService: LocalStorageService,
    private dateToLocal: DateToLocalPipe
  ) {
    this.gridOptions = {
      getRowHeight: (params: any) => {
        if (params.node.detail) {
          return params.node.data.chapterData.length * 75;
        }
        return 70;
      },
      detailCellRendererParams: {
        autoHeight: true,
      },
      suppressContextMenu: true,
    };
  }

  ngOnInit() {
    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
      sortUnSort: '<span class="ag-icon ag-icon-desc"></span>',
    };

    this.activatedRoute.paramMap.subscribe((params) => {
      const _planningData = params.get('planningData');
      if (_planningData) {
        this.planningData = JSON.parse(atob(params.get('planningData')));
        this.showPlanningPreparationForm();
      } else {
        this.initPreparationForm();
      }
    });
  }

  initPreparationForm() {
    this.setFormFields();
    this.addFormFields();
  }

  showPlanningPreparationForm() {
    if (this.planningData.inspectionMasterId) {
      this.auditService.getInspectionMaster(this.planningData.inspectionMasterId).subscribe((inspectionDetail) => {
        this.planningData = {
          ...inspectionDetail[0],
          ...this.planningData,
        };

        this.initPreparationForm();
        this.showPreparationForm(this.planningData);
      });
    } else {
      this.planningData = {};
    }
  }

  onGridReady(params: IJson) {
    this.getPreparationList();
  }

  getPreparationList() {
    this.gridOptions.api.showLoadingOverlay();
    this.preparationService
      .getPreparationList()
      .pipe(untilDestroyed(this))
      .subscribe((preparationList: IJson[]) => {
        this.rowData = this.getFormattedPreparationList(preparationList);
        if (this.gridOptions.api) {
          this.gridOptions.api.hideOverlay();
        }
      });
  }

  getFormattedPreparationList(preparationList: IJson[]) {
    return preparationList.map((prepItem) => {
      prepItem.action = {
        disabled: prepItem.isRequested === 0,
      };
      prepItem.tasksCompleted = prepItem.progress.checkListCompletion;
      prepItem.timeElapsed = prepItem.progress.timeScale;
      return prepItem;
    });
  }

  getVesselCellRenderer() {
    const _this = this;
    function VesselCellRenderer() {}
    VesselCellRenderer.prototype.init = function (params: IJson) {
      const vesselDiv = document.createElement('div');
      vesselDiv.onclick = (e: IJson) => {
        if (params.data) {
          const chapterId = params.data.chapterData[0].chapterId;
          const inspectionMasterId = params.data.inspectionMasterId;
          _this.localStorageService.set('preperation-data', params.data);
          return _this.router.navigate([`../details/${inspectionMasterId}/${chapterId}`], {
            relativeTo: _this.activatedRoute,
          });
        }
      };
      vesselDiv.innerHTML = '<div class="cursor-pointer">' + params.value + '</div>';
      this.eGui = vesselDiv;
    };
    VesselCellRenderer.prototype.getGui = function () {
      return this.eGui;
    };
    return VesselCellRenderer;
  }

  onInspectionTypeChanged(event: MatSelectChange) {
    const selectedInspectionType = this.auditUtilityList.filter((item) => item.inspectionTypeId === event.value);
    if (selectedInspectionType.length === 1) {
      this.removeUncommonFormGroupControls();
      this.removeDynamicForms();

      this.preparationForm.patchValue({ inspectionType: selectedInspectionType[0].type });

      if (selectedInspectionType[0].subInspectionType && selectedInspectionType[0].subInspectionType.length > 0) {
        this.setSubInspectionTypeField(selectedInspectionType[0].subInspectionType);
      } else {
        this.removeSubInspectionType();
        this.setAttributeFormFields(selectedInspectionType[0].inspectionAttributesDetails);
      }
      this.addFormFields();
    }
  }

  removeUncommonFormGroupControls() {
    for (const control of Object.keys(this.preparationForm.controls)) {
      if (!this.commonFormFields.includes(control)) {
        this.preparationForm.removeControl(control);
      }
    }
  }

  removeDynamicForms() {
    this.dynamicFormFields = this.dynamicFormFields.filter((formField) => formField.formKey !== 'dynamicForm');
  }

  showPreparationForm(event?: IJson) {
    this.loadingUtilityData = true;
    this.removeDynamicForms();
    this.addFormFields();
    forkJoin([this.auditService.getUtility(), this.vesselService.getVessels()])
      .pipe(untilDestroyed(this))
      .subscribe(([utilities, vessels]: [IJson[], VesselModel[]]) => {
        this.loadingUtilityData = false;
        this.newPreparationDialog = true;
        this.auditUtilityList = utilities.filter((utility) => utility.planType !== 'surprise');
        this.vesselList = vessels;
        this.preparationForm.patchValue({
          vesselId: event.vesselId || this.vesselList[0].vesselId,
          inspectionTypeId: event.inspectionTypeMasterId || this.auditUtilityList[0].inspectionTypeId,
          inspectionType: event.type || this.auditUtilityList[0].type,
        });
        this.setSubInspectionTypeField(this.auditUtilityList[0].subInspectionType);
        this.addFormFields();
      });
  }

  discard() {
    for (const control of Object.keys(this.preparationForm.controls)) {
      if (!['vesselId', 'inspectionTypeId'].includes(control)) {
        this.preparationForm.controls[control].reset();
      }
    }
    this.closeDialog.emit();
  }

  save() {
    this.preparationForm.markAllAsTouched();
    if (this.preparationForm.invalid) {
      return;
    }

    const preparationData = { ...this.preparationForm.value };

    if (preparationData.subInspectionTypeId) {
      preparationData.inspectionTypeId = preparationData.subInspectionTypeId;
      delete preparationData.subInspectionTypeId;
    }

    if (this.planningData.inspectionMasterId) {
      preparationData.inspectionMasterId = this.planningData.inspectionMasterId;
    }

    this.savingPreparationForm = true;

    this.preparationService
      .save(preparationData)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response: IJson) => {
          this.savingPreparationForm = false;
          this.newPreparationDialog = false;
          this.notifyService.showSuccess(response.message);
          this.planningData = {};
          this.getPreparationList();
        },
        (error) => {
          this.savingPreparationForm = false;
          this.notifyService.showError(error);
        }
      );

    this.preparationForm.markAllAsTouched();
  }

  async setFormFields() {
    await this.setPortFormField();
    this.setDateFormField();
  }

  async setPortFormField() {
    const portList = await this.nearMissService.getPortList().toPromise();

    const getDefaultPortId = () => {
      return this.planningData.portId || this.preparationForm.value.portId || '';
    };

    this.dynamicFormFields.push({
      className: 'flex-1',
      key: 'portId',
      type: 'select',
      defaultValue: getDefaultPortId(),
      templateOptions: {
        label: 'PORT',
        placeholder: 'PORT',
        appearance: 'outline',
        required: true,
        options: portList,
        valueProp: 'portId',
        labelProp: 'name',
      },
    });
  }

  setDateFormField() {
    const getDefaultInspectionDate = () => {
      if (this.planningData && this.preparationForm) {
        // debugger;

        // TODO: refactor later
        let inspectionDate: any = this.planningData.dateOfInspection
          ? new Date(this.planningData.dateOfInspection)
          : '';
        inspectionDate = inspectionDate || this.preparationForm.value.dateOfInspection || '';

        // const localDate = this.dateToLocal.transform(inspectionDate.toISOString());

        if (inspectionDate) {
          return inspectionDate ? new Date(this.dateToLocal.transform(inspectionDate.toDateString())) : '';
        }
        return '';
      }
      return '';
    };

    this.dynamicFormFields.push({
      className: 'flex-1',
      key: 'dateOfInspection',
      type: 'matdatepicker',
      defaultValue: getDefaultInspectionDate(),
      templateOptions: {
        label: 'Inspection Date',
        placeholder: 'Inspection Date',
        appearance: 'outline',
        required: true,
        datepickerOptions: {
          min: new Date(),
          dateChange: (field: FormlyFieldConfig, event: IJson) => {},
        },
      },
    });
  }

  removeSubInspectionType() {
    this.dynamicFormFields = this.dynamicFormFields.filter((formField) => formField.key !== 'subInspectionTypeId');
  }

  setSubInspectionTypeField(items: IJson[] = []) {
    this.removeSubInspectionType();

    const getDefaultSubInspectionTypeId = () => {
      return this.planningData.inspectionTypeSubId || this.preparationForm.value.subInspectionTypeId || '';
    };

    const setFormFields = (selectedSubInspectionType: IJson) => {
      this.preparationForm.patchValue({
        inspectionType: selectedSubInspectionType.type,
        subInspectionTypeId: selectedSubInspectionType.inspectionTypeId,
      });
      this.setAttributeFormFields(selectedSubInspectionType.inspectionAttributesDetails);
    };

    const defaultValue = getDefaultSubInspectionTypeId();

    if (defaultValue) {
      const selectedSubInspectionType = items.filter((item) => item.inspectionTypeId === defaultValue);
      setFormFields(selectedSubInspectionType[0]);
    }

    this.dynamicFormFields.push({
      className: 'flex-1',
      key: 'subInspectionTypeId',
      type: 'matselect',
      defaultValue: getDefaultSubInspectionTypeId(),
      templateOptions: {
        label: 'Sub Inspection Type',
        placeholder: 'Sub Inspection Type',
        appearance: 'outline',
        required: true,
        options: items,
        valueProp: 'inspectionTypeId',
        labelProp: 'type',
        disabled: !!this.planningData.inspectionTypeSubId,
        onChange: (field: FormlyFieldConfig, event: IJson, item: IJson) => {
          setFormFields(item);
        },
      },
    });
  }

  setAttributeFormFields(attrArray: IJson[]) {
    this.dynamicFormFields = this.dynamicFormFields.filter((field) => field.formKey !== 'dynamicForm');

    const dataSourceCalls: Observable<any>[] = [];
    const dataSourceItems: string[] = [];
    attrArray.forEach((attr: IJson) => {
      dataSourceCalls.push(this.getDataSourceData(attr.inspectionAttributeId).pipe(catchError((err) => of([]))));
      dataSourceItems.push(attr.dataSource);
    });

    forkJoin(dataSourceCalls).subscribe((dataSourceResponse) => {
      attrArray.forEach((attr: IJson, index: number) => {
        // TODO: refactor this later
        if (attr.inspectionAttributeId === 'aa1da347-cafd-4b06-993f-74511d969958') {
          attr.bindTo = 'designationId';
          attr.bindKey = 'designation';
        }

        const getDefaultAttributeValue = () => {
          return this.planningData[attr.attributeKey] || this.preparationForm.value[attr.dataSource] || '';
        };

        this.dynamicFormFields.push({
          formKey: 'dynamicForm',
          className: 'flex-1',
          key: attr.attributeKey,
          type: attr.dataType === 'User Input List' || attr.dataType === 'select' ? 'matselect' : 'input',
          defaultValue: getDefaultAttributeValue(),
          templateOptions: {
            label: attr.labelName,
            placeholder: attr.labelName,
            appearance: 'outline',
            readonly: attr.dataType === 'text',
            required: attr.isMandatory === 1,
            options: dataSourceResponse[index],
            valueProp: 'value',
            labelProp: 'key',
            multiple: attr.isMultiSelectAllow === 1,
            onChange: (field: CustomFormlyFieldConfig, event: IJson, item: IJson) => {
              if (attr.bindTo && attr.bindKey) {
                field.form.patchValue({
                  [attr.bindTo]: field.fieldObject[attr.bindKey],
                });
              }
            },
          },
        });
      });
      this.addFormFields();
    });
  }

  getDataSourceData(inspectionAttributeId: string) {
    return this.auditService.getDataSourceData(inspectionAttributeId);
  }

  addFormFields() {
    const formFields: IJson[] = [];
    const fields = this.dynamicFormFields.chunk(2);

    this.preparationForm.clearValidators();

    fields.forEach((innerfields: IJson[]) => {
      const groupFields: FormlyFieldConfig = {
        fieldGroupClassName: 'display-flex',
        fieldGroup: [],
      };

      innerfields.forEach((field: IJson) => {
        groupFields.fieldGroup.push(field);
      });

      formFields.push(groupFields);
    });

    this.preparationFields = [...formFields];

    this.formlyConfig.config.fields = [...this.preparationFields];

    this.cdr.detectChanges();
  }

  ngOnDestroy() {}
}
