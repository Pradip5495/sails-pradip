import { IJson } from '@types';
import * as moment from 'moment';

export const columns = [
  {
    headerName: 'VESSEL',
    field: 'vessel',
    cellRenderer: 'agGroupCellRenderer',
    headerTooltip: 'Vessel',
    unSortIcon: true,
    suppressMenu: true,
    cellClass: ['vessel-cell'],
    cellStyle: (params: any) => {
      if (params.column.colId === 'vessel') {
        return {
          fontWeight: '500',
          fontSize: '14px',
        };
      }
    },
    flex: 1.5,
    cellRendererParams: {
      innerRenderer: 'vesselCellRenderer',
    },
  },
  {
    headerName: 'INSPECTION',
    field: 'inspectionType',
    unSortIcon: true,
    suppressMenu: true,
    headerTooltip: 'Inspection Type',
    cellClass: ['inspection-cell'],
  },
  {
    headerName: 'PROGRESS',
    field: 'status',
    unSortIcon: true,
    cellRenderer: 'progressBar',
    suppressMenu: true,
    cellStyle: (params: any) => {
      return {
        paddingTop: '4px',
      };
    },
    headerTooltip: 'Progress',
    flex: 1.0,
    hide: true,
  },
  {
    headerName: 'PROGRESS',
    headerClass: 'header-center',
    field: 'status',
    unSortIcon: true,
    cellRenderer: 'progressBar2',
    suppressMenu: true,
    cellStyle: (params: any) => {
      return {
        paddingTop: '20px',
        textAlign: 'center',
      };
    },
    headerTooltip: 'Progress',
    flex: 1.0,
  },
  {
    headerName: 'DATE',
    field: 'dateOfInspection',
    headerCheckboxSelectionFilteredOnly: true,
    unSortIcon: true,
    suppressMenu: true,
    cellStyle: (params: any) => {
      return {
        padding: '10px 0px 15px 20px',
        fontSize: '14px',
      };
    },
    headerTooltip: 'Date',
    flex: 0.7,
    valueFormatter: (data: IJson) => {
      return data.value ? moment.utc(data.value).local().format('DD MMMM yyy') : '';
    },
  },
  {
    headerName: 'PORT',
    field: 'port',
    unSortIcon: true,
    suppressMenu: true,
    cellStyle: (params: any) => {
      return {
        padding: '10px 0px 15px 20px',
        fontSize: '14px',
        fontWeight: '500',
      };
    },
    headerTooltip: 'Port',
  },
  {
    headerName: 'Action',
    field: 'action',
    suppressMenu: true,
    sortable: false,
    cellRenderer: 'button',
    cellRendererParams: {
      label: 'Request',
      className: 'prep-action-btn',
      matIcon: 'forward',
    },
    cellStyle: (params: any) => {
      return { padding: '10px' };
    },
    flex: 0.7,
  },
];
