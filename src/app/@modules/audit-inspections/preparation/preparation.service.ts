import { EventEmitter, Injectable } from '@angular/core';
import { HttpDispatcherService } from '@core';
import { PreparationModel } from '@shared/models/preparation-model.model';
import { IJson } from '@types';

const routes = {
  getAll: () => `/preparation`,
  save: () => `/preparation`,
  getCheckList: (inspectionId: string, chapterId: string) => `/preparation/checklist/${inspectionId}/${chapterId}`,
  saveChecklist: () => `/preparation/checklist`,
  deleteChecklistAttachment: (id: string) => `/preparation/checklist/attachment/${id}`,
  approveChecklist: () => `/preparation/checklist/approve`,
  getHistory: (checklistid: string) => `/preparation/checklist/history/${checklistid}`,
};

@Injectable({
  providedIn: 'root',
})
export class PreparationService {
  private readonly updateVesselDetail = new EventEmitter<any>(null);

  get updateVesselDetail$() {
    return this.updateVesselDetail.asObservable();
  }

  set vesselDetail(value: IJson) {
    this.updateVesselDetail.next(value);
  }

  constructor(private httpDispatch: HttpDispatcherService) {}

  save(preparationData: PreparationModel) {
    return this.httpDispatch.post(routes.save(), preparationData);
  }

  saveChecklist(checklistData: IJson) {
    return this.httpDispatch.post(routes.saveChecklist(), checklistData);
  }

  deleteChecklistAttachment(attachmentId: string) {
    return this.httpDispatch.delete(routes.deleteChecklistAttachment(attachmentId));
  }

  approveChecklist(checklistData: IJson) {
    return this.httpDispatch.post(routes.approveChecklist(), checklistData);
  }

  getPreparationList() {
    return this.httpDispatch.get(routes.getAll(), { acceptNullResponse: true });
  }

  getPreparationCheckList(inspectionMasterId: string, chapterId: string) {
    return this.httpDispatch.get(routes.getCheckList(inspectionMasterId, chapterId));
  }

  getHistory(checklistid: string) {
    return this.httpDispatch.get(routes.getHistory(checklistid));
  }
}
