import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PreparationDetailsComponent } from './component/preparation-details/preparation-details.component';
import { PreparationComponent } from './preparation.component';
import { PreparationTableComponent } from './component/preparation-table/preparation-table.component';

const preparationRoutes: Routes = [
  {
    path: '',
    component: PreparationComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      {
        path: 'list',
        component: PreparationTableComponent,
      },
      {
        path: 'details/:inspectionMasterId/:chapterId',
        component: PreparationDetailsComponent,
      },
      {
        path: 'list/:planningData',
        component: PreparationTableComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(preparationRoutes)],
  exports: [RouterModule],
})
export class PreparationRoutingModule {}
