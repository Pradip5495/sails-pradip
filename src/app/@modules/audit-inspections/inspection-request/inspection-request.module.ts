import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { InspectionRequestComponent } from './inspection-request.component';
import { InspectionRequestRoutingModule } from './inspection-request-routing.module';
import {
  InspectionCloseoutStatusComponent,
  InspectionRequestFormComponent,
} from '@modules/audit-inspections/inspection-request/component';
import { FormlyModule } from '@formly';
import { InspectionRemarksComponent } from './component/inspection-closeout-status/inspection-remarks/inspection-remarks.component';

@NgModule({
  declarations: [
    InspectionRequestComponent,
    InspectionRequestFormComponent,
    InspectionCloseoutStatusComponent,
    InspectionRemarksComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    InspectionRequestRoutingModule,
    FormlyModule,
  ],
})
export class InspectionRequestModule {}
