import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionCloseoutStatusComponent } from './inspection-closeout-status.component';

describe('InspectionCloseoutStatusComponent', () => {
  let component: InspectionCloseoutStatusComponent;
  let fixture: ComponentFixture<InspectionCloseoutStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InspectionCloseoutStatusComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionCloseoutStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
