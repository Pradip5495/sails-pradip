import { Router } from '@angular/router';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { VesselService } from '@shared/service';
import { Logger } from '@core';
import { AgGridAngular } from '@ag-grid-community/angular';
import { VesselSidebarComponent } from '@shared/component/vessel-sidebar/vessel-sidebar.component';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import {
  CustomUiAgGridCellComponent,
  CustomAgGridCellComponent,
} from '@modules/audit-inspections/planning-approvals/shared';
import { AgCellButtonComponent } from '@shared/component/ag-cell-button/ag-cell-button.component';
import { CustomHeaderSelectComponent } from '@shared/component/custom-header-select/custom-header-select.component';
import { ButtonCellRendererComponent } from '@shared/component';

const log = new Logger('InspectionRequest');

@Component({
  selector: 'app-inspection-closeout-status',
  templateUrl: './inspection-closeout-status.component.html',
  styleUrls: ['./inspection-closeout-status.component.scss'],
})
export class InspectionCloseoutStatusComponent implements OnInit {
  public gridApi: any;
  public gridColumnApi: any;
  public sortingOrder: any;

  @ViewChild('agGrid') agGrid: AgGridAngular;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  // Notify parent for Page Metadata Setup.
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() activePageIndex: EventEmitter<number> = new EventEmitter<number>();

  dataSource: any;
  isLoading = false;
  isdataSource = false;
  public array: any;
  public length = 100;
  public rowData: any;
  public sideBar: any;
  public icons: any;
  public columnName: any;
  public vesselId: any;
  rowDataClicked1 = {};
  deleteData: any;
  title: string;
  error: string | undefined;

  vesselTypeValue: any = [];
  vesselTypeValueWithId: any = [];
  vesselOwnerValue: any = [];
  vesselOwnerValueWithId: any = [];
  fleetNameValue: any = [];
  fleetNameValueWithId: any = [];
  portNameValue: any = [];
  portNameValueWithId: any = [];
  fleetManagerValue: any = [];
  fleetManagerValueWithId: any = [];

  editType = '';
  domLayout = 'autoHeight';
  columnDefs = [
    {
      headerName: '',
      headerClass: 'group-header-custom-light border-right-1px-c2',
      children: [
        {
          headerClass: 'no-border-top border-right-1px-c2 no-group custom-class',
          headerName: 'VESSEL',
          field: 'vessels',
          cellClass: 'bold-text',
          flex: 1,
          resizable: true,
          width: 120,
          filter: 'agTextColumnFilter',
          menuTabs: ['generalMenuTab', 'filterMenuTab'],
        },
      ],
    },
    {
      headerName: '',
      headerClass: 'group-header-custom-light border-right-1px-c2',
      children: [
        {
          headerClass: 'no-border-top border-right-1px-c2 no-group custom-class',
          headerName: 'OIL MAJOR',
          field: 'oilmajor',
          cellClass: 'bold-text',
          cellStyle: { color: 'red' },
          flex: 1,
          resizable: true,
          width: 120,
          filter: 'agTextColumnFilter',
          menuTabs: ['generalMenuTab', 'filterMenuTab'],
        },
      ],
    },
    {
      headerName: '',
      headerClass: 'group-header-custom-light border-right-1px-c2',
      children: [
        {
          headerClass: 'no-border-top border-right-1px-c2 no-group custom-class',
          headerName: 'OBS',
          cellClass: '',
          field: 'obs',
          resizable: true,
          width: 130,
          filter: 'agTextColumnFilter',
          menuTabs: ['generalMenuTab', 'filterMenuTab'],
        },
      ],
    },
    {
      headerName: 'DATE',
      headerClass: 'group-header-custom-light border-right-1px-c2',
      children: [
        {
          headerName: 'INSPECTED',
          headerClass: 'border-right-1px-c2 custom-class',
          width: 150,
          flex: 1,
          field: 'inspected',
          resizable: true,
          cellClass: '',
          suppressSizeToFit: true,
        },
        {
          headerName: 'SIRE REP RECD',
          headerClass: 'border-right-1px-c2 custom-class',
          field: 'sire',
          width: 150,
          flex: 1,
          resizable: true,
          cellClass: '',
          columnGroupShow: 'open',
        },
        {
          headerName: 'SIRE + 14 DAYS',
          headerClass: 'border-right-1px-c2 custom-class',
          field: 'sire14',
          width: 150,
          flex: 1,
          resizable: true,
          cellClass: '',
          columnGroupShow: 'open',
        },
        {
          headerName: 'SUBMITTED',
          headerClass: 'border-right-1px-c2 custom-class',
          field: 'submit',
          width: 150,
          flex: 1,
          resizable: true,
          cellClass: '',
          columnGroupShow: 'open',
        },
      ],
    },

    {
      headerName: '',
      headerClass: 'group-header-custom-light border-right-1px-c2',
      children: [
        {
          headerName: 'PIC',
          headerClass: 'no-border-top border-right-1px-c2 custom-class no-group',
          field: 'pic',
          cellClass: '',
          width: 130,
          resizable: true,
          filter: 'agTextColumnFilter',
          menuTabs: ['generalMenuTab', 'filterMenuTab'],
        },
      ],
    },
    {
      headerName: 'INVOICE',
      headerClass: 'group-header-custom-light border-right-1px-c2',
      children: [
        {
          headerName: 'RECIEVED',
          headerClass: 'border-right-1px-c2 custom-class',
          width: 150,
          field: 'recieved',
          flex: 1,
          resizable: true,
          cellClass: '',
          suppressSizeToFit: true,
        },
        {
          headerName: 'PAID',
          headerClass: 'border-right-1px-c2 custom-class',
          width: 150,
          field: 'paid',
          flex: 1,
          resizable: true,
          cellClass: '',
          columnGroupShow: 'open',
        },
      ],
    },
    {
      headerName: '',
      headerClass: 'group-header-custom-light border-right-1px-c2',
      children: [
        {
          headerName: 'STATUS',
          headerClass: 'border-right-1px-c2 no-border-top custom-class no-group',
          field: 'status',
          width: 120,
          cellStyle: { color: 'mediumseagreen' },
          cellClass: 'bold-text',
          flex: 1,
          resizable: true,
          filter: 'agNumberColumnFilter',
          menuTabs: ['generalMenuTab', 'filterMenuTab'],
        },
      ],
    },
    {
      headerName: '',
      headerClass: 'group-header-custom-light border-right-1px-c2',
      children: [
        {
          headerName: 'REMARK',
          headerClass: 'border-right-1px-c2 no-border-top custom-class no-group',
          field: 'remark',
          width: 150,
          resizable: true,
          filter: 'agNumberColumnFilter',
          menuTabs: ['generalMenuTab', 'filterMenuTab'],
          cellRenderer: 'cellButton',
          cellRendererParams: {
            // label: 'View',
            // className: 'view-btn',
            icon: '',
            label: 'View',
            buttonType: 'stroked',
            onClick: function (params: any) {
              this.displayDialog = false;
              setTimeout(() => {
                this.displayDialog = true;
              }, 10);
            }.bind(this),
          },
          // cellRenderer: (params: any)=>{
          //     return `<button class="cellbtn" mat-button>VIEW</button>`
          // }
        },
      ],
    },
    {
      headerName: '',
      headerClass: 'group-header-custom-light',
      children: [
        {
          headerName: '',
          headerClass: 'no-border-top custom-class no-group',
          field: '',
          width: 50,
          cellRenderer: 'customizedBtnCell',
          cellRendererParams: {
            onClick(params: any) {},
            onClickEdit(params: any) {},
            hideDetailsButton: true,
            hideDeleteIcon: true,
          },
        },
      ],
    },
  ];

  headerHeight = 32;
  groupHeaderHeight = 32;

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    customStatsToolPanel: VesselSidebarComponent,
    customHtml: CustomUiAgGridCellComponent,
    scaleGridComponent: CustomAgGridCellComponent,
    cellButton: AgCellButtonComponent,
    selectHeader: CustomHeaderSelectComponent,
    button: ButtonCellRendererComponent,
  };

  defaultColDef = {
    enableValue: true,
    sortable: true,
    filter: true,
  };

  paginationPageSize: number;

  displayDialog: boolean;

  selectedButton = '2nd';

  constructor(private readonly vesselService: VesselService, private router: Router) {}

  onCellValueChanged(params: any) {}

  onBtnClick1(e: any) {
    this.rowDataClicked1 = e.rowData;
    alert(this.rowDataClicked1);
  }

  onGridReady(params: any) {
    this.rowData = [
      {
        vessels: 'Vessel 01',
        oilmajor: 'ENOC',
        obs: '3',
        inspected: '21 Mar 2020',
        sire: '26 Mar 2020',
        sire14: '09 JUN 2020',
        submit: 'Toyota',
        pic: 'Celica',
        recieved: '35000',
        paid: '45000',
        status: 'IN PROGRESS',
      },
      {
        vessels: 'Vessel 02',
        oilmajor: 'ENOC',
        obs: '6',
        inspected: '21 Mar 2020',
        sire: '26 Mar 2020',
        sire14: '09 JUN 2020',
        submit: 'Toyota',
        pic: 'Celica',
        recieved: '35000',
        paid: '45000',
        status: 'IN PROGRESS',
      },
      {
        vessels: 'Vessel 03',
        oilmajor: 'ENOC',
        obs: '3',
        inspected: '21 Mar 2020',
        sire: '26 Mar 2020',
        sire14: '09 JUN 2020',
        submit: 'Toyota',
        pic: 'Celica',
        recieved: '35000',
        paid: '45000',
        status: 'IN PROGRESS',
      },
      {
        vessels: 'Vessel 04',
        oilmajor: 'ENOC',
        obs: '8',
        inspected: '21 Mar 2020',
        sire: '26 Mar 2020',
        sire14: '09 JUN 2020',
        submit: 'Toyota',
        pic: 'Celica',
        recieved: '35000',
        paid: '45000',
        status: 'IN PROGRESS',
      },
      {
        vessels: 'Vessel 05',
        oilmajor: 'ENOC',
        obs: '1',
        inspected: '21 Mar 2020',
        sire: '26 Mar 2020',
        sire14: '09 JUN 2020',
        submit: 'Toyota',
        pic: 'Celica',
        recieved: '35000',
        paid: '45000',
        status: 'IN PROGRESS',
      },
      {
        vessels: 'Vessel 06',
        oilmajor: 'ENOC',
        obs: '5',
        inspected: '21 Mar 2020',
        sire: '26 Mar 2020',
        sire14: '09 JUN 2020',
        submit: 'Toyota',
        pic: 'Celica',
        recieved: '35000',
        paid: '45000',
        status: 'IN PROGRESS',
      },
      {
        vessels: 'Vessel 07',
        oilmajor: 'ENOC',
        obs: '8',
        inspected: 'Toyota',
        sire: '26 Mar 2020',
        sire14: '09 JUN 2020',
        submit: 'Toyota',
        pic: 'Celica',
        recieved: '35000',
        paid: '45000',
        status: 'IN PROGRESS',
      },
    ];
  }

  deleteVessel(params: any) {}

  fetchData() {}

  onAddForm() {
    this.router.navigateByUrl('/admin/vessel/create').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  ngOnInit(): void {
    this.pageSubTitle.emit('ALL VESSELS');
    this.activePageIndex.emit(0);

    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };

    this.paginationPageSize = 5;

    this.sideBar = {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'columns',
          iconKey: 'columns',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
          },
        },
        {
          id: 'customStats',
          labelDefault: 'Vessels',
          labelKey: 'customStats',
          iconKey: 'custom-stats',
          filter: true,
          toolPanel: 'customStatsToolPanel',
        },
      ],
      filter: true,
      position: 'right',
    };
  }

  vesselTypeList() {
    this.vesselService.getVesselTypes().subscribe(
      (data) => {
        this.vesselTypeValueWithId = data;
        for (const i of data) {
          this.vesselTypeValue.push(i.vesselType);
        }
      },
      (error) => {
        log.error(error);
      }
    );
  }

  onRowDataChanged($params: any) {}

  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map((node) => node.data);
    const selectedDataStringPresentation = selectedData.map((node) => node.make + ' ' + node.model).join(', ');
    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  }

  /**
   * @deprecated only for example purpose to copy in other modules.
   */
  anyFunction() {
    log.debug('AnyFunction Called');
  }

  discard(event: any) {
    this.displayDialog = false;
  }
}
