import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionRemarksComponent } from './inspection-remarks.component';

describe('InspectionRemarksComponent', () => {
  let component: InspectionRemarksComponent;
  let fixture: ComponentFixture<InspectionRemarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InspectionRemarksComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionRemarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
