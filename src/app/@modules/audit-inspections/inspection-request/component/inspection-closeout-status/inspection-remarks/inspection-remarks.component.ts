import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-inspection-remark',
  templateUrl: './inspection-remarks.component.html',
  styleUrls: [
    './inspection-remarks.component.scss',
    '../../../../planning-approvals/shared/add-plan/add-plan.component.scss',
  ],
})
export class InspectionRemarksComponent implements OnInit {
  @Output() closeDialog = new EventEmitter<any>();

  tableData = [
    {
      date: '16 Jan 2012',
      oilMajor: 'OIL Major',
      port: 'Sjdlasdfjlk',
      obs: 3,
    },
    {
      date: '16 Jan 2012',
      oilMajor: 'OIL Major',
      port: 'Sjdlasdfjlk',
      obs: 3,
    },
    {
      date: '16 Jan 2012',
      oilMajor: 'OIL Major',
      port: 'Sjdlasdfjlk',
      obs: 3,
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
