import { Component, OnInit } from '@angular/core';
import { ICustomFormlyConfig } from '@types';
import { FormlyFieldConfig } from '@formly';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-inspection-request-form',
  templateUrl: './inspection-request-form.component.html',
  styleUrls: ['./inspection-request-form.component.scss'],
})
export class InspectionRequestFormComponent implements OnInit {
  model: any = {};
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex flex-row-style',
      fieldGroup: [
        {
          key: 'vessel',
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            required: true,
            throwError: 'Vessel is required',
            options: [
              { vesselType: 'Vessel 10', vesselTypeId: '12345' },
              { vesselType: 'Vessel 12', vesselTypeId: '12345' },
              { vesselType: 'Vessel 13', vesselTypeId: '12345' },
              { vesselType: 'Vessel 14', vesselTypeId: '12345' },
            ],
            valueProp: 'vesselTypeId',
            labelProp: 'vesselType',
          },
        },
        {
          key: 'inspectionType',
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            required: true,
            throwError: 'Inspection Type is required',
            options: [
              { inspectionType: 'Inspection 10', inspectionTypeId: '12345' },
              { inspectionType: 'Inspection 12', inspectionTypeId: '12345' },
              { inspectionType: 'Inspection 13', inspectionTypeId: '12345' },
              { inspectionType: 'Inspection 14', inspectionTypeId: '12345' },
            ],
            valueProp: 'inspectionTypeId',
            labelProp: 'inspectionType',
          },
        },
        {
          key: 'oilMajor',
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            required: true,
            throwError: 'Oil Major is required',
            options: [
              { oilMajorType: 'Oil Major 10', oilMajorTypeId: '12345' },
              { oilMajorType: 'Oil Major 12', oilMajorTypeId: '12345' },
              { oilMajorType: 'Oil Major 13', oilMajorTypeId: '12345' },
              { oilMajorType: 'Oil Major 14', oilMajorTypeId: '12345' },
            ],
            valueProp: 'oilMajorTypeId',
            labelProp: 'oilMajorType',
          },
        },
        {
          type: 'button',
          className: 'btn-right flex-1 right-align-btn',
          templateOptions: {
            label: 'Create',
            text: '',
            btnType: 'create',
            color: 'success',
            className: 'success-button',
            onClick: ($event: any) => {},
          },
        },
      ],
    },
  ];
  newInspectionRequestFormGroup: FormGroup = new FormGroup({});
  /*
   * formly can be array or json object
   * If it is array, consider that there are mat steps, otherwise a plain form
   */
  formlyConfig: ICustomFormlyConfig[] = [
    {
      config: {
        model: this.model,
        fields: this.fields,
        options: {},
        form: this.newInspectionRequestFormGroup,
      },
      // discard: true,
      // onSave: () => this.onSave(),
    },
  ];

  constructor() {}

  ngOnInit(): void {}

  onSave() {}
}
