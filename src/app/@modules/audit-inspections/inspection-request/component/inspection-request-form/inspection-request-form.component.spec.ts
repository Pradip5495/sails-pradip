import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionRequestFormComponent } from './inspection-request-form.component';

describe('InspectionRequestFormComponent', () => {
  let component: InspectionRequestFormComponent;
  let fixture: ComponentFixture<InspectionRequestFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InspectionRequestFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionRequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
