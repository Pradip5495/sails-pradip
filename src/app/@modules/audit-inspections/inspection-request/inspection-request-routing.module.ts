import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InspectionRequestComponent } from './inspection-request.component';
import {
  InspectionCloseoutStatusComponent,
  InspectionRequestFormComponent,
} from '@modules/audit-inspections/inspection-request/component';
import { extract } from '@app/i18n';

const preparationRoutes: Routes = [
  {
    path: '',
    component: InspectionRequestComponent,
    children: [
      { path: '', redirectTo: 'request', pathMatch: 'full' },
      {
        path: 'request',
        component: InspectionRequestFormComponent,
        data: { title: extract('New Inspection Request') },
      },
      {
        path: 'closeout-status',
        component: InspectionCloseoutStatusComponent,
        data: { title: extract('Inspection Closeout Status') },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(preparationRoutes)],
  exports: [RouterModule],
})
export class InspectionRequestRoutingModule {}
