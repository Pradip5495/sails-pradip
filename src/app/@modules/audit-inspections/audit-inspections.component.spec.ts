import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditInspectionsComponent } from './audit-inspections.component';

describe('AuditInspectionsComponent', () => {
  let component: AuditInspectionsComponent;
  let fixture: ComponentFixture<AuditInspectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuditInspectionsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditInspectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
