import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditInspectionsComponent } from './audit-inspections.component';

const auditInspectionsRoutes: Routes = [
  {
    path: '',
    component: AuditInspectionsComponent,
    children: [
      { path: '', redirectTo: 'planning-approvals', pathMatch: 'full' },
      {
        path: 'planning-approvals',
        loadChildren: () => import('./planning-approvals').then((m) => m.PlanningApprovalsModule),
      },
      {
        path: 'preparation',
        loadChildren: () => import('./preparation').then((m) => m.PreparationModule),
      },
      {
        path: 'inspection-request',
        loadChildren: () => import('./inspection-request').then((m) => m.InspectionRequestModule),
      },
      {
        path: 'inspector',
        loadChildren: () => import('./inspector').then((m) => m.InspectorModule),
      },
      {
        path: 'inspections',
        loadChildren: () => import('./inspections').then((m) => m.InspectionsModule),
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard').then((m) => m.AuditDashboardModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(auditInspectionsRoutes)],
  exports: [RouterModule],
})
export class AuditInspectionsRoutingModule {}
