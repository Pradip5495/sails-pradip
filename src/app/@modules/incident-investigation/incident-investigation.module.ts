import { NgModule } from '@angular/core';
import { IncidentInvestigationRoutingModule } from '@modules/incident-investigation/incident-investigation-routing.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AgGridModule } from '@ag-grid-community/angular';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  imports: [
    CommonModule,
    IncidentInvestigationRoutingModule,
    MatToolbarModule,
    AgGridModule,
    MatCardModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
  ],
})
export class IncidentInvestigationModule {}
