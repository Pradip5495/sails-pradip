import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '@shared';
import { MaterialModule } from '@app/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { FormlyModule } from '@formly';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { StatisticsComponent } from './statistics.component';
import { StatisticsRoutingModule } from './statistics-routing.module';
import { HighchartsService } from './highcharts.service';
import { PieGradientFillChartComponent } from './component/pie-gradient-fill-chart/pie-gradient-fill-chart.component';
import { StackedColumnChartComponent } from './component/stacked-column-chart/stacked-column-chart.component';
import { BasicColumnChartComponent } from './component/basic-column-chart/basic-column-chart.component';
import { BasicColumn2DataChartComponent } from './component/basic-column-2-data-chart/basic-column-2-data-chart.component';
import { VariableRadiusPieChartComponent } from './component/variable-radius-pie-chart/variable-radius-pie-chart.component';
import { NearMissStatsComponent } from './component/near-miss-stats/near-miss-stats.component';
import { MachineryFailureStatsComponent } from './component/machinery-failure-stats/machinery-failure-stats.component';
import { InjuriesStatsComponent } from './component/injuries-stats/injuries-stats.component';
import { IncidentStatsComponent } from './component/incident-stats/incident-stats.component';
import { ChartsModule } from '@shared/component';

@NgModule({
  declarations: [
    StatisticsComponent,
    BasicColumnChartComponent,
    StackedColumnChartComponent,
    BasicColumn2DataChartComponent,
    PieGradientFillChartComponent,
    VariableRadiusPieChartComponent,
    NearMissStatsComponent,
    MachineryFailureStatsComponent,
    InjuriesStatsComponent,
    IncidentStatsComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    MatInputModule,
    Ng2TelInputModule,
    NgxIntlTelInputModule,
    FormlyModule,
    NgxDropzoneModule,
    StatisticsRoutingModule,
    ChartsModule,
  ],
  providers: [HighchartsService],
})
export class StatisticsModule {}
