import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-incident-stats',
  templateUrl: './incident-stats.component.html',
  styleUrls: ['../../statistics.component.scss', './incident-stats.component.scss'],
})
export class IncidentStatsComponent implements OnInit {
  chart1Data: Subject<any[]> = new Subject<any[]>();
  chart2Data: Subject<any[]> = new Subject<any[]>();
  variablePieChart1Data: Subject<any[]> = new Subject<any[]>();
  variablePieChart2Data: Subject<any[]> = new Subject<any[]>();
  variablePieChart3Data: Subject<any[]> = new Subject<any[]>();

  taskAnalysisColumnsConfig = [{ columnField: 'second', fieldDescription: 'Incident Rating', columnColor: '#0081c8' }];

  constructor() {}

  ngOnInit() {
    this.chartData();
  }

  chartData() {
    setTimeout(() => {
      const data = [
        {
          country: 'Vessel 1',
          litres: 300.9,
          color: '#f5a52e',
        },
        {
          country: 'Vessel 2',
          litres: 301.9,
          color: '#e54f60',
        },
        {
          country: 'Vessel 3',
          litres: 201.1,
          color: '#f1cd1c',
        },
        {
          country: 'Vessel 1',
          litres: 300.9,
          color: '#f5a52e',
        },
        {
          country: 'Vessel 2',
          litres: 301.9,
          color: '#e54f60',
        },
        {
          country: 'Vessel 3',
          litres: 201.1,
          color: '#f1cd1c',
        },
      ];
      this.chart1Data.next(data);
    }, 100);
    setTimeout(() => {
      const data = [
        {
          country: 'Vessel 1',
          litres: 501.9,
          color: '#52baf3',
        },
        {
          country: 'Vessel 2',
          litres: 301.9,
          color: '#16569e',
        },
        {
          country: 'Vessel 3',
          litres: 201.1,
          color: '#0081c8',
        },
      ];
      this.chart2Data.next(data);
    }, 100);
  }

  getData() {
    return this.chart1Data;
  }

  getData1() {
    return this.chart2Data;
  }

  getVariablePieChartData1() {
    return this.variablePieChart1Data;
  }

  getVariablePieChartData2() {
    return this.variablePieChart2Data;
  }

  getVariablePieChartData3() {
    return this.variablePieChart3Data;
  }
}
