import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-near-miss-stats',
  templateUrl: './near-miss-stats.component.html',
  styleUrls: ['../../statistics.component.scss', './near-miss-stats.component.scss'],
})
export class NearMissStatsComponent implements OnInit {
  testData: Subject<any[]> = new Subject<any[]>();
  constructor() {}

  ngOnInit() {
    this.chartData();
  }

  chartData() {
    setTimeout(() => {
      const data = [
        {
          country: 'Vessel 1',
          litres: 301.9,
          color: '#f5a52e',
        },
        {
          country: 'Vessel 2',
          litres: 260.9,
          color: '#e54f60',
        },
        {
          country: 'Vessel 3',
          litres: 201.1,
          color: '#f1cd1c',
        },
        {
          country: 'Vessel 4',
          litres: 225.1,
          color: '#f1cd1c',
        },
        {
          country: 'Vessel 5',
          litres: 250.1,
          color: '#f1cd1c',
        },
      ];
      this.testData.next(data);
    }, 100);
  }

  getData() {
    return this.testData;
  }
}
