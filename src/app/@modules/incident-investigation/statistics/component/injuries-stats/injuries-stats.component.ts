import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-injuries-stats',
  templateUrl: './injuries-stats.component.html',
  styleUrls: ['../../statistics.component.scss', './injuries-stats.component.scss'],
})
export class InjuriesStatsComponent implements OnInit {
  testData: Subject<any[]> = new Subject<any[]>();
  injuryNatureAnalysisColConfig = [
    { columnField: 'second', fieldDescription: 'Incident Rating', columnColor: '#52baf3' },
  ];
  taskAnalysisColumnsConfig = [{ columnField: 'second', fieldDescription: 'Incident Rating', columnColor: '#0081c8' }];

  ngOnInit() {
    this.chartData();
  }

  chartData() {
    setTimeout(() => {
      const data = [
        {
          country: 'Vessel 1',
          litres: 307.9,
          color: '#f5a52e',
        },
        {
          country: 'Vessel 2',
          litres: 301.9,
          color: '#e54f60',
        },
        {
          country: 'Vessel 3',
          litres: 201.1,
          color: '#f1cd1c',
        },
        {
          country: 'Vessel 4',
          litres: 260.9,
          color: '#e54f60',
        },
        {
          country: 'Vessel 5',
          litres: 210.1,
          color: '#f1cd1c',
        },
      ];
      this.testData.next(data);
    }, 100);
  }

  getData() {
    return this.testData;
  }
}
