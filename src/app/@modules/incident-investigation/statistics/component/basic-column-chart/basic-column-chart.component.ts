import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-basic-column-chart',
  templateUrl: './basic-column-chart.component.html',
  styleUrls: ['./basic-column-chart.component.scss'],
})
export class BasicColumnChartComponent implements OnInit {
  public options: any = {
    chart: {
      type: 'column',
    },
    colors: ['#1981c8'],
    title: {
      text: '',
      style: {
        color: '#3face8',
        font: 'bold 16px "Trebuchet MS", Verdana, sans-serif',
      },
    },
    subtitle: {
      // text: 'Source: WorldClimate.com'
    },
    xAxis: {
      categories: ['CH-1', 'CH-2', 'CH-3', 'CH-4', 'CH-5', 'CH-6', 'CH-7', 'CH-8', 'CH-9'],
      crosshair: true,
    },
    yAxis: {
      min: 0,
      max: 50,
      title: {
        // text: 'Rainfall (mm)'
      },
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat:
        '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true,
    },
    plotOptions: {
      column: {
        pointPadding: 0.4,
        borderWidth: 0,
        borderRadius: 5,
      },
    },
    series: [
      {
        data: [45, 30, 10, 12, 35, 46, 13, 14, 21],
      },
    ],
  };
  constructor() {}

  ngOnInit() {
    setTimeout(() => {
      Highcharts.chart('container', this.options);
    }, 100);
  }
}
