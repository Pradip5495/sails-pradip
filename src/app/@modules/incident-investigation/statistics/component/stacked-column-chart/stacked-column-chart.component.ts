import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-stacked-column-chart',
  templateUrl: './stacked-column-chart.component.html',
  styleUrls: ['./stacked-column-chart.component.scss'],
})
export class StackedColumnChartComponent implements OnInit {
  public options: any = {
    chart: {
      type: 'column',
    },
    color: ['#e54f60', '#f5a52e', '#f1cd1c', '#2ec43f'],
    title: {
      text: '',
      style: {
        color: '#3face8',
        font: 'bold 16px "Trebuchet MS", Verdana, sans-serif',
      },
    },
    xAxis: {
      categories: ['CH-1', 'CH-2', 'CH-3', 'CH-4', 'CH-5', 'CH-6', 'CH-7', 'CH-8', 'CH-9'],
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Total fruit consumption',
      },
      //   stackLabels: {
      //       enabled: true,
      //       style: {
      //           fontWeight: 'bold',
      //           color: ( // theme
      //               Highcharts.defaultOptions.title.style &&
      //               Highcharts.defaultOptions.title.style.color
      //           ) || 'gray'
      //       }
      //   }
    },
    //   legend: {
    //       align: 'right',
    //       x: -30,
    //       verticalAlign: 'top',
    //       y: 25,
    //       floating: true,
    //       backgroundColor:
    //           Highcharts.defaultOptions.legend.backgroundColor || 'white',
    //     //   borderColor: '#CCC',
    //       borderWidth: 1,
    //       shadow: false
    //   },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        pointPadding: 0.35,
        borderRadius: 5,
        pointStart: 11,
        //   dataLabels: {
        //       enabled: true
        //   }
      },
    },
    series: [
      {
        data: [10, 20, 15, 7, 12, 7, 8, 13, 8],
        color: '#e54f60',
      },
      {
        data: [20, 23, 13, 8, 19, 8, 14, 27, 6],
        color: '#f5a52e',
      },
      {
        data: [25, 14, 24, 12, 5, 8, 4, 18, 12],
        color: '#f1cd1c',
      },
      {
        data: [15, 14, 18, 10, 5, 4, 2, 15, 6],
        color: '#2ec43f',
      },
    ],
  };
  constructor() {}

  ngOnInit() {
    setTimeout(() => {
      Highcharts.chart('container3', this.options);
    }, 100);
  }
}
