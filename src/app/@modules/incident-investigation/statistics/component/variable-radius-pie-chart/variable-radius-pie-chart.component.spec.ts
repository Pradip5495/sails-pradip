import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { VariableRadiusPieChartComponent } from './variable-radius-pie-chart.component';

describe('VariableRadiusPieChartComponent', () => {
  let component: VariableRadiusPieChartComponent;
  let fixture: ComponentFixture<VariableRadiusPieChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VariableRadiusPieChartComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariableRadiusPieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
