import { Component, OnInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-basic-column-2-data-chart',
  templateUrl: './basic-column-2-data-chart.component.html',
  styleUrls: ['./basic-column-2-data-chart.component.scss'],
})
export class BasicColumn2DataChartComponent implements OnInit {
  @Input() id: any;
  public options: any = {
    chart: {
      type: 'column',
    },
    colors: ['#1981c8', '#2ec43f'],
    title: {
      text: '',
    },
    subtitle: {
      text: '',
    },
    xAxis: {
      categories: ['2016', '2017', '2018', '2019', '2020', '2021', '2022'],
      crosshair: true,
    },
    yAxis: {
      min: 0,
      max: 75,
      // title: {
      //     text: 'Rainfall (mm)'
      // }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat:
        '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true,
    },
    plotOptions: {
      column: {
        pointPadding: 0.37,
        borderWidth: 0,
        borderRadius: 6,
      },
    },
    series: [
      {
        name: 'No. of Near Misses/Vessel',
        data: [30, 40, 55, 60, 29, 60, 55],
      },
      {
        name: 'Near Misses Rating',
        data: [55, 60, 70, 45, 50, 55, 75],
      },
    ],
  };

  @Input() extraOption: any;
  constructor() {}

  ngOnInit() {
    setTimeout(() => {
      Highcharts.chart(this.id, this.options);
    }, 300);
    // setTimeout(() => {
    //   Highcharts.chart(this.id, this.extraOption);
    // }, 100);
  }
}
