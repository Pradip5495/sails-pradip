import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BasicColumn2DataChartComponent } from './basic-column-2-data-chart.component';

describe('BasicColumn2DataChartComponent', () => {
  let component: BasicColumn2DataChartComponent;
  let fixture: ComponentFixture<BasicColumn2DataChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BasicColumn2DataChartComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicColumn2DataChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
