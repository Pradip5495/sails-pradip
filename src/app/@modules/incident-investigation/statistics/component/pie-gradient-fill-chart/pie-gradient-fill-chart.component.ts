import { Component, OnInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-pie-gradient-fill-chart',
  templateUrl: './pie-gradient-fill-chart.component.html',
  styleUrls: ['./pie-gradient-fill-chart.component.scss'],
})
export class PieGradientFillChartComponent implements OnInit {
  @Input() id: any;

  public options: any = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
    },
    title: {
      text: '',
      style: {
        color: '#3face8',
        font: 'bold 16px "Trebuchet MS", Verdana, sans-serif',
      },
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
    },
    accessibility: {
      point: {
        valueSuffix: '%',
      },
    },
    plotOptions: {
      pie: {
        // size: 60,
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
        },
        showInLegend: true,
      },
    },
    series: [
      {
        name: 'Brands',
        colorByPoint: true,
        data: [
          {
            name: 'Vessel 1',
            y: 45,
            color: '#f5a52e',
            // sliced: true,
            // selected: true
          },
          {
            name: 'Vessel 2',
            y: 35,
            color: '#e54f60',
          },
          {
            name: 'Vessel 3',
            y: 20,
            color: '#f1cd1c',
          },
        ],
      },
    ],
  };
  constructor() {}

  ngOnInit() {
    setTimeout(() => {
      Highcharts.chart(this.id, this.options);
    }, 300);
  }
}
