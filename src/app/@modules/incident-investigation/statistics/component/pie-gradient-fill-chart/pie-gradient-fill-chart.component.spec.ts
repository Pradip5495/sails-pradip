import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PieGradientFillChartComponent } from './pie-gradient-fill-chart.component';

describe('PieGradientFillChartComponent', () => {
  let component: PieGradientFillChartComponent;
  let fixture: ComponentFixture<PieGradientFillChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PieGradientFillChartComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PieGradientFillChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
