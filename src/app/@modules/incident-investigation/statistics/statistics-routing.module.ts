import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/i18n';
import { StatisticsComponent } from './statistics.component';

const statisticsRoutes: Routes = [
  {
    path: '',
    component: StatisticsComponent,
    children: [{ path: '', component: StatisticsComponent, data: { title: extract('Statistics') } }],
    data: { title: extract('Statistics') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(statisticsRoutes)],
  exports: [RouterModule],
})
export class StatisticsRoutingModule {}
