import { Injectable } from '@angular/core';
import { HttpDispatcherService } from '@core';
import { Observable } from 'rxjs';

const routes = {
  thirdParty: {
    get: (id: string) => `/thirdpartysharing/${id}`,
    getAll: (filters?: Map<string, string>) =>
      filters ? `/thirdpartysharing?${filters.toSanitizedURLFilters()}` : `/thirdpartysharing`,
    create: () => `/thirdpartysharing`,
    update: (id: string) => `/thirdpartysharing/${id}`,
    delete: (id: string) => `/thirdpartysharing/${id}`,
    deleteAttachment: (id: string) => `/thirdpartysharing/attachment/${id}`,
  },
};
@Injectable({
  providedIn: 'root',
})
export class ThirdPartySharingService {
  constructor(private httpDispatcher: HttpDispatcherService) {}

  getAThirdPartySharing(id: string) {
    return this.httpDispatcher.get(routes.thirdParty.get(id));
  }

  getAllThirdPartySharings(filters?: Map<string, string>) {
    return this.httpDispatcher.get(routes.thirdParty.getAll(filters));
  }

  createThirdPartySharing(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.thirdParty.create(), data);
  }

  updateThirdPartySharing(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.thirdParty.update(id), data);
  }

  deleteThirdPartySharing(id: string) {
    return this.httpDispatcher.delete(routes.thirdParty.delete(id));
  }

  deleteAttachmentThirdPartySharing(attachmentId: string) {
    return this.httpDispatcher.delete(routes.thirdParty.deleteAttachment(attachmentId));
  }
}
