import { TestBed } from '@angular/core/testing';

import { ThirdPartySharingService } from './third-party-sharing.service';

describe('ThirdPartySharingService', () => {
  let service: ThirdPartySharingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThirdPartySharingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
