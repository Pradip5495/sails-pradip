import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThirdPartySharingComponent } from './third-party-sharing.component';
import {
  ThirdPartySharingFilterComponent,
  ThirdPartySharingFormComponent,
  ThirdPartySharingTableComponent,
  ThirdPartySharingDetailsComponent,
} from './';
import { ThirdPartySharingRoutingModule } from './third-party-sharing-routing.module';
import { FormlyModule } from '@formly';

@NgModule({
  declarations: [
    ThirdPartySharingComponent,
    ThirdPartySharingFilterComponent,
    ThirdPartySharingFormComponent,
    ThirdPartySharingTableComponent,
    ThirdPartySharingDetailsComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    ThirdPartySharingRoutingModule,
    FormlyModule,
  ],
})
export class ThirdPartySharingModule {}
