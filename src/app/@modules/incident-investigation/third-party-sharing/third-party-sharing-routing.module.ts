import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ThirdPartySharingComponent } from './third-party-sharing.component';
import { extract } from '@app/i18n';
import { ThirdPartySharingFormComponent } from './component';
import { ThirdPartySharingTableComponent } from './component';
import { ThirdPartySharingDetailsComponent } from '.';

const thirdPartySharingRoutes: Routes = [
  {
    path: '',
    component: ThirdPartySharingComponent,
    children: [
      { path: '', component: ThirdPartySharingTableComponent, data: { title: extract('third-party-sharing') } },
      {
        path: 'add-third-party-sharing',
        component: ThirdPartySharingFormComponent,
        data: { title: extract('third-party-sharing') },
      },
      {
        path: 'edit-third-party-sharing/:id',
        component: ThirdPartySharingFormComponent,
        data: { title: extract('third-party-sharing') },
      },
      {
        path: 'third-party-sharing-details/:id',
        component: ThirdPartySharingDetailsComponent,
        data: { title: extract('third-party-sharing') },
      },
    ],
    data: { title: extract('third-party-sharing') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(thirdPartySharingRoutes)],
  exports: [RouterModule],
})
export class ThirdPartySharingRoutingModule {}
