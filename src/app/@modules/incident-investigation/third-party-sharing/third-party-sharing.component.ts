import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Logger } from '@core';

const log = new Logger('ThirdPartySharingComponent');

@Component({
  selector: 'app-third-party-sharing',
  templateUrl: './third-party-sharing.component.html',
  styleUrls: ['./third-party-sharing.component.scss'],
})
export class ThirdPartySharingComponent implements OnInit {
  isLoading = false;
  isNewThirdParty = false;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.isLoading = true;
    const paramValue = 'value';
    log.debug(paramValue, this.route.params[paramValue]);
    if (this.route.params[paramValue].form === 'third-party-sharing-form') {
      this.isNewThirdParty = true;
    } else {
    }
  }

  parentFun() {
    this.isNewThirdParty = true;
    // alert('parent component function.' + this.isNewNearMiss);
  }
}
