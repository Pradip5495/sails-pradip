import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdPartySharingComponent } from './third-party-sharing.component';

describe('ThirdPartySharingComponent', () => {
  let component: ThirdPartySharingComponent;
  let fixture: ComponentFixture<ThirdPartySharingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThirdPartySharingComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPartySharingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
