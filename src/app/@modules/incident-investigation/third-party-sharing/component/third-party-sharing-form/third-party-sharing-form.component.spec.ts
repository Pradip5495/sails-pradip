import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdPartySharingFormComponent } from './third-party-sharing-form.component';

describe('ThirdPartySharingFormComponent', () => {
  let component: ThirdPartySharingFormComponent;
  let fixture: ComponentFixture<ThirdPartySharingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThirdPartySharingFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPartySharingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
