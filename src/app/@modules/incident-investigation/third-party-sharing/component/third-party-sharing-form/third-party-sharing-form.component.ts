import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { untilDestroyed } from '@core';
import { ICustomFormlyConfig } from '@types';
import { FormlyFieldTemplateComponent } from '@shared/component';
import { IncidentService } from '@modules/incident-investigation/incident/share/incident.service';
import { ThirdPartyService } from '@shared/service';
import { NotificationService } from '@shared/common-service';
import { Router, ActivatedRoute } from '@angular/router';
import { ThirdPartySharingService } from '../../third-party-sharing.service';
import { FormlyImagePreviewComponent } from '@shared/component/formly-image-preview/formly-image-preview.component';

@Component({
  selector: 'app-third-party-sharing-form',
  templateUrl: './third-party-sharing-form.component.html',
  styleUrls: ['./third-party-sharing-form.component.scss'],
})
export class ThirdPartySharingFormComponent implements OnInit, OnDestroy {
  incidentData: Array<any>;
  countryCode = '1';
  phonenumber: number | string;
  hasError: boolean;
  uploadedFiles: File[] = [];
  thirdPartySharingPayload: any = {};
  title = 'Third Party Sharing';
  selectedMedium = '';

  overviewFormKeys = {
    incidentId: 'incidentId',
    incidentDate: 'incidentDate',
    incidentType: 'incidentType',
    severityLevel: 'severityLevel',
    notificationTitle: 'notificationTitle',
    comments: 'comments',
  };
  overviewForm = new FormGroup({});
  overviewModel = {};
  overviewOptions: FormlyFormOptions = {
    formState: {
      date: '',
      incidentType: '',
      severityLevel: '',
      incidentTitle: '',
    },
  };
  overviewFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: this.overviewFormKeys.incidentId,
          type: 'select',
          defaultValue: '',
          templateOptions: {
            label: 'Incident Id',
            placeholder: 'Incident Id',
            appearance: 'outline',
            required: true,
            options: this.incidentService.getAllIncidents(),
            valueProp: 'incidentId',
            labelProp: 'reportId',
            change: (field: FormlyFieldConfig, event: any) => {
              const selectedIncident = this.incidentData.find((incident: any) => incident.incidentId === event.value);

              this.overviewOptions.formState.date = selectedIncident.date;
              this.overviewOptions.formState.incidentType = selectedIncident.type;
              this.overviewOptions.formState.severityLevel = selectedIncident.severityLevel;
              this.overviewOptions.formState.incidentTitle = selectedIncident.incidentTitle;
            },
          },
        },
        {
          type: 'template-component',
          className: 'flex-1',
          templateOptions: {
            component: FormlyFieldTemplateComponent,
            props: {
              key: 'date',
              label: 'Date',
            },
          },
        },
        {
          type: 'template-component',
          className: 'flex-1',
          templateOptions: {
            component: FormlyFieldTemplateComponent,
            props: {
              key: 'incidentType',
              label: 'Incident Type',
            },
          },
        },
        {
          type: 'template-component',
          className: 'flex-1',
          templateOptions: {
            component: FormlyFieldTemplateComponent,
            props: {
              key: 'severityLevel',
              label: 'Severity Level',
              isSeverityValue: true,
            },
            valueClass: 'next',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          type: 'template-component',
          className: 'flex-1',
          templateOptions: {
            component: FormlyFieldTemplateComponent,
            props: {
              key: 'incidentTitle',
              label: 'Incident Title',
            },
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.overviewFormKeys.comments,
          type: 'textarea',
          templateOptions: {
            label: 'Comments',
            required: true,
          },
        },
      ],
    },
  ];

  detailsKey = {
    thirdPartyName: 'thirdPartyName',
    thirdPartyTypeId: 'thirdPartyTypeId',
    contactDetails: 'contactDetails',
    dateShared: 'dateShared',
    medium: 'medium',
    followUp: 'followUp',
    dueDate: 'dueDate',
    status: 'status',
    commentForFollowUp: 'commentForFollowUp',
  };
  detailsForm = new FormGroup({});
  detailsModel = {};
  detailsOptions: FormlyFormOptions = {};
  detailsFields: FormlyFieldConfig[] = [
    {
      template: '<h2>3rd Party Details</h2>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.detailsKey.thirdPartyName,
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Name',
            placeholder: 'Name',
            required: true,
          },
        },
        {
          key: this.detailsKey.thirdPartyTypeId,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Type',
            placeholder: 'Type',
            options: this.thirdPartyTypeService.getThirdPartyType(),
            valueProp: 'thirdPartyTypeId',
            labelProp: 'type',
            required: true,
          },
        },
        {
          key: this.detailsKey.contactDetails,
          type: 'tel-input',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Contact Details',
            required: true,
            onError: this.onError.bind(this),
            onCountryChange: this.onCountryChange.bind(this),
            getNumber: this.getNumber.bind(this),
            telInputObject: this.telInputObject.bind(this),
            // countryExt: 'in'
          },
          hideExpression: '!(!!model.medium && model.medium === "PhoneNo")',
          modelOptions: {
            updateOn: 'blur',
          },
          validators: {
            phone: {
              expression: (c: FormControl) => {
                return this.hasError;
              },
              message: (error: any, field: FormlyFieldConfig) => {
                return `${field.formControl.value} is not a valid Phone number`;
              },
            },
          },
        },
        {
          key: this.detailsKey.contactDetails,
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Email',
            required: true,
            placeholder: 'Email',
          },
          hideExpression: '!(!!model.medium && model.medium ==="Email") ',
          validators: {
            email: {
              expression: (c: any) => /\S+@\S+\.\S+/.test(c.value),
              message: (error: any, field: FormlyFieldConfig) =>
                `"${field.formControl.value}" is not a valid Email Address`,
            },
          },
        },
      ],
    },
    {
      template: '<h2>Sharing Details</h2>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.detailsKey.dateShared,
          type: 'matdatepicker',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Date Shared',
            placeholder: 'Date Shared',
            required: true,
          },
        },
        {
          key: this.detailsKey.medium,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Medium',
            placeholder: 'Medium',
            options: [
              { label: 'Email', value: 'Email' },
              { label: 'Phone No', value: 'PhoneNo' },
            ],
            labelProp: 'label',
            valueProp: 'value',
            change: (field: FormlyFieldConfig, event: any) => {
              this.selectedMedium = event.value;
            },
            required: true,
          },
        },
      ],
    },
    {
      template: '<h2>Follow Up Details</h2>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.detailsKey.followUp,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Follow Up Required',
            placeholder: 'Follow Up Required',
            options: [{ followUp: 'Yes' }, { followUp: 'No' }],
            labelProp: 'followUp',
            valueProp: 'followUp',
            required: true,
          },
        },
        {
          key: this.detailsKey.dueDate,
          type: 'matdatepicker',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Due Date',
            placeholder: 'Due Date',
          },
        },
        {
          key: this.detailsKey.status,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Status',
            placeholder: 'Status',
            options: [
              { status: 'In Progress', value: 'InProgress' },
              { status: 'Closed', value: 'Closed' },
            ],
            labelProp: 'status',
            valueProp: 'value',
            required: true,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.detailsKey.commentForFollowUp,
          type: 'textarea',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Comments',
            placeholder: 'Comments',
          },
        },
      ],
    },
  ];

  attachmentFileKey = 'files';
  attachmentForm = new FormGroup({});
  attachmentModel = {};
  attachmentOptions: FormlyFormOptions = {
    formState: {
      images: [],
    },
  };
  attachmentFields: FormlyFieldConfig[] = [
    {
      type: 'image-template',
      templateOptions: {
        component: FormlyImagePreviewComponent,
        props: {
          key: 'date',
          label: 'Date',
          delete: this.deleteThirdPartySharingAttachemnt.bind(this),
        },
      },
    },
    {
      key: this.attachmentFileKey,
      type: 'sailfile',
      templateOptions: {
        preview: true,
        id: 'fleet-file',
        onFileChange: this.onFormlyAttachmentSubmit.bind(this),
      },
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      shortName: 'Overview',
      title: 'Overview',
      subTitle: 'Enter Overview Data',
      config: {
        model: this.overviewModel,
        fields: this.overviewFields,
        options: this.overviewOptions,
        form: this.overviewForm,
      },
      onSave: this.onFormlyOveriewSubmit.bind(this),
      discard: true,
      onDiscard: () => this.overviewForm.reset(),
    },
    {
      shortName: 'Details',
      title: 'Details',
      subTitle: 'Enter Details',
      config: {
        model: this.detailsModel,
        fields: this.detailsFields,
        options: this.detailsOptions,
        form: this.detailsForm,
      },
      onSave: this.onFormlydetailsSubmit.bind(this),
      discard: true,
      onDiscard: () => this.detailsForm.reset(),
    },
    {
      shortName: 'Attachment',
      title: 'Attachment',
      subTitle: 'Add Attachments',
      config: {
        model: this.attachmentModel,
        fields: this.attachmentFields,
        options: this.attachmentOptions,
        form: this.attachmentForm,
      },
      onSave: this.onFinalSubmit.bind(this),
      discard: true,
      onDiscard: () => this.attachmentForm.reset(),
    },
  ];

  imageUrlsArray: Array<string> = [];
  private isEditMode: boolean;
  private thirdPartySharingId: string;

  constructor(
    private incidentService: IncidentService,
    private thirdPartyTypeService: ThirdPartyService,
    private notifyService: NotificationService,
    private thirdPartyService: ThirdPartySharingService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getIncidentIds();

    const value = 'value';
    // Logic for Edit Mode
    this.isEditMode = this.activatedRoute.params[value].id ? true : false;
    this.thirdPartySharingId = this.isEditMode ? this.activatedRoute.params[value].id : null;
    if (this.isEditMode) {
      this.getSelectedThirdPartyData();
    }
  }

  //#region for Edit Functionality Start

  async getSelectedThirdPartyData() {
    const thirdPartyDetails: any = await this.thirdPartyService
      .getAThirdPartySharing(this.thirdPartySharingId)
      .pipe(untilDestroyed(this))
      .toPromise();

    // Mapping the data for Dates
    if (thirdPartyDetails.length > 0) {
      thirdPartyDetails.map((obj: any) => {
        obj.dateShared = new Date(obj.dateShared);
        obj.dueDate = new Date(obj.dueDate);
        if (obj.files) {
          obj.files.map((file: any, index: number) => {
            const splittedFileName = file.path.split('.');
            obj.files[index].fileType = splittedFileName[splittedFileName.length - 1];
          });
        }
        return obj;
      });

      // Initializing the FormData
      this.initializeOverviewFormData(thirdPartyDetails[0]);
      this.initializeDetailsFormData(thirdPartyDetails[0]);
      this.displayImagePreviewsIfAttached(thirdPartyDetails[0]);
    }
  }

  initializeOverviewFormData(overviewData: any) {
    if (overviewData) {
      this.overviewForm.patchValue(overviewData);
    }
    // Initializing the static Keys
    this.overviewOptions.formState = {
      date: overviewData.incidentDate,
      incidentType: overviewData.incidentType,
      severityLevel: overviewData.severityLevel,
      incidentTitle: overviewData.incidentTitle,
    };
  }

  initializeDetailsFormData(detailsData: any) {
    if (detailsData) {
      this.detailsForm.patchValue(detailsData);
      setTimeout(() => {
        this.detailsForm.patchValue({ contactDetails: detailsData.contactDetails });
      }, 10);
    }
  }

  displayImagePreviewsIfAttached(thirdPartyData: any) {
    if (thirdPartyData.files) {
      // this.imageUrlsArray = thirdPartyData.files;
      // this.imageUrlsArray.map( (images) => {
      //   return images = env.filePath + '/' + 'sails-attachments/initbit/ab03a79a031ddec410602a64778911136.png';
      // });
      this.attachmentOptions.formState.images = thirdPartyData.files;
    }

    // this.imageUrl = env.filePath + '/' + this.updateUser.profilePic;
  }

  //#endregion for Edit Functionality End

  //#region Formly functions and Payload Building Methods region starts

  onFormlyOveriewSubmit(overviewFormData: any) {
    this.thirdPartySharingPayload = { ...this.thirdPartySharingPayload, ...overviewFormData };
  }

  onFormlydetailsSubmit(_detailsFormData: any) {
    // NG Tel Input is validating phone number with spaces
    // Spaces are not allowed in API below logic will remove spaces in Phone Number
    if (this.selectedMedium !== 'Email') {
      const phoneNumber = _detailsFormData.contactDetails;
      const splittedPhoneNumber = phoneNumber.split(' ');
      if (splittedPhoneNumber.length > 0) {
        _detailsFormData.contactDetails = splittedPhoneNumber.join('');
      }
    }

    if (!isNaN(Date.parse(_detailsFormData[this.detailsKey.dateShared]))) {
      _detailsFormData[this.detailsKey.dateShared] = new Date(
        _detailsFormData[this.detailsKey.dateShared]
      ).toISOString();
    }

    // DueDate - Non Mandatory Field
    if (!isNaN(Date.parse(_detailsFormData[this.detailsKey.dueDate]))) {
      _detailsFormData[this.detailsKey.dueDate] = new Date(_detailsFormData[this.detailsKey.dueDate]).toISOString();
    } else {
      if (_detailsFormData.hasOwnProperty(this.detailsKey.dueDate)) {
        delete _detailsFormData[this.detailsKey.dueDate];
      }
      if (this.thirdPartySharingPayload.hasOwnProperty(this.detailsKey.dueDate)) {
        delete this.thirdPartySharingPayload[this.detailsKey.dueDate];
      }
    }

    // CommentForFollowUp - Non Mandatory Field
    if (!_detailsFormData[this.detailsKey.commentForFollowUp]) {
      delete _detailsFormData[this.detailsKey.commentForFollowUp];
      delete this.thirdPartySharingPayload[this.detailsKey.commentForFollowUp];
    }

    this.thirdPartySharingPayload = { ...this.thirdPartySharingPayload, ..._detailsFormData };
  }

  onFormlyAttachmentSubmit(files: any) {
    /* Adding attachemnts to Payload */
    this.uploadedFiles = files ? files : this.uploadedFiles;

    if (this.uploadedFiles.length > 0) {
      this.thirdPartySharingPayload = { ...this.thirdPartySharingPayload, ...{ files: this.uploadedFiles } };
    } else {
      if (this.thirdPartySharingPayload.hasOwnProperty(this.attachmentFileKey)) {
        delete this.thirdPartySharingPayload[this.attachmentFileKey];
      }
    }
  }

  checkAndAddCountryCode() {
    const countryCodeFlag = 'countryCode';
    // Adding CountryCode and Country if exists
    if (this.selectedMedium !== 'Email') {
      // this.thirdPartySharingPayload[countryCodeFlag] = '+' + this.countryCode;
      this.thirdPartySharingPayload[countryCodeFlag] = this.countryCode;
    } else {
      if (this.thirdPartySharingPayload.hasOwnProperty(countryCodeFlag)) {
        delete this.thirdPartySharingPayload[countryCodeFlag];
      }
    }
  }

  onFinalSubmit(formData: any) {
    // RePreparing the Overview Form Data
    this.onFormlyOveriewSubmit(this.overviewForm.value);

    // RePreparing the Details Form Data
    this.onFormlydetailsSubmit(this.detailsForm.value);

    // Adding Country Code
    this.checkAndAddCountryCode();

    /* Checking all form Validations before Performing API Call */
    if (this.overviewForm.valid && this.detailsForm.valid && formData) {
      if (this.isEditMode) {
        this.updateThirdPartySharingWithFormly(this.thirdPartySharingPayload);
      } else {
        this.createThirdPartySharingWithFormly(this.thirdPartySharingPayload);
      }
    } else {
      this.notifyService.showWarning('Some of Mandatory fields are missing', this.title);
    }
  }

  //#endregion Formly functions and Payload Building Methods region starts

  //#region API Calls Region Start

  getIncidentIds() {
    return this.incidentService
      .getAllIncidents()
      .pipe(untilDestroyed(this))
      .subscribe((response) => {
        this.incidentData = response;
      });
  }

  createThirdPartySharingWithFormly(payLoad: any) {
    const formData = new FormData();
    for (const key in payLoad) {
      if (key === 'files') {
        for (const file of payLoad[key]) {
          formData.append(key, file);
        }
      } else {
        formData.append(key, payLoad[key]);
      }
    }

    this.thirdPartyService
      .createThirdPartySharing(formData)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response) => {
          if (response.statusCode === 201) {
            const message = response.message || 'New Fleet Notification created successfully!';
            this.notifyService.showSuccess(message, this.title);
            this.router.navigateByUrl('/third-party-sharing');
          } else {
            const message = response.message;
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          const message = error;
          this.notifyService.showError(message, this.title);
          // this._snackBar.open(message, '', {
          //   duration: 4000
          // });
        }
      );
  }

  updateThirdPartySharingWithFormly(payLoad: any) {
    const formData = new FormData();
    for (const key in payLoad) {
      if (key === 'files') {
        for (const file of payLoad[key]) {
          formData.append(key, file);
        }
      } else {
        formData.append(key, payLoad[key]);
      }
    }
    formData.set('isActive', '1');

    this.thirdPartyService
      .updateThirdPartySharing(this.thirdPartySharingId, formData)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response: any) => {
          if (response.statusCode === 200) {
            const message = response.message || 'Fleet Notification updated successfully!';
            this.notifyService.showSuccess(message, this.title);
            this.router.navigateByUrl('/third-party-sharing');
          } else {
            const message = response.message;
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          const message = error;
          this.notifyService.showError(message, this.title);
          // this._snackBar.open(message, '', {
          //   duration: 4000
          // });
        }
      );
  }

  deleteThirdPartySharingAttachemnt(imageData: any) {
    if (imageData) {
      this.thirdPartyService
        .deleteAttachmentThirdPartySharing(imageData.thirdPartyAttachmentId)
        .pipe(untilDestroyed(this))
        .subscribe((response: any) => {
          if (response.statusCode === 200) {
            // Filtering the Deleted Image
            const images = this.attachmentOptions.formState.images;
            const filteredImages = images.filter((image: any) => {
              return image.thirdPartyAttachmentId !== imageData.thirdPartyAttachmentId;
            });
            this.attachmentOptions.formState.images = filteredImages;

            this.notifyService.showSuccess(response.message, this.title);
          } else {
            this.notifyService.showSuccess(response.message, this.title);
          }
        });
    }
  }
  //#endregion API Calls Region Ends

  //#region NG-Tel Helper Methods starts
  onCountryChange(country: any) {
    this.countryCode = country.dialCode;
    return this.countryCode;
  }

  getNumber(event: any) {
    this.phonenumber = event;
  }

  telInputObject(event: any) {}

  onError(obj: any) {
    this.hasError = obj;
    return !obj;
  }
  //#endregion NG-Tel Helper Methods ends

  navigateBack() {
    const backRoute = !this.isEditMode ? '../' : '../../';
    this.router.navigate([backRoute], { relativeTo: this.activatedRoute });
  }

  ngOnDestroy() {}
}
