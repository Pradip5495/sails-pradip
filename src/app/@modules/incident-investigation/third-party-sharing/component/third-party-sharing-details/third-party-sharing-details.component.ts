import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment } from '@env/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { ThirdPartySharingService } from '../../third-party-sharing.service';
import { untilDestroyed } from '@core';

@Component({
  selector: 'app-third-party-sharing-details',
  templateUrl: './third-party-sharing-details.component.html',
  styleUrls: ['./third-party-sharing-details.component.scss'],
})
export class ThirdPartySharingDetailsComponent implements OnInit, OnDestroy {
  public API_URL = environment.filePath;
  thirdPartySharingData: any = [];
  private thirdPartySharingId: string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private thirdPartyService: ThirdPartySharingService
  ) {}

  ngOnInit(): void {
    // Logic for Edit Mode
    const value = 'value';
    this.thirdPartySharingId = this.activatedRoute.params[value].id || null;
    if (this.thirdPartySharingId) {
      this.getSelectedThirdPartyData();
    }
  }

  async getSelectedThirdPartyData() {
    const thirdPartyDetails: any = await this.thirdPartyService
      .getAThirdPartySharing(this.thirdPartySharingId)
      .pipe(untilDestroyed(this))
      .toPromise();

    // Mapping the data for Dates
    if (thirdPartyDetails.length > 0) {
      thirdPartyDetails.map((obj: any) => {
        obj.dateShared = new Date(obj.dateShared);
        obj.dueDate = new Date(obj.dueDate);
        if (obj.files) {
          obj.files.map((file: any, index: number) => {
            const splittedFileName = file.path.split('.');
            obj.files[index].fileType = splittedFileName[splittedFileName.length - 1];
          });
        }
        return obj;
      });
      this.thirdPartySharingData = thirdPartyDetails[0];
    }
  }

  navigateBack() {
    this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
  }

  getSeverityClass(severityValue: string = '') {
    let severityClass;
    switch (severityValue.toLowerCase()) {
      case 'trival':
        severityClass = 'severity-trival-level';
        break;
      case 'minor':
        severityClass = 'severity-minor-level';
        break;
      case 'moderate':
        severityClass = 'severity-moderate-level';
        break;
      case 'major':
        severityClass = 'severity-major-level';
        break;
      case 'extreme':
        severityClass = 'severity-extreme-level';
        break;
      default:
        severityClass = 'test';
        break;
    }
    return severityClass;
  }

  getFilterFile(receivedImageData: any) {
    const imageTypeAssetMapping = {
      png: this.API_URL + '/' + receivedImageData.path,
      jpg: this.API_URL + '/' + receivedImageData.path,
      jpeg: this.API_URL + '/' + receivedImageData.path,
      zip: '/assets/zip.png',
      ' ': '/assets/zip.png',
      pdf: '/assets/pdf.png',
    };
    return imageTypeAssetMapping[receivedImageData.fileType] || 'assets/file.png';
  }

  /**
   * return image class based on file type
   */
  getImageStyleClass(imageType: any) {
    const imageTypeStyleMapping = {
      png: 'upload-images',
      jpg: 'upload-images',
      jpeg: 'upload-images',
    };

    return imageTypeStyleMapping[imageType] || 'pdfimages';
  }

  ngOnDestroy() {}
}
