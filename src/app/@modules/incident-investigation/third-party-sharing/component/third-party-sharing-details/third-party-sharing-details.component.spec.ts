import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdPartySharingDetailsComponent } from './third-party-sharing-details.component';

describe('ThirdPartySharingDetailsComponent', () => {
  let component: ThirdPartySharingDetailsComponent;
  let fixture: ComponentFixture<ThirdPartySharingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThirdPartySharingDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPartySharingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
