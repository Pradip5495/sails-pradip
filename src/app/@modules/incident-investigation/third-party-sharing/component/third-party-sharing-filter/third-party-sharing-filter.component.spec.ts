import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdPartySharingFilterComponent } from './third-party-sharing-filter.component';

describe('ThirdPartySharingFilterComponent', () => {
  let component: ThirdPartySharingFilterComponent;
  let fixture: ComponentFixture<ThirdPartySharingFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThirdPartySharingFilterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPartySharingFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
