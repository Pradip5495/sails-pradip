import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-third-party-sharing-filter',
  templateUrl: './third-party-sharing-filter.component.html',
  styleUrls: ['./third-party-sharing-filter.component.scss'],
})
export class ThirdPartySharingFilterComponent implements OnInit {
  options: FormGroup;
  colorControl = new FormControl('primary');
  fontSizeControl = new FormControl(16, Validators.min(10));

  constructor() {}

  ngOnInit(): void {}

  getFontSize() {
    return Math.max(10, this.fontSizeControl.value);
  }

  getPadding() {}
}
