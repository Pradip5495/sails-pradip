import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdPartySharingTableComponent } from './third-party-sharing-table.component';

describe('ThirdPartySharingTableComponent', () => {
  let component: ThirdPartySharingTableComponent;
  let fixture: ComponentFixture<ThirdPartySharingTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThirdPartySharingTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPartySharingTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
