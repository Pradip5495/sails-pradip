import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { CustomizedCellComponent } from '@shared';
import { ThirdPartySharingService } from '../../third-party-sharing.service';
import { untilDestroyed } from '@core';
import { DatePipe } from '@angular/common';
import { NotificationService } from '@shared/common-service';
import { DialogComponent } from '@shared/component';

@Component({
  selector: 'app-third-party-sharing-table',
  templateUrl: './third-party-sharing-table.component.html',
  styleUrls: ['./third-party-sharing-table.component.scss'],
})
export class ThirdPartySharingTableComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild('dialog', { static: true }) dialog: DialogComponent;

  displayedColumns: string[] = [
    'incedentId',
    'incedentTitle',
    'date',
    'incedentType',
    'severity',
    'shares',
    'followUpDue',
  ];
  listOfInvestigatorsData: any = [];
  dataSource: any;
  isLoading = false;
  isdataSource = false;
  isShow = false;

  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  public array: any;
  public length = 100;
  public pageSizeOptions: number[] = [5, 10, 25, 100];

  filterForm: FormGroup;
  options: FormGroup;
  colorControl = new FormControl('primary');
  fontSizeControl = new FormControl(16, Validators.min(10));
  setTimeControl = new FormControl();

  //#region Grid Properties region Start

  columnDefs = [
    {
      headerName: 'INCIDENT ID',
      field: 'reportId',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      // headerCheckboxSelection: true,
      // headerCheckboxSelectionFilteredOnly: true,
      // checkboxSelection: true,
    },
    {
      headerName: 'INCIDENT TITLE',
      field: 'incidentTitle',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'INCIDENT DATE',
      field: 'incidentDate',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'INCIDENT TYPE',
      field: 'incidentType',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: '3RD PARTY NAME',
      field: 'thirdPartyName',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: '3RD PARTY TYPE',
      field: 'thirdPartyType',
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'DATE SHARED',
      field: 'dateShared',
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'FOLLOW UP REQUIRED',
      field: 'followUp',
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'DUE DATE',
      field: 'dueDate',
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'STATUS',
      field: 'status',
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'ACTIONS',
      field: 'actions',
      // menuTabs: ['generalMenuTab', 'filterMenuTab'],
      // sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deleteFleetNotification.bind(this),
        onClickEdit: this.editItems.bind(this),
        onClickDetails: this.viewThirdPartySharing.bind(this),
      },
      // filter: 'agTextColumnFilter',
      pinned: 'right',
      width: 150,
    },
  ];
  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
  };
  defaultColDef = {
    // flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
    editable: false,
  };
  public rowData: any;
  public paginationPageSize: any;
  public icons: any;
  editType = '';
  selectedThirdPartySharingID: any;
  selectedThirdPartyName: string;

  //#region Grid Properties region Ends

  displayDialog: boolean;
  title = 'Third Party Sharing';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private thirdPartySharing: ThirdPartySharingService,
    private activatedRoute: ActivatedRoute,
    private dateFormatter: DatePipe,
    private notifyService: NotificationService
  ) {
    this.paginationPageSize = 5;
    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };
    this.filterForm = this.formBuilder.group({
      color: this.colorControl,
      fontSize: this.fontSizeControl,
      setTime: this.setTimeControl,
    });
  }

  onAddForm() {
    this.router.navigate(['./add-third-party-sharing'], { relativeTo: this.activatedRoute });
  }

  ngOnInit(): void {
    this.getThirdPartySharing();
  }

  getThirdPartySharing(isFiltersApplied: boolean = false) {
    const filters = new Map();
    if (isFiltersApplied) {
      filters.set('status', 'Closed');
    }

    this.thirdPartySharing
      .getAllThirdPartySharings(filters)
      .pipe(untilDestroyed(this))
      .subscribe((response: Array<any>) => {
        try {
          if (response.length > 0) {
            this.rowData = response.map((data: any) => {
              data.dateShared = this.dateFormatter.transform(new Date(data.dateShared), 'dd-MMM-yyyy');
              data.dueDate = this.dateFormatter.transform(new Date(data.dueDate), 'dd-MMM-yyyy');
              data.incidentDate = this.dateFormatter.transform(new Date(data.incidentDate), 'dd-MMM-yyyy');
              return data;
            });
          }
        } catch (error) {
          // Error Handling
        }
      });
  }

  getFontSize() {
    return Math.max(10, this.fontSizeControl.value);
  }

  toggleDisplay() {
    this.isShow = !this.isShow;
  }

  resetFilter() {
    this.filterForm.reset();
  }

  public handlePage(e: PageEvent) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  deleteFleetNotification(params: any) {
    if (params) {
      this.selectedThirdPartySharingID = params.rowData.thirdPartySharingId;
      this.selectedThirdPartyName = params.rowData.thirdPartyName;
      this.displayDialog = false;
      setTimeout(() => {
        this.displayDialog = true;
      }, 10);
    }
  }

  afterDeleteDialogClosed(event: any) {
    if (event && !!this.selectedThirdPartySharingID) {
      this.thirdPartySharing
        .deleteThirdPartySharing(this.selectedThirdPartySharingID)
        .pipe(untilDestroyed(this))
        .subscribe((response: { statusCode: number; message: string }) => {
          if (response.statusCode === 200) {
            this.notifyService.showSuccess(response.message, this.title);
            this.getThirdPartySharing();
            this.dialog.close();
          } else {
            this.notifyService.showError(response.message, this.title);
          }
        });
    }
  }

  editItems(params: any) {
    if (params) {
      this.selectedThirdPartySharingID = params.rowData.thirdPartySharingId;
      this.router.navigate(['edit-third-party-sharing', this.selectedThirdPartySharingID], {
        relativeTo: this.activatedRoute,
      });
    }
  }

  viewThirdPartySharing(params: any) {
    if (params) {
      this.selectedThirdPartySharingID = params.rowData.thirdPartySharingId;
      this.router.navigate(['third-party-sharing-details', this.selectedThirdPartySharingID], {
        relativeTo: this.activatedRoute,
      });
    }
  }

  onGridReady(params: any) {
    // log.debug('reached');
    // this.fleetNotificationService.getFleetNotification().subscribe(
    //   (response) => {
    //     log.debug('Users Data', response);
    //     if (!response) {
    //       this.isdataSource = true;
    //     }
    //     this.rowData = response;
    //   },
    //   (error) => {
    // this.notifyService.showError(error);
    //   }
    // );
    this.getThirdPartySharing();
  }

  ngOnDestroy() {}

  onDialogOkClick() {
    this.dialog.onAfterClosed(true);
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.dataSource = part;
  }
}
