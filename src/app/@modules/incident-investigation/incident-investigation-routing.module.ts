import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { CalenderModule } from './calender/calender-page.module';

const routes: Routes = [
  { path: '', redirectTo: 'incident', pathMatch: 'full' },
  { path: 'incident', loadChildren: () => import('./incident/incident.module').then((m) => m.IncidentModule) },
  { path: 'near-miss', loadChildren: () => import('./near-miss/near-miss.module').then((m) => m.NearMissModule) },
  {
    path: 'investigators',
    loadChildren: () => import('./investigators/investigators.module').then((m) => m.InvestigatorsModule),
  },
  {
    path: 'fleet-notification',
    loadChildren: () => import('./fleet-notification/fleet-notification.module').then((m) => m.FleetNotificationModule),
  },
  {
    path: 'lessons-learnt',
    loadChildren: () => import('./lessons-learnt/lessons-learnt.module').then((m) => m.LessonsLearntModule),
  },
  {
    path: 'third-party-sharing',
    loadChildren: () =>
      import('./third-party-sharing/third-party-sharing.module').then((m) => m.ThirdPartySharingModule),
  },
  {
    path: 'investigation-stats',
    loadChildren: () => import('./statistics').then((m) => m.StatisticsModule),
  },
  {
    path: 'investigation-report',
    loadChildren: () =>
      import('./investigation-report/investigation-report.module').then((m) => m.InvestigationReportModule),
  },
  {
    path: 'statistics',
    loadChildren: () => import('./statistics/statistics.module').then((m) => m.StatisticsModule),
  },

  // { path: 'calender', loadChildren: () => CalenderModule }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class IncidentInvestigationRoutingModule {}
