import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-incident',
  templateUrl: './incident.component.html',
  styleUrls: ['./incident.component.scss'],
})
export class IncidentComponent implements OnInit {
  isLoading = false;
  isNewIncident = false;
  isUpdateIncident = false;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.isLoading = true;
    // this.leftSidenavStore.hasSideNav = true;
    const paramValue = 'value';
    if (this.route.params[paramValue].create === 'create') {
      this.isNewIncident = true;
    } else if (this.route.params[paramValue].id) {
      this.isNewIncident = false;
      this.isUpdateIncident = true;
    }

    if (this.isNewIncident || this.isUpdateIncident) {
      // this.leftSidenavStore.hasSideNav = false;
    }
  }
}
