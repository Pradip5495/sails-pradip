import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IncidentComponent } from './incident.component';
import { extract } from '@app/i18n';
import { IncidentDetailsComponent } from './component/incident-details/incident-details.component';
import { IncidentFormComponent } from './component/incident-form/incident-form.component';

const incidentRoutes: Routes = [
  { path: '', component: IncidentComponent, data: { title: extract('Incident') } },
  { path: 'create', component: IncidentFormComponent, data: { title: extract('New Incident') } },
  { path: 'filter/:type', component: IncidentComponent, data: { title: extract('New Incident') } },
  { path: ':id/edit', component: IncidentComponent, data: { title: extract('Update Incident') } },
  { path: ':incidentId/details', component: IncidentDetailsComponent, data: { title: extract('Incident') } },
];

@NgModule({
  imports: [RouterModule.forChild(incidentRoutes)],
  exports: [RouterModule],
})
export class IncidentRoutingModule {}
