export const SEVERITY_LEVEL_POINTS = {
  Extreme: 5,
  Major: 4,
  Moderate: 3,
  Minor: 2,
  Trivial: 1,
};

export const LIKELIHOOD_POINTS = {
  Rare: 1,
  Unlikely: 2,
  Possible: 3,
  Likely: 4,
  'Almost Certain': 5,
};
