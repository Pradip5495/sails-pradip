import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { RouterModule } from '@angular/router';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { IncidentComponent } from './incident.component';
import { IncidentFormComponent } from './component/incident-form/incident-form.component';
import { IncidentFilterComponent } from './component/incident-filter/incident-filter.component';
import { IncidentTableComponent } from './component/incident-table/incident-table.component';
import { IncidentUpdateFormComponent } from './component/incident-update-form/incident-update-form.component';
import { IncidentDailogComponent } from './component/incident-dailog/incident-dailog.component';
import { FormlyModule } from '@formly';
import { IncidentDetailsComponent } from './component/incident-details/incident-details.component';
import { IncidentRoutingModule } from './incident-routing.module';
import { CauseTreeModule } from '../share/cause-tree/cause-tree.module';

@NgModule({
  declarations: [
    IncidentComponent,
    IncidentFormComponent,
    IncidentFilterComponent,
    IncidentTableComponent,
    IncidentUpdateFormComponent,
    IncidentDailogComponent,
    IncidentDetailsComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormlyModule,
    IncidentRoutingModule,
    CauseTreeModule,
  ],
})
export class IncidentModule {}
