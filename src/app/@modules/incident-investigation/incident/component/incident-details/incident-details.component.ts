import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '@shared/common-service';
import { IncidentService } from './../../share/incident.service';
@Component({
  selector: 'app-incident-details',
  templateUrl: './incident-details.component.html',
  styleUrls: ['./incident-details.component.css'],
})
export class IncidentDetailsComponent implements OnInit {
  public isLoading = false;
  public inid: any;
  public title: '';
  public isdataSource = false;
  public isTableView = true;
  public portList: any = [];
  public incidentData: any = [];
  public incidentDetailsData: any = [];
  public incidentId: any;
  public id: any;
  public isShow = false;
  public vesselTypeData: any = [];
  public states: string[] = ['None', 'Alaska'];
  public probabilityList: string[] = ['Rare', 'Unlikely', 'Possible', 'Likely', 'Almost Certain'];
  public probability = 'Possible';
  public severityLevel: string[] = ['Extreme', 'Major', 'Moderate', 'Minor', 'Trivial'];
  public reportType: any;
  public array: any;
  public length = 100;
  public pageSizeOptions: number[] = [5, 10, 25, 100];
  public rowData: any;
  public icons: any;
  error: string | undefined;
  constructor(
    private incidentService: IncidentService,
    private notifyService: NotificationService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const paramValue = 'value';
    if (this.route.params[paramValue].incidentId) {
      this.inid = this.route.params[paramValue].incidentId;
      this.getIncidentById(this.inid);
    }
  }

  getIncidentById(incidentId: any) {
    this.incidentService.getIncidentById(incidentId).subscribe(
      (response) => {
        this.incidentDetailsData = response;
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }
  onChangeprobability(event: any) {}
  setProbability(probability: any) {
    this.probability = probability;
    return this.probability;
  }
}
