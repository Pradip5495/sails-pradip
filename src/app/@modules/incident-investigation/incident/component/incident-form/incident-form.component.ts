import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CacheServiceService } from '@shared/common-service/cache-service.service';
import { NotificationService } from '@shared/common-service/notification.service';
import { UserService } from '@shared/service/user.service';
import { NearMissService } from '../../../share/near-miss.service';
import { IncidentCacheService } from '../../../share/incident-cache.service';
import { IncidentService } from '../../share/incident.service';
import { Logger } from '@core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable, forkJoin, of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { IncidentDailogComponent } from './../incident-dailog/incident-dailog.component';
import { FormlyFormOptions, FormlyFieldConfig } from '@formly';
import { map } from 'rxjs/operators';
import { ICustomFormlyConfig, IJson } from '@types';

import getOverviewFormFields from './incident-formly/overview/overview-form-fields';
import getDescriptionFormFields from './incident-formly/description/description-form-fields';
import getContactEstablishFormFields from './incident-formly/contact-establish/contact-establish-form-fields';
import getInvestigationFormFields from './incident-formly/investigation/investigation-form-fields';
import getBreachFormFields from './incident-formly/breach/breach-form-fields';
import getAnalysisFormFields from './incident-formly/analysis/analysis-form-fields';
import getActionFormFields from './incident-formly/action/action-formly-fields';
import getReportStatusFormFields from './incident-formly/report-status/report-status-form-fields';
import getTmsaFormFields from './incident-formly/tmsa/tmsa-form-fields';
import getBackgroundFormFields from './incident-formly/background/background-form-fields';
import getVdrFormFields from './incident-formly/vdr/vdr-form-fields';
import getNotificationFormFields from './incident-formly/notification/notification-form-fields';
import getPostMeasureFormFields from './incident-formly/post-measure/post-measure-form-fields';
import getSequenceEventsFormFields from './incident-formly/sequence-events/sequence-events-form-fields';
import { FormlyCacheService } from '@formly/helpers/formly-cache.service';

const log = new Logger('Incident Form');

@Component({
  selector: 'app-incident-form',
  templateUrl: './incident-form.component.html',
  styleUrls: ['./incident-form.component.scss'],
})
export class IncidentFormComponent implements OnInit {
  get getCauseValidate() {
    if (this.typeOfContactArray.length > 0) {
      return true;
    } else {
      if (this.basicCauseArray.length > 0) {
        return true;
      } else {
        if (this.rootCauseArray.length > 0) {
          return true;
        } else {
          return false;
        }
      }
    }
  }

  public impactList: any = [];

  //#region Formly Migration
  severityLevel: IJson[] = [];
  stateGroups: IJson[] = [];
  reportTypes: IJson = {};
  eventIncidents: IJson[] = [];

  currentUser: any;
  isLatLong = false;
  severityDescriptions: any = {};
  users: any = [];

  formOptions: FormlyFormOptions = {};

  formFields: FormlyFieldConfig[] = getOverviewFormFields({
    vesselList: this.getVessels(),
    portSeaOnChange: this.onPortSeaChange.bind(this),
    portList: this.getNearMissPortList(),
    taskList: this.nearMissService.getTaskList(),
    locationList: this.nearMissService.getLocationList(),
  });

  model: any = {};
  form = new FormGroup({
    userId: new FormControl('', [Validators.required]),
    isLatLong: new FormControl('0', [Validators.required]),
    latitude: new FormControl(''),
    longitude: new FormControl(''),
  });

  descriptionFormOptions: FormlyFormOptions = {
    formState: {
      severityDescriptions: {},
      resultantRisk: 6,
    },
  };
  descriptionModel: any = {};

  descriptionFormFields: FormlyFieldConfig[] = getDescriptionFormFields({
    eventIncidentItems: [],
    allUsers: [],
    formState: this.descriptionFormOptions.formState,
    severityLevel: this.severityLevel,
    eventImpacts: [],
    probability: [],
  });
  descriptionForm = new FormGroup({ eventImpact: new FormControl([], Validators.required) });

  contactEstablishFormOptions: FormlyFormOptions = {};
  contactEstablishFormFields: FormlyFieldConfig[] = getContactEstablishFormFields({
    allUsers: [],
    allCompany: [],
  });
  contactEstablishModel: any = {};
  _contactEstablishForm = new FormGroup({ intContact: new FormArray([], Validators.required) });

  investigationFormOptions: FormlyFormOptions = {};
  investigationFormFields: FormlyFieldConfig[] = getInvestigationFormFields({
    investigationList: [],
    investigationByPosition: [],
  });
  investigationModel: any = {};
  _investigationForm = new FormGroup({});

  breachFormOptions: FormlyFormOptions = {};
  breachFormFields: FormlyFieldConfig[] = getBreachFormFields();
  breachModel: any = {};
  _breachForm = new FormGroup({});

  analysisFormOptions: FormlyFormOptions = {
    formState: {
      actionType: '',
    },
  };
  analysisFormFields: FormlyFieldConfig[] = getAnalysisFormFields({
    similarIncident: [],
  });

  analysisModel: any = {};
  _analysisForm = new FormGroup({});

  actionsFormOptions: FormlyFormOptions = {};
  actionsFormFields: FormlyFieldConfig[] = getActionFormFields({
    proposedBy: [],
    action: [],
    actionType: [],
    responsibility: [],
    actionStatus: [],
    formState: this.analysisFormOptions.formState,
  });
  actionsModel: any = {};
  _actionsForm = new FormGroup({});

  reportStatusOptions: FormlyFormOptions = {
    formState: {},
  };

  reportStatusFields: FormlyFieldConfig[] = getReportStatusFormFields({
    investigationList: [],
    allUsers: [],
  });
  reportStatusModel: any = {};
  reportStatusForm = new FormGroup({});

  tmsaFormOptions: FormlyFormOptions = {
    formState: {
      specificData: {},
      stageData: {},
    },
  };
  tmsaFormFields: FormlyFieldConfig[] = getTmsaFormFields({
    elements: [],
  });
  tmsaModel: any = {};
  _tmsaForm = new FormGroup({});

  backgroundModel: any = {};
  _backgroundForm = new FormGroup({});
  backgroundOptions: FormlyFormOptions = {
    formState: {
      hideMe: true,
    },
  };
  backgroundFields: FormlyFieldConfig[] = getBackgroundFormFields({
    backgroundCondition: this.getBackgroundCondition(),
  });

  vdrModel: any = {};
  _vdrForm = new FormGroup({});
  vdrOptions: FormlyFormOptions = {};
  vdrFields: FormlyFieldConfig[] = getVdrFormFields({
    onVdrFileChange: (files: File[]) => {
      this.onReviewOfVdrFileChange(files);
    },
  });

  notificationModel: any = {};
  _notificationForm = new FormGroup({});
  notificationOptions: FormlyFormOptions = {};
  notificationFields: FormlyFieldConfig[] = getNotificationFormFields();

  postMeasureModel: any = {};
  postMeasureForm = new FormGroup({});
  postMeasureOptions: FormlyFormOptions = {};
  postMeasureFields: FormlyFieldConfig[] = getPostMeasureFormFields();

  sequenceEventsModel: any = {};
  sequenceEventsForm = new FormGroup({});
  sequenceEventsOptions: FormlyFormOptions = {};
  sequenceEventsFields: FormlyFieldConfig[] = getSequenceEventsFormFields();

  /*
   * formly can be array or json object
   * If it is array, consider that there are mat steps, otherwise a plain form
   */
  formlyConfig: ICustomFormlyConfig[] = [
    {
      id: 'overviewIncident',
      shortName: 'Overview',
      title: 'Overview',
      subTitle: 'DESCRIBE WHAT HAPPENED',
      config: {
        model: this.model,
        fields: [...this.formFields],
        options: this.formOptions,
        form: this.form,
      },
      onSave: this.onOverviewSave.bind(this),
      onPartialSave: this.onOverviewSave.bind(this),
    },
    {
      id: 'descriptionIncident',
      shortName: 'Description',
      title: 'Description of the Incident',
      subTitle: 'DESCRIBE WHAT HAPPENED',
      config: {
        model: this.descriptionModel,
        fields: this.descriptionFormFields,
        options: this.descriptionFormOptions,
        form: this.descriptionForm,
        onModelChange: this.onDescriptionModelChange.bind(this),
      },
      discard: true,
      onSave: this.onSubmitDescription.bind(this),
    },
    {
      id: 'investigationIncident',
      shortName: 'Investigation',
      title: 'Investigation Team',
      subTitle: 'ENTER INVESTIGATION DATA',
      config: {
        model: this.investigationModel,
        fields: this.investigationFormFields,
        options: this.investigationFormOptions,
        form: this._investigationForm,
      },
      discard: true,
      onSave: this.onSubmitInvestigation.bind(this),
    },
    {
      id: 'contactEstablishIncident',
      shortName: 'Contact Establish',
      title: 'Contacts Established During The Incident',
      subTitle: 'Who Investigated The Incident',
      config: {
        model: this.contactEstablishModel,
        fields: this.contactEstablishFormFields,
        options: this.contactEstablishFormOptions,
        form: this._contactEstablishForm,
      },
      discard: true,
      onSave: this.onSubmitContactEst.bind(this),
    },
    {
      id: 'backgroundIncident',
      shortName: 'Background',
      title: 'Background',
      subTitle: 'Enter Background Data',
      config: {
        model: this.backgroundModel,
        form: this._backgroundForm,
        options: this.backgroundOptions,
        fields: this.backgroundFields,
      },
      discard: true,
      onSave: this.onSubmitBackground.bind(this),
    },
    {
      id: 'vdrIncident',
      shortName: 'VDR',
      title: 'Review of VDR Data',
      subTitle: 'ENTER REVIEW OF VDR DATA',
      config: {
        model: this.vdrModel,
        form: this._vdrForm,
        options: this.vdrOptions,
        fields: this.vdrFields,
      },
      discard: true,
      onSave: this.onSubmitIncidentVdr.bind(this),
    },
    {
      id: 'notificationIncident',
      shortName: 'Notification',
      title: 'Notification (PSC, OIL MAJORS etc)',
      subTitle: 'ENTER NOTIFICATION DATA',
      config: {
        model: this.notificationModel,
        form: this._notificationForm,
        options: this.notificationOptions,
        fields: this.notificationFields,
      },
      discard: true,
      onSave: this.onSubmitnotifyData.bind(this),
    },
    {
      id: 'postincidentrecoveryIncident',
      shortName: 'Post Measures',
      title: 'Post Incident Recovery Measures',
      subTitle: 'ENTER POST INCIDENT RECOVERY MEASURES DATA',
      config: {
        model: this.postMeasureModel,
        form: this.postMeasureForm,
        options: this.postMeasureOptions,
        fields: this.postMeasureFields,
      },
      discard: true,
      onSave: this.onSubmitpostRec.bind(this),
    },
    {
      id: 'breachRegulatoryIncident',
      shortName: 'Breach Regulatory',
      title: 'Breach of Regulatory Requirements',
      subTitle: 'ENTER BREACH OF REGULATORY REQUIREMENTS DATA',
      config: {
        model: this.breachModel,
        fields: this.breachFormFields,
        options: this.breachFormOptions,
        form: this._breachForm,
      },
      discard: true,
      onSave: this.onSubmitBreach.bind(this),
    },
    {
      id: 'sequenceEventsIncident',
      shortName: 'Sequence Events',
      title: 'Sequence of Events',
      subTitle: 'ENTER SEQUENCE OF EVENTS DATA',
      config: {
        model: this.sequenceEventsModel,
        fields: this.sequenceEventsFields,
        options: this.sequenceEventsOptions,
        form: this.sequenceEventsForm,
      },
      discard: true,
      onSave: this.onSubmitIncidentSeqDataForm.bind(this),
    },
    {
      id: 'analysisIncident',
      shortName: 'Analysis',
      title: 'Analysis',
      subTitle: 'ANALYSIS WHY THIS HAPPENED',
      config: {
        model: this.analysisModel,
        fields: this.analysisFormFields,
        options: this.analysisFormOptions,
        form: this._analysisForm,
      },
      discard: true,
      onSave: this.onSubmitAnalysis.bind(this),
    },
    {
      id: 'actionIncident',
      shortName: 'Action',
      title: 'Actions',
      subTitle: 'WHAT ACTIONS TAKEN',
      config: {
        model: this.actionsModel,
        fields: this.actionsFormFields,
        options: this.actionsFormOptions,
        form: this._actionsForm,
      },
      discard: true,
      onSave: this.onSubmitActionForm.bind(this),
    },
    {
      id: 'tmsaIncident',
      shortName: 'TMSA',
      title: 'Cross Referencing to TMSA',
      subTitle: 'ENTER CROSS REFERENCE OF TMSA DATA',
      config: {
        model: this.tmsaModel,
        fields: this.tmsaFormFields,
        options: this.tmsaFormOptions,
        form: this._tmsaForm,
      },
      discard: true,
      onSave: this.onSubmitTmsa.bind(this),
    },
    {
      id: 'reportStatusIncident',
      shortName: 'Report Status',
      title: 'Report Status',
      subTitle: 'REPORT STATUS OF INCIDENT',
      config: {
        model: this.reportStatusModel,
        fields: this.reportStatusFields,
        options: this.reportStatusOptions,
        form: this.reportStatusForm,
      },
      discard: true,
      onSave: this.onSubmitReport.bind(this),
    },
  ];

  crossReferenceFormData: IJson = {};

  //#endregion End Formly Migration

  stateGroupOptions: Observable<IJson[]>;

  isLinear = false;
  overViewFormGroup: FormGroup;
  descriptionFormGroup: FormGroup;
  analysisFormGroup: FormGroup;
  attachmentFormGroup: FormGroup;
  crossReferenceForm: FormGroup;
  actionForm: FormGroup;
  investigationTeamForm: FormGroup;
  contactEstablishForm: FormGroup;
  incidentSeqDataForm: FormGroup;
  incidentVdrDataForm: FormGroup;
  postRecDataForm: FormGroup;
  notifyDataForm: FormGroup;
  breachDataForm: FormGroup;
  backgroundDataForm: FormGroup;
  reportStatusFrom: FormGroup;
  communicationDataForm: FormGroup;
  public impactSelectedArray: any = [];

  public type: string[] = ['Internal', 'External'];
  public newTeamMemberArray: any = [];
  public eqpOtherDetailsArray: any = [];
  public backgroundDetailsArray: any = [];
  public companyList: any;
  public title = 'Incident';
  public vesselTypeData: any = [];
  public states: string[] = ['None', 'Alaska'];
  public vesselLoc = 'Open Sea';
  public vesselLocation: string[] = ['Open Sea', 'In Port', 'Anchorage', 'Pilotage', 'Shipyard', 'Offshore'];
  public reportType: string;
  public reportTypeList: string[] = ['Unsafe Act', 'Unsafe Condition', 'Near Miss', 'Significant Near Miss'];
  public probability: any = [];

  public observedBy: any = [];
  public proposedBy: any = [];
  public actionStatus: any = [];
  public department: any = [];
  public severityArray: any = [];

  public bgImage: any = [];

  public isRootCause = true;
  public isCorrectiveCause = true;

  public allFiles: any = [];
  public analysisData: any;
  public impactLabel: any;
  public eventImpactTypeList = 'Injury';
  public contactOrEventOptionList: any = [];
  public contactOrEventOptionActive: any = [];
  public basicImmidateOptionList: any = [];
  public basicImmidateOptionActive: any = [];
  public ImmediateCauseId: any;
  public rootCauseId: any;
  public correctiveActionId: any;
  public immediateArray: any = [];
  public contactImmediateArray: any = [];
  public rootOptionList: any = [];
  public rootCauseOptionList: any = [];
  public rootCauseOptionActive: any = [];
  public correctiveCauseOptionList: any = [];
  public correctiveCauseOptionActive: any = [];
  public portList: any = [];
  public locationList: any = [];
  public contactList: any;
  public reportId: any;
  public color = 'primary';
  public designationData: any = [];
  public LatLongLabel = 'Port';
  error: string | undefined;
  loginForm!: FormGroup;
  public isLoading = false;
  public innerValue = 'Open Sea';
  public innerType = 'Unsafe Act';
  public stopCardIssuedValue = 'yes';
  public impactValue = 'Environment';
  public severityValue = 'Minor';
  public probabilityValue = 'Possible';
  public actionFlag = 'Open';
  public readonly: boolean;
  public images: any = [];
  public seqImagesArray: any = [];
  public backgroundImages: any = [];
  public backgroundImageArray: any = [];
  public breachFile: any = [];
  public breachFileArray: any = [];
  public reviewOfVdrFile: any = [];
  public reviewOfVdrFileArray: any = [];
  public postRecFile: any = [];
  public postRecFileArray: any = [];
  public notifyFile: any = [];
  public notifyFileArray: any = [];
  public actionFile: any = [];
  public actionFileArray: any = [];
  public checked = true;
  public subDetails: any;
  public activeImmediateCause: any;
  public activeContactCause: any;
  public activeRootCause: any;
  public activeCorrectiveCause: any;
  public correctiveCauseArray: any = [];
  public causeTree: any;
  public filterBasicImmediateCause: any = [];
  public filterBasicRootCause: any = [];
  public filterBasicCorrectiveCause: any = [];
  public taskList: any;
  public severity = 2;
  public probabilityImpact = 3;
  public impact = 6;
  public backgroundImage: any = [];
  public backgroundDataArray: any = [];
  public eventImpact: any = [];
  public injuryCategory: any = [];
  public injuryOccured: any = [];
  public rankList: any = [];
  public bodyPartAffIdList: any = [];
  public nationalityList: any = [];
  public equipmentList: any = [];
  public equipmentCategoryList: any = [];
  public investigatorsList: any = [];
  public teamMemberList: any = [];
  public userList: any = [];
  public locationData: any = [];
  public similarIncident: any = [];
  public designationList: any = [];
  public tomorrow = new Date();
  public levelName: any;
  public activeIndex: any = 1;
  public reportDate = new Date();
  public incidentPostResponse: any;
  public ltTimeInt: any;
  public spacificData: any[];
  public stageData: any;
  public relationalId: any;
  public relationalData: any;
  public elementData: any;
  public resposibilityArray: any = [];
  public proposedByArray: any = [];
  public isImmediateCauseArray: any = [];
  public typeContactView = false;
  public eventImpactType: any;
  public typeOfContactData: any;
  public causeTreeValidate = false;
  public typeOfContactArray: any = [];
  public basicCauseArray: any = [];
  public rootCauseArray: any = [];
  public correctiveArray: any = [];
  public analysisFormData: any;

  public peopleImpact: any;
  public environmentImpact: any;
  public businessImpact: any;
  public assetsImpact: any;
  public stageArray: any = [];

  public additionInjuryFlag = false;
  public eqpDamageFlag = false;
  public vdrFlag = false;

  public reviewOfVdrImages: any = [];
  public reviewVdrImage: any = [];
  public backgroundConditionData: any = [];
  public newBackgroundCondition: any = [];

  toppings = new FormControl();
  natureOfAction: string;
  name: string;

  public formStepErrorArray: any = [];
  isPartial = false;
  IsChecked: boolean;
  IsIndeterminate: boolean;
  LabelAlign: string;
  IsDisabled: boolean;
  private backgroundDataJson: any = {};

  constructor(
    private _formBuilder: FormBuilder,
    private cacheServiceService: CacheServiceService,
    private userProfileService: UserService,
    private nearMissService: NearMissService,
    private incidentService: IncidentService,
    private incidentCacheService: IncidentCacheService,
    private notifyService: NotificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private formlyCacheService: FormlyCacheService,
    private cdr: ChangeDetectorRef
  ) {
    this.IsChecked = false;
    this.IsIndeterminate = false;
    this.LabelAlign = 'after';
    this.IsDisabled = false;
    this.tomorrow.setDate(this.tomorrow.getDate());
  }

  ngOnInit(): void {
    this.createOverviewForm();
    this.createDescriptionForm();
    this.createAnalysisForm();
    this.createCrossReferencing();
    this.createAction();
    this.investigationForm();
    this.contactEstForm();
    this.incidentSeqForm();
    this.incidentVdrForm();
    this.postRecForm();
    this.notifyForm();
    this.breachForm();
    this.backgroundForm();

    this.getIncidentResources();
    this.getNearMissCauseTree();
    this.userprofile();
    this.getNearMissTastList();
    this.getNearMissLocationList();

    this.getCrossReferenceTMSA();
    this.getStageData();

    const overview = this.incidentCacheService.overviewData;
    if (overview) {
      this.overViewFormGroup.patchValue(overview);
      this.reportDate = new Date(this.overViewFormGroup.value.dateOfIncident);
      this.reportDate.setDate(this.reportDate.getDate());
    }

    const description = this.incidentCacheService.descriptionData;
    if (description) {
      this.descriptionFormGroup.patchValue(description);
    }

    const investigationData = this.incidentCacheService.investigationData;
    if (investigationData) {
      this.investigationTeamForm.patchValue(investigationData);
    }

    const contactEstablish = this.incidentCacheService.contactEstablish;
    if (contactEstablish) {
      this.contactEstablishForm.patchValue(contactEstablish);
    }

    const crossReference = this.incidentCacheService.crossReference;
    if (crossReference) {
      this.crossReferenceForm.patchValue(crossReference);
    }

    const action = this.incidentCacheService.action;
    if (action) {
      this.actionForm.patchValue(action);
    }

    const incidentSeq = this.incidentCacheService.incidentSeq;
    if (incidentSeq) {
      this.incidentSeqDataForm.patchValue(incidentSeq);
    }

    const incidentVdr = this.incidentCacheService.incidentVdr;
    if (incidentVdr) {
      this.incidentVdrDataForm.patchValue(incidentVdr);
    }

    const notify = this.incidentCacheService.notify;
    if (notify) {
      this.notifyDataForm.patchValue(notify);
    }

    const breachData = this.incidentCacheService.breachData;
    if (breachData) {
      this.breachDataForm.patchValue(breachData);
    }

    const postRec = this.incidentCacheService.postRec;
    if (postRec) {
      this.postRecDataForm.patchValue(postRec);
    }
  }

  navigateBack() {
    const navigateBackUrl = '../';
    this.router
      .navigate([navigateBackUrl], {
        relativeTo: this.activatedRoute,
      })
      .catch((e) => {
        log.error(e);
      });
  }
  _onChangeStopCardIssued = (_: any) => {};

  _onChange = (_: any) => {};

  _onChangeType = (_: any) => {};

  _onChangeImpact = (_: any) => {};

  _onChangeSeverity = (_: any) => {};

  _onChangeprobability = (_: any) => {};
  _onChangeLatLong = (_: any) => {};

  getBackgroundFormlyConfig(id: string, title: string) {
    return {
      id,
      fieldGroupClassName: '',
      fieldGroup: [
        {
          fieldGroupClassName: 'display-flex',
          fieldGroup: [
            {
              template: `<h4 class="bg-header">${title}</h4>`,
            },
          ],
        },
        {
          fieldGroupClassName: 'display-flex',
          fieldGroup: [
            {
              className: 'flex-6',
              type: 'input',
              key: 'description',
              templateOptions: {
                placeholder: 'Brief Description',
                label: 'Brief Description',
                floatLabel: 'never',
              },
            },
            {
              className: 'flex-1 contributing_factor',
              type: 'checkbox',
              key: 'isContibutingFactor',
              defaultValue: false,
            },
          ],
        },
        {
          fieldGroupClassName: 'display-flex',
          fieldGroup: [
            {
              type: 'sailfile',
              key: 'attachment',
              templateOptions: {
                preview: true,
                id: id + '_file',
              },
            },
          ],
        },
      ],
    };
  }

  onChange(event: any) {
    this._onChange(this.innerValue);
  }

  onChangeType(event: any) {
    this._onChangeType(this.innerType);
  }

  getImapctType(eventImpactType: any) {
    const eventImpact = this.eventImpactType.etuid;
    log.debug('Read impact id', eventImpact);
  }

  deleteImage(image: any) {
    const index = this.images.indexOf(image);
    this.images.splice(index, 1);
    this.allFiles.splice(index, 1);
    this.onFileChange(this.images);
  }

  addTypeofContact(index: number) {
    this.typeContactView = !this.typeContactView;
    if (!this.typeContactView) {
      setTimeout(() => {
        this.move(index);
        this.activeIndex = index + 1;
      }, 1);
    }
  }

  isPartiallySubmitted(flag: any) {
    if (flag) {
      this.isPartial = true;
    } else {
      this.isPartial = false;
    }
  }

  onSubmitAnalysisChart() {
    log.debug('this.analysisFormGroup.value: ', this.analysisFormGroup.value);
    this.incidentCacheService.setAnalysisData(this.analysisFormGroup.value);
    this.analysisData = this.incidentCacheService.analysisForm.typeOfContactOrEvent;
    this.addTypeofContact(10);
  }

  openDialog(flag: any, index: any): void {
    const dialogRef = this.dialog.open(IncidentDailogComponent, {
      width: '400px',
      data: { name: this.name, natureOfAction: this.natureOfAction, stopCardFlag: flag },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.natureOfAction = result;
      this.setActionComment(result, index);
    });
  }

  setActionComment(natureOfAction: any, index: any) {
    this.actionForm.get(`action.${index}.supportiveComments`).patchValue(natureOfAction);
  }

  getImpactType(type: any) {
    if (type === 'Injury') {
      this.additionInjuryFlag = true;
      this.eqpDamageFlag = false;
      this.vdrFlag = false;
      this.descriptionFormGroup.get(`isAddlInfoInvolvingInjury`).patchValue(1);
      this.descriptionFormGroup.get(`isAddlInfoInvolvingEqpDamage`).patchValue(0);
      this.descriptionFormGroup.get(`isIncidentVdrData`).patchValue(1);
    } else if (type === 'Equipment or Machinery Damage') {
      this.additionInjuryFlag = false;
      this.eqpDamageFlag = true;
      this.vdrFlag = false;
      this.descriptionFormGroup.get(`isAddlInfoInvolvingInjury`).patchValue(0);
      this.descriptionFormGroup.get(`isAddlInfoInvolvingEqpDamage`).patchValue(1);
      this.descriptionFormGroup.get(`isIncidentVdrData`).patchValue(1);
    } else {
      this.additionInjuryFlag = false;
      this.eqpDamageFlag = false;
      this.vdrFlag = false;
      this.descriptionFormGroup.get(`isAddlInfoInvolvingInjury`).patchValue(1);
      this.descriptionFormGroup.get(`isAddlInfoInvolvingEqpDamage`).patchValue(1);
      this.descriptionFormGroup.get(`isIncidentVdrData`).patchValue(1);
    }
  }

  getImpactActiveClass() {
    if (this.impact < 5) {
      this.impactLabel = 'Low';
      return 'normal';
    } else if (this.impact > 4 && this.impact < 10) {
      this.impactLabel = 'Medium';
      return 'medium';
    } else {
      this.impactLabel = 'High';
      return 'extreme';
    }
  }

  onChangeHour(event: any) {}

  onChangeIncidentDate(event: any) {
    const date = event.value;
    log.debug('date: ', date);
    this.reportDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    log.debug('reportDate: ', this.reportDate);
    log.debug('this.reportDate.getDate():', this.reportDate.getDate());
    this.reportDate.setDate(this.reportDate.getDate());
    log.debug('this.reportDate:', this.reportDate.setDate(this.reportDate.getDate()));
  }

  onChangeseverity(event: any) {}

  onSeverityLevel(event: any) {
    const probabilityId = event.source.id;
    const levelIndex = probabilityId.substr(-1, 1);
    this.levelName = 'level' + levelIndex;
    this.severity = 5 - levelIndex;
    this.impact = this.severity * this.probabilityImpact;
    this.descriptionFormGroup.value.resultantRisk = this.impact;
  }

  onChangeprobability(event: any) {
    const probabilityId = event.source.id;
    const value = probabilityId.substr(-1, 1);
    this.probabilityImpact = value;
    this.impact = this.severity * this.probabilityImpact;
    this.descriptionFormGroup.value.resultantRisk = this.impact;
  }

  onChangeStopCardIssued(event: any) {
    this._onChangeStopCardIssued(this.stopCardIssuedValue);
    if (this.stopCardIssuedValue === 'yes') {
      this.readonly = true;
    } else {
      this.readonly = !this.readonly;
    }
  }

  onChangeImpact(eventImpact: string, isChecked: boolean) {
    const impactFormArray = this.descriptionFormGroup.controls.eventImpact as FormArray;
    if (isChecked) {
      impactFormArray.push(new FormControl(eventImpact));
      this.impactSelectedArray.push(eventImpact);
    } else {
      const index = impactFormArray.controls.findIndex((x) => x.value === eventImpact);
      impactFormArray.removeAt(index);

      const impactIndex = this.impactSelectedArray.indexOf(eventImpact);
      this.impactSelectedArray.splice(impactIndex, 1);
    }
  }

  getSelectBackground(event: any) {
    const condition = event.value;
    this.backgroundConditionData = [];
    if (this.bgImage.length > 0) {
      this.bgImage.forEach((element: any) => {
        this.backgroundConditionData.push(element);
      });
    }

    if (condition.length) {
      condition.forEach((element: any) => {
        if (this.bgImage.indexOf(element) > -1) {
        } else {
          const indexCount = this.backgroundConditionData.indexOf(element);
          if (indexCount > -1) {
          } else {
            this.backgroundConditionData.push(element);
          }
        }
      });
    }
  }

  getSelectedImpact(eventImpact: string) {
    const impactIndex = this.impactSelectedArray.indexOf(eventImpact);
    if (impactIndex > -1) {
      return true;
    } else {
      return false;
    }
  }

  resposibilityGroup() {
    this.designationData.map((designation: any) => {
      this.stateGroups.push({
        value: designation.designationId,
        label: designation.designation,
        group: 'Designation',
      });
      return designation;
    });

    this.users.map((user: any) => {
      this.stateGroups.push({
        value: user.userId,
        label: user.fullname,
        group: 'Users',
      });
    });

    this.stateGroups.push({
      value: 'Other',
      label: 'Other',
      group: 'Others',
    });
  }

  actionResponsible(event: any, index: any) {
    const other = event.value;
    const otherIndex = this.resposibilityArray.indexOf(index);
    if (otherIndex > -1) {
      this.resposibilityArray.splice(otherIndex, 1);
    } else {
      if (other === 'Other') {
        this.resposibilityArray.push(index);
      }
    }
  }

  getEmailAvailable(index: any) {
    const otherIndex = this.resposibilityArray.indexOf(index);
    if (otherIndex > -1) {
      return true;
    } else {
      return false;
    }
  }

  move(index: number) {
    // this.stepper.selectedIndex = index;
    this.activeIndex = index + 1;
    log.debug('index: ', index);
  }

  changeValue(value: any) {
    this.checked = !value;
  }

  //#region Formly Migration

  getIncidentResources() {
    /**
     * forkJoin runs observables in parallel, at the same time.
     * Hence reducing the amount of time to get data from
     * APIs
     */
    forkJoin(
      this.nearMissService.getContactOrEvent(),
      this.incidentService.getAllUsers(),
      this.incidentService.getBackgroundCondition(),
      this.incidentService.getElements(),
      this.incidentService.getInvestigation(),
      this.incidentService.getAllCompany(),
      this.incidentService.getEventImpactType(),
      this.incidentService.getFormFeed('Severity Level'),
      this.incidentService.getAllType('Report Type'),
      this.incidentService.getFormFeed('Probablity Of Happening'),
      this.getDesignation(),
      this.incidentService.getAnalysisOfSimilarIncident(),
      this.getByPosition()
    ).subscribe(
      ([
        contactOrEvent,
        allUsers,
        backgroundConditions,
        elements,
        investigation,
        allCompany,
        eventIncidents,
        severityLevels,
        reportTypes,
        probability,
        designation,
        similarIncident,
        investigatorsByPosition,
      ]: any) => {
        this.setAllUsers(allUsers);
        this.setEventIncidents(eventIncidents);
        this.setElements(elements);
        this.setSeverityLevel(severityLevels);
        this.setAllTypeList(reportTypes);
        this.setProbability(probability);
        this.resposibilityGroup();
        this.setSimilaryIncident(similarIncident);

        this.formlyConfig = this.formlyConfig.map((config) => {
          if (config.id === 'tmsaIncident') {
            config.config.fields = getTmsaFormFields({
              elements: this.elementData,
              filterStages: this.filterStages.bind(this),
              filterSpecificKPI: this.filterSpecificKPI.bind(this),
            });
            const tmsaIncident = this.formlyCacheService.get('tmsaIncident');
            if (tmsaIncident) {
              // TODO: Call clearTimeout in OnDestroy method
              setTimeout(() => {
                this.tmsaModel = tmsaIncident;
                config.config.model = tmsaIncident;
                this.cdr.detectChanges();
              }, 0);
            }
          }

          if (config.id === 'reportStatusIncident') {
            config.config.fields = getReportStatusFormFields({
              investigationList: investigation,
              allUsers: this.users,
            });

            const reportStatusIncident = this.formlyCacheService.get('reportStatusIncident');
            if (reportStatusIncident) {
              // TODO: Call clearTimeout in OnDestroy method
              setTimeout(() => {
                this.reportStatusModel = reportStatusIncident;
                config.config.model = reportStatusIncident;
                this.setReportStatusModel();
                this.cdr.detectChanges();
              }, 0);
            }
          }

          if (config.id === 'contactEstablishIncident') {
            config.config.fields = getContactEstablishFormFields({
              allCompany,
              allUsers: this.users,
            });

            const contactEstablishIncident = this.formlyCacheService.get('contactEstablishIncident');
            if (contactEstablishIncident) {
              // TODO: Call clearTimeout in OnDestroy method
              setTimeout(() => {
                this.contactEstablishModel = contactEstablishIncident;
                config.config.model = contactEstablishIncident;
                this.setContactEstablishmentModel();
                this.cdr.detectChanges();
              }, 0);
            }
          }

          if (config.id === 'descriptionIncident') {
            config.config.fields = getDescriptionFormFields({
              eventIncidentItems: this.eventIncidents,
              allUsers: this.users,
              formState: this.descriptionFormOptions.formState,
              severityLevel: this.severityLevel,
              eventImpacts: this.reportTypes.impactList,
              probability: this.probability,
              onMachineryFieldSelected: (data: any, field: FormlyFieldConfig) => {
                this.getEquipment().subscribe((response) => {
                  this.descriptionFormOptions.formState.equipmentList = this.equipmentList;
                });
                this.getEquipmentCategory().subscribe((response) => {
                  this.descriptionFormOptions.formState.equipmentCategoryList = this.equipmentCategoryList;
                });
              },

              onInjuryFieldSelected: (data: any, field: FormlyFieldConfig) => {
                forkJoin(
                  this.getInjuryCategory(),
                  this.getInjuryOccuredBy(),
                  this.getRankList(),
                  this.getBodyPartEffect(),
                  this.getNationality()
                ).subscribe(([injuryCategory, injuryOccuredBy, rankList, bodyPartEffect, nationality]) => {
                  this.descriptionFormOptions.formState.injuryCategory = injuryCategory;
                  this.descriptionFormOptions.formState.injuryOccuredBy = injuryOccuredBy;
                  this.descriptionFormOptions.formState.rankList = rankList;
                  this.descriptionFormOptions.formState.bodyPartEffect = bodyPartEffect;
                  this.descriptionFormOptions.formState.nationality = nationality;
                });
              },
            });
          }

          if (config.id === 'actionIncident') {
            config.config.fields = getActionFormFields({
              proposedBy: this.reportTypes.proposedBy.map((proposedby: string) => {
                return {
                  value: proposedby,
                  label: proposedby,
                };
              }),
              action: [],
              actionType: [],
              responsibility: this.stateGroups,
              actionStatus: this.reportTypes.actionStatus,
              formState: this.analysisFormOptions.formState,
            });

            const actionIncident = this.formlyCacheService.get('actionIncident');
            if (actionIncident) {
              setTimeout(() => {
                this.actionsModel = actionIncident;
                config.config.model = actionIncident;
                this.cdr.detectChanges();
              }, 0);
            }
          }

          if (config.id === 'analysisIncident') {
            config.config.fields = getAnalysisFormFields({
              similarIncident: this.similarIncident,
            });
          }

          if (config.id === 'overviewIncident') {
            const overviewIncident = this.formlyCacheService.get('overviewIncident');
            if (overviewIncident) {
              // TODO: Call clearTimeout in OnDestroy method
              setTimeout(() => {
                this.model = overviewIncident;
                config.config.model = overviewIncident;
                this.setOverviewModel(overviewIncident);
                this.cdr.detectChanges();
              }, 0);
            }
          }

          if (config.id === 'investigationIncident') {
            config.config.fields = getInvestigationFormFields({
              investigationList: investigation,
              investigationByPosition: investigatorsByPosition,
            });

            const investigationIncident = this.formlyCacheService.get('investigationIncident');
            if (investigationIncident) {
              // TODO: Call clearTimeout in OnDestroy method
              setTimeout(() => {
                this.investigationModel = investigationIncident;
                config.config.model = investigationIncident;
                this.setInvestigationModel();
                this.cdr.detectChanges();
              }, 0);
            }
          }

          if (config.id === 'vdrIncident') {
            const vdrIncident = this.formlyCacheService.get('vdrIncident');
            if (vdrIncident) {
              // TODO: Call clearTimeout in OnDestroy method
              setTimeout(() => {
                this.vdrModel = vdrIncident;
                config.config.options.resetModel({ ...vdrIncident });
                this.cdr.detectChanges();
              }, 0);
            }
          }

          if (config.id === 'notificationIncident') {
            const notificationIncident = this.formlyCacheService.get('notificationIncident');
            if (notificationIncident) {
              // TODO: Call clearTimeout in OnDestroy method
              setTimeout(() => {
                this.notificationModel = notificationIncident;
                config.config.options.resetModel({ ...notificationIncident });
                this.cdr.detectChanges();
              }, 0);
            }
          }

          if (config.id === 'postincidentrecoveryIncident') {
            const postincidentrecoveryIncident = this.formlyCacheService.get('postincidentrecoveryIncident');
            if (postincidentrecoveryIncident) {
              // TODO: Call clearTimeout in OnDestroy method
              setTimeout(() => {
                this.postMeasureModel = postincidentrecoveryIncident;
                config.config.options.resetModel({ ...postincidentrecoveryIncident });
                this.cdr.detectChanges();
              }, 0);
            }
          }

          if (config.id === 'backgroundIncident') {
            const backgroundIncident = this.formlyCacheService.get('backgroundIncident');
            if (backgroundIncident) {
              // TODO: Call clearTimeout in OnDestroy method
              setTimeout(() => {
                this.backgroundModel = backgroundIncident;
                config.config.options.resetModel({ ...backgroundIncident });
                this.cdr.detectChanges();
              }, 0);
            }
          }

          if (config.id === 'breachRegulatoryIncident') {
            const breachRegulatoryIncident = this.formlyCacheService.get('breachRegulatoryIncident');
            if (breachRegulatoryIncident) {
              // TODO: Call clearTimeout in OnDestroy method
              setTimeout(() => {
                this.breachModel = breachRegulatoryIncident;
                config.config.options.resetModel({ ...breachRegulatoryIncident });
                this.cdr.detectChanges();
              }, 0);
            }
          }

          if (config.id === 'sequenceEventsIncident') {
            const sequenceEventsIncident = this.formlyCacheService.get('sequenceEventsIncident');
            if (sequenceEventsIncident) {
              // TODO: Call clearTimeout in OnDestroy method
              setTimeout(() => {
                this.sequenceEventsModel = sequenceEventsIncident;
                config.config.options.resetModel({ ...sequenceEventsIncident });
                this.cdr.detectChanges();
              }, 0);
            }
          }
          return config;
        });
      }
    );
  }

  setAllUsers(users: any) {
    this.users = users;
  }

  setProbability(probability: any) {
    this.probability = probability;
  }

  setSimilaryIncident(similaryIncident: any[]) {
    this.similarIncident = similaryIncident;
  }

  userprofile() {
    this.currentUser = this.userProfileService.currentUser;
    this.form.patchValue({ userId: this.currentUser.userId });
  }

  onReviewOfVdrFileChange(files: File[], reviewOfVdrIndex?: any, vdrIndex?: any) {
    this.reviewOfVdrImages = [];

    if (files && files.length > 0) {
      files.map((file) => {
        log.debug('event.target.files[i]: ', file);
        this.reviewOfVdrImages.push(file);
        return file;
      });
    }
  }

  deleteReviewOfVdrImage(image: any, reviewOfVdrIndex: any, vdrIndex: any) {
    log.debug('reviewVdrIndex delete:', reviewOfVdrIndex);
    const fieldName = 'vdrAttachmentFieldName' + reviewOfVdrIndex + 'x' + vdrIndex;

    const index = this.reviewOfVdrFileArray.indexOf(image);
    log.debug('index: ', index);
    this.reviewOfVdrFileArray.splice(index, 1);
    const fieldNameIndex = this.reviewVdrImage[fieldName].indexOf(image);
    this.reviewOfVdrImages.splice(index, 1);
    this.reviewVdrImage[fieldName].splice(fieldNameIndex, 1);
    this.onReviewOfVdrFileChange(this.reviewOfVdrFileArray, reviewOfVdrIndex, vdrIndex);
  }

  onSubmitDescription(formValue: any, isPartial: boolean, stepId?: string) {
    this.setDescriptionModel();
  }

  setDescriptionModel() {
    const descriptionData = { ...this.descriptionModel };

    if (!this.descriptionFormOptions.formState.isInjuryField) {
      delete descriptionData.addlInfoInvolvingInjury;
      delete descriptionData.isAddlInfoInvolvingInjury;
      delete this.vdrModel.isIncidentVdrData;
    } else {
      descriptionData.isAddlInfoInvolvingInjury = true;
      this.vdrModel.isIncidentVdrData = true;
    }

    if (!this.descriptionFormOptions.formState.isMachineryField) {
      delete descriptionData.addlInfoInvolvingEqpDamage;
      delete descriptionData.isAddlInfoInvolvingEqpDamage;
    } else {
      descriptionData.isAddlInfoInvolvingEqpDamage = true;
    }

    descriptionData.eventImpact = Object.keys(this.descriptionModel.eventImpact);
    descriptionData.severityOfConsequences = Object.values(this.descriptionModel.eventImpact);
    descriptionData.resultantRisk = this.descriptionFormOptions.formState.resultantRisk;

    if (descriptionData.addlInfoInvolvingEqpDamage) {
      descriptionData.addlInfoInvolvingEqpDamage = descriptionData.addlInfoInvolvingEqpDamage.map((damage: IJson) => {
        if (!damage.isOther) {
          delete damage.newEquipment;
        }
        return damage;
      });
    }

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      ...descriptionData,
    };

    log.debug('Description Form: ', { descriptionData });
    log.debug('Cross Reference Form: ', { form: this.crossReferenceFormData });
  }

  onSubmitInvestigation(formValue: any, isPartial: boolean, stepId?: string) {
    this.setInvestigationModel();
  }

  setInvestigationModel() {
    const investigationData: IJson = { ...this.investigationModel };

    if (investigationData.department) {
      delete investigationData.department;
    }

    if (investigationData.designation) {
      delete investigationData.designation;
    }

    if (investigationData.investigationTeamMember) {
      investigationData.investigationTeamMember = investigationData.investigationTeamMember.map((member: IJson) => {
        return member.investigatorsId;
      });
    }

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      investigationTeam: {
        ...investigationData,
      },
    };

    log.debug('Investigation Form: ', investigationData);
    log.debug('Cross Reference Form: ', this.crossReferenceFormData);
  }

  onSubmitContactEst(formValue: any, isPartial: boolean, stepId?: string) {
    this.setContactEstablishmentModel();
  }

  setContactEstablishmentModel() {
    const contactModel = { ...this.contactEstablishModel };

    if (contactModel.intContact) {
      contactModel.intContact = contactModel.intContact.map((contact: any) => {
        delete contact.designation;
        return contact;
      });
    }

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      ...contactModel,
    };
    log.debug('Contact Establishment Form: ', contactModel);
    log.debug('Cross Reference Form: ', this.crossReferenceFormData);
  }

  onSubmitAnalysis(formValue: any, isPartial: boolean, stepId?: string) {
    log.debug('Analyisis log', this.analysisFormGroup.value);
    this.crossReferenceForm.patchValue(this.analysisFormGroup.value);
    log.debug(this.crossReferenceForm.value);
  }

  onSubmitActionForm(formValue: any, isPartial: boolean, stepId?: string) {
    const actionData = this.actionsModel;

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      ...actionData,
    };

    log.debug('Action Form: ', actionData);
    log.debug('Cross Reference Form: ', this.crossReferenceFormData);
  }

  onSubmitIncidentSeqDataForm(formValue: any, isPartial: boolean, stepId?: string) {
    this.setSequenceEventsModel();
  }

  onSubmitTmsa(formValue: any, isPartial: boolean, stepId?: string) {
    const tmsaData = this.tmsaModel;

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      ...tmsaData,
    };

    log.debug('Tmsa Form: ', tmsaData);
    log.debug('Cross Reference Form: ', this.crossReferenceFormData);
  }

  onSubmitReport(formValue: any, isPartial: boolean, stepId?: string) {
    this.setReportStatusModel();
    this.onSubmitCrossReference();
  }

  setReportStatusModel() {
    const reportData = { ...this.reportStatusModel };

    if (reportData.reportPreparedBy) {
      reportData.reportPreparedBy = reportData.reportPreparedBy.map((user: any) => {
        return user.internalContactId;
      });
    }

    if (reportData.reportReviewedBy) {
      reportData.reportReviewedBy = reportData.reportReviewedBy.map((user: any) => {
        delete user.designation;
        return user;
      });
    }

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      ...reportData,
    };

    log.debug('Report Form: ', reportData);
    log.debug('Cross Reference Form: ', this.crossReferenceFormData);
  }

  setSequenceEventsModel() {
    const sequenceData = this.sequenceEventsModel;

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      ...sequenceData,
    };

    log.debug('Sequence Event Form: ', sequenceData);
    log.debug('Cross Reference Form: ', this.crossReferenceFormData);
  }

  onSubmitIncidentVdr(formValue: any, isPartial: boolean, stepId?: string) {
    this.setVdrModel();
  }

  setVdrModel() {
    const { isIncidentVdrData, ...vdrData } = this.vdrModel;
    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      incidentVdrData: {
        ...vdrData,
      },
    };

    if (isIncidentVdrData) {
      this.crossReferenceFormData.isIncidentVdrData = isIncidentVdrData;
    }

    log.debug('Vdr Form: ', vdrData);
    log.debug('Vdr Images: ', this.reviewOfVdrImages);
    log.debug('Cross Reference Form: ', this.crossReferenceFormData);
  }

  onSubmitnotifyData(formValue: any, isPartial: boolean, stepId?: string) {
    const notificationData = { ...this.notificationModel };

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      notifyData: {
        ...notificationData,
      },
    };

    log.debug('Notification Form: ', notificationData);
    log.debug('Cross Reference Form: ', this.crossReferenceFormData);
  }

  onSubmitpostRec(formValue: any, isPartial: boolean, stepId?: string) {
    const postIncidentData = { ...this.postMeasureModel };

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      postRecData: {
        ...postIncidentData,
      },
    };

    log.debug('Post Incident Recovery Form: ', postIncidentData);
    log.debug('Cross Reference Form: ', this.crossReferenceFormData);
  }

  onSubmitBreach(formValue: any, isPartial: boolean, stepId?: string) {
    const breachData = { ...this.breachModel };

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      breachData: {
        ...breachData,
      },
    };

    this.crossReferenceForm.patchValue(this.breachDataForm.value);
    this.incidentCacheService.setBreachData(this.breachDataForm.value);
    log.debug(this.crossReferenceForm.value);
  }

  onSubmitBackground(formValue: any, isPartial: boolean, stepId?: string) {
    const { backgroundConditionId, ...backgroundData } = this.backgroundModel;

    backgroundData.backgroundData.map((bData: any, index: number) => {
      bData.backgroundConditionId = backgroundConditionId[index].backgroundConditionId;
      bData.isNew = false;
    });

    if (backgroundData.btnaddcondition) {
      delete backgroundData.btnaddcondition;
    }

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      ...backgroundData,
    };

    log.debug('Background Form: ', this.backgroundModel);
    log.debug('Cross Referene Form: ', this.crossReferenceFormData);
  }

  onOverviewSave(formValue: any, isPartial: boolean, stepId?: string) {
    formValue = this.setOverviewModel(formValue);
    if (isPartial) {
      this.savePartialIncidentData(formValue);
    } else {
      formValue.userId = this.currentUser.userId;
      log.debug('Overview Form: ', this.crossReferenceFormData);
    }
  }

  setOverviewModel(formValue: any) {
    formValue = this.transformOverviewData({ ...formValue });

    this.crossReferenceFormData = {
      ...this.crossReferenceFormData,
      ...formValue,
    };
    return formValue;
  }

  transformOverviewData(formValue: any) {
    const timeIncident = moment(formValue.timeOfIncident).format('HH:mm:ss');
    const dateTimeIncident = moment(formValue.timeOfIncident).format('YYYY-MM-DD');
    formValue.dateOfIncident = moment(formValue.dateOfIncident).format('YYYY-MM-DD');
    formValue.timeOfIncident = dateTimeIncident + ' ' + timeIncident;
    formValue.userId = this.currentUser.userId;
    return { ...formValue };
  }

  savePartialIncidentData(formValue: any) {
    const formData: FormData = new FormData();
    formValue.isPartiallySubmitted = '1';

    formData.appendAll(formValue);

    this.incidentService.createIncident(formData).subscribe(
      (response) => {
        this.incidentPostResponse = response;
        if (this.incidentPostResponse.statusCode === 201) {
          const message = this.incidentPostResponse.message;
          this.notifyService.showSuccess(message, this.title);
          this.overviewReset();
          this.analysisReset();
          this.actionReset();
          this.crossReferenceReset();
          this.router.navigate(['/incident']);
        } else {
          const message = this.incidentPostResponse.error.message;
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Equipment data
  getEquipment() {
    if (this.equipmentList.length > 0) {
      return of(this.equipmentList);
    }

    return this.incidentService.getEquipment().pipe(
      map((response) => {
        this.equipmentList = response;
        return response;
      })
    );
  }

  // Get Nationality data
  getNationality() {
    if (this.nationalityList.length > 0) {
      return of(this.nationalityList);
    }

    return this.incidentService.getNationality().pipe(
      map((response) => {
        this.nationalityList = response;
        return response;
      })
    );
  }

  // Get Body part affected data
  getBodyPartEffect() {
    if (this.bodyPartAffIdList.length > 0) {
      return of(this.bodyPartAffIdList);
    }

    return this.incidentService.getBodyPartEffected().pipe(
      map((response) => {
        this.bodyPartAffIdList = response;
        return response;
      })
    );
  }

  // Get Injury Occured data
  getInjuryOccuredBy() {
    if (this.injuryOccured.length > 0) {
      return of(this.injuryOccured);
    }

    return this.incidentService.getInjuryOccuredBy().pipe(
      map((response) => {
        this.injuryOccured = response;
        return response;
      })
    );
  }

  getInjuryCategory() {
    if (this.injuryCategory.length > 0) {
      return of(this.injuryCategory);
    }

    return this.incidentService.getInjuryCategory().pipe(
      map((response) => {
        this.injuryCategory = response;
        return response;
      })
    );
  }

  // Get Rank data
  getRankList() {
    if (this.rankList.length > 0) {
      return of(this.rankList);
    }

    return this.incidentService.getRank().pipe(
      map((response) => {
        this.rankList = response;
        return response;
      })
    );
  }

  // Get Designation Condition data
  getDesignation() {
    return this.incidentService.getDesignation().pipe(
      map((response: IJson) => {
        this.designationList = response;
        return response.map((element: IJson) => {
          this.designationData.push({
            designationId: element.designationId,
            designation: element.designation,
          });
          return element;
        });
      })
    );
  }

  // Get Equipment Category data
  getEquipmentCategory() {
    return this.incidentService.getEquipmentCategory().pipe(
      map((response) => {
        this.equipmentCategoryList = response;
        return response;
      })
    );
  }

  async onDescriptionModelChange(value: IJson<any>) {
    const eventImpact: IJson<boolean> = value.eventImpact as IJson<boolean>;

    if (eventImpact.People && !this.severityDescriptions.People) {
      this.severityDescriptions.People = await this.getSeverityConsequences(['People']);
      this.descriptionFormOptions.formState.severityDescriptions.People = this.severityDescriptions.People;
    }

    if (eventImpact.Environment && !this.severityDescriptions.Environment) {
      this.severityDescriptions.Environment = await this.getSeverityConsequences(['Environment']);
      this.descriptionFormOptions.formState.severityDescriptions.Environment = this.severityDescriptions.Environment;
    }

    if (eventImpact['Assets/Property/Financial'] && !this.severityDescriptions['Assets/Property/Financial']) {
      this.severityDescriptions['Assets/Property/Financial'] = await this.getSeverityConsequences([
        'Assets/Property/Financial',
      ]);
      this.descriptionFormOptions.formState.severityDescriptions[
        'Assets/Property/Financial'
      ] = this.severityDescriptions['Assets/Property/Financial'];
    }

    if (eventImpact['Reputation/Business'] && !this.severityDescriptions['Reputation/Business']) {
      this.severityDescriptions['Reputation/Business'] = await this.getSeverityConsequences(['Reputation/Business']);
      this.descriptionFormOptions.formState.severityDescriptions['Reputation/Business'] = this.severityDescriptions[
        'Reputation/Business'
      ];
    }

    if (this.severityDescriptions.People) {
      this.descriptionModel.eventImpact.People = this.severityDescriptions.People[value.severityLevel].id;
    }

    if (this.severityDescriptions.Environment) {
      this.descriptionModel.eventImpact.Environment = this.severityDescriptions.Environment[value.severityLevel].id;
    }

    if (this.severityDescriptions['Assets/Property/Financial']) {
      this.descriptionModel.eventImpact['Assets/Property/Financial'] = this.severityDescriptions[
        'Assets/Property/Financial'
      ][value.severityLevel].id;
    }

    if (this.severityDescriptions['Reputation/Business']) {
      this.descriptionModel.eventImpact['Reputation/Business'] = this.severityDescriptions['Reputation/Business'][
        value.severityLevel
      ].id;
    }

    if (eventImpact) {
      for (const key in eventImpact) {
        if (eventImpact[key]) {
          this.descriptionFormOptions.formState.severityDescriptions[key] = this.severityDescriptions[key] || {};
        } else {
          this.descriptionFormOptions.formState.severityDescriptions[key] = {};
        }
      }
    }
  }

  async getSeverityConsequences(impact: string[]) {
    const data = await this.incidentService.getSeverityofConsequence(null, impact).toPromise();
    const consequencesData: any = {};
    data.map((_data: any) => {
      consequencesData[_data.severityLevel] = {
        description: _data.descriptions,
        id: _data.severityOfConsequencesId,
      };
      return _data;
    });
    return consequencesData;
  }

  getVessels() {
    const vesselParam = new Map<string, any>();
    vesselParam.set('myvessel', 'true');
    return this.incidentService.getVessels(vesselParam);
  }

  // Get port list data
  getNearMissPortList(): Observable<any[]> {
    return this.nearMissService.getPortList<any[]>().pipe(
      map((response) => {
        response.unshift({ portId: '', name: 'Select port name' });
        return response;
      })
    );
  }

  onPortSeaChange(value: string) {
    this.form.patchValue({ isLatLong: value === '1' ? '1' : '0' });
  }

  // Get Position Category data
  getByPosition(position: string = 'TM') {
    return this.incidentService.getByPosition(position);
  }

  // Get Eelement Data list data
  setElements(elements: any[]) {
    this.incidentCacheService.setElementData(elements[0]);
    this.elementData = elements[0];
  }

  // Get Background Condition data
  getBackgroundCondition() {
    return this.incidentService.getBackgroundCondition<any>().pipe(
      map((value) => {
        this.backgroundDataArray = value;
        this.backgroundDataJson = {};
        this.backgroundDataArray.map((bgData: any) => {
          this.backgroundDataJson[bgData.backgroundConditionId] = bgData.background;
          return bgData;
        });
        return value;
      })
    );
  }

  // Get Event Impact data
  getEventImpact(eventImpacts: any[]) {
    this.eventImpact = eventImpacts.map((event: IJson) => {
      if (event.eventImpactType === 'Injury') {
        event.group = 'Injury';
      } else if (event.eventImpactType === 'Equipment or Machinery Damage') {
        event.group = 'Equipment Or Machinery Damage';
      } else {
        event.group = 'General';
      }
      return event;
    });
  }

  setEventIncidents(incidents: any[]) {
    this.eventIncidents = incidents.map((event: IJson) => {
      if (event.eventImpactType === 'Injury') {
        event.group = 'Injury';
      } else if (event.eventImpactType === 'Equipment or Machinery Damage') {
        event.group = 'Equipment Or Machinery Damage';
      } else {
        event.group = 'General';
      }
      return event;
    });
  }

  // Get Report Type List data
  setSeverityLevel(severityLevels: any[]) {
    this.severityLevel = severityLevels
      .filter((severity) => severity.module === 'incident')
      .map((level) => {
        return {
          id: level.formFeederId,
          name: level.name,
          description: level.description,
        };
      });
  }

  setAllTypeList(reportTypes: IJson) {
    this.reportTypes = {
      impactList: reportTypes.impactList.map((impact: string) => {
        return {
          value: impact,
          label: impact,
        };
      }),
      actionStatus: reportTypes.status.map((status: string) => {
        return {
          value: status,
          label: status,
        };
      }),
      observedBy: reportTypes.observedBy,
      proposedBy: reportTypes.proposedBy,
      department: reportTypes.department,
    };
  }

  //#endregion End Formly Migration

  /**  Reset stepper Form Function :: start */

  analysisRest() {
    this.analysisFormGroup.reset();
  }

  analysisReset() {
    this.analysisFormGroup.reset();
    this.contactOrEventOptionActive = [];
    this.basicImmidateOptionActive = [];
    this.rootCauseOptionActive = [];
    this.correctiveCauseOptionActive = [];
  }

  analysisChartReset() {
    this.contactOrEventOptionActive = [];
    this.basicImmidateOptionActive = [];
    this.rootCauseOptionActive = [];
    this.correctiveCauseOptionActive = [];
    this.typeOfContactArray = [];
    this.basicCauseArray = [];
    this.rootCauseArray = [];
    this.correctiveActionId = '';
    this.activeRootCause = '';
    this.activeImmediateCause = '';
    this.activeContactCause = '';
    const form = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
    form.clear();
  }

  descriptionReset() {
    this.descriptionFormGroup.reset();
    this.incidentCacheService.setAnalysisData('');
  }

  attachmentReset() {
    this.attachmentFormGroup.reset();
  }

  crossReferenceReset() {
    this.crossReferenceForm.reset();
    this.incidentCacheService.setCrossReferenceData('');
  }

  actionReset() {
    this.actionForm.reset();
    this.incidentCacheService.setActionData('');
  }

  overviewReset() {
    this.form.reset();
    this.incidentCacheService.setOverviewData('');
  }

  seqDataReset() {
    this.incidentSeqDataForm.reset();
    this.incidentCacheService.setIncidentSeqData();
  }

  reviewVdrReset() {
    this.incidentVdrDataForm.reset();
    this.incidentCacheService.setIncidentVdrData('');
  }

  notifyReset() {
    this.notifyDataForm.reset();
    this.incidentCacheService.setNotifyData('');
  }

  postRecReset() {
    this.postRecDataForm.reset();
    this.incidentCacheService.setPostRecData('');
  }

  breachReset() {
    this.breachDataForm.reset();
    this.incidentCacheService.setBreachData('');
  }

  investigationReset() {
    this.investigationTeamForm.reset();
    this.incidentCacheService.setInvestigationData('');
  }

  contactEstReset() {
    this.contactEstablishForm.reset();
    this.incidentCacheService.setContactEstablishData('');
  }

  backgroundDataFormReset() {
    this.backgroundDataForm.reset();
    this.backgroundImage = [];
  }

  /**  Reset stepper Form Function :: end */

  onSubmitCrossReference() {
    this.incidentCacheService.setCrossReferenceData(this.crossReferenceForm.value);
    this.crossReferenceForm.patchValue(this.backgroundDataForm.value);
    log.debug('------', this.backgroundDataForm.value);
    this.crossReferenceForm.patchValue(this.descriptionFormGroup.value);
    const data = this.crossReferenceForm.value;
    const latitude = this.overViewFormGroup.value.latitude;
    const longitude = this.overViewFormGroup.value.longitude;
    const latLong = latitude + ', ' + longitude;
    const impactData = this.descriptionFormGroup.value.eventImpact;

    const formData: FormData = new FormData();
    let sequenceCount = 0;
    // tslint:disable-next-line:forin
    for (const dataKey in data) {
      if (dataKey === 'incidentSeqData') {
        // append nested object
        // const datas = data[dataKey];
        const incidentSeqData = this.incidentSeqDataForm.value.incidentSeqData;
        let index = 0;
        if (incidentSeqData instanceof Array) {
          incidentSeqData.forEach((element) => {
            for (const previewKey in element) {
              if (previewKey === 'seqData') {
                const seqData = incidentSeqData[index][previewKey];
                let index2 = 0;
                seqData.forEach((elements: any) => {
                  // tslint:disable-next-line:forin
                  for (const seqDat in elements) {
                    const seData = incidentSeqData[index][previewKey][index2][seqDat];
                    if (seqDat === 'triggeringFactors') {
                      if (seData === true) {
                        formData.append(`incidentSeqData[${index}][${previewKey}][${index2}][${seqDat}]`, '1');
                      } else {
                        formData.append(`incidentSeqData[${index}][${previewKey}][${index2}][${seqDat}]`, '0');
                      }
                    } else if (seqDat === 'ltTime') {
                      this.ltTimeInt = '';
                      const ltTimeFormatting = moment(seData).format('HH:mm');
                      this.ltTimeInt = parseInt(ltTimeFormatting.replace(':', ''), 10);
                      formData.append(`incidentSeqData[${index}][${previewKey}][${index2}][${seqDat}]`, this.ltTimeInt);
                    } else {
                      formData.append(`incidentSeqData[${index}][${previewKey}][${index2}][${seqDat}]`, seData);
                    }
                  }
                  index2++;
                });
              } else if (previewKey === 'date') {
                const convertedDate = moment(incidentSeqData[index][previewKey]).startOf('day');
                const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
                formData.append(`incidentSeqData[${index}][${previewKey}]`, dateOfExpiry);
              } else {
                formData.append(`incidentSeqData[${index}][${previewKey}]`, incidentSeqData[index][previewKey]);
              }
            }
            index++;
          });
        }

        for (let index5 = 0; index5 < this.images.length; index5++) {
          formData.append(`seqOfEventAttachments[${index5}]`, this.images[index5]);
        }
      } else if (dataKey === 'probablityReccData') {
        // append nested object
        const datas = data[dataKey];
        for (const previewKey in datas) {
          if (data[dataKey][previewKey]) {
            formData.append(`probablityReccData[${previewKey}]`, data[dataKey][previewKey]);
          }
        }
      } else if (dataKey === 'isAddlInfoInvolvingInjury') {
        const keyValue = data[dataKey];
        if (keyValue) {
          const newkeyValues = '1';
          formData.append(`isAddlInfoInvolvingInjury`, newkeyValues);
        } else {
          const newkeyValues = '0';
          formData.append(`isAddlInfoInvolvingInjury`, newkeyValues);
        }
      } else if (dataKey === 'addlInfoInvolvingInjury') {
        // append nested object
        const addlInfoInvolvingInjury = this.descriptionFormGroup.value.addlInfoInvolvingInjury;
        let index = 0;
        if (addlInfoInvolvingInjury instanceof Array) {
          addlInfoInvolvingInjury.forEach((element) => {
            // tslint:disable-next-line:forin
            for (const previewKey in element) {
              formData.append(
                `addlInfoInvolvingInjury[${index}][${previewKey}]`,
                addlInfoInvolvingInjury[index][previewKey]
              );
            }
            index++;
          });
        }
      } else if (dataKey === 'isAddlInfoInvolvingEqpDamage') {
        const keyValue = data[dataKey];
        if (keyValue) {
          const newkeyValues = '1';
          formData.append(`isAddlInfoInvolvingEqpDamage`, newkeyValues);
        } else {
          const newkeyValues = '0';
          formData.append(`isAddlInfoInvolvingEqpDamage`, newkeyValues);
        }
      } else if (dataKey === 'addlInfoInvolvingEqpDamage') {
        // append nested object
        // const addlInfoInvolvingInjury = data[dataKey];
        const addlInfoInvolvingInjury = this.descriptionFormGroup.value.addlInfoInvolvingEqpDamage;

        let index = 0;
        if (addlInfoInvolvingInjury instanceof Array) {
          addlInfoInvolvingInjury.forEach((element: any) => {
            // tslint:disable-next-line:forin
            for (const previewKey in element) {
              const isOther = element.isOther;
              if (isOther) {
                if (previewKey === 'equipmentId') {
                } else if (previewKey === 'isOther') {
                  formData.append(`addlInfoInvolvingEqpDamage[${index}][${previewKey}]`, '1');
                } else {
                  formData.append(
                    `addlInfoInvolvingEqpDamage[${index}][${previewKey}]`,
                    addlInfoInvolvingInjury[index][previewKey]
                  );
                }
              } else {
                if (previewKey === 'newEquipment') {
                } else if (previewKey === 'isOther') {
                  formData.append(`addlInfoInvolvingEqpDamage[${index}][${previewKey}]`, '0');
                } else {
                  formData.append(
                    `addlInfoInvolvingEqpDamage[${index}][${previewKey}]`,
                    addlInfoInvolvingInjury[index][previewKey]
                  );
                }
                // formData.append(
                //   `addlInfoInvolvingEqpDamage[${index}][${previewKey}]`,
                //   addlInfoInvolvingInjury[index][previewKey]
                // );
              }
            }
            index++;
          });
        }
      } else if (dataKey === 'investigationTeam') {
        const investigationTeam = this.investigationTeamForm.value.investigationTeam;
        for (const previewKey in investigationTeam) {
          if (previewKey === 'investigationTeamMember') {
            const investigationTeamMember = investigationTeam[previewKey];
            let index2 = 0;
            let newIndexKey = 0;
            investigationTeamMember.forEach((elements: any) => {
              const newTeamMember = elements.isNewTeamMember;

              if (newTeamMember) {
                for (const teamMember in elements) {
                  if (teamMember === 'teamMemberId') {
                  } else if (teamMember === 'position') {
                    formData.append(
                      `investigationTeam[newinvestigationTeamMember][${newIndexKey}][${teamMember}]`,
                      'Team Member'
                    );
                  } else {
                    formData.append(
                      `investigationTeam[newinvestigationTeamMember][${newIndexKey}][${teamMember}]`,
                      investigationTeam[previewKey][index2][teamMember]
                    );
                  }
                }

                // formData.append(
                //   `investigationTeam[newinvestigationTeamMember][${newIndexKey}]`,
                //   investigationTeam[previewKey]
                // );
                newIndexKey++;
              } else {
                for (const teamMember in elements) {
                  if (teamMember === 'teamMemberId') {
                    formData.append(
                      `investigationTeam[${previewKey}][${index2}]`,
                      investigationTeam[previewKey][index2][teamMember]
                    );
                  }
                }
              }

              index2++;
            });
          } else if (previewKey === 'reportPreparedBy') {
            const investigationTeamMember = investigationTeam[previewKey];
            let index2 = 0;
            investigationTeamMember.forEach((elements: any) => {
              // tslint:disable-next-line:forin
              for (const teamMember in elements) {
                if (teamMember === 'reportPreparedById') {
                  formData.append(
                    `investigationTeam[${previewKey}][${index2}]`,
                    investigationTeam[previewKey][index2][teamMember]
                  );
                }
              }
              index2++;
            });
          } else if (previewKey === 'reportReviewedBy') {
            const investigationTeamMember = investigationTeam[previewKey];
            let index2 = 0;
            investigationTeamMember.forEach((elements: any) => {
              // tslint:disable-next-line:forin
              for (const teamMember in elements) {
                if (teamMember === 'reportReviewedById') {
                  formData.append(
                    `investigationTeam[${previewKey}][${index2}][${teamMember}]`,
                    investigationTeam[previewKey][index2][teamMember]
                  );
                } else if (teamMember === 'dateOfReview') {
                  const convertedDate = moment(investigationTeam[previewKey][index2][teamMember]).startOf('day');
                  const dateOfReview = moment(convertedDate).format('YYYY-MM-DD');
                  formData.append(`investigationTeam[${previewKey}][${index2}][${teamMember}]`, dateOfReview);
                }
              }
              index2++;
            });
          } else if (previewKey === 'dateOfApprove') {
            const convertedDate = moment(investigationTeam[previewKey]).startOf('day');
            const dateOfApprove = moment(convertedDate).format('YYYY-MM-DD');
            // THH:mm:ss
            formData.append(`investigationTeam[${previewKey}]`, dateOfApprove);
          } else {
            formData.append(`investigationTeam[${previewKey}]`, investigationTeam[previewKey]);
          }
        }
        //  index++;
        //   });
        // }
      } else if (dataKey === 'intContact') {
        // append nested object
        // const intContact = data[dataKey];
        const intContact = this.contactEstablishForm.value.intContact;
        let index = 0;
        if (intContact instanceof Array) {
          intContact.forEach((element) => {
            // tslint:disable-next-line:forin
            for (const previewKey in element) {
              formData.append(`intContact[${index}][${previewKey}]`, intContact[index][previewKey]);
            }
            index++;
          });
        }
      } else if (dataKey === 'extContact') {
        // append nested object
        // const extContact = data[dataKey];
        const extContact = this.contactEstablishForm.value.extContact;
        let index = 0;
        if (extContact instanceof Array) {
          extContact.forEach((element: any) => {
            // tslint:disable-next-line:forin
            for (const previewKey in element) {
              formData.append(`extContact[${index}][${previewKey}]`, extContact[index][previewKey]);
            }
            index++;
          });
        }
      } else if (dataKey === 'backgroundData') {
        // append nested object
        const backgroundData = this.backgroundDataForm.value.backgroundData;
        let index = 0;
        if (backgroundData instanceof Array) {
          backgroundData.forEach((element: any) => {
            // tslint:disable-next-line:forin
            for (const previewKey in element) {
              const fieldName = 'backgroundAttachments' + index;
              if (this.backgroundImage[fieldName]) {
                if (previewKey === 'attachmentFieldName') {
                  formData.append(`backgroundData[${index}][${previewKey}]`, fieldName);
                  // tslint:disable-next-line:forin
                  for (let index5 = 0; index5 < this.backgroundImage[fieldName].length; index5++) {
                    formData.append(`${fieldName}[${index5}]`, this.backgroundImage[fieldName][index5]);
                  }
                } else if (previewKey === 'isContibutingFactor') {
                  if (backgroundData[index][previewKey] === true) {
                    formData.append(`backgroundData[${index}][${previewKey}]`, '1');
                  } else {
                    formData.append(`backgroundData[${index}][${previewKey}]`, '0');
                  }
                } else if (previewKey === 'backgroundId') {
                  if (backgroundData[index][previewKey]) {
                    formData.append(`backgroundData[${index}][${previewKey}]`, backgroundData[index][previewKey]);
                  } else {
                  }
                } else {
                  formData.append(`backgroundData[${index}][${previewKey}]`, backgroundData[index][previewKey]);
                }
              } else if (!this.backgroundImage[fieldName] && element.attachmentFieldName) {
                if (previewKey === 'isContibutingFactor') {
                  if (backgroundData[index][previewKey] === true) {
                    formData.append(`backgroundData[${index}][${previewKey}]`, '1');
                  } else {
                    formData.append(`backgroundData[${index}][${previewKey}]`, '0');
                  }
                } else if (previewKey === 'attachmentFieldName') {
                } else {
                  formData.append(`backgroundData[${index}][${previewKey}]`, backgroundData[index][previewKey]);
                }
              }
            }

            // formData.append(`backgroundData[${index}][AttachmentFieldName]`, 'attachmentName');

            index++;
          });
        }
      } else if (dataKey === 'isIncidentVdrData') {
        const keyValue = data[dataKey];
        if (keyValue) {
          const newkeyValues = '1';
          formData.append(`isIncidentVdrData`, newkeyValues);
        } else {
          const newkeyValues = '0';
          formData.append(`isIncidentVdrData`, newkeyValues);
        }
      } else if (dataKey === 'incidentVdrData') {
        // append nested object
        // const incidentVdrData = data[dataKey];
        const incidentVdrData = this.incidentVdrDataForm.value.incidentVdrData;
        const isVdrDataKey = this.descriptionFormGroup.value.isIncidentVdrData;
        log.debug('Incident VDR Key check', isVdrDataKey);
        if (isVdrDataKey) {
          let index = 0;
          for (const previewKey in incidentVdrData) {
            if (previewKey === 'reviewVdrDate') {
              const reviewVdrDate = incidentVdrData[previewKey];
              let index2 = 0;
              reviewVdrDate.forEach((elements: any) => {
                for (const reviewVdr in elements) {
                  if (reviewVdr === 'reviewVdrData') {
                    log.debug('Read reviewVdr data', elements);
                    const reviewVdrData = incidentVdrData[previewKey][index2][reviewVdr];
                    let index3 = 0;
                    reviewVdrData.forEach((elements2: any) => {
                      // tslint:disable-next-line:forin
                      for (const review in elements2) {
                        const fieldName = 'vdrAttachmentFieldName' + index + 'x' + index2;
                        if (review === 'vdrAttachmentFieldName') {
                          formData.append(
                            `incidentVdrData[${previewKey}][${index2}][${reviewVdr}][${index3}][${review}]`,
                            fieldName
                          );
                          if (this.reviewVdrImage[fieldName]) {
                            for (let index5 = 0; index5 < this.reviewVdrImage[fieldName].length; index5++) {
                              formData.append(`${fieldName}[${index5}]`, this.reviewVdrImage[fieldName][index5]);
                            }
                          }
                          // tslint:disable-next-line:forin
                          // for (let index5 = 0; index5 < this.reviewVdrImage[fieldName].length; index5++) {
                          //   formData.append(`${fieldName}[${index5}]`, this.reviewVdrImage[fieldName][index5]);
                          // }
                        } else if (review === 'ltTime') {
                          this.ltTimeInt = '';
                          const ltTime = incidentVdrData[previewKey][index2][reviewVdr][index3][review];
                          const ltTimeFormatting = moment(ltTime).format('HH:mm');
                          this.ltTimeInt = parseInt(ltTimeFormatting.replace(':', ''), 10);
                          formData.append(
                            `incidentVdrData[${previewKey}][${index2}][${reviewVdr}][${index3}][${review}]`,
                            this.ltTimeInt
                          );
                        } else {
                          formData.append(
                            `incidentVdrData[${previewKey}][${index2}][${reviewVdr}][${index3}][${review}]`,
                            incidentVdrData[previewKey][index2][reviewVdr][index3][review]
                          );
                        }
                      }
                      index3++;
                    });
                  } else if (reviewVdr === 'date') {
                    const convertedDate = moment(incidentVdrData[previewKey][index2][reviewVdr]).startOf('day');
                    const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
                    formData.append(`incidentVdrData[${previewKey}][${index2}][${reviewVdr}]`, dateOfExpiry);
                  } else {
                    formData.append(
                      `incidentVdrData[${previewKey}][${index2}][${reviewVdr}]`,
                      incidentVdrData[previewKey][index2][reviewVdr]
                    );
                  }
                }
                index2++;
              });
            } else {
              formData.append(`incidentVdrData[${previewKey}]`, incidentVdrData[previewKey]);
            }
          }
          index++;
        }
      } else if (dataKey === 'postRecData') {
        // append nested object
        // const postRecData = data[dataKey];
        const postRecData = this.postRecDataForm.value.postRecData;
        let index = 0;
        for (const previewKey in postRecData) {
          if (previewKey === 'postRecoveryDate') {
            const reviewVdrDate = postRecData[previewKey];
            let index2 = 0;
            reviewVdrDate.forEach((elements: any) => {
              for (const postRecoveryData in elements) {
                if (postRecoveryData === 'postRecoveryData') {
                  const postRecovery = postRecData[previewKey][index2][postRecoveryData];
                  let index3 = 0;
                  postRecovery.forEach((elements2: any) => {
                    // tslint:disable-next-line:forin
                    for (const post in elements2) {
                      if (post === 'date') {
                        const convertedDate = moment(postRecovery[index3][post]).startOf('day');
                        const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
                        formData.append(
                          `postRecData[${previewKey}][${index2}][${postRecoveryData}][${index3}][${post}]`,
                          dateOfExpiry
                        );
                      } else if (post === 'ltTime') {
                        this.ltTimeInt = '';
                        const ltTime = postRecovery[index3][post];
                        const ltTimeFormatting = moment(ltTime).format('HH:mm');
                        this.ltTimeInt = parseInt(ltTimeFormatting.replace(':', ''), 10);
                        formData.append(
                          `postRecData[${previewKey}][${index2}][${postRecoveryData}][${index3}][${post}]`,
                          this.ltTimeInt
                        );
                      } else {
                        formData.append(
                          `postRecData[${previewKey}][${index2}][${postRecoveryData}][${index3}][${post}]`,
                          postRecovery[index3][post]
                        );
                      }
                    }
                    index3++;
                  });
                } else if (postRecoveryData === 'date') {
                  const convertedDate = moment(postRecData[previewKey][index2][postRecoveryData]).startOf('day');
                  const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
                  formData.append(`postRecData[${previewKey}][${index2}][${postRecoveryData}]`, dateOfExpiry);
                } else {
                  formData.append(
                    `postRecData[${previewKey}][${index2}][${postRecoveryData}]`,
                    postRecData[previewKey][index2][postRecoveryData]
                  );
                }
              }
              index2++;
            });
          } else {
            formData.append(`postRecData[${previewKey}]`, postRecData[previewKey]);
          }
        }
        index++;
        for (let index5 = 0; index5 < this.postRecFile.length; index5++) {
          formData.append(`postRecoveryMeasuresAttachments[${index5}]`, this.postRecFile[index5]);
        }
      } else if (dataKey === 'notifyData') {
        // append nested object
        // const notifyData = data[dataKey];
        const notifyData = this.notifyDataForm.value.notifyData;
        let index = 0;
        for (const previewKey in notifyData) {
          if (previewKey === 'notificationDate') {
            const notificationDate = notifyData[previewKey];
            let index2 = 0;
            notificationDate.forEach((elements: any) => {
              for (const notify in elements) {
                if (notify === 'notificationData') {
                  const notificationData = notifyData[previewKey][index2][notify];
                  let index3 = 0;
                  notificationData.forEach((elements2: any) => {
                    // tslint:disable-next-line:forin
                    for (const notifydata in elements2) {
                      if (notifydata === 'ltTime') {
                        this.ltTimeInt = '';
                        const ltTime = notificationData[index3][notifydata];
                        const ltTimeFormatting = moment(ltTime).format('HH:mm');
                        this.ltTimeInt = parseInt(ltTimeFormatting.replace(':', ''), 10);
                        formData.append(
                          `notifyData[${previewKey}][${index2}][${notify}][${index3}][${notifydata}]`,
                          this.ltTimeInt
                        );
                      } else {
                        formData.append(
                          `notifyData[${previewKey}][${index2}][${notify}][${index3}][${notifydata}]`,
                          notificationData[index3][notifydata]
                        );
                      }
                    }
                    index3++;
                  });
                } else if (notify === 'date') {
                  const convertedDate = moment(notifyData[previewKey][index2][notify]).startOf('day');
                  const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
                  formData.append(`notifyData[${previewKey}][${index2}][${notify}]`, dateOfExpiry);
                } else {
                  formData.append(
                    `notifyData[${previewKey}][${index2}][${notify}]`,
                    notifyData[previewKey][index2][notify]
                  );
                }
              }
              index2++;
            });
          } else {
            if (notifyData[previewKey]) {
              formData.append(`notifyData[${previewKey}]`, notifyData[previewKey]);
            }
          }
        }
        index++;
        for (let index5 = 0; index5 < this.notifyFile.length; index5++) {
          formData.append(`notificationAttachments[${index5}]`, this.notifyFile[index5]);
        }
      } else if (dataKey === 'breachData') {
        // append nested object
        // const breachData = data[dataKey];
        const breachData = this.breachDataForm.value.breachData;
        let index = 0;
        for (const previewKey in breachData) {
          if (previewKey === 'breachReqData') {
            const breachReqData = breachData[previewKey];
            let index2 = 0;
            breachReqData.forEach((elements: any) => {
              // tslint:disable-next-line:forin
              for (const breachReq in elements) {
                if (breachReq === 'ltTime') {
                  this.ltTimeInt = '';
                  const ltTime = breachData[previewKey][index2][breachReq];
                  const ltTimeFormatting = moment(ltTime).format('HH:mm');
                  this.ltTimeInt = parseInt(ltTimeFormatting.replace(':', ''), 10);
                  formData.append(`breachData[${previewKey}][${index2}][${breachReq}]`, this.ltTimeInt);
                } else {
                  formData.append(
                    `breachData[${previewKey}][${index2}][${breachReq}]`,
                    breachData[previewKey][index2][breachReq]
                  );
                }
              }
              index2++;
            });
          } else {
            formData.append(`breachData[${previewKey}]`, breachData[previewKey]);
          }
        }
        index++;

        for (let index5 = 0; index5 < this.breachFile.length; index5++) {
          formData.append(`breachAttachments[${index5}]`, this.breachFile[index5]);
        }
      } else if (dataKey === 'typeOfContactOrEvent') {
        const datas = this.analysisFormGroup.value.typeOfContactOrEvent;
        let index = 0;
        datas.forEach((element: any) => {
          if (element.typeOfCntctId) {
            for (const previewKey in element) {
              if (previewKey === 'basicImmediateCause') {
                const basicImmediateCause = datas[index][previewKey];
                let index2 = 0;
                basicImmediateCause.forEach((elements: any) => {
                  for (const basicCause in elements) {
                    if (datas[index][previewKey][index2][basicCause]) {
                      if (basicCause === 'basicRootCause') {
                        const basicRootCause = datas[index][previewKey][index2][basicCause];
                        let index3 = 0;
                        basicRootCause.forEach((element3: any) => {
                          for (const rootCause in element3) {
                            if (rootCause === 'correctiveActionId') {
                              const basicRoot = basicRootCause[index3][rootCause];
                              let index4 = 0;
                              basicRoot.forEach((element4: any) => {
                                formData.append(
                                  `typeOfContactOrEvent[${index}][${previewKey}][${index2}][${basicCause}][${index3}][${rootCause}][${index4}]`,
                                  basicRoot[index4]
                                );
                                index4++;
                              });
                            } else if (rootCause === 'rootCauseExplanation') {
                              const rootCauseId = 'furtherExplanation';
                              const basicRoot = basicRootCause[index3][rootCauseId];
                              formData.append(
                                `typeOfContactOrEvent[${index}][${previewKey}][${index2}][${basicCause}][${index3}][${rootCause}]`,
                                basicRoot
                              );
                            } else if (rootCause === 'furtherExplanation') {
                            } else {
                              formData.append(
                                `typeOfContactOrEvent[${index}][${previewKey}][${index2}][${basicCause}][${index3}][${rootCause}]`,
                                basicRootCause[index3][rootCause]
                              );
                            }
                          }
                          index3++;
                        });
                      } else if (basicCause === 'immediateCauseExplanation') {
                        const basicCauseId = 'furtherExplanation';
                        const basicRootCause = datas[index][previewKey][index2][basicCauseId];
                        formData.append(
                          `typeOfContactOrEvent[${index}][${previewKey}][${index2}][${basicCause}]`,
                          basicRootCause
                        );
                      } else if (basicCause === 'furtherExplanation') {
                      } else {
                        formData.append(
                          `typeOfContactOrEvent[${index}][${previewKey}][${index2}][${basicCause}]`,
                          datas[index][previewKey][index2][basicCause]
                        );
                      }
                    }
                  }
                  index2++;
                });
              } else {
                formData.append(`typeOfContactOrEvent[${index}][${previewKey}]`, datas[index][previewKey]);
              }
            }
          }
          index++;
        });
      } else if (dataKey === 'crossRefToTMSA') {
        // const datas = data[dataKey];
        const crossRefToTMSA = this.crossReferenceForm.value.crossRefToTMSA;
        let index = 0;
        crossRefToTMSA.forEach((element: any) => {
          for (const previewKey in element) {
            if (crossRefToTMSA[index][previewKey]) {
              formData.append(`crossRefToTMSA[${index}][${previewKey}]`, crossRefToTMSA[index][previewKey]);
            }
          }
          index++;
        });
      } else if (dataKey === 'analysisData') {
        // const datas = data[dataKey];
        const analysisData = this.analysisFormGroup.value.analysisData;
        let index = 0;
        analysisData.forEach((element: any) => {
          for (const previewKey in element) {
            if (analysisData[index][previewKey]) {
              formData.append(`analysisData[${index}][${previewKey}]`, analysisData[index][previewKey]);
            }
          }
          index++;
        });
      } else if (dataKey === 'action') {
        const datas = this.actionForm.value.action;
        let index = 0;
        datas.forEach((element: any) => {
          for (const previewKey in element) {
            if (previewKey === 'dueDate') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                if (datas[index][previewKey]) {
                  const convertedDate = moment(datas[index][previewKey]).startOf('day');
                  const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
                  formData.append(`action[${index}][${previewKey}]`, dateOfExpiry);
                }
              }
            } else if (previewKey === 'dateCompleted') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                if (datas[index][previewKey]) {
                  const convertedDate = moment(datas[index][previewKey]).startOf('day');
                  const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
                  formData.append(`action[${index}][${previewKey}]`, dateOfExpiry);
                }
              }
            } else if (previewKey === 'status') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                formData.append(`action[${index}][${previewKey}]`, datas[index][previewKey]);
              }
            } else if (previewKey === 'supportiveComments') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                if (datas[index][previewKey] && datas[index][previewKey] !== 'undefined') {
                  formData.append(`action[${index}][${previewKey}]`, datas[index][previewKey]);
                }
              }
            } else if (previewKey === 'email') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                if (datas[index][previewKey]) {
                  const emails = datas[index][previewKey];
                  const email = emails.split(';');
                  let index2 = 0;
                  log.debug('email: ', email);
                  email.forEach((value: any) => {
                    formData.append(`action[${index}][${previewKey}][${index}]`, value);
                    index2++;
                  });
                  // formData.append(`action[${index}][${previewKey}][${0}]`, datas[index][previewKey]);
                }
              }
            } else if (previewKey === 'actionAccepted') {
              if (datas[index][previewKey]) {
                formData.append(`action[${index}][${previewKey}]`, '1');
              }
            } else if (previewKey === 'designationId') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                if (datas[index][previewKey] !== 'Other') {
                  formData.append(`action[${index}][${previewKey}]`, datas[index][previewKey]);
                }
              }
            } else {
              log.debug('datas[index][previewKey]: ', datas[index]);
              if (datas[index][previewKey]) {
                formData.append(`action[${index}][${previewKey}]`, datas[index][previewKey]);
              }
            }
          }
          index++;
        });
      } else if (dataKey === 'attachment') {
      } else if (dataKey === 'notificationAttachments') {
      } else if (dataKey === 'reviewOfVdrDataAttachments') {
      } else if (dataKey === 'postRecoveryMeasuresAttachments') {
      } else if (dataKey === 'seqOfEventAttachments') {
      } else if (dataKey === 'breachAttachments') {
      } else if (dataKey === 'actionAttachments') {
      } else if (dataKey === 'fileSource') {
      } else if (dataKey === 'dateOfIncident') {
        const dateTimeIncident = moment(data[dataKey]).format('YYYY-MM-DD');
        formData.append(dataKey, dateTimeIncident);
      } else if (dataKey === 'timeOfIncident') {
        const key = 'dateOfIncident';
        const timeIncident = moment(data[key]).format('HH:mm:ss');
        const dateTimeIncident = moment(data[key]).format('YYYY-MM-DD');
        formData.append(dataKey, dateTimeIncident + ' ' + timeIncident);
      } else if (dataKey === 'personalInjury') {
        const existFlag = impactData.indexOf('People');
        if (existFlag > -1) {
          if (data[dataKey]) {
            formData.append(`severityOfConsequences[${sequenceCount}]`, data[dataKey]);
            sequenceCount++;
          }
        }
      } else if (dataKey === 'propertyDamage') {
        const existFlag = impactData.indexOf('Assets/Property/Financial');
        if (existFlag > -1) {
          if (data[dataKey]) {
            formData.append(`severityOfConsequences[${sequenceCount}]`, data[dataKey]);
            sequenceCount++;
          }
        }
      } else if (dataKey === 'environment') {
        const existFlag = impactData.indexOf('Environment');
        if (existFlag > -1) {
          if (data[dataKey]) {
            formData.append(`severityOfConsequences[${sequenceCount}]`, data[dataKey]);
            sequenceCount++;
          }
        }
      } else if (dataKey === 'businessimpact') {
        const existFlag = impactData.indexOf('Reputation/Business');
        if (existFlag > -1) {
          if (data[dataKey]) {
            formData.append(`severityOfConsequences[${sequenceCount}]`, data[dataKey]);
            sequenceCount++;
          }
        }
      } else if (dataKey === 'isLatLong') {
        if (this.isLatLong) {
          formData.append(dataKey, '1');
        } else {
          formData.append(dataKey, '0');
        }
      } else if (dataKey === 'portId') {
        if (!this.isLatLong) {
          formData.append(dataKey, data[dataKey]);
        }
      } else if (dataKey === 'latLong') {
        if (this.isLatLong) {
          formData.append(dataKey, latLong);
        }
      } else if (dataKey === 'isPartiallySubmitted') {
        const keyValue = data[dataKey];
        if (keyValue) {
          const newkeyValues = '1';
          formData.append(`isPartiallySubmitted`, newkeyValues);
        } else {
          const newkeyValues = '0';
          formData.append(`isPartiallySubmitted`, newkeyValues);
        }
      } else if (dataKey === 'eventImpact') {
        const impactValue = this.descriptionFormGroup.value.eventImpact;
        let index = 0;
        impactValue.forEach((element: any) => {
          log.debug('element Read', element);
          formData.append(`eventImpact[${index}]`, impactValue[index]);
          index++;
        });
      } else {
        formData.append(dataKey, data[dataKey]);
      }
    }

    this.incidentService.createIncident(formData).subscribe(
      (response) => {
        this.incidentPostResponse = response;
        if (this.incidentPostResponse.statusCode === 201) {
          const message = this.incidentPostResponse.message;
          this.notifyService.showSuccess(message, this.title);
          this.analysisRest();
          this.analysisReset();
          this.descriptionReset();
          this.crossReferenceReset();
          this.actionReset();
          this.overviewReset();
          this.seqDataReset();
          this.reviewVdrReset();
          this.notifyReset();
          this.postRecReset();
          this.breachReset();
          this.investigationReset();
          this.contactEstReset();
          this.backgroundDataFormReset();
          this.router.navigate(['/incident']);
        } else {
          const message = this.incidentPostResponse.message;
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  onSeqFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        this.images.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.seqImagesArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deleteSeqImage(image: any) {
    const index = this.seqImagesArray.indexOf(image);
    this.seqImagesArray.splice(index, 1);
    this.images.splice(index, 1);
    this.onSeqFileChange(this.seqImagesArray);
  }

  onBreachFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        this.breachFile.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.breachFileArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deleteBreachImage(image: any) {
    const index = this.breachFileArray.indexOf(image);
    this.breachFileArray.splice(index, 1);
    this.breachFile.splice(index, 1);
    this.onBreachFileChange(this.breachFileArray);
  }

  onNotifyFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        this.notifyFile.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.notifyFileArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deleteNotifyImage(image: any) {
    const index = this.notifyFileArray.indexOf(image);
    this.notifyFileArray.splice(index, 1);
    this.notifyFile.splice(index, 1);
    this.onNotifyFileChange(this.notifyFileArray);
  }

  onPostRecFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        this.postRecFile.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.postRecFileArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deletePostRecImage(image: any) {
    const index = this.postRecFileArray.indexOf(image);
    this.postRecFileArray.splice(index, 1);
    this.postRecFile.splice(index, 1);
    this.onPostRecFileChange(this.postRecFileArray);
  }

  onActionFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        this.actionFile.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.actionFileArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deleteActionImage(image: any) {
    const index = this.actionFileArray.indexOf(image);
    this.actionFileArray.splice(index, 1);
    this.actionFile.splice(index, 1);
    this.onActionFileChange(this.actionFileArray);
  }

  onBackGroundFileChange(event: any, backgroundIndex: any) {
    log.debug('backgroundIndex', backgroundIndex);
    const fieldName = 'backgroundAttachments' + backgroundIndex;
    const backgroundDataField = 'backgroundData';
    const attachmentFieldName = this.backgroundDataForm.controls[backgroundDataField] as FormArray;
    attachmentFieldName.value[backgroundIndex].attachmentFieldName = fieldName;
    this.addBackgroundField(fieldName);

    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
          fieldName: '',
        };
        log.debug('event.target.files[i]: ', event.target.files[i]);
        log.debug('fieldName: ', fieldName);
        this.backgroundImages.push(event.target.files[i] as File);
        this.backgroundImage[fieldName] = this.backgroundImages;
        log.debug('this.backgroundImage[fieldName]: ', this.backgroundImage[fieldName]);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;
        image.fieldName = fieldName;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.backgroundImageArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deleteBackgroundImage(image: any, backgroundIndex: any) {
    const fieldName = 'backgroundAttachments' + backgroundIndex;
    log.debug('image: ', image);

    const index = this.backgroundImageArray.indexOf(image);
    log.debug('index: ', index);
    this.backgroundImageArray.splice(index, 1);
    const fieldNameIndex = this.backgroundImage[fieldName].indexOf(image);
    this.backgroundImages.splice(index, 1);
    this.backgroundImage[fieldName].splice(fieldNameIndex, 1);
    this.onBackGroundFileChange(this.backgroundImageArray, backgroundIndex);
  }

  addBackgroundField(fieldName: any) {
    this.backgroundDataForm.addControl(fieldName, new FormControl('', Validators.required));
  }

  addReviewVdrField(fieldName: any) {
    this.incidentVdrDataForm.addControl(fieldName, new FormControl('', Validators.required));
  }

  /** Add Sub details Dynamically */
  addSubdetails(): FormGroup {
    return this._formBuilder.group({
      elementId: ['', Validators.required],
      stageId: ['', Validators.required],
      specificKpiId: ['', Validators.required],
      additionalComments: [''],
    });
  }

  addinvestigationForm() {
    return this._formBuilder.group({
      investigatorsId: [''],
      department: [''],
      position: [''],
      reportApprovedById: [''],
      dateOfApprove: [''],
      investigationTeamMember: new FormArray([this.addTeamMemberForm()]),
      reportPreparedBy: new FormArray([this.addReportPreparedByForm()]),
      reportReviewedBy: new FormArray([this.addReportReviewedByForm()]),
    });
  }

  addTeamMemberForm() {
    return this._formBuilder.group({
      isNewTeamMember: [''],
      teamMemberId: [''],
      name: [''],
      department: [''],
      position: ['Team Member'],
      type: [''],
    });
  }

  addinvestigationNewTeamForm() {
    return this._formBuilder.group({
      investigatorsId: [''],
      department: [''],
      position: [''],
      reportApprovedById: [''],
      dateOfApprove: [''],
      newInvestigationTeamMember: new FormArray([this.addNewTeamMemberForm()]),
      investigationTeamMember: new FormArray([]),
      reportPreparedBy: new FormArray([this.addReportPreparedByForm()]),
      reportReviewedBy: new FormArray([this.addReportReviewedByForm()]),
    });
  }

  addNewTeamMemberForm() {
    return this._formBuilder.group({
      name: [''],
      department: [''],
      position: ['Team Member'],
      type: [''],
    });
  }

  addReportPreparedByForm() {
    return this._formBuilder.group({
      reportPreparedById: [''],
      department: [''],
      position: [''],
    });
  }

  addReportReviewedByForm() {
    return this._formBuilder.group({
      reportReviewedById: [''],
      position: [''],
      dateOfReview: [''],
    });
  }

  addReportApprovedByForm() {
    return this._formBuilder.group({
      reportApprovedById: [''],
      position: [''],
      dateOfApprove: [''],
    });
  }

  /**
   * contact established form build
   */

  addIntContactForm() {
    return this._formBuilder.group({
      position: [''],
      internalContactId: [''],
    });
  }

  addExtContactForm() {
    return this._formBuilder.group({
      companyId: [''],
      name: [''],
      designation: [''],
      // location: [''],
    });
  }

  onAddExtContact() {
    this.subDetails = this.contactEstablishForm.get(`extContact`) as FormArray;
    this.subDetails.push(this.addExtContactForm());
  }

  /**
   * Begin:: Sequence of event form Building
   */

  addincidentSeqForm() {
    return this._formBuilder.group({
      date: [''],
      seqData: new FormArray([this.addSeqDataForm()]),
    });
  }

  addSeqDataForm() {
    return this._formBuilder.group({
      ltTime: ['', [Validators.required, Validators.maxLength(4), , Validators.minLength(4)]],
      description: [''],
      triggeringFactors: [''],
    });
  }

  /**
   * Begin:: breach Data form Building
   */

  addBreachForm() {
    return this._formBuilder.group({
      breachAdditionalComments: [''],
      breachReqData: new FormArray([this.addBreachDataForm()]),
    });
  }

  addBreachDataForm() {
    return this._formBuilder.group({
      documents: [''],
      refernce: [''],
      description: [''],
    });
  }

  /**
   * Begin:: incidentVdrData Form Field
   */

  addIncidentVdrForm() {
    return this._formBuilder.group({
      additionalComments: [''],
      reviewVdrDate: new FormArray([this.addReviewVdr()]),
    });
  }

  addReviewVdr() {
    return this._formBuilder.group({
      date: [''],
      reviewVdrData: new FormArray([this.addReviewVdrData()]),
    });
  }

  addReviewVdrData() {
    return this._formBuilder.group({
      ltTime: ['', [Validators.required, Validators.maxLength(4), , Validators.minLength(4)]],
      description: [''],
      vdrAttachmentFieldName: [''],
    });
  }

  /**
   * End:: incidentVdrData Form Field
   * Begin:: postRecoveryData Form Field
   */

  addpostRecDataForm() {
    return this._formBuilder.group({
      postRecAdditionalComments: [''],
      postRecoveryDate: new FormArray([this.addPostRecovery()]),
    });
  }

  addPostRecovery() {
    return this._formBuilder.group({
      date: [''],
      postRecoveryData: new FormArray([this.addPostRecoveryData()]),
    });
  }

  addPostRecoveryData() {
    return this._formBuilder.group({
      ltTime: ['', [Validators.required, Validators.maxLength(4), , Validators.minLength(4)]],
      description: [''],
    });
  }

  /**
   * End:: PostRecoveryData Form Field
   * Begin:: Notification Form Field
   */

  addNotifyDataForm() {
    return this._formBuilder.group({
      notifyAdditionalComments: [''],
      notificationDate: new FormArray([this.addNotify()]),
    });
  }

  addNotify() {
    return this._formBuilder.group({
      date: [''],
      notificationData: new FormArray([this.addNotifyData()]),
    });
  }

  addNotifyData() {
    return this._formBuilder.group({
      ltTime: ['', [Validators.required, Validators.maxLength(4), , Validators.minLength(4)]],
      notificationParty: [''],
      methodUsed: [''],
    });
  }

  /**
   * End:: incidentVdrData Form Field
   * End:: backgroud Form Field
   */

  addBackgroundDataForm() {
    return this._formBuilder.group({
      // newBackground: [''],
      backgroundCondition: [''],
      backgroundConditionId: [''],
      description: [''],
      isNew: ['0'],
      isContibutingFactor: [''],
      attachmentFieldName: [''],
    });
  }

  addInfoInvolving(): FormGroup {
    return this._formBuilder.group({
      categoryId: [''],
      injuryOccuredId: [''],
      rankId: [''],
      age: [''],
      bodyPartAffId: [''],
      nationalityId: [''],
      timeOnBoard: [''],
      timeInRank: [''],
      timeInCompany: [''],
    });
  }

  addlInfoDamage() {
    return this._formBuilder.group({
      isOther: [false],
      newEquipment: [''],
      equipmentId: [''],
      equipmentCategoryId: [''],
      make: [''],
      model: [''],
    });
  }

  onAddTypeOfContactOrEventField() {
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    this.subDetails = this.analysisFormGroup.get(typeOfContactOrEvent) as FormArray;
    this.subDetails.push(this.addContactEventFields());
  }

  onAddBasicImmidateCauseField(index: any, basicImmediateIndex: any) {
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    this.subDetails = this.analysisFormGroup.controls[typeOfContactOrEvent].value[index].basicImmediateCause;
    // log.debug('subDetails: ', this.subDetails);
    this.subDetails.push(this.addBasicImmediate());
  }

  onAddBasicRootCauseField(index: any, basicImmediateIndex: any) {
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    this.subDetails = this.analysisFormGroup.controls[typeOfContactOrEvent].value[index].basicImmediateCause[
      basicImmediateIndex
    ].basicRootCause;
    this.subDetails.push(this.addRootCauseRow());
  }

  addBasicImmediate() {
    return { basicImmediateCauseId: '', immediateCauseExplanation: '', basicRootCause: [this.addRootCauseRow()] };
  }

  addRootCauseRow() {
    return { basicRootCauseId: '', rootCauseExplanation: '', correctiveActionId: Array() };
  }

  addContactEventFields(): FormGroup {
    return this._formBuilder.group({
      typeOfCntctId: [''],
      contact: [''],
      typeOfContactCategory: [''],
      basicImmediateCause: new FormArray([this.addBasicImmidateCause()]),
    });
  }

  addprobablityReccData(): FormGroup {
    return this._formBuilder.group({
      probablityResultantRisk: [this.impact],
      probablityOfRecurrence: [''],
    });
  }

  addBasicImmidateCause(): FormGroup {
    return this._formBuilder.group({
      basicImmediateCauseId: [''],
      immediateCauseExplanation: [''],
      furtherExplanation: [''],
      basicRootCause: new FormArray([this.addRootCause()]),
    });
  }

  addRootCause(): FormGroup {
    return this._formBuilder.group({
      basicRootCauseId: [''],
      rootCauseExplanation: [''],
      furtherExplanation: [''],
      correctiveActionId: new FormArray([]),
    });
  }

  actionFields(): FormGroup {
    return this._formBuilder.group({
      proposedBy: [''],
      type: ['', Validators.required],
      natureOfAction: [''],
      dueDate: [''],
      designationId: [''],
      email: [''],
      dateCompleted: [''],
      actionAccepted: [''],
      supportiveComments: [''],
      status: [''],
    });
  }

  getBackTypeofContact() {
    this.typeContactView = !this.typeContactView;
    if (!this.typeContactView) {
      this.contactOrEventOptionActive = [];
      this.basicImmidateOptionActive = [];
      this.rootCauseOptionActive = [];
      this.correctiveCauseOptionActive = [];
      this.typeOfContactArray = [];
      this.basicCauseArray = [];
      this.rootCauseArray = [];
      const form = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
      form.clear();
      setTimeout(() => {
        // this.stepper.next();
      }, 1);
    }
  }

  /**
   * Begin:: Type of Contact
   * Set Active Class to ContactorEvent List
   */
  getContactClass(contactEvent: any) {
    if (this.typeOfContactArray.indexOf(contactEvent) > -1) {
      return 'contact-table-incomplete';
    } else if (this.contactOrEventOptionActive.indexOf(contactEvent) !== -1) {
      return 'contact-table-active';
    }
  }

  /**
   * Set Active Class to ImmediateCause List
   */
  getImmidiateClass(basicImmediateCause: any, idx: any) {
    const basicImmediateCauseId = basicImmediateCause.immediateCauseId;
    const immediateCause = basicImmediateCause.immediateCause;
    const immediateId = basicImmediateCauseId + '_' + idx;
    if (this.basicCauseArray.indexOf(immediateCause) > -1) {
      return 'contact-table-incomplete';
    } else if (this.basicImmidateOptionActive[immediateId]) {
      return 'contact-table-active';
    }
  }

  /**
   * Set Active Class to RootCause List
   */
  getRootClass(rootCause: any, basicIndex: any, idx: any) {
    const rootCauseId = rootCause.basicRootCauseId;
    const basicRootCause = rootCause.basicRootCause;
    if (this.rootCauseArray.indexOf(basicRootCause) > -1) {
      return 'contact-table-incomplete';
    } else if (this.rootCauseOptionActive[rootCauseId + '_' + idx + '_' + basicIndex]) {
      return 'contact-table-active';
    }
  }

  /**
   * Set Active Class to Corrective List
   */
  getCorrectiveClass(correctiveCauseId: any, rootIndex: any, basicIndex: any, idx: any) {
    if (
      this.correctiveCauseOptionActive.indexOf(correctiveCauseId + '_' + idx + '_' + basicIndex + '_' + rootIndex) !==
      -1
    ) {
      return 'contact-table-active';
    }
  }

  /**
   * set Contact or event Cause Active
   */
  setActivateContactCause(contactTitle: any) {
    this.activeContactCause = contactTitle;
  }

  /**
   * set Immediate Cause as Active
   */
  setActivateImmediateCauses(basicImmediate: any, index: any) {
    this.activeImmediateCause = basicImmediate + '_' + index;
  }

  /**
   * set Immediate Cause as Active
   */
  setActivateRootCauses(basicRootCuaseId: any, basicIndex: any, index: any) {
    this.activeRootCause = basicRootCuaseId + '_' + index + '_' + basicIndex;
  }

  getBasicImmediateCause(contactEventValue: any, index: any) {
    const contactEvent = contactEventValue.typeOfContact;
    this.isRootCause = false;
    this.isCorrectiveCause = false;

    if (this.contactOrEventOptionActive.indexOf(contactEvent) !== -1) {
      const contactIndex = this.contactOrEventOptionActive.indexOf(contactEvent);
      this.contactOrEventOptionActive.splice(contactIndex, 1);
      if (this.basicImmidateOptionActive[index]) {
        this.basicImmidateOptionActive[index].splice(contactIndex, 1);
      }

      delete this.contactOrEventOptionActive[contactEvent];
      this.analysisFormData = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
      let typeOfContactIndex = 0;
      this.analysisFormData.value.forEach((element: any) => {
        const contact = element.contact;
        if (contact === contactEvent) {
          this.analysisFormData.removeAt(typeOfContactIndex);
          this.typeOfContactRemove(contact);
        }
        typeOfContactIndex++;
      });
      this.causeAnalysisValidate();
    } else {
      this.setActivateContactCause(contactEventValue.typeOfContact);
      this.contactOrEventOptionActive.push(contactEvent);
      const contactIndex = this.contactOrEventOptionActive.indexOf(contactEvent);
      this.analysisFormData = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
      if (contactIndex >= 1) {
        this.onAddTypeOfContactOrEventField();
      }
      if (contactIndex === 0) {
        this.onAddTypeOfContactOrEventField();
      }
      this.contactOrEventOptionList.forEach((element: any) => {
        const contact = element.typeOfContact;
        if (contact === contactEvent) {
          if (this.analysisFormData.value instanceof Array) {
            this.analysisFormGroup
              .get(`typeOfContactOrEvent.${contactIndex}.typeOfCntctId`)
              .patchValue(element.typeOfContactId);

            this.analysisFormGroup
              .get(`typeOfContactOrEvent.${contactIndex}.contact`)
              .patchValue(element.typeOfContact);

            this.analysisFormGroup
              .get(`typeOfContactOrEvent.${contactIndex}.typeOfContactCategory`)
              .patchValue(element.typeOfContactCategory);

            this.getNearMissImmidateCause(element.typeOfContactId, index, contactIndex);
          } else {
            this.analysisFormData.value.typeOfCntctId = element.typeOfContactId;
            this.analysisFormData.value.contact = element.typeOfContact;
            this.getNearMissImmidateCause(element.typeOfContactId, index, contactIndex);
          }
        }
      });
    }
    this.causeAnalysisValidate();
  }

  inFormArray(contactIndex: any): FormArray {
    return this.analysisFormGroup.get(`typeOfContactOrEvent.${contactIndex}.basicImmediateCause`) as FormArray;
  }

  getRootCause(basicImmediate: any, contactActiveType: any, basicIndex: any, index: any) {
    const basicImmediateCauseId = basicImmediate.immediateCauseId;
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    this.isRootCause = true;
    this.isCorrectiveCause = false;
    const contactIndex = this.contactOrEventOptionActive.indexOf(contactActiveType.typeOfContact);
    log.debug('getRootCause: ');

    if (this.basicImmidateOptionActive[basicImmediateCauseId + '_' + index]) {
      delete this.basicImmidateOptionActive[basicImmediateCauseId + '_' + index];
      const analysisFormData = this.inFormArray(contactIndex);

      if (analysisFormData) {
        let immediateCauseIndex = 0;
        analysisFormData.value.forEach((analysisData: any) => {
          const immediateCause = analysisData.basicImmediateCauseId;
          if (immediateCause === basicImmediateCauseId) {
            log.debug('analysisData.basicImmediateCauseId: ', analysisData.basicImmediateCauseId);
            this.immediateCauseRemove(analysisData.immediateCauseExplanation);
            analysisFormData.removeAt(immediateCauseIndex);
          }
          immediateCauseIndex++;
        });
      }
      this.causeAnalysisValidate();
    } else {
      const immediateId = basicImmediateCauseId + '_' + index;
      this.basicImmidateOptionActive[immediateId] = basicImmediate;
      this.setActivateImmediateCauses(basicImmediateCauseId, index);
      const formArray = this.analysisFormGroup.controls[typeOfContactOrEvent].value[contactIndex]
        .basicImmediateCause as FormArray;

      const basicFormArray = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause`
      ) as FormArray;

      const basicFormValue = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${0}.basicImmediateCauseId`
      ) as FormArray;

      if (formArray[0] == null) {
        this.onAddBasicImmidateCauseField(contactIndex, basicIndex);
      } else if (basicFormValue.value !== '') {
        this.onAddBasicImmidateCauseField(contactIndex, basicIndex);
      }
      this.ImmediateCauseId = basicImmediateCauseId;
      this.basicImmidateOptionList[index].forEach((element: any) => {
        const basicImmediateId = element.immediateCauseId;
        if (basicImmediateId === basicImmediateCauseId) {
          let formArrayIndex = 0;
          if (basicFormArray.value.length > 1) {
            formArrayIndex = basicFormArray.value.length - 1;
          }

          this.analysisFormGroup
            .get(`typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${formArrayIndex}.basicImmediateCauseId`)
            .patchValue(element.immediateCauseId);
          this.analysisFormGroup
            .get(`typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${formArrayIndex}.immediateCauseExplanation`)
            .patchValue(element.immediateCause);

          // formArray[formArrayIndex].basicImmediateCauseId = element.immediateCauseId;
          // formArray[formArrayIndex].immediateCauseExplanation = element.immediateCause;
          this.getNearMissRootCause(basicImmediate, basicIndex, index, element.immediateCauseRelationId);
        }
      });
      this.causeAnalysisValidate();
    }
  }

  getCorrectiveCause(
    contactActiveType: any,
    currentRootCause: any,
    basicImmediate: any,
    basicIndex: any,
    rootCauseIndex: any,
    index: any
  ) {
    const rootCause = currentRootCause.basicRootCause;
    const rootCauseId = currentRootCause.basicRootCauseId;
    const immediateCauseId = basicImmediate.immediateCauseId;

    this.isRootCause = true;
    this.isCorrectiveCause = true;
    const indexing = rootCauseId + '_' + index + '_' + basicIndex;

    const contactIndex = this.contactOrEventOptionActive.indexOf(contactActiveType.typeOfContact);

    if (this.rootCauseOptionActive[indexing]) {
      delete this.rootCauseOptionActive[indexing];
      // const basicImmediateData = this.analysisFormGroup.controls[typeOfContactOrEvent].value[contactIndex]
      //   .basicImmediateCause as FormArray;

      const basicImmediateData = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause`
      ) as FormArray;

      if (basicImmediateData) {
        let immediateFormIndex = 0;

        let basicRootIndex = 0;
        basicImmediateData.value.forEach((elements: any) => {
          const immediateId = elements.immediateCauseId;
          if (immediateId === immediateCauseId) {
            immediateFormIndex = basicRootIndex;
          }
          basicRootIndex++;
        });

        this.activeRootCause = '';
        const formArray = this.analysisFormGroup.get(
          `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause`
        ) as FormArray;

        let rootIndex = 0;
        formArray.value.forEach((element: any) => {
          if (element.rootCauseExplanation === rootCause) {
            formArray.removeAt(rootIndex);
            this.rootCauseRemove(rootCause);
          }
          rootIndex++;
        });
        this.causeAnalysisValidate();
      }
    } else {
      this.setActivateRootCauses(rootCauseId, basicIndex, index);
      this.rootCauseOptionActive[indexing] = currentRootCause;
      const basicImmediateData = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause`
      ) as FormArray;
      let immediateFormIndex = 0;

      let basicCauseIndex = 0;
      basicImmediateData.value.forEach((element: any) => {
        const immediateId = element.basicImmediateCauseId;
        if (immediateId === immediateCauseId) {
          immediateFormIndex = basicCauseIndex;
        }
        basicCauseIndex++;
      });
      log.debug('immediateFormIndex: ', immediateFormIndex);

      const basicRootCause = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause`
      ) as FormArray;

      const basicRootCauseValue = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${0}.basicRootCauseId`
      ) as FormArray;

      if (basicRootCause.value.length < 1) {
        this.onAddBasicRootCauseField(contactIndex, immediateFormIndex);
      } else if (basicRootCauseValue.value !== '') {
        this.onAddBasicRootCauseField(contactIndex, immediateFormIndex);
      }

      this.rootCauseOptionList[immediateCauseId].forEach((element: any) => {
        if (element) {
          const basicRootCauseId = element.basicRootCauseId;
          if (basicRootCauseId === rootCauseId) {
            let formArrayIndex = 0;

            if (basicRootCause.value.length > 1) {
              formArrayIndex = basicRootCause.value.length - 1;
            }
            this.analysisFormGroup
              .get(
                `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${0}.basicRootCauseId`
              )
              .patchValue(element.basicRootCauseId);
            // formArray[formArrayIndex].basicRootCauseId = element.basicRootCauseId;
            this.analysisFormGroup
              .get(
                `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${0}.rootCauseExplanation`
              )
              .patchValue(element.basicRootCause);
            // formArray[formArrayIndex].rootCauseExplanation = element.basicRootCause;
            this.getNearMissCorrectiveCause(contactActiveType, basicImmediate, element, formArrayIndex, index);
          }
        }
      });
      this.causeAnalysisValidate();
    }
  }

  setCorrectiveCause(
    contactActiveType: any,
    basicImmidate: any,
    rootCauseData: any,
    correctiveData: any,
    rootIndex: any,
    basicIndex: any,
    idx: any
  ) {
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    const correctiveIndexing = correctiveData.correctiveActionId + '_' + idx + '_' + basicIndex + '_' + rootIndex;
    const immediateCauseId = basicImmidate.immediateCauseId;
    const rootCauseId = rootCauseData.basicRootCauseId;
    const correctiveActionId = correctiveData.correctiveActionId;
    const contactTypeIndex = this.contactOrEventOptionActive.indexOf(contactActiveType.typeOfContact);

    if (this.correctiveCauseOptionActive.indexOf(correctiveIndexing) !== -1) {
      this.correctiveActionId = '';
      const contactIndex = this.correctiveCauseOptionActive.indexOf(correctiveIndexing);
      this.correctiveCauseOptionActive.splice(contactIndex, 1);

      const basicImmediateData = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactTypeIndex}.basicImmediateCause`
      ) as FormArray;

      if (basicImmediateData) {
        let immediateFormIndex = 0;
        let basicRootCauseId = 0;
        let correctiveId = 0;
        let bIndex = 0;
        basicImmediateData.value.forEach((element: any) => {
          const immediateId = element.basicImmediateCauseId;
          if (immediateId === immediateCauseId) {
            immediateFormIndex = bIndex;
            let rIndex = 0;
            const basicRootCause = element.basicRootCause;
            basicRootCause.forEach((rootElmenet: any) => {
              const rootId = rootElmenet.basicRootCauseId;
              if (rootCauseId === rootId) {
                basicRootCauseId = rIndex;
                const basicCorrectiveCause = rootElmenet.correctiveActionId;
                for (let x = 0; x < basicCorrectiveCause.length; x++) {
                  if (correctiveActionId === basicCorrectiveCause[x]) {
                    correctiveId = x;
                  }
                }
              }
              rIndex++;
            });
          }
          bIndex++;
        });

        const formArray = this.analysisFormGroup.get(
          `typeOfContactOrEvent.${contactTypeIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${basicRootCauseId}.correctiveActionId`
        ) as FormArray;
        formArray.removeAt(correctiveId);
        this.causeAnalysisValidate();
      }
    } else {
      this.correctiveActionId = correctiveIndexing;
      this.correctiveCauseOptionActive.push(this.correctiveActionId);

      const basicImmediateData = this.analysisFormGroup.controls[typeOfContactOrEvent].value[contactTypeIndex]
        .basicImmediateCause as FormArray;
      let immediateFormIndex = 0;
      let basicRootCauseId = 0;
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < basicImmediateData.length; i++) {
        const immediateId = basicImmediateData[i].basicImmediateCauseId;
        if (immediateId === immediateCauseId) {
          immediateFormIndex = i;
          const basicRootCause = basicImmediateData[i].basicRootCause;
          // tslint:disable-next-line:prefer-for-of
          for (let j = 0; j < basicRootCause.length; j++) {
            const rootId = basicRootCause[j].basicRootCauseId;
            if (rootCauseId === rootId) {
              basicRootCauseId = j;
            }
          }
        }
      }

      const formArray = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactTypeIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${basicRootCauseId}.correctiveActionId`
      ) as FormArray;
      formArray.push(new FormControl(correctiveData.correctiveActionId));
    }
    this.causeAnalysisValidate();
  }

  getNearMissImmidateCause(typeOfContactId: any, index: any, contactIndex: any) {
    if (this.cacheServiceService.basicImmediateCause) {
      this.filterImmediateCause(index, contactIndex);
    } else {
      this.nearMissService.getBasicImmediate().subscribe(
        (response) => {
          this.contactList = response;
          // log.debug(`${JSON.stringify(response)}`);
          this.cacheServiceService.setBasicImmediateCause(response);
          this.filterImmediateCause(index, contactIndex);
          if (response != null) {
            this.contactOrEventOptionList[index].immidiateCauses = response;
            this.basicImmidateOptionList[index] = response;
          } else {
            const message = 'basic Immidate list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  getNearMissRootCause(basicImmediateCause: any, basicIndex: any, typeOfContactIndex: any, immidiateCauseIndex: any) {
    if (this.cacheServiceService.basicRootCause) {
      this.filterRootCause(basicImmediateCause, typeOfContactIndex, immidiateCauseIndex);
    } else {
      this.nearMissService.getRootCause().subscribe(
        (response) => {
          this.contactList = response;
          // log.debug(`${JSON.stringify(response)}`);
          if (response) {
            this.cacheServiceService.setBasicRootCause(response);
            this.filterRootCause(basicImmediateCause, typeOfContactIndex, immidiateCauseIndex);
          } else {
            const message = 'RootCause list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  getNearMissCorrectiveCause(
    typeOfContact: any,
    basicImmediate: any,
    basicRootCause: any,
    basicRootCauseIndex: any,
    contactIndex: any
  ) {
    if (this.cacheServiceService.basicCorrectiveCause) {
      this.filterCorrectiveCause(typeOfContact, basicImmediate, basicRootCause, contactIndex);
    } else {
      this.nearMissService.getCorrectiveCause().subscribe(
        (response) => {
          this.contactList = response;
          // log.debug(`${JSON.stringify(response)}`);
          if (response) {
            this.cacheServiceService.setBasicCorrectiveCause(response);
            this.filterCorrectiveCause(typeOfContact, basicImmediate, basicRootCause, contactIndex);
            // this.correctiveCauseOptionList[basicRootCauseId] = response;
          } else {
            const message = 'RootCause list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  getNearMissCauseTree() {
    if (this.cacheServiceService.causeTree) {
      this.causeTree = this.cacheServiceService.causeTree;
    } else {
      this.nearMissService.getCauseTree().subscribe(
        (response) => {
          this.causeTree = response;
          if (response) {
            // this.correctiveCauseOptionList = response;
            this.cacheServiceService.setCauseTree(response);
          } else {
            const message = 'RootCause list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  filterImmediateCause(typeOfContactIndex: any, contactIndex: any) {
    const typeData = this.cacheServiceService.basicImmediateCause;
    const causeTree = this.cacheServiceService.causeTree;
    this.filterBasicImmediateCause = [];
    causeTree.forEach((element: any) => {
      if (element.typeOfContactId === typeOfContactIndex) {
        const immediateArray = element.immediateCause;
        immediateArray.forEach((element2: any) => {
          const immediateIndex = element2.basicImmediateCauseId - 1;
          if (typeData[immediateIndex]) {
            this.filterBasicImmediateCause.push(typeData[immediateIndex]);
          }
        });
      }
    });
    this.basicImmidateOptionList[typeOfContactIndex] = this.filterBasicImmediateCause;
  }

  filterRootCause(basicImmediateCause: any, typeOfContactIndex: any, immidiateCauseIndex: any) {
    const rootData = this.cacheServiceService.basicRootCause;
    const causeTree = this.cacheServiceService.causeTree;
    this.filterBasicRootCause = [];
    causeTree.forEach((element: any) => {
      if (element.typeOfContactId === typeOfContactIndex) {
        const immediateArray = element.immediateCause;
        immediateArray.forEach((element2: any) => {
          if (immidiateCauseIndex === element2.basicImmediateCauseId) {
            const rootArray = element2.basicRootCause;
            rootArray.forEach((element3: any) => {
              const rootIndex = element3.basicRootCauseId - 1;
              if (rootData[rootIndex]) {
                this.filterBasicRootCause.push(rootData[rootIndex]);
              }
            });
          }
        });
      }
    });
    this.rootCauseOptionList[basicImmediateCause.immediateCauseId] = this.filterBasicRootCause;
  }

  filterCorrectiveCause(typeOfContact: any, basicImmediateCause: any, rootCause: any, typeOfContactIndex: any) {
    const causeTree = this.cacheServiceService.causeTree;
    const basicCorrectiveCause = this.cacheServiceService.basicCorrectiveCause;
    this.filterBasicCorrectiveCause = [];
    causeTree.forEach((element: any) => {
      if (element.typeOfContactId === typeOfContactIndex) {
        const immediateArray = element.immediateCause;
        immediateArray.forEach((element2: any) => {
          if (basicImmediateCause.immediateCauseRelationId === element2.basicImmediateCauseId) {
            const rootArray = element2.basicRootCause;
            rootArray.forEach((element3: any) => {
              if (element3.basicRootCauseId === rootCause.basicRootCauseRelationId) {
                const correctiveArray = element3.correctiveAction;
                correctiveArray.forEach((element4: any) => {
                  if (basicCorrectiveCause[element4]) {
                    this.filterBasicCorrectiveCause.push(basicCorrectiveCause[element4]);
                  }
                });
              }
            });
          }
        });
      }
    });
    this.correctiveCauseOptionList[rootCause.basicRootCauseId] = this.filterBasicCorrectiveCause;
  }

  onFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        this.allFiles.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.images.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  filterStages(event: any) {
    const elementId = event.value;
    let relationalId = '';
    this.elementData = this.incidentCacheService.elementData;

    this.elementData.forEach((element: any) => {
      if (element.elementId === elementId) {
        relationalId = element.elementRelationId;
      }
    });
    this.relationalData = this.incidentCacheService.crossReferenceRelationData;
    this.relationalData.forEach((elements: any) => {
      if (elements[0].elementId === relationalId) {
        this.relationalId = elements[0].stageId;
      }
    });

    this.stageData = [];
    this.incidentCacheService.stageData.forEach((element: any) => {
      const stageId = element.stageRelationId;
      if (this.relationalId && this.relationalId.indexOf(stageId) > -1) {
        this.stageData.push(element);
      }
    });

    this.tmsaFormOptions.formState.stageData = {
      ...this.tmsaFormOptions.formState.stageData,
      [elementId]: this.stageData,
    };
  }

  filterSpecificKPI(event: any) {
    const stageId = event.value;
    this.spacificData = [];

    this.incidentCacheService.stageData.forEach((element: any) => {
      const stage = element.stageId;
      if (stageId === stage) {
        this.spacificData.push({
          specificKpi: element.specificKpi,
          stageKpiId: element.stageId,
          stageNo: element.stageNo,
          stageRelationId: element.stageRelationId,
        });
      }
    });

    this.tmsaFormOptions.formState.specificData = {
      ...this.tmsaFormOptions.formState.specificData,
      [stageId]: this.spacificData,
    };
  }

  causeAnalysisValidate() {
    const datas = this.analysisFormGroup.value.typeOfContactOrEvent;
    let index = 0;
    const contact = 'contact';
    const immediateCauseExplanation = 'immediateCauseExplanation';
    const rootCauseExplanation = 'rootCauseExplanation';
    datas.forEach((element: any) => {
      if (element.typeOfCntctId) {
        for (const previewKey in element) {
          if (element.basicImmediateCause.length > 0) {
            if (previewKey === 'basicImmediateCause') {
              const basicImmediate = datas[index][previewKey];
              if (basicImmediate) {
                let index2 = 0;
                basicImmediate.forEach((elements: any) => {
                  const basicImmediateCauseId = elements.basicImmediateCauseId;
                  if (basicImmediateCauseId) {
                    const contactType = datas[index][contact];
                    const typeIndex = this.typeOfContactArray.indexOf(contactType);
                    if (typeIndex > -1) {
                      this.typeOfContactArray.splice(typeIndex, 1);
                    }
                    for (const basicCause in elements) {
                      if (datas[index][previewKey][index2][basicCause]) {
                        if (basicCause === 'basicRootCause') {
                          const basicRootCause = datas[index][previewKey][index2][basicCause];
                          let index3 = 0;
                          if (basicRootCause.length > 0) {
                            basicRootCause.forEach((element3: any) => {
                              const basicRootCauseId = element3.basicRootCauseId;
                              if (basicRootCauseId) {
                                const causeExplanation = datas[index][previewKey][index2][immediateCauseExplanation];
                                const basicIndex = this.basicCauseArray.indexOf(causeExplanation);
                                if (basicIndex > -1) {
                                  this.basicCauseArray.splice(basicIndex, 1);
                                }
                                if (element3.correctiveActionId.length > 0) {
                                  const explanation = basicRootCause[index3][rootCauseExplanation];
                                  const rootIndex = this.rootCauseArray.indexOf(explanation);
                                  if (rootIndex > -1) {
                                    this.rootCauseArray.splice(rootIndex, 1);
                                  }
                                } else {
                                  const explanation = basicRootCause[index3][rootCauseExplanation];
                                  if (this.rootCauseArray.indexOf(explanation) < 0) {
                                    this.rootCauseArray.push(explanation);
                                  }
                                }
                              } else {
                                const causeExplanation = datas[index][previewKey][index2][immediateCauseExplanation];
                                if (this.basicCauseArray.indexOf(causeExplanation) < 0) {
                                  this.basicCauseArray.push(causeExplanation);
                                }
                              }
                              index3++;
                            });
                          } else {
                            const causeExplanation = datas[index][previewKey][index2][immediateCauseExplanation];
                            if (this.basicCauseArray.indexOf(causeExplanation) < 0) {
                              this.basicCauseArray.push(causeExplanation);
                            }
                          }
                        }
                      }
                    }
                  } else {
                    const contactType = datas[index][contact];
                    if (this.typeOfContactArray.indexOf(contactType) < 0) {
                      this.typeOfContactArray.push(contactType);
                    }
                  }
                  index2++;
                });
              } else {
              }
            }
          } else {
            const contactType = datas[index][contact];
            if (this.typeOfContactArray.indexOf(contactType) < 0) {
              this.typeOfContactArray.push(contactType);
            }
          }
        }
      }
      index++;
    });
  }

  typeOfContactRemove(type: any) {
    const typeIndex = this.typeOfContactArray.indexOf(type);
    if (typeIndex > -1) {
      this.typeOfContactArray.splice(typeIndex, 1);
    }
  }

  immediateCauseRemove(type: any) {
    const typeIndex = this.basicCauseArray.indexOf(type);
    if (typeIndex > -1) {
      this.basicCauseArray.splice(typeIndex, 1);
    }
  }

  rootCauseRemove(type: any) {
    const typeIndex = this.rootCauseArray.indexOf(type);
    if (typeIndex > -1) {
      this.rootCauseArray.splice(typeIndex, 1);
    }
  }

  // Get Task Involved list data
  getNearMissTastList() {
    this.nearMissService.getTaskList().subscribe(
      (response) => {
        this.contactList = response;
        // log.debug(`${JSON.stringify(response)}`);
        if (response) {
          this.taskList = response;
        } else {
          const message = 'Task list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Location list data
  getNearMissLocationList() {
    this.nearMissService.getLocationList().subscribe(
      (response) => {
        if (response) {
          this.locationList = response;
        } else {
          const message = 'Task list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Vessel list data
  getCrossReferenceTMSA() {
    this.incidentService.getCrossReferenceTMSA().subscribe(
      (response) => {
        if (response) {
          this.incidentCacheService.setCrossReferenceRelationData(response);
        } else {
          const message = 'cross reference relation not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Stage Data list data
  getStageData() {
    this.incidentService.getStage().subscribe(
      (response) => {
        if (response) {
          // this.stageData = response[0];
          this.incidentCacheService.setStageData(response[0]);
        } else {
          const message = 'stage not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  addAnalysisData(): FormGroup {
    return this._formBuilder.group({
      questionId: [''],
      numberOfTimesRepeated: [''],
      briefDescription: [''],
    });
  }

  private createOverviewForm() {
    this.overViewFormGroup = this._formBuilder.group({
      vesselId: ['', Validators.required],
      portId: [''],
      latLong: [''],
      latitude: [''],
      longitude: [''],
      isLatLong: [''],
      userId: [''],
      incidentTitle: ['', Validators.required],
      vesselLocation: ['', Validators.required],
      dateOfIncident: ['', Validators.required],
      timeOfIncident: [''],
      briefDescription: ['', Validators.required],
      description: ['', Validators.required],
      taskId: ['', Validators.required],
      locationId: ['', Validators.required],
      isPartiallySubmitted: [''],
    });
  }

  private createDescriptionForm() {
    this.descriptionFormGroup = this._formBuilder.group({
      eventId: [''],
      // impact: [this.impactValue],
      eventImpact: new FormArray([]),
      severityLevel: [''],
      personalInjury: [''],
      propertyDamage: [''],
      environment: [''],
      businessimpact: [''],
      additionalComments: [''],
      probablityReccData: this.addprobablityReccData(),
      resultantRisk: [this.impact],
      severityAdditionalComments: [''],
      addlInfoInvolvingInjury: new FormArray([this.addInfoInvolving()]),
      addlInfoInvolvingEqpDamage: new FormArray([this.addlInfoDamage()]),
      isAddlInfoInvolvingEqpDamage: [''],
      isAddlInfoInvolvingInjury: [''],
      isIncidentVdrData: [''],
      // isIncidentVdrData: false
      // severityLevelImpact: this.addseverityLevelImpact()
    });
  }

  private createAnalysisForm() {
    this.analysisFormGroup = this._formBuilder.group({
      severityLevel: [''],
      // probablityReccData: this.addprobablityReccData(),
      typeOfContactOrEvent: new FormArray([this.addContactEventFields()]),
      analysisData: new FormArray([this.addAnalysisData()]),
    });
  }

  private investigationForm() {
    this.investigationTeamForm = this._formBuilder.group({
      investigationTeam: this.addinvestigationForm(),
    });
  }

  private contactEstForm() {
    this.contactEstablishForm = this._formBuilder.group({
      intContact: new FormArray([this.addIntContactForm()]),
      extContact: new FormArray([this.addExtContactForm()]),
    });
  }

  private incidentSeqForm() {
    this.incidentSeqDataForm = this._formBuilder.group({
      incidentSeqData: new FormArray([this.addincidentSeqForm()]),
      seqOfEventAttachments: [''],
    });
  }

  private incidentVdrForm() {
    this.incidentVdrDataForm = this._formBuilder.group({
      incidentVdrData: this.addIncidentVdrForm(),
      reviewOfVdrDataAttachments: [''],
      isIncidentVdrData: [''],
    });
  }

  private postRecForm() {
    this.postRecDataForm = this._formBuilder.group({
      postRecData: this.addpostRecDataForm(),
      postRecoveryMeasuresAttachments: [''],
    });
  }

  private notifyForm() {
    this.notifyDataForm = this._formBuilder.group({
      notifyData: this.addNotifyDataForm(),
      notificationAttachments: [''],
    });
  }

  private breachForm() {
    this.breachDataForm = this._formBuilder.group({
      breachData: this.addBreachForm(),
      breachAttachments: [''],
    });
  }

  private backgroundForm() {
    this.backgroundDataForm = this._formBuilder.group({
      backgroundData: new FormArray([this.addBackgroundDataForm()]),
    });
  }

  private createCrossReferencing() {
    this.crossReferenceForm = this._formBuilder.group({
      vesselId: [''],
      portId: [''],
      latLong: [''],
      latitude: [''],
      longitude: [''],
      isLatLong: [''],
      incidentTitle: [''],
      vesselLocation: [''],
      description: ['', Validators.required],
      taskId: ['', Validators.required],
      locationId: ['', Validators.required],
      severityAdditionalComments: [''],
      isAddlInfoInvolvingInjury: [''],
      addlInfoInvolvingInjury: new FormArray([this.addInfoInvolving()]),
      isAddlInfoInvolvingEqpDamage: [''],
      addlInfoInvolvingEqpDamage: new FormArray([this.addlInfoDamage()]),
      userId: ['', Validators.required],
      dateOfIncident: [''],
      timeOfIncident: [''],
      briefDescription: [''],
      isPartiallySubmitted: [false],
      // stopCardIssued: [''],
      // details: [''],
      eventImpact: [''],
      severityLevel: [''],
      resultantRisk: [''],
      probablityReccData: this.addprobablityReccData(),
      typeOfContactOrEvent: new FormArray([this.addContactEventFields()]),
      analysisData: new FormArray([this.addAnalysisData()]),
      crossRefToTMSA: new FormArray([this.addSubdetails()]),
      action: new FormArray([this.actionFields()]),
      actionAttachments: [''],
      attachment: [''],
      fileSource: [''],
      backgroundData: new FormArray([this.addBackgroundDataForm()]),
      breachData: this.addBreachForm(),
      breachAttachments: [''],
      notifyData: this.addNotifyDataForm(),
      notificationAttachments: [''],
      postRecData: this.addpostRecDataForm(),
      postRecoveryMeasuresAttachments: [''],
      isIncidentVdrData: [false],
      incidentVdrData: this.addIncidentVdrForm(),
      reviewOfVdrDataAttachments: [''],
      incidentSeqData: new FormArray([this.addincidentSeqForm()]),
      seqOfEventAttachments: [''],
      investigationTeam: this.addinvestigationNewTeamForm(),
      intContact: new FormArray([this.addIntContactForm()]),
      extContact: new FormArray([this.addExtContactForm()]),
      eventId: [''],
      personalInjury: [''],
      propertyDamage: [''],
    });
  }

  private createAction() {
    this.actionForm = this._formBuilder.group({
      officeComments: [''],
      action: new FormArray([this.actionFields()]),
      actionAttachments: [''],
    });
  }
}
