import { FormlyFieldConfig } from '@formly';
import { CauseTreeIncidentComponent } from '@modules/incident-investigation/share/cause-tree/cause-tree.component';

export default (props: any): FormlyFieldConfig[] => {
  if (props.similarIncident.length === 0) {
    return [];
  }

  return [
    {
      type: 'button',
      key: 'addNewEditCauseTree',
      templateOptions: {
        type: 'button',
        label: 'Add New/Edit',
        color: 'primary',
        onClick: (event: any, field: FormlyFieldConfig) => {
          field.options.formState.causeTree = !field.options.formState.causeTree;
        },
      },
      hideExpression: 'formState.causeTree',
    },
    {
      type: 'template-component',
      className: 'flex-1',
      templateOptions: {
        component: CauseTreeIncidentComponent,
        props: {
          key: 'date',
          label: 'Date',
        },
      },
      hideExpression: '!formState.causeTree',
    },
    {
      template: `<div><h3>SUMMARY OF AVAILABLE INFORMATION</h3><hr class="full-width-form"/></div>`,
      hideExpression: 'formState.causeTree',
    },
    {
      template: `<div><h3>TYPE OF CONTACT OR EVENT</h3><hr class="full-width-form"/></div>`,
      hideExpression: 'formState.causeTree',
    },
    {
      template: `<div><h3>IMMEDIATE CAUSE</h3><hr class="full-width-form"/></div>`,
      hideExpression: 'formState.causeTree',
    },
    {
      template: `<div><h3>ROOT CAUSE</h3><hr class="full-width-form"/></div>`,
      hideExpression: 'formState.causeTree',
    },
    {
      type: 'repeat',
      key: 'analysis',
      fieldGroupClassName: 'display-flex',
      hideExpression: 'formState.causeTree',
      templateOptions: {
        addText: 'ADD ROW +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex mt-15',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'select',
                key: 'questions',
                templateOptions: {
                  placeholder: 'Question',
                  label: 'Question',
                  required: true,
                  valueProp: 'analysisOfSimilarIncidentId',
                  labelProp: 'questions',
                  options: props.similarIncident,
                },
              },
              {
                className: 'flex-1',
                type: 'input',
                key: 'repeat_no_of_times',
                templateOptions: {
                  type: 'number',
                  min: 0,
                  placeholder: 'No. Of Times Repeated',
                  label: 'No. Of Times Repeated',
                  required: true,
                },
              },
              {
                className: 'flex-2',
                type: 'input',
                key: 'brief_description',
                templateOptions: {
                  placeholder: 'Comment',
                  label: 'Brief Description',
                  required: true,
                },
              },
            ],
          },
        ],
      },
    },
  ];
};
