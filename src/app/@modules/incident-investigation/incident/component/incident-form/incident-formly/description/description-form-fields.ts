import { FormlyFieldConfig } from '@formly';
import { IJson } from '@types';
import { SEVERITY_LEVEL_POINTS, LIKELIHOOD_POINTS } from '@modules/incident-investigation/incident/incident.constant';
import { FormlyField } from '@ngx-formly/core';

export default (props: IJson): FormlyFieldConfig[] => {
  if (
    props.eventIncidentItems.length === 0 ||
    props.probability.length === 0 ||
    props.eventImpacts.length === 0 ||
    props.allUsers.length === 0 ||
    props.severityLevel.length === 0
  ) {
    return [];
  }

  return [
    {
      template: `<div><h3>THE EVENT/INCIDENT HAD AN IMPACT ON</h3><hr /></div>`,
    },
    {
      fieldGroupClassName: 'display-flex',
      id: 'eventImpact',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'eventImpact',
          type: 'multicheckbox',
          defaultValue: [],
          templateOptions: {
            required: true,
            options: props.eventImpacts || [],
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'severityLevel',
          type: 'severity-level',
          defaultValue: 'Minor',
          templateOptions: {
            severityLevel: props.severityLevel || [],
          },
          hideExpression: (model: any) => model.eventImpact.length === 0,
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'likelyhood',
          type: 'likelyhood',
          defaultValue: 'Possible',
          templateOptions: {
            impact: 6,
            probability: props.probability || [],
          },
          hideExpression: (model: any) => model.eventImpact.length === 0,
          expressionProperties: {
            'templateOptions.impact': (model: any, formState: any, field: FormlyFieldConfig) => {
              if (model.severityLevel && model.likelyhood) {
                formState.resultantRisk =
                  SEVERITY_LEVEL_POINTS[model.severityLevel] * LIKELIHOOD_POINTS[model.likelyhood];
                return formState.resultantRisk;
              }
              return 0;
            },
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'severityAdditionalComments',
          type: 'textarea',
          templateOptions: {
            rows: 3,
            label: 'Additional Comments',
            placeholder: 'Any additional comments concerning the impact of the incident',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'eventId',
          type: 'matselect',
          className: 'flex-1',
          templateOptions: {
            required: true,
            options: props.eventIncidentItems || [],
            placeholder: 'Select Event/Incident Type',
            label: 'Event/Incident',
            labelProp: 'eventImpactType',
            valueProp: 'eventImpactTypeId',
            onChange: (field: FormlyFieldConfig, event: any, data: any) => {
              if (data) {
                if (!props.formState.isMachineryField) {
                  props.formState.isMachineryField = data.eventImpactTypeId === '7df6098c-5c12-49fb-8974-5e19e54bd111';
                }

                if (!props.formState.isInjuryField) {
                  props.formState.isInjuryField = data.eventImpactTypeId === '706ad736-81d3-47a4-a0a4-ebb8ec95c35e';
                }

                if (props.formState.isMachineryField && props.onMachineryFieldSelected) {
                  props.onMachineryFieldSelected(data, field);
                }

                if (props.formState.isInjuryField && props.onInjuryFieldSelected) {
                  props.onInjuryFieldSelected(data, field);
                }
              }
            },
          },
        },
      ],
    },
    {
      template: `<div><h3>ADDITION INFORMATION INVOLVING AN EQPT./MACHINERY FAILURE OR DAMAGE</h3></div>`,
      hideExpression: '!formState.isMachineryField',
    },
    {
      id: 'machineryFailureField',
      type: 'repeat',
      key: 'addlInfoInvolvingEqpDamage',
      fieldGroupClassName: 'display-flex mt-10',
      templateOptions: {
        addText: 'Add',
        addTextClass: 'add-btn',
        layout: 'modern',
        onDelete: (fieldGroup: FormlyFieldConfig[], model: IJson, formState: IJson) => {
          if (fieldGroup.length === 0) {
            formState.isMachineryField = false;
          }
        },
      },
      hideExpression: '!formState.isMachineryField',
      fieldArray: {
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'matselect',
                key: 'equipmentId',
                templateOptions: {
                  required: true,
                  placeholder: 'Equipment',
                  label: 'Equipment',
                  valueProp: 'equipmentId',
                  labelProp: 'equipment',
                  options: [],
                  onChange: (field: FormlyField, event: any, item: any) => {
                    if (item) {
                      field.model.isOther = item.equipment === 'Others';

                      if (field.model.isOther) {
                        delete field.model.equipmentId;
                      } else {
                        delete field.model.newEquipment;
                      }
                    }
                  },
                },
                expressionProperties: {
                  'templateOptions.options': (model: any, formState: any, field: FormlyFieldConfig) => {
                    return formState.equipmentList || [];
                  },
                },
              },
              {
                className: 'flex-1',
                type: 'input',
                key: 'newEquipment',
                templateOptions: {
                  placeholder: '(If Others, Specify)',
                  label: 'Other',
                },
                expressionProperties: {
                  'templateOptions.required': 'model.isOther',
                },
                hideExpression: '!model.isOther',
              },
              {
                className: 'flex-1',
                type: 'input',
                key: 'make',
                templateOptions: {
                  placeholder: 'Make',
                  label: 'Make',
                },
                hideExpression: 'model.isOther',
              },
            ],
          },
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'matselect',
                key: 'equipmentCategoryId',
                templateOptions: {
                  required: true,
                  placeholder: 'Equipment Category',
                  label: 'Equipment Category',
                  valueProp: 'equipmentCategoryId',
                  labelProp: 'categoryName',
                  options: [],
                },
                expressionProperties: {
                  'templateOptions.options': (model: any, formState: any, field: FormlyFieldConfig) => {
                    return formState.equipmentCategoryList || [];
                  },
                },
              },
              {
                className: 'flex-1',
                type: 'input',
                key: 'model',
                templateOptions: {
                  placeholder: 'Model',
                  label: 'Model',
                },
                hideExpression: 'model.isOther',
              },
            ],
          },
        ],
      },
    },
    {
      template: `<div><h3>ADDITION INFORMATION INVOLVING AN INJURY</h3></div>`,
      hideExpression: '!formState.isInjuryField',
    },
    {
      id: 'injuryField',
      type: 'repeat',
      key: 'addlInfoInvolvingInjury',
      fieldGroupClassName: 'display-flex-mt-10',
      templateOptions: {
        addText: 'Add',
        addTextClass: 'add-btn',
        layout: 'modern',
        onDelete: (fieldGroup: FormlyFieldConfig[], model: IJson, formState: IJson) => {
          if (fieldGroup.length === 0) {
            formState.isInjuryField = false;
          }
        },
      },
      hideExpression: '!formState.isInjuryField',
      fieldArray: {
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'matselect',
                key: 'categoryId',
                templateOptions: {
                  placeholder: 'Injury Category',
                  label: 'Injury Category',
                  valueProp: 'injuryCategoryId',
                  labelProp: 'category',
                  options: [],
                },
                expressionProperties: {
                  'templateOptions.options': (model: any, formState: any, field: FormlyFieldConfig) => {
                    return formState.injuryCategory || [];
                  },
                },
              },
              {
                className: 'flex-1',
                type: 'matselect',
                key: 'injuryOccuredId',
                templateOptions: {
                  placeholder: 'Injury Occured By',
                  label: 'Injury Occured By',
                  valueProp: 'injuryOccuredById',
                  labelProp: 'cause',
                  options: [],
                },
                expressionProperties: {
                  'templateOptions.options': (model: any, formState: any, field: FormlyFieldConfig) => {
                    return formState.injuryOccuredBy || [];
                  },
                },
              },
              {
                className: 'flex-1',
                type: 'matselect',
                key: 'rankId',
                templateOptions: {
                  placeholder: 'Rank',
                  label: 'Rank',
                  valueProp: 'rankId',
                  labelProp: 'rank',
                  options: [],
                },
                expressionProperties: {
                  'templateOptions.options': (model: any, formState: any, field: FormlyFieldConfig) => {
                    return formState.rankList || [];
                  },
                },
              },
              {
                className: 'flex-1',
                type: 'matselect',
                key: 'bodyPartAffId',
                templateOptions: {
                  placeholder: 'Body Part Affected',
                  label: 'Body Part Affected',
                  valueProp: 'bodyPartAffectedId',
                  labelProp: 'typeOfPart',
                  options: [],
                },
                expressionProperties: {
                  'templateOptions.options': (model: any, formState: any, field: FormlyFieldConfig) => {
                    return formState.bodyPartEffect || [];
                  },
                },
              },
              {
                className: 'flex-1',
                type: 'input',
                key: 'age',
                templateOptions: {
                  placeholder: 'Age',
                  label: 'Age',
                },
              },
            ],
          },
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'matselect',
                key: 'nationalityId',
                templateOptions: {
                  placeholder: 'Nationality',
                  label: 'Nationality',
                  valueProp: 'nationalityId',
                  labelProp: 'countryName',
                  options: [],
                },
                expressionProperties: {
                  'templateOptions.options': (model: any, formState: any, field: FormlyFieldConfig) => {
                    return formState.nationality || [];
                  },
                },
              },
              {
                className: 'flex-1',
                type: 'input',
                key: 'timeOnBoard',
                defaultValue: '',
                templateOptions: {
                  label: 'Time of Board',
                  placeholder: 'Time of Board',
                },
              },
              {
                className: 'flex-1',
                type: 'input',
                key: 'timeInRank',
                defaultValue: '',
                templateOptions: {
                  label: 'Time in Rank',
                  placeholder: 'Time in Rank',
                },
              },
              {
                className: 'flex-1',
                type: 'input',
                key: 'timeInCompany',
                defaultValue: '',
                templateOptions: {
                  label: 'Time in Company',
                  placeholder: 'Time in Company',
                },
              },
            ],
          },
        ],
      },
    },
  ];
};
