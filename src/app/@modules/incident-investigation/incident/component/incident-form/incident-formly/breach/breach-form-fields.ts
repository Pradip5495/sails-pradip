import { FormlyFieldConfig } from '@formly';

export default (): FormlyFieldConfig[] => {
  return [
    {
      template: `
      <span><i>
      Review company procedures and industry regulations to identify any breach
      </i></span>
      <br /><br />
      `,
    },
    {
      type: 'repeat',
      key: 'breachReqData',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add Document +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'input',
                key: 'documents',
                templateOptions: {
                  placeholder: 'Documents',
                  label: 'Documents',
                  required: true,
                },
              },
              {
                className: 'flex-1',
                type: 'input',
                key: 'refernce',
                templateOptions: {
                  placeholder: 'Reference',
                  label: 'Reference',
                  required: true,
                },
              },
              {
                className: 'flex-1',
                type: 'input',
                key: 'description',
                templateOptions: {
                  placeholder: 'Description',
                  label: 'Description',
                  required: true,
                },
              },
            ],
          },
        ],
      },
    },
    {
      fieldGroupClassName: 'display-flex mt-10',
      fieldGroup: [
        {
          className: 'flex-1 mt-10',
          type: 'textarea',
          key: 'breachAdditionalComments',
          templateOptions: {
            placeholder: 'Additional Comments',
            label: 'Additional Comments',
            rows: 3,
            required: true,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'sailfile',
          key: 'breanchAttachment',
          templateOptions: {
            preview: true,
            id: 'breachFileId',
          },
        },
      ],
    },
  ];
};
