import { FormlyFieldConfig } from '@formly';
import { IJson } from '@types';

export default (props: IJson): FormlyFieldConfig[] => {
  return [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'incidentTitle',
          type: 'input',
          templateOptions: {
            placeholder: 'Incident Title',
            label: 'Incident Title',
            required: true,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'matdatepicker',
          key: 'dateOfIncident',
          templateOptions: {
            label: 'Date of Incident',
            placeholder: 'Date of Incident',
            required: true,
            readonly: true,
            appearance: 'outline',
            datepickerOptions: {
              max: new Date(),
            },
          },
        },
        {
          className: 'flex-1',
          key: 'vesselId',
          type: 'select',
          templateOptions: {
            label: 'Vessel',
            placeholder: 'Vessel',
            appearance: 'outline',
            required: true,
            valueProp: 'vesselId',
            labelProp: 'vessel',
            options: props.vesselList,
          },
        },
        {
          className: 'flex-1',
          type: 'datetimepicker',
          key: 'timeOfIncident',
          templateOptions: {
            label: 'Time of Incident',
            required: true,
            addonRight: {
              icon: 'access_time',
            },
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'toggle-button',
          key: 'portSea',
          defaultValue: '0',
          templateOptions: {
            label1: 'At Sea',
            label2: 'Port',
            onChange: props.portSeaOnChange,
          },
        },
        {
          className: 'flex-6',
          key: 'portId',
          type: 'select',
          templateOptions: {
            label: 'Port Name',
            placeholder: 'Port Name',
            appearance: 'outline',
            valueProp: 'portId',
            labelProp: 'name',
            options: props.portList,
          },
          expressionProperties: {
            'templateOptions.required': 'model.portSea == 0',
          },
          hideExpression: 'model.portSea == 1',
        },
        {
          className: 'flex-3',
          key: 'latitude',
          type: 'input',
          templateOptions: {
            label: 'Latitude',
            placeholder: 'Latitude',
            appearance: 'outline',
          },
          hideExpression: 'model.portSea == 0',
        },
        {
          className: 'flex-3',
          key: 'longitude',
          type: 'input',
          templateOptions: {
            label: 'Longitude',
            placeholder: 'Longitude',
            appearance: 'outline',
          },
          hideExpression: 'model.portSea == 0',
        },
        {
          className: 'flex-3',
          key: 'vesselLocation',
          type: 'select',
          templateOptions: {
            label: 'Vessel Location',
            placeholder: 'Vessel Location',
            appearance: 'outline',
            valueProp: 'value',
            labelProp: 'label',
            options: [],
          },
          expressionProperties: {
            'templateOptions.options': (model: any, formState: any, field: FormlyFieldConfig) => {
              if (model.portSea === '0') {
                return [
                  { value: 'In Port', label: 'In Port', group: 'Port' },
                  { value: 'Anchorage', label: 'Anchorage', group: 'Port' },
                  { value: 'Pilotage', label: 'Pilotage', group: 'Port' },
                  { value: 'Shipyard', label: 'Shipyard', group: 'Port' },
                  { value: 'Offshore', label: 'Offshore', group: 'Port' },
                ];
              } else {
                return [{ label: 'Open Sea', value: 'Open Sea', group: 'At Sea' }];
              }
            },
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'briefDescription',
          type: 'textarea',
          templateOptions: {
            appearance: 'outline',
            placeholder: 'Brief description of voyage',
            label: 'Brief description',
            required: true,
            rows: 3,
          },
        },
      ],
    },
    {
      fieldGroupClassName: '',
      wrappers: ['card'],
      templateOptions: { label: 'DESCRIPTION OF THE INCIDENT' },
      fieldGroup: [
        {
          className: 's1-description',
          key: 'description',
          type: 'textarea',
          templateOptions: {
            appearance: 'outline',
            placeholder: 'Description',
            label: 'Description',
            required: true,
            rows: 3,
          },
        },
        {
          fieldGroupClassName: 'display-flex',
          fieldGroup: [
            {
              className: 'flex-1',
              key: 'taskId',
              type: 'select',
              templateOptions: {
                label: 'Task Involved',
                placeholder: 'Task Involved',
                appearance: 'outline',
                valueProp: 'taskId',
                labelProp: 'task',
                required: true,
                options: props.taskList,
              },
            },
            {
              className: 'flex-1',
              key: 'locationId',
              type: 'select',
              templateOptions: {
                label: 'Location',
                placeholder: 'Location',
                appearance: 'outline',
                valueProp: 'locationId',
                labelProp: 'location',
                required: true,
                options: props.locationList,
              },
            },
          ],
        },
      ],
    },
  ];
};
