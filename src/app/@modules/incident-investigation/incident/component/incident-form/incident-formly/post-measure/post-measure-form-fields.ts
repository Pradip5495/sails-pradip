import { FormlyFieldConfig } from '@formly';
import { IJson } from '@types';

export default (): FormlyFieldConfig[] => {
  return [
    {
      template: `
      <p>
        <i>
        Record details of immediate action taken to normalize the incident
        </i>
      </p>
      `,
    },
    {
      type: 'repeat',
      key: 'postRecoveryDate',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'New Date +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-2',
                type: 'matdatepicker',
                key: 'date',
                templateOptions: {
                  required: true,
                  readonly: true,
                  placeholder: 'Date',
                  label: 'Date',
                },
              },
              {
                className: 'flex-6',
                type: 'repeat',
                key: 'postRecoveryData',
                templateOptions: {
                  addText: 'Add',
                  addTextClass: 'add-btn',
                  nonRemovableIndex: 0,
                  inlineAdd: true,
                  // addOnInit: false
                },
                fieldArray: {
                  fieldGroupClassName: '',
                  fieldGroup: [
                    {
                      fieldGroupClassName: 'display-flex',
                      fieldGroup: [
                        {
                          className: 'flex-1',
                          type: 'datetimepicker',
                          key: 'ltTime',
                          defaultValue: '',
                          templateOptions: {
                            label: 'LT Time',
                            readonly: true,
                            required: true,
                          },
                        },
                        {
                          className: 'flex-2',
                          type: 'textarea',
                          key: 'description',
                          templateOptions: {
                            required: true,
                            label: 'Description',
                            placeholder: 'Description',
                          },
                        },
                        {
                          className: 'flex-0-auto',
                          type: 'button',
                          templateOptions: {
                            label: 'Add',
                            className: 'action-btn-delete',
                            onClick: (event: IJson, field: FormlyFieldConfig) => {
                              field.parent.parent.parent.templateOptions.componentRef.add();
                            },
                          },
                        },
                      ],
                    },
                  ],
                },
              },
            ],
          },
        ],
      },
    },
    {
      fieldGroupClassName: 'display-flex mt-15',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'textarea',
          key: 'postRecAdditionalComments',
          templateOptions: {
            label: 'Additional Comments',
            placeholder: 'Comment',
            rows: 3,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'sailfile',
          key: 'postMeasureAttachment',
          templateOptions: {
            preview: true,
            id: 'postMeasureFileId',
          },
        },
      ],
    },
  ];
};
