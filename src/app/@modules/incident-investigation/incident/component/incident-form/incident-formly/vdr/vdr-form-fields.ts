import { FormlyFieldConfig } from '@formly';
import { IJson } from '@types';

export default (props: IJson = {}): FormlyFieldConfig[] => {
  return [
    {
      type: 'repeat',
      key: 'reviewVdrDate',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'New Date +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'matdatepicker',
                key: 'date',
                templateOptions: {
                  required: true,
                  readonly: true,
                  placeholder: 'Date',
                  label: 'Date',
                },
              },
              {
                className: 'flex-5',
                type: 'repeat',
                key: 'reviewVdrData',
                templateOptions: {
                  addText: 'Add',
                  addTextClass: 'add-btn',
                  nonRemovableIndex: 0,
                  inlineAdd: true,
                  deleteBtnClass: 'adjust-margin',
                  // addOnInit: false
                },
                fieldArray: {
                  fieldGroupClassName: '',
                  fieldGroup: [
                    {
                      fieldGroupClassName: 'display-flex',
                      fieldGroup: [
                        {
                          className: 'flex-1',
                          type: 'datetimepicker',
                          key: 'ltTime',
                          templateOptions: {
                            label: 'LT Time',
                            readonly: true,
                            required: true,
                          },
                        },
                        {
                          className: 'flex-2',
                          type: 'textarea',
                          key: 'description',
                          templateOptions: {
                            required: true,
                            label: 'Description',
                            placeholder: 'Description',
                          },
                        },
                        {
                          className: 'flex-0-auto',
                          type: 'sailfile',
                          key: 'vdrFileUpload',
                          templateOptions: {
                            emptyLabel: true,
                            preview: true,
                            unsetModel: true,
                            id: 'vdrfile',
                            onFileChange: (files: any[]) => {
                              if (props.onVdrFileChange) {
                                props.onVdrFileChange(files);
                              }
                            },
                          },
                        },
                        {
                          className: 'flex-0-auto',
                          type: 'button',
                          templateOptions: {
                            label: 'Add',
                            type: 'button',
                            className: 'action-btn-delete',
                            onClick: (event: IJson, field: FormlyFieldConfig) => {
                              field.parent.parent.parent.templateOptions.componentRef.add();
                            },
                          },
                        },
                      ],
                    },
                  ],
                },
              },
            ],
          },
        ],
      },
    },
    {
      fieldGroupClassName: 'display-flex mt-15',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'textarea',
          key: 'additionalComments',
          templateOptions: {
            label: 'Additional Comments',
            placeholder: 'Comment',
            rows: 3,
          },
        },
      ],
    },
  ];
};
