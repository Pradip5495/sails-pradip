import { FormlyFieldConfig } from '@formly';
import { IJson } from '@types';

export default (props: any): FormlyFieldConfig[] => {
  if (props.investigationByPosition.length === 0 || props.investigationList.length === 0) {
    return [];
  }

  return [
    {
      template: `
        <div>Team Lead</div>
        <hr/>
      `,
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'ng-select',
          key: 'investigatorsId',
          templateOptions: {
            required: true,
            placeholder: 'Name',
            label: 'Name',
            bindValue: 'investigatorsId',
            bindLabel: 'name',
            items: props.investigationList,
            onChange: (event: IJson, field: FormlyFieldConfig) => {
              field.parent.form.patchValue({ designation: event?.position || '' });
              field.parent.form.patchValue({ department: event?.department || '' });
            },
          },
        },
        {
          className: 'flex-1',
          type: 'input',
          key: 'designation',
          defaultValue: '',
          templateOptions: {
            readonly: true,
            placeholder: 'Designation',
            label: 'Designation',
          },
        },
        {
          className: 'flex-1',
          type: 'input',
          key: 'department',
          defaultValue: '',
          templateOptions: {
            readonly: true,
            placeholder: 'Organization / Department',
            label: 'Organization / Department',
          },
        },
      ],
    },
    {
      template: `
        <div>Team Member</div>
        <hr/>
      `,
    },
    {
      type: 'repeat',
      key: 'investigationTeamMember',
      fieldGroupClassName: 'display-flex',
      defaultValue: [],
      templateOptions: {
        addText: 'Add Team Member +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
      },
      fieldArray: {
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-4',
                type: 'matselect',
                key: 'investigatorsId',
                defaultValue: '',
                templateOptions: {
                  required: true,
                  placeholder: 'Name',
                  label: 'Name',
                  valueProp: 'investigatorsId',
                  labelProp: 'name',
                  options: props.investigationByPosition,
                  onChange: (field: FormlyFieldConfig, event: IJson, item: any) => {
                    field.parent.form.patchValue({ designation: item.position || '' });
                    field.parent.form.patchValue({ department: item.department || '' });
                  },
                },
              },
              {
                className: 'flex-4',
                type: 'input',
                key: 'designation',
                defaultValue: '',
                templateOptions: {
                  readonly: true,
                  placeholder: 'Designation',
                  label: 'Designation',
                },
              },
              {
                className: 'flex-1',
                type: 'input',
                key: 'department',
                defaultValue: '',
                templateOptions: {
                  readonly: true,
                  placeholder: 'Organization / Department',
                  label: 'Organization / Department',
                },
              },
            ],
          },
        ],
      },
    },
  ];
};
