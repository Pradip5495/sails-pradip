import { FormlyFieldConfig } from '@formly';
import { IJson } from '@types';

export default (props: IJson): FormlyFieldConfig[] => {
  if (props.allUsers.length === 0 || props.investigationList.length === 0) {
    return [];
  }

  return [
    {
      template: `
        <div>Report Prepared By:</div>
        <hr/>
      `,
    },
    {
      type: 'repeat',
      key: 'reportPreparedBy',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add Prepared By +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-4',
                type: 'matselect',
                key: 'internalContactId',
                templateOptions: {
                  required: true,
                  placeholder: 'Name',
                  label: 'Name',
                  valueProp: 'userId',
                  labelProp: 'fullname',
                  options: props.allUsers,
                  onChange: (field: FormlyFieldConfig, event: IJson, item: any) => {
                    field.parent.form.patchValue({ designation: item?.designation || '' });
                  },
                },
              },
              {
                className: 'flex-4',
                type: 'input',
                key: 'designation',
                templateOptions: {
                  placeholder: 'Designation',
                  label: 'Designation',
                  readonly: true,
                },
              },
              {
                className: 'flex-4',
                type: 'input',
                key: 'department',
                templateOptions: {
                  placeholder: 'Organization / Department',
                  label: 'Organization / Department',
                },
              },
            ],
          },
        ],
      },
    },
    {
      template: `
        <br /><br />
        <div>Report Reviewed By:</div>
        <hr/>
      `,
    },
    {
      type: 'repeat',
      key: 'reportReviewedBy',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add Reviewed By +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-4',
                type: 'matselect',
                key: 'reportReviewedById',
                templateOptions: {
                  required: true,
                  placeholder: 'Name',
                  label: 'Name',
                  valueProp: 'investigatorsId',
                  labelProp: 'name',
                  options: props.investigationList,
                  onChange: (field: FormlyFieldConfig, event: IJson, item: any) => {
                    field.parent.form.patchValue({ designation: item?.position || '' });
                  },
                },
              },
              {
                className: 'flex-4',
                type: 'input',
                key: 'designation',
                templateOptions: {
                  placeholder: 'Designation',
                  label: 'Designation',
                  readonly: true,
                },
              },
              {
                className: 'flex-4',
                type: 'matdatepicker',
                key: 'dateOfReview',
                templateOptions: {
                  required: true,
                  placeholder: 'Date of Review',
                  label: 'Date of Review',
                },
              },
            ],
          },
        ],
      },
    },
    {
      template: `
        <br /><br />
        <div>Report Approved By:</div>
        <mat-divider role="separator" class="mat-divider divider mat-divider-horizontal" aria-orientation="horizontal"></mat-divider>
        <hr/>
      `,
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'matselect',
          key: 'reportApprovedById',
          templateOptions: {
            options: props.investigationList,
            required: true,
            valueProp: 'investigatorsId',
            labelProp: 'name',
            placeholder: 'Name',
            label: 'Name',
          },
        },
        {
          className: 'flex-1',
          type: 'matdatepicker',
          key: 'dateOfApprove',
          templateOptions: {
            required: true,
            placeholder: 'Date of Approve',
            label: 'Date of Approve',
          },
        },
      ],
    },
  ];
};
