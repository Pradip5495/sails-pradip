import { FormlyFieldConfig } from '@formly';
import { IJson } from '@types';

export default (props: IJson): FormlyFieldConfig[] => {
  if (props.elements.length === 0) {
    return [];
  }

  return [
    {
      type: 'repeat',
      key: 'crossRefToTMSA',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add TMSA +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-2',
                type: 'select',
                key: 'elementId',
                templateOptions: {
                  placeholder: 'Element',
                  label: 'Element',
                  required: true,
                  valueProp: 'elementId',
                  labelProp: 'elementTitle',
                  options: props.elements,
                  change: (field: FormlyFieldConfig, event: IJson) => {
                    props.filterStages({ value: event.value });
                    field.model.stageId = '';
                    field.model.specificKpiId = '';
                  },
                },
              },
              {
                className: 'flex-2',
                type: 'select',
                key: 'stageId',
                templateOptions: {
                  placeholder: 'Stage',
                  label: 'Stage',
                  required: true,
                  valueProp: 'stageId',
                  labelProp: 'stageNo',
                  options: [],
                  change: (field: FormlyFieldConfig, event: IJson) => {
                    props.filterSpecificKPI({ value: event.value });
                  },
                },
                expressionProperties: {
                  'templateOptions.options': (model: any, formState: any, field: FormlyFieldConfig) => {
                    if (model.elementId && formState.stageData) {
                      return formState.stageData[model.elementId] || [];
                    }

                    return [];
                  },
                },
              },
              {
                className: 'flex-2',
                type: 'select',
                key: 'specificKpiId',
                templateOptions: {
                  required: true,
                  placeholder: 'Specific KPI',
                  label: 'Specific KPI',
                  valueProp: 'stageKpiId',
                  labelProp: 'specificKpi',
                  options: [],
                },
                expressionProperties: {
                  'templateOptions.options': (model: any, formState: any, field: FormlyFieldConfig) => {
                    if (model.stageId && formState.specificData) {
                      return formState.specificData[model.stageId] || [];
                    }
                    return [];
                  },
                },
              },
              {
                className: 'flex-2',
                type: 'textarea',
                key: 'additionalComments',
                templateOptions: {
                  placeholder: 'Additional Comments',
                  label: 'Additional Comments',
                  rows: 1,
                },
              },
            ],
          },
        ],
      },
    },
  ];
};
