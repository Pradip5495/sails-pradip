import { FormlyFieldConfig } from '@formly';
import { IJson } from '@types';

export default (): FormlyFieldConfig[] => {
  return [
    {
      template: `
      <p>
        <i>
        Record details of notification made to internal/external parties such as branch offices,
        fleet vessels,port state,flag state,Characters,Oil Majors.etc
        </i>
      </p>
      `,
    },
    {
      type: 'repeat',
      key: 'notificationDate',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'New Date +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-2',
                type: 'matdatepicker',
                key: 'date',
                templateOptions: {
                  required: true,
                  readonly: true,
                  placeholder: 'Date',
                  label: 'Date',
                },
              },
              {
                className: 'flex-6',
                type: 'repeat',
                key: 'notificationData',
                templateOptions: {
                  addText: 'Add',
                  addTextClass: 'add-btn',
                  nonRemovableIndex: 0,
                  inlineAdd: true,
                  // addOnInit: false
                },
                fieldArray: {
                  fieldGroupClassName: '',
                  fieldGroup: [
                    {
                      fieldGroupClassName: 'display-flex',
                      fieldGroup: [
                        {
                          className: 'flex-1',
                          type: 'datetimepicker',
                          key: 'ltTime',
                          defaultValue: '',
                          templateOptions: {
                            label: 'LT Time',
                            readonly: true,
                            required: true,
                          },
                        },
                        {
                          className: 'flex-2',
                          type: 'input',
                          key: 'notificationParty',
                          templateOptions: {
                            required: true,
                            label: 'Notification Party',
                            placeholder: 'Notification Party',
                          },
                        },
                        {
                          className: 'flex-2',
                          type: 'input',
                          key: 'methodUsed',
                          templateOptions: {
                            required: true,
                            label: 'Method',
                            placeholder: 'Method',
                          },
                        },
                        {
                          className: 'flex-0-auto',
                          type: 'button',
                          templateOptions: {
                            label: 'Add',
                            className: 'action-btn-delete',
                            onClick: (event: IJson, field: FormlyFieldConfig) => {
                              field.parent.parent.parent.templateOptions.componentRef.add();
                            },
                          },
                        },
                      ],
                    },
                  ],
                },
              },
            ],
          },
        ],
      },
    },
    {
      fieldGroupClassName: 'display-flex mt-15',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'textarea',
          key: 'notifyAdditionalComments',
          templateOptions: {
            label: 'Additional Comments',
            placeholder: 'Comment',
            rows: 3,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'sailfile',
          key: 'attachment',
          templateOptions: {
            preview: true,
            id: 'notificationFileId',
          },
        },
      ],
    },
  ];
};
