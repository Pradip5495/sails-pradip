import { FormlyFieldConfig } from '@formly';
import { IJson } from '@types';

export default (): FormlyFieldConfig[] => {
  return [
    {
      type: 'repeat',
      key: 'incidentSeqData',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'New Date +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-2',
                type: 'matdatepicker',
                key: 'date',
                templateOptions: {
                  required: true,
                  readonly: true,
                  placeholder: 'Date',
                  label: 'Date',
                },
              },
              {
                className: 'flex-6',
                type: 'repeat',
                key: 'seqData',
                templateOptions: {
                  addText: 'Add',
                  addTextClass: 'add-btn',
                  nonRemovableIndex: 0,
                  inlineAdd: true,
                },
                fieldArray: {
                  fieldGroupClassName: '',
                  fieldGroup: [
                    {
                      fieldGroupClassName: 'display-flex',
                      fieldGroup: [
                        {
                          className: 'flex-1',
                          type: 'datetimepicker',
                          key: 'ltTime',
                          defaultValue: '',
                          templateOptions: {
                            label: 'LT Time',
                            readonly: true,
                            required: true,
                          },
                        },
                        {
                          className: 'flex-2',
                          type: 'textarea',
                          key: 'description',
                          templateOptions: {
                            required: true,
                            label: 'Description',
                            placeholder: 'Description',
                          },
                        },
                        {
                          type: 'toggle',
                          key: 'triggeringFactors',
                          templateOptions: {
                            label: 'Trigger Fact',
                          },
                        },
                        {
                          className: 'flex-0-auto',
                          type: 'button',
                          templateOptions: {
                            label: 'Add',
                            className: 'action-btn-delete',
                            onClick: (event: IJson, field: FormlyFieldConfig) => {
                              field.parent.parent.parent.templateOptions.componentRef.add();
                            },
                          },
                        },
                      ],
                    },
                  ],
                },
              },
            ],
          },
        ],
      },
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'sailfile',
          key: 'sequenceEventAttachment',
          templateOptions: {
            preview: true,
            id: 'sequenceEventAttachment',
          },
        },
      ],
    },
  ];
};
