export const BackgroundFormRepeatItem = [
  {
    fieldGroupClassName: '',
    fieldGroup: [
      {
        fieldGroupClassName: 'display-flex',
        fieldGroup: [
          {
            template: `<h4 class="bg-header">Brief</h4>`,
          },
        ],
      },
      {
        fieldGroupClassName: 'display-flex',
        fieldGroup: [
          {
            className: 'flex-6',
            type: 'input',
            key: 'description',
            defaultValue: '',
            templateOptions: {
              placeholder: 'Brief Description',
              label: 'Brief Description',
              floatLabel: 'never',
            },
          },
          {
            className: 'flex-1 contributing_factor',
            type: 'checkbox',
            key: 'isContibutingFactor',
            defaultValue: false,
          },
        ],
      },
      {
        fieldGroupClassName: 'display-flex',
        fieldGroup: [
          {
            type: 'sailfile',
            key: 'attachment',
            templateOptions: {
              preview: true,
              id: '_file_' + new Date().getTime(),
            },
          },
        ],
      },
    ],
  },
];
