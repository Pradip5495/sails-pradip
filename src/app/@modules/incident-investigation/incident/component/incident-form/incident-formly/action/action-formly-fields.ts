import { FormlyFieldConfig } from '@formly';

export default (props: any): FormlyFieldConfig[] => {
  if (props.proposedBy.length === 0 && props.responsibility.length === 0 && props.actionStatus.length === 0) {
    return [];
  }

  return [
    {
      type: 'repeat',
      key: 'action',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add Action +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'select',
                key: 'proposedBy',
                templateOptions: {
                  placeholder: 'Proposed By',
                  label: 'Proposed By',
                  required: true,
                  valueProp: 'value',
                  labelProp: 'label',
                  options: props.proposedBy,
                },
                hideExpression: (model: any) => model.actionType === 'TAKEN' && model.type !== 'Immediate Action',
              },
              {
                className: 'flex-1 pl-0',
                type: 'select',
                key: 'type',
                templateOptions: {
                  class: 'testing',
                  placeholder: 'Action',
                  label: 'Action',
                  required: true,
                  valueProp: 'value',
                  labelProp: 'label',
                  options: [
                    {
                      value: 'Immediate Action',
                      label: 'Immediate Action',
                    },
                    {
                      value: 'Corrective Action',
                      label: 'Corrective Action',
                    },
                    {
                      value: 'Preventive Action',
                      label: 'Preventive Action',
                    },
                  ],
                },
              },
              {
                className: 'flex-1 pl-0',
                type: 'select',
                key: 'actionType',
                templateOptions: {
                  placeholder: 'Action Type',
                  label: 'Action Type',
                  required: true,
                  valueProp: 'value',
                  labelProp: 'label',
                  options: [
                    {
                      value: 'PLANNED',
                      label: 'Planned',
                    },
                    {
                      value: 'TAKEN',
                      label: 'Taken',
                    },
                  ],
                },
                expressionProperties: {
                  'templateOptions.disabled': (model: any, formState: any, field: FormlyFieldConfig) => {
                    if (model.type === 'Immediate Action') {
                      field.formControl.patchValue('TAKEN');
                      return true;
                    }
                    return false;
                  },
                },
              },
              {
                className: 'flex-2',
                type: 'textarea',
                key: 'natureOfAction',
                templateOptions: {
                  placeholder: 'Nature of Action',
                  label: 'Nature of Action',
                  rows: 1,
                },
              },
            ],
          },
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'select',
                key: 'designationId',
                templateOptions: {
                  placeholder: 'Responsibility',
                  label: 'Responsibility',
                  required: true,
                  valueProp: 'value',
                  labelProp: 'label',
                  options: props.responsibility,
                },
                hideExpression: (model: any) => model.actionType === 'TAKEN',
              },
              {
                className: 'flex-1 pl-0',
                type: 'input',
                key: 'email',
                templateOptions: {
                  placeholder: 'Email',
                  label: 'Email',
                },
                hideExpression: (model: any) => model.actionType === 'TAKEN',
              },
              {
                className: 'flex-1 pl-0',
                type: 'matdatepicker',
                key: 'dueDate',
                templateOptions: {
                  placeholder: 'Due Date',
                  label: 'Due Date',
                },
                hideExpression: (model: any) => model.actionType === 'TAKEN',
              },
              {
                className: 'flex-1 pl-0',
                type: 'matdatepicker',
                key: 'dateCompleted',
                templateOptions: {
                  placeholder: 'Date Completed',
                  label: 'Date Completed',
                },
                hideExpression: (model: any) => model.type === 'Immediate Action',
              },
              {
                className: 'flex-1',
                type: 'select',
                key: 'status',
                templateOptions: {
                  placeholder: 'Status',
                  label: 'Status',
                  required: true,
                  valueProp: 'value',
                  labelProp: 'label',
                  options: props.actionStatus,
                },
                hideExpression: (model: any) => model.type === 'Immediate Action',
              },
            ],
          },
        ],
      },
    },
    {
      fieldGroupClassName: 'display-flex mt-10',
      fieldGroup: [
        {
          className: 'flex-1 mt-10',
          type: 'textarea',
          key: 'office_comment',
          templateOptions: {
            placeholder: 'Office Comment',
            label: 'Office Comment',
            rows: 3,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          type: 'sailfile',
          key: 'attachment',
          templateOptions: {
            preview: true,
            id: 'actionFileId',
          },
        },
      ],
    },
  ];
};
