import { FormlyFieldConfig } from '@formly';
import { IJson } from '@types';
import { BackgroundFormRepeatItem } from './background-form.util';

export default (props: IJson): FormlyFieldConfig[] => {
  let itemsToAdd: any[] = [];

  return [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-4',
          type: 'ng-select',
          key: 'backgroundConditionId',
          templateOptions: {
            /**
             * When bindValue is not empty, ng-select doesn't detect bindLabel value
             * when we add section in repeat type
             * https://stackoverflow.com/a/55088469
             */
            bindValue: '',
            bindLabel: 'background',
            required: true,
            multiple: true,
            maxSelectedItems: 100,
            label: 'Background Conditions',
            placeholder: 'Background Conditions',
            hideSelected: true,
            items: props.backgroundCondition,
            onChange: (event: any, field: FormlyFieldConfig) => {
              itemsToAdd = event;
            },
          },
        },
        {
          className: 'flex-1',
          type: 'button',
          key: 'btnaddcondition',
          templateOptions: {
            label: 'Add Conditions',
            appearance: 'raised',
            color: 'primary',
            onClick: (event: any, field: FormlyFieldConfig) => {
              const parentField = field.parent.parent;
              field.parent.parent.fieldGroup[1].options.resetModel({
                backgroundConditionId: field.model.backgroundConditionId,
              });
              itemsToAdd.forEach((item, index: number) => {
                field.parent.parent.fieldGroup[1].fieldArray.fieldGroup[0].fieldGroup[0].fieldGroup[0].template = `
                <h4 class="bg-header">${item.background}</h4>
                `;

                parentField.fieldGroup[1].fieldArray.fieldGroup[0].fieldGroup[2].fieldGroup[0].templateOptions.id =
                  '' + new Date().getTime();
                field.parent.parent.fieldGroup[1].templateOptions.componentRef.add();
                return item;
              });
            },
          },
        },
        {
          template: '<b>Contributing Factor</b>',
        },
      ],
    },
    {
      type: 'repeat',
      key: 'backgroundData',
      defaultValue: [],
      templateOptions: {
        addText: '',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
        allowDelete: false,
        addOnInit: false,
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: BackgroundFormRepeatItem,
      },
    },
  ];
};
