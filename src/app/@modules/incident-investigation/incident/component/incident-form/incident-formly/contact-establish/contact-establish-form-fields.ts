import { FormlyFieldConfig } from '@formly';
import { IJson } from '@types';

export default (props: IJson): FormlyFieldConfig[] => {
  if (props.allUsers.length === 0 && props.allCompany.length === 0) {
    return [];
  }

  return [
    {
      template: `
        <div>INTERNAL CONTACTS</div>
        <hr/>
      `,
    },
    {
      type: 'repeat',
      key: 'intContact',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'New Contact +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-4',
                type: 'select',
                key: 'internalContactId',
                templateOptions: {
                  required: true,
                  placeholder: 'Name',
                  label: 'Name',
                  valueProp: 'userId',
                  labelProp: 'fullname',
                  options: props.allUsers,
                },
              },
              {
                className: 'flex-4',
                type: 'input',
                key: 'designation',
                templateOptions: {
                  placeholder: 'Designation',
                  label: 'Designation',
                },
              },
            ],
          },
        ],
      },
    },
    {
      template: `
        <br /><br />
        <div>EXTERNAL CONTACTS</div>
        <hr/>
      `,
    },
    {
      type: 'repeat',
      key: 'extContact',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'New Contact +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
        nonRemovableIndex: 0,
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-4',
                type: 'select',
                key: 'companyId',
                templateOptions: {
                  required: true,
                  placeholder: 'Company',
                  label: 'Company',
                  valueProp: 'companyId',
                  labelProp: 'companyName',
                  options: props.allCompany,
                },
              },
              {
                className: 'flex-4',
                type: 'input',
                key: 'name',
                templateOptions: {
                  placeholder: 'Name',
                  label: 'Name',
                },
              },
              {
                className: 'flex-4',
                type: 'input',
                key: 'designation',
                templateOptions: {
                  placeholder: 'Designation',
                  label: 'Designation',
                },
              },
            ],
          },
        ],
      },
    },
  ];
};
