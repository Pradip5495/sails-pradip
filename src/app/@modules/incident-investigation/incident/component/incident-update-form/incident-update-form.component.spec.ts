import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentUpdateFormComponent } from './incident-update-form.component';

describe('IncidentUpdateFormComponent', () => {
  let component: IncidentUpdateFormComponent;
  let fixture: ComponentFixture<IncidentUpdateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IncidentUpdateFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
