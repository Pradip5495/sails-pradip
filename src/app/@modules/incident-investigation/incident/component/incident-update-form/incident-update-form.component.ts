import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService, CacheServiceService, NotificationService } from '@shared/common-service';
import { HelperService } from '@shared/common-service/utility/helper.service';
import { UserService } from '@shared/service/user.service';
import { NearMissService } from '../../../share/near-miss.service';
import { IncidentCacheService } from '../../../share/incident-cache.service';
import { IncidentService } from '../../share/incident.service';
import { environment } from '../../../../../../environments/environment';
import { Logger } from '@core';
import { MatStepper } from '@angular/material/stepper';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { IncidentDailogComponent } from './../incident-dailog/incident-dailog.component';

const log = new Logger('Incident');

interface VesselLocation {
  value: string;
  viewValue: string;
}

interface VesselLocationGroup {
  disabled?: boolean;
  name: string;
  vesselLocation: VesselLocation[];
}

interface Designation {
  uid: string;
  name: string;
}

export interface StateGroup {
  letter: string;
  names: Designation[];
}
@Component({
  selector: 'app-incident-update-form',
  templateUrl: './incident-update-form.component.html',
  styleUrls: ['./incident-update-form.component.scss'],
})
export class IncidentUpdateFormComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;

  stateGroups: StateGroup[] = [];

  stateGroupOptions: Observable<StateGroup[]>;

  isLinear = false;
  overViewFormGroup: FormGroup;
  descriptionFormGroup: FormGroup;
  analysisFormGroup: FormGroup;
  attachmentFormGroup: FormGroup;
  crossReferenceForm: FormGroup;
  actionForm: FormGroup;
  investigationTeamForm: FormGroup;
  contactEstablishForm: FormGroup;
  incidentSeqDataForm: FormGroup;
  incidentVdrDataForm: FormGroup;
  postRecDataForm: FormGroup;
  notifyDataForm: FormGroup;
  breachDataForm: FormGroup;
  backgroundDataForm: FormGroup;
  communicationDataForm: FormGroup;

  public companyList: any;
  public title: 'Incident';
  public currentUser: any;
  public vesselTypeData: any = [];
  public states: string[] = ['None', 'Alaska'];
  public vesselLoc = 'Open Sea';
  public type: string[] = ['Internal', 'External'];
  public vesselLocation: string[] = ['Open Sea', 'In Port', 'Anchorage', 'Pilotage', 'Shipyard', 'Offshore'];
  public reportType: string;
  public severityLevel2: string[] = ['Extreme', 'Major', 'Moderate', 'Minor', 'Trivial'];
  public reportTypeList: any = [];
  public severityLevel: any = [];
  public probability: any = [];
  public impactList: any = [];
  public observedBy: any = [];
  public proposedBy: any = [];
  public actionStatus: any = [];
  public department: any = [];
  public bgImage: string[] = [];

  vesselLocationGroups: VesselLocationGroup[] = [
    {
      name: 'At Sea',
      vesselLocation: [{ value: 'Open Sea', viewValue: 'Open Sea' }],
    },
  ];

  toppings = new FormControl();
  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];

  public indexing: any = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'];
  public impactLabel: any;
  public contactOrEventOptionList: any = [];
  public contactOrEventOptionActive: any = [];
  public basicImmidateOptionList: any = [];
  public basicImmidateOptionActive: any = [];
  public ImmediateCauseId: any;
  public rootCauseId: any;
  public correctiveActionId: any;
  public immediateArray: any = [];
  public contactImmediateArray: any = [];
  public rootOptionList: any = [];
  public rootCauseOptionList: any = [];
  public rootCauseOptionActive: any = [];
  public correctiveCauseOptionList: any = [];
  public correctiveCauseOptionActive: any = [];
  public portList: any = [];
  public locationList: any = [];
  public contactList: any;
  public levelName: any;
  public isLatLong = true;
  public LatLongLabel = 'Port';
  error: string | undefined;
  public isLoading = false;
  public innerValue = 'Open Sea';
  public vesselLocValue = 'Open Sea';
  public innerType = 'Unsafe Act';
  public stopCardIssuedValue = 'yes';
  public impactValue = 'Unsafe Act';
  public impactSelectedArray: any = [];
  public severityValue = 'Minor';
  public probabilityValue = 'Possible';
  public actionFlag = 'Open';
  public readonly: boolean;
  public images: any = [];
  public seqImagesArray: any = [];
  public backgroundImageArray: any = [];
  public backgroundImages: any = [];
  public breachFile: any = [];
  public breachFileArray: any = [];
  public reviewOfVdrFile: any = [];
  public reviewOfVdrFileArray: any = [];
  public postRecFile: any = [];
  public postRecFileArray: any = [];
  public notifyFile: any = [];
  public notifyFileArray: any = [];
  public actionFile: any = [];
  public actionFileArray: any = [];
  public checked = true;
  public subDetails: any;
  public activeImmediateCause: any;
  public activeContactCause: any;
  public activeRootCause: any;
  public activeCorrectiveCause: any;
  public correctiveCauseArray: any = [];
  public causeTree: any;
  public filterBasicImmediateCause: any = [];
  public filterBasicRootCause: any = [];
  public filterBasicCorrectiveCause: any = [];
  public taskList: any;
  public severity = 2;
  public probabilityImpact = 3;
  public impact = 6;
  public backgroundImage: any = [];
  public backgroundDataArray: any = [];
  public eventImpact: any = [];
  public injuryCategory: any = [];
  public injuryOccured: any = [];
  public rankList: any = [];
  public bodyPartAffIdList: any = [];
  public nationalityList: any = [];
  public equipmentList: any = [];
  public equipmentCategoryList: any = [];
  public investigatorsList: any = [];
  public teamMemberList: any = [];
  public userList: any = [];
  public locationData: any = [];
  public similarIncident: any = [];
  public designationList: any = [];
  public incidentId: any;
  public incidentData: any;
  public spacificData: any = [];
  public stageData: any = [];
  public relationalId: any;
  public relationalData: any;
  public options: any = [];
  public designationData: any = [];
  public elementData: any;
  public resposibilityArray: any = [];
  public stageArray: any = [];
  public newTeamMemberArray: any = [];
  public eqpOtherDetailsArray: any = [];
  public backgroundDetailsArray: any = [];

  test = [new Date(), new Date()];

  public eventImpactType: any;
  public seqOfEventAttachments: any;
  public actionAttachments: any;
  public reviewOfVdrDataAttachments: any;
  public notificationAttachments: any;
  public postRecoveryMeasuresAttachments: any;
  public breachAttachments: any;
  public timeOfIncident = new Date();
  public tomorrow = new Date();
  public reportDate = new Date();
  public incidentPutResponse: any;
  public overviewError = true;
  public activeIndex: any = 1;
  public ltTimeInt: any;
  public proposedByArray: any = [];
  public isImmediateCauseArray: any = [];
  public typeContactView = false;
  public typeOfContactData: any;
  public causeTreeValidate = false;
  public typeOfContactArray: any = [];
  public basicCauseArray: any = [];
  public rootCauseArray: any = [];
  public correctiveArray: any = [];
  public analysisFormData: any;
  public isRootCause = true;
  public isCorrectiveCause = true;
  public analysisData: any;
  public reviewOfVdrImages: any = [];
  public reviewVdrImage: any = [];
  public peopleImpact: any;
  public environmentImpact: any;
  public businessImpact: any;
  public assetsImpact: any;
  public additionInjuryFlag = false;
  public eqpDamageFlag = false;
  public vdrFlag = false;
  public severityArray: any = [];
  public backgroundConditionData: any = [];
  public newBackgroundCondition: any = [];
  public formStepErrorArray: any = [];
  public impactValue2: any = [];

  natureOfAction: string;
  name: string;

  color = 'primary';
  check = false;
  public API_URL = environment.filePath;

  IsChecked: boolean;
  IsIndeterminate: boolean;
  LabelAlign: string;
  IsDisabled: boolean;

  private _checkBoxChecked = false;

  get checkBoxChecked() {
    return this._checkBoxChecked;
  }

  set checkBoxChecked(val: any) {
    this._checkBoxChecked = val;
    this.actionForm.get('status').setValue(this.checkBoxChecked);
  }

  get vessels(): string | null {
    const vesselTypes = this.localStorageService.vessels;
    return vesselTypes ? vesselTypes.vesselType : null;
  }

  get getIncidentSeqControls(): FormArray {
    return this.incidentSeqDataForm.get('incidentSeqData') as FormArray;
  }

  get f() {
    return this.attachmentFormGroup.controls;
  }

  constructor(
    private _formBuilder: FormBuilder,
    private localStorageService: LocalStorageService,
    private cacheServiceService: CacheServiceService,
    private userProfileService: UserService,
    private nearMissService: NearMissService,
    private incidentService: IncidentService,
    private incidentCacheService: IncidentCacheService,
    private route: ActivatedRoute,
    private notifyService: NotificationService,
    private helperService: HelperService,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.IsChecked = false;
    this.IsIndeterminate = false;
    this.LabelAlign = 'after';
    this.IsDisabled = false;
    this.tomorrow.setDate(this.tomorrow.getDate());
  }

  ngOnInit(): void {
    const paramValue = 'value';
    this.getSeverityLevel('Severity Level');
    if (this.route.params[paramValue].id) {
      this.incidentId = this.route.params[paramValue].id;
      this.getIncidentById(this.incidentId);
    }

    this.levelName = 'level3';

    this.createOverviewForm();
    this.createDescriptionForm();
    this.createAnalysisForm();
    this.createCrossReferencing();
    this.createAction();
    this.investigationForm();
    this.contactEstForm();
    this.incidentSeqForm();
    this.incidentVdrForm();
    this.postRecForm();
    this.notifyForm();
    this.breachForm();
    this.backgroundForm();
    this.communicationForm();

    this.getNearMissContactorEvent();
    this.getNearMissCauseTree();
    this.getNearMissPortList();
    this.userprofile();
    this.getNearMissTastList();
    this.getNearMissLocationList();
    this.getBackgroundCondition();
    this.getvessels();
    this.getEventImpact();
    this.getInjuryOccuredBy();
    this.getInjuryCategory();
    this.getRankList();
    this.getByPosition();
    this.getInvestigation();
    this.getBodyPartEffect();
    this.getNationality();
    this.getAllUsers();
    this.getEquipment();
    this.getEquipmentCategory();
    this.getAnalysisOfSimilarIncident();
    this.getDesignation();
    this.getCrossReferenceTMSA();
    this.getElement();
    this.getStageData();
    this.resposibilityGroup();
    this.getAllTypeList();
    this.getProbability('Probablity Of Happening');
    this.getAllCompany();

    // this.getCreateBackgroundField();
  }

  _onChangeStopCardIssued = (_: any) => {};

  _onChange = (_: any) => {};

  _onChangeType = (_: any) => {};

  _onChangeImpact = (_: any) => {};

  _onChangeSeverity = (_: any) => {};

  _onChangeprobability = (_: any) => {};

  _onChangeLatLong = (_: any) => {};

  onChange(event: any) {
    this._onChange(this.vesselLocValue);
  }

  onChangeType(event: any) {
    this._onChangeType(this.innerType);
  }

  // onChangeImpact(event: any) {
  //   this._onChangeImpact(this.impactValue);
  // }

  onChangeImpact(eventImpact: string, isChecked: boolean) {
    const impactFormArray = this.descriptionFormGroup.controls.eventImpact as FormArray;
    if (isChecked) {
      impactFormArray.push(new FormControl(eventImpact));
      this.impactSelectedArray.push(eventImpact);

      this.getSeverityOfConsequences();
    } else {
      const index = impactFormArray.controls.findIndex((x) => x.value === eventImpact);
      impactFormArray.removeAt(index);

      const impactIndex = this.impactSelectedArray.indexOf(eventImpact);
      this.impactSelectedArray.splice(impactIndex, 1);

      this.getSeverityOfConsequences();
    }
  }

  setImpactType(type: any) {
    const availableType = this.impactSelectedArray.indexOf(type);
    if (availableType > -1) {
      return true;
    } else {
      return false;
    }
  }

  getSelectedImpact(eventImpact: string) {
    const impactIndex = this.impactSelectedArray.indexOf(eventImpact);
    if (impactIndex > -1) {
      return true;
    } else {
      return false;
    }
  }

  getImpactActiveClass() {
    if (this.impact < 5) {
      this.impactLabel = 'Low';
      return 'normal';
    } else if (this.impact > 4 && this.impact < 10) {
      this.impactLabel = 'Medium';
      return 'medium';
    } else {
      this.impactLabel = 'High';
      return 'extreme';
    }
  }

  getImpactType(type: any) {
    if (type === 'Injury') {
      this.additionInjuryFlag = true;
      this.eqpDamageFlag = false;
      this.vdrFlag = false;
      this.descriptionFormGroup.get(`isAddlInfoInvolvingInjury`).patchValue(1);
      this.descriptionFormGroup.get(`isAddlInfoInvolvingEqpDamage`).patchValue(0);
      this.descriptionFormGroup.get(`isIncidentVdrData`).patchValue(1);
    } else if (type === 'Equipment or Machinery Damage') {
      this.additionInjuryFlag = false;
      this.eqpDamageFlag = true;
      this.vdrFlag = false;
      this.descriptionFormGroup.get(`isAddlInfoInvolvingInjury`).patchValue(0);
      this.descriptionFormGroup.get(`isAddlInfoInvolvingEqpDamage`).patchValue(1);
      this.descriptionFormGroup.get(`isIncidentVdrData`).patchValue(1);
    } else {
      this.additionInjuryFlag = false;
      this.eqpDamageFlag = false;
      this.vdrFlag = false;
      this.descriptionFormGroup.get(`isAddlInfoInvolvingInjury`).patchValue(1);
      this.descriptionFormGroup.get(`isAddlInfoInvolvingEqpDamage`).patchValue(1);
      this.descriptionFormGroup.get(`isIncidentVdrData`).patchValue(1);
    }
  }

  onChangeLatLong(latlongValue: any) {
    this.isLatLong = !this.isLatLong;
    if (!this.isLatLong) {
      this.isLatLong = false;
      // this.portFieldValidate = true;
      this.overViewFormGroup.value.isLatLong = 0;
      this.LatLongLabel = 'Lat Long';
      this.vesselLocationGroups = [
        {
          name: 'Port',
          vesselLocation: [
            { value: 'In Port', viewValue: 'In Port' },
            { value: 'Anchorage', viewValue: 'Anchorage' },
            { value: 'Pilotage', viewValue: 'Pilotage' },
            { value: 'Shipyard', viewValue: 'Shipyard' },
            { value: 'Offshore', viewValue: 'Offshore' },
          ],
        },
      ];
    } else {
      this.isLatLong = true;
      // this.portFieldValidate = false;
      this.overViewFormGroup.value.isLatLong = 1;
      this.LatLongLabel = 'Port';
      this.vesselLocationGroups = [
        {
          name: 'At Sea',
          vesselLocation: [{ value: 'Open Sea', viewValue: 'Open Sea' }],
        },
      ];
    }
  }

  move(index: number) {
    this.stepper.selectedIndex = index;
    this.activeIndex = index + 1;
    // this.validateSteppper(index);
  }

  getStepDone(index: number) {
    if (this.formStepErrorArray.indexOf(index) > -1) {
      const primary = 'warning';
      return primary;
    } else if (index < this.activeIndex) {
      const primary = 'primary';
      return primary;
    }
  }

  radioChangeImpact(event: any) {}

  onSeverityLevel(event: any) {
    const probabilityId = event.source.id;
    const levelIndex = probabilityId.substr(-1, 1);
    this.levelName = 'level' + levelIndex;
    this.severity = 5 - levelIndex;
    this.impact = this.severity * this.probabilityImpact;
    this.analysisFormGroup.value.resultantRisk = this.impact;
    this.getSeverityOfConsequences();
  }

  onChangeIncidentDate(event: any) {
    const date = event.value;
    this.reportDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    this.reportDate.setDate(this.reportDate.getDate());
  }

  onChangeseverity(event: any, severityIndex: any) {}

  onChangeprobability(event: any) {
    const probabilityId = event.source.id;
    const value = probabilityId.substr(-1, 1);
    this.probabilityImpact = value;
    this.impact = this.severity * this.probabilityImpact;
    this.analysisFormGroup.value.resultantRisk = this.impact;
  }

  onChangeStopCardIssued(event: any) {
    this._onChangeStopCardIssued(this.stopCardIssuedValue);
    if (this.stopCardIssuedValue === 'no') {
      this.readonly = true;
    } else {
      this.readonly = !this.readonly;
    }
  }

  resposibilityGroup() {
    this.stateGroups = [
      {
        letter: 'Designation',
        names: this.designationData,
      },
      {
        letter: 'Users',
        names: this.options,
      },
      {
        letter: 'Others',
        names: [{ name: 'Other', uid: 'Other' }],
      },
    ];
  }

  actionResponsible(event: any, index: any) {
    const other = event.value;
    const otherIndex = this.resposibilityArray.indexOf(index);
    if (otherIndex > -1) {
      this.resposibilityArray.splice(otherIndex, 1);
    } else {
      if (other === 'Other') {
        this.resposibilityArray.push(index);
      }
    }
  }

  getEmailAvailable(index: any) {
    const otherIndex = this.resposibilityArray.indexOf(index);
    if (otherIndex > -1) {
      return true;
    } else {
      return false;
    }
  }

  getltTimeOfIncident(event: any) {
    // const dateTime = event.value;
    // const ltTimeOfIncident = this.helperService.getLocalTime(dateTime);
    // this.overViewFormGroup.get(`ltTimeOfIncident`).patchValue(ltTimeOfIncident);
  }

  setIncidentData() {
    // this.attachmentFormGroup.patchValue(this.incidentData);
    this.isLatLong = !this.incidentData.isLatLong;
    this.onChangeLatLong(this.isLatLong);
    this.setAnalysisForm();
    this.customUpdateFieldDataSet();
    this.setFormData();
    this.seqOfEventAttachments = this.incidentData.seqOfEventAttachments;
    this.actionAttachments = this.incidentData.actionAttachments;
    this.reviewOfVdrDataAttachments = this.incidentData.reviewOfVdrDataAttachments;
    this.postRecoveryMeasuresAttachments = this.incidentData.postRecoveryMeasuresAttachments;
    this.breachAttachments = this.incidentData.breachAttachments;
    const levelValue = this.incidentData.severityLevel;
    const levelIndex = this.severityLevel2.indexOf(levelValue);
    this.levelName = 'level' + levelIndex;
    this.impact = this.incidentData.resultantRisk;
    this.severityValue = this.incidentData.severityLevel;

    this.additionInjuryFlag = this.incidentData.isAddlInfoInvolvingInjury;
    this.eqpDamageFlag = this.incidentData.isAddlInfoInvolvingEqpDamage;
    this.vdrFlag = this.incidentData.isIncidentVdrData;

    this.impactSelectedArray = [];
    if (this.incidentData.eventImpact) {
      const impactOn = this.incidentData.eventImpact;
      impactOn.forEach((element: any) => {
        this.impactSelectedArray.push(element.eventImpact);
      });
    }
    this.getSeverityOfConsequences();
  }

  setFormData() {
    const data = this.incidentData;
    // tslint:disable-next-line:forin
    for (const dataKey in data) {
      if (dataKey === 'dateOfIncident') {
        this.helperService.getFullDate(this.incidentData.dateOfIncident);
      } else if (dataKey === 'timeOfIncident') {
        const ltTimeOfIncident = this.helperService.getLocalTime(this.incidentData.timeOfIncident);
        this.overViewFormGroup.get(`ltTimeOfIncident`).patchValue(ltTimeOfIncident);
      } else if (dataKey === 'intContact') {
        const intContact = this.incidentData.intContact;
        let index = 0;
        intContact.forEach((element: any) => {
          if (index > 0) {
            this.onAddIntContact();
          }
          index++;
        });
      } else if (dataKey === 'extContact') {
        const extContact = this.incidentData.extContact;
        let index = 0;
        extContact.forEach((element: any) => {
          if (index > 0) {
            this.onAddExtContact();
          }
          index++;
        });
      } else if (dataKey === 'breachData') {
        const breachData = this.incidentData.breachData.breachReqData;
        let index = 0;
        breachData.forEach((element: any) => {
          if (index > 0) {
            this.onAddBreachData();
          }
          index++;
        });
      } else if (dataKey === 'crossRefToTMSA') {
        const crossRefToTMSA = this.incidentData.crossRefToTMSA;
        let index = 0;
        if (crossRefToTMSA) {
          crossRefToTMSA.forEach((element: any) => {
            const elementId = this.incidentData.crossRefToTMSA[index].elementId;
            const stageId = this.incidentData.crossRefToTMSA[index].stageId;
            this.incidentData.crossRefToTMSA[index].specificKpiId = stageId;
            this.getFilterStages(elementId, index);
            this.getFilterSpecificKPI(stageId, index);
            if (index > 0) {
              this.onAddItem();
            }
            index++;
          });
          this.crossReferenceForm.patchValue(this.incidentData);
        }
      } else if (dataKey === 'backgroundData') {
        const backgroundData = this.incidentData.backgroundData;
        let index = 0;
        backgroundData.forEach((element: any) => {
          const backgroundCondition = element.backgroundCondition;
          const backgroundIndex = this.bgImage.indexOf(backgroundCondition);
          if (backgroundIndex > -1) {
            this.backgroundDataForm
              .get(`backgroundData.${backgroundIndex}.description`)
              .patchValue(element.description);
            this.backgroundDataForm
              .get(`backgroundData.${backgroundIndex}.backgroundId`)
              .patchValue(element.backgroundId);
            this.backgroundDataForm
              .get(`backgroundData.${backgroundIndex}.attachmentFieldName`)
              .patchValue(element.attachmentFieldName);
            this.backgroundDataForm
              .get(`backgroundData.${backgroundIndex}.backgroundCondition`)
              .patchValue(element.backgroundCondition);
            this.backgroundDataForm
              .get(`backgroundData.${backgroundIndex}.isContibutingFactor`)
              .patchValue(element.isContibutingFactor);
            this.backgroundDataForm
              .get(`backgroundData.${backgroundIndex}.backgroundConditionId`)
              .patchValue(element.backgroundConditionId);
          } else {
            if (index > 0) {
              this.onAddBackgroundData();
            }
            this.bgImage.push(element.backgroundCondition);
            if (element.backgroundCondition) {
              this.newBackgroundCondition.push(element.backgroundCondition);
            }

            this.backgroundDataForm.get(`backgroundData.${index}.description`).patchValue(element.description);
            this.backgroundDataForm.get(`backgroundData.${index}.backgroundId`).patchValue(element.backgroundId);
            this.backgroundDataForm
              .get(`backgroundData.${index}.attachmentFieldName`)
              .patchValue(element.attachmentFieldName);
            this.backgroundDataForm
              .get(`backgroundData.${index}.backgroundCondition`)
              .patchValue(element.backgroundCondition);
            this.backgroundDataForm
              .get(`backgroundData.${index}.isContibutingFactor`)
              .patchValue(element.isContibutingFactor);
            this.backgroundDataForm
              .get(`backgroundData.${index}.backgroundConditionId`)
              .patchValue(element.backgroundConditionId);
          }
          index++;
        });
      } else if (dataKey === 'addlInfoInvolvingInjury') {
        const addlInfoInvolvingInjury = this.incidentData.addlInfoInvolvingInjury;
        let index = 0;
        if (addlInfoInvolvingInjury) {
          addlInfoInvolvingInjury.forEach((element: any) => {
            if (index > 0) {
              this.onAddIInfoInvolving();
            }
            index++;
          });
          this.descriptionFormGroup.get(`addlInfoInvolvingInjury`).patchValue(addlInfoInvolvingInjury);
        }
      } else if (dataKey === 'addlInfoInvolvingEqpDamage') {
        const addlInfoInvolvingEqpDamage = this.incidentData.addlInfoInvolvingEqpDamage;
        let index = 0;
        if (addlInfoInvolvingEqpDamage) {
          addlInfoInvolvingEqpDamage.forEach((element: any) => {
            if (index > 0) {
              this.onAddInfoDamage();
            }
            index++;
          });
          this.descriptionFormGroup.get(`addlInfoInvolvingEqpDamage`).patchValue(addlInfoInvolvingEqpDamage);
        }
      } else if (dataKey === 'action') {
        const action = this.incidentData.action;
        let index = 0;
        if (action) {
          action.forEach((element: any) => {
            const convertedDate = moment(this.incidentData.action[index].dateCompleted).startOf('day');
            const observationYear = moment(convertedDate).format('YYYY-MM-DD');
            if (observationYear !== 'Invalid date') {
              this.incidentData.action[index].dateCompleted = observationYear;
            }

            const dueDates = moment(this.incidentData.action[index].dueDate).startOf('day');
            const dueDate = moment(dueDates).format('YYYY-MM-DD');
            if (dueDate !== 'Invalid date') {
              this.incidentData.action[index].dueDate = dueDate;
            }

            const status = this.incidentData.action[index].status;
            if (!status) {
              this.incidentData.action[index].status = 'Open';
            }

            const proposedBy = this.incidentData.action[index].proposedBy;
            if (proposedBy === 'Officer') {
              this.incidentData.action[index].status = status;
              this.onChangeProposedBy(proposedBy, index);
            }

            const type = this.incidentData.action[index].type;
            if (type === 'Immediate Action') {
              this.changeAction(type, index);
            }

            const otherOption = this.incidentData.action[index].designationId;
            const email = this.incidentData.action[index].email;
            if (otherOption === null) {
              if (email) {
                this.resposibilityArray.push(index);
                this.incidentData.action[index].designationId = 'Other';
                this.incidentData.action[index].email = email[0];
              }
            }

            if (index > 0) {
              this.onAddAtionField();
            }
            index++;
          });
          this.actionForm.patchValue(this.incidentData);
        }
      } else if (dataKey === 'analysisData') {
        const analysisData = this.incidentData.analysisData;
        const analysisForm = this.analysisFormGroup.get(`analysisData`) as FormArray;

        let index = 0;
        analysisData.forEach((element: any) => {
          if (index > 0) {
            this.onAddAnalysisData();
          }
          index++;
        });
        analysisForm.setValue(analysisData);
      } else if (dataKey === 'notifyData') {
        const notifyData = this.incidentData.notifyData;
        const notificationDate = notifyData.notificationDate;
        let index = 0;
        notificationDate.forEach((element: any) => {
          if (index > 0) {
            this.onAddNotification();
            const notificationData = notificationDate[index].notificationData;
            let index1 = 0;
            notificationData.forEach((elements: any) => {
              if (index1 > 0) {
                this.onAddNotifyData(index);
              }
              index1++;
            });
          } else {
            const notificationData = notificationDate[index].notificationData;
            let index1 = 0;
            notificationData.forEach((elements: any) => {
              if (index1 > 0) {
                this.onAddNotifyData(index);
              }
              index1++;
            });
          }
          index++;
        });
        this.notifyDataForm.patchValue(this.incidentData);
      } else if (dataKey === 'investigationTeam') {
        const investigationTeam = this.incidentData.investigationTeam;
        const investigationTeamMember = investigationTeam.investigationTeamMember;
        let index = 0;
        investigationTeamMember.forEach((element: any) => {
          if (index > 0) {
            this.addTeamMemberForm();
          }
          index++;
        });

        const reportPreparedBy = investigationTeam.reportPreparedBy;
        let index1 = 0;
        reportPreparedBy.forEach((element: any) => {
          if (index1 > 0) {
            this.addReportPreparedByForm();
          }
          index1++;
        });

        const reportReviewedBy = investigationTeam.reportReviewedBy;
        let index2 = 0;
        reportReviewedBy.forEach((element: any) => {
          if (index2 > 0) {
            this.addReportReviewedByForm();
          }
          index2++;
        });

        this.investigationTeamForm.patchValue(this.incidentData);
      } else if (dataKey === 'incidentSeqData') {
        const incidentSeqData = this.incidentData.incidentSeqData;
        let index = 0;
        incidentSeqData.forEach((element: any) => {
          if (index > 0) {
            this.onAddIncidentSeq();
            const seqData = incidentSeqData[index].seqData;
            let index1 = 0;
            seqData.forEach((elements: any) => {
              if (index1 > 0) {
                this.onAddSeqData(index);
              }
              index1++;
            });
          } else {
            const seqData = incidentSeqData[index].seqData;
            let index1 = 0;
            seqData.forEach((elements: any) => {
              if (index1 > 0) {
                this.onAddSeqData(index);
              }
              index1++;
            });
          }
          index++;
        });
        this.incidentSeqDataForm.patchValue(this.incidentData);
      } else if (dataKey === 'incidentVdrData') {
        const incidentVdrData = this.incidentData.incidentVdrData;
        if (incidentVdrData) {
          const reviewVdrDate = incidentVdrData.reviewVdrDate;
          let index = 0;
          reviewVdrDate.forEach((element: any) => {
            if (index > 0) {
              this.onAddReviewVdr();
              const reviewVdrData = reviewVdrDate[index].reviewVdrData;
              let index1 = 0;
              reviewVdrData.forEach((elements: any) => {
                if (index1 > 0) {
                  this.onAddReviewVdrData(index);
                }
                index1++;
              });
            } else {
              const reviewVdrData = reviewVdrDate[index].reviewVdrData;
              let index1 = 0;
              reviewVdrData.forEach((elements: any) => {
                if (index1 > 0) {
                  this.onAddReviewVdrData(index);
                }
                index1++;
              });
            }
            index++;
          });
          this.incidentVdrDataForm.patchValue(this.incidentData);
        }
      } else if (dataKey === 'postRecData') {
        const postRecData = this.incidentData.postRecData;
        const postRecoveryDate = postRecData.postRecoveryDate;
        let index = 0;
        postRecoveryDate.forEach((element: any) => {
          if (index > 0) {
            this.onAddPostRecovery();
            const postRecoveryData = postRecoveryDate[index].postRecoveryData;
            let index1 = 0;
            postRecoveryData.forEach((elements: any) => {
              if (index1 > 0) {
                this.onAddPostRecoveryData(index);
              }
              index1++;
            });
          } else {
            const postRecoveryData = postRecoveryDate[index].postRecoveryData;
            let index1 = 0;
            postRecoveryData.forEach((elements: any) => {
              if (index1 > 0) {
                this.onAddPostRecoveryData(index);
              }
              index1++;
            });
          }
          index++;
        });
        this.postRecDataForm.patchValue(this.incidentData);
      }
    }
    this.overViewFormGroup.patchValue(this.incidentData);
    this.breachDataForm.patchValue(this.incidentData);
    this.contactEstablishForm.patchValue(this.incidentData);
    this.setCommunication();
    // this.descriptionFormGroup.patchValue(this.incidentData);
  }

  setCommunication() {
    const data = this.incidentData;
    for (const dataKey in data) {
      if (dataKey === 'communication') {
        const communication = this.incidentData.communication;
        let index = 0;
        if (communication) {
          communication.forEach((element: any) => {
            if (index > 0) {
              this.onAddCommunicationInvestigation();
            }
            this.communicationDataForm
              .get(`communication.${index}.communicationId`)
              .patchValue(element.communicationId);
            this.communicationDataForm.get(`communication.${index}.commentBy`).patchValue(element.commentBy);
            this.communicationDataForm.get(`communication.${index}.comment`).patchValue(element.comment);
            index++;
          });
        }
      }
    }
  }

  setAnalysisForm() {
    const analysisData = this.incidentData.typeOfContactOrEvent;
    if (analysisData) {
      analysisData.forEach((element: any) => {
        const typeOfContactId = element.typeOfCntctId;
        let typeContactIndex = 0;
        this.contactOrEventOptionList.forEach((elements: any) => {
          if (elements.typeOfContactId === typeOfContactId) {
            this.getBasicImmediateCause(elements, typeContactIndex + 1);
            const immedateData = this.basicImmidateOptionList[typeContactIndex + 1];
            const basicImmediateCause = element.basicImmediateCause;
            basicImmediateCause.forEach((immedaiteElements: any) => {
              let basicImmediateIndex = 0;
              const immdediateCauseId = immedaiteElements.basicImmediateCauseId;
              immedateData.forEach((immedaiteElement: any) => {
                const immedaiteData = immedaiteElement;

                if (immdediateCauseId === immedaiteData.immediateCauseId) {
                  this.getRootCause(immedaiteElement, elements, basicImmediateIndex, typeContactIndex + 1);

                  const basicRootCause = immedaiteElements.basicRootCause;
                  basicRootCause.forEach((basicRootCauseElements: any) => {
                    const basicRootCauseId = basicRootCauseElements.basicRootCauseId;
                    let rootIndex = 0;

                    this.rootCauseOptionList[immdediateCauseId].forEach((basicRootCauseElement: any) => {
                      if (basicRootCauseId === basicRootCauseElement.basicRootCauseId) {
                        this.getCorrectiveCause(
                          elements,
                          basicRootCauseElement,
                          immedaiteElement,
                          basicImmediateIndex,
                          rootIndex,
                          typeContactIndex + 1
                        );

                        const correctiveAction = basicRootCauseElements.correctiveActionId;
                        correctiveAction.forEach((correctiveActionElements: any) => {
                          const correctiveActionId = correctiveActionElements.correctiveActionId;
                          let correctiveIndex = 0;

                          this.correctiveCauseOptionList[basicRootCauseId].forEach((correctiveActionElement: any) => {
                            if (correctiveActionId === correctiveActionElement.correctiveActionId) {
                              this.setCorrectiveCause(
                                elements,
                                immedaiteElement,
                                basicRootCauseElement,
                                correctiveActionElement,
                                rootIndex,
                                basicImmediateIndex,
                                typeContactIndex + 1
                              );
                            }
                            correctiveIndex++;
                          });
                        });
                      }
                      rootIndex++;
                    });
                  });
                }
                basicImmediateIndex++;
              });
            });
          }
          typeContactIndex++;
        });
      });
    }
  }

  customUpdateFieldDataSet() {
    // this.overViewFormGroup.value.timeOfIncident = dateOfExpiry;
    this.vesselLocValue = this.incidentData.vesselLocation;
    this.severityValue = this.incidentData.severityLevel;

    const convertedDate = moment(this.incidentData.timeOfIncident).startOf('day');
    // tslint:disable-next-line: radix
    const dateY = parseInt(moment(convertedDate).format('YYYY'));
    // tslint:disable-next-line: radix
    const dateM = parseInt(moment(convertedDate).format('MM'));
    // tslint:disable-next-line: radix
    const dateD = parseInt(moment(convertedDate).format('DD'));
    // tslint:disable-next-line: radix
    const dateH = parseInt(moment(convertedDate).format('HH'));
    // tslint:disable-next-line: radix
    const dateMM = parseInt(moment(convertedDate).format('mm'));
    // log.debug('dateOfExpiry: ', dateOfExpiry);
    this.timeOfIncident = new Date(dateY, dateM, dateD, dateH, dateMM);
    // this.overViewFormGroup.value.timeOfIncident = new Date(2018, 3, 21, 20, 30);
    // log.debug('this.incidentData>>: ', this.overViewFormGroup.value.timeOfIncident);
  }

  changeValue(value: any) {
    this.checked = !value;
  }

  userprofile() {
    this.currentUser = this.userProfileService.currentUser;
  }

  /**  Reset stepper Form Function :: start */

  analysisReset() {
    this.contactOrEventOptionActive = [];
    this.basicImmidateOptionActive = [];
    this.rootCauseOptionActive = [];
    this.correctiveCauseOptionActive = [];
    this.typeOfContactArray = [];
    this.basicCauseArray = [];
    this.rootCauseArray = [];
    this.correctiveActionId = '';
    this.activeRootCause = '';
    this.activeImmediateCause = '';
    this.activeContactCause = '';
    this.analysisFormGroup.reset();
    this.incidentCacheService.setAnalysisData('');
  }

  analysisChartReset() {
    this.contactOrEventOptionActive = [];
    this.basicImmidateOptionActive = [];
    this.rootCauseOptionActive = [];
    this.correctiveCauseOptionActive = [];
    this.typeOfContactArray = [];
    this.basicCauseArray = [];
    this.rootCauseArray = [];
    this.correctiveActionId = '';
    this.activeRootCause = '';
    this.activeImmediateCause = '';
    this.activeContactCause = '';
    const form = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
    form.clear();
  }

  descriptionReset() {
    this.descriptionFormGroup.reset();
    this.incidentCacheService.setAnalysisData('');
  }

  attachmentReset() {
    this.attachmentFormGroup.reset();
  }

  crossReferenceReset() {
    this.crossReferenceForm.reset();
    this.incidentCacheService.setCrossReferenceData('');
  }

  actionReset() {
    this.actionForm.reset();
    this.incidentCacheService.setActionData('');
  }

  overviewReset() {
    this.overViewFormGroup.reset();
    this.incidentCacheService.setOverviewData('');
  }

  seqDataReset() {
    this.incidentSeqDataForm.reset();
    this.incidentCacheService.setIncidentSeqData();
  }

  reviewVdrReset() {
    this.incidentVdrDataForm.reset();
    this.incidentCacheService.setIncidentVdrData('');
  }

  notifyReset() {
    this.notifyDataForm.reset();
    this.incidentCacheService.setNotifyData('');
  }

  postRecReset() {
    this.postRecDataForm.reset();
    this.incidentCacheService.setPostRecData('');
  }

  breachReset() {
    this.breachDataForm.reset();
    this.incidentCacheService.setBreachData('');
  }

  investigationReset() {
    this.investigationTeamForm.reset();
    this.incidentCacheService.setInvestigationData('');
  }

  contactEstReset() {
    this.contactEstablishForm.reset();
    this.incidentCacheService.setContactEstablishData('');
  }

  backgroundDataFormReset() {
    this.backgroundDataForm.reset();
    this.backgroundImage = [];
  }

  openDialog(flag: any, index: any): void {
    const dialogRef = this.dialog.open(IncidentDailogComponent, {
      width: '400px',
      data: { name: this.name, natureOfAction: this.natureOfAction, stopCardFlag: flag },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.natureOfAction = result;
      this.setActionComment(result, index);
    });
  }

  setActionComment(natureOfAction: any, index: any) {
    this.actionForm.get(`action.${index}.supportiveComments`).patchValue(natureOfAction);
  }

  /**  Reset stepper Form Function :: end */

  onSubmitOverview() {
    // if (this.overViewFormGroup.invalid) {
    //   return;
    // }
    this.overViewFormGroup.value.userId = this.currentUser.userId;
    // this.incidentCacheService.setOverviewData(this.overViewFormGroup.value);
    this.communicationDataForm.patchValue(this.overViewFormGroup.value);
  }

  onSubmitDescription() {
    this.communicationDataForm.patchValue(this.descriptionFormGroup.value);
    // this.incidentCacheService.setDescriptionData(this.descriptionFormGroup.value);
  }

  onSubmitInvestigation() {
    this.communicationDataForm.patchValue(this.investigationTeamForm.value);
    // this.incidentCacheService.setInvestigationData(this.investigationTeamForm.value);
  }

  onSubmitContactEst() {
    this.communicationDataForm.patchValue(this.contactEstablishForm.value);
    // this.incidentCacheService.setContactEstablishData(this.contactEstablishForm.value);
  }

  onSubmitAnalysis() {
    log.debug('this.analysisFormGroup.value: ', this.analysisFormGroup.value);
    this.communicationDataForm.patchValue(this.analysisFormGroup.value);
  }

  addTypeofContact(index: number) {
    this.typeContactView = !this.typeContactView;
    if (!this.typeContactView) {
      setTimeout(() => {
        this.move(index);
        this.activeIndex = index + 1;
      }, 1);
    }
  }

  onSubmitAnalysisChart() {
    this.incidentCacheService.setAnalysisData(this.analysisFormGroup.value);
    this.analysisData = this.incidentCacheService.analysisForm.typeOfContactOrEvent;
    this.addTypeofContact(10);
  }

  analysisRest() {
    this.analysisFormGroup.reset();
  }

  onSubmitCrossReference() {
    this.communicationDataForm.patchValue(this.crossReferenceForm.value);
    // this.incidentCacheService.setCrossReferenceData(this.crossReferenceForm.value);
  }

  onSubmitActionForm() {
    this.communicationDataForm.patchValue(this.actionForm.value);
    // this.incidentCacheService.setActionData(this.actionForm.value);
  }

  onSubmitIncidentSeqDataForm() {
    this.communicationDataForm.patchValue(this.incidentSeqDataForm.value);
    // this.incidentCacheService.setIncidentSeqData(this.incidentSeqDataForm.value);
  }

  onSubmitIncidentVdr() {
    this.communicationDataForm.patchValue(this.incidentVdrDataForm.value);
    // this.incidentCacheService.setIncidentVdrData(this.incidentVdrDataForm.value);
  }

  onSubmitnotifyData() {
    this.communicationDataForm.patchValue(this.notifyDataForm.value);
    // this.incidentCacheService.setNotifyData(this.notifyDataForm.value);
  }

  onSubmitpostRec() {
    this.communicationDataForm.patchValue(this.postRecDataForm.value);
    // this.incidentCacheService.setPostRecData(this.postRecDataForm.value);
  }

  onSubmitBreach() {
    this.communicationDataForm.patchValue(this.breachDataForm.value);
    // this.incidentCacheService.setBreachData(this.breachDataForm.value);
  }

  onSubmitBackground() {}

  onSubmitCommunicationForm() {
    this.communicationDataForm.patchValue(this.backgroundDataForm.value);
    this.communicationDataForm.patchValue(this.descriptionFormGroup.value);
    const data = this.communicationDataForm.value;
    const latitude = this.overViewFormGroup.value.latitude;
    const longitude = this.overViewFormGroup.value.longitude;
    const impactData = this.descriptionFormGroup.value.eventImpact;

    const latLong = latitude + ', ' + longitude;

    const formData: FormData = new FormData();
    let sequenceCount = 0;
    // tslint:disable-next-line:forin
    for (const dataKey in data) {
      if (dataKey === 'incidentSeqData') {
        // append nested object
        // const datas = data[dataKey];
        const incidentSeqData = this.incidentSeqDataForm.value.incidentSeqData;
        let index = 0;
        if (incidentSeqData instanceof Array) {
          incidentSeqData.forEach((element) => {
            for (const previewKey in element) {
              if (previewKey === 'seqData') {
                const seqData = incidentSeqData[index][previewKey];
                let index2 = 0;
                seqData.forEach((elements: any) => {
                  // tslint:disable-next-line:forin
                  for (const seqDat in elements) {
                    const seData = incidentSeqData[index][previewKey][index2][seqDat];
                    if (seqDat === 'triggeringFactors') {
                      if (seData === true) {
                        formData.append(`incidentSeqData[${index}][${previewKey}][${index2}][${seqDat}]`, '1');
                      } else {
                        formData.append(`incidentSeqData[${index}][${previewKey}][${index2}][${seqDat}]`, '0');
                      }
                    } else if (seqDat === 'ltTime') {
                      this.ltTimeInt = '';
                      const ltTimeFormatting = moment(seData).format('HH:mm');
                      this.ltTimeInt = parseInt(ltTimeFormatting.replace(':', ''), 10);
                      formData.append(`incidentSeqData[${index}][${previewKey}][${index2}][${seqDat}]`, this.ltTimeInt);
                    } else {
                      formData.append(`incidentSeqData[${index}][${previewKey}][${index2}][${seqDat}]`, seData);
                    }
                  }
                  index2++;
                });
              } else if (previewKey === 'date') {
                const dateOfExpiry = this.helperService.getFullDate(incidentSeqData[index][previewKey]);
                if (dateOfExpiry) {
                  formData.append(`incidentSeqData[${index}][${previewKey}]`, dateOfExpiry);
                }
              } else {
                formData.append(`incidentSeqData[${index}][${previewKey}]`, incidentSeqData[index][previewKey]);
              }
            }
            index++;
          });
        }

        for (let index5 = 0; index5 < this.images.length; index5++) {
          formData.append(`seqOfEventAttachments[${index5}]`, this.images[index5]);
        }
      } else if (dataKey === 'probablityReccData') {
        // append nested object
        const datas = data[dataKey];
        for (const previewKey in datas) {
          if (data[dataKey][previewKey]) {
            formData.append(`probablityReccData[${previewKey}]`, data[dataKey][previewKey]);
          }
        }
      } else if (dataKey === 'addlInfoInvolvingInjury') {
        // append nested object
        const addlInfoInvolvingInjury = this.descriptionFormGroup.value.addlInfoInvolvingInjury;
        let index = 0;
        if (addlInfoInvolvingInjury instanceof Array) {
          addlInfoInvolvingInjury.forEach((element) => {
            // tslint:disable-next-line:forin
            for (const previewKey in element) {
              formData.append(
                `addlInfoInvolvingInjury[${index}][${previewKey}]`,
                addlInfoInvolvingInjury[index][previewKey]
              );
            }
            index++;
          });
        }
      } else if (dataKey === 'addlInfoInvolvingEqpDamage') {
        // append nested object
        const addlInfoInvolvingEqpDamage = this.descriptionFormGroup.value.addlInfoInvolvingEqpDamage;
        let index = 0;
        if (addlInfoInvolvingEqpDamage instanceof Array) {
          addlInfoInvolvingEqpDamage.forEach((element: any) => {
            log.debug('element: ', element);
            for (const previewKey in element) {
              if (element.isOther) {
                if (previewKey === 'equipmentId') {
                } else if (previewKey === 'isOther') {
                  formData.append(`addlInfoInvolvingEqpDamage[${index}][${previewKey}]`, '1');
                } else {
                  formData.append(
                    `addlInfoInvolvingEqpDamage[${index}][${previewKey}]`,
                    addlInfoInvolvingEqpDamage[index][previewKey]
                  );
                }
              } else {
                if (previewKey === 'newEquipment') {
                } else if (previewKey === 'isOther') {
                  formData.append(`addlInfoInvolvingEqpDamage[${index}][${previewKey}]`, '0');
                } else {
                  formData.append(
                    `addlInfoInvolvingEqpDamage[${index}][${previewKey}]`,
                    addlInfoInvolvingEqpDamage[index][previewKey]
                  );
                }
              }
            }
            index++;
          });
        }
      } else if (dataKey === 'investigationTeam') {
        const investigationTeam = this.investigationTeamForm.value.investigationTeam;
        for (const previewKey in investigationTeam) {
          if (previewKey === 'investigationTeamMember') {
            const investigationTeamMember = investigationTeam[previewKey];
            let index2 = 0;
            investigationTeamMember.forEach((elements: any) => {
              // tslint:disable-next-line:forin
              for (const teamMember in elements) {
                if (teamMember === 'teamMemberId') {
                  formData.append(
                    `investigationTeam[${previewKey}][${index2}]`,
                    investigationTeam[previewKey][index2][teamMember]
                  );
                }
              }
              index2++;
            });
          } else if (previewKey === 'reportPreparedBy') {
            const investigationTeamMember = investigationTeam[previewKey];
            let index2 = 0;
            investigationTeamMember.forEach((elements: any) => {
              // tslint:disable-next-line:forin
              for (const teamMember in elements) {
                if (teamMember === 'reportPreparedById') {
                  formData.append(
                    `investigationTeam[${previewKey}][${index2}]`,
                    investigationTeam[previewKey][index2][teamMember]
                  );
                }
              }
              index2++;
            });
          } else if (previewKey === 'reportReviewedBy') {
            const investigationTeamMember = investigationTeam[previewKey];
            let index2 = 0;
            investigationTeamMember.forEach((elements: any) => {
              // tslint:disable-next-line:forin
              for (const teamMember in elements) {
                if (teamMember === 'reportReviewedById') {
                  formData.append(
                    `investigationTeam[${previewKey}][${index2}][${teamMember}]`,
                    investigationTeam[previewKey][index2][teamMember]
                  );
                } else if (teamMember === 'dateOfReview') {
                  const dateOfReview = this.helperService.getFullDate(
                    investigationTeam[previewKey][index2][teamMember]
                  );
                  if (dateOfReview) {
                    formData.append(`investigationTeam[${previewKey}][${index2}][${teamMember}]`, dateOfReview);
                  }
                }
              }
              index2++;
            });
          } else {
            formData.append(`investigationTeam[${previewKey}]`, investigationTeam[previewKey]);
          }
        }
      } else if (dataKey === 'intContact') {
        // append nested object
        const intContact = this.contactEstablishForm.value.intContact;
        let index = 0;
        if (intContact instanceof Array) {
          intContact.forEach((element) => {
            // tslint:disable-next-line:forin
            for (const previewKey in element) {
              formData.append(`intContact[${index}][${previewKey}]`, intContact[index][previewKey]);
            }
            index++;
          });
        }
      } else if (dataKey === 'extContact') {
        // append nested object
        const extContact = this.contactEstablishForm.value.extContact;
        let index = 0;
        if (extContact instanceof Array) {
          extContact.forEach((element: any) => {
            // tslint:disable-next-line:forin
            for (const previewKey in element) {
              formData.append(`extContact[${index}][${previewKey}]`, extContact[index][previewKey]);
            }
            index++;
          });
        }
      } else if (dataKey === 'backgroundData') {
        // append nested object
        const backgroundData = this.backgroundDataForm.value.backgroundData;
        let index = 0;
        if (backgroundData instanceof Array) {
          backgroundData.forEach((element: any) => {
            // tslint:disable-next-line:forin
            for (const previewKey in element) {
              const fieldName = 'backgroundAttachments' + index;
              if (this.backgroundImage[fieldName]) {
                if (previewKey === 'attachmentFieldName') {
                  formData.append(`backgroundData[${index}][${previewKey}]`, fieldName);
                  // tslint:disable-next-line:forin
                  for (let index5 = 0; index5 < this.backgroundImage[fieldName].length; index5++) {
                    formData.append(`${fieldName}[${index5}]`, this.backgroundImage[fieldName][index5]);
                  }
                } else if (previewKey === 'isContibutingFactor') {
                  if (backgroundData[index][previewKey] === true) {
                    formData.append(`backgroundData[${index}][${previewKey}]`, '1');
                  } else {
                    formData.append(`backgroundData[${index}][${previewKey}]`, '0');
                  }
                } else if (previewKey === 'backgroundId') {
                  if (backgroundData[index][previewKey]) {
                    formData.append(`backgroundData[${index}][${previewKey}]`, backgroundData[index][previewKey]);
                  } else {
                  }
                } else {
                  formData.append(`backgroundData[${index}][${previewKey}]`, backgroundData[index][previewKey]);
                }
              } else if (!this.backgroundImage[fieldName] && element.attachmentFieldName) {
                if (previewKey === 'isContibutingFactor') {
                  if (backgroundData[index][previewKey] === true) {
                    formData.append(`backgroundData[${index}][${previewKey}]`, '1');
                  } else {
                    formData.append(`backgroundData[${index}][${previewKey}]`, '0');
                  }
                } else if (previewKey === 'attachmentFieldName') {
                } else {
                  formData.append(`backgroundData[${index}][${previewKey}]`, backgroundData[index][previewKey]);
                }
              }
            }

            index++;
          });
        }
      } else if (dataKey === 'incidentVdrData') {
        // append nested object
        const incidentVdrData = this.incidentVdrDataForm.value.incidentVdrData;
        let index = 0;
        for (const previewKey in incidentVdrData) {
          if (previewKey === 'reviewVdrDate') {
            const reviewVdrDate = incidentVdrData[previewKey];
            let index2 = 0;
            reviewVdrDate.forEach((elements: any) => {
              for (const reviewVdr in elements) {
                if (reviewVdr === 'reviewVdrData') {
                  const reviewVdrData = incidentVdrData[previewKey][index2][reviewVdr];
                  let index3 = 0;
                  reviewVdrData.forEach((elements2: any) => {
                    const fieldName = 'vdrAttachmentFieldName' + index + 'x' + index2;
                    for (const review in elements2) {
                      if (review === 'vdrAttachmentFieldName') {
                        formData.append(
                          `incidentVdrData[${previewKey}][${index2}][${reviewVdr}][${index3}][${review}]`,
                          fieldName
                        );
                        if (this.reviewVdrImage[fieldName]) {
                          for (let index5 = 0; index5 < this.reviewVdrImage[fieldName].length; index5++) {
                            formData.append(`${fieldName}[${index5}]`, this.reviewVdrImage[fieldName][index5]);
                          }
                        }
                      } else if (review === 'ltTime') {
                        this.ltTimeInt = '';
                        const ltTime = incidentVdrData[previewKey][index2][reviewVdr][index3][review];
                        const ltTimeFormatting = moment(ltTime).format('HH:mm');
                        this.ltTimeInt = parseInt(ltTimeFormatting.replace(':', ''), 10);
                        formData.append(
                          `incidentVdrData[${previewKey}][${index2}][${reviewVdr}][${index3}][${review}]`,
                          this.ltTimeInt
                        );
                      } else {
                        formData.append(
                          `incidentVdrData[${previewKey}][${index2}][${reviewVdr}][${index3}][${review}]`,
                          incidentVdrData[previewKey][index2][reviewVdr][index3][review]
                        );
                      }
                    }
                    index3++;
                  });
                } else if (reviewVdr === 'date') {
                  const dateOfExpiry = this.helperService.getFullDate(incidentVdrData[previewKey][index2][reviewVdr]);
                  if (dateOfExpiry) {
                    formData.append(`incidentVdrData[${previewKey}][${index2}][${reviewVdr}]`, dateOfExpiry);
                  }
                } else {
                  formData.append(
                    `incidentVdrData[${previewKey}][${index2}][${reviewVdr}]`,
                    incidentVdrData[previewKey][index2][reviewVdr]
                  );
                }
              }
              index2++;
            });
          } else {
            formData.append(`incidentVdrData[${previewKey}]`, incidentVdrData[previewKey]);
          }
        }
        index++;
        for (let index5 = 0; index5 < this.reviewOfVdrFile.length; index5++) {
          formData.append(`incidentVdrDataAttachment[${index5}]`, this.reviewOfVdrFile[index5]);
        }
      } else if (dataKey === 'postRecData') {
        // append nested object
        const postRecData = this.postRecDataForm.value.postRecData;
        let index = 0;
        for (const previewKey in postRecData) {
          if (previewKey === 'postRecoveryDate') {
            const reviewVdrDate = postRecData[previewKey];
            let index2 = 0;
            reviewVdrDate.forEach((elements: any) => {
              for (const postRecoveryData in elements) {
                if (postRecoveryData === 'postRecoveryData') {
                  const postRecovery = postRecData[previewKey][index2][postRecoveryData];
                  let index3 = 0;
                  postRecovery.forEach((elements2: any) => {
                    // tslint:disable-next-line:forin
                    for (const post in elements2) {
                      if (post === 'date') {
                        const dateOfExpiry = this.helperService.getFullDate(postRecovery[index3][post]);
                        if (dateOfExpiry) {
                          formData.append(
                            `postRecData[${previewKey}][${index2}][${postRecoveryData}][${index3}][${post}]`,
                            dateOfExpiry
                          );
                        }
                      } else if (post === 'ltTime') {
                        this.ltTimeInt = '';
                        const ltTime = postRecovery[index3][post];
                        const ltTimeFormatting = moment(ltTime).format('HH:mm');
                        this.ltTimeInt = parseInt(ltTimeFormatting.replace(':', ''), 10);
                        formData.append(
                          `postRecData[${previewKey}][${index2}][${postRecoveryData}][${index3}][${post}]`,
                          this.ltTimeInt
                        );
                      } else {
                        formData.append(
                          `postRecData[${previewKey}][${index2}][${postRecoveryData}][${index3}][${post}]`,
                          postRecovery[index3][post]
                        );
                      }
                    }
                    index3++;
                  });
                } else if (postRecoveryData === 'date') {
                  const convertedDate = moment(postRecData[previewKey][index2][postRecoveryData]).startOf('day');
                  const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
                  formData.append(`postRecData[${previewKey}][${index2}][${postRecoveryData}]`, dateOfExpiry);
                } else {
                  formData.append(
                    `postRecData[${previewKey}][${index2}][${postRecoveryData}]`,
                    postRecData[previewKey][index2][postRecoveryData]
                  );
                }
              }
              index2++;
            });
          } else {
            formData.append(`postRecData[${previewKey}]`, postRecData[previewKey]);
          }
        }
        index++;
        for (let index5 = 0; index5 < this.postRecFile.length; index5++) {
          formData.append(`postRecoveryMeasuresAttachments[${index5}]`, this.postRecFile[index5]);
        }
      } else if (dataKey === 'notifyData') {
        // append nested object
        const notifyData = this.notifyDataForm.value.notifyData;
        let index = 0;
        for (const previewKey in notifyData) {
          if (previewKey === 'notificationDate') {
            const notificationDate = notifyData[previewKey];
            let index2 = 0;
            notificationDate.forEach((elements: any) => {
              for (const notify in elements) {
                if (notify === 'notificationData') {
                  const notificationData = notifyData[previewKey][index2][notify];
                  let index3 = 0;
                  notificationData.forEach((elements2: any) => {
                    // tslint:disable-next-line:forin
                    for (const notifydata in elements2) {
                      if (notifydata === 'ltTime') {
                        this.ltTimeInt = '';
                        const ltTime = notificationData[index3][notifydata];
                        const ltTimeFormatting = moment(ltTime).format('HH:mm');
                        this.ltTimeInt = parseInt(ltTimeFormatting.replace(':', ''), 10);
                        formData.append(
                          `notifyData[${previewKey}][${index2}][${notify}][${index3}][${notifydata}]`,
                          this.ltTimeInt
                        );
                      } else {
                        formData.append(
                          `notifyData[${previewKey}][${index2}][${notify}][${index3}][${notifydata}]`,
                          notificationData[index3][notifydata]
                        );
                      }
                    }
                    index3++;
                  });
                } else if (notify === 'date') {
                  const convertedDate = moment(notifyData[previewKey][index2][notify]).startOf('day');
                  const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
                  formData.append(`notifyData[${previewKey}][${index2}][${notify}]`, dateOfExpiry);
                } else {
                  formData.append(
                    `notifyData[${previewKey}][${index2}][${notify}]`,
                    notifyData[previewKey][index2][notify]
                  );
                }
              }
              index2++;
            });
          } else {
            if (notifyData[previewKey]) {
              formData.append(`notifyData[${previewKey}]`, notifyData[previewKey]);
            }
          }
        }
        index++;
        for (let index5 = 0; index5 < this.notifyFile.length; index5++) {
          formData.append(`notificationAttachments[${index5}]`, this.notifyFile[index5]);
        }
      } else if (dataKey === 'breachData') {
        // append nested object
        const breachData = this.breachDataForm.value.breachData;
        let index = 0;
        for (const previewKey in breachData) {
          if (previewKey === 'breachReqData') {
            const breachReqData = breachData[previewKey];
            let index2 = 0;
            breachReqData.forEach((elements: any) => {
              // tslint:disable-next-line:forin
              for (const breachReq in elements) {
                if (breachReq === 'ltTime') {
                  this.ltTimeInt = '';
                  const ltTime = breachData[previewKey][index2][breachReq];
                  const ltTimeFormatting = moment(ltTime).format('HH:mm');
                  this.ltTimeInt = parseInt(ltTimeFormatting.replace(':', ''), 10);
                  formData.append(`breachData[${previewKey}][${index2}][${breachReq}]`, this.ltTimeInt);
                } else {
                  formData.append(
                    `breachData[${previewKey}][${index2}][${breachReq}]`,
                    breachData[previewKey][index2][breachReq]
                  );
                }
              }
              index2++;
            });
          } else {
            formData.append(`breachData[${previewKey}]`, breachData[previewKey]);
          }
        }
        index++;

        for (let index5 = 0; index5 < this.breachFile.length; index5++) {
          formData.append(`breachAttachments[${index5}]`, this.breachFile[index5]);
        }
      } else if (dataKey === 'typeOfContactOrEvent') {
        const datas = this.analysisFormGroup.value.typeOfContactOrEvent;
        let index = 0;
        datas.forEach((element: any) => {
          if (element.typeOfCntctId) {
            for (const previewKey in element) {
              if (previewKey === 'basicImmediateCause') {
                const basicImmediateCause = datas[index][previewKey];
                let index2 = 0;
                basicImmediateCause.forEach((elements: any) => {
                  for (const basicCause in elements) {
                    if (datas[index][previewKey][index2][basicCause]) {
                      if (basicCause === 'basicRootCause') {
                        const basicRootCause = datas[index][previewKey][index2][basicCause];
                        let index3 = 0;
                        basicRootCause.forEach((element3: any) => {
                          for (const rootCause in element3) {
                            if (rootCause === 'correctiveActionId') {
                              const basicRoot = basicRootCause[index3][rootCause];
                              let index4 = 0;
                              basicRoot.forEach((element4: any) => {
                                formData.append(
                                  `typeOfContactOrEvent[${index}][${previewKey}][${index2}][${basicCause}][${index3}][${rootCause}][${index4}]`,
                                  basicRoot[index4]
                                );
                                index4++;
                              });
                            } else if (rootCause === 'rootCauseExplanation') {
                              const rootCauseId = 'furtherExplanation';
                              const basicRoot = basicRootCause[index3][rootCauseId];
                              formData.append(
                                `typeOfContactOrEvent[${index}][${previewKey}][${index2}][${basicCause}][${index3}][${rootCause}]`,
                                basicRoot
                              );
                            } else if (rootCause === 'furtherExplanation') {
                            } else {
                              formData.append(
                                `typeOfContactOrEvent[${index}][${previewKey}][${index2}][${basicCause}][${index3}][${rootCause}]`,
                                basicRootCause[index3][rootCause]
                              );
                            }
                          }
                          index3++;
                        });
                      } else if (basicCause === 'immediateCauseExplanation') {
                        const basicCauseId = 'furtherExplanation';
                        const basicRootCause = datas[index][previewKey][index2][basicCauseId];
                        formData.append(
                          `typeOfContactOrEvent[${index}][${previewKey}][${index2}][${basicCause}]`,
                          basicRootCause
                        );
                      } else if (basicCause === 'furtherExplanation') {
                      } else {
                        formData.append(
                          `typeOfContactOrEvent[${index}][${previewKey}][${index2}][${basicCause}]`,
                          datas[index][previewKey][index2][basicCause]
                        );
                      }
                    }
                  }
                  index2++;
                });
              } else {
                formData.append(`typeOfContactOrEvent[${index}][${previewKey}]`, datas[index][previewKey]);
              }
            }
          }
          index++;
        });
      } else if (dataKey === 'crossRefToTMSA') {
        const crossRefToTMSA = this.crossReferenceForm.value.crossRefToTMSA;
        let index = 0;
        if (crossRefToTMSA) {
          crossRefToTMSA.forEach((element: any) => {
            for (const previewKey in element) {
              if (crossRefToTMSA[index][previewKey]) {
                formData.append(`crossRefToTMSA[${index}][${previewKey}]`, crossRefToTMSA[index][previewKey]);
              }
            }
            index++;
          });
        }
      } else if (dataKey === 'analysisData') {
        const analysisData = this.analysisFormGroup.value.analysisData;
        let index = 0;
        analysisData.forEach((element: any) => {
          for (const previewKey in element) {
            if (analysisData[index][previewKey]) {
              formData.append(`analysisData[${index}][${previewKey}]`, analysisData[index][previewKey]);
            }
          }
          index++;
        });
      } else if (dataKey === 'action') {
        const datas = this.actionForm.value.action;
        let index = 0;
        datas.forEach((element: any) => {
          for (const previewKey in element) {
            if (previewKey === 'dueDate') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                if (datas[index][previewKey]) {
                  const convertedDate = moment(datas[index][previewKey]).startOf('day');
                  const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
                  formData.append(`action[${index}][${previewKey}]`, dateOfExpiry);
                }
              }
            } else if (previewKey === 'dateCompleted') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                if (datas[index][previewKey]) {
                  const convertedDate = moment(datas[index][previewKey]).startOf('day');
                  const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
                  formData.append(`action[${index}][${previewKey}]`, dateOfExpiry);
                }
              }
            } else if (previewKey === 'status') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                formData.append(`action[${index}][${previewKey}]`, datas[index][previewKey]);
              }
            } else if (previewKey === 'supportiveComments') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                if (datas[index][previewKey] && datas[index][previewKey] !== 'undefined') {
                  formData.append(`action[${index}][${previewKey}]`, datas[index][previewKey]);
                }
              }
            } else if (previewKey === 'email') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                if (datas[index][previewKey]) {
                  const emails = datas[index][previewKey];
                  const email = emails.split(';');
                  let index2 = 0;
                  log.debug('email: ', email);
                  email.forEach((value: any) => {
                    formData.append(`action[${index}][${previewKey}][${index}]`, value);
                    index2++;
                  });
                }
              }
            } else if (previewKey === 'actionAccepted') {
              if (datas[index][previewKey]) {
                formData.append(`action[${index}][${previewKey}]`, '1');
              }
            } else if (previewKey === 'designationId') {
              const key = 'type';
              if (datas[index][key] !== 'Immediate Action') {
                if (datas[index][previewKey] !== 'Other') {
                  formData.append(`action[${index}][${previewKey}]`, datas[index][previewKey]);
                }
              }
            } else {
              log.debug('datas[index][previewKey]: ', datas[index]);
              if (datas[index][previewKey]) {
                formData.append(`action[${index}][${previewKey}]`, datas[index][previewKey]);
              }
            }
          }
          index++;
        });
      } else if (dataKey === 'communication') {
        const communication = this.communicationDataForm.value.communication;
        let index = 0;
        communication.forEach((element: any) => {
          // tslint:disable-next-line:forin
          for (const previewKey in element) {
            if (communication[index][previewKey]) {
              formData.append(`communication[${index}][${previewKey}]`, communication[index][previewKey]);
            }
          }
          index++;
        });
      } else if (dataKey === 'attachment') {
      } else if (dataKey === 'notificationAttachments') {
      } else if (dataKey === 'reviewOfVdrDataAttachments') {
      } else if (dataKey === 'postRecoveryMeasuresAttachments') {
      } else if (dataKey === 'seqOfEventAttachments') {
      } else if (dataKey === 'breachAttachments') {
      } else if (dataKey === 'actionAttachments') {
      } else if (dataKey === 'fileSource') {
      } else if (dataKey === 'dateOfIncident') {
        const dateTimeIncident = moment(data[dataKey]).format('YYYY-MM-DD');
        formData.append(dataKey, dateTimeIncident);
      } else if (dataKey === 'timeOfIncident') {
        const key = 'dateOfIncident';
        const timeIncident = moment(data[key]).format('HH:mm:ss');
        const dateTimeIncident = moment(data[key]).format('YYYY-MM-DD');
        formData.append(dataKey, dateTimeIncident + ' ' + timeIncident);
      } else if (dataKey === 'details') {
        if (data[dataKey] === null) {
          formData.append(dataKey, '');
        } else {
          formData.append(dataKey, data[dataKey]);
        }
      } else if (dataKey === 'isLatLong') {
        if (this.isLatLong) {
          formData.append(dataKey, '1');
        } else {
          formData.append(dataKey, '0');
        }
      } else if (dataKey === 'portId') {
        if (!this.isLatLong) {
          formData.append(dataKey, data[dataKey]);
        }
      } else if (dataKey === 'latLong') {
        if (this.isLatLong) {
          formData.append(dataKey, latLong);
        }
      } else if (dataKey === 'personalInjury') {
        const existFlag = impactData.indexOf('People');
        if (existFlag > -1) {
          if (data[dataKey]) {
            formData.append(`severityOfConsequences[${sequenceCount}]`, data[dataKey]);
            sequenceCount++;
          }
        }
      } else if (dataKey === 'propertyDamage') {
        const existFlag = impactData.indexOf('Assets/Property/Financial');
        if (existFlag > -1) {
          if (data[dataKey]) {
            formData.append(`severityOfConsequences[${sequenceCount}]`, data[dataKey]);
            sequenceCount++;
          }
        }
      } else if (dataKey === 'environment') {
        const existFlag = impactData.indexOf('Environment');
        if (existFlag > -1) {
          if (data[dataKey]) {
            formData.append(`severityOfConsequences[${sequenceCount}]`, data[dataKey]);
            sequenceCount++;
          }
        }
      } else if (dataKey === 'businessimpact') {
        const existFlag = impactData.indexOf('Reputation/Business');
        if (existFlag > -1) {
          if (data[dataKey]) {
            formData.append(`severityOfConsequences[${sequenceCount}]`, data[dataKey]);
            sequenceCount++;
          }
        }
      } else if (dataKey === 'isPartiallySubmitted') {
        formData.append(dataKey, '0');
      } else if (dataKey === 'severityOfConsequences') {
      } else if (dataKey === 'eventImpact') {
        let index = 0;
        impactData.forEach((element: any) => {
          formData.append(`eventImpact[${index}]`, impactData[index]);
          index++;
        });
      } else if (dataKey === 'isAddlInfoInvolvingInjury') {
        if (data[dataKey]) {
          const keyValue = '1';
          formData.append(dataKey, keyValue);
        } else {
          const keyValue = '0';
          formData.append(dataKey, keyValue);
        }
      } else if (dataKey === 'isAddlInfoInvolvingEqpDamage') {
        if (data[dataKey]) {
          const keyValue = '1';
          formData.append(dataKey, keyValue);
        } else {
          const keyValue = '0';
          formData.append(dataKey, keyValue);
        }
      } else if (dataKey === 'isIncidentVdrData') {
        if (data[dataKey]) {
          const keyValue = '1';
          formData.append(dataKey, keyValue);
        } else {
          const keyValue = '0';
          formData.append(dataKey, keyValue);
        }
      } else if (dataKey === 'isActive') {
        formData.append(dataKey, '1');
      } else {
        formData.append(dataKey, data[dataKey]);
      }
    }

    this.incidentService.updateIncident(formData, this.incidentId).subscribe(
      (response) => {
        this.incidentPutResponse = response;
        if (this.incidentPutResponse.statusCode === 200) {
          const message = this.incidentPutResponse.message;
          this.notifyService.showSuccess(message, this.title);
          this.router.navigate(['/incident']);
        } else {
          const message = this.incidentPutResponse.error.message;
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  validateSteppper(validIndex: any) {
    if (validIndex <= 1) {
      log.debug('validIndex: ', validIndex);
      if (this.overViewFormGroup.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 2) {
      log.debug('validIndex: ', validIndex);
      if (this.descriptionFormGroup.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 3) {
      log.debug('this.investigationTeamForm.status: ', this.investigationTeamForm.status);
      if (this.investigationTeamForm.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 4) {
      log.debug('this.contactEstablishForm.status: ', this.contactEstablishForm.status);
      if (this.contactEstablishForm.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 5) {
      if (this.backgroundDataForm.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 6) {
      if (this.incidentVdrDataForm.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 7) {
      if (this.notifyDataForm.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 8) {
      if (this.postRecDataForm.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 9) {
      if (this.breachDataForm.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 10) {
      if (this.incidentSeqDataForm.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 11) {
      if (this.analysisFormGroup.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 12) {
      if (this.actionForm.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 13) {
      if (this.investigationTeamForm.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    } else if (validIndex <= 14) {
      if (this.crossReferenceForm.status === 'INVALID') {
        if (this.formStepErrorArray.indexOf(validIndex) === -1) {
          this.formStepErrorArray.push(validIndex);
        }
      } else {
        const index = this.formStepErrorArray.indexOf(validIndex);
        if (this.formStepErrorArray.indexOf(validIndex) === 0) {
          this.formStepErrorArray.splice(index, 1);
        }
      }
    }
  }

  onSeqFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        this.images.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.seqImagesArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deleteSeqFileImage(image: any) {
    const index = this.seqImagesArray.indexOf(image);
    this.seqImagesArray.splice(index, 1);
    this.images.splice(index, 1);
    this.onSeqFileChange(this.seqImagesArray);
  }

  onBreachFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        this.breachFile.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.breachFileArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deleteBreachImage(image: any) {
    const index = this.breachFileArray.indexOf(image);
    this.breachFileArray.splice(index, 1);
    this.breachFile.splice(index, 1);
    this.onBreachFileChange(this.breachFileArray);
  }

  onReviewOfVdrFileChange(event: any, reviewOfVdrIndex: any, vdrIndex: any) {
    const fieldName = 'vdrAttachmentFieldName' + reviewOfVdrIndex + 'x' + vdrIndex;
    const attachmentFieldName = this.incidentVdrDataForm
      .get('incidentVdrData')
      .get(`reviewVdrDate.${reviewOfVdrIndex}.reviewVdrData.${vdrIndex}.vdrAttachmentFieldName`);
    attachmentFieldName.patchValue(fieldName);
    this.addReviewVdrField(fieldName);
    if (event.target) {
      if (event.target.files && event.target.files[0]) {
        const filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
          const image = {
            name: '',
            type: '',
            size: '',
            url: '',
            fieldName: '',
          };
          this.reviewOfVdrImages.push(event.target.files[i] as File);
          this.reviewVdrImage[fieldName] = this.reviewOfVdrImages;
          image.name = event.target.files[i].name;
          image.type = event.target.files[i].type;
          image.size = event.target.files[i].size;
          image.fieldName = fieldName;

          const reader = new FileReader();
          reader.onload = (filedata) => {
            image.url = reader.result + '';
            this.reviewOfVdrFileArray.push(image);
          };
          reader.readAsDataURL(event.target.files[i]);
        }
      }
      event.srcElement.value = null;
    }
  }

  deleteReviewOfVdrImage(image: any, reviewOfVdrIndex: any, vdrIndex: any) {
    const fieldName = 'vdrAttachmentFieldName' + reviewOfVdrIndex + 'x' + vdrIndex;

    const index = this.reviewOfVdrFileArray.indexOf(image);
    this.reviewOfVdrFileArray.splice(index, 1);
    const fieldNameIndex = this.reviewVdrImage[fieldName].indexOf(image);
    this.reviewOfVdrImages.splice(index, 1);
    this.reviewVdrImage[fieldName].splice(fieldNameIndex, 1);
    this.onReviewOfVdrFileChange(this.reviewOfVdrFileArray, reviewOfVdrIndex, vdrIndex);
  }

  onNotifyFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        this.notifyFile.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.notifyFileArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deleteNotifyImage(image: any) {
    const index = this.notifyFileArray.indexOf(image);
    this.notifyFileArray.splice(index, 1);
    this.notifyFile.splice(index, 1);
    this.onNotifyFileChange(this.notifyFileArray);
  }

  // onPostRecFileChange(event: any) {
  //   if (event.target.files && event.target.files[0]) {
  //     const filesAmount = event.target.files.length;
  //     this.postRecFile = [];
  //     for (let i = 0; i < filesAmount; i++) {
  //       this.postRecFile.push(event.target.files[i] as File);
  //       this.postRecDataForm.patchValue({
  //         postRecoveryMeasuresAttachments: this.postRecFile,
  //       });
  //     }
  //   }
  // }
  onPostRecFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        this.postRecFile.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.postRecFileArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deletePostRecImage(image: any) {
    const index = this.postRecFileArray.indexOf(image);
    this.postRecFileArray.splice(index, 1);
    this.postRecFile.splice(index, 1);
    this.onPostRecFileChange(this.postRecFileArray);
  }

  onActionFileChange(event: any) {
    log.debug('onActionFileChange');
    // if (event.target.files && event.target.files[0]) {
    //   const filesAmount = event.target.files.length;
    //   this.actionFile = [];
    //   for (let i = 0; i < filesAmount; i++) {
    //     this.actionFile.push(event.target.files[i] as File);
    //     this.actionForm.patchValue({
    //       actionAttachments: this.actionFile,
    //     });
    //   }
    // }

    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        this.actionFile.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.actionFileArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deleteActionImage(image: any) {
    const index = this.actionFileArray.indexOf(image);
    this.actionFileArray.splice(index, 1);
    this.actionFile.splice(index, 1);
    this.onActionFileChange(this.actionFileArray);
  }

  // onBackGroundFileChange(event: any, backgroundIndex: any) {
  //   const fieldName = 'backgroundAttachments' + backgroundIndex;
  //   const backgroundDataField = 'backgroundData';
  //   const attachmentFieldName = this.backgroundDataForm.controls[backgroundDataField] as FormArray;
  //   attachmentFieldName.value[backgroundIndex].attachmentFieldName = fieldName;
  //   this.addBackgroundField(fieldName);
  //   if (event.target.files && event.target.files[0]) {
  //     const filesAmount = event.target.files.length;
  //     this.backgroundImages = [];
  //     for (let i = 0; i < filesAmount; i++) {
  //       this.backgroundImages.push(event.target.files[i] as File);
  //       this.backgroundImage[fieldName] = this.backgroundImages;
  //       this.backgroundDataForm.patchValue({
  //         attachmentFieldName: this.backgroundImages,
  //       });
  //     }
  //   }
  // }

  onBackGroundFileChange(event: any, backgroundIndex: any) {
    log.debug('backgroundIndex', backgroundIndex);
    const fieldName = 'backgroundAttachments' + backgroundIndex;
    const backgroundDataField = 'backgroundData';
    const attachmentFieldName = this.backgroundDataForm.controls[backgroundDataField] as FormArray;
    attachmentFieldName.value[backgroundIndex].attachmentFieldName = fieldName;
    this.addBackgroundField(fieldName);

    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
          fieldName: '',
        };
        log.debug('event.target.files[i]: ', event.target.files[i]);
        log.debug('fieldName: ', fieldName);
        this.backgroundImages.push(event.target.files[i] as File);
        this.backgroundImage[fieldName] = this.backgroundImages;
        log.debug('this.backgroundImage[fieldName]: ', this.backgroundImage[fieldName]);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;
        image.fieldName = fieldName;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.backgroundImageArray.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deleteBackgroundImage(image: any, backgroundIndex: any) {
    const fieldName = 'backgroundAttachments' + backgroundIndex;

    log.debug('image: ', image);

    const index = this.backgroundImageArray.indexOf(image);
    log.debug('index: ', index);
    this.backgroundImageArray.splice(index, 1);
    const fieldNameIndex = this.backgroundImage[fieldName].indexOf(image);
    this.backgroundImages.splice(index, 1);
    this.backgroundImage[fieldName].splice(fieldNameIndex, 1);
    this.onBackGroundFileChange(this.backgroundImageArray, backgroundIndex);
  }

  addBackgroundField(fieldName: any) {
    this.backgroundDataForm.addControl(fieldName, new FormControl('', Validators.required));
  }

  addReviewVdrField(fieldName: any) {
    this.incidentVdrDataForm.addControl(fieldName, new FormControl('', Validators.required));
  }

  onAddItem() {
    const crossRefToTMSA = 'crossRefToTMSA';
    this.subDetails = this.crossReferenceForm.get(crossRefToTMSA) as FormArray;
    this.subDetails.push(this.addSubdetails());
  }

  onAddAtionField() {
    const action = 'action';
    this.subDetails = this.actionForm.get(action) as FormArray;
    this.subDetails.push(this.actionFields());
  }

  /** Add Sub details Dynamically */
  addSubdetails(): FormGroup {
    return this._formBuilder.group({
      elementId: [''],
      stageId: [''],
      specificKpiId: [''],
      additionalComments: [''],
    });
  }

  addinvestigationForm() {
    return this._formBuilder.group({
      investigatorsId: [''],
      department: [''],
      position: [''],
      reportApprovedById: [''],
      dateOfApprove: [''],
      investigationTeamMember: new FormArray([this.addTeamMemberForm()]),
      reportPreparedBy: new FormArray([this.addReportPreparedByForm()]),
      reportReviewedBy: new FormArray([this.addReportReviewedByForm()]),
    });
  }

  addTeamMemberForm() {
    return this._formBuilder.group({
      isNewTeamMember: [''],
      teamMemberId: [''],
      name: [''],
      department: [''],
      position: [''],
      type: [''],
    });
  }

  addinvestigationNewTeamForm() {
    return this._formBuilder.group({
      investigatorsId: [''],
      department: [''],
      position: [''],
      reportApprovedById: [''],
      dateOfApprove: [''],
      newInvestigationTeamMember: new FormArray([this.addNewTeamMemberForm()]),
      investigationTeamMember: new FormArray([]),
      reportPreparedBy: new FormArray([this.addReportPreparedByForm()]),
      reportReviewedBy: new FormArray([this.addReportReviewedByForm()]),
    });
  }

  // addNewTeamMemberForm() {
  //   return this._formBuilder.group({
  //     teamMemberId: [''],
  //     department: [''],
  //     position: [''],
  //   });
  // }
  addNewTeamMemberForm() {
    return this._formBuilder.group({
      name: [''],
      department: [''],
      position: ['Team Member'],
      type: [''],
    });
  }

  addReportPreparedByForm() {
    return this._formBuilder.group({
      reportPreparedById: [''],
      department: [''],
      position: [''],
    });
  }

  addReportReviewedByForm() {
    return this._formBuilder.group({
      reportReviewedById: [''],
      position: [''],
      dateOfReview: [''],
    });
  }

  addReportApprovedByForm() {
    return this._formBuilder.group({
      reportApprovedById: [''],
      position: [''],
      dateOfApprove: [''],
    });
  }

  getTeamMemberControls() {
    const investigationTeamMember = this.investigationTeamForm
      .get(`investigationTeam`)
      .get(`investigationTeamMember`) as FormArray;
    return investigationTeamMember;
  }

  getReportPreparedByControls() {
    const reportPreparedBy = this.investigationTeamForm.get(`investigationTeam`).get(`reportPreparedBy`) as FormArray;
    return reportPreparedBy;
  }

  getReportReviewedByControls() {
    const reportReviewedBy = this.investigationTeamForm.get(`investigationTeam`).get(`reportReviewedBy`) as FormArray;
    return reportReviewedBy;
  }

  onAddinvestigationTeam() {
    this.subDetails = this.investigationTeamForm.get(`investigationTeam`).get(`investigationTeamMember`) as FormArray;
    this.subDetails.push(this.addTeamMemberForm());
  }

  onAddreportPreparedBy() {
    this.subDetails = this.investigationTeamForm.get(`investigationTeam`).get(`reportPreparedBy`) as FormArray;
    this.subDetails.push(this.addReportPreparedByForm());
  }

  onAddreportReviewedBy() {
    this.subDetails = this.investigationTeamForm.get(`investigationTeam`).get(`reportReviewedBy`) as FormArray;
    this.subDetails.push(this.addReportReviewedByForm());
  }

  setInvestigators(event: any) {
    const investigate = event.value;
    log.debug('event: ', investigate);
    this.investigatorsList.forEach((element: any) => {
      if (investigate === element.investigatorsId) {
        const position = element.position;
        const department = element.department;
        this.investigationTeamForm.get(`investigationTeam`).get(`position`).patchValue(position);
        this.investigationTeamForm.get(`investigationTeam`).get(`department`).patchValue(department);
      }
    });
  }

  setInvestigatorsTeamMember(event: any, index: any) {
    const investigate = event.value;
    this.investigatorsList.forEach((element: any) => {
      if (investigate === element.investigatorsId) {
        const position = element.position;
        const department = element.department;
        this.investigationTeamForm
          .get(`investigationTeam`)
          .get(`investigationTeamMember.${index}.position`)
          .patchValue(position);
        this.investigationTeamForm
          .get(`investigationTeam`)
          .get(`investigationTeamMember.${index}.department`)
          .patchValue(department);
      }
    });
  }

  setReportPrepare(event: any, index: any) {
    const userId = event.value;
    log.debug('userId: ', userId);
    this.userList.forEach((element: any) => {
      log.debug('element: ', element);
      if (userId === element.userId) {
        const position = element.designation;
        const department = element.department;
        this.investigationTeamForm
          .get(`investigationTeam`)
          .get(`reportPreparedBy.${index}.position`)
          .patchValue(position);
        this.investigationTeamForm
          .get(`investigationTeam`)
          .get(`reportPreparedBy.${index}.department`)
          .patchValue(department);
      }
    });
  }

  setReportReview(event: any, index: any) {
    const investigate = event.value;
    this.investigatorsList.forEach((element: any) => {
      log.debug('element: ', element);
      if (investigate === element.investigatorsId) {
        const position = element.position;
        this.investigationTeamForm
          .get(`investigationTeam`)
          .get(`reportReviewedBy.${index}.position`)
          .patchValue(position);
      }
    });
  }

  onDeleteTeamMember(index: number) {
    const investigationTeam = this.investigationTeamForm
      .get(`investigationTeam`)
      .get(`investigationTeamMember`) as FormArray;
    investigationTeam.removeAt(index);
  }

  onDeleteReportPreparedBy(index: number) {
    const investigationTeam = this.investigationTeamForm.get(`investigationTeam`).get(`reportPreparedBy`) as FormArray;
    investigationTeam.removeAt(index);
  }

  onDeleteReportReviewedBy(index: number) {
    const investigationTeam = this.investigationTeamForm.get(`investigationTeam`).get(`reportReviewedBy`) as FormArray;
    investigationTeam.removeAt(index);
  }

  getInvestigationControls() {
    const investigationTeam = this.investigationTeamForm.get(`investigationTeam`) as FormArray;
    return investigationTeam;
  }

  onAddInvestigation() {
    this.subDetails = this.investigationTeamForm.get(`investigationTeam`) as FormArray;
    this.subDetails.push(this.addinvestigationForm());
  }

  onDeleteInvestigation(index: number) {
    const investigationTeam = this.investigationTeamForm.get(`investigationTeam`) as FormArray;
    investigationTeam.removeAt(index);
  }

  /** Add Communication form Field Dynamically */
  addCommunication(): FormGroup {
    return this._formBuilder.group({
      communicationId: [''],
      commentBy: [''],
      comment: [''],
    });
  }

  getCommunicationControls() {
    const communication = this.communicationDataForm.get(`communication`) as FormArray;
    return communication;
  }

  onAddCommunicationInvestigation() {
    this.subDetails = [];
    this.subDetails = this.communicationDataForm.get(`communication`) as FormArray;
    this.subDetails.push(this.addCommunication());
  }

  onDeleteCommunication(index: number) {
    const communication = this.communicationDataForm.get(`communication`) as FormArray;
    communication.removeAt(index);
  }

  /**
   * contact established form build
   */

  addIntContactForm() {
    return this._formBuilder.group({
      intContactId: [''],
      locationId: [''],
      internalContactId: [''],
    });
  }

  getIntContactControls() {
    const intContact = this.contactEstablishForm.get(`intContact`) as FormArray;
    return intContact;
  }

  onAddIntContact() {
    this.subDetails = this.contactEstablishForm.get(`intContact`) as FormArray;
    this.subDetails.push(this.addIntContactForm());
  }

  onDeleteIntContact(index: number) {
    const intContact = this.contactEstablishForm.get(`intContact`) as FormArray;
    intContact.removeAt(index);
  }

  addExtContactForm() {
    return this._formBuilder.group({
      extContactId: [''],
      companyId: [''],
      name: [''],
      designation: [''],
      // location: [''],
    });
  }

  getExtContactControls() {
    const extContact = this.contactEstablishForm.get(`extContact`) as FormArray;
    return extContact;
  }

  onAddExtContact() {
    this.subDetails = this.contactEstablishForm.get(`extContact`) as FormArray;
    this.subDetails.push(this.addExtContactForm());
  }

  onDeleteExtContact(index: number) {
    const extContact = this.contactEstablishForm.get(`extContact`) as FormArray;
    extContact.removeAt(index);
  }

  /**
   * Begin:: Sequence of event form Building
   */

  addincidentSeqForm() {
    return this._formBuilder.group({
      seqEventsMasterId: [''],
      date: [''],
      seqData: new FormArray([this.addSeqDataForm()]),
    });
  }

  addSeqDataForm() {
    return this._formBuilder.group({
      ltTime: [''],
      description: [''],
      triggeringFactors: [''],
    });
  }

  getSeqDataControls(index: any) {
    const seqData = this.incidentSeqDataForm.get(`incidentSeqData.${index}.seqData`) as FormArray;
    return seqData;
  }

  getRootCauseControls(index: any, immediateFormIndex: any) {
    const analysisF = this.analysisFormGroup.get(
      `typeOfContactOrEvent.${index}.basicImmediateCause.${immediateFormIndex}.basicRootCause`
    ) as FormArray;
    return analysisF;
  }

  getImmediateCauseControls(index: any) {
    const analysisF = this.analysisFormGroup.get(`typeOfContactOrEvent.${index}.basicImmediateCause`) as FormArray;
    return analysisF;
  }

  onAddIncidentSeq() {
    const incidentSeqData = 'incidentSeqData';
    this.subDetails = [];
    this.subDetails = this.incidentSeqDataForm.get(incidentSeqData) as FormArray;
    this.subDetails.push(this.addincidentSeqForm());
  }

  onAddSeqData(index: any) {
    this.subDetails = [];

    this.subDetails = this.incidentSeqDataForm.get(`incidentSeqData.${index}.seqData`) as FormArray;
    this.subDetails.push(this.addSeqDataForm());
  }

  onDeleteSeqData(index: number, seqIndex: number) {
    const incidentSeqData = this.incidentSeqDataForm.get(`incidentSeqData.${index}.seqData`) as FormArray;
    incidentSeqData.removeAt(seqIndex);
  }

  onDeleteIncidentSeq(index: number) {
    const incidentSeqData = this.incidentSeqDataForm.controls.incidentSeqData as FormArray;
    incidentSeqData.removeAt(index);
  }

  /**
   * Begin:: breach Data form Building
   */

  addBreachForm() {
    return this._formBuilder.group({
      breachOfRegulatoryReqMasterId: [''],
      breachAdditionalComments: [''],
      breachReqData: new FormArray([this.addBreachDataForm()]),
    });
  }

  addBreachDataForm() {
    return this._formBuilder.group({
      documents: [''],
      refernce: [''],
      description: [''],
    });
  }

  getBreachDataControls() {
    const seqData = this.breachDataForm.get(`breachData`).get(`breachReqData`) as FormArray;
    return seqData;
  }

  onAddBreachData() {
    this.subDetails = [];
    this.subDetails = this.breachDataForm.get(`breachData`).get(`breachReqData`) as FormArray;
    this.subDetails.push(this.addBreachDataForm());
  }

  onDeleteaddBreachData(index: number) {
    const breachData = this.breachDataForm.get(`breachData`).get(`breachReqData`) as FormArray;
    breachData.removeAt(index);
  }

  /**
   * Begin:: incidentVdrData Form Field
   */

  addIncidentVdrForm() {
    return this._formBuilder.group({
      additionalComments: [''],
      reviewofvdrdatamasterId: [''],
      reviewVdrDate: new FormArray([this.addReviewVdr()]),
    });
  }

  addReviewVdr() {
    return this._formBuilder.group({
      date: ['', Validators.required],
      reviewVdrData: new FormArray([this.addReviewVdrData()]),
    });
  }

  addReviewVdrData() {
    return this._formBuilder.group({
      ltTime: ['', Validators.required],
      description: ['', Validators.required],
      vdrAttachmentFieldName: ['', Validators.required],
    });
  }

  getReviewVdrDataControls(index: any) {
    const seqData = this.incidentVdrDataForm
      .get('incidentVdrData')
      .get(`reviewVdrDate.${index}.reviewVdrData`) as FormArray;
    return seqData;
  }

  getReviewVdrControls() {
    return this.incidentVdrDataForm.get('incidentVdrData').get('reviewVdrDate') as FormArray;
  }

  onAddReviewVdr() {
    this.subDetails = [];
    this.subDetails = this.incidentVdrDataForm.get('incidentVdrData').get('reviewVdrDate') as FormArray;
    this.subDetails.push(this.addReviewVdr());
  }

  onAddReviewVdrData(index: any) {
    this.subDetails = [];
    this.subDetails = this.incidentVdrDataForm
      .get('incidentVdrData')
      .get(`reviewVdrDate.${index}.reviewVdrData`) as FormArray;
    this.subDetails.push(this.addReviewVdrData());
  }

  onDeleteVdrData(index: number, vdrIndex: number) {
    const incidentVdrData = this.incidentVdrDataForm
      .get('incidentVdrData')
      .get(`reviewVdrDate.${index}.reviewVdrData`) as FormArray;
    incidentVdrData.removeAt(vdrIndex);
  }

  onDeleteIncidentVdr(index: number) {
    const incidentVdrData = this.incidentVdrDataForm.get('incidentVdrData').get('reviewVdrDate') as FormArray;
    incidentVdrData.removeAt(index);
  }

  /**
   * End:: incidentVdrData Form Field
   * Begin:: postRecoveryData Form Field
   */

  addpostRecDataForm() {
    return this._formBuilder.group({
      postincidentrecoverymeasuresmasterId: [''],
      postRecAdditionalComments: [''],
      postRecoveryDate: new FormArray([this.addPostRecovery()]),
    });
  }

  addPostRecovery() {
    return this._formBuilder.group({
      date: [''],
      postRecoveryData: new FormArray([this.addPostRecoveryData()]),
    });
  }

  addPostRecoveryData() {
    return this._formBuilder.group({
      ltTime: [''],
      description: [''],
    });
  }

  getPostRecoveryDataControls(index: any) {
    const seqData = this.postRecDataForm
      .get('postRecData')
      .get(`postRecoveryDate.${index}.postRecoveryData`) as FormArray;
    return seqData;
  }

  getPostRecoveryControls() {
    return this.postRecDataForm.get('postRecData').get('postRecoveryDate') as FormArray;
  }

  onAddPostRecovery() {
    this.subDetails = [];
    this.subDetails = this.postRecDataForm.get('postRecData').get('postRecoveryDate') as FormArray;
    this.subDetails.push(this.addPostRecovery());
  }

  onAddPostRecoveryData(index: any) {
    this.subDetails = [];
    this.subDetails = this.postRecDataForm
      .get('postRecData')
      .get(`postRecoveryDate.${index}.postRecoveryData`) as FormArray;
    this.subDetails.push(this.addPostRecoveryData());
  }

  onDeletePostRecovery(index: number, vdrIndex: number) {
    const incidentVdrData = this.postRecDataForm
      .get('postRecData')
      .get(`postRecoveryDate.${index}.postRecoveryData`) as FormArray;
    incidentVdrData.removeAt(vdrIndex);
  }

  onDeletePostRecoveryData(index: number) {
    const incidentVdrData = this.postRecDataForm.get('postRecData').get('postRecoveryDate') as FormArray;
    incidentVdrData.removeAt(index);
  }

  /**
   * End:: PostRecoveryData Form Field
   * Begin:: Notification Form Field
   */

  addNotifyDataForm() {
    return this._formBuilder.group({
      notificationMasterId: [''],
      notifyAdditionalComments: [''],
      notificationDate: new FormArray([this.addNotify()]),
    });
  }

  addNotify() {
    return this._formBuilder.group({
      date: [''],
      notificationData: new FormArray([this.addNotifyData()]),
    });
  }

  addNotifyData() {
    return this._formBuilder.group({
      ltTime: [''],
      notificationParty: [''],
      methodUsed: [''],
    });
  }

  getNotifyDataControls(index: any) {
    const seqData = this.notifyDataForm
      .get('notifyData')
      .get(`notificationDate.${index}.notificationData`) as FormArray;
    return seqData;
  }

  getNotificationControls() {
    return this.notifyDataForm.get('notifyData').get('notificationDate') as FormArray;
  }

  onAddNotification() {
    this.subDetails = [];
    this.subDetails = this.notifyDataForm.get('notifyData').get('notificationDate') as FormArray;
    this.subDetails.push(this.addNotify());
  }

  onAddNotifyData(index: any) {
    this.subDetails = [];
    this.subDetails = this.notifyDataForm
      .get('notifyData')
      .get(`notificationDate.${index}.notificationData`) as FormArray;
    this.subDetails.push(this.addNotifyData());
  }

  onDeleteNotification(index: number, vdrIndex: number) {
    const incidentVdrData = this.notifyDataForm
      .get('notifyData')
      .get(`notificationDate.${index}.notificationData`) as FormArray;
    incidentVdrData.removeAt(vdrIndex);
  }

  onDeleteNotificationData(index: number) {
    const incidentVdrData = this.notifyDataForm.get('notifyData').get('notificationDate') as FormArray;
    incidentVdrData.removeAt(index);
  }

  /**
   * End:: incidentVdrData Form Field
   * End:: backgroud Form Field
   */

  addBackgroundDataForm() {
    return this._formBuilder.group({
      newBackground: [''],
      backgroundConditionId: [''],
      backgroundCondition: [''],
      description: [''],
      backgroundId: [''],
      isNew: ['0'],
      isContibutingFactor: [''],
      attachmentFieldName: [''],
    });
  }

  getCreateBackgroundField() {
    for (let i = 0; i < this.bgImage.length - 1; i++) {
      this.onAddBackgroundData();
    }
  }

  getSelectBackground(event: any) {
    const condition = event.value;
    this.backgroundConditionData = [];
    if (this.bgImage.length > 0) {
      this.bgImage.forEach((element: any) => {
        this.backgroundConditionData.push(element);
      });
    }

    if (condition.length) {
      condition.forEach((element: any) => {
        if (this.bgImage.indexOf(element) > -1) {
        } else {
          const indexCount = this.backgroundConditionData.indexOf(element);
          if (indexCount > -1) {
          } else {
            this.backgroundConditionData.push(element);
          }
        }
      });
    }
  }

  getCreateBackgroundCondition() {
    let index = 0;
    if (this.bgImage.length > 0) {
      index = this.bgImage.length - 1;
    }

    this.newBackgroundCondition = [];

    this.backgroundConditionData.forEach((element: any) => {
      if (this.bgImage.indexOf(element) > -1) {
        this.newBackgroundCondition.push(element);
      } else {
        this.newBackgroundCondition.push(element);
        const arrayLength = this.backgroundDataForm.get(`backgroundData`) as FormArray;
        let conditionId = '';
        this.backgroundDataArray.forEach((condition: any) => {
          if (element === condition.background) {
            conditionId = condition.bgcuid;
          }
        });
        if (arrayLength.length === index + 1 && index === 0) {
          this.backgroundDataForm.get(`backgroundData.${index}.backgroundCondition`).patchValue(element);
          this.backgroundDataForm.get(`backgroundData.${index}.backgroundConditionId`).patchValue(conditionId);
          this.backgroundDataForm.get(`backgroundData.${index}.isNew`).patchValue(0);
        } else if (arrayLength.length < index + 1) {
          this.onAddBackgroundData();
          this.backgroundDataForm.get(`backgroundData.${index}.backgroundCondition`).patchValue(element);
          this.backgroundDataForm.get(`backgroundData.${index}.backgroundConditionId`).patchValue(conditionId);
          this.backgroundDataForm.get(`backgroundData.${index}.isNew`).patchValue(0);
        } else if (arrayLength.length > this.backgroundConditionData.length) {
          const deleteIndex = arrayLength.length - 1;
          const backgroundFieldRemove = this.backgroundDataForm.get(`backgroundData`) as FormArray;
          backgroundFieldRemove.removeAt(deleteIndex);
        }
      }
      index++;
    });
  }

  getFilterImageData(index: any) {
    if (this.backgroundDataArray[index]) {
      return this.backgroundDataArray[index];
    }
  }

  getBackgroundControls() {
    return this.backgroundDataForm.get('backgroundData') as FormArray;
  }

  onAddBackgroundData() {
    this.subDetails = [];
    this.subDetails = this.backgroundDataForm.get('backgroundData') as FormArray;
    this.subDetails.push(this.addBackgroundDataForm());
  }

  getControls() {
    const crossReferenceFormF = this.crossReferenceForm.controls.crossRefToTMSA as FormArray;
    return crossReferenceFormF;
  }

  getActionControls() {
    const actionF = this.actionForm.controls.action as FormArray;
    return actionF;
  }

  onChangeProposedBy(event: any, index: any) {
    const proposedBy = event.value;
    const otherIndex = this.proposedByArray.indexOf(index);
    if (otherIndex > -1) {
      this.proposedByArray.splice(otherIndex, 1);
    } else {
      if (proposedBy === 'Officer') {
        this.proposedByArray.push(index);
      }
    }
  }

  onChangeAction(event: any, index: any) {
    const isImmediateCause = event.value;
    const otherIndex = this.isImmediateCauseArray.indexOf(index);
    if (otherIndex > -1) {
      this.isImmediateCauseArray.splice(otherIndex, 1);
    } else {
      if (isImmediateCause === 'Immediate Action') {
        this.isImmediateCauseArray.push(index);
      }
    }
  }

  changeAction(event: any, index: any) {
    const isImmediateCause = event;
    const otherIndex = this.isImmediateCauseArray.indexOf(index);
    if (otherIndex > -1) {
      this.isImmediateCauseArray.splice(otherIndex, 1);
    } else {
      if (isImmediateCause === 'Immediate Action') {
        this.isImmediateCauseArray.push(index);
      }
    }
  }

  isImmediateCause(index: any) {
    const otherIndex = this.isImmediateCauseArray.indexOf(index);
    if (otherIndex > -1) {
      return true;
    } else {
      return false;
    }
  }

  addInfoInvolving(): FormGroup {
    return this._formBuilder.group({
      addlInfoInvolvingInjuryId: [''],
      categoryId: [''],
      injuryOccuredId: [''],
      rankId: [''],
      age: [''],
      bodyPartAffId: [''],
      nationalityId: [''],
      timeOnBoard: [''],
      timeInRank: [''],
      timeInCompany: [''],
    });
  }

  getInfoInvolvingInjuryControls() {
    const infoInvolvingInjury = this.descriptionFormGroup.get(`addlInfoInvolvingInjury`) as FormArray;
    return infoInvolvingInjury;
  }

  onAddIInfoInvolving() {
    const InfoInvolvingInjury = 'addlInfoInvolvingInjury';
    this.subDetails = this.descriptionFormGroup.get(InfoInvolvingInjury) as FormArray;
    this.subDetails.push(this.addInfoInvolving());
  }

  onDeleteInfoInvolvingInjury(index: number) {
    const addlInfoInvolvingInjury = this.descriptionFormGroup.get('addlInfoInvolvingInjury') as FormArray;
    addlInfoInvolvingInjury.removeAt(index);
  }

  onAddNewTeamMember(index: any) {
    const teamMember = this.newTeamMemberArray.indexOf(index);
    if (teamMember > -1) {
      this.newTeamMemberArray.splice(teamMember, 1);
      this.investigationTeamForm
        .get(`investigationTeam`)
        .get(`investigationTeamMember.${index}.isNewTeamMember`)
        .patchValue(0);
    } else {
      this.newTeamMemberArray.push(index);
      this.investigationTeamForm
        .get(`investigationTeam`)
        .get(`investigationTeamMember.${index}.isNewTeamMember`)
        .patchValue(1);
    }
  }

  getNewTeamMember(index: any) {
    const teamMember = this.newTeamMemberArray.indexOf(index);
    if (teamMember > -1) {
      return true;
    } else {
      return false;
    }
  }

  getEqpOtherDetails(index: any) {
    const otherDetails = this.eqpOtherDetailsArray.indexOf(index);
    if (otherDetails > -1) {
      return true;
    } else {
      return false;
    }
  }

  getOtherBackGround(index: any) {
    const otherDetails = this.backgroundDetailsArray.indexOf(index);
    if (otherDetails > -1) {
      return true;
    } else {
      return false;
    }
  }

  getOtherDetails(event: any, index: any) {
    const other = event.value;
    const otherIndex = this.eqpOtherDetailsArray.indexOf(index);
    if (otherIndex > -1) {
      this.descriptionFormGroup.get(`addlInfoInvolvingEqpDamage.${index}.isOther`).patchValue(0);
      this.eqpOtherDetailsArray.splice(otherIndex, 1);
    } else {
      if (other === 'Other') {
        this.descriptionFormGroup.get(`addlInfoInvolvingEqpDamage.${index}.isOther`).patchValue(1);
        this.eqpOtherDetailsArray.push(index);
      }
    }
  }

  getOtherBackgroundDetails(event: any, index: any) {
    const other = event.value;
    const otherIndex = this.backgroundDetailsArray.indexOf(index);
    if (otherIndex > -1) {
      this.backgroundDataForm.get(`backgroundData.${index}.isNew`).patchValue(0);
      this.backgroundDetailsArray.splice(otherIndex, 1);
    } else {
      if (other === 'Other') {
        this.backgroundDataForm.get(`backgroundData.${index}.isNew`).patchValue(1);
        this.backgroundDetailsArray.push(index);
      }
    }
  }

  addlInfoDamage() {
    return this._formBuilder.group({
      addlInfoInvolvingInjuryEqpDamageId: [''],
      isOther: [false],
      newEquipment: [''],
      equipmentId: [''],
      equipmentCategoryId: [''],
      make: [''],
      model: [''],
    });
  }

  getInfoDamageControls() {
    const infoInvolvingInjury = this.descriptionFormGroup.get(`addlInfoInvolvingEqpDamage`) as FormArray;
    return infoInvolvingInjury;
  }

  onAddInfoDamage() {
    const InfoInvolvingEqpDamage = 'addlInfoInvolvingEqpDamage';
    this.subDetails = this.descriptionFormGroup.get(InfoInvolvingEqpDamage) as FormArray;
    this.subDetails.push(this.addlInfoDamage());
  }

  onDeleteInfoDamage(index: number) {
    const addlInfoInvolvingEqpDamage = this.descriptionFormGroup.get('addlInfoInvolvingEqpDamage') as FormArray;
    addlInfoInvolvingEqpDamage.removeAt(index);
  }

  getContactEventControls() {
    const analysisF = this.analysisFormGroup.controls.typeOfContactOrEvent as FormArray;
    return analysisF;
  }

  getBackTypeofContact() {
    this.typeContactView = !this.typeContactView;
    if (!this.typeContactView) {
      this.contactOrEventOptionActive = [];
      this.basicImmidateOptionActive = [];
      this.rootCauseOptionActive = [];
      this.correctiveCauseOptionActive = [];
      this.typeOfContactArray = [];
      this.basicCauseArray = [];
      this.rootCauseArray = [];
      const form = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
      form.clear();
      setTimeout(() => {
        this.move(10);
      }, 1);
    }
  }

  getAnalysisFormData(index: any) {
    if (this.analysisData) {
      if (this.analysisData[index]) {
        return this.analysisData[index].typeOfCntctId;
      }
    }
  }

  onAddTypeOfContactOrEventField() {
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    this.subDetails = this.analysisFormGroup.get(typeOfContactOrEvent) as FormArray;
    this.subDetails.push(this.addContactEventFields());
  }

  onAddBasicImmidateCauseField(index: any, basicImmediateIndex: any) {
    this.subDetails = this.analysisFormGroup.get(`typeOfContactOrEvent.${index}.basicImmediateCause`) as FormArray;
    this.subDetails.push(this.addBasicImmidateCause());
  }

  onAddBasicRootCauseField(index: any, basicImmediateIndex: any) {
    this.subDetails = this.analysisFormGroup.get(
      `typeOfContactOrEvent.${index}.basicImmediateCause.${basicImmediateIndex}.basicRootCause`
    ) as FormArray;
    this.subDetails.push(this.addRootCause());
  }

  addBasicImmediate() {
    return { basicImmediateCauseId: '', immediateCauseExplanation: '', basicRootCause: [this.addRootCauseRow()] };
  }

  addRootCauseRow() {
    return { basicRootCauseId: '', rootCauseExplanation: '', correctiveActionId: Array() };
  }

  addContactEventFields(): FormGroup {
    return this._formBuilder.group({
      typeOfCntctId: [''],
      contact: [''],
      typeOfContactCategory: [''],
      basicImmediateCause: new FormArray([this.addBasicImmidateCause()]),
    });
  }

  addprobablityReccData(): FormGroup {
    return this._formBuilder.group({
      probablityReccId: [''],
      probablityResultantRisk: [this.impact],
      probablityOfRecurrence: [''],
    });
  }

  addBasicImmidateCause(): FormGroup {
    return this._formBuilder.group({
      basicImmediateCauseId: [''],
      immediateCauseExplanation: [''],
      furtherExplanation: ['', Validators.required],
      basicRootCause: new FormArray([this.addRootCause()]),
    });
  }

  addRootCause(): FormGroup {
    return this._formBuilder.group({
      basicRootCauseId: [''],
      rootCauseExplanation: [''],
      furtherExplanation: ['', Validators.required],
      correctiveActionId: new FormArray([]),
    });
  }

  addCorrectiveCause(): FormGroup {
    return this._formBuilder.group({
      category: [''],
      correctiveAction: [''],
      correctiveActionId: new FormArray([]),
    });
  }

  actionFields(): FormGroup {
    return this._formBuilder.group({
      proposedBy: [''],
      type: ['', Validators.required],
      natureOfAction: ['', Validators.required],
      dueDate: [''],
      designationId: [''],
      email: [''],
      dateCompleted: [''],
      actionAccepted: [''],
      supportiveComments: [''],
      status: [''],
    });
  }

  onDeleteIngredient(index: number) {
    const crossReferenceFormF = this.crossReferenceForm.controls.crossRefToTMSA as FormArray;
    crossReferenceFormF.removeAt(index);
  }

  onDeleteAction(index: number) {
    const actionForm = this.actionForm.controls.action as FormArray;
    actionForm.removeAt(index);
  }

  getCrossReferencingControls() {
    const crossReferenceFormF = this.crossReferenceForm.controls.crossRefToTMSA as FormArray;
    return crossReferenceFormF;
  }

  addAnalysisData(): FormGroup {
    return this._formBuilder.group({
      analysisId: [''],
      questionId: [''],
      numberOfTimesRepeated: ['', Validators.required],
      briefDescription: ['', Validators.required],
      question: [''],
    });
  }

  onAddAnalysisData() {
    this.subDetails = this.analysisFormGroup.get(`analysisData`) as FormArray;
    this.subDetails.push(this.addAnalysisData());
  }

  getAnalysisDataControls() {
    const analysisData = this.analysisFormGroup.get(`analysisData`) as FormArray;
    return analysisData;
  }

  onDeleteAnalysisData(index: number) {
    const analysisData = this.analysisFormGroup.get(`analysisData`) as FormArray;
    analysisData.removeAt(index);
  }

  /**
   * Begin:: Type of Contact
   * Set Active Class to ContactorEvent List
   */
  getContactClass(contactEvent: any) {
    if (this.typeOfContactArray.indexOf(contactEvent) > -1) {
      return 'contact-table-incomplete';
    } else if (this.contactOrEventOptionActive.indexOf(contactEvent) !== -1) {
      return 'contact-table-active';
    }
  }

  /**
   * Set Active Class to ImmediateCause List
   */
  getImmidiateClass(basicImmediateCause: any, idx: any) {
    const basicImmediateCauseId = basicImmediateCause.immediateCauseId;
    const immediateCause = basicImmediateCause.immediateCause;
    const immediateId = basicImmediateCauseId + '_' + idx;
    if (this.basicCauseArray.indexOf(immediateCause) > -1) {
      return 'contact-table-incomplete';
    } else if (this.basicImmidateOptionActive[immediateId]) {
      return 'contact-table-active';
    }
  }

  /**
   * Set Active Class to RootCause List
   */
  getRootClass(rootCause: any, basicIndex: any, idx: any) {
    const rootCauseId = rootCause.basicRootCauseId;
    const basicRootCause = rootCause.basicRootCause;
    if (this.rootCauseArray.indexOf(basicRootCause) > -1) {
      return 'contact-table-incomplete';
    } else if (this.rootCauseOptionActive[rootCauseId + '_' + idx + '_' + basicIndex]) {
      return 'contact-table-active';
    }
  }

  /**
   * Set Active Class to Corrective List
   */
  getCorrectiveClass(correctiveCauseId: any, rootIndex: any, basicIndex: any, idx: any) {
    if (
      this.correctiveCauseOptionActive.indexOf(correctiveCauseId + '_' + idx + '_' + basicIndex + '_' + rootIndex) !==
      -1
    ) {
      return 'contact-table-active';
    }
  }

  /**
   * set Contact or event Cause Active
   */
  setActivateContactCause(contactTitle: any) {
    this.activeContactCause = contactTitle;
  }

  /**
   * set Immediate Cause as Active
   */
  setActivateImmediateCauses(basicImmediate: any, index: any) {
    this.activeImmediateCause = basicImmediate + '_' + index;
  }

  /**
   * set Immediate Cause as Active
   */
  setActivateRootCauses(basicRootCuaseId: any, basicIndex: any, index: any) {
    this.activeRootCause = basicRootCuaseId + '_' + index + '_' + basicIndex;
  }

  getBasicImmediateCause(contactEventValue: any, index: any) {
    const contactEvent = contactEventValue.typeOfContact;
    this.isRootCause = false;
    this.isCorrectiveCause = false;

    if (this.contactOrEventOptionActive.indexOf(contactEvent) !== -1) {
      const contactIndex = this.contactOrEventOptionActive.indexOf(contactEvent);
      this.contactOrEventOptionActive.splice(contactIndex, 1);
      if (this.basicImmidateOptionActive[index]) {
        this.basicImmidateOptionActive[index].splice(contactIndex, 1);
      }

      delete this.contactOrEventOptionActive[contactEvent];
      this.analysisFormData = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
      let typeOfContactIndex = 0;
      this.activeContactCause = '';
      this.analysisFormData.value.forEach((element: any) => {
        const contact = element.contact;
        if (contact === contactEvent) {
          this.analysisFormData.removeAt(typeOfContactIndex);
          this.typeOfContactRemove(contact);
        }
        typeOfContactIndex++;
      });
      this.causeAnalysisValidate();
    } else {
      this.setActivateContactCause(contactEventValue.typeOfContact);
      this.contactOrEventOptionActive.push(contactEvent);
      const contactIndex = this.contactOrEventOptionActive.indexOf(contactEvent);
      this.analysisFormData = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
      if (contactIndex >= 1) {
        this.onAddTypeOfContactOrEventField();
      }
      if (contactIndex === 0) {
        // this.analysisFormData.reset();
        this.onAddTypeOfContactOrEventField();
      }
      this.contactOrEventOptionList.forEach((element: any) => {
        const contact = element.typeOfContact;
        if (contact === contactEvent) {
          if (this.analysisFormData.value instanceof Array) {
            // this.analysisFormData.value[contactIndex].typeOfCntctId = element.typeOfContactId;
            // this.analysisFormData.value[contactIndex].contact = element.typeOfContact;
            this.analysisFormGroup
              .get(`typeOfContactOrEvent.${contactIndex}.typeOfCntctId`)
              .patchValue(element.typeOfContactId);

            this.analysisFormGroup
              .get(`typeOfContactOrEvent.${contactIndex}.contact`)
              .patchValue(element.typeOfContact);

            this.analysisFormGroup
              .get(`typeOfContactOrEvent.${contactIndex}.typeOfContactCategory`)
              .patchValue(element.typeOfContactCategory);

            this.getNearMissImmidateCause(element.typeOfContactId, index, contactIndex);
          } else {
            this.analysisFormData.value.typeOfCntctId = element.typeOfContactId;
            this.analysisFormData.value.contact = element.typeOfContact;
            this.getNearMissImmidateCause(element.typeOfContactId, index, contactIndex);
          }
        }
      });
    }
    this.causeAnalysisValidate();
  }

  inFormArray(contactIndex: any): FormArray {
    return this.analysisFormGroup.get(`typeOfContactOrEvent.${contactIndex}.basicImmediateCause`) as FormArray;
  }

  getRootCause(basicImmediate: any, contactActiveType: any, basicIndex: any, index: any) {
    const basicImmediateCauseId = basicImmediate.immediateCauseId;
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    this.isRootCause = true;
    this.isCorrectiveCause = false;
    const contactIndex = this.contactOrEventOptionActive.indexOf(contactActiveType.typeOfContact);

    if (this.basicImmidateOptionActive[basicImmediateCauseId + '_' + index]) {
      delete this.basicImmidateOptionActive[basicImmediateCauseId + '_' + index];
      const analysisFormData = this.inFormArray(contactIndex);

      this.activeImmediateCause = '';
      if (analysisFormData) {
        let immediateCauseIndex = 0;
        analysisFormData.value.forEach((analysisData: any) => {
          const immediateCause = analysisData.basicImmediateCauseId;
          if (immediateCause === basicImmediateCauseId) {
            this.immediateCauseRemove(analysisData.immediateCauseExplanation);
            analysisFormData.removeAt(immediateCauseIndex);
          }
          immediateCauseIndex++;
        });
      }
      this.causeAnalysisValidate();
    } else {
      const immediateId = basicImmediateCauseId + '_' + index;
      this.basicImmidateOptionActive[immediateId] = basicImmediate;
      this.setActivateImmediateCauses(basicImmediateCauseId, index);
      const formArray = this.analysisFormGroup.controls[typeOfContactOrEvent].value[contactIndex]
        .basicImmediateCause as FormArray;

      const basicFormArray = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause`
      ) as FormArray;

      const basicFormValue = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${0}.basicImmediateCauseId`
      ) as FormArray;

      if (formArray[0] == null) {
        this.onAddBasicImmidateCauseField(contactIndex, basicIndex);
      } else if (basicFormValue.value !== '') {
        this.onAddBasicImmidateCauseField(contactIndex, basicIndex);
      }
      this.ImmediateCauseId = basicImmediateCauseId;
      this.basicImmidateOptionList[index].forEach((element: any) => {
        const basicImmediateId = element.immediateCauseId;
        if (basicImmediateId === basicImmediateCauseId) {
          let formArrayIndex = 0;
          if (basicFormArray.value.length > 1) {
            formArrayIndex = basicFormArray.value.length - 1;
          }
          this.analysisFormGroup
            .get(`typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${formArrayIndex}.basicImmediateCauseId`)
            .patchValue(element.immediateCauseId);
          this.analysisFormGroup
            .get(`typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${formArrayIndex}.immediateCauseExplanation`)
            .patchValue(element.immediateCause);
          const furtherExplanation = this.getFilterImmediateCause(
            element.immediateCauseId,
            contactIndex,
            formArrayIndex
          );
          this.analysisFormGroup
            .get(`typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${formArrayIndex}.furtherExplanation`)
            .patchValue(furtherExplanation);

          // formArray[formArrayIndex].basicImmediateCauseId = element.immediateCauseId;
          // formArray[formArrayIndex].immediateCauseExplanation = element.immediateCause;
          this.getNearMissRootCause(basicImmediate, basicIndex, index, element.immediateCauseRelationId);
        }
      });
      this.causeAnalysisValidate();
    }
  }

  getCorrectiveCause(
    contactActiveType: any,
    currentRootCause: any,
    basicImmediate: any,
    basicIndex: any,
    rootCauseIndex: any,
    index: any
  ) {
    const rootCause = currentRootCause.basicRootCause;
    const rootCauseId = currentRootCause.basicRootCauseId;
    const immediateCauseId = basicImmediate.immediateCauseId;
    this.isRootCause = true;
    this.isCorrectiveCause = true;
    const indexing = rootCauseId + '_' + index + '_' + basicIndex;

    const contactIndex = this.contactOrEventOptionActive.indexOf(contactActiveType.typeOfContact);

    if (this.rootCauseOptionActive[indexing]) {
      delete this.rootCauseOptionActive[indexing];

      const basicImmediateData = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause`
      ) as FormArray;

      if (basicImmediateData) {
        let immediateFormIndex = 0;

        let basicRootIndex = 0;
        basicImmediateData.value.forEach((elements: any) => {
          const immediateId = elements.immediateCauseId;
          if (immediateId === immediateCauseId) {
            immediateFormIndex = basicRootIndex;
          }
          basicRootIndex++;
        });

        this.activeRootCause = '';
        const formArray = this.analysisFormGroup.get(
          `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause`
        ) as FormArray;

        let rootIndex = 0;
        formArray.value.forEach((element: any) => {
          if (element.rootCauseExplanation === rootCause) {
            formArray.removeAt(rootIndex);
            this.rootCauseRemove(rootCause);
          }
          rootIndex++;
        });
        this.causeAnalysisValidate();
      }
    } else {
      this.setActivateRootCauses(rootCauseId, basicIndex, index);
      this.rootCauseOptionActive[indexing] = currentRootCause;
      const basicImmediateData = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause`
      ) as FormArray;
      let immediateFormIndex = 0;

      let basicCauseIndex = 0;
      basicImmediateData.value.forEach((element: any) => {
        const immediateId = element.basicImmediateCauseId;
        if (immediateId === immediateCauseId) {
          immediateFormIndex = basicCauseIndex;
        }
        basicCauseIndex++;
      });

      const basicRootCause = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause`
      ) as FormArray;

      const basicRootCauseValue = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${0}.basicRootCauseId`
      ) as FormArray;

      if (basicRootCause.value.length < 1) {
        this.onAddBasicRootCauseField(contactIndex, immediateFormIndex);
      } else if (basicRootCauseValue.value !== '') {
        this.onAddBasicRootCauseField(contactIndex, immediateFormIndex);
      }

      this.rootCauseOptionList[immediateCauseId].forEach((element: any) => {
        if (element) {
          const basicRootCauseId = element.basicRootCauseId;
          if (basicRootCauseId === rootCauseId) {
            let formArrayIndex = 0;

            if (basicRootCause.value.length > 1) {
              formArrayIndex = basicRootCause.value.length - 1;
            }

            this.analysisFormGroup
              .get(
                `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${formArrayIndex}.basicRootCauseId`
              )
              .patchValue(element.basicRootCauseId);
            // formArray[formArrayIndex].basicRootCauseId = element.basicRootCauseId;
            this.analysisFormGroup
              .get(
                `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${formArrayIndex}.rootCauseExplanation`
              )
              .patchValue(element.basicRootCause);

            const furtherExplanation = this.getFilterRootCause(
              element.basicRootCauseId,
              contactIndex,
              immediateFormIndex,
              formArrayIndex
            );

            this.analysisFormGroup
              .get(
                `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${formArrayIndex}.furtherExplanation`
              )
              .patchValue(furtherExplanation);

            // formArray[formArrayIndex].basicRootCauseId = element.basicRootCauseId;
            // formArray[formArrayIndex].rootCauseExplanation = element.basicRootCause;
            this.getNearMissCorrectiveCause(contactActiveType, basicImmediate, element, formArrayIndex, index);
          }
        }
      });
      this.causeAnalysisValidate();
    }
  }

  setCorrectiveCause(
    contactActiveType: any,
    basicImmidate: any,
    rootCauseData: any,
    correctiveData: any,
    rootIndex: any,
    basicIndex: any,
    idx: any
  ) {
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    const correctiveIndexing = correctiveData.correctiveActionId + '_' + idx + '_' + basicIndex + '_' + rootIndex;
    const immediateCauseId = basicImmidate.immediateCauseId;
    const rootCauseId = rootCauseData.basicRootCauseId;
    const correctiveActionId = correctiveData.correctiveActionId;
    const contactTypeIndex = this.contactOrEventOptionActive.indexOf(contactActiveType.typeOfContact);

    if (this.correctiveCauseOptionActive.indexOf(correctiveIndexing) !== -1) {
      this.correctiveActionId = '';
      const contactIndex = this.correctiveCauseOptionActive.indexOf(correctiveIndexing);
      this.correctiveCauseOptionActive.splice(contactIndex, 1);

      const basicImmediateData = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactTypeIndex}.basicImmediateCause`
      ) as FormArray;

      if (basicImmediateData) {
        let immediateFormIndex = 0;
        let basicRootCauseId = 0;
        let correctiveId = 0;
        let bIndex = 0;
        basicImmediateData.value.forEach((element: any) => {
          const immediateId = element.basicImmediateCauseId;
          if (immediateId === immediateCauseId) {
            immediateFormIndex = bIndex;
            let rIndex = 0;
            const basicRootCause = element.basicRootCause;
            basicRootCause.forEach((rootElmenet: any) => {
              const rootId = rootElmenet.basicRootCauseId;
              if (rootCauseId === rootId) {
                basicRootCauseId = rIndex;
                const basicCorrectiveCause = rootElmenet.correctiveActionId;
                for (let x = 0; x < basicCorrectiveCause.length; x++) {
                  if (correctiveActionId === basicCorrectiveCause[x]) {
                    correctiveId = x;
                  }
                }
              }
              rIndex++;
            });
          }
          bIndex++;
        });

        const formArray = this.analysisFormGroup.get(
          `typeOfContactOrEvent.${contactTypeIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${basicRootCauseId}.correctiveActionId`
        ) as FormArray;
        formArray.removeAt(correctiveId);
        this.causeAnalysisValidate();
      }
    } else {
      this.correctiveActionId = correctiveIndexing;
      this.correctiveCauseOptionActive.push(this.correctiveActionId);

      const basicImmediateData = this.analysisFormGroup.controls[typeOfContactOrEvent].value[contactTypeIndex]
        .basicImmediateCause as FormArray;
      let immediateFormIndex = 0;
      let basicRootCauseId = 0;
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < basicImmediateData.length; i++) {
        const immediateId = basicImmediateData[i].basicImmediateCauseId;
        if (immediateId === immediateCauseId) {
          immediateFormIndex = i;
          const basicRootCause = basicImmediateData[i].basicRootCause;
          // tslint:disable-next-line:prefer-for-of
          for (let j = 0; j < basicRootCause.length; j++) {
            const rootId = basicRootCause[j].basicRootCauseId;
            if (rootCauseId === rootId) {
              basicRootCauseId = j;
            }
          }
        }
      }

      const formArray = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactTypeIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${basicRootCauseId}.correctiveActionId`
      ) as FormArray;
      if (correctiveData.correctiveActionId) {
        formArray.push(new FormControl(correctiveData.correctiveActionId));
      }
    }
    this.causeAnalysisValidate();
  }

  getNearMissContactorEvent() {
    if (this.cacheServiceService.typeOfContact) {
      this.contactOrEventOptionList = this.cacheServiceService.typeOfContact;
    } else {
      this.nearMissService.getContactOrEvent().subscribe(
        (response) => {
          this.contactList = response;
          // log.debug(`${JSON.stringify(response)}`);
          if (response) {
            this.contactOrEventOptionList = response;
            this.cacheServiceService.setTypeOfContact(response);
          } else {
            this.contactOrEventOptionList = [];
            const message = 'Contact or Event list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  getNearMissImmidateCause(typeOfContactId: any, index: any, contactIndex: any) {
    if (this.cacheServiceService.basicImmediateCause) {
      this.filterImmediateCause(index, contactIndex);
    } else {
      this.nearMissService.getBasicImmediate().subscribe(
        (response) => {
          this.contactList = response;
          // log.debug(`${JSON.stringify(response)}`);
          this.cacheServiceService.setBasicImmediateCause(response);
          this.filterImmediateCause(index, contactIndex);
          if (response != null) {
            this.contactOrEventOptionList[index].immidiateCauses = response;
            this.basicImmidateOptionList[index] = response;
          } else {
            const message = 'basic Immidate list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  getNearMissRootCause(basicImmediateCause: any, basicIndex: any, typeOfContactIndex: any, immidiateCauseIndex: any) {
    if (this.cacheServiceService.basicRootCause) {
      this.filterRootCause(basicImmediateCause, typeOfContactIndex, immidiateCauseIndex);
    } else {
      this.nearMissService.getRootCause().subscribe(
        (response) => {
          this.contactList = response;
          // log.debug(`${JSON.stringify(response)}`);
          if (response) {
            this.cacheServiceService.setBasicRootCause(response);
            this.filterRootCause(basicImmediateCause, typeOfContactIndex, immidiateCauseIndex);
          } else {
            const message = 'RootCause list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  getNearMissCorrectiveCause(
    typeOfContact: any,
    basicImmediate: any,
    basicRootCause: any,
    basicRootCauseIndex: any,
    contactIndex: any
  ) {
    if (this.cacheServiceService.basicCorrectiveCause) {
      this.filterCorrectiveCause(typeOfContact, basicImmediate, basicRootCause, contactIndex);
    } else {
      this.nearMissService.getCorrectiveCause().subscribe(
        (response) => {
          this.contactList = response;
          // log.debug(`${JSON.stringify(response)}`);
          if (response) {
            this.cacheServiceService.setBasicCorrectiveCause(response);
            this.filterCorrectiveCause(typeOfContact, basicImmediate, basicRootCause, contactIndex);
            // this.correctiveCauseOptionList[basicRootCauseId] = response;
          } else {
            const message = 'RootCause list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  getNearMissCauseTree() {
    if (this.cacheServiceService.causeTree) {
      this.causeTree = this.cacheServiceService.causeTree;
    } else {
      this.nearMissService.getCauseTree().subscribe(
        (response) => {
          this.causeTree = response;
          if (response) {
            // this.correctiveCauseOptionList = response;
            this.cacheServiceService.setCauseTree(response);
          } else {
            const message = 'RootCause list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  filterImmediateCause(typeOfContactIndex: any, contactIndex: any) {
    const typeData = this.cacheServiceService.basicImmediateCause;
    const causeTree = this.cacheServiceService.causeTree;
    this.filterBasicImmediateCause = [];
    causeTree.forEach((element: any) => {
      if (element.typeOfContactId === typeOfContactIndex) {
        const immediateArray = element.immediateCause;
        immediateArray.forEach((element2: any) => {
          const immediateIndex = element2.basicImmediateCauseId - 1;
          if (typeData[immediateIndex]) {
            this.filterBasicImmediateCause.push(typeData[immediateIndex]);
          }
        });
      }
    });
    // this.contactOrEventOptionList[typeOfContactIndex].immidiateCauses = this.filterBasicImmediateCause;
    this.basicImmidateOptionList[typeOfContactIndex] = this.filterBasicImmediateCause;
  }

  filterRootCause(basicImmediateCause: any, typeOfContactIndex: any, immidiateCauseIndex: any) {
    const rootData = this.cacheServiceService.basicRootCause;
    const causeTree = this.cacheServiceService.causeTree;
    this.filterBasicRootCause = [];
    causeTree.forEach((element: any) => {
      if (element.typeOfContactId === typeOfContactIndex) {
        const immediateArray = element.immediateCause;
        immediateArray.forEach((element2: any) => {
          if (immidiateCauseIndex === element2.basicImmediateCauseId) {
            const rootArray = element2.basicRootCause;
            rootArray.forEach((element3: any) => {
              const rootIndex = element3.basicRootCauseId - 1;
              if (rootData[rootIndex]) {
                this.filterBasicRootCause.push(rootData[rootIndex]);
              }
            });
          }
        });
      }
    });
    // this.contactOrEventOptionList[typeOfContactIndex].immidiateCauses = this.filterBasicImmediateCause;
    this.rootCauseOptionList[basicImmediateCause.immediateCauseId] = this.filterBasicRootCause;
  }

  filterCorrectiveCause(typeOfContact: any, basicImmediateCause: any, rootCause: any, typeOfContactIndex: any) {
    const causeTree = this.cacheServiceService.causeTree;
    const basicCorrectiveCause = this.cacheServiceService.basicCorrectiveCause;
    this.filterBasicCorrectiveCause = [];
    causeTree.forEach((element: any) => {
      if (element.typeOfContactId === typeOfContactIndex) {
        const immediateArray = element.immediateCause;
        immediateArray.forEach((element2: any) => {
          if (basicImmediateCause.immediateCauseRelationId === element2.basicImmediateCauseId) {
            const rootArray = element2.basicRootCause;
            rootArray.forEach((element3: any) => {
              if (element3.basicRootCauseId === rootCause.basicRootCauseRelationId) {
                const correctiveArray = element3.correctiveAction;
                correctiveArray.forEach((element4: any) => {
                  if (basicCorrectiveCause[element4]) {
                    this.filterBasicCorrectiveCause.push(basicCorrectiveCause[element4]);
                  }
                });
              }
            });
          }
        });
      }
    });
    this.correctiveCauseOptionList[rootCause.basicRootCauseId] = this.filterBasicCorrectiveCause;
  }

  onFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        this.images.push(event.target.files[i] as File);
        this.attachmentFormGroup.patchValue({
          fileSource: this.images,
        });
      }
    }
  }

  filterStages(event: any, index: any) {
    const elementId = event.value;
    let relationalId = '';
    this.elementData = this.incidentCacheService.elementData;

    this.elementData.forEach((element: any) => {
      if (element.elementId === elementId) {
        relationalId = element.elementRelationId;
      }
    });
    this.relationalData = this.incidentCacheService.crossReferenceRelationData;
    this.relationalData.forEach((elements: any) => {
      if (elements[0].elementId === relationalId) {
        this.relationalId = elements[0].stageId;
      }
    });

    this.stageArray = [];

    this.incidentCacheService.stageData.forEach((element: any) => {
      const stageId = element.stageRelationId;
      if (this.relationalId.indexOf(stageId) > -1) {
        this.stageArray.push(element);
      }
    });
    this.stageData[index] = this.stageArray;
  }

  filterSpecificKPI(event: any, index: any) {
    const stageId = event.value;

    this.stageArray = [];
    this.incidentCacheService.stageData.forEach((element: any) => {
      const stage = element.stageId;
      if (stageId === stage) {
        this.stageArray.push(element);
      }
    });
    this.spacificData[index] = this.stageArray;
  }

  getFilterStages(event: any, index: any) {
    const elementId = event;
    let relationalId = '';
    this.elementData = this.incidentCacheService.elementData;

    this.elementData.forEach((element: any) => {
      if (element.elementId === elementId) {
        relationalId = element.elementRelationId;
      }
    });
    this.relationalData = this.incidentCacheService.crossReferenceRelationData;
    this.relationalData.forEach((elements: any) => {
      if (elements[0].elementId === relationalId) {
        this.relationalId = elements[0].stageId;
      }
    });

    this.stageArray = [];
    this.incidentCacheService.stageData.forEach((element: any) => {
      const stageId = element.stageRelationId;
      if (this.relationalId.indexOf(stageId) > -1) {
        this.stageArray.push(element);
      }
    });
    this.stageData[index] = this.stageArray;
  }

  getFilterSpecificKPI(event: any, index: any) {
    const stageId = event;

    this.stageArray = [];
    this.incidentCacheService.stageData.forEach((element: any) => {
      const stage = element.stageId;
      if (stageId === stage) {
        this.stageArray.push(element);
      }
    });
    this.spacificData[index] = this.stageArray;
  }

  getFilterImmediateCause(immediateCauseId: any, typeIndex: any, basicIndex: any) {
    if (this.incidentData) {
      const immediateCuase = this.incidentData.typeOfContactOrEvent;
      if (immediateCuase) {
        if (immediateCuase[typeIndex]) {
          const basicCause = immediateCuase[typeIndex].basicImmediateCause;
          if (basicCause[basicIndex]) {
            const basicImmediateCauseId = basicCause[basicIndex].basicImmediateCauseId;
            if (basicImmediateCauseId === immediateCauseId) {
              return basicCause[basicIndex].basicImmediateCauseExplanation;
            } else {
              return '';
            }
          }
        }
      }
    }
  }

  getFilterRootCause(rootCauseId: any, typeIndex: any, basicIndex: any, rootIndex: any) {
    if (this.incidentData) {
      const immediateCuase = this.incidentData.typeOfContactOrEvent;
      if (immediateCuase) {
        if (immediateCuase[typeIndex]) {
          const basicCause = immediateCuase[typeIndex].basicImmediateCause;
          if (basicCause[basicIndex]) {
            const basicRootCause = basicCause[basicIndex].basicRootCause;
            if (basicRootCause[rootIndex]) {
              const basicRootCauseId = basicRootCause[rootIndex].basicRootCauseId;
              if (rootCauseId === basicRootCauseId) {
                return basicRootCause[rootIndex].basicRootCauseExplanation;
              } else {
                return '';
              }
            }
          }
        }
      }
    }
  }

  causeAnalysisValidate() {
    const datas = this.analysisFormGroup.value.typeOfContactOrEvent;
    let index = 0;
    const contact = 'contact';
    const immediateCauseExplanation = 'immediateCauseExplanation';
    const rootCauseExplanation = 'rootCauseExplanation';
    datas.forEach((element: any) => {
      if (element.typeOfCntctId) {
        for (const previewKey in element) {
          if (element.basicImmediateCause.length > 0) {
            if (previewKey === 'basicImmediateCause') {
              const basicImmediate = datas[index][previewKey];
              if (basicImmediate) {
                let index2 = 0;
                basicImmediate.forEach((elements: any) => {
                  const basicImmediateCauseId = elements.basicImmediateCauseId;
                  if (basicImmediateCauseId) {
                    const contactType = datas[index][contact];
                    const typeIndex = this.typeOfContactArray.indexOf(contactType);
                    if (typeIndex > -1) {
                      this.typeOfContactArray.splice(typeIndex, 1);
                    }
                    for (const basicCause in elements) {
                      if (datas[index][previewKey][index2][basicCause]) {
                        if (basicCause === 'basicRootCause') {
                          const basicRootCause = datas[index][previewKey][index2][basicCause];
                          let index3 = 0;
                          if (basicRootCause.length > 0) {
                            basicRootCause.forEach((element3: any) => {
                              const basicRootCauseId = element3.basicRootCauseId;
                              if (basicRootCauseId) {
                                const causeExplanation = datas[index][previewKey][index2][immediateCauseExplanation];
                                const basicIndex = this.basicCauseArray.indexOf(causeExplanation);
                                if (basicIndex > -1) {
                                  this.basicCauseArray.splice(basicIndex, 1);
                                }
                                if (element3.correctiveActionId.length > 0) {
                                  const explanation = basicRootCause[index3][rootCauseExplanation];
                                  const rootIndex = this.rootCauseArray.indexOf(explanation);
                                  if (rootIndex > -1) {
                                    this.rootCauseArray.splice(rootIndex, 1);
                                  }
                                } else {
                                  const explanation = basicRootCause[index3][rootCauseExplanation];
                                  if (this.rootCauseArray.indexOf(explanation) < 0) {
                                    this.rootCauseArray.push(explanation);
                                  }
                                }
                              } else {
                                const causeExplanation = datas[index][previewKey][index2][immediateCauseExplanation];
                                if (this.basicCauseArray.indexOf(causeExplanation) < 0) {
                                  this.basicCauseArray.push(causeExplanation);
                                }
                              }
                              index3++;
                            });
                          } else {
                            const causeExplanation = datas[index][previewKey][index2][immediateCauseExplanation];
                            if (this.basicCauseArray.indexOf(causeExplanation) < 0) {
                              this.basicCauseArray.push(causeExplanation);
                            }
                          }
                        }
                      }
                    }
                  } else {
                    const contactType = datas[index][contact];
                    if (this.typeOfContactArray.indexOf(contactType) < 0) {
                      this.typeOfContactArray.push(contactType);
                    }
                  }
                  index2++;
                });
              } else {
              }
            }
          } else {
            const contactType = datas[index][contact];
            if (this.typeOfContactArray.indexOf(contactType) < 0) {
              this.typeOfContactArray.push(contactType);
            }
          }
        }
      }
      index++;
    });
  }

  typeOfContactRemove(type: any) {
    const typeIndex = this.typeOfContactArray.indexOf(type);
    if (typeIndex > -1) {
      this.typeOfContactArray.splice(typeIndex, 1);
    }
  }

  immediateCauseRemove(type: any) {
    const typeIndex = this.basicCauseArray.indexOf(type);
    if (typeIndex > -1) {
      this.basicCauseArray.splice(typeIndex, 1);
    }
  }

  rootCauseRemove(type: any) {
    const typeIndex = this.rootCauseArray.indexOf(type);
    if (typeIndex > -1) {
      this.rootCauseArray.splice(typeIndex, 1);
    }
  }

  get getCauseValidate() {
    if (this.typeOfContactArray.length > 0) {
      return true;
    } else {
      if (this.basicCauseArray.length > 0) {
        return true;
      } else {
        if (this.rootCauseArray.length > 0) {
          return true;
        } else {
          return false;
        }
      }
    }
  }

  // Get port list data
  getNearMissPortList() {
    this.nearMissService.getPortList().subscribe(
      (response) => {
        this.contactList = response;
        // log.debug(`${JSON.stringify(response)}`);
        if (response) {
          this.portList = response;
        } else {
          const message = 'RootCause list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Task Involved list data
  getNearMissTastList() {
    this.nearMissService.getTaskList().subscribe(
      (response) => {
        this.contactList = response;
        // log.debug(`${JSON.stringify(response)}`);
        if (response) {
          this.taskList = response;
        } else {
          const message = 'Task list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Location list data
  getNearMissLocationList() {
    this.nearMissService.getLocationList().subscribe(
      (response) => {
        if (response) {
          this.locationList = response;
        } else {
          const message = 'Task list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  /**
   * End:: Type of Contact
   */

  // Get Background Condition data
  getBackgroundCondition() {
    this.incidentService.getBackgroundCondition().subscribe(
      (response) => {
        // log.debug(`${JSON.stringify(response)}`);
        if (response) {
          this.backgroundDataArray = response;
        } else {
          const message = 'background image data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Background Condition data
  getvessels() {
    this.incidentService.getVessels().subscribe(
      (response) => {
        // log.debug(`${JSON.stringify(response)}`);
        if (response) {
          this.vesselTypeData = response;
        } else {
          const message = 'vessels data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Event Impact data
  getEventImpact() {
    this.incidentService.getEventImpactType().subscribe(
      (response) => {
        if (response) {
          this.eventImpactType = response;
        } else {
          const message = 'Event Impact data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Injury Category data
  getInjuryCategory() {
    this.incidentService.getInjuryCategory().subscribe(
      (response) => {
        if (response) {
          this.injuryCategory = response;
        } else {
          const message = 'Event Impact data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Injury Occured data
  getInjuryOccuredBy() {
    this.incidentService.getInjuryOccuredBy().subscribe(
      (response) => {
        if (response) {
          this.injuryOccured = response;
        } else {
          const message = 'Event Impact data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Rank data
  getRankList() {
    this.incidentService.getRank().subscribe(
      (response) => {
        if (response) {
          this.rankList = response;
        } else {
          const message = 'Rank data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Body part affected data
  getBodyPartEffect() {
    this.incidentService.getBodyPartEffected().subscribe(
      (response) => {
        if (response) {
          this.bodyPartAffIdList = response;
        } else {
          const message = 'Rank data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Nationality data
  getNationality() {
    this.incidentService.getNationality().subscribe(
      (response) => {
        if (response) {
          this.nationalityList = response;
        } else {
          const message = 'nationality data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Equipment data
  getEquipment() {
    this.incidentService.getEquipment().subscribe(
      (response) => {
        if (response) {
          this.equipmentList = response;
        } else {
          const message = 'equipmentList data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Equipment Category data
  getEquipmentCategory() {
    this.incidentService.getEquipmentCategory().subscribe(
      (response) => {
        if (response) {
          this.equipmentCategoryList = response;
        } else {
          const message = 'equipment Category List data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Investigation Category data
  getInvestigation() {
    this.incidentService.getInvestigation().subscribe(
      (response) => {
        if (response) {
          this.investigatorsList = response;
        } else {
          const message = 'investigators List data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Position Category data
  getByPosition() {
    const poition = 'TM';
    this.incidentService.getByPosition(poition).subscribe(
      (response) => {
        if (response) {
          this.teamMemberList = response;
        } else {
          const message = 'teamMemberList List data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get User  data
  getAllUsers() {
    this.incidentService.getAllUsers().subscribe(
      (response) => {
        if (response) {
          this.userList = response;
          this.userList.forEach((element: any) => {
            const user = {
              uid: element.userId,
              name: element.firstname,
            };
            this.options.push(user);
          });
        } else {
          const message = 'user List data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get User  data
  getLocationData() {
    this.incidentService.getLocation().subscribe(
      (response) => {
        if (response) {
          this.locationList = response;
        } else {
          const message = 'Location List data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get company  data
  getAllCompany() {
    this.incidentService.getAllCompany().subscribe(
      (response) => {
        if (response) {
          this.companyList = response;
          log.debug('this.companyList: ', this.companyList);
        } else {
          const message = 'Comapny List data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Analysis Of Similar Incident  data
  getAnalysisOfSimilarIncident() {
    this.incidentService.getAnalysisOfSimilarIncident().subscribe(
      (response) => {
        if (response) {
          this.similarIncident = response;
        } else {
          const message = 'similar Incident List data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Designation Condition data
  getDesignation() {
    this.incidentService.getDesignation().subscribe(
      (response) => {
        if (response) {
          this.designationList = response;
          this.designationList.forEach((element: any) => {
            const designation = {
              uid: element.duid,
              name: element.designation,
            };
            this.designationData.push(designation);
          });
        } else {
          const message = 'Designation data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Incident Data by Id
  getIncidentById(incidentId: any) {
    this.incidentService.getIncidentById(incidentId).subscribe(
      (response) => {
        if (response) {
          this.incidentData = response;
          this.setIncidentData();
          this.incidentCacheService.setAnalysisData(this.analysisFormGroup.value);
          this.analysisData = this.incidentCacheService.analysisForm.typeOfContactOrEvent;
        } else {
          const message = 'Incident data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Vessel list data
  getCrossReferenceTMSA() {
    this.incidentService.getCrossReferenceTMSA().subscribe(
      (response) => {
        if (response) {
          this.incidentCacheService.setCrossReferenceRelationData(response);
        } else {
          const message = 'cross reference relation not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Eelement Data list data
  getElement() {
    this.incidentService.getElements().subscribe(
      (response) => {
        if (response) {
          this.elementData = response[0];
          this.incidentCacheService.setElementData(this.elementData);
        } else {
          const message = 'element data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Stage Data list data
  getStageData() {
    this.incidentService.getStage().subscribe(
      (response) => {
        if (response) {
          // this.stageData = response[0];
          this.incidentCacheService.setStageData(response[0]);
        } else {
          const message = 'stage not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Specific KPI Data list data
  getSpacificKPIData(event: any) {
    const suid = event.value;
    this.incidentService.getSpecificKPI(suid).subscribe(
      (response) => {
        if (response) {
          // this.spacificData = response;
        } else {
          const message = 'Specific KPI list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get All Type List data
  getAllTypeList() {
    const type = 'Report Type';
    this.incidentService.getAllType(type).subscribe(
      (response) => {
        if (response) {
          this.impactList = response.impactList;
          this.actionStatus = response.status;
          this.observedBy = response.observedBy;
          this.proposedBy = response.proposedBy;
          this.department = response.department;
        } else {
          const message = 'all type list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Report Type List data
  getSeverityLevel(type: any) {
    this.incidentService.getFormFeed(type).subscribe(
      (response) => {
        if (response) {
          this.severityArray = response;
          this.stageArray = [];
          this.severityArray.forEach((element: any) => {
            if (element.module === 'incident') {
              this.stageArray.push(element);
            }
          });
          this.severityLevel = this.stageArray;
        } else {
          const message = 'severity level not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }
  // Get Report Type List data
  getProbability(type: any) {
    this.incidentService.getFormFeed(type).subscribe(
      (response) => {
        if (response) {
          this.probability = response;
        } else {
          const message = 'probability list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Sevelrity injury and damage data List data
  getSeverityLevelData(type: any) {
    this.incidentService.getFormFeed(type).subscribe(
      (response) => {
        if (response) {
          this.probability = response;
        } else {
          const message = 'probability list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Sevelrity injury and damage data List data
  getSeverityOfConsequences() {
    const severity = this.severityValue;
    const impact = this.impactSelectedArray;
    this.incidentService.getSeverityofConsequence(severity, impact).subscribe(
      (response) => {
        this.stageArray = response;
        if (response) {
          this.stageArray.forEach((element: any) => {
            if (element.eventImpact === 'People') {
              this.peopleImpact = element.descriptions;
              this.descriptionFormGroup.get(`personalInjury`).patchValue(element.severityOfConsequencesId);
            }

            if (element.eventImpact === 'Environment') {
              this.environmentImpact = element.descriptions;
              this.descriptionFormGroup.get(`environment`).patchValue(element.severityOfConsequencesId);
            }

            if (element.eventImpact === 'Assets/Property/Financial') {
              this.assetsImpact = element.descriptions;
              this.descriptionFormGroup.get(`propertyDamage`).patchValue(element.severityOfConsequencesId);
            }

            if (element.eventImpact === 'Reputation/Business') {
              this.businessImpact = element.descriptions;
              this.descriptionFormGroup.get(`businessimpact`).patchValue(element.severityOfConsequencesId);
            }
          });
        } else {
          const message = 'Severity of Consequences not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  deleteSeqImage(sequenceOfEventsAttachmentId: any) {
    this.incidentService.getSeqImageDelete(sequenceOfEventsAttachmentId).subscribe(
      (response) => {
        const message = 'Incident Attachment deleted successfully!';
        this.getIncidentById(this.incidentId);
        this.notifyService.showError(message, this.title);
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  private createOverviewForm() {
    this.overViewFormGroup = this._formBuilder.group({
      vesselId: ['', Validators.required],
      vesselName: [''],
      portId: [''],
      incidentTitle: [''],
      userId: [''],
      latLong: [''],
      latitude: [''],
      longitude: [''],
      reportId: [''],
      vesselLocation: [''],
      dateOfIncident: [''],
      timeOfIncident: [''],
      ltTimeOfIncident: [''],
      dateOfReport: [''],
      briefDescription: [''],
      description: ['', Validators.required],
      taskId: [''],
      locationId: [''],
    });
  }

  private createDescriptionForm() {
    this.descriptionFormGroup = this._formBuilder.group({
      stopCardIssued: [''],
      details: [''],
      eventImpact: new FormArray([]),
      eventId: [''],
      severityOfConsequences: [''],
      severityLevelOfImpactId: [''],
      severityLevel: [''],
      personalInjury: [''],
      propertyDamage: [''],
      environment: [''],
      businessimpact: [''],
      additionalComments: [''],
      probablityReccData: this.addprobablityReccData(),
      resultantRisk: [this.impact],
      severityAdditionalComments: [''],
      isAddlInfoInvolvingInjury: [''],
      isAddlInfoInvolvingEqpDamage: [''],
      isIncidentVdrData: [''],
      addlInfoInvolvingInjury: new FormArray([this.addInfoInvolving()]),
      addlInfoInvolvingEqpDamage: new FormArray([this.addlInfoDamage()]),
      // severityLevelImpact: this.addseverityLevelImpact()
    });
  }

  private createAnalysisForm() {
    this.analysisFormGroup = this._formBuilder.group({
      severityLevel: [''],
      probablityReccData: this.addprobablityReccData(),
      typeOfContactOrEvent: new FormArray([this.addContactEventFields()]),
      analysisData: new FormArray([this.addAnalysisData()]),
    });
  }

  private investigationForm() {
    this.investigationTeamForm = this._formBuilder.group({
      investigationTeam: this.addinvestigationForm(),
    });
  }

  private contactEstForm() {
    this.contactEstablishForm = this._formBuilder.group({
      intContact: new FormArray([this.addIntContactForm()]),
      extContact: new FormArray([this.addExtContactForm()]),
    });
  }

  private incidentSeqForm() {
    this.incidentSeqDataForm = this._formBuilder.group({
      incidentSeqData: new FormArray([this.addincidentSeqForm()]),
      seqOfEventAttachments: [''],
    });
  }

  private incidentVdrForm() {
    this.incidentVdrDataForm = this._formBuilder.group({
      incidentVdrData: this.addIncidentVdrForm(),
      reviewOfVdrDataAttachments: [''],
    });
  }

  private postRecForm() {
    this.postRecDataForm = this._formBuilder.group({
      postRecData: this.addpostRecDataForm(),
      postRecoveryMeasuresAttachments: [''],
    });
  }

  private notifyForm() {
    this.notifyDataForm = this._formBuilder.group({
      notifyData: this.addNotifyDataForm(),
      notificationAttachments: [''],
    });
  }

  private breachForm() {
    this.breachDataForm = this._formBuilder.group({
      breachData: this.addBreachForm(),
      breachAttachments: [''],
    });
  }

  private backgroundForm() {
    this.backgroundDataForm = this._formBuilder.group({
      backgroundData: new FormArray([this.addBackgroundDataForm()]),
    });
  }

  private createCrossReferencing() {
    this.crossReferenceForm = this._formBuilder.group({
      crossRefToTMSA: new FormArray([this.addSubdetails()]),
    });
  }

  private createAction() {
    this.actionForm = this._formBuilder.group({
      action: new FormArray([this.actionFields()]),
      actionAttachments: [''],
    });
  }

  private communicationForm() {
    this.communicationDataForm = this._formBuilder.group({
      vesselId: [''],
      portId: [''],
      incidentTitle: [''],
      observedBy: [''],
      latLong: [''],
      latitude: [''],
      isLatLong: [''],
      longitude: [''],
      vesselLocation: [''],
      dateOfReport: [''],
      severityAdditionalComments: [''],
      isAddlInfoInvolvingInjury: [''],
      isAddlInfoInvolvingEqpDamage: [''],
      isIncidentVdrData: [''],
      addlInfoInvolvingInjury: new FormArray([this.addInfoInvolving()]),
      addlInfoInvolvingEqpDamage: new FormArray([this.addlInfoDamage()]),
      userId: ['', Validators.required],
      dateOfIncident: [''],
      timeOfIncident: [''],
      briefDescription: [''],
      description: [''],
      taskId: [''],
      locationId: [''],
      stopCardIssued: [''],
      details: [''],
      eventImpact: [''],
      eventId: [''],
      severityLevel: [''],
      probablityOfHappening: [''],
      resultantRisk: [''],
      severityOfConsequences: [''],
      probablityReccData: this.addprobablityReccData(),
      typeOfContactOrEvent: new FormArray([this.addContactEventFields()]),
      analysisData: new FormArray([this.addAnalysisData()]),
      crossRefToTMSA: new FormArray([this.addSubdetails()]),
      action: new FormArray([this.actionFields()]),
      actionAttachments: [''],
      attachment: [''],
      fileSource: [''],
      backgroundData: new FormArray([this.addBackgroundDataForm()]),
      breachData: this.addBreachForm(),
      breachAttachments: [''],
      notifyData: this.addNotifyDataForm(),
      notificationAttachments: [''],
      postRecData: this.addpostRecDataForm(),
      postRecoveryMeasuresAttachments: [''],
      incidentVdrData: this.addIncidentVdrForm(),
      reviewOfVdrDataAttachments: [''],
      incidentSeqData: new FormArray([this.addincidentSeqForm()]),
      seqOfEventAttachments: [''],
      investigationTeam: this.addinvestigationForm(),
      intContact: new FormArray([this.addIntContactForm()]),
      extContact: new FormArray([this.addExtContactForm()]),
      personalInjury: [''],
      propertyDamage: [''],
      environment: [''],
      businessimpact: [''],
      additionalComments: [''],
      communication: new FormArray([this.addCommunication()]),
      isPartiallySubmitted: [''],
      isActive: [''],
    });
  }
}
