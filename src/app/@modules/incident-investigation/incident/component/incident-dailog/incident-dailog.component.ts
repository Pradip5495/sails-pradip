import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  natureOfAction: string;
  name: string;
  stopCardFlag: boolean;
}

@Component({
  selector: 'app-incident-dailog',
  templateUrl: './incident-dailog.component.html',
  styleUrls: ['./incident-dailog.component.css'],
})
export class IncidentDailogComponent {
  public flag = true;

  constructor(
    public dialogRef: MatDialogRef<IncidentDailogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.flag = data.stopCardFlag;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
