import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentDailogComponent } from './incident-dailog.component';

describe('IncidentDailogComponent', () => {
  let component: IncidentDailogComponent;
  let fixture: ComponentFixture<IncidentDailogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IncidentDailogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentDailogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
