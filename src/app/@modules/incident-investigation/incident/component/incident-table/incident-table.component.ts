import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Logger } from '@core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService, NotificationService } from '@shared/common-service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { IncidentService } from './../../share/incident.service';
import { SidenavStoreService } from '@app/shell/sidenav/sidenav-store.service';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { VesselSidebarComponent } from '@shared/component/vessel-sidebar/vessel-sidebar.component';

import * as moment from 'moment';

const log = new Logger('Near Miss');

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

export interface IncidentElement {
  reportId: string;
  severityLevel: string;
  type: string;
  vessel: string;
  date: string;
  task: string;
  description: string;
  eventImpact: string;
}

@Component({
  selector: 'app-incident-table',
  templateUrl: './incident-table.component.html',
  styleUrls: ['./incident-table.component.scss'],
})
export class IncidentTableComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = [
    'reportId',
    'severityLevel',
    'type',
    'vessel',
    'date',
    'task',
    'description',
    'eventImpact',
    'actions',
  ];
  public dataSource = new MatTableDataSource([]);
  public selection = new SelectionModel<PeriodicElement>(true, []);

  public title: '';
  public data: IncidentElement[] = [];
  // public dataSource: new MatTableDataSource([]);
  public isLoading = false;
  public inid: any;
  public isdataSource = false;
  public isTableView = true;
  public portList: any = [];
  public incidentData: any = [];
  public incidentDetailsData: any = [];
  public incidentId: any;
  public id: any;
  public isShow = false;
  public vesselTypeData: any = [];
  public states: string[] = ['None', 'Alaska'];
  public probabilityList: string[] = ['Rare', 'Unlikely', 'Possible', 'Likely', 'Almost Certain'];
  public probability = 'Possible';
  public severityLevel: string[] = ['Extreme', 'Major', 'Moderate', 'Minor', 'Trivial'];
  public reportType: any;
  public pageEvent: PageEvent;
  public deleteData: any;
  public vesselFilterData: any;
  public impactList: any = [];
  public sideBar: any;
  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  public array: any;
  public length = 100;
  public pageSizeOptions: number[] = [5, 10, 25, 100];
  public rowData: any;
  public icons: any;
  public checkedVessels: any = [];
  public checkedVesselIdArray: any = [];
  error: string | undefined;
  filterForm: FormGroup;
  options: FormGroup;
  colorControl = new FormControl('primary');
  fontSizeControl = new FormControl(16, Validators.min(10));
  setTimeControl = new FormControl();

  editType = '';
  paginationPageSize = 20;
  // domLayout = 'autoHeight';

  columnDefs = [
    {
      headerName: 'Report Id',
      field: 'reportId',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'SEVERITY LEVEL',
      field: 'severityLevel',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'TYPE',
      field: 'type',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'VESSEL',
      field: 'vessel',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'DATE',
      field: 'date',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'TASK',
      field: 'task',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Description',
      field: 'description',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'IMPACT',
      field: 'eventImpact',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Actions',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deleteIncident.bind(this),
        onClickEdit: this.editItems.bind(this),
        onClickDetails: this.getIncidentDetails.bind(this),
      },
      filter: 'agTextColumnFilter',
    },
  ];
  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    customStatsToolPanel: VesselSidebarComponent,
  };
  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private localStorageService: LocalStorageService,
    private formBuilder: FormBuilder,
    private incidentService: IncidentService,
    private notifyService: NotificationService,
    private leftSidenavStore: SidenavStoreService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };
    const paramValue = 'value';
    this.leftSidenavStore.hasSideNav = true;
    if (this.route.params[paramValue].type) {
      const type = this.route.params[paramValue].type;
      this.reportType = type;
      this.getFilterByType(type);
    } else if (this.route.params[paramValue].incidentId) {
      this.leftSidenavStore.hasSideNav = false;
      this.isTableView = false;
      this.incidentId = this.route.params[paramValue].incidentId;
      this.getIncidentById(this.incidentId);
    }

    this.leftSidenavStore.myVessels$.subscribe((vesselData: any) => {
      if (vesselData) {
        log.debug('vesselData: ', vesselData);
        this.vesselFilter(vesselData);
      }
    });

    this.getVesselType();
    this.getPortData();
    this.getAllTypeList();
    this.sideBar = {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'columns',
          iconKey: 'columns',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
          },
        },
        {
          id: 'customStats',
          labelDefault: 'Vessels',
          labelKey: 'customStats',
          iconKey: 'custom-stats',
          filter: true,
          toolPanel: 'customStatsToolPanel',
        },
      ],
      filter: true,
      position: 'right',
    };

    this.filterForm = this.formBuilder.group({
      reportType: [''],
      vesselId: [''],
      reportId: [''],
      eventImpactType: [''],
      dateOfReport: [''],
      severityLevel: [''],
    });
  }

  public handlePage(e: PageEvent) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getIncident() {
    this.isLoading = true;
    this.incidentService.getIncident().subscribe(
      (response) => {
        this.dataSource = new MatTableDataSource([]);
        this.incidentData = response;
        this.isLoading = false;
        if (!response) {
          this.isdataSource = true;
          const message = 'No Record Found!';
          this.notifyService.showError(message, this.title);
        } else {
          this.isdataSource = false;
          this.data = this.incidentData;
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  onGridReady(params: any) {
    log.debug('reached');
    this.incidentService.getIncident().subscribe(
      (response) => {
        log.debug('Users Data', response);
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
      },
      (error) => {
        this.notifyService.showError(error);
      }
    );
    this.leftSidenavStore.myVessels$.subscribe((vessels) => {
      log.debug('checked vessels', vessels);
      this.checkedVessels = vessels;
      this.checkedVesselIdArray = [];

      let stringd = '';
      vessels.forEach((vessel: any, index) => {
        stringd = stringd + 'vesselId=' + vessel.vesselId + '&';
      });
      this.incidentService.incidentGetFilterVessel(stringd).subscribe((response: any) => {
        this.rowData = response;
      });

      if (vessels.length === 0) {
        this.incidentService.getIncident().subscribe(
          (response) => {
            log.debug('Users Data', response);
            if (!response) {
              this.isdataSource = true;
            }
            this.rowData = response;
          },
          (error) => {
            this.notifyService.showError(error);
          }
        );
      }
    });
  }

  getFilterIncident(type: any) {
    this.isLoading = true;
    this.incidentService.getFilterIncident(type).subscribe(
      (response) => {
        this.dataSource = new MatTableDataSource([]);
        this.isLoading = false;
        if (!response) {
          this.isdataSource = true;
          const message = 'No Record Found!';
          this.notifyService.showError(message, this.title);
        } else {
          this.isdataSource = false;
          this.data = this.incidentData;
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getFilterByType(type: any) {
    log.debug('type: ', type);
    this.isLoading = true;
    this.incidentService.getFilterByType(type).subscribe(
      (response) => {
        this.dataSource = new MatTableDataSource([]);
        this.isLoading = false;
        if (!response) {
          this.isdataSource = true;
          const message = 'No Record Found!';
          this.notifyService.showError(message, this.title);
        } else {
          this.isdataSource = false;
          this.data = this.incidentData;
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getFilter(type: any) {
    this.isLoading = true;
    const vesselList = this.vesselFilterData;
    this.incidentService.getFilter(type, vesselList).subscribe(
      (response) => {
        this.dataSource = new MatTableDataSource([]);
        this.isLoading = false;
        if (!response) {
          this.isdataSource = true;
          const message = 'No Record Found!';
          this.notifyService.showError(message, this.title);
        } else {
          this.isdataSource = false;
          this.data = this.incidentData;
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },

      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  onAddForm() {
    this.router.navigate(['create'], { relativeTo: this.route }).catch((e) => {
      log.error(e);
    });
  }

  getIncidentById(incidentId: any) {
    this.isLoading = true;
    this.isTableView = false;
    this.incidentService.incidentGetFilterVessel(incidentId).subscribe(
      (response) => {
        this.dataSource = new MatTableDataSource([]);
        this.isLoading = false;
        this.incidentDetailsData = response;
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  vesselFilter(vessels: any) {
    let vessel = '';
    let index = 0;
    if (vessels.length > 0) {
      vessels.forEach((vesselValue: any) => {
        if (index === 0) {
          vessel = 'vesselId=' + vesselValue;
        } else {
          vessel = vessel + '&vesselId=' + vesselValue;
        }
        index++;
      });
      this.vesselFilterData = vessel;
      this.onSubmit(1);
    } else {
      this.getIncident();
    }
  }

  onSubmit(type: any) {
    this.isLoading = true;
    if (!type) {
      if (this.filterForm.value.dateOfReport) {
        const dateOfReport = this.filterForm.value.dateOfReport;
        const dateTimeIncident = moment(dateOfReport).format('YYYY-MM-DD');

        this.filterForm.value.dateOfReport = dateTimeIncident;
      }
    }

    if (this.reportType) {
      this.filterForm.value.reportType = this.reportType;
    }

    const vesselList = this.vesselFilterData;

    this.incidentService.getFilter(this.filterForm.value, vesselList).subscribe(
      (response) => {
        this.dataSource = new MatTableDataSource([]);
        this.isLoading = false;
        this.incidentData = response;
        if (!response) {
          this.isdataSource = true;
        } else {
          this.isdataSource = false;
          log.debug('onSubmit:', this.incidentData);
          this.data = this.incidentData;
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },

      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  resetFilter() {
    this.filterForm.reset();
    this.getIncident();
  }

  onChangeHour(event: any) {}

  getFontSize() {
    return Math.max(10, this.fontSizeControl.value);
  }

  getPadding() {}

  toggleDisplay() {
    this.isShow = !this.isShow;
  }

  getVesselType() {
    this.incidentService.getVesselType().subscribe(
      (response) => {
        this.vesselTypeData = response;
        this.localStorageService.setVessels(response);
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getPortData() {
    this.incidentService.getPortLocation().subscribe(
      (response) => {
        this.portList = response;
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get All Type List data
  getAllTypeList() {
    const type = 'Report Type';
    this.incidentService.getAllType(type).subscribe(
      (response) => {
        if (response) {
          this.impactList = response.impactList;
        } else {
          const message = 'all type list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row: any) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  onChangeprobability(event: any) {}

  setProbability(probability: any) {
    this.probability = probability;
    return this.probability;
  }

  editItems(params: any) {
    log.debug('router', params.rowData.incidentId);
    log.debug('link', this.router);
    this.id = params.rowData.incidentId;
    this.router.navigate([`/incident/${this.id}/edit`]);
  }
  getIncidentDetails(params: any) {
    this.inid = params.rowData.incidentId;
    this.router.navigate([`/incident/${this.inid}/details`]);
  }

  deleteIncident(params: any) {
    this.incidentService.deleteIncident(params.rowData.incidentId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.getIncident();
        } else {
          const message = 'Record No Deteted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error: any) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // incidentService.getIncident
  fetchData() {
    this.incidentService.getIncident().subscribe((data) => {
      this.rowData = data;
    });
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.dataSource = part;
  }
}
