import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';
import { VesselService } from '@shared/service';
import { VesselModel } from '@shared/models';

const routes = {
  reporttype: {
    get: (id: string) => `/form-feeder?type=${id}`,
    getList: (id: string) => `/form-feeder/list?type=${id}&listType=incident`,
    getAll: () => `/form-feeder/list`,
    getConsequences: (params: string) => `/form-feeder/severityofconsequences?${params}`,
  },

  crossreference: {
    getAll: () => `/crossreferencingtotsmatree`,
  },

  element: {
    getAll: () => `/crossreferencingtotsmatree/element`,
  },

  stage: {
    getAll: () => `/crossreferencingtotsmatree/stage`,
  },

  specifickpi: {
    get: (id: string) => `cross-referencing-to-tsma/stage/${id}`,
  },

  company: {
    getAll: () => `/company`,
  },

  incident: {
    get: (id: string) => `/incident/${id}`,
    getFilter: (params: string) => `/incident/findByFilter?${params}`,
    getAll: () => `/incident`,
    create: () => `/incident`,
    update: (id: string) => `/incident/${id}`,
    delete: (id: string) => `/incident/${id}`,
  },
  eventImpact: {
    getEventImpact: () => `/eventimpacttype`,
  },
  investigator: {
    getByPosition: (position: string) => `/investigators/position/${position}`,
  },
};
@Injectable({
  providedIn: 'root',
})
export class IncidentService {
  private readonly STATUS_KEY = 'statusCode';
  private readonly DATA_KEY = 'data';

  constructor(
    private httpClient: HttpClient,
    private readonly httpDispatcher: HttpDispatcherService,
    private readonly vesselService: VesselService
  ) {}

  isSuccessResponse(response: object) {
    return response[this.STATUS_KEY] && response[this.STATUS_KEY] === 200;
  }

  /**
   *  Get Event Impact Type Details
   */
  getEventImpactType<T = Response>(): Observable<T> {
    return this.httpDispatcher.get<T>(routes.eventImpact.getEventImpact());
  }

  /**
   *  Get All Injury Category Data
   */
  getInjuryCategory(): Observable<Response> {
    const API_URL = `/injuryCategory`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All Injury Category Data
   */
  getInjuryOccuredBy(): Observable<Response> {
    const API_URL = `/injuryoccuredby`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All Body Part effected data
   */
  getBodyPartEffected(): Observable<Response> {
    const API_URL = `/bodyPartAffected`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get nationality list
   */
  getNationality(): Observable<Response> {
    const API_URL = `/nationality`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get equipment data
   */
  getEquipment(): Observable<Response> {
    const API_URL = `/equipment`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get equipment category data
   */
  getEquipmentCategory(): Observable<Response> {
    const API_URL = `/equipment/category`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get all background Condition Data
   */
  getBackgroundCondition<T = Response>(): Observable<T> {
    const API_URL = `/background/condition`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get all position of  users for investigation team
   */
  getInvestigation(): Observable<Response> {
    const API_URL = `/investigators`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get all position of  users for investigation team
   */
  getByPosition(position: any): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.investigator.getByPosition(position));
  }

  /**
   *  Get all reportreviwed by users for investigation team
   */
  getAllUsers(): Observable<Response> {
    const API_URL = `/user`;

    const transform = (users: any) =>
      users.map((user: any) => {
        return {
          ...user,
          fullname: user.firstname + ' ' + user.lastname,
        };
      });

    return this.httpClient.get(API_URL).pipe(
      map((responseData) =>
        this.isSuccessResponse(responseData) ? transform(responseData[this.DATA_KEY]) : throwError(false)
      ),
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get all comapny details
   */
  getAllCompany(): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.company.getAll());
  }

  /**
   *  Get all reportreviwed by users for investigation team
   */
  getLocation(): Observable<Response> {
    const API_URL = `/location`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get analysis of similar Incident in the fleet in the past Questions
   */
  getAnalysisOfSimilarIncident(): Observable<Response> {
    const API_URL = `/analysisofsimilarincident`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All Vessel Details
   */

  getVessels(filters?: Map<string, string>): Observable<VesselModel[]> {
    return this.vesselService.getVessels(filters);
  }

  /**
   *  Get All Vessel Details
   */

  getVesselType(): Observable<any> {
    return this.vesselService.getVesselTypes();
  }

  /**
   *  Get All Designation Details
   */

  getDesignation(): Observable<Response> {
    const API_URL = `/user/designation`;
    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All Port Location Details
   */

  getPortLocation(): Observable<any> {
    return this.vesselService.getVesselPorts();
  }

  /**
   *  Get Rank List
   */

  getRank(): Observable<Response> {
    const API_URL = `/rank`;
    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Incident List
   */

  getIncident(): Observable<Response> {
    const API_URL = `/incident`;
    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  getAllIncidents(): Observable<any> {
    const API_URL = `/incident`;
    return this.httpDispatcher.get(API_URL);
  }

  /**
   *  Get Filter Incident List
   */

  getFilterIncident(type: any): Observable<Response> {
    const API_URL = `/incident/findbyfilter`;
    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Filter Incident List
   */

  getFilterByType(type: any): Observable<Response> {
    const API_URL = `/incident/findbyfilter?${type}=true`;
    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   * Create new Incident
   */
  createIncident(data: any): Observable<Response> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
      }),
    };

    return this.httpClient.post('/incident', data, httpOptions).pipe(
      tap((responseData: any) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        if (responseData[statusCode] === 200) {
          return responseData[dataParam];
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }

  /**
   * Create new Incident
   */
  updateIncident(data: any, incidentId: any): Observable<Response> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
      }),
    };
    const API_URL = `/incident/${incidentId}`;
    return this.httpClient.put(API_URL, data, httpOptions).pipe(
      tap((responseData: any) => {
        const statusCode = 'statusCode';
        if (responseData[statusCode] === 200) {
          return responseData;
        } else {
          return responseData;
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }

  /**
   *  Get User Profile Details
   */
  getFilterNearMiss(type: string): Observable<Response> {
    const API_URL = `/incident/findByFilter?reportType=${type}`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Incident Details By Id
   */
  incidentGetFilterVessel(vesselId: any): Observable<Response> {
    const API_URL = `/incident?${vesselId}`;
    return this.httpDispatcher.get(API_URL);
  }

  getIncidentById(incidentId: string): Observable<Response> {
    const API_URL = `/incident/${incidentId}`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Incident Details By Id
   */
  getSeqImageDelete(sequenceOfEventsAttachmentId: string): Observable<Response> {
    const API_URL = `/incident/attachment/${sequenceOfEventsAttachmentId}`;

    return this.httpClient.delete(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Filter Near Miss Details
   */
  getFilter(formData: any, vesselData: any): Observable<Response> {
    let formValue = '';
    let isFirstFlag = 0;
    // Query Binding
    if (vesselData) {
      if (isFirstFlag) {
        formValue = formValue + '&' + vesselData;
      } else {
        formValue = vesselData;
        isFirstFlag = 1;
      }
    }

    if (formData.reportId !== '' && formData.reportId) {
      if (isFirstFlag) {
        formValue = formValue + '&reportId=' + formData.reportId;
      } else {
        formValue = 'reportId=' + formData.reportId + formValue;
        isFirstFlag = 1;
      }
    }

    if (formData.eventImpactType !== '' && formData.eventImpactType) {
      if (isFirstFlag) {
        formValue = formValue + '&eventImpactType=' + formData.eventImpactType;
      } else {
        formValue = 'eventImpactType=' + formData.eventImpactType + formValue;
        isFirstFlag = 1;
      }
    }

    if (formData.dateOfReport !== '' && formData.dateOfReport) {
      if (isFirstFlag) {
        formValue = formValue + '&dateOfReport=' + formData.dateOfReport;
      } else {
        formValue = 'dateOfReport=' + formData.dateOfReport + formValue;
        isFirstFlag = 1;
      }
    }

    if (formData.reportType !== '' && formData.reportType) {
      if (isFirstFlag) {
        formValue = formValue + '&reportType=' + formData.reportType;
      } else {
        formValue = 'reportType=' + formData.reportType + formValue;
        isFirstFlag = 1;
      }
    }

    if (formData.severityLevel !== '' && formData.severityLevel) {
      if (isFirstFlag) {
        formValue = formValue + '&severityLevel=' + formData.severityLevel;
      } else {
        formValue = 'severityLevel=' + formData.severityLevel + formValue;
        isFirstFlag = 1;
      }
    }

    return this.httpDispatcher.get<any>(routes.incident.getFilter(formValue));
  }

  /**
   *  Get all cross reference Element
   */
  getElements(): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.element.getAll());
  }

  /**
   *  Get all cross reference Stage
   */
  getStage(): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.stage.getAll());
  }

  /**
   *  Get all cross reference Stage
   */
  getSpecificKPI(suid: any): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.specifickpi.get(suid));
  }

  /**
   *  Get all cross reference relation data
   */
  getCrossReferenceTMSA(): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.crossreference.getAll());
  }

  getFormFeed(type: string): Observable<any> {
    return this.httpDispatcher.get<any>(routes.reporttype.get(type));
  }

  /**
   * Form Feeder for fields (Impact Type, status, Proposed By)
   */
  getAllTypeList(): Observable<any> {
    return this.httpDispatcher.get<any>(routes.reporttype.getAll());
  }

  /**
   * Form Feeder for fields (Impact Type, status, Proposed By)
   */
  getAllType(type: string): Observable<any> {
    return this.httpDispatcher.get<any>(routes.reporttype.getList(type));
  }

  getSeverityofConsequence(severity: string, impact: string[]) {
    let formValue = '';

    if (severity) {
      formValue += 'severityLevel=' + severity;
    }

    if (impact.length > 0) {
      impact.forEach((elment: any) => {
        formValue = formValue + `${formValue === '' ? '' : '&'}eventImpact=` + elment;
      });
    }

    return this.httpDispatcher.get<any>(routes.reporttype.getConsequences(formValue));
  }

  /**
   * Delete Inident
   */
  deleteIncident(incidentId: any): Observable<any> {
    return this.httpDispatcher.delete(routes.incident.delete(incidentId));
  }
}
