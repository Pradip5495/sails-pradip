import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { RouterModule } from '@angular/router';

import { FleetNotificationFilterComponent } from './component/fleet-notification-filter/fleet-notification-filter.component';
import { FleetNotificationTableComponent } from './component/fleet-notification-table/fleet-notification-table.component';
import { FleetNotificationFormComponent } from './component/fleet-notification-form/fleet-notification-form.component';
import { FleetNotificationComponent } from './fleet-notification.component';
import { FleetAcknowledgementFormComponent } from './component/fleet-acknowledgement-form/fleet-acknowledgement-form.component';
import { FleetVesselConfirmationTableComponent } from './component/fleet-vessel-confirmation-table/fleet-vessel-confirmation-table.component';
import { FleetUpdateFormComponent } from './component/fleet-update-form/fleet-update-form.component';
import { FleetUpdateAcknowledgementFormComponent } from './component/fleet-update-acknowledgement-form/fleet-update-acknowledgement-form.component';
import { FleetNotificationDetailsComponent } from './component/fleet-notification-details/fleet-notification-details.component';
import { FormlyModule } from '@formly';

import { FleetNotificationRoutingModule } from './fleet-notification-routing.module';

@NgModule({
  declarations: [
    FleetNotificationFilterComponent,
    FleetNotificationTableComponent,
    FleetNotificationFormComponent,
    FleetNotificationComponent,
    FleetAcknowledgementFormComponent,
    FleetVesselConfirmationTableComponent,
    FleetUpdateFormComponent,
    FleetUpdateAcknowledgementFormComponent,
    FleetNotificationDetailsComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    FormlyModule,
    FleetNotificationRoutingModule,
  ],
})
export class FleetNotificationModule {}
