import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fleet-notification',
  templateUrl: './fleet-notification.component.html',
  styleUrls: ['./fleet-notification.component.scss'],
})
export class FleetNotificationComponent implements OnInit {
  isLoading = false;
  isNewFleetNotification = false;
  fleetAcknowledge = false;
  isUpdateFleetNotification = false;
  isUpdateFleetAcknowledgement = false;
  isAcknowledgementDetail = false;

  constructor() {}

  ngOnInit() {}
}
