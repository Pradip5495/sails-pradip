import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetNotificationComponent } from './fleet-notification.component';

describe('FleetNotificationComponent', () => {
  let component: FleetNotificationComponent;
  let fixture: ComponentFixture<FleetNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FleetNotificationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
