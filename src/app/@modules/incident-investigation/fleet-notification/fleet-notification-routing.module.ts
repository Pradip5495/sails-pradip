import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FleetNotificationComponent } from './fleet-notification.component';
import { extract } from '@app/i18n';
import { FleetNotificationDetailsComponent } from './component/fleet-notification-details/fleet-notification-details.component';
import { FleetNotificationFormComponent } from './component/fleet-notification-form/fleet-notification-form.component';
import { FleetAcknowledgementFormComponent } from './component/fleet-acknowledgement-form/fleet-acknowledgement-form.component';
import { FleetUpdateAcknowledgementFormComponent } from './component/fleet-update-acknowledgement-form/fleet-update-acknowledgement-form.component';
import { FleetNotificationTableComponent } from './component/fleet-notification-table/fleet-notification-table.component';
import { FleetVesselConfirmationTableComponent } from './component/fleet-vessel-confirmation-table/fleet-vessel-confirmation-table.component';

const fleetNotificationRoutes: Routes = [
  {
    path: '',
    component: FleetNotificationComponent,
    children: [
      { path: '', component: FleetNotificationTableComponent },
      { path: 'applicable-vessels/:fnuid', component: FleetVesselConfirmationTableComponent },
      {
        path: 'form',
        component: FleetNotificationFormComponent,
        data: { title: extract('fleetNotification') },
      },
      {
        path: 'new-fleet-acknowledgement-form/:avuid',
        component: FleetAcknowledgementFormComponent,
        data: { title: extract('fleetNotification') },
      },
      {
        path: 'update-fleet-acknowledgement-form/:avuid',
        component: FleetUpdateAcknowledgementFormComponent,
        data: { title: extract('fleetNotification') },
      },
      {
        path: 'acknowledgement-detail/:ackuid',
        component: FleetNotificationComponent,
        data: { title: extract('fleetNotification') },
      },
      {
        path: ':fnuid/edit',
        component: FleetNotificationFormComponent,
        data: { title: extract('fleetNotification') },
      },
      {
        path: ':fnuid/details',
        component: FleetNotificationDetailsComponent,
        data: { title: extract('fleetNotification') },
      },
    ],
    data: { title: extract('fleetNotification') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(fleetNotificationRoutes)],
  exports: [RouterModule],
})
export class FleetNotificationRoutingModule {}
