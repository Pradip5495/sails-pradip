import { Component, OnInit, VERSION, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FleetNotificationService } from '../../fleet-notification.service';
import { Logger } from '@core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { MatStepper } from '@angular/material/stepper';
import { NotificationService } from '@shared/common-service';
import { UserService } from '@shared';

const log = new Logger('Fleet-Notification-Update-Acknowledgement-Form');

@Component({
  selector: 'app-fleet-update-acknowledgement-form',
  templateUrl: './fleet-update-acknowledgement-form.component.html',
  styleUrls: ['./fleet-update-acknowledgement-form.component.scss'],
})
export class FleetUpdateAcknowledgementFormComponent implements OnInit {
  ngVersion: string = VERSION.full;

  @ViewChild('stepper') stepper: MatStepper;
  public isLinear = false;

  acknowledgementFormGroup: FormGroup;
  actionForm: FormGroup;
  applicablevesselId: any;
  participants: any[];
  public alertValue = 'yes';
  public readonly: boolean;
  public userData: any;
  public userList: any;
  public fleetAcknowledgementData: any;
  public title = 'Fleet Acknowledgement';
  public designationList: any;
  public designationData: any;
  error: any;
  public subDetails: any;
  public akuid: any;
  public avuid: any;
  public fleetNotificationAcknowledgementData: any;
  public type: string[] = ['Immediate Action', 'Corrective Action', 'Preventive Action'];
  public activeIndex: any = 1;

  get checkBoxChecked() {
    return this._checkBoxChecked;
  }

  set checkBoxChecked(val: any) {
    this._checkBoxChecked = val;
    this.actionForm.get('status').setValue(this.checkBoxChecked);
  }

  get f() {
    return this.actionForm.controls;
  }

  private _checkBoxChecked = false;

  constructor(
    private route: ActivatedRoute,
    private fleetNotificationService: FleetNotificationService,
    private userService: UserService,
    private router: Router,
    private formBuilder: FormBuilder,
    private notifyService: NotificationService
  ) {}

  ngOnInit() {
    this.getAllUsers();
    this.getDesignation();

    this.createAcknowledgement();
    this.createAction();
    const paramValue = 'value';
    if (this.route.params[paramValue].avuid) {
      this.avuid = this.route.params[paramValue].avuid;
      this.getApplicableVesselById(this.avuid);
    }
  }

  _onAlertOnBoard = (_: any) => {};

  onAddActionField() {
    const action = 'action';
    this.subDetails = this.actionForm.get(action) as FormArray;
    this.subDetails.push(this.actionFields());
  }

  move(index: number) {
    this.stepper.selectedIndex = index;
    this.activeIndex = index + 1;
  }

  getStepDone(index: number) {
    if (index < this.activeIndex) {
      const primary = 'primary';
      return primary;
    }
  }

  getActionControls() {
    const actionF = this.actionForm.controls.action as FormArray;
    return actionF;
  }

  actionFields(): FormGroup {
    return this.formBuilder.group({
      actionId: [''],
      type: [''],
      natureOfAction: [''],
      dueDate: [''],
      designationId: [''],
      status: [this.checkBoxChecked],
      dateCompleted: [''],
    });
  }

  onDeleteAction(index: number) {
    const actionForm = this.actionForm.controls.action as FormArray;
    actionForm.removeAt(index);
  }
  onAlertOnBoard(event: any) {
    this._onAlertOnBoard(this.alertValue);
    if (this.alertValue === 'yes') {
      this.readonly = true;
    } else {
      this.readonly = !this.readonly;
    }
  }

  // Get Near Miss DetailsBy Id
  getApplicableVesselById(avuid: any) {
    // log.debug('avuid: ', avuid);
    // this.fleetNotificationService.getApplicableVesselById(avuid).subscribe(
    //   (response) => {
    //     this.fleetNotificationAcknowledgementData = response[0];
    //     this.akuid = this.fleetNotificationAcknowledgementData.acknowledgementId;
    //     this.setFleetAcknowledgementFormData();
    //     // this.setNeaMissFormData();
    //     log.debug('this.fleetNotificationAcknowledgementData:', this.fleetNotificationAcknowledgementData);
    //   },
    //   (error) => {
    //     this.error = error;
    //     const message = error;
    //     this._snackBar.open(message, '', {
    //       duration: 4000,
    //     });
    //   }
    // );
  }

  setFleetAcknowledgementFormData() {
    this.acknowledgementFormGroup.patchValue(this.fleetNotificationAcknowledgementData);
    log.debug('Tesing master Name', this.acknowledgementFormGroup);
    this.setFormData();
    // this.actionForm.patchValue(this.fleetNotificationAcknowledgementData);
  }

  setFormData() {
    const data = this.fleetNotificationAcknowledgementData;
    // tslint:disable-next-line:forin
    for (const dataKey in data) {
      // if (dataKey === 'action') {
      //   const action = this.fleetNotificationAcknowledgementData.action;
      //   let index = 0;
      //   action.forEach((element: any) => {
      //     if (index > 0) {
      //       this.onAddActionField();
      //     }
      //     index++;
      //   });
      // }
      if (dataKey === 'action') {
        const action = this.fleetNotificationAcknowledgementData.action;
        let index = 0;
        action.forEach((element: any) => {
          const convertedDate = moment(this.fleetNotificationAcknowledgementData.action[index].dateCompleted).startOf(
            'day'
          );
          const observationYear = moment(convertedDate).format('YYYY-MM-DD');
          this.fleetNotificationAcknowledgementData.action[index].dateCompleted = observationYear;

          const dueDates = moment(this.fleetNotificationAcknowledgementData.action[index].dueDate).startOf('day');
          const dueDate = moment(dueDates).format('YYYY-MM-DD');
          this.fleetNotificationAcknowledgementData.action[index].dueDate = dueDate;

          if (index > 0) {
            this.onAddActionField();
          }
          index++;
        });
        this.actionForm.patchValue(this.fleetNotificationAcknowledgementData);
      }
    }
  }

  // Get All User list data
  getAllUsers() {
    this.userService.getUsers().subscribe((response: any) => {
      this.userList = response;
      if (response) {
        this.userData = response;
      }
    });
  }

  // Get All Designation list data
  getDesignation() {
    this.userService.getDesignations().subscribe((response: any) => {
      this.designationList = response;
      if (response) {
        this.designationData = response;
      }
    });
  }

  onSubmit() {
    log.debug('AcknowledgementForm: ', this.acknowledgementFormGroup.value);
  }

  onSubmitActionForm() {
    this.actionForm.patchValue(this.acknowledgementFormGroup.value);
    const data = this.actionForm.value;
    const formData = new FormData();

    for (const dataKey in data) {
      if (dataKey === 'action') {
        const datas = data[dataKey];
        let index = 0;
        datas.forEach((element: any) => {
          for (const previewKey in element) {
            if (previewKey === 'dueDate') {
              if (datas[index][previewKey]) {
                const convertedDueDates = moment(datas[index][previewKey]).startOf('day');
                const dueDate = moment(convertedDueDates).format('YYYY-MM-DD');
                formData.append(`action[${index}][${previewKey}]`, dueDate);
              }
              // const convertedDueDate = moment(datas[index][previewKey]).startOf('day');
              // const dueDate = moment(convertedDueDate).format('YYYY-MM-DD');
              // formData.append(`action[${index}][${previewKey}]`, dueDate);
            } else if (previewKey === 'dateCompleted') {
              if (datas[index][previewKey]) {
                const convertedDateCompleted = moment(datas[index][previewKey]).startOf('day');
                const dateCompleted = moment(convertedDateCompleted).format('YYYY-MM-DD');
                formData.append(`action[${index}][${previewKey}]`, dateCompleted);
              }
              // const convertedDateCompleted = moment(datas[index][previewKey]).startOf('day');
              // const dateCompleted = moment(convertedDateCompleted).format('YYYY-MM-DD');
              // formData.append(`action[${index}][${previewKey}]`, dateCompleted);
            } else if (previewKey === 'status') {
              if (datas[index][previewKey] === true) {
                const keyValue = '1';
                formData.append(`action[${index}][${previewKey}]`, keyValue);
              } else {
                const keyValue = '0';
                formData.append(`action[${index}][${previewKey}]`, keyValue);
              }
            } else {
              formData.append(`action[${index}][${previewKey}]`, datas[index][previewKey]);
            }
          }
          index++;
        });
      } else if (dataKey === 'date') {
        const convertedDate = moment(data[dataKey]).startOf('day');
        const date = moment(convertedDate).format('YYYY-MM-DD');
        formData.append(dataKey, date);
      } else if (dataKey === 'participants') {
        const participants = this.actionForm.value.participants;
        log.debug(participants);
        for (let index = 0; index < participants.length; index++) {
          log.debug(participants);
          formData.append(`participants[${index}]`, participants[index]);
        }
      } else if (dataKey === 'emails') {
        const emails = this.actionForm.value.emails;
        const email = emails.split(';');
        for (let index = 0; index < email.length; index++) {
          formData.append(`emails[${index}]`, email[index]);
        }
      } else {
        formData.append(dataKey, data[dataKey]);
      }
    }

    // this.actionForm.value.date = moment(convertedDate).format('YYYY-MM-DD HH:mm:ss');
    log.debug(this.actionForm.value);

    this.fleetNotificationService.updateFleetAcknowledgementForAVId(formData, this.akuid).subscribe(
      (response) => {
        log.debug('response:', response);
        this.fleetAcknowledgementData = response;
        if (this.fleetAcknowledgementData.statusCode === 200) {
          const message = 'Acknowledgement updated successfully!';
          this.notifyService.showSuccess(message, this.title);
          this.router.navigateByUrl('/fleetNotification');
        } else {
          const message = this.fleetAcknowledgementData.message;
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
        // this._snackBar.open(message, '', {
        //   duration: 4000
        // });
      }
    );
  }

  private createAcknowledgement() {
    this.acknowledgementFormGroup = this.formBuilder.group({
      alertDiscussedOnBoard: ['', Validators.required],
      masterNameId: ['', Validators.required],
      date: ['', Validators.required],
      participants: ['', Validators.required],
      emails: [''],
      comments: [''],
    });
  }

  private createAction() {
    this.actionForm = this.formBuilder.group({
      applicablevesselId: [this.applicablevesselId],
      alertDiscussedOnBoard: [''],
      masterNameId: [''],
      date: [''],
      participants: [''],
      emails: [''],
      comments: [''],
      action: new FormArray([this.actionFields()]),
    });
  }
}
