import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetUpdateAcknowledgementFormComponent } from './fleet-update-acknowledgement-form.component';

describe('FleetUpdateAcknowledgementFormComponent', () => {
  let component: FleetUpdateAcknowledgementFormComponent;
  let fixture: ComponentFixture<FleetUpdateAcknowledgementFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FleetUpdateAcknowledgementFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetUpdateAcknowledgementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
