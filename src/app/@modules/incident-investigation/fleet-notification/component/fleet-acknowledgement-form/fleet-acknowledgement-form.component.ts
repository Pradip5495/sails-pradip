import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FleetNotificationService } from '../../fleet-notification.service';
import { Logger, untilDestroyed } from '@core';
import { FormGroup } from '@angular/forms';
import { NotificationService } from '@shared/common-service';
import { UserService } from '@shared';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { of, Subject } from 'rxjs';
import { ICustomFormlyConfig } from '@types';

const log = new Logger('Fleet-Notification-Acknowledgement-Form');

@Component({
  selector: 'app-fleet-acknowledgement-form',
  templateUrl: './fleet-acknowledgement-form.component.html',
  styleUrls: ['./fleet-acknowledgement-form.component.scss'],
})
export class FleetAcknowledgementFormComponent implements OnInit, OnDestroy {
  @Input() applicableVesselId: string;
  @Output() formOutput = new Subject<boolean>();
  statusArray: any[] = [];
  fleetVesselPayload: any;
  acknowledgementId: string;
  acknowledgementData: any;
  isEditMode: boolean;
  title: 'Fleet Acknowledgement';

  acknowledgeFormKeys = {
    masterNameId: 'masterNameId',
    designationId: 'designationId',
    userId: 'userId',
    email: 'email',
    comments: 'comments',
    alertDiscussedOnBoard: 'alertDiscussedOnBoard',
  };
  acknowledgeForm = new FormGroup({});
  acknowledgeModel = {
    alertDiscussedOnBoard: true,
  };
  acknowledgeOptions: FormlyFormOptions = {
    formState: {
      users: this.userService.getUsers(),
    },
  };
  acknowledgeFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.acknowledgeFormKeys.alertDiscussedOnBoard,
          className: 'flex-1',
          type: 'checkbox',
          templateOptions: {
            label: 'Alert Discussed On Board',
          },
        },
        {
          key: this.acknowledgeFormKeys.masterNameId,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Master Name',
            options: this.userService.getUsers(),
            valueProp: 'userId',
            labelProp: 'firstname',
            required: true,
          },
        },
        {
          key: this.acknowledgeFormKeys.designationId,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            required: true,
            multiple: true,
            label: 'Designation',
            options: this.userService.getDesignations(),
            labelProp: 'designation',
            valueProp: 'designationId',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.acknowledgeFormKeys.userId,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            label: 'Participants',
            placeholder: 'Enter Vessel Name',
            multiple: true,
            required: true,
            options: this.userService.getUsers(),
            valueProp: 'userId',
            labelProp: 'firstname',
          },
        },
        {
          key: this.acknowledgeFormKeys.email,
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Additional Email Ids',
            description: 'Separate Email with Comma',
          },
          validators: {
            validation: ['multipleEmails'],
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.acknowledgeFormKeys.comments,
          type: 'textarea',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Comments',
            placeholder: 'Comments',
            rows: 2,
          },
        },
      ],
    },
  ];

  actionFormKeys = {
    action: 'action',
    natureOfAction: 'natureOfAction',
    dueDate: 'dueDate',
    status: 'status',
    dateCompleted: 'dateCompleted',
  };
  actionsForm = new FormGroup({});
  actionsModel = {};
  actionsOptions: FormlyFormOptions = {};
  actionsFields: FormlyFieldConfig[] = [
    {
      type: 'repeat',
      key: this.actionFormKeys.action,
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add Action',
        addTextClass: 'add-btn',
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'input',
                key: this.actionFormKeys.natureOfAction,
                templateOptions: {
                  placeholder: 'Nature of Action',
                  label: 'Nature of Action',
                  required: true,
                },
              },
              {
                className: 'flex-1',
                type: 'matdatepicker',
                key: this.actionFormKeys.dueDate,
                defaultValue: '',
                templateOptions: {
                  label: 'Due Date',
                  appearance: 'outline',
                  datepickerOptions: {
                    // max: new Date(),
                  },
                },
              },
              {
                className: 'flex-1',
                key: this.actionFormKeys.status,
                type: 'autocomplete',
                defaultValue: 'Open',
                templateOptions: {
                  label: 'Status',
                  required: true,
                  appearance: 'outline',
                  filter: (term: string) => of(term ? this.filterStatus(term) : this.statusArray.slice()),
                },
              },
              {
                className: 'flex-1',
                type: 'matdatepicker',
                key: this.actionFormKeys.dateCompleted,
                defaultValue: '',
                templateOptions: {
                  label: 'Completed Date',
                  appearance: 'outline',
                  datepickerOptions: {
                    // max: new Date(),
                  },
                },
              },
            ],
          },
        ],
      },
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      shortName: 'Acknowledgement',
      title: 'Acknowledgement',
      subTitle: 'Enter Acknowledgement Data',
      config: {
        model: this.acknowledgeModel,
        fields: this.acknowledgeFields,
        options: this.acknowledgeOptions,
        form: this.acknowledgeForm,
      },
      onSave: this.onFormlyAcknowledgeSubmit.bind(this),
      discard: true,
      onDiscard: () => this.acknowledgeForm.reset(),
    },
    {
      shortName: 'Actions',
      title: 'Actions',
      subTitle: 'Enter Actions Data',
      config: {
        model: this.actionsModel,
        fields: this.actionsFields,
        options: this.actionsOptions,
        form: this.actionsForm,
      },
      onSave: this.onFinalSubmit.bind(this),
      discard: true,
      onDiscard: () => this.actionsForm.reset(),
    },
  ];

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router,
    private notifyService: NotificationService,
    private fleetNotificationService: FleetNotificationService
  ) {}

  /* This function is used by autocomplete field in Action section */
  filterStatus(name: string) {
    return this.statusArray.filter((state: any) => state.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  ngOnInit() {
    // this.applicableVesselId = this.route.params['value'].avuid;
    this.getAllTypes();
    this.getFleetVesselAcknowledgement();
  }

  navigateBack() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }

  initializeAcknowledgementFormDataForEdit() {
    let userIds = [];
    if (this.acknowledgementData.user) {
      userIds = this.acknowledgementData.user.map((user: any) => {
        return user.userId;
      });
    }
    let designationIds = [];
    if (this.acknowledgementData.user) {
      designationIds = this.acknowledgementData.designation.map((user: any) => {
        return user.designationId;
      });
    }
    const acknowledgementData = {
      alertDiscussedOnBoard: this.acknowledgementData.alertDiscussedOnBoard === 'Yes' ? true : false,
      comments: this.acknowledgementData.comments,
      email: this.acknowledgementData.email ? this.acknowledgementData.email.toString() : '',
      masterNameId: this.acknowledgementData.masterNameId,
      userId: userIds,
      designationId: designationIds,
    };
    this.acknowledgeForm.patchValue(acknowledgementData);
  }

  initializeActionFormDataForEdit() {
    this.actionsForm.patchValue(this.acknowledgementData);
  }

  onFormlyAcknowledgeSubmit(formData: any) {
    this.fleetVesselPayload = { ...this.fleetVesselPayload, ...formData };
  }

  onFormlyActionsSubmit(formData: any) {
    const actions = [...formData.action];
    for (const action of actions) {
      if (action) {
        if (!isNaN(Date.parse(action[this.actionFormKeys.dateCompleted]))) {
          action[this.actionFormKeys.dateCompleted] = new Date(action[this.actionFormKeys.dateCompleted]).toISOString();
        } else {
          if (action.hasOwnProperty(this.actionFormKeys.dateCompleted)) {
            delete action[this.actionFormKeys.dateCompleted];
          }
        }

        if (!isNaN(Date.parse(action[this.actionFormKeys.dueDate]))) {
          action[this.actionFormKeys.dueDate] = new Date(action[this.actionFormKeys.dueDate]).toISOString();
        } else {
          if (action.hasOwnProperty(this.actionFormKeys.dueDate)) {
            delete action[this.actionFormKeys.dueDate];
          }
        }

        action[this.actionFormKeys.status] = action[this.actionFormKeys.status];
        action[this.actionFormKeys.natureOfAction] = action[this.actionFormKeys.natureOfAction];
        action.isFleetNotification = true;
      }
    }
    const modifiedActionFormData = {
      action: actions,
    };
    this.fleetVesselPayload = { ...this.fleetVesselPayload, ...modifiedActionFormData };
  }

  onFinalSubmit(formData: any) {
    this.onFormlyAcknowledgeSubmit(this.acknowledgeForm.value);
    this.onFormlyActionsSubmit(this.actionsForm.value);

    const alertDiscussedOnBoard = this.fleetVesselPayload.alertDiscussedOnBoard;
    this.fleetVesselPayload.alertDiscussedOnBoard = alertDiscussedOnBoard ? 'Yes' : 'No';
    if (this.fleetVesselPayload.email) {
      const emails = this.fleetVesselPayload.email;
      const isMultipleEmails = this.fleetVesselPayload.email.search(',');
      const emailIds: string[] = [];
      const emailArray = emails.split(',');
      if (isMultipleEmails > 0) {
        emailArray.forEach((value: string) => {
          if (value) {
            emailIds.push(value.trim());
          }
        });
      } else {
        emailIds.push(emails.trim());
      }
      this.fleetVesselPayload.email = emailArray;
    }

    if (!this.isEditMode) {
      this.createFleetVesselAcknowledge();
    } else {
      this.updateFleetVesselAcknowledge();
    }
  }

  //#region API Calls Region Start

  getAllTypes() {
    this.fleetNotificationService.getAllTypes().subscribe(async (data: any) => {
      if (data) {
        this.statusArray = (await data.status) ? data.status : [];
      }
    });
  }

  async getFleetVesselAcknowledgement() {
    if (this.applicableVesselId) {
      const responseData = await this.fleetNotificationService
        .getFleetAcknowledgementByForAVId(this.applicableVesselId)
        .pipe(untilDestroyed(this))
        .toPromise();
      if (responseData) {
        if (responseData[0].action) {
          responseData[0].action.map((action: any) => {
            action.dateCompleted = new Date(action.dateCompleted);
            action.dueDate = new Date(action.dueDate);
          });
        }
        log.debug(responseData);
        this.acknowledgementData = responseData[0];
        this.acknowledgementId = responseData[0].acknowledgementId;
        this.isEditMode = true;
        setTimeout(() => {
          this.initializeAcknowledgementFormDataForEdit();
          this.initializeActionFormDataForEdit();
        }, 50);
      }
    }
  }

  createFleetVesselAcknowledge() {
    this.fleetVesselPayload.applicablevesselId = this.applicableVesselId;
    this.fleetNotificationService
      .createAcknowledgementForAVId(this.fleetVesselPayload)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response) => {
          if (response.statusCode === 201) {
            const message = 'New Acknowledgement data Added!';
            this.notifyService.showSuccess(message, this.title);
            // this.navigateBack();
            this.formOutput.next(true);
          } else {
            const message = response.message;
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
  }

  updateFleetVesselAcknowledge() {
    this.fleetNotificationService
      .updateFleetAcknowledgementForAVId(this.fleetVesselPayload, this.acknowledgementId)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response) => {
          if (response.statusCode === 200) {
            const message = 'Acknowledgement data updated!';
            this.notifyService.showSuccess(message, this.title);
            this.formOutput.next(true);
            // this.navigateBack();
          } else {
            const message = response.message;
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
  }
  //#endregion API Calls Region Ends

  ngOnDestroy() {}
}
