import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetAcknowledgementFormComponent } from './fleet-acknowledgement-form.component';

describe('FleetAcknowledgementFormComponent', () => {
  let component: FleetAcknowledgementFormComponent;
  let fixture: ComponentFixture<FleetAcknowledgementFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FleetAcknowledgementFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetAcknowledgementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
