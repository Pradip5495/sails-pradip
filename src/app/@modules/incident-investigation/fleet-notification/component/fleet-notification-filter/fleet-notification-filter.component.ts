import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RightMenuService } from '@shared/common-service/right-menu.service';
import { LocalStorageService } from '@shared/common-service/local-storage.service';

@Component({
  selector: 'app-fleet-notification-filter',
  templateUrl: './fleet-notification-filter.component.html',
  styleUrls: ['./fleet-notification-filter.component.scss'],
})
export class FleetNotificationFilterComponent implements OnInit {
  isShow = false;
  vesselTypeData: any = [];
  states: string[] = ['None', 'Alaska'];

  options: FormGroup;
  colorControl = new FormControl('primary');
  fontSizeControl = new FormControl(16, Validators.min(10));
  setTimeControl = new FormControl();

  constructor(
    fb: FormBuilder,
    private rightSideMenuService: RightMenuService,
    private localStorageService: LocalStorageService
  ) {
    this.options = fb.group({
      color: this.colorControl,
      fontSize: this.fontSizeControl,
      setTime: this.setTimeControl,
    });
  }

  onChangeHour(event: any) {}

  getFontSize() {
    return Math.max(10, this.fontSizeControl.value);
  }

  getPadding() {}

  ngOnInit() {
    this.getVesselType();
  }

  toggleDisplay() {
    this.isShow = !this.isShow;
  }

  getVesselType() {
    this.rightSideMenuService.getVesselType().subscribe(
      (response) => {
        this.vesselTypeData = response;
        this.localStorageService.setVessels(response);
      },
      (error) => {}
    );
  }
}
