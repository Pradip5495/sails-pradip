import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetNotificationFilterComponent } from './fleet-notification-filter.component';

describe('FleetNotificationFilterComponent', () => {
  let component: FleetNotificationFilterComponent;
  let fixture: ComponentFixture<FleetNotificationFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FleetNotificationFilterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetNotificationFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
