import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetNotificationTableComponent } from './fleet-notification-table.component';

describe('FleetNotificationTableComponent', () => {
  let component: FleetNotificationTableComponent;
  let fixture: ComponentFixture<FleetNotificationTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FleetNotificationTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetNotificationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
