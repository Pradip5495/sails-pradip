import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Logger, untilDestroyed } from '@core';
import { FleetNotificationService } from '../../fleet-notification.service';
import { environment } from '@env/environment';
import { NotificationService } from '@shared/common-service/notification.service';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { DatePipe } from '@angular/common';
import { DialogComponent } from '@shared/component';

const log = new Logger('Fleet-Notification');

@Component({
  selector: 'app-fleet-notification-table',
  templateUrl: './fleet-notification-table.component.html',
  styleUrls: ['./fleet-notification-table.component.scss'],
})
export class FleetNotificationTableComponent implements OnInit, OnDestroy {
  @ViewChild('dialog') dialog: DialogComponent;

  error: string | undefined;
  public rowData: any;
  public icons: any;
  public id: any;
  displayedColumns = [
    'notificationId',
    'notificationTitle',
    'dateOfIssue',
    'relatedIncidentId',
    'actions',
    'status',
    'confirmation',
  ];
  // sorting data
  public title = 'Fleet Notification';
  public API_URL = environment.filePath;
  editType = '';

  columnDefs = [
    {
      headerName: 'NOTIFICATION ID',
      field: 'notificationId',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'NOTIFICATION TITLE',
      field: 'notificationTitle',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'RELATED INCIDENT ID',
      field: 'relatedIncidentId',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'DATE OF ISSUE',
      field: 'dateOfIssue',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'ACTION',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'CONFIRMATION',
      field: 'confirmation',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'STATUS',
      field: 'status',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'ACTIONS',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deleteFleetNotification.bind(this),
        onClickEdit: this.editItems.bind(this),
        onClickDetails: this.getFleetDetails.bind(this),
        showCustomIcon: true,
        customIconName: 'list',
        onCustomIconClick: this.openConfirmationByVesselTable.bind(this),
      },
      filter: 'agTextColumnFilter',
    },
  ];

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
  };
  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };
  fnuid: any;
  paginationPageSize = 5;
  displayDialog: boolean;
  selectedFleetNotificationID: string;
  selectedFleetNotificationName: string;

  constructor(
    private route: ActivatedRoute,
    private fleetNotificationService: FleetNotificationService,
    private notifyService: NotificationService,
    private router: Router,
    private dateFormatter: DatePipe
  ) {
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  onAddForm() {
    this.router.navigate(['./form'], { relativeTo: this.route });
  }

  ngOnInit() {
    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };
  }

  getFleetNoftifications() {
    this.fleetNotificationService.getFleetNotification().subscribe(
      (response) => {
        log.debug(`${response}`);
        this.rowData = response.map((data: any) => {
          data.dateOfIssue = this.dateFormatter.transform(new Date(data.dateOfIssue), 'dd-MMM-yyyy');
          return data;
        });
        if (!response) {
          const message = 'No Record Found!';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  onGridReady(params: any) {
    log.debug('reached');
    this.getFleetNoftifications();
  }

  /**
   * Delete  Fleet Notification Data
   */

  deleteFleetNotification(params: any) {
    if (params) {
      this.selectedFleetNotificationID = params.rowData.fleetNotificationId;
      this.selectedFleetNotificationName = params.rowData.notificationTitle;
      this.displayDialog = false;
      setTimeout(() => {
        this.displayDialog = true;
      }, 10);
    }
  }

  afterDeleteDialogClosed(event: any) {
    if (event && !!this.selectedFleetNotificationID) {
      this.fleetNotificationService
        .deleteFleetNotification(this.selectedFleetNotificationID)
        .pipe(untilDestroyed(this))
        .subscribe(
          (response: { statusCode: number; message: string }) => {
            if (response.statusCode === 200) {
              this.notifyService.showSuccess(response.message, this.title);
              this.getFleetNoftifications();
              this.dialog.close();
            } else {
              this.notifyService.showError(response.message, this.title);
            }
          },
          (error) => {
            this.error = error;
            const message = error;
            this.notifyService.showError(message, this.title);
          }
        );
    }
  }

  onDialogOkClick() {
    this.dialog.onAfterClosed(true);
  }

  editItems(params: any) {
    log.debug('router', params.rowData.fleetNotificationId);
    log.debug('link', this.router);
    log.debug('params data', params);
    this.id = params.rowData.fleetNotificationId;
    this.router.navigate([`./${this.id}/edit`], { relativeTo: this.route });
  }

  getFleetDetails(params: any) {
    log.debug('router', params.rowData.fleetNotificationId);
    log.debug('link', this.router);
    log.debug('params data', params);
    this.fnuid = params.rowData.fleetNotificationId;
    this.router.navigate([`./${this.fnuid}/details`], { relativeTo: this.route });
  }

  openConfirmationByVesselTable(params: any) {
    this.fnuid = params.rowData.fleetNotificationId;
    this.router.navigate([`applicable-vessels`, this.fnuid], { relativeTo: this.route });
  }

  ngOnDestroy() {}
}
