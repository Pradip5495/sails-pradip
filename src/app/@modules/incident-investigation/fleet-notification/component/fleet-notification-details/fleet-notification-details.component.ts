import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FleetNotificationService } from '../../fleet-notification.service';
import { Logger } from '@core';
import { environment } from '@env/environment';

const log = new Logger('Fleet-Notification-Form');
@Component({
  selector: 'app-fleet-notification-details',
  templateUrl: './fleet-notification-details.component.html',
  styleUrls: ['./fleet-notification-details.component.css'],
})
export class FleetNotificationDetailsComponent implements OnInit {
  public dataSource: any;
  public fid: any;
  public API_URL = environment.filePath;
  fleetNotificationDetailsData: any = [];

  constructor(
    private route: ActivatedRoute,
    private fleetNotificationService: FleetNotificationService,
    private router: Router
  ) {}

  ngOnInit() {
    const paramValue = 'value';
    if (this.route.params[paramValue].fnuid) {
      this.fid = this.route.params[paramValue].fnuid;
      log.debug('fid', this.fid);
      this.getFleetNotificationById(this.fid);
    }
  }
  getFleetNotificationById(fleetId: any) {
    this.fleetNotificationService.getFleetNotificationById(fleetId).subscribe(
      (response) => {
        this.dataSource = [];
        this.fleetNotificationDetailsData = response[0];
        log.debug('feelt deatils', this.fleetNotificationDetailsData);
        // this.fleetVesselData = response;
        // if (!response) {
        //   this.isdataSource = true;
        // }
        // this.dataSource = new MatTableDataSource(this.fleetVesselData);
      },
      (error) => {}
    );
  }

  navigateBack() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }
}
