import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetNotificationDetailsComponent } from './fleet-notification-details.component';

describe('FleetNotificationDetailsComponent', () => {
  let component: FleetNotificationDetailsComponent;
  let fixture: ComponentFixture<FleetNotificationDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FleetNotificationDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetNotificationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
