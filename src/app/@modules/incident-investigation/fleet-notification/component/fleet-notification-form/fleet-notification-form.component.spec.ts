import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetNotificationFormComponent } from './fleet-notification-form.component';

describe('FleetNotificationFormComponent', () => {
  let component: FleetNotificationFormComponent;
  let fixture: ComponentFixture<FleetNotificationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FleetNotificationFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetNotificationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
