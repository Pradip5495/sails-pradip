import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FleetNotificationService } from '../../fleet-notification.service';
import { Logger, untilDestroyed } from '@core';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from '@shared/common-service';
import { FormlyFormOptions, FormlyFieldConfig } from '@formly';
import { ICustomFormlyConfig } from '@types';
import { of } from 'rxjs';
import { UserService } from '@shared';
import { FormlyImagePreviewComponent } from '@shared/component/formly-image-preview/formly-image-preview.component';
import { VesselService } from '@shared/service';
import { IncidentService } from '@modules/incident-investigation/incident/share/incident.service';

const log = new Logger('Fleet-Notification-Form');

@Component({
  selector: 'app-fleet-notification-form',
  templateUrl: './fleet-notification-form.component.html',
  styleUrls: ['./fleet-notification-form.component.scss'],
})
export class FleetNotificationFormComponent implements OnInit, OnDestroy {
  fleetVesselData: any = [];
  error: string | undefined;
  public fleetNotificationData: any;
  public title = 'Fleet Notification';

  userData: any;

  statusArray: any[] = [];
  uploadedFiles: File[] = [];
  fleetNotificationPayload: any = {};
  isEditMode: boolean;
  fleetNotificationId: string;
  fleetNotificationDataToUpdate: any;

  overviewFormKeys = {
    vesselTypeId: 'vesselTypeId',
    issuedbyName: 'issuedByNameId',
    notificationTitle: 'notificationTitle',
    incidentId: 'incidentId',
  };
  overviewForm = new FormGroup({});
  overviewModel = {};
  overviewOptions: FormlyFormOptions = {
    formState: {
      incidents: this.incidentService.getAllIncidents(),
      users: this.userService.getUsers(),
    },
  };
  overviewFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.overviewFormKeys.vesselTypeId,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Applicable to Vessel Type',
            options: this.vesselService.getVesselTypes(),
            valueProp: 'vesselTypeId',
            labelProp: 'vesselType',
            required: true,
            multiple: true,
          },
        },
        {
          key: this.overviewFormKeys.issuedbyName,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Issued By Name',
            options: [],
            valueProp: 'userId',
            labelProp: 'firstname',
            required: true,
          },
          expressionProperties: {
            'templateOptions.options': 'formState.users',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.overviewFormKeys.notificationTitle,
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Notification Title',
            placeholder: 'Enter Vessel Name',
            required: true,
          },
          hooks: {
            onInit: (field) => {
              let incidents: any;
              this.overviewOptions.formState.incidents.pipe(untilDestroyed(this)).subscribe((value1: any) => {
                incidents = value1;
              });
              this.overviewForm
                .get(this.overviewFormKeys.incidentId)
                .valueChanges.pipe(untilDestroyed(this))
                .subscribe((value) => {
                  if (incidents) {
                    const selectedVessel = incidents.find((incident: any) => incident.incidentId === value);
                    this.overviewForm.patchValue({
                      notificationTitle: selectedVessel ? selectedVessel.incidentTitle : '',
                    });
                  }
                });
            },
          },
        },
        {
          className: 'flex-1',
          key: this.overviewFormKeys.incidentId,
          type: 'select',
          defaultValue: '',
          templateOptions: {
            label: 'Report Id',
            placeholder: 'Report Id',
            appearance: 'outline',
            required: true,
            options: [],
            valueProp: 'incidentId',
            labelProp: 'reportId',
          },
          expressionProperties: {
            'templateOptions.options': 'formState.incidents',
          },
        },
      ],
    },
  ];

  descriptionKey = 'description';
  descriptionForm = new FormGroup({});
  descriptionModel = {};
  descriptionOptions: FormlyFormOptions = {};
  descriptionFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.descriptionKey,
          type: 'textarea',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Description',
            placeholder: 'Description',
            rows: 10,
          },
        },
      ],
    },
  ];

  attachmentFileKey = 'files';
  attachmentForm = new FormGroup({});
  attachmentModel = {};
  attachmentOptions: FormlyFormOptions = {
    formState: {
      images: [],
    },
  };
  attachmentFields: FormlyFieldConfig[] = [
    {
      type: 'image-template',
      templateOptions: {
        component: FormlyImagePreviewComponent,
        props: {
          key: 'date',
          label: 'Date',
          delete: this.deleteFleetNotificationAttachment.bind(this),
        },
      },
    },
    {
      key: this.attachmentFileKey,
      type: 'sailfile',
      templateOptions: {
        preview: true,
        id: 'fleet-file',
        onFileChange: function filesAdded(files: any) {
          this.onFormlyAttachmentSubmit(null, files);
        }.bind(this),
      },
    },
  ];

  vesselsFormKeys = {
    vesselSelection: 'vesselSelection',
    vesselIds: 'vesselIds',
    email: 'email',
    vesselId: 'vesselId',
  };
  vesselsForm = new FormGroup({});
  vesselsModel = {};
  vesselsOptions: FormlyFormOptions = {};
  vesselsFields: FormlyFieldConfig[] = [
    {
      template:
        '<div style="display: block; width: 100%; border-bottom: 5px solid black"> <b> Vessel Type *</b> </div>',
    },
    {
      template: '<hr />',
    },
    {
      fieldGroup: [
        {
          key: this.vesselsFormKeys.vesselSelection,
          type: 'radio',
          templateOptions: {
            className: 'flex-1',
            required: true,
            options: this.vesselService.getVesselTypes(),
            valueProp: 'vesselTypeId',
            labelProp: 'vesselType',
          },
        },
        {
          template: '<hr />',
        },
        {
          key: this.vesselsFormKeys.vesselIds,
          type: 'multicheckbox',
          templateOptions: {
            label: 'Vessel',
            className: 'flex-1',
            required: true,
            options: [],
            valueProp: 'vesselId',
            labelProp: 'vessel',
          },
          hideExpression: (model, formState, field) => {
            return !field.templateOptions.options;
          },
          hooks: {
            onInit: (field) => {
              this.vesselsForm
                .get(this.vesselsFormKeys.vesselSelection)
                .valueChanges.pipe(untilDestroyed(this))
                .subscribe((value) => {
                  const selectedVessel = this.fleetVesselData.filter((vessels: any) => vessels.vesselTypeId === value);
                  field.templateOptions.options = selectedVessel;
                });
            },
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.vesselsFormKeys.email,
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Additional Email Ids',
            description: 'Separate Email with Comma',
          },
          validators: {
            validation: ['multipleEmails'],
          },
        },
      ],
    },
  ];

  actionFormKeys = {
    action: 'action',
    natureOfAction: 'natureOfAction',
    dueDate: 'dueDate',
    status: 'status',
    dateCompleted: 'dateCompleted',
    send: 'send',
  };
  actionsForm = new FormGroup({});
  actionsModel = {
    action: [{}],
  };
  actionsOptions: FormlyFormOptions = {};
  actionsFields: FormlyFieldConfig[] = [
    {
      type: 'repeat',
      key: this.actionFormKeys.action,
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add Action',
        addTextClass: 'add-btn',
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'input',
                key: this.actionFormKeys.natureOfAction,
                templateOptions: {
                  placeholder: 'Nature of Action',
                  label: 'Nature of Action',
                  required: true,
                },
              },
              {
                className: 'flex-1',
                type: 'matdatepicker',
                key: this.actionFormKeys.dueDate,
                defaultValue: '',
                templateOptions: {
                  label: 'Due Date',
                  appearance: 'outline',
                  datepickerOptions: {
                    // max: new Date(),
                  },
                },
              },
              {
                className: 'flex-1',
                key: this.actionFormKeys.status,
                type: 'autocomplete',
                defaultValue: 'Open',
                templateOptions: {
                  label: 'Status',
                  required: true,
                  appearance: 'outline',
                  filter: (term: string) => of(term ? this.filterStatus(term) : this.statusArray.slice()),
                },
              },
              {
                className: 'flex-1',
                type: 'matdatepicker',
                key: this.actionFormKeys.dateCompleted,
                defaultValue: '',
                templateOptions: {
                  label: 'Completed Date',
                  appearance: 'outline',
                  datepickerOptions: {
                    // max: new Date(),
                  },
                },
              },
            ],
          },
        ],
      },
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.actionFormKeys.send,
          type: 'checkbox',
          defaultValue: false,
          templateOptions: {
            label: 'Send *',
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      shortName: 'Overview',
      title: 'Overview',
      subTitle: 'Enter Overview Data',
      config: {
        model: this.overviewModel,
        fields: this.overviewFields,
        options: this.overviewOptions,
        form: this.overviewForm,
      },
      onSave: this.onFormlyOveriewSubmit.bind(this),
      discard: true,
      onDiscard: () => this.overviewForm.reset(),
    },
    {
      shortName: 'Description',
      title: 'Description',
      subTitle: 'Enter Description Data',
      config: {
        model: this.descriptionModel,
        fields: this.descriptionFields,
        options: this.descriptionOptions,
        form: this.descriptionForm,
      },
      onSave: this.onFormlyDescriptionSubmit.bind(this),
      discard: true,
      onDiscard: () => this.descriptionForm.reset(),
    },
    {
      shortName: 'Attachment',
      title: 'Attachment',
      subTitle: 'Enter Attachment Data',
      config: {
        model: this.attachmentModel,
        fields: this.attachmentFields,
        options: this.attachmentOptions,
        form: this.attachmentForm,
      },
      onSave: this.onFormlyAttachmentSubmit.bind(this),
      discard: true,
      onDiscard: () => this.attachmentForm.reset(),
    },
    {
      shortName: 'Vessels',
      title: 'Vessels',
      subTitle: 'Enter Vessels Data',
      config: {
        model: this.vesselsModel,
        fields: this.vesselsFields,
        options: this.vesselsOptions,
        form: this.vesselsForm,
      },
      onSave: this.onFormlyVesselsSubmit.bind(this),
      discard: true,
      onDiscard: () => this.vesselsForm.reset(),
    },
    {
      shortName: 'Actions',
      title: 'Actions',
      subTitle: 'Enter Actions Data',
      config: {
        model: this.actionsModel,
        fields: this.actionsFields,
        options: this.actionsOptions,
        form: this.actionsForm,
      },
      onSave: this.onFinalSubmit.bind(this),
      discard: true,
      onDiscard: () => this.actionsForm.reset(),
    },
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private notifyService: NotificationService,
    private fleetNotificationService: FleetNotificationService,
    private userService: UserService,
    private vesselService: VesselService,
    private incidentService: IncidentService
  ) {
    this.userData = this.userService.currentUser;
    this.overviewFields[0].fieldGroup[1].defaultValue = this.userData.userId;
  }

  /* This function is used by autocomplete field in Action section */
  filterStatus(name: string) {
    return this.statusArray.filter((state: any) => state.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  ngOnInit() {
    // Used in Vessels Step to Display Checkboxes
    this.getVessel();
    /* For Auto Complete Select Field Status */
    this.getAllTypes();
    const value = 'value';
    this.fleetNotificationId = this.activatedRoute.params[value].fnuid;
    this.isEditMode = !!this.fleetNotificationId ? true : false;
    if (this.isEditMode) {
      this.getFleetNotificationData();
    }
  }

  navigateBack() {
    const backRoute = !this.isEditMode ? '../' : '../../';
    this.router
      .navigate([backRoute], { relativeTo: this.activatedRoute })
      .then((r) => {})
      .catch();
  }

  ngOnDestroy() {}

  async getFleetNotificationData() {
    this.fleetNotificationDataToUpdate = await this.fleetNotificationService
      .getFleetNotificationById(this.fleetNotificationId)
      .pipe(untilDestroyed(this))
      .toPromise();

    // Mapping the data for Dates
    if (this.fleetNotificationDataToUpdate.length > 0) {
      this.fleetNotificationDataToUpdate.map((fleetData: any) => {
        fleetData.date = new Date(fleetData.date);
        if (fleetData.email) {
          fleetData.email = fleetData.email.toString();
        }
        if (fleetData.files) {
          fleetData.files.map((file: any, index: number) => {
            const splittedFileName = file.path.split('.');
            fleetData.files[index].fileType = splittedFileName[splittedFileName.length - 1];
          });
        }
        if (fleetData.action) {
          fleetData.action.map((actionData: any, index: number) => {
            if (actionData.dateCompleted) {
              actionData.dateCompleted = new Date(actionData.dateCompleted);
            }
            if (actionData.dueDate) {
              actionData.dueDate = new Date(actionData.dueDate);
            }
          });
        }
        // Requried in Overfiew Form Vessel Type Field
        if (fleetData.vesselType) {
          fleetData.vesselTypeId = fleetData.vesselType.map((vessel: any) => {
            return vessel.vesselTypeId;
          });
        }
        // Required in Vessels Form Step for Radio and checkbox selection
        if (fleetData.vessel) {
          fleetData.vesselSelection = fleetData.vessel.map((vessel: any) => {
            return vessel.vesselTypeId;
          });
          const initialValue = {};
          fleetData.vesselIds = fleetData.vessel.reduce((obj: any, item: any) => {
            return {
              ...obj,
              [item.vesselId]: true,
            };
          }, initialValue);
        }
        return fleetData;
      });
    }

    this.initializeOverviewFormDataInEditMode(this.fleetNotificationDataToUpdate[0]);
    this.initializeDescriptionFormDataInEditMode(this.fleetNotificationDataToUpdate[0]);
    this.displayImagePreviewsIfAttached(this.fleetNotificationDataToUpdate[0]);
    this.initializeVesselsFormDataInEditMode(this.fleetNotificationDataToUpdate[0]);
    this.initializeActionFormDataInEditMode(this.fleetNotificationDataToUpdate[0]);
  }

  initializeOverviewFormDataInEditMode(fleetNotificationData: any) {
    if (fleetNotificationData) {
      const formFieldsData = {
        vesselTypeId: fleetNotificationData.vesselTypeId,
        issuedByNameId: fleetNotificationData.issuedByNameId,
        notificationTitle: fleetNotificationData.notificationTitle,
        incidentId: fleetNotificationData.incidentId,
      };
      this.overviewForm.patchValue(formFieldsData);
    }
  }

  initializeDescriptionFormDataInEditMode(fleetNotificationData: any) {
    if (fleetNotificationData) {
      const formFieldsData = {
        description: fleetNotificationData.description,
      };
      this.descriptionForm.patchValue(formFieldsData);
    }
  }

  displayImagePreviewsIfAttached(fleetNotificationData: any) {
    if (fleetNotificationData.files) {
      this.attachmentOptions.formState.images = fleetNotificationData.files;
    }
  }

  initializeVesselsFormDataInEditMode(fleetNotificationData: any) {
    if (fleetNotificationData) {
      const formFieldsData = {
        vesselSelection: fleetNotificationData.vesselSelection[0],
        email: fleetNotificationData.email,
      };
      this.vesselsForm.patchValue(formFieldsData);

      setTimeout(() => {
        this.vesselsForm.patchValue({ vesselIds: fleetNotificationData.vesselIds });
      }, 150);
    }
  }

  initializeActionFormDataInEditMode(fleetNotificationData: any) {
    if (fleetNotificationData) {
      // const formFieldsData = {
      //   action: fleetNotificationData.action,
      //   send: fleetNotificationData.email,
      // };
      // this.actionsForm.patchValue({action: fleetNotificationData.action});
      this.actionsModel = {
        ...this.actionsModel,
        action: fleetNotificationData.action,
      };
    }
  }

  //#region Form Submit Function Region Start

  onFormlyOveriewSubmit(formData: any) {
    this.fleetNotificationPayload = { ...this.fleetNotificationPayload, ...formData };
  }

  onFormlyDescriptionSubmit(formData: any) {
    if (formData.description) {
      this.fleetNotificationPayload = { ...this.fleetNotificationPayload, ...formData };
    } else {
      if (this.fleetNotificationPayload.hasOwnProperty(this.descriptionKey)) {
        delete this.fleetNotificationPayload[this.descriptionKey];
      }
    }
  }

  onFormlyAttachmentSubmit(formData: any, files: any) {
    this.uploadedFiles = files ? files : this.uploadedFiles;

    if (this.uploadedFiles.length > 0) {
      this.fleetNotificationPayload = { ...this.fleetNotificationPayload, ...{ files: this.uploadedFiles } };
    } else {
      if (this.fleetNotificationPayload.hasOwnProperty(this.attachmentFileKey)) {
        delete this.fleetNotificationPayload[this.attachmentFileKey];
      }
    }
  }

  onFormlyVesselsSubmit(formData: any) {
    // Preparing VesselIds Array
    const vesselIds = [];
    if (formData.vesselIds) {
      for (const [key, value] of Object.entries(formData.vesselIds)) {
        if (value) {
          vesselIds.push(key);
        }
      }
    }

    // Preparing EmailIds Array
    const emailIds: string[] = [];
    if (formData.email) {
      const emailArray = formData.email.split(',');
      emailArray.forEach((value: string) => {
        if (value) {
          emailIds.push(value.trim());
        }
      });
    }

    const modifiedVesselFormData = {};
    if (emailIds.length > 0) {
      modifiedVesselFormData[this.vesselsFormKeys.email] = emailIds;
    } else {
      if (this.fleetNotificationPayload.hasOwnProperty(this.vesselsFormKeys.email)) {
        delete this.fleetNotificationPayload[this.vesselsFormKeys.email];
      }
    }
    if (vesselIds.length > 0) {
      modifiedVesselFormData[this.vesselsFormKeys.vesselId] = vesselIds;
    } else {
      if (this.fleetNotificationPayload.hasOwnProperty(this.vesselsFormKeys.vesselId)) {
        delete this.fleetNotificationPayload[this.vesselsFormKeys.vesselId];
      }
    }

    this.fleetNotificationPayload = { ...this.fleetNotificationPayload, ...modifiedVesselFormData };
  }

  onFormlyActionsSubmit(formData: any) {
    const actions = [...formData.action];
    for (const action of actions) {
      if (action) {
        if (!isNaN(Date.parse(action[this.actionFormKeys.dateCompleted]))) {
          action[this.actionFormKeys.dateCompleted] = new Date(action[this.actionFormKeys.dateCompleted]).toISOString();
        } else {
          if (action.hasOwnProperty(this.actionFormKeys.dateCompleted)) {
            delete action[this.actionFormKeys.dateCompleted];
          }
        }

        if (!isNaN(Date.parse(action[this.actionFormKeys.dueDate]))) {
          action[this.actionFormKeys.dueDate] = new Date(action[this.actionFormKeys.dueDate]).toISOString();
        } else {
          if (action.hasOwnProperty(this.actionFormKeys.dueDate)) {
            delete action[this.actionFormKeys.dueDate];
          }
        }

        action[this.actionFormKeys.status] = action[this.actionFormKeys.status];
        action[this.actionFormKeys.natureOfAction] = action[this.actionFormKeys.natureOfAction];
        action.isFleetNotification = 1;
      }
    }
    const modifiedActionFormData = {
      send: formData.send ? '1' : '0',
      action: actions,
    };
    this.fleetNotificationPayload = { ...this.fleetNotificationPayload, ...modifiedActionFormData };
  }

  onFinalSubmit() {
    /* Checking if Below three forms data is saved or not */
    this.onFormlyOveriewSubmit(this.overviewForm.value);

    this.onFormlyDescriptionSubmit(this.descriptionForm.value);

    this.onFormlyVesselsSubmit(this.vesselsForm.value);

    this.onFormlyActionsSubmit(this.actionsForm.value);

    if (this.overviewForm.valid && this.actionsForm.valid && this.vesselsForm.valid) {
      if (!this.isEditMode) {
        this.createFleetNotificationWithFormly(this.fleetNotificationPayload);
        return;
      }
      this.updateFleetNotificationWithFormly(this.fleetNotificationPayload);
    } else {
      this.notifyService.showWarning('Please fill All Mandatory Fields', this.title);
    }
  }

  createFormDataFromObject(payLoad: any): FormData {
    const formData = new FormData();
    let i = 0;
    for (const key in payLoad) {
      if (key === 'files') {
        for (const file of payLoad[key]) {
          formData.append(`${key}`, file);
        }
      } else if (key === 'vesselTypeId' || key === 'vesselId' || key === 'email') {
        i = 0;
        for (const data of payLoad[key]) {
          formData.append(`${key}[${i}]`, data);
          i++;
        }
      } else if (key === 'action') {
        i = 0;
        for (const action of payLoad[key]) {
          if (action.dateCompleted) {
            formData.append(`${key}[${i}][dateCompleted]`, action.dateCompleted);
          }
          if (action.dueDate) {
            formData.append(`${key}[${i}][dueDate]`, action.dueDate);
          }
          formData.append(`${key}[${i}][isFleetNotification]`, '1');
          formData.append(`${key}[${i}][natureOfAction]`, action.natureOfAction);
          formData.append(`${key}[${i}][status]`, action.status);
          i++;
        }
      } else {
        formData.append(key, payLoad[key]);
      }
    }
    return formData;
  }
  //#endregion Form Submit Function Region End

  //#region API Calls Region Start

  getAllTypes() {
    this.fleetNotificationService.getAllTypes().subscribe(async (data: any) => {
      if (data) {
        this.statusArray = (await data.status) ? data.status : [];
      }
    });
  }

  getVessel() {
    this.vesselService.getVessels().subscribe(
      (response) => {
        this.fleetVesselData = response;
        if (this.fleetVesselData.statusCode === 200) {
          this.fleetVesselData = this.fleetVesselData.data;
        }
      },
      (error) => {}
    );
  }

  createFleetNotificationWithFormly(payLoad: any) {
    const formData: FormData = this.createFormDataFromObject(payLoad);

    this.fleetNotificationService
      .createFleetNotification(formData)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response) => {
          log.debug('response: ', response);
          this.fleetNotificationData = response;
          log.debug('Responese', this.fleetNotificationData);
          if (this.fleetNotificationData.statusCode === 201) {
            const message = 'New Fleet Notification created successfully!';
            this.notifyService.showSuccess(message, this.title);
            this.navigateBack();
          } else {
            const message = this.fleetNotificationData.message;
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
          // this._snackBar.open(message, '', {
          //   duration: 4000
          // });
        }
      );
  }

  updateFleetNotificationWithFormly(payLoad: any) {
    const formData: FormData = this.createFormDataFromObject(payLoad);
    formData.set('isActive', '1');

    this.fleetNotificationService
      .updateFleetNotification(formData, this.fleetNotificationId)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response) => {
          log.debug('response: ', response);
          this.fleetNotificationData = response;
          log.debug('Responese', this.fleetNotificationData);
          if (this.fleetNotificationData.statusCode === 200) {
            const message = 'New Fleet Notification created successfully!';
            this.notifyService.showSuccess(message, this.title);
            this.navigateBack();
          } else {
            const message = this.fleetNotificationData.message;
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
          // this._snackBar.open(message, '', {
          //   duration: 4000
          // });
        }
      );
  }

  deleteFleetNotificationAttachment(imageData: any) {
    if (imageData) {
      this.fleetNotificationService
        .deleteFleetNotificationImage(imageData.fleetNotificationAttachmentId)
        .pipe(untilDestroyed(this))
        .subscribe((response: any) => {
          if (response.statusCode === 200) {
            // Filtering the Deleted Image
            const images = this.attachmentOptions.formState.images;
            const filteredImages = images.filter((image: any) => {
              return image.fleetNotificationAttachmentId !== imageData.fleetNotificationAttachmentId;
            });
            this.attachmentOptions.formState.images = filteredImages;

            this.notifyService.showSuccess(response.message, this.title);
          } else {
            this.notifyService.showSuccess(response.message, this.title);
          }
        });
    }
  }

  //#endregion API Calls Region End
}
