import { Component, OnInit, ViewChild } from '@angular/core';
import { Logger } from '@core';
import { FleetNotificationService } from '../../fleet-notification.service';
import { CustomizedCellComponent } from '@shared';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { DialogComponent } from '@shared/component';

const log = new Logger('Fleet-Notification');

@Component({
  selector: 'app-fleet-vessel-confirmation-table',
  templateUrl: './fleet-vessel-confirmation-table.component.html',
  styleUrls: ['./fleet-vessel-confirmation-table.component.scss'],
})
export class FleetVesselConfirmationTableComponent implements OnInit {
  @ViewChild('dialog') dialog: DialogComponent;

  selectedApplicableVesselId: string;
  openDialog: boolean;
  rowData: any;
  editType = '';
  domLayout = 'autoHeight';

  columnDefs = [
    {
      field: 'vessel',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      field: 'confirmed',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Date Confirmed',
      field: 'dateConfirmed',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Action Due',
      field: 'actionDue',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Action Completed',
      field: 'actionCompleted',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Date Completed',
      field: 'dateCompleted',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'ACTIONS',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        hideEditIcon: true,
        hideDetailsButton: true,
        hideDeleteIcon: true,
        showCustomIcon: true,
        customIconName: 'find_in_page',
        onCustomIconClick: this.openApplicableVessel.bind(this),
      },
      filter: 'agTextColumnFilter',
    },
  ];

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
  };
  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };
  fnuid: string;
  paginationPageSize = 5;

  constructor(
    private fleetNotificationService: FleetNotificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dateTransform: DatePipe
  ) {}

  ngOnInit() {
    const value = 'value';
    this.fnuid = this.activatedRoute.params[value].fnuid;
  }

  backToFleetNotification() {
    this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
  }

  getFleetVessels() {
    if (this.fnuid) {
      this.fleetNotificationService.getFleetVesselsById(this.fnuid).subscribe(
        (response: any) => {
          log.debug(`${response}`);
          response.map((fleetVessel: any) => {
            if (fleetVessel.dateConfirmed) {
              fleetVessel.dateConfirmed = this.dateTransform.transform(fleetVessel.dateConfirmed, 'dd-MMM-yyy');
            }
            if (fleetVessel.dateCompleted) {
              fleetVessel.dateCompleted = this.dateTransform.transform(fleetVessel.dateCompleted, 'dd-MMM-yyy');
            }
          });
          this.rowData = response;
        },
        (error) => {}
      );
    }
  }

  openApplicableVessel(params: any) {
    this.selectedApplicableVesselId = params.rowData.applicableVesselId;
    log.debug('reached', params);
    this.openDialog = false;
    setTimeout(() => {
      this.openDialog = true;
    }, 50);
  }

  onGridReady(params: any) {
    log.debug('reached', params);
    this.getFleetVessels();
  }

  onFormResponse(response: boolean) {
    if (response) {
      this.dialog.close();
      this.getFleetVessels();
    }
  }
}
