import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetVesselConfirmationTableComponent } from './fleet-vessel-confirmation-table.component';

describe('FleetVesselConfirmationTableComponent', () => {
  let component: FleetVesselConfirmationTableComponent;
  let fixture: ComponentFixture<FleetVesselConfirmationTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FleetVesselConfirmationTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetVesselConfirmationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
