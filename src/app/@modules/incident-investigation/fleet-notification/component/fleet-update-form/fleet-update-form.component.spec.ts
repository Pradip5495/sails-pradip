import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetUpdateFormComponent } from './fleet-update-form.component';

describe('FleetUpdateFormComponent', () => {
  let component: FleetUpdateFormComponent;
  let fixture: ComponentFixture<FleetUpdateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FleetUpdateFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
