import { Component, OnInit, VERSION, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FleetNotificationService } from '../../fleet-notification.service';
import { Logger } from '@core';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { MatStepper } from '@angular/material/stepper';
import { NotificationService } from '@shared/common-service';
import { environment } from '../../../../../../environments/environment';
import { UserService } from '@shared';
import { VesselService } from '@shared/service';
import { IncidentService } from '@modules/incident-investigation/incident/share/incident.service';

const log = new Logger('Fleet-Notification-Form');

@Component({
  selector: 'app-fleet-update-form',
  templateUrl: './fleet-update-form.component.html',
  styleUrls: ['./fleet-update-form.component.scss'],
})
export class FleetUpdateFormComponent implements OnInit {
  ngVersion: string = VERSION.full;

  @ViewChild('stepper') stepper: MatStepper;
  public isLinear = false;

  fleetOverviewFormGroup: FormGroup;
  descriptionFormGroup: FormGroup;
  attachmentFormGroup: FormGroup;
  actionForm: FormGroup;
  vesselFormGroup: FormGroup;
  incidentList: any;
  incidentData: any;
  userList: any;

  vesselArray: Array<any> = [];

  vesselTypeArray: any = [];
  vesselsArray: any = [];

  vesselType: any = [];
  emailIds: any = [];
  vesselTypeData: any = [];
  fleetVesselTypeData: any = [];
  fleetVesselType: any;
  fleetVesselData: any = [];
  userData: any;
  designationData: any = [];
  selectedIds: any = [];
  error: string | undefined;
  checked = true;
  public API_URL = environment.filePath;
  public allFiles: any = [];
  public images: any = [];
  public fleetNotificationGetData: any;
  public fleetNotificationImagesData: any;
  public fleetNotificationImage: any;
  public subDetails: any;
  public fnuid: any;
  public vesselTypeList = 'Oil Tanker';
  public title = 'Fleet Notification';
  public fleetNotificationData: any;
  public activeIndex: any = 1;
  private _checkBoxChecked = false;

  constructor(
    private formBuilder: FormBuilder,
    private fleetNotificationService: FleetNotificationService,
    private vesselService: VesselService,
    private userService: UserService,
    private incidentService: IncidentService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private notifyService: NotificationService
  ) {}

  get checkBoxChecked() {
    return this._checkBoxChecked;
  }

  set checkBoxChecked(val: any) {
    this._checkBoxChecked = val;
    this.actionForm.get('status').setValue(this.checkBoxChecked);
  }

  get f() {
    return this.actionForm.controls;
  }

  ngOnInit() {
    this.getVesselType();
    this.getVessel();
    this.getDesignation();
    this.getAllIncident();
    this.getAllUsers();
    this.createFleetOverviewForm();
    this.createFleetDescriptionForm();
    this.createAttachmentFormGroup();
    this.createFleetVesselForm();
    this.createAction();
    const paramValue = 'value';
    if (this.route.params[paramValue].id) {
      this.fnuid = this.route.params[paramValue].id;
      this.getFleetNotificationById(this.fnuid);
    }
  }

  _onChangeVesselType = (_: any) => {};

  onChangeVessel(vesselType: any) {
    log.debug('testing');
    this._onChangeVesselType(this.vesselTypeList);
    this.vesselTypeList = vesselType;
  }

  move(index: number) {
    this.stepper.selectedIndex = index;
    this.activeIndex = index + 1;
  }

  getStepDone(index: number) {
    if (index < this.activeIndex) {
      const primary = 'primary';
      return primary;
    }
  }

  actionFields(): FormGroup {
    return this.formBuilder.group({
      actionId: [''],
      type: ['', Validators.required],
      natureOfAction: ['', Validators.required],
      dueDate: [''],
      designationId: [''],
      status: [this.checkBoxChecked],
      dateCompleted: [''],
    });
  }

  onDeleteAction(index: number) {
    const actionForm = this.actionForm.controls.action as FormArray;
    actionForm.removeAt(index);
  }

  getActionControls() {
    const actionF = this.actionForm.controls.action as FormArray;
    return actionF;
  }

  onChange(vesselId: string, isChecked: boolean) {}

  onFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      log.debug('reached Again');
      const filesAmount = event.target.files.length;
      log.debug('filesAmount:', filesAmount);
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        log.debug('urls is:', this.images);
        this.allFiles.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.images.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }

  deleteImage(image: any) {
    const index = this.images.indexOf(image);
    this.images.splice(index, 1);
    this.allFiles.splice(index, 1);
    this.onFileChange(this.images);
  }

  // Get Fleet Notification Details By Id
  getFleetNotificationById(fnuid: any) {
    log.debug('fnuid: ', fnuid);
    this.fleetNotificationService.getFleetNotificationById(fnuid).subscribe(
      (response) => {
        this.fleetNotificationData = response[0];
        this.fleetNotificationGetData = response;
        log.debug('Files details', this.fleetNotificationGetData);
        this.fleetNotificationImage = this.fleetNotificationGetData.files;
        log.debug('Files details after get Api calling', this.fleetNotificationGetData.files);
        this.setFleetNotificationFormData();
        log.debug('this.fleetNotificationData:', this.fleetNotificationData);
      },
      (error) => {
        this.error = error;
        const message = error;
        this._snackBar.open(message, '', {
          duration: 4000,
        });
      }
    );
  }

  onAddActionField() {
    const action = 'action';
    this.subDetails = this.actionForm.get(action) as FormArray;
    this.subDetails.push(this.actionFields());
  }

  setFleetNotificationFormData() {
    this.descriptionFormGroup.patchValue(this.fleetNotificationData);
    this.attachmentFormGroup.patchValue(this.fleetNotificationData);
    log.debug('Attachment Update senario', this.attachmentFormGroup);
    this.vesselFormGroup.patchValue(this.fleetNotificationData);
    this.setFormData();
    // this.actionForm.patchValue(this.fleetNotificationData);
  }

  setFormData() {
    const data = this.fleetNotificationData;
    // tslint:disable-next-line:forin
    for (const dataKey in data) {
      if (dataKey === 'action') {
        const action = this.fleetNotificationData.action;
        let index = 0;
        action.forEach((element: any) => {
          const convertedDate = moment(this.fleetNotificationData.action[index].dateCompleted).startOf('day');
          const observationYear = moment(convertedDate).format('YYYY-MM-DD');
          this.fleetNotificationData.action[index].dateCompleted = observationYear;

          const dueDates = moment(this.fleetNotificationData.action[index].dueDate).startOf('day');
          const dueDate = moment(dueDates).format('YYYY-MM-DD');
          this.fleetNotificationData.action[index].dueDate = dueDate;

          if (index > 0) {
            this.onAddActionField();
          }
          index++;
        });
        this.actionForm.patchValue(this.fleetNotificationData);
      }
      if (dataKey === 'vesselType') {
        const vesselTypeData = this.fleetNotificationData.vesselType;
        vesselTypeData.forEach((element: any) => {
          // const vessel = this.fleetNotificationData.vesselTypeId;
          this.vesselTypeArray.push(element.vesselTypeId);

          log.debug('vesselType', this.vesselTypeArray);
        });
        // this.fleetOverviewFormGroup.value.vesselTypeId = this.vesselTypeArray;
        this.fleetOverviewFormGroup.get('vesselTypeId').patchValue(this.vesselTypeArray);
        this.fleetOverviewFormGroup.patchValue(this.fleetNotificationData);
      }
      if (dataKey === 'vessel') {
        const vesselData = this.fleetNotificationData.vessel;
        vesselData.forEach((element: any) => {
          this.vesselsArray.push(element.vesselId);

          log.debug('vessel', this.vesselsArray);
        });
        this.vesselFormGroup.get('vesselId').patchValue(this.vesselsArray);
        this.vesselFormGroup.patchValue(this.fleetNotificationData);
      }
    }
  }

  // Get fleet notification images
  getFleetNotificationImages() {
    const fleetNotificationId = this.fleetNotificationData.fleetNotificationId;
    this.fleetNotificationService.getFleetNotificationById(fleetNotificationId).subscribe(
      (response) => {
        this.fleetNotificationImagesData = response;
        this.fleetNotificationImage = this.fleetNotificationImagesData.files;
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  deleteFleetNotificationImage(fnauid: any) {
    this.fleetNotificationService.deleteFleetNotificationImage(fnauid).subscribe(
      (response) => {
        if (response) {
          this.getFleetNotificationImages();
          const message = 'Fleet Notification Images deleted successfully!';
          this.notifyService.showSuccess(message, this.title);
        } else {
          const message = 'Fleet Notification Images not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getVesselType() {
    this.vesselService.getVesselTypes().subscribe(
      (response) => {
        this.fleetVesselType = response;
        if (this.fleetVesselType.statusCode === 200) {
          this.fleetVesselTypeData = this.fleetVesselType.data;
        }
      },
      (error) => {}
    );
  }

  getVessel() {
    this.vesselService.getVessels().subscribe(
      (response) => {
        this.fleetVesselData = response;
        if (this.fleetVesselData.statusCode === 200) {
          this.fleetVesselData = this.fleetVesselData.data;
        }
      },
      (error) => {}
    );
  }

  getDesignation() {
    this.userService.getDesignations().subscribe(
      (response) => {
        this.designationData = response;
        if (this.designationData.statusCode === 200) {
          this.designationData = this.designationData.data;
        }
      },
      (error) => {}
    );
  }

  // Get All Designation list data
  getAllIncident() {
    this.incidentService.getAllIncidents().subscribe(
      (response) => {
        this.incidentData = response;
        if (this.incidentData.statusCode === 200) {
          this.incidentData = this.incidentData.data;
        }
      },
      (error) => {}
    );
  }

  // Get All User list data
  getAllUsers() {
    this.userService.getUsers().subscribe(
      (response) => {
        this.userList = response;
        if (response) {
          this.userData = response;
        }
      },
      (error) => {}
    );
  }

  onSubmit() {
    log.debug('fleetOverviewForm: ', this.fleetOverviewFormGroup.value);
  }

  onSubmitDescription() {
    log.debug('onSubmitDescription: ', this.descriptionFormGroup.value);
  }

  onSubmitAttachment() {
    log.debug('Attachments Submit', this.attachmentFormGroup.value);
  }

  onSubmitVessel() {
    log.debug('vessel Submit', this.vesselFormGroup.value);
  }

  onSubmitActionForm() {
    this.actionForm.patchValue(this.fleetOverviewFormGroup.value);
    this.actionForm.patchValue(this.descriptionFormGroup.value);
    this.actionForm.patchValue(this.attachmentFormGroup.value);
    this.actionForm.patchValue(this.vesselFormGroup.value);
    const data = this.actionForm.value;
    const formData = new FormData();
    for (const dataKey in data) {
      if (dataKey === 'action') {
        const datas = data[dataKey];
        let index = 0;
        datas.forEach((element: any) => {
          for (const previewKey in element) {
            if (previewKey === 'dueDate') {
              if (datas[index][previewKey]) {
                const convertedDueDates = moment(datas[index][previewKey]).startOf('day');
                const dueDate = moment(convertedDueDates).format('YYYY-MM-DD');
                formData.append(`action[${index}][${previewKey}]`, dueDate);
              }
              // const convertedDueDate = moment(datas[index][previewKey]).startOf('day');
              // const dueDate = moment(convertedDueDate).format('YYYY-MM-DD');
              // formData.append(`action[${index}][${previewKey}]`, dueDate);
            } else if (previewKey === 'dateCompleted') {
              if (datas[index][previewKey]) {
                const convertedDateCompleted = moment(datas[index][previewKey]).startOf('day');
                const dateCompleted = moment(convertedDateCompleted).format('YYYY-MM-DD');
                formData.append(`action[${index}][${previewKey}]`, dateCompleted);
              }
              // const convertedDateCompleted = moment(datas[index][previewKey]).startOf('day');
              // const dateCompleted = moment(convertedDateCompleted).format('YYYY-MM-DD');
              // formData.append(`action[${index}][${previewKey}]`, dateCompleted);
            } else if (previewKey === 'status') {
              if (datas[index][previewKey] === true) {
                const keyValue = '1';
                formData.append(`action[${index}][${previewKey}]`, keyValue);
              } else {
                const keyValue = '0';
                formData.append(`action[${index}][${previewKey}]`, keyValue);
              }
            } else {
              formData.append(`action[${index}][${previewKey}]`, datas[index][previewKey]);
            }
          }
          index++;
        });
      } else if (dataKey === 'files') {
        for (let index5 = 0; index5 < this.allFiles.length; index5++) {
          formData.append(`files[${index5}]`, this.allFiles[index5]);
        }
      } else if (dataKey === 'date') {
        const convertedDate = moment(data[dataKey]).startOf('day');
        const date = moment(convertedDate).format('YYYY-MM-DD');
        formData.append(dataKey, date);
      } else if (dataKey === 'vesselId') {
        const veseels = this.vesselArray;
        log.debug(veseels);
        for (let index = 0; index < veseels.length; index++) {
          log.debug(veseels);
          formData.append(`vesselId[${index}]`, veseels[index]);
        }
      } else if (dataKey === 'vesselTypeId') {
        const vesselTypeId = this.actionForm.value.vesselTypeId;
        log.debug(vesselTypeId);
        for (let index = 0; index < vesselTypeId.length; index++) {
          log.debug(vesselTypeId);
          formData.append(`vesselTypeId[${index}]`, vesselTypeId[index]);
        }
      } else if (dataKey === 'email') {
        const emails = this.actionForm.value.email;
        const email = emails.split(';');
        for (let index = 0; index < email.length; index++) {
          formData.append(`email[${index}]`, email[index]);
        }
      } else if (dataKey === 'send') {
        const keyValue = data[dataKey];
        if (keyValue === true) {
          const newkeyValues = '1';
          formData.append(`send`, newkeyValues);
        } else {
          const newkeyValues = '0';
          formData.append(`send`, newkeyValues);
        }
      } else if (dataKey === 'isActive') {
        const keyValue = data[dataKey];
        if (keyValue === true) {
          const newkeyValues = '1';
          formData.append(`isActive`, newkeyValues);
        } else {
          const newkeyValues = '0';
          formData.append(`isActive`, newkeyValues);
        }
      } else {
        formData.append(dataKey, data[dataKey]);
      }
    }

    this.fleetNotificationService.updateFleetNotification(formData, this.fnuid).subscribe(
      (response) => {
        log.debug('response: ', response);
        this.fleetNotificationData = response;
        if (this.fleetNotificationData.statusCode === 201) {
          const message = 'New Fleet Notification updated successfully!';
          this.notifyService.showSuccess(message, this.title);
          this.router.navigateByUrl('/fleetNotification');
        } else {
          const message = this.fleetNotificationData.message;
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
        // this._snackBar.open(message, '', {
        //   duration: 4000
        // });
      }
    );
  }

  private createFleetOverviewForm() {
    this.fleetOverviewFormGroup = this.formBuilder.group({
      notificationId: ['', Validators.required],
      date: ['', Validators.required],
      vesselTypeId: ['', Validators.required],
      incidentId: ['', Validators.required],
      notificationTitle: ['', Validators.required],
      issuedByNameId: ['', Validators.required],
    });
  }

  private createFleetDescriptionForm() {
    this.descriptionFormGroup = this.formBuilder.group({
      description: [''],
    });
  }

  private createAttachmentFormGroup() {
    this.attachmentFormGroup = this.formBuilder.group({
      files: [''],
    });
  }

  private createFleetVesselForm() {
    this.vesselFormGroup = this.formBuilder.group({
      // vesselType: ['', Validators.required],
      vesselId: ['', Validators.required],
      email: [''],
    });
  }

  private createAction() {
    this.actionForm = this.formBuilder.group({
      notificationId: [''],
      date: [''],
      vesselTypeId: [''],
      notificationTitle: [''],
      incidentId: [''],
      issuedByNameId: [''],
      description: [''],
      vesselId: [''],
      email: [''],
      action: new FormArray([this.actionFields()]),
      send: [''],
      isActive: [''],
      files: [''],
    });
  }
}
