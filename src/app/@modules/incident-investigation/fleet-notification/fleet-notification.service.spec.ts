import { TestBed } from '@angular/core/testing';

import { FleetNotificationService } from './fleet-notification.service';

describe('FleetNotificationService', () => {
  let service: FleetNotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FleetNotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
