import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';

// const routes = {
//   quote: `/fleet/notification`,
// };

const routes = {
  fleetNotification: {
    get: (fnuid: string) => `/fleet/notification/${fnuid}`,
    getAll: () => `/fleet/notification`,
    create: () => `/fleet/notification`,
    update: (fnuid: string) => `/fleet/notification/${fnuid}`,
    delete: (fnauid: string) => `/fleet/notification/${fnauid}`,
    deleteAttachment: (fnauid: string) => `/fleet/notification/attachment/${fnauid}`,
  },

  fleetVessel: {
    getFleetVessels: (fnuid: string) => `/fleet/notification/fleetvessel/${fnuid}`,
    getFleetAckById: (avuid: string) => `/acknowledgement/${avuid}`,
    create: () => `/acknowledgement`,
    update: (avuid: string) => `/acknowledgement/${avuid}`,
  },

  fleetNotificationAttachments: {
    delete: (fnauid: string) => `/fleet/notification/attachment/${fnauid}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class FleetNotificationService {
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /**
   * get All Fleet Notification
   */
  getFleetNotification(): Observable<any> {
    return this.httpDispatcher.get<any>(routes.fleetNotification.getAll());
  }

  /**
   * get  All Fleet Notification By Id
   */
  getFleetNotificationById(fnuid: any): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.fleetNotification.get(fnuid));
  }

  /**
   * Submit Near Miss Data
   */
  createFleetNotification(formData: any): Observable<Response> {
    return this.httpDispatcher.post(routes.fleetNotification.create(), formData);
  }

  /**
   * Update Fleet Notification Data
   */
  updateFleetNotification(formData: any, fnuid: any): Observable<Response> {
    return this.httpDispatcher.put(routes.fleetNotification.update(fnuid), formData);
  }

  /**
   * delete All Fleet Notification
   */

  deleteFleetNotification(fnauid: string): Observable<any> {
    return this.httpDispatcher.delete<any>(routes.fleetNotification.delete(fnauid));
  }

  /**
   * delete All Fleet Notification attachments
   */
  deleteFleetNotificationImage(fnauid: string): Observable<any> {
    return this.httpDispatcher.delete(routes.fleetNotificationAttachments.delete(fnauid));
  }

  getAllTypes(): Observable<any> {
    return this.httpDispatcher.get('/form-feeder/list');
  }

  /**
   * get  All Fleet Vessel  Notification
   */
  getFleetVesselsById(fnuid: any): Observable<any> {
    return this.httpDispatcher.get(routes.fleetVessel.getFleetVessels(fnuid));
  }

  /**
   * Submit Near Miss Data
   */
  createAcknowledgementForAVId(formData: any): Observable<any> {
    return this.httpDispatcher.post(routes.fleetVessel.create(), formData);
    // return this.httpClient.post('/fleet/notification/acknowledgement', formData).pipe(
    //   tap((responseData: any) => {
    //     const statusCode = 'statusCode';
    //     const dataParam = 'data';
    //     if (responseData[statusCode] === 200) {
    //       return responseData[dataParam];
    //     }
    //   }),
    //   catchError((err) => {
    //     return of(err);
    //   })
    // );
  }

  /**
   *  Update Fleet Acknowledgement Data
   */
  updateFleetAcknowledgementForAVId(formData: any, ackid: any): Observable<any> {
    return this.httpDispatcher.put(routes.fleetVessel.update(ackid), formData);

    // const API_URL = `/fleet/notification/acknowledgement/${akuid}`;
    // return this.httpClient.put(API_URL, formData).pipe(
    //   tap((responseData: any) => {
    //     const statusCode = 'statusCode';
    //     const dataParam = 'data';
    //     if (responseData[statusCode] === 200) {
    //       return responseData[dataParam];
    //     }
    //   }),
    //   catchError((err) => {
    //     return of(err);
    //   })
    // );
  }

  getFleetAcknowledgementByForAVId(ackid: any): Observable<any> {
    return this.httpDispatcher.get(routes.fleetVessel.getFleetAckById(ackid));
  }
}
