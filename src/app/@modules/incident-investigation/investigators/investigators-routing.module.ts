import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InvestigatorsComponent } from './investigators.component';
import { extract } from '@app/i18n';
import { InvestigatorsDetailsComponent, InvestigatorsFormComponent, InvestigatorsTableComponent } from './components';

const investigatorsRoutes: Routes = [
  {
    path: '',
    component: InvestigatorsComponent,
    data: { title: extract('Investigators') },
    children: [
      {
        path: '',
        component: InvestigatorsTableComponent,
        data: { title: extract('Investigators') },
      },
      {
        path: 'create',
        component: InvestigatorsFormComponent,
        data: { title: extract('New Investigator') },
      },
      {
        path: ':id/edit',
        component: InvestigatorsFormComponent,
        data: { title: extract('Update Investigator') },
      },
      {
        path: ':id/details',
        component: InvestigatorsDetailsComponent,
        data: { title: extract('Investigators') },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(investigatorsRoutes)],
  exports: [RouterModule],
})
export class InvestigatorsRoutingModule {}
