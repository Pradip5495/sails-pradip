import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '@shared/common-service';
import { InvestigatorsService } from '@shared/service/investigators.service';
import { environment } from '@env/environment';

@Component({
  selector: 'app-investigators-details',
  templateUrl: './investigators-details.component.html',
  styleUrls: ['./investigators-details.component.scss'],
})
export class InvestigatorsDetailsComponent implements OnInit, OnDestroy {
  public title: 'Investigator';
  public investigatorsDetailsData: any = [];
  public API_URL = environment.filePath;
  error: string | undefined;

  constructor(
    private investigatorsService: InvestigatorsService,
    private notifyService: NotificationService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const paramValue = 'value';
    if (this.route.params[paramValue].id) {
      this.getInvestigatorsById(this.route.params[paramValue].id);
    }
  }

  getInvestigatorsById(investigatorsId: any) {
    this.investigatorsService.getInvestigatorsById(investigatorsId).subscribe(
      (response) => {
        this.investigatorsDetailsData = response;
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getFilterFile(receivedImageData: any) {
    const imageTypeAssetMapping = {
      png: this.API_URL + '/' + receivedImageData.path,
      jpg: this.API_URL + '/' + receivedImageData.path,
      jpeg: this.API_URL + '/' + receivedImageData.path,
      zip: '/assets/zip.png',
      ' ': '/assets/zip.png',
      pdf: '/assets/pdf.png',
    };
    return imageTypeAssetMapping[receivedImageData.fileType] || 'assets/file.png';
  }

  /**
   * return image class based on file type
   */
  getImageStyleClass(imageType: any) {
    const imageTypeStyleMapping = {
      png: 'upload-images',
      jpg: 'upload-images',
      jpeg: 'upload-images',
    };

    return imageTypeStyleMapping[imageType] || 'pdfimages';
  }

  ngOnDestroy() {}
}
