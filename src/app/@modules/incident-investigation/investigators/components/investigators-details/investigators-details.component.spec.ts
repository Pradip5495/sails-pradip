import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InvestigatorsDetailsComponent } from './investigators-details.component';

describe('IncidentDetailsComponent', () => {
  let component: InvestigatorsDetailsComponent;
  let fixture: ComponentFixture<InvestigatorsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvestigatorsDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestigatorsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
