import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestigatorsFormComponent } from './investigators-form.component';

describe('InvestigatorsFormComponent', () => {
  let component: InvestigatorsFormComponent;
  let fixture: ComponentFixture<InvestigatorsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvestigatorsFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestigatorsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
