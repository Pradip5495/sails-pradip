import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { InvestigatorsService } from '@shared/service/investigators.service';
import * as moment from 'moment'; // FIXME
import { Logger, untilDestroyed } from '@core';

import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '@shared/common-service/notification.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ICustomFormlyConfig } from '@types';
import { FormlyFieldConfig, FormlyFormOptions } from '@formly';

const log = new Logger('Investigators-Form');

@Component({
  selector: 'app-investigators-new-form',
  templateUrl: './investigators-form.component.html',
  styleUrls: ['./investigators-form.component.scss'],
})
export class InvestigatorsFormComponent implements OnInit, OnDestroy {
  dateChange: EventEmitter<MatDatepickerInputEvent<any>>;
  investigatorsForm: FormGroup = new FormGroup({});

  investigatorsPayload: any = {};
  overViewFormGroup: FormGroup;
  images: any = [];
  allFiles: any = [];
  uploadedFiles: File[] = [];
  error: string | undefined;
  public title = 'Investigators';

  positionOption: object[] = [
    { value: 'Team Lead', label: 'Team Lead' },
    { value: 'Team Member', label: 'Team Member' },
    { value: 'Marine Manager', label: 'Marine Manager' },
    { value: 'Marine Superintendent', label: 'Marine Superintendent' },
    { value: 'Lead Investigator', label: 'Lead Investigator' },
  ];

  typeOption: object[] = [
    { value: 'Internal', label: 'Internal' },
    { value: 'External', label: 'External' },
  ];

  overviewFormKeys = {
    name: 'name',
    department: 'department',
    dateOfTraining: 'dateOfTraining',
    dateOfExpiry: 'dateOfExpiry',
    typeOfTraining: 'typeOfTraining',
    position: 'position',
    type: 'type',
    rank: 'rank',
  };
  overviewForm = new FormGroup({});
  overviewOptions: FormlyFormOptions = {
    formState: {
      name: '',
      department: '',
      dateOfTraining: '',
      dateOfExpiry: '',
      typeOfTraining: '',
      position: '',
      type: '',
      rank: '',
    },
  };
  overviewModel = {};
  overviewFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.overviewFormKeys.name,
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Name',
            placeholder: 'Name',
            required: true,
          },
        },
        {
          key: this.overviewFormKeys.department,
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Department',
            placeholder: 'Department',
            required: true,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.overviewFormKeys.dateOfTraining,
          type: 'datepicker',
          className: 'flex-2',
          templateOptions: {
            appearance: 'outline',
            label: 'Date of Training',
            placeholder: 'Date of Training',
            required: false,
          },
        },
        {
          key: this.overviewFormKeys.dateOfExpiry,
          type: 'datepicker',
          className: 'flex-2',
          templateOptions: {
            appearance: 'outline',
            label: 'Date of Expiry',
            placeholder: 'Date of Expiry',
            required: false,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.overviewFormKeys.typeOfTraining,
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Type of Training',
            placeholder: 'Type of Training',
            required: false,
          },
        },
        {
          key: this.overviewFormKeys.position,
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            appearance: 'outline',
            label: 'Position',
            options: this.positionOption,
            required: true,
            floatLabel: 'never',
            valueProp: 'value',
            labelProp: 'label',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.overviewFormKeys.type,
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            appearance: 'outline',
            label: 'Type',
            options: this.typeOption,
            required: true,
            floatLabel: 'never',
            valueProp: 'value',
            labelProp: 'label',
          },
        },
        {
          key: this.overviewFormKeys.rank,
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Rank',
            placeholder: 'Rank',
          },
        },
      ],
    },
  ];
  attachmentFileKey = 'files';
  attachmentForm = new FormGroup({});
  attachmentModel = {};
  attachmentOptions: FormlyFormOptions = {
    formState: {
      images: [],
    },
  };
  attachmentFields: FormlyFieldConfig[] = [
    // {
    //   type: 'image-template',
    //   templateOptions: {
    //     component: FormlyImagePreviewComponent,
    //     props: {
    //       key: 'date',
    //       label: 'Date',
    //       delete: this.deleteThirdPartySharingAttachemnt.bind(this),
    //     },
    //   },
    // },
    {
      key: this.attachmentFileKey,
      type: 'sailfile',
      templateOptions: {
        preview: true,
        id: 'fleet-file',
        onFileChange: this.onFormlyAttachmentSubmit.bind(this),
      },
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      id: 'overview',
      shortName: 'Overview',
      title: 'Overview',
      subTitle: 'ENTER OVERVIEW DATA:',
      config: {
        model: this.overviewModel,
        fields: this.overviewFields,
        options: this.overviewOptions,
        form: this.overviewForm,
      },
      onSave: this.onFormlyOverviewSubmit.bind(this),
      discard: true,
      onDiscard: () => this.overviewForm.reset(),
    },
    {
      shortName: 'Attachment',
      title: 'Attachment',
      subTitle: 'Add Attachments',
      config: {
        model: this.attachmentModel,
        fields: this.attachmentFields,
        options: this.attachmentOptions,
        form: this.attachmentForm,
      },
      onSave: this.onFinalSubmit.bind(this),
      discard: true,
      onDiscard: () => this.attachmentForm.reset(),
    },
  ];

  imageUrlsArray: Array<string> = [];
  isEditMode: boolean;
  private investigatorsId: string;

  constructor(
    private investigatorsService: InvestigatorsService,
    private router: Router,
    private notifyService: NotificationService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    const value = 'value';
    // Logic for Edit Mode
    this.isEditMode = !!this.activatedRoute.params[value].id;
    this.investigatorsId = this.isEditMode ? this.activatedRoute.params[value].id : null;
    if (this.isEditMode) {
      this.getSelectedInvestigatorsData();
    }
  }

  //#region for Edit Functionality Start

  async getSelectedInvestigatorsData() {
    const investigatorsDetails: any = await this.investigatorsService
      .getInvestigatorsById(this.investigatorsId)
      .pipe(untilDestroyed(this))
      .toPromise();

    // Mapping the data for Dates
    if (investigatorsDetails.length > 0) {
      investigatorsDetails.map((obj: any) => {
        obj.dateShared = new Date(obj.dateShared);
        obj.dueDate = new Date(obj.dueDate);
        if (obj.files) {
          obj.files.map((file: any, index: number) => {
            const splittedFileName = file.path.split('.');
            obj.files[index].fileType = splittedFileName[splittedFileName.length - 1];
          });
        }
        return obj;
      });

      // Initializing the FormData
      this.initializeOverviewFormData(investigatorsDetails[0]);
      this.displayImagePreviewsIfAttached(investigatorsDetails[0]);
    }
  }

  initializeOverviewFormData(overviewData: any) {
    if (overviewData) {
      this.overviewForm.patchValue(overviewData);
    }
    // Initializing the static Keys
    this.overviewOptions.formState = {
      name: overviewData.name,
      department: overviewData.department,
      dateOfTraining: overviewData.dateOfTraining,
      dateOfExpiry: overviewData.dateOfExpiry,
      typeOfTraining: overviewData.typeOfTraining,
      position: overviewData.position,
      type: overviewData.type,
    };
  }

  displayImagePreviewsIfAttached(investigatorsData: any) {
    if (investigatorsData.attachments) {
      this.imageUrlsArray = investigatorsData.attachments;
      this.imageUrlsArray.map((images) => {
        // return images = env.filePath + '/' + 'sails-attachments/initbit/ab03a79a031ddec410602a64778911136.png';
      });
      this.attachmentOptions.formState.images = investigatorsData.attachments;
    }
  }

  onFormlyAttachmentSubmit(files: any) {
    /* Adding attachments to Payload */
    this.uploadedFiles = files ? files : this.uploadedFiles;

    if (this.uploadedFiles.length > 0) {
      this.investigatorsPayload = { ...this.investigatorsPayload, ...{ files: this.uploadedFiles } };
    } else {
      if (this.investigatorsPayload.hasOwnProperty(this.attachmentFileKey)) {
        delete this.investigatorsPayload[this.attachmentFileKey];
      }
    }
  }

  onFormlyOverviewSubmit(overviewFormData: any) {
    this.investigatorsPayload = { ...this.investigatorsPayload, ...overviewFormData };
  }

  onFinalSubmit(formData: any) {
    // RePreparing the Overview Form Data
    this.onFormlyOverviewSubmit(this.overviewForm.value);

    /* Checking all form Validations before Performing API Call */
    if (this.overviewForm.valid && formData) {
      if (this.isEditMode) {
        this.updateInvestigatorsWithFormly(this.investigatorsPayload);
      } else {
        this.createInvestigatorsWithFormly(this.investigatorsPayload);
      }
    } else {
      this.notifyService.showWarning('Some of Mandatory fields are missing', this.title);
    }
  }

  createInvestigatorsWithFormly(payLoad: any) {
    const convertedDate = moment(payLoad.dateOfTraining).startOf('day');
    payLoad.dateOfTraining = moment(convertedDate).format('YYYY-MM-DD');

    const convertedDateOfExpiry = moment(payLoad.dateOfExpiry).startOf('day');
    payLoad.dateOfExpiry = moment(convertedDateOfExpiry).format('YYYY-MM-DD');

    const formData = new FormData();
    for (const key in payLoad) {
      if (key === 'files') {
        for (const file of payLoad[key]) {
          formData.append(key, file);
        }
      } else {
        formData.append(key, payLoad[key]);
      }
    }
    this.investigatorsService
      .createInvestigators(formData)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response: any) => {
          if (response.statusCode === 201) {
            const message = response.message || 'New Investigator created successfully!';
            this.notifyService.showSuccess(message, this.title);
            this.navigateBack();
          } else {
            const message = response.message;
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          const message = error;
          this.notifyService.showError(message, this.title);
          // this._snackBar.open(message, '', {
          //   duration: 4000
          // });
        }
      );
  }

  updateInvestigatorsWithFormly(payLoad: any) {
    const convertedDate = moment(payLoad.dateOfTraining).startOf('day');
    payLoad.dateOfTraining = moment(convertedDate).format('YYYY-MM-DD');

    const convertedDateOfExpiry = moment(payLoad.dateOfExpiry).startOf('day');
    payLoad.dateOfExpiry = moment(convertedDateOfExpiry).format('YYYY-MM-DD');
    const formData = new FormData();
    for (const key in payLoad) {
      if (key === 'files') {
        for (const file of payLoad[key]) {
          formData.append(key, file);
        }
      } else {
        formData.append(key, payLoad[key]);
      }
    }
    formData.set('isActive', '1');
    this.investigatorsService
      .updateInvestigators(this.investigatorsId, formData)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response: any) => {
          if (response.statusCode === 200) {
            const message = response.message || 'investigator updated successfully!';
            this.notifyService.showSuccess(message, this.title);
            this.navigateBack();
          } else {
            const message = response.message;
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          const message = error;
          this.notifyService.showError(message, this.title);
          // this._snackBar.open(message, '', {
          //   duration: 4000
          // });
        }
      );
  }

  onFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      log.debug('reached Again');
      const filesAmount = event.target.files.length;
      log.debug('filesAmount:', filesAmount);
      for (let i = 0; i < filesAmount; i++) {
        const image = {
          name: '',
          type: '',
          size: '',
          url: '',
        };

        log.debug('urls is:', this.images);
        this.allFiles.push(event.target.files[i] as File);
        image.name = event.target.files[i].name;
        image.type = event.target.files[i].type;
        image.size = event.target.files[i].size;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.images.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
        /*
      this.attachmentFormGroup.patchValue({
        files: this.images
      });*/
      }
    }
    event.srcElement.value = null;
  }

  deleteImage(image: any) {
    const index = this.images.indexOf(image);
    this.images.splice(index, 1);
    this.allFiles.splice(index, 1);
    this.onFileChange(this.images);
  }

  deleteThirdPartySharingAttachemnt(imageData: any) {
    if (imageData) {
      this.investigatorsService
        .deleteAttachmentInvestigators(imageData.thirdPartyAttachmentId)
        .pipe(untilDestroyed(this))
        .subscribe((response: any) => {
          if (response.statusCode === 200) {
            // Filtering the Deleted Image
            const images = this.attachmentOptions.formState.images;
            const filteredImages = images.filter((image: any) => {
              return image.thirdPartyAttachmentId !== imageData.thirdPartyAttachmentId;
            });
            this.attachmentOptions.formState.images = filteredImages;

            this.notifyService.showSuccess(response.message, this.title);
          } else {
            this.notifyService.showSuccess(response.message, this.title);
          }
        });
    }
  }

  overviewReset() {
    this.investigatorsForm.reset();
  }

  navigateBack() {
    const navigateBackUrl = this.isEditMode ? '../../' : '../';
    this.router
      .navigate([navigateBackUrl], {
        relativeTo: this.activatedRoute,
      })
      .catch((e) => {
        log.error(e);
      });
  }

  onEditSubmit(iuid: any) {
    const formData = new FormData();
    const convertedDate = moment(this.investigatorsForm.value.dateOfTraining).startOf('day');
    const dateOfTraining = moment(convertedDate).format('YYYY-MM-DD');

    const convertedDateOfExpiry = moment(this.investigatorsForm.value.dateOfTraining).startOf('day');
    const dateOfExpiry = moment(convertedDateOfExpiry).format('YYYY-MM-DD');

    formData.append('name', this.investigatorsForm.value.name);
    formData.append('department', this.investigatorsForm.value.department);
    formData.append('dateOfTraining', dateOfTraining);
    formData.append('dateOfExpiry', dateOfExpiry);
    formData.append('typeOfTraining', this.investigatorsForm.value.typeOfTraining);
    formData.append('position', this.investigatorsForm.value.position);
    formData.append('type', this.investigatorsForm.value.type);
    for (let index5 = 0; index5 < this.investigatorsForm.value.files.length; index5++) {
      formData.append(`files[${index5}]`, this.investigatorsForm.value.files[index5]);
    }
    this.investigatorsService.updateInvestigators(iuid, formData).subscribe((response) => {});
  }

  ngOnDestroy() {}
}
