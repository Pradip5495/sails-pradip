import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InvestigatorsService } from '@shared/service/investigators.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Logger } from '@core';
import { MatTableDataSource } from '@angular/material/table';
// tslint:disable-next-line:max-line-length
import { NotificationService } from '@shared/common-service/notification.service';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { VesselSidebarComponent } from '@shared/component/vessel-sidebar/vessel-sidebar.component';
import { DatePipe } from '@angular/common';

const log = new Logger('Investigators');

@Component({
  selector: 'app-investigators-table',
  templateUrl: './investigators-table.component.html',
  styleUrls: ['./investigators-table.component.scss'],
})
export class InvestigatorsTableComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  displayedColumns: string[] = [
    'name',
    'position',
    'internal/external',
    'department',
    'typeOfTraining',
    'dateOfTraining',
    'dateOfExpiry',
    'actions',
  ];
  listOfInvestigatorsData: any = [];
  dataSource: any;
  isLoading = false;
  isdataSource = false;
  public title = 'Investigators';
  error: string | undefined;

  public inid: any;
  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  public array: any;
  public rowData: any;
  public icons: any;
  paginationPageSize: number;
  public sideBar: any;
  public id: any;
  selectedInvestigatorsID: any;
  selectedInvestigatorsName: string;

  public deleteData: any;

  editType = '';

  columnDefs = [
    {
      headerName: 'NAME',
      field: 'name',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'POSITION',
      field: 'position',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'INTERNAL/EXTERNAL',
      field: 'type',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'DEPARTMENT',
      field: 'department',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'TYPE OF TRAINING',
      field: 'typeOfTraining',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'DATE OF TRAINING',
      field: 'dateOfTraining',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'DATE OF EXPIRY',
      field: 'dateOfExpiry',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'ACTIONS',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deleteInvestigators.bind(this),
        onClickEdit: this.editItems.bind(this),
        onClickDetails: this.getInvestigatorsDetails.bind(this),
        // hideDetailsButton: true,
      },
      filter: 'agTextColumnFilter',
    },
  ];
  headerHeight: 100;
  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    customStatsToolPanel: VesselSidebarComponent,
  };
  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };

  constructor(
    private investigatorsService: InvestigatorsService,
    private router: Router,
    private notifyService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private dateFormatter: DatePipe
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.paginationPageSize = 5;
    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };
    this.getListOfInvestigators();
    this.sideBar = {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'columns',
          iconKey: 'columns',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
          },
        },
        {
          id: 'customStats',
          labelDefault: 'Vessels',
          labelKey: 'customStats',
          iconKey: 'custom-stats',
          filter: true,
          toolPanel: 'customStatsToolPanel',
        },
      ],
      filter: true,
      hiddenByDefault: true,
      position: 'right',
    };
  }

  onGridReady(params: any) {
    log.debug('reached');
    this.investigatorsService.getListOfInvestigators().subscribe(
      (response) => {
        log.debug('Users Data investigator', response);
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
        this.rowData.forEach((element: any) => {
          element.dateOfExpiry = this.dateFormatter.transform(new Date(element.dateOfExpiry), 'dd-MMM-yyyy');
          element.dateOfTraining = this.dateFormatter.transform(new Date(element.dateOfTraining), 'dd-MMM-yyyy');
        });
      },
      (error) => {
        this.notifyService.showError(error);
      }
    );
  }

  onAddForm() {
    this.router
      .navigate(['create'], {
        relativeTo: this.activatedRoute,
      })
      .catch((e) => {
        log.error(e);
      });
  }

  editItems(params: any) {
    if (params) {
      this.id = params.rowData.investigatorsId;
      this.router
        .navigate([this.id, 'edit'], {
          relativeTo: this.activatedRoute,
        })
        .catch((e) => {
          log.error(e);
        });
    }
  }
  // TODO: Cleanup duplicate code. Make generic Navigation Service
  getInvestigatorsDetails(params: any) {
    if (params) {
      this.id = params.rowData.investigatorsId;
      this.router
        .navigate([this.id, 'details'], {
          relativeTo: this.activatedRoute,
        })
        .catch((e) => {
          log.error(e);
        });
    }
  }

  getListOfInvestigators() {
    this.investigatorsService.getListOfInvestigators().subscribe(
      (response) => {
        log.debug(`${response}`);
        this.listOfInvestigatorsData = response;
        this.listOfInvestigatorsData.forEach((element: any) => {
          element.dateOfExpiry = this.dateFormatter.transform(new Date(element.dateOfExpiry), 'dd-MMM-yyyy');
          element.dateOfTraining = this.dateFormatter.transform(new Date(element.dateOfTraining), 'dd-MMM-yyyy');
        });
        if (!response) {
          this.isdataSource = true;
          const message = 'No Record Found!';
          this.notifyService.showError(message, this.title);
        }
        this.dataSource = new MatTableDataSource(this.listOfInvestigatorsData);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.array = response;
        this.totalSize = this.array.length;
        this.iterator();
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  public handlePage(e: PageEvent) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  deleteInvestigators(params: any) {
    this.investigatorsService.deleteInvestigators(params.rowData.investigatorsId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record No Deteted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error: any) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  fetchData() {
    this.investigatorsService.getListOfInvestigators().subscribe((data) => {
      this.rowData = data;
    });
  }

  ngOnDestroy() {}

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.dataSource = part;
  }
}
