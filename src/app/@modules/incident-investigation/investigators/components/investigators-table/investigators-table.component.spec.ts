import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestigatorsTableComponent } from './investigators-table.component';

describe('InvestigatorsTableComponent', () => {
  let component: InvestigatorsTableComponent;
  let fixture: ComponentFixture<InvestigatorsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvestigatorsTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestigatorsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
