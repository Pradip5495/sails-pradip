import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-investigators',
  templateUrl: './investigators.component.html',
  styleUrls: ['./investigators.component.scss'],
})
export class InvestigatorsComponent implements OnInit {
  isLoading = false;

  ngOnInit() {
    this.isLoading = true;
  }
}
