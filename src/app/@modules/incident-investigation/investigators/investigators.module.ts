import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '@shared';
import { MaterialModule } from '@app/material.module';
import { InvestigatorsComponent } from './investigators.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
  InvestigatorsDetailsComponent,
  InvestigatorsFormComponent,
  InvestigatorsTableComponent,
} from '@modules/incident-investigation/investigators/components';
import { FormlyModule } from '@formly';
import { InvestigatorsRoutingModule } from './investigators-routing.module';

@NgModule({
  declarations: [
    InvestigatorsComponent,
    InvestigatorsTableComponent,
    InvestigatorsFormComponent,
    InvestigatorsDetailsComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule,
    FormlyModule,
    InvestigatorsRoutingModule,
  ],
})
export class InvestigatorsModule {}
