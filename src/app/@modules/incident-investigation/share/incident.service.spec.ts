import { TestBed } from '@angular/core/testing';

import { IncidentInvestigationService } from './incidentInvestigation.service';

describe('IncidentInvestigationService', () => {
  let service: IncidentInvestigationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IncidentInvestigationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
