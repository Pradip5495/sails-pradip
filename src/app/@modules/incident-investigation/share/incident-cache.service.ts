import { Injectable } from '@angular/core';

const overviewFormKey = 'overviewForm';
const descriptionFormKey = 'descriptionForm';
const invitationFormKey = 'investigation';
const contactEstablishFormKey = 'contactEstablishForm';
const analysisFormKey = 'incidentAnalysisForm';
const crossReferenceFormKey = 'crossReferenceForm';
const actionFormKey = 'actionForm';
const incidentSeqDataFormKey = 'incidentSeqDataForm';
const incidentVdrDataFormKey = 'incidentVdrDataForm';
const notifyDataFormKey = 'notifyDataForm';
const postRecDataFormKey = 'postRecDataForm';
const breachDataFormKey = 'breachDataForm';
const backgroundDataFormKey = 'backgroundDataForm';

const crossReferencerelationKey = 'crossReferencerelation';
const elementKey = 'element';
const stageKey = 'stage';

@Injectable({
  providedIn: 'root',
})
export class IncidentCacheService {
  overviewFormStorage: any;
  descriptionFormStorage: any;
  investigationFormStorage: any;
  contactEstablishFormStorage: any;
  analysisFormStorage: any;
  crossReferenceFormStorage: any;
  actionFormStorage: any;
  incidentSeqDataFormStorage: any;
  incidentVdrDataFormStorage: any;
  notifyDataFormStorage: any;
  postRecDataFormStorage: any;
  backgroundDataFormStorage: any;
  breachDataFormStorage: any;
  crossReferenceRelationStorage: any;
  elementStorage: any;
  stageStorage: any;

  constructor() {
    const savedOverviewForm = sessionStorage.getItem(overviewFormKey) || localStorage.getItem(overviewFormKey);
    if (savedOverviewForm) {
      this.overviewFormStorage = JSON.parse(savedOverviewForm);
    }

    const savedDescriptionForm = sessionStorage.getItem(descriptionFormKey) || localStorage.getItem(descriptionFormKey);
    if (savedDescriptionForm) {
      this.descriptionFormStorage = JSON.parse(savedDescriptionForm);
    }

    const savedInvestigationForm = sessionStorage.getItem(invitationFormKey) || localStorage.getItem(invitationFormKey);
    if (savedInvestigationForm) {
      this.investigationFormStorage = JSON.parse(savedInvestigationForm);
    }

    const savedContactEstablishForm =
      sessionStorage.getItem(contactEstablishFormKey) || localStorage.getItem(contactEstablishFormKey);
    if (savedContactEstablishForm) {
      this.contactEstablishFormStorage = JSON.parse(savedContactEstablishForm);
    }

    const savedCrossReferenceForm = sessionStorage.getItem(analysisFormKey) || localStorage.getItem(analysisFormKey);
    if (savedCrossReferenceForm) {
      this.analysisFormStorage = JSON.parse(savedCrossReferenceForm);
    }

    const savedAnalysisForm =
      sessionStorage.getItem(crossReferenceFormKey) || localStorage.getItem(crossReferenceFormKey);
    if (savedAnalysisForm) {
      this.crossReferenceFormStorage = JSON.parse(savedAnalysisForm);
    }

    const savedActionForm = sessionStorage.getItem(actionFormKey) || localStorage.getItem(actionFormKey);
    if (savedActionForm) {
      this.actionFormStorage = JSON.parse(savedActionForm);
    }

    const savedincidentSeqData =
      sessionStorage.getItem(incidentSeqDataFormKey) || localStorage.getItem(incidentSeqDataFormKey);
    if (savedincidentSeqData) {
      this.incidentSeqDataFormStorage = JSON.parse(savedincidentSeqData);
    }

    const savedIncidentVdrData =
      sessionStorage.getItem(incidentVdrDataFormKey) || localStorage.getItem(incidentVdrDataFormKey);
    if (savedIncidentVdrData) {
      this.incidentVdrDataFormStorage = JSON.parse(savedIncidentVdrData);
    }

    const savedNotifyData = sessionStorage.getItem(notifyDataFormKey) || localStorage.getItem(notifyDataFormKey);
    if (savedNotifyData) {
      this.notifyDataFormStorage = JSON.parse(savedNotifyData);
    }

    const savedPostRecData = sessionStorage.getItem(postRecDataFormKey) || localStorage.getItem(postRecDataFormKey);
    if (savedPostRecData) {
      this.postRecDataFormStorage = JSON.parse(savedPostRecData);
    }

    const savedBreachData = sessionStorage.getItem(breachDataFormKey) || localStorage.getItem(breachDataFormKey);
    if (savedBreachData) {
      this.breachDataFormStorage = JSON.parse(savedBreachData);
    }

    const savedBackgroundData =
      sessionStorage.getItem(backgroundDataFormKey) || localStorage.getItem(backgroundDataFormKey);
    if (savedBackgroundData) {
      this.backgroundDataFormStorage = JSON.parse(savedBackgroundData);
    }

    const savedCrossReferenceRelationData =
      sessionStorage.getItem(crossReferencerelationKey) || localStorage.getItem(crossReferencerelationKey);
    if (savedCrossReferenceRelationData) {
      this.crossReferenceRelationStorage = JSON.parse(savedCrossReferenceRelationData);
    }

    const savedElementData = sessionStorage.getItem(elementKey) || localStorage.getItem(elementKey);
    if (savedElementData) {
      this.elementStorage = JSON.parse(savedElementData);
    }

    const savedStageData = sessionStorage.getItem(stageKey) || localStorage.getItem(stageKey);
    if (savedStageData) {
      this.stageStorage = JSON.parse(savedStageData);
    }
  }

  /**
   * Get the type of Contact.
   * @return The type of Contact or null if the user is not authenticated.
   */
  get overviewData(): any | null {
    return this.overviewFormStorage;
  }

  get descriptionData(): any | null {
    return this.descriptionFormStorage;
  }

  get investigationData(): any | null {
    return this.investigationFormStorage;
  }

  get contactEstablish(): any | null {
    return this.contactEstablishFormStorage;
  }

  get analysisForm(): any | null {
    return this.analysisFormStorage;
  }

  get crossReference(): any | null {
    return this.crossReferenceFormStorage;
  }

  get action(): any | null {
    return this.actionFormStorage;
  }

  get incidentSeq(): any | null {
    return this.incidentSeqDataFormStorage;
  }

  get incidentVdr(): any | null {
    return this.incidentVdrDataFormStorage;
  }

  get notify(): any | null {
    return this.notifyDataFormStorage;
  }

  get postRec(): any | null {
    return this.postRecDataFormStorage;
  }

  get breachData(): any | null {
    return this.breachDataFormStorage;
  }

  get backgroundData(): any | null {
    return this.backgroundDataFormStorage;
  }

  get crossReferenceRelationData(): any | null {
    return this.crossReferenceRelationStorage;
  }

  get elementData(): any | null {
    return this.elementStorage;
  }

  get stageData(): any | null {
    return this.stageStorage;
  }

  /**
   * Sets the vessel types.
   * The vessel types may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the vessels are only persisted for the current session.
   * @param vesselTypes vessel type list.
   * @param remember True to remember vessel type across sessions.
   */
  setOverviewData(overviewData?: any) {
    this.overviewFormStorage = overviewData || null;
    if (overviewData) {
      const storage = overviewData ? localStorage : sessionStorage;
      storage.setItem(overviewFormKey, JSON.stringify(this.overviewFormStorage));
    } else {
      sessionStorage.removeItem(overviewFormKey);
      localStorage.removeItem(overviewFormKey);
    }
  }

  setDescriptionData(descriptionData?: any) {
    this.descriptionFormStorage = descriptionData || null;
    if (descriptionData) {
      const storage = descriptionData ? localStorage : sessionStorage;
      storage.setItem(descriptionFormKey, JSON.stringify(this.descriptionFormStorage));
    } else {
      sessionStorage.removeItem(descriptionFormKey);
      localStorage.removeItem(descriptionFormKey);
    }
  }

  setInvestigationData(investigationData?: any) {
    this.investigationFormStorage = investigationData || null;
    if (investigationData) {
      const storage = investigationData ? localStorage : sessionStorage;
      storage.setItem(invitationFormKey, JSON.stringify(this.investigationFormStorage));
    } else {
      sessionStorage.removeItem(invitationFormKey);
      localStorage.removeItem(invitationFormKey);
    }
  }

  setContactEstablishData(contactEstablishData?: any) {
    this.contactEstablishFormStorage = contactEstablishData || null;
    if (contactEstablishData) {
      const storage = contactEstablishData ? localStorage : sessionStorage;
      storage.setItem(contactEstablishFormKey, JSON.stringify(this.contactEstablishFormStorage));
    } else {
      sessionStorage.removeItem(contactEstablishFormKey);
      localStorage.removeItem(contactEstablishFormKey);
    }
  }

  setAnalysisData(analysisData?: any) {
    this.analysisFormStorage = analysisData || null;
    if (analysisData) {
      const storage = analysisData ? localStorage : sessionStorage;
      storage.setItem(analysisFormKey, JSON.stringify(this.analysisFormStorage));
    } else {
      sessionStorage.removeItem(analysisFormKey);
      localStorage.removeItem(analysisFormKey);
    }
  }

  setCrossReferenceData(crossReferenceData?: any) {
    this.crossReferenceFormStorage = crossReferenceData || null;
    if (crossReferenceData) {
      const storage = crossReferenceData ? localStorage : sessionStorage;
      storage.setItem(crossReferenceFormKey, JSON.stringify(this.crossReferenceFormStorage));
    } else {
      sessionStorage.removeItem(crossReferenceFormKey);
      localStorage.removeItem(crossReferenceFormKey);
    }
  }

  setActionData(actionData?: any) {
    this.actionFormStorage = actionData || null;
    if (actionData) {
      const storage = actionData ? localStorage : sessionStorage;
      storage.setItem(actionFormKey, JSON.stringify(this.actionFormStorage));
    } else {
      sessionStorage.removeItem(actionFormKey);
      localStorage.removeItem(actionFormKey);
    }
  }

  setIncidentSeqData(incidentSeqData?: any) {
    this.incidentSeqDataFormStorage = incidentSeqData || null;
    if (incidentSeqData) {
      const storage = incidentSeqData ? localStorage : sessionStorage;
      storage.setItem(incidentSeqDataFormKey, JSON.stringify(this.incidentSeqDataFormStorage));
    } else {
      sessionStorage.removeItem(incidentSeqDataFormKey);
      localStorage.removeItem(incidentSeqDataFormKey);
    }
  }

  setIncidentVdrData(incidentVdrData?: any) {
    this.incidentVdrDataFormStorage = incidentVdrData || null;
    if (incidentVdrData) {
      const storage = incidentVdrData ? localStorage : sessionStorage;
      storage.setItem(incidentVdrDataFormKey, JSON.stringify(this.incidentVdrDataFormStorage));
    } else {
      sessionStorage.removeItem(incidentVdrDataFormKey);
      localStorage.removeItem(incidentVdrDataFormKey);
    }
  }

  setNotifyData(notifyData?: any) {
    this.notifyDataFormStorage = notifyData || null;
    if (notifyData) {
      const storage = notifyData ? localStorage : sessionStorage;
      storage.setItem(notifyDataFormKey, JSON.stringify(this.notifyDataFormStorage));
    } else {
      sessionStorage.removeItem(notifyDataFormKey);
      localStorage.removeItem(notifyDataFormKey);
    }
  }

  setPostRecData(postRecData?: any) {
    this.postRecDataFormStorage = postRecData || null;
    if (postRecData) {
      const storage = postRecData ? localStorage : sessionStorage;
      storage.setItem(postRecDataFormKey, JSON.stringify(this.postRecDataFormStorage));
    } else {
      sessionStorage.removeItem(postRecDataFormKey);
      localStorage.removeItem(postRecDataFormKey);
    }
  }

  setBreachData(breachData?: any) {
    this.breachDataFormStorage = breachData || null;
    if (breachData) {
      const storage = breachData ? localStorage : sessionStorage;
      storage.setItem(breachDataFormKey, JSON.stringify(this.breachDataFormStorage));
    } else {
      sessionStorage.removeItem(breachDataFormKey);
      localStorage.removeItem(breachDataFormKey);
    }
  }

  setBackgroundData(backgroundData?: any) {
    this.backgroundDataFormStorage = backgroundData || null;
    if (backgroundData) {
      const storage = backgroundData ? localStorage : sessionStorage;
      storage.setItem(backgroundDataFormKey, JSON.stringify(this.backgroundDataFormStorage));
    } else {
      sessionStorage.removeItem(backgroundDataFormKey);
      localStorage.removeItem(backgroundDataFormKey);
    }
  }

  setCrossReferenceRelationData(crossReferenceData?: any) {
    this.crossReferenceRelationStorage = crossReferenceData || null;
    if (crossReferenceData) {
      const storage = crossReferenceData ? localStorage : sessionStorage;
      storage.setItem(crossReferencerelationKey, JSON.stringify(this.crossReferenceRelationStorage));
    } else {
      sessionStorage.removeItem(crossReferencerelationKey);
      localStorage.removeItem(crossReferencerelationKey);
    }
  }

  setElementData(elementData?: any) {
    this.elementStorage = elementData || null;
    if (elementData) {
      const storage = elementData ? localStorage : sessionStorage;
      storage.setItem(elementKey, JSON.stringify(this.elementStorage));
    } else {
      sessionStorage.removeItem(elementKey);
      localStorage.removeItem(elementKey);
    }
  }

  setStageData(stageData?: any) {
    this.stageStorage = stageData || null;
    if (stageData) {
      const storage = stageData ? localStorage : sessionStorage;
      storage.setItem(stageKey, JSON.stringify(this.stageStorage));
    } else {
      sessionStorage.removeItem(stageKey);
      localStorage.removeItem(stageKey);
    }
  }
}
