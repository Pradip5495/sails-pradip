import { TestBed } from '@angular/core/testing';

import { NearMissCacheService } from './near-miss-cache.service';

describe('NearMissCacheService', () => {
  let service: NearMissCacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NearMissCacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
