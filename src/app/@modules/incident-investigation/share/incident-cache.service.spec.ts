import { TestBed } from '@angular/core/testing';

import { IncidentCacheService } from './incident-cache.service';

describe('IncidentCacheService', () => {
  let service: IncidentCacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IncidentCacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
