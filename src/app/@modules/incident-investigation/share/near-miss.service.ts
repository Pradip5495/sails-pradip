import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';

const routes = {
  nearmiss: {
    getAll: () => `/nearmiss`,
  },

  vessel: {
    getAll: () => `/vessel?myvessel=true`,
  },

  reporttype: {
    get: (id: string) => `/form-feeder?type=${id}`,
    getAll: () => `/form-feeder/list`,
  },

  crossreference: {
    getAll: () => `/crttsma`,
  },

  element: {
    getAll: () => `/crossreferencingtotsmatree/element`,
  },

  stage: {
    getStage: (id: string) => `/crossreferencingtotsmatree/element/${id}`,
  },

  specifickpi: {
    get: (id: string) => `cross-referencing-to-tsma/stage/${id}`,
  },

  formfeederlist: {
    getAll: (id: string) => `/form-feeder/list?listType=${id}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class NearMissService {
  constructor(private httpClient: HttpClient, private readonly httpDispatcher: HttpDispatcherService) {}
  getElements(): Observable<any> {
    return this.httpDispatcher.get<any>(routes.element.getAll());
  }
  getStage(id: string): Observable<any> {
    return this.httpDispatcher.get<any>(routes.stage.getStage(id));
  }
  getFormFedderList(id: string) {
    return this.httpDispatcher.get<any>(routes.formfeederlist.getAll(id));
  }
  getAllVessels(): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.vessel.getAll());
  }

  getFilterNearMiss(type: string): Observable<Response> {
    const API_URL = `/form-feeder/form?module=${type}`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  getDesignation(): Observable<any> {
    const API_URL = `/user/designation`;
    return this.httpDispatcher.get(API_URL);
  }

  /**
   *  Get contact or event
   */
  getContactOrEvent(): Observable<Response> {
    const API_URL = `/cause/typeOfContact`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get contact or event
   */
  getBasicImmediate(): Observable<Response> {
    const API_URL = `/cause/basicimmediatecause`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Root Cause
   */
  getRootCause(): Observable<Response> {
    const API_URL = `/cause/basicrootcause`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Corrective Action Cause
   */
  getCorrectiveCause(): Observable<Response> {
    const API_URL = `/cause/correctiveaction`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Corrective Action Cause
   */
  getCauseTree(): Observable<Response> {
    const API_URL = `/cause/causetree`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Port List
   */
  getPortList<T = Response>(): Observable<T> {
    const API_URL = `/vessel/port`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Task Involved List
   */
  getTaskList<T = Response>(): Observable<T> {
    const API_URL = `/task`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Task Involved List
   */
  getLocationList<T = Response>(): Observable<T> {
    const API_URL = `/location`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Create Near Miss
   */

  createNearMiss(data: any): Observable<Response> {
    return this.httpClient.post('/nearmiss', data).pipe(
      tap((responseData: any) => {
        const statusCode = 'statusCode';
        if (responseData[statusCode] === 201) {
          return responseData;
        } else {
          return responseData;
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }
}
