import { Component, OnInit, Input } from '@angular/core';
import { IJson } from '@types';
import { Logger } from '@core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { IncidentCacheService } from '../incident-cache.service';
import { CacheServiceService, NotificationService } from '@shared/common-service';
import { NearMissService } from '@modules/incident-investigation/near-miss/share/near-miss.service';

const log = new Logger('Incident Form');

@Component({
  selector: 'cause-tree-incident',
  templateUrl: './cause-tree.component.html',
  styleUrls: ['./cause-tree.component.scss'],
})
export class CauseTreeIncidentComponent implements OnInit {
  contactOrEventOptionList: IJson = [];
  contactOrEventOptionActive: IJson = [];
  basicImmidateOptionActive: IJson = [];
  rootCauseOptionActive: IJson = [];
  correctiveCauseOptionActive: IJson = [];
  typeOfContactArray: IJson = [];
  basicCauseArray: IJson = [];
  rootCauseArray: IJson = [];
  filterBasicImmediateCause: IJson = [];
  contactList: IJson = [];
  basicImmidateOptionList: IJson = [];
  filterBasicRootCause: IJson = [];
  rootCauseOptionList: IJson = [];
  filterBasicCorrectiveCause: IJson = [];
  correctiveCauseOptionList: IJson = [];

  analysisData: IJson;

  activeImmediateCause: any;
  ImmediateCauseId: any;
  activeRootCause: any;
  correctiveActionId: any;

  activeContactCause = '';
  title = '';
  error = '';

  formBuilder: FormBuilder = new FormBuilder();
  analysisFormGroup: FormGroup;

  typeContactView = false;
  isRootCause = true;
  isCorrectiveCause = true;

  analysisFormData: FormArray;

  // TODO: review that why this variable is accepting both FormArray and array type of values;
  subDetails: any;

  @Input()
  model?: any;

  @Input()
  formState?: any;

  @Input()
  props?: any;

  get getCauseValidate() {
    if (this.typeOfContactArray.length > 0) {
      return true;
    } else {
      if (this.basicCauseArray.length > 0) {
        return true;
      } else {
        if (this.rootCauseArray.length > 0) {
          return true;
        } else {
          return false;
        }
      }
    }
  }

  constructor(
    private incidentCacheService: IncidentCacheService,
    private cacheServiceService: CacheServiceService,
    private nearMissService: NearMissService,
    private notifyService: NotificationService
  ) {
    this.addAnalysisFormGroup();
  }

  ngOnInit() {
    this.getNearMissContactorEvent();
  }

  getNearMissContactorEvent() {
    if (this.cacheServiceService.typeOfContact) {
      this.contactOrEventOptionList = this.cacheServiceService.typeOfContact;
    } else {
      this.nearMissService.getContactOrEvent().subscribe(
        (response) => {
          this.contactList = response;
          // log.debug(`${JSON.stringify(response)}`);
          if (response) {
            this.contactOrEventOptionList = response;
            this.cacheServiceService.setTypeOfContact(response);
          } else {
            this.contactOrEventOptionList = [];
            const message = 'Contact or Event list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  addAnalysisFormGroup() {
    this.analysisFormGroup = this.formBuilder.group({
      severityLevel: [''],
      typeOfContactOrEvent: new FormArray([this.addContactEventFields()]),
      analysisData: new FormArray([this.addAnalysisData()]),
    });
  }

  addAnalysisData(): FormGroup {
    return this.formBuilder.group({
      questionId: [''],
      numberOfTimesRepeated: [''],
      briefDescription: [''],
    });
  }

  addContactEventFields(): FormGroup {
    return this.formBuilder.group({
      typeOfCntctId: [''],
      contact: [''],
      typeOfContactCategory: [''],
      basicImmediateCause: new FormArray([this.addBasicImmidateCause()]),
    });
  }

  addBasicImmidateCause(): FormGroup {
    return this.formBuilder.group({
      basicImmediateCauseId: [''],
      immediateCauseExplanation: [''],
      furtherExplanation: [''],
      basicRootCause: new FormArray([this.addRootCause()]),
    });
  }

  addRootCause(): FormGroup {
    return this.formBuilder.group({
      basicRootCauseId: [''],
      rootCauseExplanation: [''],
      furtherExplanation: [''],
      correctiveActionId: new FormArray([]),
    });
  }

  getBackTypeofContact() {
    this.typeContactView = !this.typeContactView;
    this.closeCauseTree();
    if (!this.typeContactView) {
      this.contactOrEventOptionActive = [];
      this.basicImmidateOptionActive = [];
      this.rootCauseOptionActive = [];
      this.correctiveCauseOptionActive = [];
      this.typeOfContactArray = [];
      this.basicCauseArray = [];
      this.rootCauseArray = [];
      const form = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
      form.clear();
      setTimeout(() => {
        // this.stepper.next();
      }, 1);
    }
  }

  closeCauseTree() {
    this.props.afterCauseTreeSelected(this.analysisFormGroup.value);
    this.formState.causeTree = false;
  }

  onSubmitAnalysisChart() {
    log.debug('this.analysisFormGroup.value: ', this.analysisFormGroup.value);
    this.incidentCacheService.setAnalysisData(this.analysisFormGroup.value);
    this.analysisData = this.incidentCacheService.analysisForm.typeOfContactOrEvent;

    this.addTypeofContact(10);
    this.closeCauseTree();
  }

  addTypeofContact(index: number) {
    this.typeContactView = !this.typeContactView;
    // TODO: Check need of following code
    // if (!this.typeContactView) {
    //   setTimeout(() => {
    //     this.move(index);
    //     this.activeIndex = index + 1;
    //   }, 1);
    // }
  }

  /**
   * Begin:: Type of Contact
   * Set Active Class to ContactorEvent List
   */
  getContactClass(contactEvent: any) {
    if (this.typeOfContactArray.indexOf(contactEvent) > -1) {
      return 'contact-table-incomplete';
    } else if (this.contactOrEventOptionActive.indexOf(contactEvent) !== -1) {
      return 'contact-table-active';
    }
  }

  getBasicImmediateCause(contactEventValue: any, index: any) {
    const contactEvent = contactEventValue.typeOfContact;
    this.isRootCause = false;
    this.isCorrectiveCause = false;

    if (this.contactOrEventOptionActive.indexOf(contactEvent) !== -1) {
      const contactIndex = this.contactOrEventOptionActive.indexOf(contactEvent);
      this.contactOrEventOptionActive.splice(contactIndex, 1);
      if (this.basicImmidateOptionActive[index]) {
        this.basicImmidateOptionActive[index].splice(contactIndex, 1);
      }

      delete this.contactOrEventOptionActive[contactEvent];
      this.analysisFormData = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
      let typeOfContactIndex = 0;
      this.analysisFormData.value.forEach((element: any) => {
        const contact = element.contact;
        if (contact === contactEvent) {
          this.analysisFormData.removeAt(typeOfContactIndex);
          this.typeOfContactRemove(contact);
        }
        typeOfContactIndex++;
      });
      this.causeAnalysisValidate();
    } else {
      this.setActivateContactCause(contactEventValue.typeOfContact);
      this.contactOrEventOptionActive.push(contactEvent);
      const contactIndex = this.contactOrEventOptionActive.indexOf(contactEvent);
      this.analysisFormData = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
      if (contactIndex >= 1) {
        this.onAddTypeOfContactOrEventField();
      }
      if (contactIndex === 0) {
        this.onAddTypeOfContactOrEventField();
      }
      this.contactOrEventOptionList.forEach((element: any) => {
        const contact = element.typeOfContact;
        if (contact === contactEvent) {
          if (this.analysisFormData.value instanceof Array) {
            this.analysisFormGroup
              .get(`typeOfContactOrEvent.${contactIndex}.typeOfCntctId`)
              .patchValue(element.typeOfContactId);

            this.analysisFormGroup
              .get(`typeOfContactOrEvent.${contactIndex}.contact`)
              .patchValue(element.typeOfContact);

            this.analysisFormGroup
              .get(`typeOfContactOrEvent.${contactIndex}.typeOfContactCategory`)
              .patchValue(element.typeOfContactCategory);

            this.getNearMissImmidateCause(element.typeOfContactId, index, contactIndex);
          } else {
            this.analysisFormData.value.typeOfCntctId = element.typeOfContactId;
            this.analysisFormData.value.contact = element.typeOfContact;
            this.getNearMissImmidateCause(element.typeOfContactId, index, contactIndex);
          }
        }
      });
    }
    this.causeAnalysisValidate();
  }

  /**
   * set Contact or event Cause Active
   */
  setActivateContactCause(contactTitle: any) {
    this.activeContactCause = contactTitle;
  }

  onAddTypeOfContactOrEventField() {
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    this.subDetails = this.analysisFormGroup.get(typeOfContactOrEvent) as FormArray;
    this.subDetails.push(this.addContactEventFields());
  }

  causeAnalysisValidate() {
    const datas = this.analysisFormGroup.value.typeOfContactOrEvent;
    let index = 0;
    const contact = 'contact';
    const immediateCauseExplanation = 'immediateCauseExplanation';
    const rootCauseExplanation = 'rootCauseExplanation';
    datas.forEach((element: any) => {
      if (element.typeOfCntctId) {
        for (const previewKey in element) {
          if (element.basicImmediateCause.length > 0) {
            if (previewKey === 'basicImmediateCause') {
              const basicImmediate = datas[index][previewKey];
              if (basicImmediate) {
                let index2 = 0;
                basicImmediate.forEach((elements: any) => {
                  const basicImmediateCauseId = elements.basicImmediateCauseId;
                  if (basicImmediateCauseId) {
                    const contactType = datas[index][contact];
                    const typeIndex = this.typeOfContactArray.indexOf(contactType);
                    if (typeIndex > -1) {
                      this.typeOfContactArray.splice(typeIndex, 1);
                    }
                    for (const basicCause in elements) {
                      if (datas[index][previewKey][index2][basicCause]) {
                        if (basicCause === 'basicRootCause') {
                          const basicRootCause = datas[index][previewKey][index2][basicCause];
                          let index3 = 0;
                          if (basicRootCause.length > 0) {
                            basicRootCause.forEach((element3: any) => {
                              const basicRootCauseId = element3.basicRootCauseId;
                              if (basicRootCauseId) {
                                const causeExplanation = datas[index][previewKey][index2][immediateCauseExplanation];
                                const basicIndex = this.basicCauseArray.indexOf(causeExplanation);
                                if (basicIndex > -1) {
                                  this.basicCauseArray.splice(basicIndex, 1);
                                }
                                if (element3.correctiveActionId.length > 0) {
                                  const explanation = basicRootCause[index3][rootCauseExplanation];
                                  const rootIndex = this.rootCauseArray.indexOf(explanation);
                                  if (rootIndex > -1) {
                                    this.rootCauseArray.splice(rootIndex, 1);
                                  }
                                } else {
                                  const explanation = basicRootCause[index3][rootCauseExplanation];
                                  if (this.rootCauseArray.indexOf(explanation) < 0) {
                                    this.rootCauseArray.push(explanation);
                                  }
                                }
                              } else {
                                const causeExplanation = datas[index][previewKey][index2][immediateCauseExplanation];
                                if (this.basicCauseArray.indexOf(causeExplanation) < 0) {
                                  this.basicCauseArray.push(causeExplanation);
                                }
                              }
                              index3++;
                            });
                          } else {
                            const causeExplanation = datas[index][previewKey][index2][immediateCauseExplanation];
                            if (this.basicCauseArray.indexOf(causeExplanation) < 0) {
                              this.basicCauseArray.push(causeExplanation);
                            }
                          }
                        }
                      }
                    }
                  } else {
                    const contactType = datas[index][contact];
                    if (this.typeOfContactArray.indexOf(contactType) < 0) {
                      this.typeOfContactArray.push(contactType);
                    }
                  }
                  index2++;
                });
              } else {
              }
            }
          } else {
            const contactType = datas[index][contact];
            if (this.typeOfContactArray.indexOf(contactType) < 0) {
              this.typeOfContactArray.push(contactType);
            }
          }
        }
      }
      index++;
    });
  }

  getNearMissImmidateCause(typeOfContactId: any, index: any, contactIndex: any) {
    if (this.cacheServiceService.basicImmediateCause) {
      this.filterImmediateCause(index, contactIndex);
    } else {
      this.nearMissService.getBasicImmediate().subscribe(
        (response) => {
          this.contactList = response;
          // log.debug(`${JSON.stringify(response)}`);
          this.cacheServiceService.setBasicImmediateCause(response);
          this.filterImmediateCause(index, contactIndex);
          if (response != null) {
            this.contactOrEventOptionList[index].immidiateCauses = response;
            this.basicImmidateOptionList[index] = response;
          } else {
            const message = 'basic Immidate list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  filterImmediateCause(typeOfContactIndex: any, contactIndex: any) {
    const typeData = this.cacheServiceService.basicImmediateCause;
    const causeTree = this.cacheServiceService.causeTree;
    this.filterBasicImmediateCause = [];
    causeTree.forEach((element: any) => {
      if (element.typeOfContactId === typeOfContactIndex) {
        const immediateArray = element.immediateCause;
        immediateArray.forEach((element2: any) => {
          const immediateIndex = element2.basicImmediateCauseId - 1;
          if (typeData[immediateIndex]) {
            this.filterBasicImmediateCause.push(typeData[immediateIndex]);
          }
        });
      }
    });
    this.basicImmidateOptionList[typeOfContactIndex] = this.filterBasicImmediateCause;
  }

  typeOfContactRemove(type: any) {
    const typeIndex = this.typeOfContactArray.indexOf(type);
    if (typeIndex > -1) {
      this.typeOfContactArray.splice(typeIndex, 1);
    }
  }

  /**
   * Set Active Class to ImmediateCause List
   */
  getImmidiateClass(basicImmediateCause: any, idx: any) {
    const basicImmediateCauseId = basicImmediateCause.immediateCauseId;
    const immediateCause = basicImmediateCause.immediateCause;
    const immediateId = basicImmediateCauseId + '_' + idx;
    if (this.basicCauseArray.indexOf(immediateCause) > -1) {
      return 'contact-table-incomplete';
    } else if (this.basicImmidateOptionActive[immediateId]) {
      return 'contact-table-active';
    }
  }

  inFormArray(contactIndex: any): FormArray {
    return this.analysisFormGroup.get(`typeOfContactOrEvent.${contactIndex}.basicImmediateCause`) as FormArray;
  }

  immediateCauseRemove(type: any) {
    const typeIndex = this.basicCauseArray.indexOf(type);
    if (typeIndex > -1) {
      this.basicCauseArray.splice(typeIndex, 1);
    }
  }

  /**
   * set Immediate Cause as Active
   */
  setActivateImmediateCauses(basicImmediate: any, index: any) {
    this.activeImmediateCause = basicImmediate + '_' + index;
  }

  addBasicImmediate() {
    return { basicImmediateCauseId: '', immediateCauseExplanation: '', basicRootCause: [this.addRootCauseRow()] };
  }

  addRootCauseRow() {
    return { basicRootCauseId: '', rootCauseExplanation: '', correctiveActionId: Array() };
  }

  onAddBasicImmidateCauseField(index: any, basicImmediateIndex: any) {
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    this.subDetails = this.analysisFormGroup.controls[typeOfContactOrEvent].value[index].basicImmediateCause;
    this.subDetails.push(this.addBasicImmediate());
  }

  getRootCause(basicImmediate: any, contactActiveType: any, basicIndex: any, index: any) {
    const basicImmediateCauseId = basicImmediate.immediateCauseId;
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    this.isRootCause = true;
    this.isCorrectiveCause = false;
    const contactIndex = this.contactOrEventOptionActive.indexOf(contactActiveType.typeOfContact);
    log.debug('getRootCause: ');

    if (this.basicImmidateOptionActive[basicImmediateCauseId + '_' + index]) {
      delete this.basicImmidateOptionActive[basicImmediateCauseId + '_' + index];
      const analysisFormData = this.inFormArray(contactIndex);

      if (analysisFormData) {
        let immediateCauseIndex = 0;
        analysisFormData.value.forEach((analysisData: any) => {
          const immediateCause = analysisData.basicImmediateCauseId;
          if (immediateCause === basicImmediateCauseId) {
            log.debug('analysisData.basicImmediateCauseId: ', analysisData.basicImmediateCauseId);
            this.immediateCauseRemove(analysisData.immediateCauseExplanation);
            analysisFormData.removeAt(immediateCauseIndex);
          }
          immediateCauseIndex++;
        });
      }
      this.causeAnalysisValidate();
    } else {
      const immediateId = basicImmediateCauseId + '_' + index;
      this.basicImmidateOptionActive[immediateId] = basicImmediate;
      this.setActivateImmediateCauses(basicImmediateCauseId, index);
      const formArray = this.analysisFormGroup.controls[typeOfContactOrEvent].value[contactIndex]
        .basicImmediateCause as FormArray;

      const basicFormArray = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause`
      ) as FormArray;

      const basicFormValue = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${0}.basicImmediateCauseId`
      ) as FormArray;

      if (formArray[0] == null) {
        this.onAddBasicImmidateCauseField(contactIndex, basicIndex);
      } else if (basicFormValue && basicFormValue.value !== '') {
        this.onAddBasicImmidateCauseField(contactIndex, basicIndex);
      }
      this.ImmediateCauseId = basicImmediateCauseId;
      this.basicImmidateOptionList[index].forEach((element: any) => {
        const basicImmediateId = element.immediateCauseId;
        if (basicImmediateId === basicImmediateCauseId) {
          let formArrayIndex = 0;
          if (basicFormArray.value.length > 1) {
            formArrayIndex = basicFormArray.value.length - 1;
          }

          this.analysisFormGroup
            .get(`typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${formArrayIndex}.basicImmediateCauseId`)
            .patchValue(element.immediateCauseId);
          this.analysisFormGroup
            .get(`typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${formArrayIndex}.immediateCauseExplanation`)
            .patchValue(element.immediateCause);

          this.getNearMissRootCause(basicImmediate, basicIndex, index, element.immediateCauseRelationId);
        }
      });
      this.causeAnalysisValidate();
    }
  }

  getNearMissRootCause(basicImmediateCause: any, basicIndex: any, typeOfContactIndex: any, immidiateCauseIndex: any) {
    if (this.cacheServiceService.basicRootCause) {
      this.filterRootCause(basicImmediateCause, typeOfContactIndex, immidiateCauseIndex);
    } else {
      this.nearMissService.getRootCause().subscribe(
        (response) => {
          this.contactList = response;
          // log.debug(`${JSON.stringify(response)}`);
          if (response) {
            this.cacheServiceService.setBasicRootCause(response);
            this.filterRootCause(basicImmediateCause, typeOfContactIndex, immidiateCauseIndex);
          } else {
            const message = 'RootCause list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  filterRootCause(basicImmediateCause: any, typeOfContactIndex: any, immidiateCauseIndex: any) {
    const rootData = this.cacheServiceService.basicRootCause;
    const causeTree = this.cacheServiceService.causeTree;
    this.filterBasicRootCause = [];
    causeTree.forEach((element: any) => {
      if (element.typeOfContactId === typeOfContactIndex) {
        const immediateArray = element.immediateCause;
        immediateArray.forEach((element2: any) => {
          if (immidiateCauseIndex === element2.basicImmediateCauseId) {
            const rootArray = element2.basicRootCause;
            rootArray.forEach((element3: any) => {
              const rootIndex = element3.basicRootCauseId - 1;
              if (rootData[rootIndex]) {
                this.filterBasicRootCause.push(rootData[rootIndex]);
              }
            });
          }
        });
      }
    });
    this.rootCauseOptionList[basicImmediateCause.immediateCauseId] = this.filterBasicRootCause;
  }

  /**
   * Set Active Class to RootCause List
   */
  getRootClass(rootCause: any, basicIndex: any, idx: any) {
    const rootCauseId = rootCause.basicRootCauseId;
    const basicRootCause = rootCause.basicRootCause;
    if (this.rootCauseArray.indexOf(basicRootCause) > -1) {
      return 'contact-table-incomplete';
    } else if (this.rootCauseOptionActive[rootCauseId + '_' + idx + '_' + basicIndex]) {
      return 'contact-table-active';
    }
  }

  getCorrectiveCause(
    contactActiveType: any,
    currentRootCause: any,
    basicImmediate: any,
    basicIndex: any,
    rootCauseIndex: any,
    index: any
  ) {
    const rootCause = currentRootCause.basicRootCause;
    const rootCauseId = currentRootCause.basicRootCauseId;
    const immediateCauseId = basicImmediate.immediateCauseId;

    this.isRootCause = true;
    this.isCorrectiveCause = true;
    const indexing = rootCauseId + '_' + index + '_' + basicIndex;

    const contactIndex = this.contactOrEventOptionActive.indexOf(contactActiveType.typeOfContact);

    if (this.rootCauseOptionActive[indexing]) {
      delete this.rootCauseOptionActive[indexing];

      const basicImmediateData = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause`
      ) as FormArray;

      if (basicImmediateData) {
        let immediateFormIndex = 0;

        let basicRootIndex = 0;
        basicImmediateData.value.forEach((elements: any) => {
          const immediateId = elements.immediateCauseId;
          if (immediateId === immediateCauseId) {
            immediateFormIndex = basicRootIndex;
          }
          basicRootIndex++;
        });

        this.activeRootCause = '';
        const formArray = this.analysisFormGroup.get(
          `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause`
        ) as FormArray;

        let rootIndex = 0;
        formArray.value.forEach((element: any) => {
          if (element.rootCauseExplanation === rootCause) {
            formArray.removeAt(rootIndex);
            this.rootCauseRemove(rootCause);
          }
          rootIndex++;
        });
        this.causeAnalysisValidate();
      }
    } else {
      this.setActivateRootCauses(rootCauseId, basicIndex, index);
      this.rootCauseOptionActive[indexing] = currentRootCause;
      const basicImmediateData = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause`
      ) as FormArray;
      let immediateFormIndex = 0;

      let basicCauseIndex = 0;
      basicImmediateData.value.forEach((element: any) => {
        const immediateId = element.basicImmediateCauseId;
        if (immediateId === immediateCauseId) {
          immediateFormIndex = basicCauseIndex;
        }
        basicCauseIndex++;
      });
      log.debug('immediateFormIndex: ', immediateFormIndex);

      const basicRootCause = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause`
      ) as FormArray;

      const basicRootCauseValue = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${0}.basicRootCauseId`
      ) as FormArray;

      if (basicRootCause.value.length < 1) {
        this.onAddBasicRootCauseField(contactIndex, immediateFormIndex);
      } else if (basicRootCauseValue.value !== '') {
        this.onAddBasicRootCauseField(contactIndex, immediateFormIndex);
      }

      this.rootCauseOptionList[immediateCauseId].forEach((element: any) => {
        if (element) {
          const basicRootCauseId = element.basicRootCauseId;
          if (basicRootCauseId === rootCauseId) {
            let formArrayIndex = 0;

            if (basicRootCause.value.length > 1) {
              formArrayIndex = basicRootCause.value.length - 1;
            }
            this.analysisFormGroup
              .get(
                `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${0}.basicRootCauseId`
              )
              .patchValue(element.basicRootCauseId);
            // formArray[formArrayIndex].basicRootCauseId = element.basicRootCauseId;
            this.analysisFormGroup
              .get(
                `typeOfContactOrEvent.${contactIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${0}.rootCauseExplanation`
              )
              .patchValue(element.basicRootCause);
            // formArray[formArrayIndex].rootCauseExplanation = element.basicRootCause;
            this.getNearMissCorrectiveCause(contactActiveType, basicImmediate, element, formArrayIndex, index);
          }
        }
      });
      this.causeAnalysisValidate();
    }
  }

  onAddBasicRootCauseField(index: any, basicImmediateIndex: any) {
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    this.subDetails = this.analysisFormGroup.controls[typeOfContactOrEvent].value[index].basicImmediateCause[
      basicImmediateIndex
    ].basicRootCause;
    this.subDetails.push(this.addRootCauseRow());
  }

  /**
   * set Immediate Cause as Active
   */
  setActivateRootCauses(basicRootCuaseId: any, basicIndex: any, index: any) {
    this.activeRootCause = basicRootCuaseId + '_' + index + '_' + basicIndex;
  }

  rootCauseRemove(type: any) {
    const typeIndex = this.rootCauseArray.indexOf(type);
    if (typeIndex > -1) {
      this.rootCauseArray.splice(typeIndex, 1);
    }
  }

  getNearMissCorrectiveCause(
    typeOfContact: any,
    basicImmediate: any,
    basicRootCause: any,
    basicRootCauseIndex: any,
    contactIndex: any
  ) {
    if (this.cacheServiceService.basicCorrectiveCause) {
      this.filterCorrectiveCause(typeOfContact, basicImmediate, basicRootCause, contactIndex);
    } else {
      this.nearMissService.getCorrectiveCause().subscribe(
        (response) => {
          this.contactList = response;
          // log.debug(`${JSON.stringify(response)}`);
          if (response) {
            this.cacheServiceService.setBasicCorrectiveCause(response);
            this.filterCorrectiveCause(typeOfContact, basicImmediate, basicRootCause, contactIndex);
            // this.correctiveCauseOptionList[basicRootCauseId] = response;
          } else {
            const message = 'RootCause list not found';
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
    }
  }

  filterCorrectiveCause(typeOfContact: any, basicImmediateCause: any, rootCause: any, typeOfContactIndex: any) {
    const causeTree = this.cacheServiceService.causeTree;
    const basicCorrectiveCause = this.cacheServiceService.basicCorrectiveCause;
    this.filterBasicCorrectiveCause = [];
    causeTree.forEach((element: any) => {
      if (element.typeOfContactId === typeOfContactIndex) {
        const immediateArray = element.immediateCause;
        immediateArray.forEach((element2: any) => {
          if (basicImmediateCause.immediateCauseRelationId === element2.basicImmediateCauseId) {
            const rootArray = element2.basicRootCause;
            rootArray.forEach((element3: any) => {
              if (element3.basicRootCauseId === rootCause.basicRootCauseRelationId) {
                const correctiveArray = element3.correctiveAction;
                correctiveArray.forEach((element4: any) => {
                  if (basicCorrectiveCause[element4]) {
                    this.filterBasicCorrectiveCause.push(basicCorrectiveCause[element4]);
                  }
                });
              }
            });
          }
        });
      }
    });
    // this.contactOrEventOptionList[typeOfContactIndex].immidiateCauses = this.filterBasicImmediateCause;
    this.correctiveCauseOptionList[rootCause.basicRootCauseId] = this.filterBasicCorrectiveCause;
    // this.rootCauseOptionList[basicImmediateCause.immediateCauseId] = this.filterBasicRootCause;
  }

  setCorrectiveCause(
    contactActiveType: any,
    basicImmidate: any,
    rootCauseData: any,
    correctiveData: any,
    rootIndex: any,
    basicIndex: any,
    idx: any
  ) {
    const typeOfContactOrEvent = 'typeOfContactOrEvent';
    const correctiveIndexing = correctiveData.correctiveActionId + '_' + idx + '_' + basicIndex + '_' + rootIndex;
    const immediateCauseId = basicImmidate.immediateCauseId;
    const rootCauseId = rootCauseData.basicRootCauseId;
    const correctiveActionId = correctiveData.correctiveActionId;
    const contactTypeIndex = this.contactOrEventOptionActive.indexOf(contactActiveType.typeOfContact);

    if (this.correctiveCauseOptionActive.indexOf(correctiveIndexing) !== -1) {
      this.correctiveActionId = '';
      const contactIndex = this.correctiveCauseOptionActive.indexOf(correctiveIndexing);
      this.correctiveCauseOptionActive.splice(contactIndex, 1);

      const basicImmediateData = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactTypeIndex}.basicImmediateCause`
      ) as FormArray;

      if (basicImmediateData) {
        let immediateFormIndex = 0;
        let basicRootCauseId = 0;
        let correctiveId = 0;
        let bIndex = 0;
        basicImmediateData.value.forEach((element: any) => {
          const immediateId = element.basicImmediateCauseId;
          if (immediateId === immediateCauseId) {
            immediateFormIndex = bIndex;
            let rIndex = 0;
            const basicRootCause = element.basicRootCause;
            basicRootCause.forEach((rootElmenet: any) => {
              const rootId = rootElmenet.basicRootCauseId;
              if (rootCauseId === rootId) {
                basicRootCauseId = rIndex;
                const basicCorrectiveCause = rootElmenet.correctiveActionId;
                for (let x = 0; x < basicCorrectiveCause.length; x++) {
                  if (correctiveActionId === basicCorrectiveCause[x]) {
                    correctiveId = x;
                  }
                }
              }
              rIndex++;
            });
          }
          bIndex++;
        });

        const formArray = this.analysisFormGroup.get(
          `typeOfContactOrEvent.${contactTypeIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${basicRootCauseId}.correctiveActionId`
        ) as FormArray;
        formArray.removeAt(correctiveId);
        this.causeAnalysisValidate();
      }
    } else {
      this.correctiveActionId = correctiveIndexing;
      this.correctiveCauseOptionActive.push(this.correctiveActionId);

      const basicImmediateData = this.analysisFormGroup.controls[typeOfContactOrEvent].value[contactTypeIndex]
        .basicImmediateCause as FormArray;
      let immediateFormIndex = 0;
      let basicRootCauseId = 0;
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < basicImmediateData.length; i++) {
        const immediateId = basicImmediateData[i].basicImmediateCauseId;
        if (immediateId === immediateCauseId) {
          immediateFormIndex = i;
          const basicRootCause = basicImmediateData[i].basicRootCause;
          // tslint:disable-next-line:prefer-for-of
          for (let j = 0; j < basicRootCause.length; j++) {
            const rootId = basicRootCause[j].basicRootCauseId;
            if (rootCauseId === rootId) {
              basicRootCauseId = j;
            }
          }
        }
      }

      const formArray = this.analysisFormGroup.get(
        `typeOfContactOrEvent.${contactTypeIndex}.basicImmediateCause.${immediateFormIndex}.basicRootCause.${basicRootCauseId}.correctiveActionId`
      ) as FormArray;
      formArray.push(new FormControl(correctiveData.correctiveActionId));
    }
    this.causeAnalysisValidate();
  }

  /**
   * Set Active Class to Corrective List
   */
  getCorrectiveClass(correctiveCauseId: any, rootIndex: any, basicIndex: any, idx: any) {
    if (
      this.correctiveCauseOptionActive.indexOf(correctiveCauseId + '_' + idx + '_' + basicIndex + '_' + rootIndex) !==
      -1
    ) {
      return 'contact-table-active';
    }
  }

  analysisChartReset() {
    this.contactOrEventOptionActive = [];
    this.basicImmidateOptionActive = [];
    this.rootCauseOptionActive = [];
    this.correctiveCauseOptionActive = [];
    this.typeOfContactArray = [];
    this.basicCauseArray = [];
    this.rootCauseArray = [];
    this.correctiveActionId = '';
    this.activeRootCause = '';
    this.activeImmediateCause = '';
    this.activeContactCause = '';
    const form = this.analysisFormGroup.get(`typeOfContactOrEvent`) as FormArray;
    form.clear();
  }
}
