import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const routes = {
  quote: `/nearmiss`,
};

@Injectable({
  providedIn: 'root',
})
export class IncidentInvestigationService {
  constructor(private httpClient: HttpClient) {}

  /**
   *  Get User Profile Details
   */
  getNearMiss(): Observable<Response> {
    return this.httpClient
      .cache()
      .get(routes.quote)
      .pipe(
        map((responseData) => {
          const statusCode = 'statusCode';
          const dataParam = 'data';
          return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
        }),
        tap((item) => {
          if (item) {
            return item;
          }
        }), // when success, add the item to the local service
        catchError((err) => {
          return of(false);
        })
      );
  }
}
