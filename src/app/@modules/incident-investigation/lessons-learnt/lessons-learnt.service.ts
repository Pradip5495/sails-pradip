import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpDispatcherService } from '@core';

const routes = {
  lessonLearnt: {
    get: (id: string) => `/lessonslearnt/${id}`,
    delete: (id: string) => `/lessonslearnt/${id}`,
    update: (id: string) => `/lessonslearnt/${id}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class LessonsLearntService {
  constructor(private httpClient: HttpClient, private httpDispatcher: HttpDispatcherService) {}

  /**
   *  Get All lessons learnt
   */
  getAllLessonsLearntSharing(): Observable<Response> {
    const API_URL = `/lessonslearnt`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All User List
   */
  getAllUsers(): Observable<any> {
    const API_URL = `/user`;
    return this.httpDispatcher.get(API_URL);

    /* Code Cleanup */
    // return this.httpClient.get(API_URL).pipe(
    //   map((responseData) => {
    //     const statusCode = 'statusCode';
    //     const dataParam = 'payload';
    //     return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
    //   }),
    //   tap((item) => {
    //     if (item) {
    //       return item;
    //     }
    //   }), // when success, add the item to the local service
    //   catchError((err) => {
    //     return of(false);
    //   })
    // );
  }

  /**
   *  Get All Related Incident Id's
   */
  getAllIncident(): Observable<any> {
    const API_URL = `/incident`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        const incidentIds = responseData[dataParam];
        /* This code will be removed
          Reason to add this code is, as of now we are not able to get the
          lessons learnts of an incident.
          Once we are able to get the lessons learnt respective to selected Incident
          we will remove this code
        */
        incidentIds.forEach((element: any) => {
          element.lessons = [
            {
              lessonsLearntTypeId: '9bc66ee5-44fc-417a-9190-e028c274e66d',
              description: 'Updated After selecting Data 1 Demo 1',
            },
            {
              lessonsLearntTypeId: '728b87c7-5fd7-43dc-941f-98725c76023d',
              description: 'Updated After selecting Data 2 Demo 2',
            },
          ];
        });
        if (responseData[statusCode] && responseData[statusCode] === 200) {
          incidentIds.forEach((element: any, index: any) => {
            if (index % 2 === 0) {
              element.lessons = [
                {
                  lessonsLearntTypeId: '82cc45b1-f372-48d5-8c01-9c02365331cc',
                  description: 'Updated After selecting Data 3 Demo600',
                },
                {
                  lessonsLearntTypeId: '0c0764f3-7fcb-457a-8f2a-808632e5242f',
                  description: 'Updated After selecting Data 4 Demo 4',
                },
              ];
            } else {
              element.lessons = [
                {
                  lessonsLearntTypeId: '51aa22ac-64ca-42c1-a2bd-8456af6f7b66',
                  description: 'Updated After selecting Data 5 Gas',
                },
              ];
            }
          });
          return incidentIds;
        } else {
          return false;
        }
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All UserName
   */

  getUserProfile(): Observable<Response> {
    return this.httpClient.get('/profile').pipe(
      tap((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        if (responseData[statusCode] === 200) {
          return responseData[dataParam];
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }

  /**
   *  Get All Vessel Type Details
   */

  getVesselType(): Observable<any> {
    const API_EndPoint = '/vessel/type';

    return this.httpDispatcher.get(API_EndPoint);

    /* Code Clean up */
    // return this.httpClient.get('/vessel/type').pipe(
    //   tap((responseData) => {
    //     const statusCode = 'statusCode';
    //     const dataParam = 'data';
    //     if (responseData[statusCode] === 200) {
    //       return responseData[dataParam];
    //     } else {
    //       return [];
    //     }
    //   }),
    //   catchError((err) => {
    //     return of(err);
    //   })
    // );
  }

  /**
   *  Get All Vessel Details
   */

  getVessel(): Observable<Response> {
    const API_URL = `/vessel`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All Designation
   */
  getDesignation(): Observable<Response> {
    const API_URL = `/user/designation`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All Deficiency Group List
   */
  getAllDeficiency(): Observable<Response> {
    const API_URL = `/deficiencyGroup`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All Target Group List
   */
  getAllTargetGroup(): Observable<Response> {
    const API_URL = `/targetgroup`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All Lessons Type  List
   */
  getAllLessonType(): Observable<any> {
    const API_URL = `/lessonslearnttype`;

    return this.httpDispatcher.get(API_URL);

    // return this.httpClient.get(API_URL).pipe(
    //   map((responseData) => {
    //     const statusCode = 'statusCode';
    //     const dataParam = 'data';
    //     return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
    //   }),
    //   tap((item) => {
    //     if (item) {
    //       return item;
    //     }
    //   }), // when success, add the item to the local service
    //   catchError((err) => {
    //     return of(false);
    //   })
    // );
  }

  /**
   * Submit Near Miss Data
   */
  createLessonsLearnt(formData: any): Observable<Response> {
    return this.httpClient.post('/lessonslearnt', formData).pipe(
      tap((responseData: any) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        if (responseData[statusCode] === 200) {
          return responseData[dataParam];
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }

  /**
   * Delete lesson lernt data
   */
  deleteLessonLearnt(lessonLearntId: any): Observable<any> {
    return this.httpDispatcher.delete(routes.lessonLearnt.delete(lessonLearntId));
  }

  /**
   * get lessons learnt data
   */
  getLessonsLearntById(lessonLearntId: any): Observable<Response> {
    return this.httpDispatcher.get(routes.lessonLearnt.get(lessonLearntId));
  }

  /**
   * update lessons learnt
   */
  updateLessonsLearnt(lessonLearntId: string, data: any): Observable<Response> {
    return this.httpDispatcher.put(routes.lessonLearnt.update(lessonLearntId), data);
  }

  /**
   * Submit Acknowledgement form
   */
  createAcknowledgement(formData: any): Observable<Response> {
    return this.httpClient.post('/fleet/notification/acknowledgement', formData).pipe(
      tap((responseData: any) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        if (responseData[statusCode] === 200) {
          return responseData[dataParam];
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }

  /**
   * Get Types
   */
  getAllTypes(): Observable<any> {
    return this.httpDispatcher.get('/form-feeder/list');
  }
}
