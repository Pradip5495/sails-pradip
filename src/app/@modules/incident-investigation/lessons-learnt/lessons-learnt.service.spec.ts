import { TestBed } from '@angular/core/testing';

import { LessonsLearntService } from './lessons-learnt.service';

describe('LessonsLearntService', () => {
  let service: LessonsLearntService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LessonsLearntService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
