import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '@shared/common-service';
import { environment } from '@env/environment';
import { LessonsLearntService } from '../../lessons-learnt.service';

@Component({
  selector: 'app-lessons-learnt-details',
  templateUrl: './lessons-learnt-details.component.html',
  styleUrls: ['./lessons-learnt-details.component.scss'],
})
export class LessonsLearntDetailsComponent implements OnInit, OnDestroy {
  public isLoading = false;
  public inid: any;
  public title: '';
  public isdataSource = false;
  public isTableView = true;
  public portList: any = [];
  public incidentData: any = [];
  public lessonslLearntDetailsData: any = [];
  public investigatorsId: any;
  public id: any;
  public isShow = false;
  public vesselTypeData: any = [];
  public states: string[] = ['None', 'Alaska'];
  public probabilityList: string[] = ['Rare', 'Unlikely', 'Possible', 'Likely', 'Almost Certain'];
  public probability = 'Possible';
  public severityLevel: string[] = ['Extreme', 'Major', 'Moderate', 'Minor', 'Trivial'];
  public reportType: any;
  public array: any;
  public length = 100;
  public pageSizeOptions: number[] = [5, 10, 25, 100];
  public rowData: any;
  public icons: any;

  public API_URL = environment.filePath;
  error: string | undefined;
  constructor(
    private lessonsLearntService: LessonsLearntService,
    private notifyService: NotificationService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const paramValue = 'value';
    if (this.route.params[paramValue].id) {
      this.inid = this.route.params[paramValue].id;
      this.getLessonsLearntById(this.inid);
    }
  }

  getLessonsLearntById(lessonsLearntId: any) {
    this.lessonsLearntService.getLessonsLearntById(lessonsLearntId).subscribe(
      (response) => {
        this.lessonslLearntDetailsData = response;
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }
  onChangeprobability(event: any) {}
  setProbability(probability: any) {
    this.probability = probability;
    return this.probability;
  }

  getFilterFile(receivedImageData: any) {
    const imageTypeAssetMapping = {
      png: this.API_URL + '/' + receivedImageData.path,
      jpg: this.API_URL + '/' + receivedImageData.path,
      jpeg: this.API_URL + '/' + receivedImageData.path,
      zip: '/assets/zip.png',
      ' ': '/assets/zip.png',
      pdf: '/assets/pdf.png',
    };
    return imageTypeAssetMapping[receivedImageData.fileType] || 'assets/file.png';
  }

  /**
   * return image class based on file type
   */
  getImageStyleClass(imageType: any) {
    const imageTypeStyleMapping = {
      png: 'upload-images',
      jpg: 'upload-images',
      jpeg: 'upload-images',
    };

    return imageTypeStyleMapping[imageType] || 'pdfimages';
  }

  ngOnDestroy() {}
}
