import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LessonsLearntDetailsComponent } from './lessons-learnt-details.component';

describe('LessonsLearntDetailsComponent', () => {
  let component: LessonsLearntDetailsComponent;
  let fixture: ComponentFixture<LessonsLearntDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LessonsLearntDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonsLearntDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
