import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonsLearntFormComponent } from './lessons-learnt-form.component';

describe('LessonsLearntFormComponent', () => {
  let component: LessonsLearntFormComponent;
  let fixture: ComponentFixture<LessonsLearntFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LessonsLearntFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonsLearntFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
