import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { LessonsLearntService } from '../../lessons-learnt.service';
import { Logger, untilDestroyed } from '@core';
import { ActivatedRoute, Router } from '@angular/router';
import { ICustomFormlyConfig, IJson } from '@types';
import { FormlyFieldConfig, FormlyFormOptions } from '@formly';
import { of } from 'rxjs';
import { NotificationService } from '@shared/common-service';
import { DatePipe } from '@angular/common';

const log = new Logger('Lessons-Learnt-Form');

@Component({
  template: `
    <div class="form-control">
      <p>
        <span class="font-weight-600"> {{ props.label || 'Field' }}:</span> {{ formState[props.key] }}
      </p>
    </div>
  `,
  styles: [
    `
      .font-weight-600 {
        font-weight: 600;
      }
    `,
  ],
})
class TemplateComponent {
  @Input()
  model: IJson = {};

  @Input()
  formState: IJson = {};

  @Input()
  props: IJson = {};
}

@Component({
  selector: 'app-lessons-learnt-form',
  templateUrl: './lessons-learnt-form.component.html',
  styleUrls: ['./lessons-learnt-form.component.scss'],
})
export class LessonsLearntFormComponent implements OnInit, OnDestroy {
  incidentData: any;
  lessonsLearntVesselTypeData: any = [];
  lessonsLearntVesselType: any;
  lessonsLearntVesselData: any = [];
  error: string | undefined;
  statusArray: any[] = [];
  uploadedFiles: File[] = [];
  newUploadedFile: File[] = [];
  lessonsLearntPayload: any = {};
  public title = 'Lessons Learnt';
  vesselTypeId: any = [];
  actions: any = [];

  overviewFormKeys = {
    issuedbyName: 'issuedByNameId',
    notificationTitle: 'notificationTitle',
    vesselTypeId: 'vesselTypeId',
    incidentId: 'incidentId',
    comments: 'comments',
    incidentDate: 'incidentDate',
    incidentType: 'incidentType',
    severityLevel: 'severityLevel',
  };
  overviewForm = new FormGroup({});
  overviewModel = {};
  overviewOptions: FormlyFormOptions = {
    formState: {
      date: '',
      incidentType: '',
      severityLevel: '',
      incidentTitle: '',
    },
  };
  overviewFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          key: this.overviewFormKeys.incidentId,
          type: 'select',
          defaultValue: '',
          templateOptions: {
            label: 'Report Id',
            placeholder: 'Report Id',
            appearance: 'outline',
            required: true,
            options: this.lessonsLearntService.getAllIncident(),
            valueProp: 'incidentId',
            labelProp: 'reportId',
            change: (field: FormlyFieldConfig, event: any) => {
              const selectedIncident = this.incidentData.find((incident: any) => incident.incidentId === event.value);
              this.overviewOptions.formState.date = selectedIncident.date;
              this.overviewOptions.formState.incidentType = selectedIncident.type;
              this.overviewOptions.formState.severityLevel = selectedIncident.severityLevel;
              this.overviewOptions.formState.incidentTitle = selectedIncident.incidentTitle;
              this.lessonsLearntForm.patchValue({
                lessons: selectedIncident.lessons,
              });
            },
          },
        },
        {
          type: 'template-component',
          className: 'flex-1',
          templateOptions: {
            component: TemplateComponent,
            props: {
              key: 'date',
              label: 'Date',
            },
          },
        },
        {
          type: 'template-component',
          className: 'flex-1',
          templateOptions: {
            component: TemplateComponent,
            props: {
              key: 'incidentType',
              label: 'Incident Type',
            },
          },
        },
        {
          type: 'template-component',
          className: 'flex-1',
          templateOptions: {
            component: TemplateComponent,
            props: {
              key: 'severityLevel',
              label: 'Severity Level',
            },
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          type: 'template-component',
          className: 'flex-1',
          templateOptions: {
            component: TemplateComponent,
            props: {
              key: 'incidentTitle',
              label: 'Notification Title',
            },
          },
        },
        {
          key: this.overviewFormKeys.vesselTypeId,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Applicable to Vessel Type',
            options: this.lessonsLearntService.getVesselType(),
            valueProp: 'vesselTypeId',
            labelProp: 'vesselType',
            required: true,
            multiple: true,
            change: (field: FormlyFieldConfig, event: any) => {},
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.overviewFormKeys.comments,
          type: 'textarea',
          templateOptions: {
            label: 'Comments',
            required: true,
          },
        },
      ],
    },
  ];

  descriptionKey = 'description';
  descriptionForm = new FormGroup({});
  descriptionModel = {};
  descriptionOptions: FormlyFormOptions = {};
  descriptionFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.descriptionKey,
          type: 'textarea',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            label: 'Description',
            placeholder: 'Description',
            rows: 10,
          },
        },
      ],
    },
  ];

  attachmentFileKey = 'files';
  attachmentForm = new FormGroup({});
  attachmentModel = {};
  attachmentOptions: FormlyFormOptions = {
    formState: {
      images: [],
    },
  };
  attachmentFields: FormlyFieldConfig[] = [
    {
      key: this.attachmentFileKey,
      type: 'sailfile',
      templateOptions: {
        preview: true,
        id: 'fleet-file',
        onFileChange: this.onFormlyAttachmentSubmit.bind(this),
      },
    },
  ];

  lessonsLearntKey = {
    lessons: 'lessons',
    relatedTo: 'relatedTo',
    description: 'description',
    lessonsLearntTypeId: 'lessonsLearntTypeId',
  };
  lessonsLearntForm = new FormGroup({});
  lessonsLearntModel = {
    lessons: [
      {
        lessonsLearntTypeId: '728b87c7-5fd7-43dc-941f-98725c76023d',
        description: 'sklasgdkjash Demo 2',
      },
      {
        lessonsLearntTypeId: '51aa22ac-64ca-42c1-a2bd-8456af6f7b66',
        description: 'Tesitng Gas',
      },
    ],
  };
  lessonsLearntOptions: FormlyFormOptions = {};
  lessonsLearntFields: FormlyFieldConfig[] = [
    {
      type: 'repeat',
      key: this.lessonsLearntKey.lessons,
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add New',
        addTextClass: 'add-btn',
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'select',
                key: this.lessonsLearntKey.lessonsLearntTypeId,
                templateOptions: {
                  placeholder: 'Related To',
                  label: 'Related To',
                  required: true,
                  options: this.lessonsLearntService.getAllLessonType(),
                  valueProp: 'lessonsLearntTypeId',
                  labelProp: 'lessonType',
                },
              },
              {
                className: 'flex-2',
                type: 'input',
                key: this.lessonsLearntKey.description,
                templateOptions: {
                  placeholder: 'Description',
                  label: 'Description',
                  required: true,
                },
              },
            ],
          },
        ],
      },
    },
  ];

  vesselsFormKeys = {
    vesselSelection: 'vesselSelection',
    vesselIds: 'vesselIds',
    email: 'email',
    vesselId: 'vesselId',
  };
  vesselsForm = new FormGroup({});
  vesselsModel = {};
  vesselsOptions: FormlyFormOptions = {};
  vesselsFields: FormlyFieldConfig[] = [
    {
      template:
        '<div style="display: block; width: 100%; border-bottom: 5px solid black"> <b> Vessel Type *</b> </div>',
    },
    {
      template: '<hr />',
    },
    {
      fieldGroup: [
        {
          key: this.vesselsFormKeys.vesselSelection,
          type: 'radio',
          templateOptions: {
            className: 'flex-1',
            required: true,
            options: this.lessonsLearntService.getVesselType(),
            valueProp: 'vesselTypeId',
            labelProp: 'vesselType',
          },
        },
        {
          template: `
            <hr class="margin-bottom-2rem">
          `,
        },
        {
          key: this.vesselsFormKeys.vesselIds,
          type: 'multicheckbox',
          templateOptions: {
            label: 'Vessel',
            className: 'flex-1',
            required: true,
            options: [],
            valueProp: 'vesselId',
            labelProp: 'vessel',
          },
          hideExpression: (model, formState, field) => {
            return !field.templateOptions.options;
          },
          hooks: {
            onInit: (field) => {
              this.vesselsForm
                .get(this.vesselsFormKeys.vesselSelection)
                .valueChanges.pipe(untilDestroyed(this))
                .subscribe((value) => {
                  const selectedVessel = this.lessonsLearntVesselData.filter(
                    (vessels: any) => vessels.vesselTypeId === value
                  );
                  field.templateOptions.options = selectedVessel;
                });
            },
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.vesselsFormKeys.email,
          type: 'input',
          className: 'flex-1',
          templateOptions: {
            label: 'Additional Email Ids',
            description: 'Separate Email with Comma',
          },
          validators: {
            validation: ['multipleEmails'],
          },
        },
      ],
    },
  ];

  actionFormKeys = {
    action: 'action',
    natureOfAction: 'natureOfAction',
    dueDate: 'dueDate',
    status: 'status',
    dateCompleted: 'dateCompleted',
    send: 'send',
  };
  actionsForm = new FormGroup({});
  actionsModel = {
    action: [
      {
        natureOfAction: '',
        dueDate: '',
        status: 'open',
        dateCompleted: '',
      },
    ],
  };
  actionsOptions: FormlyFormOptions = {};
  actionsFields: FormlyFieldConfig[] = [
    {
      type: 'repeat',
      key: this.actionFormKeys.action,
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add Action',
        addTextClass: 'add-btn',
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'input',
                key: this.actionFormKeys.natureOfAction,
                templateOptions: {
                  placeholder: 'Nature of Action',
                  label: 'Nature of Action',
                  required: true,
                },
              },
              {
                className: 'flex-1',
                type: 'matdatepicker',
                key: this.actionFormKeys.dueDate,
                defaultValue: '',
                templateOptions: {
                  label: 'Due Date',
                  appearance: 'outline',
                  datepickerOptions: {
                    // max: new Date(),
                  },
                },
              },
              {
                className: 'flex-1',
                key: this.actionFormKeys.status,
                type: 'autocomplete',
                defaultValue: 'Open',
                templateOptions: {
                  label: 'Status',
                  required: true,
                  appearance: 'outline',
                  filter: (term: string) => of(term ? this.filterStatus(term) : this.statusArray.slice()),
                },
              },
              {
                className: 'flex-1',
                type: 'matdatepicker',
                key: this.actionFormKeys.dateCompleted,
                defaultValue: '',
                templateOptions: {
                  label: 'Completed Date',
                  appearance: 'outline',
                  datepickerOptions: {
                    // max: new Date(),
                  },
                },
              },
            ],
          },
        ],
      },
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.actionFormKeys.send,
          type: 'checkbox',
          defaultValue: false,
          templateOptions: {
            label: 'Send *',
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      shortName: 'Overview',
      title: 'Overview',
      subTitle: 'Enter Overview Data',
      config: {
        model: this.overviewModel,
        fields: this.overviewFields,
        options: this.overviewOptions,
        form: this.overviewForm,
      },
      onSave: this.onFormlyOverviewSubmit.bind(this),
      discard: true,
      onDiscard: () => this.overviewForm.reset(),
    },
    {
      shortName: 'Description',
      title: 'Description',
      subTitle: 'Enter Description Data',
      config: {
        model: this.descriptionModel,
        fields: this.descriptionFields,
        options: this.descriptionOptions,
        form: this.descriptionForm,
      },
      onSave: this.onFormlyDescriptionSubmit.bind(this),
      discard: true,
      onDiscard: () => this.descriptionForm.reset(),
    },
    {
      shortName: 'Attachment',
      title: 'Attachment',
      subTitle: 'Enter Attachment Data',
      config: {
        model: this.attachmentModel,
        fields: this.attachmentFields,
        options: this.attachmentOptions,
        form: this.attachmentForm,
      },
      onSave: this.onFormlyAttachmentSubmit.bind(this),
      discard: true,
      onDiscard: () => this.attachmentForm.reset(),
    },
    {
      shortName: 'Lessons Learnt',
      title: 'Lessons Learnt',
      subTitle: 'Enter Lessons Learnt Data',
      config: {
        model: this.lessonsLearntModel,
        fields: this.lessonsLearntFields,
        options: this.lessonsLearntOptions,
        form: this.lessonsLearntForm,
      },
      onSave: this.onFormlyLessonsLearntSubmit.bind(this),
      discard: true,
      onDiscard: () => this.lessonsLearntForm.reset(),
    },
    {
      shortName: 'Vessels',
      title: 'Vessels',
      subTitle: 'Enter Vessels Data',
      config: {
        model: this.vesselsModel,
        fields: this.vesselsFields,
        options: this.vesselsOptions,
        form: this.vesselsForm,
      },
      onSave: this.onFormlyVesselsSubmit.bind(this),
      discard: true,
      onDiscard: () => this.vesselsForm.reset(),
    },
    {
      shortName: 'Actions',
      title: 'Actions',
      subTitle: 'Enter Actions Data',
      config: {
        model: this.actionsModel,
        fields: this.actionsFields,
        options: this.actionsOptions,
        form: this.actionsForm,
      },
      onSave: this.onFormlyActionsSubmit.bind(this),
      discard: true,
      onDiscard: () => this.actionsForm.reset(),
    },
  ];

  isEditMode: boolean;
  private lessonsLearntId: string;

  constructor(
    private lessonsLearntService: LessonsLearntService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dateFormatter: DatePipe,
    private notifyService: NotificationService
  ) {}

  ngOnInit() {
    this.getVessel();
    this.getAllIncident();
    this.getAllTypes();

    const value = 'value';
    // Logic for Edit Mode
    this.isEditMode = !!this.activatedRoute.params[value].id;
    this.lessonsLearntId = this.isEditMode ? this.activatedRoute.params[value].id : null;
    if (this.isEditMode) {
      // Get Lesson Learnt Details by ID
      this.getSelectedLesseonLearntData();
    }
  }

  ngOnDestroy() {}

  navigateBack() {
    const navigateBackUrl = this.isEditMode ? '../../' : '../';
    this.router
      .navigate([navigateBackUrl], {
        relativeTo: this.activatedRoute,
      })
      .catch((e) => {
        log.error(e);
      });
  }

  /* This function is used by autocomplete field in Action section */
  filterStatus(name: string) {
    return this.statusArray.filter((state: any) => state.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  //#region Form Submit Function Region Start

  onFormlyOverviewSubmit(formData: any) {
    const requiredOverviewData = {
      incidentId: formData.incidentId,
      comments: formData.comments,
      vesselTypeId: formData.vesselTypeId,
    };
    this.lessonsLearntPayload = { ...this.lessonsLearntPayload, ...requiredOverviewData };
  }

  onFormlyDescriptionSubmit(formData: any) {
    if (formData.description) {
      this.lessonsLearntPayload = { ...this.lessonsLearntPayload, ...formData };
    } else {
      if (this.lessonsLearntPayload.hasOwnProperty(this.descriptionKey)) {
        delete this.lessonsLearntPayload[this.descriptionKey];
      }
    }
  }

  onFormlyAttachmentSubmit(files: any) {
    this.uploadedFiles = files ? files : this.uploadedFiles;
    if (this.uploadedFiles.length > 0) {
      this.newUploadedFile = this.uploadedFiles;
      this.lessonsLearntPayload = { ...this.lessonsLearntPayload, ...{ files: this.uploadedFiles } };
    } else {
      if (this.lessonsLearntPayload.hasOwnProperty(this.attachmentFileKey)) {
        this.lessonsLearntPayload = { ...this.lessonsLearntPayload, ...{ files: this.newUploadedFile } };
      }
    }
  }

  onFormlyLessonsLearntSubmit(formData: any) {
    const lessonsLearnt: any[] = [];
    if (formData.lessons.length > 0) {
      formData.lessons.forEach((element: any) => {
        const lesson = {};
        if (element.hasOwnProperty(this.lessonsLearntKey.lessonsLearntTypeId)) {
          if (element[this.lessonsLearntKey.lessonsLearntTypeId]) {
            lesson[this.lessonsLearntKey.lessonsLearntTypeId] = element[this.lessonsLearntKey.lessonsLearntTypeId];
          }
        }

        if (element.hasOwnProperty(this.lessonsLearntKey.description)) {
          if (element[this.lessonsLearntKey.description]) {
            lesson[this.lessonsLearntKey.description] = element[this.lessonsLearntKey.description];
          }
        }

        if (Object.keys(lesson).length) {
          lessonsLearnt.push(lesson);
        }
      });
    }
    if (lessonsLearnt.length > 0) {
      this.lessonsLearntPayload = { ...this.lessonsLearntPayload, ...{ lessons: lessonsLearnt } };
    }
  }

  onFormlyVesselsSubmit(formData: any) {
    // Preparing VesselIds Array
    const vesselIds = [];
    if (formData.vesselIds) {
      for (const [key, value] of Object.entries(formData.vesselIds)) {
        if (value) {
          vesselIds.push(key);
        }
      }
    }

    // Preparing EmailIds Array
    const emailIds: string[] = [];
    if (formData.email) {
      const emailArray = formData.email.split(',');
      emailArray.forEach((value: string) => {
        if (value) {
          emailIds.push(value.trim());
        }
      });
    }
    const modifiedVesselFormData = {};
    this.vesselsFormKeys.email = formData.email;
    // if (emailIds.length > 0) {
    //   modifiedVesselFormData[this.vesselsFormKeys.email] = emailIds;
    // } else {
    if (this.lessonsLearntPayload.hasOwnProperty(this.vesselsFormKeys.email)) {
      delete this.lessonsLearntPayload[this.vesselsFormKeys.email];
    }
    // }
    if (vesselIds.length > 0) {
      modifiedVesselFormData[this.vesselsFormKeys.vesselIds] = vesselIds;
    } else {
      if (this.lessonsLearntPayload.hasOwnProperty(this.vesselsFormKeys.vesselIds)) {
        delete this.lessonsLearntPayload[this.vesselsFormKeys.vesselIds];
      }
    }

    this.lessonsLearntPayload = { ...this.lessonsLearntPayload, ...modifiedVesselFormData };
  }

  onFormlyActionsSubmit(formData: any) {
    /* Checking if Below three forms data is saved or not */
    this.onFormlyOverviewSubmit(this.overviewForm.value);

    this.onFormlyDescriptionSubmit(this.descriptionForm.value);

    this.onFormlyVesselsSubmit(this.vesselsForm.value);

    this.onFormlyLessonsLearntSubmit(this.lessonsLearntForm.value);

    const actions = [...formData.action];
    for (const action of actions) {
      if (action) {
        if (!isNaN(Date.parse(action[this.actionFormKeys.dateCompleted]))) {
          action[this.actionFormKeys.dateCompleted] = new Date(action[this.actionFormKeys.dateCompleted]).toISOString();
        } else {
          if (action.hasOwnProperty(this.actionFormKeys.dateCompleted)) {
            delete action[this.actionFormKeys.dateCompleted];
          }
        }

        if (!isNaN(Date.parse(action[this.actionFormKeys.dueDate]))) {
          action[this.actionFormKeys.dueDate] = new Date(action[this.actionFormKeys.dueDate]).toISOString();
        } else {
          if (action.hasOwnProperty(this.actionFormKeys.dueDate)) {
            delete action[this.actionFormKeys.dueDate];
          }
        }

        action[this.actionFormKeys.status] = action[this.actionFormKeys.status];
        action[this.actionFormKeys.natureOfAction] = action[this.actionFormKeys.natureOfAction];
        action.isFleetNotification = 1;
      }
    }
    const modifiedActionFormData = {
      send: '1',
      action: actions,
    };
    this.lessonsLearntPayload = { ...this.lessonsLearntPayload, ...modifiedActionFormData };

    if (this.overviewForm.valid && this.actionsForm.valid && this.vesselsForm.valid) {
      if (this.isEditMode) {
        this.updateLessonsLearntWithFormly(this.lessonsLearntPayload);
      } else {
        this.createLessonLearntWithFormly(this.lessonsLearntPayload);
      }
    } else {
      this.notifyService.showWarning('Please fill All Mandatory Fields', this.title);
    }
  }

  //#endregion Form Submit Function Region End

  createLessonLearntWithFormly(payLoad: any) {
    const formData = new FormData();
    for (const key in payLoad) {
      if (key === 'files') {
        for (const file of payLoad[key]) {
          formData.append(key, file);
        }
      } else if (key === 'vesselTypeId') {
        payLoad.vesselTypeId.forEach((element: any, index: any) => {
          formData.append('vesselTypeId[' + index + ']', element);
        });
      } else if (key === 'vesselIds') {
        payLoad.vesselIds.forEach((element: any, index0: any) => {
          formData.append('vesselIds[' + index0 + ']', element);
        });
      } else if (key === 'lessons') {
        payLoad.lessons.forEach((element: any, index1: any) => {
          formData.append('lessons[' + index1 + '][lessonsLearntTypeId]', element.lessonsLearntTypeId);
          formData.append('lessons[' + index1 + '][description]', element.description);
        });
      } else if (key === 'action') {
        payLoad.action.forEach((element: any, index2: any) => {
          formData.append('action[' + index2 + '][dateCompleted]', element.dateCompleted);
          formData.append('action[' + index2 + '][dueDate]', element.dueDate);
          formData.append('action[' + index2 + '][isFleetNotification]', '1');
          formData.append('action[' + index2 + '][natureOfAction]', element.natureOfAction);
          formData.append('action[' + index2 + '][status]', element.status);
        });
      } else if (key === 'send') {
        formData.append('send', payLoad.send);
      } else if (key === 'email') {
        payLoad.email.forEach((element: any, index3: any) => {
          formData.append('email[' + index3 + ']', element);
        });
      } else {
        formData.append(key, payLoad[key]);
      }
    }
    this.lessonsLearntService
      .createLessonsLearnt(formData)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response: any) => {
          log.debug('response: ', response);
          if (response.statusCode === 201) {
            const message = 'New Lessons Learnt created successfully!';
            this.notifyService.showSuccess(message, this.title);
            this.navigateBack();
          } else {
            const message = response.message;
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          this.error = error;
          const message = error;
          this.notifyService.showError(message, this.title);
        }
      );
  }

  updateLessonsLearntWithFormly(payLoad: any) {
    const formData = new FormData();
    for (const key in payLoad) {
      if (key === 'files') {
        for (const file of payLoad[key]) {
          formData.append(key, file);
        }
      } else if (key === 'vesselTypeId') {
        payLoad.vesselTypeId.forEach((element: any, index: any) => {
          formData.append('vesselTypeId[' + index + ']', element);
        });
      } else if (key === 'vesselIds') {
        payLoad.vesselIds.forEach((element: any, index1: any) => {
          formData.append('vesselIds[' + index1 + ']', element);
        });
      } else if (key === 'lessons') {
        payLoad.lessons.forEach((element: any, index2: any) => {
          formData.append('lessons[' + index2 + '][lessonsLearntTypeId]', element.lessonsLearntTypeId);
          formData.append('lessons[' + index2 + '][description]', element.description);
        });
      } else if (key === 'action') {
        payLoad.action.forEach((element: any, index3: any) => {
          formData.append('action[' + index3 + '][dateCompleted]', element.dateCompleted);
          formData.append('action[' + index3 + '][dueDate]', element.dueDate);
          formData.append('action[' + index3 + '][isFleetNotification]', '1');
          formData.append('action[' + index3 + '][natureOfAction]', element.natureOfAction);
          formData.append('action[' + index3 + '][status]', element.status);
        });
      } else if (key === 'send') {
        formData.append('send', payLoad.send);
      } else if (key === 'email') {
        payLoad.email.forEach((element: any, index3: any) => {
          formData.append('email[' + index3 + ']', element);
        });
      } else {
        formData.append(key, payLoad[key]);
      }
    }
    formData.set('isActive', '1');

    this.lessonsLearntService
      .updateLessonsLearnt(this.lessonsLearntId, formData)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response: any) => {
          if (response.statusCode === 200) {
            const message = response.message || 'Lessons Learnt updated successfully!';
            this.notifyService.showSuccess(message, this.title);
            this.navigateBack();
          } else {
            const message = response.message;
            this.notifyService.showError(message, this.title);
          }
        },
        (error) => {
          const message = error;
          this.notifyService.showError(message, this.title);
          // this._snackBar.open(message, '', {
          //   duration: 4000
          // });
        }
      );
  }

  getAllTypes() {
    this.lessonsLearntService.getAllTypes().subscribe(async (data: any) => {
      if (data) {
        this.statusArray = (await data.status) ? data.status : [];
      }
    });
  }

  //#region for Edit Functionality Start

  async getSelectedLesseonLearntData() {
    const lessonsLearntDetails: any = await this.lessonsLearntService
      .getLessonsLearntById(this.lessonsLearntId)
      .pipe(untilDestroyed(this))
      .toPromise();
    // Mapping the data for Dates
    if (lessonsLearntDetails.length > 0) {
      lessonsLearntDetails.map((obj: any) => {
        if (obj.files) {
          obj.files.map((file: any, index: number) => {
            const splittedFileName = file.path.split('.');
            obj.files[index].fileType = splittedFileName[splittedFileName.length - 1];
          });
        }
        return obj;
      });

      // Initializing the FormData
      this.initializeOverviewFormData(lessonsLearntDetails[0]);

      this.initializedescriptionFormData(lessonsLearntDetails[0]);

      this.displayImagePreviewsIfAttached(lessonsLearntDetails[0]);

      this.initializedlessonsLearntFormData(lessonsLearntDetails[0]);

      this.initializedVesselsFormData(lessonsLearntDetails[0]);

      this.initializeActionFormData(lessonsLearntDetails[0]);
    }
  }

  initializeOverviewFormData(overviewData: any) {
    const vesselType: any = [];
    // Initializing the static Keys
    overviewData.vesselType.forEach((value: any, index: any) => {
      vesselType.push(value.vesselTypeId);
    });
    overviewData.vesselTypeId = vesselType;
    if (overviewData) {
      this.overviewForm.patchValue(overviewData);
    }
    this.overviewOptions.formState = {
      date: overviewData.date,
      incidentType: overviewData.incidentType,
      severityLevel: overviewData.severityLevel,
      incidentTitle: overviewData.incidentTitle,
    };

    this.overviewFormKeys.incidentId = overviewData.reportId;
  }

  initializedescriptionFormData(descriptionData: any) {
    if (descriptionData) {
      this.descriptionForm.patchValue(descriptionData);
      setTimeout(() => {
        this.descriptionForm.patchValue({ descriptionKey: descriptionData.description });
      }, 10);
    }
  }

  displayImagePreviewsIfAttached(attachmentData: any) {
    if (attachmentData.lessonAttachments) {
      // this.imageUrlsArray = thirdPartyData.files;
      // this.imageUrlsArray.map( (images) => {
      //   return images = env.filePath + '/' + 'sails-attachments/initbit/ab03a79a031ddec410602a64778911136.png';
      // });
      this.attachmentOptions.formState.images = attachmentData.lessonAttachments;
    }

    // this.imageUrl = env.filePath + '/' + this.updateUser.profilePic;
  }

  initializedlessonsLearntFormData(lessonLearntdata: any) {
    if (lessonLearntdata) {
      this.lessonsLearntForm.patchValue(lessonLearntdata);
    }
  }

  initializedVesselsFormData(vesselsData: any) {
    const vessel: any = [];
    let email: any;
    // Initializing the static Keys
    vesselsData.vessel.forEach((value: any, index: any) => {
      vessel.push(value.vesselIds);
    });
    vesselsData.vesselId = vessel;
    vesselsData.email.forEach((value: any, index2: any) => {
      if (index2 === 0) {
        email = value;
      } else {
        email = email + ',' + value;
      }
    });
    vesselsData.email = email;
    if (vesselsData) {
      this.vesselsForm.patchValue(vesselsData);
    }
  }

  initializeActionFormData(actionData: any) {
    // Initializing the static Keys
    actionData.action.forEach((value: any, index: any) => {
      if (value.dueDate) {
        value.dueDate = this.dateFormatter.transform(new Date(value.dueDate), 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
      }
      if (value.dateCompleted) {
        value.dateCompleted = this.dateFormatter.transform(
          new Date(value.dateCompleted),
          'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''
        );
      }
      this.actions.push({
        natureOfAction: value.natureOfAction,
        dueDate: value.dueDate,
        status: value.status,
        dateCompleted: value.dateCompleted,
      });
    });
    //  actionData. = this.actions;
    if (actionData) {
      this.actionsForm.patchValue(actionData);
    }
  }

  getVesselType() {
    this.lessonsLearntService.getVesselType().subscribe(
      (response) => {
        this.lessonsLearntVesselType = response;
        if (this.lessonsLearntVesselType.statusCode === 200) {
          this.lessonsLearntVesselTypeData = this.lessonsLearntVesselType.data;
        }
      },
      (error) => {}
    );
  }

  getVessel() {
    this.lessonsLearntService.getVessel().subscribe(
      (response) => {
        this.lessonsLearntVesselData = response;
        if (this.lessonsLearntVesselData.statusCode === 200) {
          this.lessonsLearntVesselData = this.lessonsLearntVesselData.data;
        }
      },
      (error) => {}
    );
  }

  // Get All Designation list data
  getAllIncident() {
    this.lessonsLearntService.getAllIncident().subscribe(
      (response) => {
        this.incidentData = response;
        if (this.incidentData.statusCode === 200) {
          this.incidentData = this.incidentData.data;
        }
      },
      (error) => {}
    );
  }
}
