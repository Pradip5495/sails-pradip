import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Logger } from '@core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { LessonsLearntService } from '../../lessons-learnt.service';

const log = new Logger('Fleet-Notification-Acknowledgement-Form');

@Component({
  selector: 'app-lessons-learnt-acknowledgement-form',
  templateUrl: './lessons-learnt-acknowledgement-form.component.html',
  styleUrls: ['./lessons-learnt-acknowledgement-form.component.scss'],
})
export class LessonsLearntAcknowledgementFormComponent implements OnInit {
  acknowledgementFormGroup: FormGroup;
  actionForm: FormGroup;
  applicablevesselId: any;
  participants: any[];
  public alertValue = 'yes';
  public readonly: boolean;
  public userData: any;
  public userList: any;
  public designationList: any;
  public designationData: any;
  error: any;
  public subDetails: any;

  private _checkBoxChecked = false;

  get checkBoxChecked() {
    return this._checkBoxChecked;
  }

  set checkBoxChecked(val: any) {
    this._checkBoxChecked = val;
    this.actionForm.get('status').setValue(this.checkBoxChecked);
  }

  get f() {
    return this.actionForm.controls;
  }

  constructor(
    private route: ActivatedRoute,
    private lessonsLearntService: LessonsLearntService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.applicablevesselId = this.route.snapshot.paramMap.get('acknowledgement');
    this.readonly = true;
    this.getAllUsers();
    this.getDesignation();

    this.createAcknowledgement();
    this.createAction();
  }

  _onAlertOnBoard = (_: any) => {};

  onAddActionField() {
    const action = 'action';
    this.subDetails = this.actionForm.get(action) as FormArray;
    this.subDetails.push(this.actionFields());
  }

  getActionControls() {
    const actionF = this.actionForm.controls.action as FormArray;
    return actionF;
  }

  actionFields(): FormGroup {
    return this.formBuilder.group({
      type: [''],
      natureOfAction: [''],
      dueDate: [''],
      designationId: [''],
      status: [this.checkBoxChecked],
      dateCompleted: [''],
    });
  }

  onAlertOnBoard(event: any) {
    this._onAlertOnBoard(this.alertValue);
    if (this.alertValue === 'yes') {
      this.readonly = true;
    } else {
      this.readonly = !this.readonly;
    }
  }

  // Get All User list data
  getAllUsers() {
    this.lessonsLearntService.getAllUsers().subscribe((response) => {
      this.userList = response;
      if (response) {
        this.userData = response;
      }
    });
  }

  // Get All Designation list data
  getDesignation() {
    this.lessonsLearntService.getDesignation().subscribe((response) => {
      this.designationList = response;
      if (response) {
        this.designationData = response;
      }
    });
  }

  onSubmit() {
    log.debug('AcknowledgementForm: ', this.acknowledgementFormGroup.value);
  }

  onSubmitActionForm() {
    this.actionForm.patchValue(this.acknowledgementFormGroup.value);
    const data = this.actionForm.value;
    const formData = new FormData();

    for (const dataKey in data) {
      if (dataKey === 'action') {
        const datas = data[dataKey];
        let index = 0;
        datas.forEach((element: any) => {
          for (const previewKey in element) {
            if (previewKey === 'dueDate') {
              const convertedDueDate = moment(datas[index][previewKey]).startOf('day');
              const dueDate = moment(convertedDueDate).format('YYYY-MM-DD');
              formData.append(`action[${index}][${previewKey}]`, dueDate);
            } else if (previewKey === 'dateCompleted') {
              const convertedDateCompleted = moment(datas[index][previewKey]).startOf('day');
              const dateCompleted = moment(convertedDateCompleted).format('YYYY-MM-DD');
              formData.append(`action[${index}][${previewKey}]`, dateCompleted);
            } else if (previewKey === 'status') {
              if (datas[index][previewKey] === true) {
                const keyValue = '1';
                formData.append(`action[${index}][${previewKey}]`, keyValue);
              } else {
                const keyValue = '0';
                formData.append(`action[${index}][${previewKey}]`, keyValue);
              }
            } else {
              formData.append(`action[${index}][${previewKey}]`, datas[index][previewKey]);
            }
          }
          index++;
        });
      } else if (dataKey === 'date') {
        const convertedDate = moment(data[dataKey]).startOf('day');
        const date = moment(convertedDate).format('YYYY-MM-DD');
        formData.append(dataKey, date);
      } else if (dataKey === 'participants') {
        const participants = this.actionForm.value.participants;
        log.debug(participants);
        for (let index = 0; index < participants.length; index++) {
          log.debug(participants);
          formData.append(`participants[${index}]`, participants[index]);
        }
      } else if (dataKey === 'emails') {
        const emails = this.actionForm.value.emails;
        const email = emails.split(';');
        for (let index = 0; index < email.length; index++) {
          formData.append(`emails[${index}]`, email[index]);
        }
      } else {
        formData.append(dataKey, data[dataKey]);
      }
    }

    log.debug(this.actionForm.value);

    this.lessonsLearntService.createAcknowledgement(formData).subscribe(
      (response) => {
        log.debug('response:', response);
        if (response) {
          this.router.navigateByUrl('/lessonsLearnt');
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this._snackBar.open(message, '', {
          duration: 4000,
        });
      }
    );
  }

  private createAcknowledgement() {
    this.acknowledgementFormGroup = this.formBuilder.group({
      alertDiscussedOnBoard: ['', Validators.required],
      masterName: ['', Validators.required],
      date: ['', Validators.required],
      participants: ['', Validators.required],
      emails: ['', Validators.required],
      comments: ['', Validators.required],
    });
  }

  private createAction() {
    this.actionForm = this.formBuilder.group({
      applicablevesselId: [this.applicablevesselId],
      alertDiscussedOnBoard: [''],
      masterName: [''],
      date: [''],
      participants: [''],
      emails: [''],
      comments: [''],
      action: new FormArray([this.actionFields()]),
    });
  }
}
