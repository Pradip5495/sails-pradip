import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonsLearntAcknowledgementFormComponent } from './lessons-learnt-acknowledgement-form.component';

describe('LessonsLearntAcknowledgementFormComponent', () => {
  let component: LessonsLearntAcknowledgementFormComponent;
  let fixture: ComponentFixture<LessonsLearntAcknowledgementFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LessonsLearntAcknowledgementFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonsLearntAcknowledgementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
