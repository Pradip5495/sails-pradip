import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonsLearntTableComponent } from './lessons-learnt-table.component';

describe('LessonsLearntTableComponent', () => {
  let component: LessonsLearntTableComponent;
  let fixture: ComponentFixture<LessonsLearntTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LessonsLearntTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonsLearntTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
