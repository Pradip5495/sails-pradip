import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Logger } from '@core';
import { MatTableDataSource } from '@angular/material/table';
import { LessonsLearntService } from '../../lessons-learnt.service';
import { MatPaginator } from '@angular/material/paginator';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { NotificationService } from '@shared/common-service/notification.service';
import { DatePipe } from '@angular/common';

const log = new Logger('Lessons-Learnt');

@Component({
  selector: 'app-lessons-learnt-table',
  templateUrl: './lessons-learnt-table.component.html',
  styleUrls: ['./lessons-learnt-table.component.scss'],
})
export class LessonsLearntTableComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = [
    'incidentId',
    'incidentTitle',
    'incidentType',
    'severityLevel',
    'dateOfSharing',
    'status',
    'confirmation',
    'action',
    'actions',
  ];
  lessonsLearntData: any;
  dataSource: any;
  isLoading = false;
  loadComponent = false;
  isdataSource = false;
  isShow = false;
  vesselTypeData: any;
  public deleteData: any;
  public title: '';
  error: string | undefined;

  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  public array: any;
  public length = 100;
  public pageSizeOptions: number[] = [5, 10, 25, 100];
  public rowData: any;
  public sideBar: any;
  paginationPageSize: number;
  public icons: any;
  public inid: any;
  selectedLessonsLearntID: any;

  filterForm: FormGroup;
  options: FormGroup;
  colorControl = new FormControl('primary');
  fontSizeControl = new FormControl(16, Validators.min(10));
  setTimeControl = new FormControl();

  editType = '';
  domLayout = 'autoHeight';

  columnDefs = [
    {
      headerName: 'INCIDENT ID',
      field: 'incidentId',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'INCIDENT TITLE',
      field: 'incidentTitle',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'INCIDENT TYPE',
      field: 'incidentType',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'SEVERITY LEVEL',
      field: 'severityLevel',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'DATE OF SHARING',
      field: 'dateOfSharing',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'STATUS',
      field: 'status',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: '	CONFIRMATION',
      field: 'confirmation',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'ACTION',
      field: 'action',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'ACTIONS',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deleteLessonLearnt.bind(this),
        onClickEdit: this.editItems.bind(this),
        onClickDetails: this.getLessonLearntDetails.bind(this),
      },
      filter: 'agTextColumnFilter',
    },
  ];
  headerHeight: 100;
  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
  };
  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private lessonsLearntService: LessonsLearntService,
    private notifyService: NotificationService,
    private dateFormatter: DatePipe
  ) {
    this.filterForm = this.formBuilder.group({
      reportId: [''],
      group: [''],
      securityLevel: [''],
      type: [''],
    });
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  onAddForm() {
    this.router
      .navigate(['create'], {
        relativeTo: this.activatedRoute,
      })
      .catch((e) => {
        log.error(e);
      });
  }

  editItems(params: any) {
    if (params) {
      this.router
        .navigate([params.rowData.lessonsLearntId, 'edit'], {
          relativeTo: this.activatedRoute,
        })
        .catch((e) => {
          log.error(e);
        });
    }
  }

  // TODO: Cleanup duplicate code. Make generic Navigation Service
  getLessonLearntDetails(params: any) {
    if (params) {
      this.router
        .navigate([params.rowData.lessonsLearntId, 'details'], {
          relativeTo: this.activatedRoute,
        })
        .catch((e) => {
          log.error(e);
        });
    }
  }

  ngOnInit() {
    this.paginationPageSize = 5;
    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };
    this.getAllLessonsLearntSharing();
    this.sideBar = {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'columns',
          iconKey: 'columns',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
          },
        },
        {
          id: 'customStats',
          labelDefault: 'Vessels',
          labelKey: 'customStats',
          iconKey: 'custom-stats',
          filter: true,
          toolPanel: 'customStatsToolPanel',
        },
      ],
      filter: true,
      hiddenByDefault: true,
      position: 'right',
    };
  }

  onGridReady(params: any) {
    log.debug('reached');
    this.lessonsLearntService.getAllLessonsLearntSharing().subscribe(
      (response) => {
        log.debug('Data:', JSON.stringify(response));
        if (!response) {
          this.isdataSource = true;
        }
        this.rowData = response;
        this.rowData.forEach((element: any) => {
          element.dateOfSharing = this.dateFormatter.transform(new Date(element.dateOfSharing), 'dd-MMM-yyyy');
        });
      },
      (error) => {
        this.notifyService.showError(error);
      }
    );
  }

  deleteLessonLearnt(params: any) {
    this.lessonsLearntService.deleteLessonLearnt(params.rowData.lessonsLearntId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record Not Deleted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  fetchData() {
    this.lessonsLearntService.getAllLessonsLearntSharing().subscribe((data) => {
      this.rowData = data;
    });
  }

  loadMyChildComponent(lessonsLearntId: any) {
    this.loadComponent = true;
    if (this.loadComponent === true) {
      this.displayedColumns = ['vessel', 'actionDue', 'confirmed', 'dateConfirmed', 'dateCompleted', 'actions'];

      // this.getFleetVesselNotificationById(lessonsLearntId);
    }
  }

  getAllLessonsLearntSharing() {
    this.lessonsLearntService.getAllLessonsLearntSharing().subscribe(
      (response) => {
        log.debug(`${response}`);
        this.lessonsLearntData = response;
        if (!response) {
          this.isdataSource = true;
        }
        this.dataSource = new MatTableDataSource(this.lessonsLearntData);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.array = response;
        this.isdataSource = true;
        this.totalSize = this.array.length;
        this.iterator();
      },
      (error) => {}
    );
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.dataSource = part;
  }
}
