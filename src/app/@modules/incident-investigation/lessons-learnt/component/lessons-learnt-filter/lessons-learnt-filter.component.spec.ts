import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonsLearntFilterComponent } from './lessons-learnt-filter.component';

describe('LessonsLearntFilterComponent', () => {
  let component: LessonsLearntFilterComponent;
  let fixture: ComponentFixture<LessonsLearntFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LessonsLearntFilterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonsLearntFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
