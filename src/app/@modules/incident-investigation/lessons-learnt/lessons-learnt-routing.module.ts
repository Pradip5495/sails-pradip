import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LessonsLearntComponent } from './lessons-learnt.component';
import {
  LessonsLearntFormComponent,
  LessonsLearntTableComponent,
} from '@modules/incident-investigation/lessons-learnt/component';
import { extract } from '@app/i18n';
import { LessonsLearntDetailsComponent } from './component/lessons-learnt-details/lessons-learnt-details.component';

const lessonsLearntRoutes: Routes = [
  {
    path: '',
    component: LessonsLearntComponent,
    children: [
      {
        path: '',
        component: LessonsLearntTableComponent,
        data: { title: extract('Lessons Learnt') },
      },
      {
        path: 'create',
        component: LessonsLearntFormComponent,
        data: { title: extract('New Lessons Learnt') },
      },
      {
        path: ':id/edit',
        component: LessonsLearntFormComponent,
        data: { title: extract('Update Lessons Learnt') },
      },
      {
        path: ':id/details',
        component: LessonsLearntDetailsComponent,
        data: { title: extract('Lesson Learnt') },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(lessonsLearntRoutes)],
  exports: [RouterModule],
})
export class LessonsLearntRoutingModule {}
