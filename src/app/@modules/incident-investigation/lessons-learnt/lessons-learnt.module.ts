import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { LessonsLearntComponent } from './lessons-learnt.component';
import { LessonsLearntFilterComponent } from './component/lessons-learnt-filter/lessons-learnt-filter.component';
import { LessonsLearntTableComponent } from './component/lessons-learnt-table/lessons-learnt-table.component';
import { LessonsLearntFormComponent } from './component/lessons-learnt-form/lessons-learnt-form.component';
import { LessonsLearntAcknowledgementFormComponent } from './component/lessons-learnt-acknowledgement-form/lessons-learnt-acknowledgement-form.component';
import { FormlyModule } from '@formly';
import { LessonsLearntRoutingModule } from './lessons-learnt-routing.module';
import { LessonsLearntDetailsComponent } from './component/lessons-learnt-details/lessons-learnt-details.component';

@NgModule({
  declarations: [
    LessonsLearntFilterComponent,
    LessonsLearntTableComponent,
    LessonsLearntFormComponent,
    LessonsLearntComponent,
    LessonsLearntAcknowledgementFormComponent,
    LessonsLearntDetailsComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    FormlyModule,
    LessonsLearntRoutingModule,
  ],
})
export class LessonsLearntModule {}
