import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearMissFilterComponent } from './near-miss-filter.component';

describe('NearMissFilterComponent', () => {
  let component: NearMissFilterComponent;
  let fixture: ComponentFixture<NearMissFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NearMissFilterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearMissFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
