import { AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { NearMissService } from '../../../share/near-miss.service';
import { MatStepper } from '@angular/material/stepper';
import { map } from 'rxjs/operators';
import { FormlyFieldConfig, FormlyFormOptions } from '@formly';
import { MatDialog } from '@angular/material/dialog';
import { NearMissDialogComponent } from '../near-miss-dialog/near-miss-dialog.component';
import { NotificationService } from '@shared/common-service/notification.service';
import { FormControl, FormGroup, ValidationErrors } from '@angular/forms';
import { LocalStorageService } from '@shared/common-service/local-storage.service';

import { ICustomFormlyConfig } from '@types';
import { Subject } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Logger } from '@core';
import { DialogComponent } from '@shared/component';
import { NEAR_MISS_SEVERITY_LEVEL_POINTS, NEAR_MISS_LIKELIHOOD_POINTS } from '../../near-miss.constant';
import { CauseTreeIncidentComponent } from '@modules/incident-investigation/share/cause-tree/cause-tree.component';

const log = new Logger('Near Miss Form');

@Component({
  selector: 'app-near-miss-form',
  templateUrl: './near-miss-form.component.html',
  styleUrls: ['./near-miss-form.component.scss'],
})
export class NearMissFormComponent implements OnInit, AfterViewInit, OnDestroy {
  public activeIndex: any = 1;
  public onDestroy$ = new Subject<void>();
  public getObservedByData = new Subject<any>();
  public getDepartmentByData = new Subject<any>();
  public getProposedByData = new Subject<any>();
  public getStatusData = new Subject<any>();
  public getReportTypeData = new Subject<any>();
  public getActionsData = new Subject<any>();
  public designationListData = new Subject<any>();
  public tsmaElementData = new Subject<any>();
  public tsmaStageData = new Subject<any>();
  public getvesselLocationsData = new Subject<any>();
  public getPortLocationsData = new Subject<any>();
  public getAllVesselsData = new Subject<any>();
  public getUserListData = new Subject<any>();
  public getImpactList = new Subject<any>();
  public getSeverityLevels = new Subject<any>();
  public getProbabilityOfHappening = new Subject<any>();

  public contactList: any;
  public getObservedBy: any = [];
  public getDepartment: any = [];
  // public getImpactList: any = [];
  public getProposedBy: any = [];
  public getVesselLocations: any = [];
  public getAllVessels: any = [];
  public tsmaElement: any = [];
  public getActionData: any;
  public getStatus: any = [];
  public getReportType: any = [];
  public designationList: any = [];
  public tsmaStage: any = [];
  public getUserList: any = [];
  public getFormFeeder: any;
  public createQuickNearMiss: any;
  public title: any;
  public actionList: any;
  error: string | undefined;

  resultantRisk: string;

  immidiateAction: string;
  stopCardFlag: any;
  isLinear = false;
  radioFlag = true;
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('stepper') stepper: MatStepper;
  @ViewChild('immediateActionDialog') immediateActionDialog: DialogComponent;

  overviewCacheKey = 'c_nm_overview';
  analysisCacheKey = 'c_nm_analysis';
  tmsaCacheKey = 'c_nm_tmsa';
  actionCacheKey = 'c_nm_action';

  openImmediateActionDialog: boolean;
  overviewFormFiles: Array<File> = [];
  overViewFormKeys = {
    vesselId: 'vesselId',
    dateOfObservation: 'dateOfObservation',
    timeOfObservation: 'timeOfObservation',
    observedById: 'observedById',
    description: 'description',
    isLatLong: 'isLatLong',
    portId: 'portId',
    vessellocation: 'vessellocation',
    longitudeId: 'longitudeId',
    latitudeId: 'latitudeId',
    obsbyNameId: 'obsbyNameId',
    observerDepartmentId: 'observerDepartmentId',
    nearMissTaskId: 'nearMissTaskId',
    nearMissLocationId: 'nearMissLocationId',
    reportType: 'reportType',
    stopCardIssued: 'stopCardIssued',
    overviewFormFiles: 'overviewFormFiles',
    overviewFiles: 'overviewFiles',
  };
  overViewForm = new FormGroup({});
  overViewModel: any = {};
  overViewformOptions: FormlyFormOptions = {};
  overViewformFields: FormlyFieldConfig[] = [
    // VesselId
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.overViewFormKeys.vesselId,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Vessel',
            options: this.getAllVesselsData,
            valueProp: 'vesselId',
            labelProp: 'vessel',
            required: true,
          },
        },
      ],
    },

    // Basic Information
    {
      template: '<div class="horizontal_dotted_line">Basic Information </div>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'matdatepicker',
          key: this.overViewFormKeys.dateOfObservation,
          templateOptions: {
            label: 'Date of Incident',
            placeholder: 'Date of Incident',
            required: true,
            readonly: true,
            appearance: 'outline',
            datepickerOptions: {
              max: new Date(),
            },
          },
        },
        {
          className: 'flex-1',
          type: 'datetimepicker',
          key: this.overViewFormKeys.timeOfObservation,
          templateOptions: {
            label: 'Time of Incident',
            required: true,
            addonRight: {
              icon: 'access_time',
            },
          },
        },
        {
          key: this.overViewFormKeys.observedById,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            required: true,
            options: this.getObservedByData,
            label: 'Observed by',
            labelProp: 'observedBy',
            valueProp: 'key',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: this.overViewFormKeys.description,
          type: 'textarea',
          className: 'flex-2',
          templateOptions: {
            label: 'Description',
            appearance: 'outline',
            floatLabel: 'never',
            placeholder: 'Placeholder',
            rows: 3,
          },
        },
      ],
    },

    // Quick Submit Button
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          type: 'button',
          className: 'btn-right',
          templateOptions: {
            label: 'Quick Submit',
            text: '',
            btnType: 'info',
            onClick: ($event: any) => {
              const allValue = this.overViewForm.value;
              const formatDateTime = allValue.dateOfObservation.toISOString().split('T')[0];
              const formateTime = new Date(allValue.timeOfObservation).toLocaleTimeString();
              const data = {
                isPartiallySubmitted: true,
                vesselId: allValue.vesselId,
                dateOfObservation: formatDateTime + ' ' + formateTime,
                observedById: allValue.observedById,
                description: allValue.descriptionId,
              };
              this.sendCreateNearMissData(data);
            },
          },
        },
      ],
    },

    // Additional Information
    {
      template: '<div class="horizontal_dotted_line">Additional Information </div>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          template: '<div class="right-label"> Vessel Location: </div>',
        },
        {
          className: 'flex-1',
          type: 'toggle-button',
          key: this.overViewFormKeys.isLatLong,
          defaultValue: '0',
          templateOptions: {
            label1: 'At Sea',
            label2: 'Port',
            onChange: (value: any) => {},
          },
        },
        {
          className: 'flex-6',
          key: this.overViewFormKeys.portId,
          type: 'select',
          defaultValue: '',
          templateOptions: {
            floatLabel: 'never',
            label: 'Port Name',
            // placeholder: 'Port Name',
            appearance: 'outline',
            valueProp: 'portId',
            labelProp: 'name',
            options: this.nearMissService.getPortList<any[]>().pipe(
              map((response) => {
                response.unshift({ portId: '', name: 'Select port name' });
                return response;
              })
            ),
          },
          hideExpression: 'model.isLatLong === "1"',
        },
        {
          key: this.overViewFormKeys.vessellocation,
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            required: true,
            options: this.getvesselLocationsData,
            label: 'Vessel Location',
            labelProp: 'value',
            valueProp: 'viewValue',
          },
          hideExpression: 'model.isLatLong === "1"',
        },
        {
          key: this.overViewFormKeys.longitudeId,
          type: 'input',
          className: 'flex-4',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Longitude',
            type: 'number',
            step: 0.01,
            min: 0,
            max: 99.99,
            maxLength: 5,
            valueProp: 'longitudeId',
            labelProp: 'longitudeType',
            // pattern:"/^\d+(\.\d{1,2})?$/"
          },
          hideExpression: 'model.isLatLong === "0"',
          // validators: {
          //   validation: NearMissFormComponent.maxLengthValidator(6)
          // },
        },
        {
          key: this.overViewFormKeys.latitudeId,
          type: 'input',
          className: 'flex-4',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Latitude',
            type: 'number',
            step: 0.01,
            min: 0,
            max: 99.99,
            valueProp: 'latitudeId',
            labelProp: 'latitudeType',
          },
          hideExpression: 'model.isLatLong === "0"',
        },
        {
          key: this.overViewFormKeys.vessellocation,
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            label: 'port location',
            required: false,
            options: this.getPortLocationsData,
            labelProp: 'value',
            valueProp: 'viewValue',
          },
          hideExpression: 'model.isLatLong === "0"',
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          template: '<div class="right-label"> Observer Details: </div>',
        },

        {
          key: this.overViewFormKeys.obsbyNameId,
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'Observer Name',
            placeholder: 'Observer Name (optional)',
            required: false,
          },
        },
        {
          key: this.overViewFormKeys.observerDepartmentId,
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            options: this.getDepartmentByData,
            label: 'Observer Department',
            labelProp: 'department',
            valueProp: 'key',
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          template: '<div class="right-label"> Task Details: </div>',
        },
        {
          key: this.overViewFormKeys.nearMissTaskId,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Task Involved ',
            options: this.nearMissService.getTaskList(),
            valueProp: 'taskId',
            labelProp: 'task',
            required: true,
          },
        },
        {
          key: this.overViewFormKeys.nearMissLocationId,
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Location ',
            options: this.nearMissService.getLocationList(),
            valueProp: 'locationId',
            labelProp: 'location',
            required: true,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          template: '<div class="right-label"> Report Type*: </div>',
        },
        {
          key: this.overViewFormKeys.reportType,
          type: 'radio',
          defaultValue: '1',
          className: 'flex-2',
          templateOptions: {
            floatLabel: 'never',
            required: true,
            options: this.getReportTypeData,
            labelProp: 'name',
            valueProp: 'name',
          },
        },
        {
          template: '<div class="right-label"> Stop Card Issued: </div>',
        },
        {
          key: this.overViewFormKeys.stopCardIssued,
          type: 'radio',
          defaultValue: 'No',
          className: 'flex-2',
          templateOptions: {
            floatLabel: 'never',
            options: [
              {
                value: 'Yes',
                label: 'Yes',
              },
              {
                value: 'No',
                label: 'No',
              },
            ],
            change: (field: FormlyFieldConfig, event: any) => {
              if (event.value === 'Yes') {
                this.openImmediateActionDialog = false;
                setTimeout(() => {
                  this.openImmediateActionDialog = true;
                }, 50);
              }
            },
          },
          hooks: {
            // onInit: (field) => {
            //   field.formControl.valueChanges
            //     .pipe(
            //       distinctUntilChanged(),
            //       takeUntil(this.onDestroy$),
            //       tap((val) => {
            //         if (val === 'Yes') {
            //           this.radioFlag = false;
            //           this.openDialog((this.stopCardFlag = true));
            //         }
            //         if (val === 'No') {
            //           this.radioFlag = true;
            //         }
            //       })
            //     )
            //     .subscribe();
            // },
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'fileupload',
          className: 'flex-1',
          type: 'sailfile',
          templateOptions: {
            preview: true,
            id: 'report-type',
            onFileChange: this.onOverviewAttachmentUpload.bind(this),
          },
        },
      ],
    },
  ];

  analysisCauseTreeData: any;
  analysisFrom = new FormGroup({});
  analysisModel: any = {};
  analysisFormOptions: FormlyFormOptions = {
    formState: {
      severityDescriptions: {
        Environment: '',
        People: '',
        'Assets/Property/Financial': '',
        'Reputation/Business': '',
      },
    },
  };
  analysisFormFields: FormlyFieldConfig[] = [
    {
      template: '<h4 class="heading-with-underline">IMPACT OR NEAR IMPACT RELATED TO</h4>',
      hideExpression: 'formState.causeTree',
    },
    {
      fieldGroupClassName: 'display-flex',
      id: 'eventImpact',
      fieldGroup: [
        {
          className: 'flex-1',
          key: 'impact',
          type: 'multicheckbox',
          defaultValue: [],
          templateOptions: {
            required: true,
            options: this.getImpactList,
          },
        },
      ],
      hideExpression: 'formState.causeTree',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1 margin-right-3rem',
          key: 'severityLevel',
          type: 'severity-level',
          defaultValue: 'Minor',
          templateOptions: {
            showLabel: true,
            label: 'Severity Level',
            severityLevel: [],
          },
          hideExpression: (model: any) => model.impact.length === 0,
        },
        {
          className: 'flex-3',
          key: 'probablityOfHappening',
          type: 'likelyhood',
          defaultValue: 'Possible',
          templateOptions: {
            impact: 2,
            probability: [],
            label: 'Probability of Happening',
          },
          hideExpression: (model: any) => model.impact.length === 0,
          expressionProperties: {
            'templateOptions.impact': (model: any, formState: any, field: FormlyFieldConfig) => {
              if (model.severityLevel && model.probablityOfHappening) {
                const severityLevelValue = NEAR_MISS_SEVERITY_LEVEL_POINTS[model.severityLevel];
                const likelyHoodPoints = NEAR_MISS_LIKELIHOOD_POINTS[model.probablityOfHappening];
                this.resultantRisk = (severityLevelValue * likelyHoodPoints).toString();
                return severityLevelValue * likelyHoodPoints;
              }
              return 0;
            },
          },
        },
      ],
      hideExpression: 'formState.causeTree',
    },
    // {
    //   fieldGroupClassName: 'display-flex',
    //   fieldGroup: [
    //     {
    //       className: 'flex-1',
    //       key: 'likelyhood',
    //       type: 'likelyhood',
    //       defaultValue: 'Possible',
    //       templateOptions: {
    //         impact: 2,
    //         probability: [],
    //       },
    //       hideExpression: (model: any) => model.eventImpact.length === 0,
    //       expressionProperties: {
    //         'templateOptions.impact': (model: any, formState: any, field: FormlyFieldConfig) => {
    //           if (model.severityLevel && model.likelyhood) {
    //             return SEVERITY_LEVEL_POINTS[model.severityLevel] * LIKELIHOOD_POINTS[model.likelyhood];
    //           }
    //           return 0;
    //         },
    //       },
    //     },
    //   ],
    // },
    {
      template: '<h4 class="heading-with-underline">LOSS POTENTIAL IF NOT CONTROLLED</h4>',
    },
    {
      type: 'button',
      key: 'addNewEditCauseTree',
      templateOptions: {
        type: 'button',
        label: 'Add New/Edit',
        color: 'primary',
        onClick: (event: any, field: FormlyFieldConfig) => {
          field.options.formState.causeTree = !field.options.formState.causeTree;
        },
      },
      hideExpression: 'formState.causeTree',
    },
    {
      type: 'template-component',
      className: 'flex-1',
      templateOptions: {
        component: CauseTreeIncidentComponent,
        props: {
          key: 'date',
          label: 'Date',
          afterCauseTreeSelected: function (data: any) {
            if (data) {
              if (data.typeOfContactOrEvent.length > 0) {
                this.analysisCauseTreeData = [data.typeOfContactOrEvent[0]];
                // this.analysisCauseTreeData = data.typeOfContactOrEvent;
                this.nearMissPayload = {
                  ...this.nearMissPayload,
                  ...{ typeOfContactOrEvent: this.analysisCauseTreeData },
                };
                // this.localStorageService.set('c_nm_causetree', this.analysisCauseTreeData);
              }
            }
          }.bind(this),
        },
      },
      hideExpression: '!formState.causeTree',
    },
  ];

  actionFiles: Array<File> = [];
  actionFormKeys = {
    type: 'type',
    isFleetNotification: 'isFleetNotification',
    state: 'state',
    natureOfAction: 'natureOfAction',
    dateCompleted: 'dateCompleted',
    dueDate: 'dueDate',
    email: 'email',
    userId: 'userId',
    proposedBy: 'proposedBy',
    status: 'status',
    designationId: 'designationId',
    actionFiles: 'actionFiles',
  };
  actionForm = new FormGroup({});
  actionModel: any = {};
  actionFormOptions: FormlyFormOptions = {};
  actionFormFields: FormlyFieldConfig[] = [
    {
      type: 'repeat',
      key: 'action',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add Action +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                key: this.actionFormKeys.state,
                type: 'select',
                className: 'flex-1',
                defaultValue: 'Planned',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Action Type',
                  valueProp: 'id',
                  labelProp: 'name',
                  options: [
                    { id: 'Taken', name: 'Taken' },
                    { id: 'Planned', name: 'Planned' },
                  ],
                  required: true,
                },
              },
              {
                key: this.actionFormKeys.type,
                type: 'select',
                className: 'flex-1',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Type',
                  options: this.getActionsData,
                  valueProp: 'actionType',
                  labelProp: 'key',
                  required: true,
                },
              },
              {
                key: this.actionFormKeys.proposedBy,
                type: 'select',
                className: 'flex-1',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Proposed By ',
                  valueProp: 'key',
                  labelProp: 'proposedBy',
                  options: this.getProposedByData,
                  required: true,
                },
                hideExpression: 'model.state === "Taken"',
              },
              {
                key: this.actionFormKeys.natureOfAction,
                defaultValue: this.immidiateAction ? this.immidiateAction : '',
                type: 'input',
                className: 'flex-2',
                templateOptions: {
                  label: 'Nature of Action',
                  placeholder: 'Nature of Action',
                  required: true,
                },
              },
            ],
          },
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                key: this.actionFormKeys.designationId,
                type: 'select',
                className: 'flex-1',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Responsibility',
                  valueProp: 'designationId',
                  labelProp: 'designation',
                  options: this.getAllDesignation(),
                },
                hideExpression: 'model.state === "Taken"',
              },
              {
                className: 'flex-0',
                type: 'matdatepicker',
                key: this.actionFormKeys.dueDate,
                templateOptions: {
                  label: 'Due Date',
                  placeholder: 'Due Date',
                  readonly: true,
                  appearance: 'outline',
                  required: true,
                  datepickerOptions: {
                    max: new Date(),
                  },
                },
                hideExpression: 'model.state === "Taken"',
              },
              {
                className: 'flex-0',
                type: 'matdatepicker',
                key: this.actionFormKeys.dateCompleted,
                templateOptions: {
                  label: 'Date of Completed',
                  placeholder: 'Date of Completed',
                  readonly: true,
                  appearance: 'outline',
                  datepickerOptions: {
                    max: new Date(),
                  },
                },
                // hideExpression: 'model.state === "Planned"',
              },
              {
                key: this.actionFormKeys.status,
                type: 'select',
                className: 'flex-1',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Status',
                  options: this.getStatusData,
                  valueProp: 'key',
                  labelProp: 'status',
                  required: true,
                },
                hideExpression: 'model.state  === "Taken"',
              },
              {
                key: this.actionFormKeys.email,
                type: 'input',
                className: 'flex-3',
                templateOptions: {
                  label: 'Additional Email Ids',
                  description: 'Separate Email with Comma',
                },
                validators: {
                  validation: ['multipleEmails'],
                },
                hideExpression: 'model.designationId',
              },
            ],
          },
        ],
      },
    },
    {
      template: ' <hr/> ',
    },
    {
      fieldGroupClassName: '',
      fieldGroup: [
        {
          key: 'files',
          className: 'flex-1',
          type: 'sailfile',
          templateOptions: {
            preview: true,
            id: 'overview',
            onFileChange: this.onFilesUpload.bind(this),
          },
        },
      ],
    },
  ];

  tsmaFormKeys = {
    crossRefToTMSA: 'crossRefToTMSA',
    elementId: 'elementId',
    stageId: 'stageId',
    additionalComments: 'additionalComments',
  };
  tsmaForm = new FormGroup({});
  tsmaModel: any = {};
  tsmaFormOptions: FormlyFormOptions = {
    formState: {},
  };
  tmsaFormFieldsTMSA: FormlyFieldConfig[] = [
    {
      key: 'crossRefToTMSA',
      type: 'repeat',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add TSMA +',
        addTextClass: 'add-btn',
        addBtnParentClass: 'text-center',
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                key: this.tsmaFormKeys.elementId,
                type: 'select',
                className: 'flex-1',
                templateOptions: {
                  placeholder: 'Elements',
                  label: 'Elements',
                  valueProp: 'elementId',
                  labelProp: 'elementTitle',
                  options: this.getTsmaElement(),
                  change: (field: FormlyFieldConfig, event: any) => {
                    if (event) {
                      this.nearMissService.getStage(event.value).subscribe((response) => {
                        field.parent.fieldGroup[1].templateOptions.options = response;
                        this.tsmaStage = response;
                      });
                    }
                  },
                  // options: this.tsmaElementData,
                  // change: ($event: any) => {
                  //   this.tsmaForm.patchValue({ tsmaAdd: [{ specificKpi: '' }] });
                  //   const formValue = this.tsmaForm.value;
                  //   const eleMentId = formValue.tsmaAdd[0].elementId;
                  //   this.nearMissService.getStage(eleMentId).subscribe((response) => {
                  //     this.tsmaStage = response;
                  //     this.tsmaStageData.next(this.tsmaStage);
                  //   });
                  // },
                },
              },
              {
                key: this.tsmaFormKeys.stageId,
                type: 'select',
                className: 'flex-0',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Stage',
                  placeholder: 'Stage',
                  valueProp: 'stageId',
                  labelProp: 'stageNo',
                  options: this.tsmaStageData,
                  change: (field: FormlyFieldConfig, event: any) => {
                    if (event) {
                      const selectedStageData = this.tsmaStage.find((stage: any) => stage.stageId === event.value);
                      field.parent.form.patchValue({ specificKpi: selectedStageData.specificKpi });
                      // field.parent.options.formState.specificKpi = selectedStageData.specificKpi;
                    }
                    // const tempFormValue = this.tsmaForm.value;
                    // const tempStageId = tempFormValue.tsmaAdd[0].stageId;
                    // this.tsmaStage.map((e: any) => {
                    //   if (e.stageId === tempStageId) {
                    //     this.tsmaForm.patchValue({ tsmaAdd: [{ specificKpi: e.specificKpi }] });
                    //   }
                    // });
                  },
                },
              },
              // {
              //   type: 'template-component',
              //   className: 'flex-1',
              //   templateOptions: {
              //     component: FormlyFieldTemplateComponent,
              //     props: {
              //       key: 'specificKpi',
              //       label: 'Specific KPI',
              //     },
              //   },
              // },
              {
                className: 'flex-2',
                type: 'input',
                key: 'specificKpi',
                defaultValue: '',
                templateOptions: {
                  rows: 1,
                  placeholder: 'Specific KPI',
                  label: 'Specific KPI',
                  disabled: true,
                },
              },
            ],
          },
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                key: this.tsmaFormKeys.additionalComments,
                className: 'flex-2',
                type: 'input',
                templateOptions: {
                  rows: 1,
                  placeholder: 'Additional Comments',
                  label: 'Additional Comments (optional)',
                },
              },
            ],
          },
        ],
      },
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      shortName: 'Overview',
      title: 'Overview',
      subTitle: ' DESCRIBE WHAT HAPPENED',
      config: {
        model: this.overViewModel,
        fields: this.overViewformFields,
        options: this.overViewformOptions,
        form: this.overViewForm,
      },
      onDiscard: () => this.overViewForm.reset(),
      onSave: this.onSubmitOverview.bind(this, true),
    },
    {
      shortName: 'Analysis',
      title: 'Analysis',
      subTitle: 'ANALYSE WHY THIS HAPPENED',
      config: {
        model: this.analysisModel,
        fields: this.analysisFormFields,
        options: this.analysisFormOptions,
        form: this.analysisFrom,
      },
      onSave: this.onAnalysisSubmit.bind(this, true),
      onDiscard: () => this.analysisFrom.reset(),
    },
    {
      shortName: 'TMSA',
      title: 'Cross Reference to TMSA',
      subTitle: 'ENTER CROSS REFERENCE TO TMSA DATA',
      config: {
        model: this.tsmaModel,
        fields: this.tmsaFormFieldsTMSA,
        options: this.tsmaFormOptions,
        form: this.tsmaForm,
      },
      onSave: this.onSubmitTsma.bind(this, true),
      onDiscard: () => this.tsmaForm.reset(),
    },
    {
      shortName: 'Action',
      title: 'Action',
      subTitle: 'WHAT ACTIONS TAKEN',
      config: {
        model: this.actionModel,
        fields: this.actionFormFields,
        options: this.actionFormOptions,
        form: this.actionForm,
      },
      onSave: this.onSubmitActionForm.bind(this),
      onDiscard: () => this.actionForm.reset(),
    },
  ];

  nearMissPayload: any;

  static maxLengthValidator(maxLength: number) {
    return (control: FormControl): ValidationErrors => {
      if (!control.value) {
        return null;
      }
      if (control.value.length > maxLength) {
        // force the length to
        control.setValue(control.value.substring(0, maxLength));
      }

      return null;
    };
  }

  constructor(
    private nearMissService: NearMissService,
    private dialog: MatDialog,
    private readonly router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private notifyService: NotificationService,
    private localStorageService: LocalStorageService
  ) {
    this.titleService.setTitle('Near Miss');
  }

  ngOnInit(): void {
    const vessels = [{ vesselId: 'test' }, { vesselId: 'test' }];
    let stringd = '';
    vessels.forEach((vessel: any, index) => {
      stringd = stringd + 'vesselId=' + vessel.vesselId + '&';
    });
    // this.getAllDesignation();
    this.getTsmaElement();
    // this.getTsmaStageData();
    this.getVessels();
    // this.getFeederList('near miss');
    // this.localStorageService.setData("Aniket Bhavsar Set local Storage");
    //  const getData = this.localStorageService.getData()
  }

  onOverviewAttachmentUpload(files: Array<File>) {
    /* Adding attachemnts to Payload */
    this.overviewFormFiles = files ? files : this.overviewFormFiles;

    if (this.overviewFormFiles.length > 0) {
      this.nearMissPayload = { ...this.nearMissPayload, ...{ files: this.overviewFormFiles } };
    } else {
      if (this.nearMissPayload.hasOwnProperty(this.overViewFormKeys.overviewFiles)) {
        delete this.nearMissPayload[this.overViewFormKeys.overviewFiles];
      }
    }
  }

  onSubmitOverview(storeDataInCache?: boolean) {
    if (storeDataInCache) {
      this.localStorageService.set(this.overviewCacheKey, this.overViewForm.value);
    }
    let overviewFormData = this.overViewForm.value;

    if (!isNaN(Date.parse(overviewFormData[this.overViewFormKeys.dateOfObservation]))) {
      overviewFormData[this.overViewFormKeys.dateOfObservation] = new Date(
        overviewFormData[this.overViewFormKeys.dateOfObservation]
      ).toISOString();
    }

    // Time of Observation - Non Mandatory Field
    if (!isNaN(Date.parse(overviewFormData[this.overViewFormKeys.timeOfObservation]))) {
      const timeKey = this.overViewFormKeys.timeOfObservation;
      overviewFormData[timeKey] = new Date(overviewFormData[timeKey]).toLocaleTimeString();
    } else {
      overviewFormData = this.checkIfItHasPropertyOrRemove(overviewFormData, this.overViewFormKeys.timeOfObservation);
      this.checkIfItHasPropertyOrRemove(this.nearMissPayload, this.overViewFormKeys.timeOfObservation);
    }

    if (!overviewFormData[this.overViewFormKeys.description]) {
      overviewFormData = this.checkIfItHasPropertyOrRemove(overviewFormData, this.overViewFormKeys.description);
      this.checkIfItHasPropertyOrRemove(this.nearMissPayload, this.overViewFormKeys.description);
    }

    overviewFormData[this.overViewFormKeys.isLatLong] = +overviewFormData[this.overViewFormKeys.isLatLong];

    if (!overviewFormData[this.overViewFormKeys.portId]) {
      overviewFormData = this.checkIfItHasPropertyOrRemove(overviewFormData, this.overViewFormKeys.portId);
      this.checkIfItHasPropertyOrRemove(this.nearMissPayload, this.overViewFormKeys.portId);
    }

    if (!overviewFormData[this.overViewFormKeys.obsbyNameId]) {
      overviewFormData = this.checkIfItHasPropertyOrRemove(overviewFormData, this.overViewFormKeys.obsbyNameId);
      this.checkIfItHasPropertyOrRemove(this.nearMissPayload, this.overViewFormKeys.obsbyNameId);
    }

    if (!overviewFormData[this.overViewFormKeys.observerDepartmentId]) {
      const deptkey = this.overViewFormKeys.observerDepartmentId;
      overviewFormData = this.checkIfItHasPropertyOrRemove(overviewFormData, deptkey);
      this.checkIfItHasPropertyOrRemove(this.nearMissPayload, deptkey);
    }

    const stopCardKey = this.overViewFormKeys.stopCardIssued;
    overviewFormData[stopCardKey] = overviewFormData[stopCardKey] === 'No' ? false : true;

    this.nearMissPayload = { ...this.nearMissPayload, ...overviewFormData };

    // const allValues = this.overViewForm.value;
    // if (allValues.portSea === '1') {
    //   allValues.VessellocationId = 'Open Sea';
    // }
    // const formatDate = allValues.dateOfObservation.toISOString().split('T')[0];
    // const formateTime = new Date(allValues.timeOfObservation).toLocaleTimeString();
    // const data = {
    //   isPartiallySubmitted: isPartial,
    //   vesselId: allValues.vesselId,
    //   dateOfObservation: formatDate + ' ' + formateTime,
    //   observedById: allValues.observedById,
    //   description: allValues.descriptionId,
    //   vesselLocation: allValues.VessellocationId,
    //   portId: allValues.portId,
    //   isLatLong: allValues.portSea,
    //   latLong: allValues.longitudeId + '.' + allValues.latitudeId,
    //   reportType: allValues.ReportTypeId,
    //   stopCardIssued: allValues.StopCardId,
    //   name: allValues.obsbyNameId,
    //   department: allValues.observerDepartmentId,
    //   taskId: allValues.nearMissTaskId,
    //   locationId: allValues.nearMissLocationId,
    // };
    // if (data.isLatLong === '0') {
    //   data.latLong = null;
    // }
    // this.localStorageService.setFormData(data);
    // this.overViewForm.reset();
  }

  onAnalysisSubmit(storeDataInCache?: boolean) {
    if (storeDataInCache) {
      this.localStorageService.set(this.analysisCacheKey, this.analysisFrom.value);
    }

    const analysisFormData = this.analysisFrom.value;
    const impactArray = [];

    if (analysisFormData) {
      for (const [key, value] of Object.entries(analysisFormData.impact)) {
        if (value) {
          impactArray.push(key);
        }
      }
    }

    if (impactArray.length > 0) {
      this.nearMissPayload = {
        ...this.nearMissPayload,
        ...{
          impact: impactArray,
          severityLevel: analysisFormData.severityLevel,
          probablityOfHappening: analysisFormData.probablityOfHappening,
          resultantRisk: this.resultantRisk,
        },
      };
      // this.nearMissPayload = { ...this.nearMissPayload, ...{impact: impactArray.toString()} };
    } else {
      this.checkIfItHasPropertyOrRemove(this.nearMissPayload, 'impact');
    }

    if (!this.nearMissPayload.severityLevel) {
      this.checkIfItHasPropertyOrRemove(this.nearMissPayload, 'severityLevel');
    }

    if (!this.nearMissPayload.probablityOfHappening) {
      this.checkIfItHasPropertyOrRemove(this.nearMissPayload, 'probablityOfHappening');
    }

    if (!this.nearMissPayload.resultantRisk) {
      this.checkIfItHasPropertyOrRemove(this.nearMissPayload, 'resultantRisk');
    }
  }

  onSubmitTsma(storeDataInCache?: boolean) {
    if (storeDataInCache) {
      this.localStorageService.set(this.tmsaCacheKey, this.tsmaForm.value);
    }
    const tsmaFormValues = this.tsmaForm.value;

    const tsmaValues = [...tsmaFormValues.crossRefToTMSA];
    for (let tsmaValue of tsmaValues) {
      if (tsmaValue) {
        if (!tsmaValue[this.tsmaFormKeys.additionalComments]) {
          tsmaValue = this.checkIfItHasPropertyOrRemove(tsmaValue, this.tsmaFormKeys.additionalComments);
        }
        tsmaValue[this.tsmaFormKeys.elementId] = tsmaValue[this.tsmaFormKeys.elementId];
        tsmaValue[this.tsmaFormKeys.stageId] = tsmaValue[this.tsmaFormKeys.stageId];
      }
    }

    this.nearMissPayload = { ...this.nearMissPayload, ...{ crossRefToTMSA: tsmaValues } };

    // const tempLocalData = JSON.parse(this.localStorageService.getFormData());
    // const tsmaFormValues = this.tsmaForm.value.tsmaAdd;
    // tempLocalData.crossRefToTMSA = tsmaFormValues;
    // this.localStorageService.setFormData(tempLocalData);
    // this.tsmaForm.reset();
  }

  onSubmitActionForm() {
    this.onSubmitOverview();
    this.onAnalysisSubmit();
    this.onSubmitTsma();

    const actionFormData = this.actionForm.value;
    this.localStorageService.set(this.actionCacheKey, { ...actionFormData });

    const actions = [...actionFormData.action];
    for (let action of actions) {
      if (action) {
        // Preparing EmailIds Array
        const emailIds: string[] = [];
        const emailsType = typeof action.email;
        if (action.email) {
          if (emailsType === 'string') {
            const emailArray = action.email.split(',');
            emailArray.forEach((value: string) => {
              if (value) {
                emailIds.push(value.trim());
              }
            });
            action.email = [...emailIds];
          }
        } else {
          action = this.checkIfItHasPropertyOrRemove(action, this.actionFormKeys.email);
        }

        if (!isNaN(Date.parse(action[this.actionFormKeys.dateCompleted]))) {
          action[this.actionFormKeys.dateCompleted] = new Date(action[this.actionFormKeys.dateCompleted]).toISOString();
        } else {
          if (action.hasOwnProperty(this.actionFormKeys.dateCompleted)) {
            delete action[this.actionFormKeys.dateCompleted];
          }
        }

        if (!isNaN(Date.parse(action[this.actionFormKeys.dueDate]))) {
          action[this.actionFormKeys.dueDate] = new Date(action[this.actionFormKeys.dueDate]).toISOString();
        } else {
          if (action.hasOwnProperty(this.actionFormKeys.dueDate)) {
            delete action[this.actionFormKeys.dueDate];
          }
        }

        action[this.actionFormKeys.type] = action[this.actionFormKeys.type];
        action.isFleetNotification = 0;
        action[this.actionFormKeys.state] = action[this.actionFormKeys.state];
        action[this.actionFormKeys.natureOfAction] = action[this.actionFormKeys.natureOfAction];

        if (action[this.actionFormKeys.state] === 'Planned') {
          // action[this.actionFormKeys.userId] = action[this.actionFormKeys.designationId];
          action[this.actionFormKeys.proposedBy] = action[this.actionFormKeys.proposedBy];
          action[this.actionFormKeys.status] = action[this.actionFormKeys.status];
        }
      }
    }
    this.nearMissPayload = { ...this.nearMissPayload, ...{ action: actions } };

    if (this.actionForm.valid && this.overViewForm.valid && this.analysisFrom.valid && this.tsmaForm.valid) {
      this.sendCreateNearMissData(this.createFormDataFromObject(this.nearMissPayload));
    } else {
      this.notifyService.showInfo('Some Mandatory Fields are left empty', this.title);
    }
    // const tempLocalData = JSON.parse(this.localStorageService.getFormData());
    // const actionFormValues = this.actionForm.value.action;
    // actionFormValues.map((action: any) => {
    //   if (action.state === 1) {
    //     action.state = 'Planned';
    //   } else {
    //     action.state = 'Taken';
    //   }
    // });
    // tempLocalData.action = actionFormValues;
    // this.actionForm.reset();
  }

  onFilesUpload(files: Array<File>) {
    this.actionFiles = files ? files : this.actionFiles;

    if (this.actionFiles.length > 0) {
      this.nearMissPayload = { ...this.nearMissPayload, ...{ files: this.actionFiles } };
    } else {
      if (this.nearMissPayload.hasOwnProperty('files')) {
        delete this.nearMissPayload.files;
      }
    }
  }

  createFormDataFromObject(payLoad: any): FormData {
    const formData = new FormData();
    formData.append('isPartiallySubmitted', '0');
    let i = 0;
    for (const key in payLoad) {
      if (key === 'stopCardIssued') {
        const value = payLoad[key] ? 'yes' : 'no';
        formData.append(key, value);
      } else if (key === 'files') {
        for (const file of payLoad[key]) {
          formData.append(`${key}`, file);
        }
      } else if (key === 'impact') {
        i = 0;
        for (const data of payLoad[key]) {
          formData.append(`${key}[${i}]`, data);
          i++;
        }
      } else if (key === 'typeOfContactOrEvent') {
        i = 0;
        for (const data of payLoad[key]) {
          formData.append(`${key}[${i}][contact]`, data.contact);
          formData.append(`${key}[${i}][typeOfCntctId]`, data.typeOfCntctId);
          formData.append(`${key}[${i}][typeOfContactCategory]`, data.typeOfContactCategory);
          let j = 0;
          for (const subData1 of data.basicImmediateCause) {
            const basicImmCauseStr = `${key}[${i}][basicImmediateCause][${j}]`;
            formData.append(`${basicImmCauseStr}[basicImmediateCauseId]`, subData1.basicImmediateCauseId);
            formData.append(`${basicImmCauseStr}[furtherExplanation]`, subData1.furtherExplanation);
            formData.append(`${basicImmCauseStr}[immediateCauseExplanation]`, subData1.immediateCauseExplanation);
            let k = 0;
            for (const subData2 of subData1.basicRootCause) {
              const rootcauseString = `${key}[${i}][basicImmediateCause][${j}][basicRootCause][${k}]`;
              formData.append(`${rootcauseString}[basicRootCauseId]`, subData2.basicRootCauseId);
              formData.append(`${rootcauseString}[furtherExplanation]`, subData2.furtherExplanation);
              formData.append(`${rootcauseString}[rootCauseExplanation]`, subData2.rootCauseExplanation);
              let l = 0;
              for (const subData3 of subData2.correctiveActionId) {
                formData.append(`${rootcauseString}[correctiveActionId][${l}]`, subData3);
                l++;
              }
              k++;
            }
            j++;
          }
          i++;
        }
      } else if (key === 'crossRefToTMSA') {
        i = 0;
        for (const tsma of payLoad[key]) {
          formData.append(`${key}[${i}][elementId]`, tsma.elementId);
          formData.append(`${key}[${i}][stageId]`, tsma.stageId);
          if (tsma.additionalComments) {
            formData.append(`${key}[${i}][additionalComments]`, tsma.additionalComments);
          }
          i++;
        }
      } else if (key === 'action') {
        i = 0;
        for (const action of payLoad[key]) {
          formData.append(`${key}[${i}][isFleetNotification]`, '0');
          formData.append(`${key}[${i}][type]`, action.type);
          formData.append(`${key}[${i}][state]`, action.state);
          formData.append(`${key}[${i}][natureOfAction]`, action.natureOfAction);
          if (action.email) {
            let j = 0;
            for (const data of action.email) {
              formData.append(`${key}[${i}][email][${j}]`, data);
              j++;
            }
          }
          if (action.status) {
            formData.append(`${key}[${i}][status]`, action.status);
          }
          if (action.userId) {
            formData.append(`${key}[${i}][userId]`, action.userId);
          }
          if (action.proposedBy) {
            formData.append(`${key}[${i}][proposedBy]`, action.proposedBy);
          }
          if (action.dateCompleted) {
            formData.append(`${key}[${i}][dateCompleted]`, action.dateCompleted);
          }
          if (action.dueDate) {
            formData.append(`${key}[${i}][dueDate]`, action.dueDate);
          }
          if (action.designationId) {
            formData.append(`${key}[${i}][designationId]`, action.designationId);
          }
          i++;
        }
      } else {
        formData.append(key, payLoad[key]);
      }
    }
    return formData;
  }

  overviewReset() {
    this.overViewForm.reset();
  }

  afterImmediateActionDialogClose(value: any) {
    if (value === 'Ok') {
      this.immediateActionDialog.close();
    }
  }

  onDialogOkAction() {
    this.immediateActionDialog.onAfterClosed('Ok');
  }

  openDialog(flag: any): void {
    const dialogRef = this.dialog.open(NearMissDialogComponent, {
      width: '400px',
      data: { stopCardFlag: flag },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.immidiateAction = result;
      this.radioFlag = true;
      if (flag) {
        if (this.immidiateAction) {
          this.actionForm.patchValue({ action: [{ natureOfAction: this.immidiateAction }] });
          this.actionForm.patchValue({ action: [{ typeId: 1 }] });
        } else {
          this.overViewForm.patchValue({ StopCardId: '2' });
        }
      }
    });
  }

  onAddForm() {
    this.router.navigateByUrl('').then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  navigateBack() {
    const navigateBackUrl = '../';
    this.router
      .navigate([navigateBackUrl], {
        relativeTo: this.activatedRoute,
      })
      .catch((e) => {
        log.error(e);
      });
  }
  checkIfItHasPropertyOrRemove(actualData: any, key: string | Array<string>) {
    if (actualData) {
      if (actualData.hasOwnProperty(key) && typeof key === 'string') {
        delete actualData[key];
      }

      // if (typeof key === 'object') {
      //   key.forEach((key) => {
      //     if (actualData.hasOwnProperty(key) && typeof key === 'string') {
      //       delete actualData[key];
      //     }
      //   })
      // }
    }
    return actualData;
  }

  //#region API Call Start

  getNearMissFormFeeder(type: string) {
    this.nearMissService.getFilterNearMiss(type).subscribe(
      (response) => {
        if (response) {
          this.getFormFeeder = response;

          this.getObservedBy = [...this.getFormFeeder.observedBy.map((e: any, i: any) => ({ key: e, observedBy: e }))];
          this.getObservedByData.next(this.getObservedBy);

          this.getDepartment = [...this.getFormFeeder.department.map((e: any, i: any) => ({ key: e, department: e }))];
          this.getDepartmentByData.next(this.getDepartment);

          this.getActionData = [...this.getFormFeeder.actionType.map((e: any, i: any) => ({ key: e, actionType: e }))];
          this.getActionsData.next(this.getActionData);

          this.getReportType = [...this.getFormFeeder.reportType];
          this.getReportTypeData.next(this.getReportType);

          this.getProposedBy = [...this.getFormFeeder.proposedBy.map((e: any, i: any) => ({ key: e, proposedBy: e }))];
          this.getProposedByData.next(this.getProposedBy);

          this.getStatus = [...this.getFormFeeder.status.map((e: any, i: any) => ({ key: e, status: e }))];
          this.getStatusData.next(this.getStatus);

          this.getVesselLocations = this.getFormFeeder.vesselLocation;

          this.getPortLocationsData.next(this.getVesselLocations[0].vesselLocation);
          this.getvesselLocationsData.next(this.getVesselLocations[1].vesselLocation);

          const impactList = [...this.getFormFeeder.impactList.map((e: any, i: any) => ({ label: e, value: e }))];
          this.getImpactList.next(impactList);

          const probablityOfHappening = [...this.getFormFeeder.probablityOfHappening];

          const severityLevel = [
            ...this.getFormFeeder.severityLevel.map((e: any, i: any) => {
              e.id = e.formFeederId;
              return e;
            }),
          ];
          this.formlyConfig[1].config.fields[2].fieldGroup[0].templateOptions.severityLevel = severityLevel.reverse();
          this.formlyConfig[1].config.fields[2].fieldGroup[1].templateOptions.probability = probablityOfHappening;

          // Accessing the Action Fields
          const actionGroup1 = this.formlyConfig[3].config.fields[0].fieldArray.fieldGroup[0];
          const actionGroup2 = this.formlyConfig[3].config.fields[0].fieldArray.fieldGroup[1];
          actionGroup1.fieldGroup[1].templateOptions.options = this.getActionData;
          actionGroup1.fieldGroup[2].templateOptions.options = this.getProposedBy;
          actionGroup2.fieldGroup[3].templateOptions.options = this.getStatus;
        } else {
          // const message = 'Vessel list not found';
          // this.notifyService.showError(message, this.title);
          // this._snackBar.open(message, '', {
          //   duration: 4000
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getAllDesignation() {
    return this.nearMissService.getDesignation();
  }

  getTsmaElement() {
    return this.nearMissService.getElements().pipe(map((data) => data[0]));
    // this.nearMissService.getElements().subscribe(
    //   (response) => {
    //     if (response) {
    //       this.tsmaElement = response[0];
    //       this.tsmaElementData.next(this.tsmaElement);
    //       // this.nearMissCacheService.setElementData(this.elementData);
    //     } else {
    //       const message = 'element data not found';
    //       this.notifyService.showError(message, this.title);
    //     }
    //   },
    //   (error) => {
    //     this.error = error;
    //     const message = error;
    //     this.notifyService.showError(message, this.title);
    //   }
    // );
  }

  getVessels() {
    this.nearMissService.getAllVessels().subscribe(
      (response) => {
        if (response) {
          this.getAllVessels = response;
          this.getAllVesselsData.next(this.getAllVessels);
          // this.nearMissCacheService.setElementData(this.elementData);
        } else {
          const message = 'element data not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  sendCreateNearMissData(data: any) {
    this.nearMissService.createNearMiss(data).subscribe(
      (response) => {
        this.createQuickNearMiss = response;
        if (this.createQuickNearMiss.statusCode === 201) {
          const message = this.createQuickNearMiss.message;
          this.notifyService.showSuccess(message, this.title);
          this.localStorageService.clear(this.overviewCacheKey);
          this.localStorageService.clear(this.analysisCacheKey);
          this.localStorageService.clear(this.tmsaCacheKey);
          this.localStorageService.clear(this.actionCacheKey);
          this.router.navigate(['/incident/near-miss']);
        } else {
          const message = this.createQuickNearMiss.error.message;
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        log.debug('message3: ', message);
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // getFeederList(id: string) {
  //   this.nearMissService.getFormFedderList(id).subscribe(
  //     (response) => {
  //       if (response) {
  //         this.getActionData = response.status;

  //         this.getActionData = [...this.getActionData.map((e: any, i: any) => ({ key: e, actionId: e }))];

  //         this.getActionsData.next(this.getActionData);
  //       } else {
  //         const message = 'stage not found';
  //         this.notifyService.showError(message, this.title);
  //       }
  //     },
  //     (error) => {
  //       this.error = error;
  //       const message = error;
  //       this.notifyService.showError(message, this.title);
  //     }
  //   );
  // }

  //#endregion API Call End

  ngAfterViewInit() {
    this.getNearMissFormFeeder('near miss');
    const overviewCacheddata = this.localStorageService.get(this.overviewCacheKey);
    const analysisCacheddata = this.localStorageService.get(this.analysisCacheKey);
    const tmsaCacheddata = this.localStorageService.get(this.tmsaCacheKey);
    const actionCacheddata = this.localStorageService.get(this.actionCacheKey);
    if (overviewCacheddata) {
      this.overViewForm.patchValue(overviewCacheddata);
    }
    if (analysisCacheddata) {
      this.analysisFrom.patchValue(analysisCacheddata);
    }
    if (tmsaCacheddata) {
      this.tsmaForm.patchValue(tmsaCacheddata);
    }
    if (actionCacheddata) {
      // this.actionModel = {
      //   ...this.actionModel,
      //   action: actionCacheddata,
      // };
      this.actionForm.patchValue(actionCacheddata);
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
