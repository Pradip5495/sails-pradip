import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearMissFormComponent } from './near-miss-form.component';

describe('NearMissFormComponent', () => {
  let component: NearMissFormComponent;
  let fixture: ComponentFixture<NearMissFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NearMissFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearMissFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
