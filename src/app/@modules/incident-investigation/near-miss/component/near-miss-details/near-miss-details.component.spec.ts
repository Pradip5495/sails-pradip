import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearMissDetailsComponent } from './near-miss-details.component';

describe('NearMissDetailsComponent', () => {
  let component: NearMissDetailsComponent;
  let fixture: ComponentFixture<NearMissDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NearMissDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearMissDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
