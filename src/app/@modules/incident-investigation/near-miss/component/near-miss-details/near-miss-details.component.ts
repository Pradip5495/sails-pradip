import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NearMissService } from './../../share/near-miss.service';
import { NotificationService } from '@shared/common-service';
import { environment } from '../../../../../../environments/environment';
import { Logger } from '@core';

const log = new Logger('NearMissDetailsComponent');
@Component({
  selector: 'app-near-miss-details',
  templateUrl: './near-miss-details.component.html',
  styleUrls: ['./near-miss-details.component.css'],
})
export class NearMissDetailsComponent implements OnInit {
  public nid: any;
  public nearmissData: any = [];
  error: string | undefined;
  public impactList: any = [];
  public impactValue2: any = [];
  public severityLevel: any = [];
  public stageArray: any = [];
  public severityArray: any = [];
  public probability: any = [];
  public impactLabel: any;
  public title = '';
  public indexing: any = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'];
  public API_URL = environment.filePath;
  constructor(
    private route: ActivatedRoute,
    private nearMissService: NearMissService,
    private notifyService: NotificationService
  ) {}

  ngOnInit() {
    log.debug('details reached');
    log.debug('details params', this.route.params);
    const paramVlaue = 'value';
    if (this.route.params[paramVlaue].nearmissId) {
      this.nid = this.route.params[paramVlaue].nearmissId;
      log.debug('id', this.nid);
      this.getNearMissDetails(this.nid);
    }
    this.getSeverityLevel('Severity Level');
    this.getProbability('Probablity Of Happening');
  }
  getNearMissDetails(nearmissId: any) {
    log.debug('near miss', nearmissId);
    this.nearMissService.getNearmissDetails(nearmissId).subscribe(
      (response) => {
        log.debug('response', response);
        this.nearmissData = response;
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }
  setImpactType(type: any, impactList: any) {
    if (impactList) {
      const impactOn = impactList;
      impactOn.forEach((element: any) => {
        this.impactValue2.push(element.eventImpact);
      });

      const availableType = this.impactValue2.indexOf(type);
      if (availableType > -1) {
        return true;
      } else {
        return false;
      }
    }
  }
  getSeverityLevel(type: any) {
    this.nearMissService.getFormFeed(type).subscribe(
      (response) => {
        if (response) {
          this.severityArray = response;
          this.stageArray = [];
          this.severityArray.forEach((element: any) => {
            if (element.module === 'near miss') {
              this.stageArray.push(element);
            }
          });
          this.severityLevel = this.stageArray;
        } else {
          const message = 'severity level not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getProbability(type: any) {
    this.nearMissService.getFormFeed(type).subscribe(
      (response) => {
        if (response) {
          this.probability = response;
        } else {
          const message = 'probability list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getImpactActiveClass(resultantRisk: any) {
    if (resultantRisk < 5) {
      this.impactLabel = 'Low';
      return 'normal';
    } else if (resultantRisk > 4 && resultantRisk < 10) {
      this.impactLabel = 'Medium';
      return 'medium';
    } else {
      this.impactLabel = 'High';
      return 'extreme';
    }
  }
  handleImgError(ev: any) {
    const source = ev.srcElement;
    const imgSrc = `assets/file.png`;
    source.src = imgSrc;
  }
}
