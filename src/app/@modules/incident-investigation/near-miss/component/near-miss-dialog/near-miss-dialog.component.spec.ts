import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearMissDialogComponent } from './near-miss-dialog.component';

describe('NearMissDialogComponent', () => {
  let component: NearMissDialogComponent;
  let fixture: ComponentFixture<NearMissDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NearMissDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearMissDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
