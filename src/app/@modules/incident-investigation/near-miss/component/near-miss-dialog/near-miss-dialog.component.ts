import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
export interface DialogData {
  natureOfAction: string;
  name: string;
  stopCardFlag: boolean;
}

@Component({
  selector: 'app-near-miss-dialog',
  templateUrl: './near-miss-dialog.component.html',
  styleUrls: ['./near-miss-dialog.component.css'],
})
export class NearMissDialogComponent implements OnInit {
  @Input() dialogConfig: string;

  public flag = true;

  constructor(
    public dialogRef: MatDialogRef<NearMissDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.flag = data.stopCardFlag;
  }

  ngOnInit(): void {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  isImmediateActionDialog() {
    return this.dialogConfig === 'immediateAction';
  }

  isActionCommentDialog() {
    return this.dialogConfig === 'actionComment';
  }
}
