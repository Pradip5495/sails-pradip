import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearMissUpdateFormComponent } from './near-miss-update-form.component';

describe('NearMissUpdateFormComponent', () => {
  let component: NearMissUpdateFormComponent;
  let fixture: ComponentFixture<NearMissUpdateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NearMissUpdateFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearMissUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
