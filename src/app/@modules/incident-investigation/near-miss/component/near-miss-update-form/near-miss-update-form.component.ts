import { AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NearMissService } from '../../share/near-miss.service';
import { VesselService } from '@shared/service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

import { NearMissDialogComponent } from './../near-miss-dialog/near-miss-dialog.component';
import { MatStepper } from '@angular/material/stepper';
import { distinctUntilChanged, takeUntil, tap } from 'rxjs/operators';
import { ICustomFormlyConfig } from '@types';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { Subject } from 'rxjs';

const VesselLocations = [{ id: '1', VessellocName: 'Open Sea' }];

@Component({
  selector: 'app-near-miss-update-form',
  templateUrl: './near-miss-update-form.component.html',
  styleUrls: ['./near-miss-update-form.component.scss'],
})
export class NearMissUpdateFormComponent implements OnInit, AfterViewInit, OnDestroy {
  public getObservedByData = new Subject<any>();
  public getDepartmentByData = new Subject<any>();
  public getProposedByData = new Subject<any>();
  public getStatusData = new Subject<any>();
  public getReportTypeData = new Subject<any>();
  public getActionsData = new Subject<any>();
  public designationListData = new Subject<any>();
  public tsmaElementData = new Subject<any>();
  public tsmaStageData = new Subject<any>();
  public getUserListData = new Subject<any>();
  public activeIndex: any = 1;
  public nearMissId: any;
  public nearmissData: any;
  public contactList: any;
  public getObservedBy: any = [];
  public getDepartment: any = [];
  public getImpactList: any = [];
  public getProposedBy: any = [];
  public tsmaElement: any = [];
  public getActionData: any;
  public getStatus: any = [];
  public getReportType: any = [];
  public designationList: any = [];
  public tsmaStage: any = [];
  public getUserList: any = [];
  public getFormFeeder: any;
  public actionList: any;
  error: string | undefined;
  public onDestroy$ = new Subject<void>();
  immidiateAction: string;
  stopCardFlag: any;
  isLinear = false;
  radioFlag = true;
  @Output() pageSubTitle: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('stepper') stepper: MatStepper;

  overViewForm = new FormGroup({});
  overViewModel: any = { set: 'test' };
  overViewformOptions: FormlyFormOptions = {};
  // model = new CreateNearMissModel();
  overViewformFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'vesselId',
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline', // vesselId
            floatLabel: 'never',
            label: 'Type of Vessel',
            options: this.vesselService.getVessels(),
            valueProp: 'vesselId',
            labelProp: 'vessel',
            required: true,
          },
        },
        {
          className: 'flex-1',
          type: 'toggle-button',
          key: 'portSea',
          defaultValue: '0',
          templateOptions: {
            label1: 'At Sea',
            label2: 'Port',
          },
        },
        {
          className: 'flex-6',
          key: 'portId',
          type: 'select',
          defaultValue: '',
          templateOptions: {
            floatLabel: 'never',
            label: 'Port Name',
            // placeholder: 'Port Name',
            appearance: 'outline',
            valueProp: 'portId',
            labelProp: 'name',
            // options: this.nearMissService.getPortList<any[]>().pipe(
            //   map((response) => {
            //     response.unshift({ portId: '', name: 'Select port name' });
            //     return response;
            //   })
            // ),
          },
          hideExpression: 'model.portSea == 1',
        },
        {
          key: 'longitudeId',
          type: 'input',
          className: 'flex-4',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Longitude',
            type: 'number',
            step: 0.01,
            min: 0,
            max: 99.99,
            maxLength: 5,
            valueProp: 'longitudeId',
            labelProp: 'longitudeType',
            // pattern:"/^\d+(\.\d{1,2})?$/"
          },
          hideExpression: 'model.portSea == 0',
          // validators: {
          //   validation: NearMissFormComponent.maxLengthValidator(6)
          // },
        },
        {
          key: 'latitudeId',
          type: 'input',
          className: 'flex-4',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Latitude',
            type: 'number',
            step: 0.01,
            min: 0,
            max: 99.99,
            valueProp: 'latitudeId',
            labelProp: 'latitudeType',
          },
          hideExpression: 'model.portSea == 0',
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'matdatepicker',
          key: 'dateOfIncident',
          templateOptions: {
            label: 'Date of Incident',
            placeholder: 'Date of Incident',
            required: true,
            readonly: true,
            appearance: 'outline',
            datepickerOptions: {
              max: new Date(),
            },
          },
        },
        {
          className: 'flex-1',
          type: 'datetimepicker',
          key: 'timeOfIncident',
          templateOptions: {
            label: 'Time of Incident',
            required: true,
            addonRight: {
              icon: 'access_time',
            },
          },
        },
        {
          key: 'ObservedbyId',
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            required: true,
            options: this.getObservedByData,
            label: 'Observed by',
            labelProp: 'ObservedbyId',
            valueProp: 'id',
          },
        },
        {
          key: 'obsbyNameId',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'name',
            placeholder: 'Name (optional)',
            required: false,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'VessellocationId',
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            required: true,
            options: VesselLocations,
            label: 'Vessel Location',
            labelProp: 'VessellocName',
            valueProp: 'id',
          },
        },
        {
          key: 'DepartmentId',
          type: 'select',
          className: 'flex-2',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            required: true,
            options: this.getDepartmentByData,
            label: 'Department Location',
            labelProp: 'DeptName',
            valueProp: 'id',
          },
        },
        {
          key: 'reportId',
          type: 'input',
          className: 'flex-2',
          templateOptions: {
            label: 'report-id',
            placeholder: 'Report',
            required: false,
          },
        },
      ],
    },
    {
      template: '<div><strong>Report Type:</strong></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'reportByTypeId',
          type: 'radio',
          className: 'flex-2',
          templateOptions: {
            floatLabel: 'never',
            required: true,
            options: [
              {
                value: '1',
                label: 'Unsafe Act',
                selected: true,
              },
              {
                value: '2',
                label: 'Unsafe Condition',
              },
              {
                value: '3',
                label: 'Significant Near Miss',
              },
              {
                value: '4',
                label: 'Near Miss',
              },
            ],
          },
        },
      ],
    },
    {
      template: '<div> <h4 class="heading-with-underline">DESCRIPTION OF THE NEAR MISS</h4></div><hr/>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'descriptionId',
          type: 'textarea',
          className: 'flex-2',
          templateOptions: {
            label: 'Description',
            appearance: 'outline',
            floatLabel: 'never',
            placeholder: 'Placeholder',
            rows: 3,
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'nearmissTaskId',
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Task Involved ',
            options: this.nearMissService.getTaskList(),
            valueProp: 'taskId',
            labelProp: 'task',
            required: true,
          },
        },
        {
          key: 'nearmissLocationId',
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Location ',
            options: this.nearMissService.getLocationList(),
            valueProp: 'locationId',
            labelProp: 'location',
            required: true,
          },
        },
      ],
    },
    {
      template: '<h4>Stop Card Issued</h4>',
    },
    {
      defaultValue: '2',
      key: 'StopCardId',
      type: 'radio',
      className: 'flex-2',
      templateOptions: {
        floatLabel: 'never',
        options: [
          {
            value: '1',
            label: 'Yes',
          },
          {
            value: '2',
            label: 'No',
          },
        ],
      },
      hooks: {
        onInit: (field) => {
          // const form = field.formControl;

          field.formControl.valueChanges
            .pipe(
              distinctUntilChanged(),
              takeUntil(this.onDestroy$),
              tap((val) => {
                if (this.radioFlag && val === '1') {
                  this.radioFlag = false;
                  this.openDialog((this.stopCardFlag = true));
                }
                if (this.radioFlag && val === '2') {
                  this.radioFlag = true;
                }
              })
            )
            .subscribe();
        },
      },
    },

    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'sailfile',
          key: 'fileupload',
          templateOptions: {
            preview: true,
            id: 'report-type',
            onFileChange: (files: File[]) => {
              // Handle your files here
            },
          },
        },
      ],
    },
  ];

  analysisFrom = new FormGroup({});
  analysisModel: any = {};
  analysisFormOptions: FormlyFormOptions = {};

  formFieldsAnalysis: FormlyFieldConfig[] = [
    {
      template: '<h4 class="heading-with-underline">IMPACT OR NEAR IMPACT RELATED TO</h4>',
    },
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'people',
          type: 'checkbox',
          className: 'flex-1',
          templateOptions: {
            label: 'People',
            floatLabel: 'never',
          },
        },
        {
          key: 'Enviorment',
          type: 'checkbox',
          className: 'flex-1',
          templateOptions: {
            label: 'Enviorment',
            floatLabel: 'never',
          },
        },
        {
          key: 'Asset-Property',
          type: 'checkbox',
          className: 'flex-1',
          templateOptions: {
            label: 'Asset/Property',
            floatLabel: 'never',
          },
        },
        {
          key: 'Process-Countinity',
          type: 'checkbox',
          className: 'flex-1',
          templateOptions: {
            label: 'Process Countinity',
            floatLabel: 'never',
          },
        },
        {
          key: 'Security',
          type: 'checkbox',
          className: 'flex-2',
          templateOptions: {
            label: 'Security',
            floatLabel: 'never',
          },
        },
      ],
    },
    {
      template: '<h4 class="heading-with-underline">LOSS POTENTIAL IF NOT CONTROLLED</h4>',
    },
  ];
  actionForm = new FormGroup({});
  actionModel: any = {};
  actionFormOptions: FormlyFormOptions = {};

  formFieldsAction: FormlyFieldConfig[] = [
    {
      type: 'repeat',
      key: 'action',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add Action +',
        addTextClass: 'add-btn',
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                key: 'proposedId',
                type: 'select',
                className: 'flex-1',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Proposed By ',
                  valueProp: 'key',
                  labelProp: 'proposedBy',
                  options: this.getProposedByData,
                  required: true,
                },
              },

              {
                key: 'typeId',
                type: 'select',
                className: 'flex-0',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Type',
                  options: this.getActionsData,
                  valueProp: 'actionId',
                  labelProp: 'type',
                  required: true,
                },
              },
              {
                key: 'natureAction',
                type: 'input',
                className: 'flex-4',
                templateOptions: {
                  label: 'Nature of Action',
                  placeholder: 'Nature of Action',
                  required: false,
                },
              },
            ],
          },

          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                key: 'responsibilityId',
                type: 'select',
                className: 'flex-1',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Responsibility',
                  valueProp: 'designationId',
                  labelProp: 'designation',
                  options: this.designationListData,
                },
              },
              {
                key: 'emailId',
                type: 'input',
                className: 'flex-1',
                templateOptions: {
                  label: 'email',
                  placeholder: 'Email',
                  required: false,
                },
              },
              {
                className: 'flex-1',
                type: 'matdatepicker',
                key: 'dueDate',
                templateOptions: {
                  label: 'Due Date',
                  placeholder: 'Due Date',
                  readonly: true,
                  appearance: 'outline',
                  datepickerOptions: {
                    max: new Date(),
                  },
                },
              },
            ],
          },
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                className: 'flex-1',
                type: 'matdatepicker',
                key: 'dateOfCompleted',
                templateOptions: {
                  label: 'Date of Completed',
                  placeholder: 'Date of Completed',
                  readonly: true,
                  appearance: 'outline',
                  datepickerOptions: {
                    max: new Date(),
                  },
                },
              },
              {
                key: 'statusId',
                type: 'select',
                className: 'flex-4',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Status',
                  options: this.getStatusData,
                  valueProp: 'key',
                  labelProp: 'status',
                },
              },
            ],
          },
        ],
      },
    },
    {
      template: '<hr/>',
    },
    {
      fieldGroupClassName: '',
      fieldGroup: [
        {
          className: 'flex-1',
          type: 'sailfile',
          key: 'fileupload',
          templateOptions: {
            preview: true,
            id: 'overview',
            onFileChange: (files: File[]) => {
              // Handle your files here
            },
          },
        },
      ],
    },
  ];

  tsmaForm = new FormGroup({});
  tsmamodel: any = { tsmaAdd: [] };
  tsmaFormOptions: FormlyFormOptions = {};

  formFieldsTMSA: FormlyFieldConfig[] = [
    {
      key: 'tsmaAdd',
      type: 'repeat',
      fieldGroupClassName: 'display-flex',
      templateOptions: {
        addText: 'Add TSMA +',
        addTextClass: 'add-btn',
      },
      fieldArray: {
        fieldGroupClassName: '',
        fieldGroup: [
          {
            fieldGroupClassName: 'display-flex',
            fieldGroup: [
              {
                type: 'select',
                key: 'elementId',
                templateOptions: {
                  placeholder: 'Elements',
                  label: 'Elements',
                  valueProp: 'elementId',
                  labelProp: 'elementTitle',
                  options: this.tsmaElementData,
                },
              },
              {
                type: 'select',
                key: 'stageId',
                className: 'flex-0',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Stage',
                  placeholder: 'Stage',
                  valueProp: 'stageId',
                  labelProp: 'stageNo',
                  options: this.tsmaStageData,
                },
              },
              {
                type: 'select',
                key: 'specificKpiId',
                className: 'flex-1',
                templateOptions: {
                  appearance: 'outline',
                  floatLabel: 'never',
                  label: 'Specific KPI',
                  placeholder: 'Specific KPI',
                  valueProp: 'id',
                  labelProp: 'name',
                  options: [
                    { id: 1, name: 'KPI 1' },
                    { id: 2, name: 'KPI 2' },
                    { id: 2, name: 'KPI 3' },
                  ],
                },
              },
              {
                className: 'flex-2',
                type: 'input',
                key: 'commentId',
                templateOptions: {
                  rows: 1,
                  placeholder: 'Additional Comments',
                  label: 'Additional Comments (optional)',
                },
              },
            ],
          },
        ],
      },
    },
  ];

  communicationForm = new FormGroup({});
  communicationModel: any = {};
  communicationOptions: FormlyFormOptions = {};

  communicationFormFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'communicatoinById',
          type: 'select',
          className: 'flex-1',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Communication By ',
            valueProp: 'userId',
            labelProp: 'firstname',
            options: this.getUserListData,
            required: true,
          },
        },
        {
          className: 'flex-2',
          type: 'input',
          key: 'commentId',
          templateOptions: {
            rows: 1,
            placeholder: 'Additional Comments',
            label: 'Additional Comments (optional)',
          },
        },
      ],
    },
  ];

  formlyConfig: ICustomFormlyConfig[] = [
    {
      shortName: 'Overview',
      title: 'Description of the Incident',
      subTitle: ' DESCRIBE WHAT HAPPENED',
      config: {
        model: this.overViewModel,
        fields: this.overViewformFields,
        options: this.overViewformOptions,
        form: this.overViewForm,
      },
      onDiscard: () => this.overViewForm.reset(),
      onPartialSave: this.onSubmit.bind(this),
      onSave: this.onSubmit.bind(this),
    },
    {
      shortName: 'Analysis',
      title: 'Analysis',
      subTitle: 'ANALYSE WHY THIS HAPPENED',
      config: {
        model: this.analysisModel,
        fields: this.formFieldsAnalysis,
        options: this.analysisFormOptions,
        form: this.analysisFrom,
      },
      onSave: this.onSubmit.bind(this),
      onDiscard: () => this.analysisFrom.reset(),
    },
    {
      shortName: 'Action',
      title: 'Action',
      subTitle: 'WHAT ACTIONS TAKEN',
      config: {
        model: this.actionModel,
        fields: this.formFieldsAction,
        options: this.actionFormOptions,
        form: this.actionForm,
      },
      onSave: this.onSubmit.bind(this),
      onDiscard: () => this.actionForm.reset(),
    },
    {
      shortName: 'TMSA',
      title: 'Cross Reference to TMSA',
      subTitle: 'ENTER CROSS REFERENCE TO TMSA DATA',
      config: {
        model: this.tsmamodel,
        fields: this.formFieldsTMSA,
        options: this.tsmaFormOptions,
        form: this.tsmaForm,
      },
      onSave: this.onSubmit.bind(this),
      onDiscard: () => this.tsmaForm.reset(),
    },
    {
      shortName: 'Communication',
      title: 'Communication',
      subTitle: 'Communication',
      config: {
        model: this.communicationModel,
        fields: this.communicationFormFields,
        options: this.communicationOptions,
        form: this.communicationForm,
      },
      onSave: this.onSubmit.bind(this),
      onDiscard: () => this.tsmaForm.reset(),
    },
  ];

  constructor(
    private readonly vesselService: VesselService,
    private nearMissService: NearMissService,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    const paramValue = 'value';
    if (this.route.params[paramValue].id) {
      this.nearMissId = this.route.params[paramValue].id;
      this.getNearMissById(this.nearMissId);
    }
    this.getDesignation();
    // this.getNearMissVessel();
    this.getTsmaElement();
    this.getTsmaStageData();
    this.getAllUsers();
  }

  getNearMissById(nearmissId: any) {
    this.nearMissService.getNearmissDetails(nearmissId).subscribe(
      (response) => {
        // console.log(response);
        this.nearmissData = response;
        this.getActionData = [...this.nearmissData.action];
        this.getActionsData.next(this.getActionData);

        this.overViewForm.patchValue({
          vesselId: this.nearmissData.vesselId,
          VessellocationId: this.nearmissData.vesselLocation,
          observedByStatus: this.nearmissData.observedById,
          reportByTypeId: this.nearmissData.reportType,
          nearMissTaskId: this.nearmissData.nearMissTaskId,
          descriptionId: this.nearmissData.description,
          nearmissLocationId: this.nearmissData.locationId,
          nearmissTaskId: this.nearmissData.taskId,
          DepartmentId: this.nearmissData.department,
        });
      },
      (error) => {
        this.error = error;
        // this.notifyService.showError(message, this.title);
      }
    );
  }

  getNearMissFormFeeder(type: string) {
    this.nearMissService.getFilterNearMiss(type).subscribe(
      (response) => {
        if (response) {
          this.getFormFeeder = response;
          this.getObservedBy = [...this.getFormFeeder.observedBy.map((e: any, i: any) => ({ key: e, observedBy: e }))];
          this.getObservedByData.next(this.getObservedBy);

          this.getDepartment = [...this.getFormFeeder.department.map((e: any, i: any) => ({ key: e, department: e }))];
          this.getDepartmentByData.next(this.getDepartment);
          this.getReportType = [...this.getFormFeeder.reportType];
          this.getReportTypeData.next(this.getReportType);
          this.getProposedBy = [...this.getFormFeeder.proposedBy.map((e: any, i: any) => ({ key: e, proposedBy: e }))];
          this.getProposedByData.next(this.getProposedBy);
          this.getStatus = [...this.getFormFeeder.status.map((e: any, i: any) => ({ key: e, status: e }))];
          this.getStatusData.next(this.getStatus);
        } else {
          // const message = 'Vessel list not found';
          // this.notifyService.showError(message, this.title);
          // this._snackBar.open(message, '', {
          //   duration: 4000
        }
      },
      (error) => {
        this.error = error;
        // this.notifyService.showError(message, this.title);
      }
    );
  }

  getDesignation() {
    this.nearMissService.getDesignation().subscribe(
      (response) => {
        if (response) {
          this.designationList = response;
          this.designationList.forEach((element: any) => {
            const designation = {
              uid: element.duid,
              name: element.designation,
            };
            this.designationList.push(designation);
          });
          this.designationListData.next(this.designationList);
        } else {
          const message = 'Designation data not found';
          this._snackBar.open(message, '', {
            duration: 4000,
          });
        }
      },
      (error) => {
        this.error = error;
        // this.notifyService.showError(message, this.title);
      }
    );
  }

  getTsmaElement() {
    this.nearMissService.getElements().subscribe(
      (response) => {
        if (response) {
          this.tsmaElement = response[0];
          this.tsmaElementData.next(this.tsmaElement);
          // this.nearMissCacheService.setElementData(this.elementData);
        } else {
          // this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        // this.notifyService.showError(message, this.title);
      }
    );
  }

  getTsmaStageData() {
    this.nearMissService.getStage().subscribe(
      (response) => {
        if (response) {
          this.tsmaStage = response[0];
          this.tsmaStageData.next(this.tsmaStage);

          // this.nearMissCacheService.setStageData(response[0]);
        } else {
          // this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        // this.notifyService.showError(message, this.title);
      }
    );
  }

  getAllUsers() {
    this.nearMissService.getAllUsers().subscribe(
      (response) => {
        if (response) {
          this.getUserList = response;
          this.getUserList.forEach((element: any) => {
            const user = {
              uid: element.userId,
              name: element.firstname,
            };
            this.getUserList.push(user);
          });
          this.getUserListData.next(this.getUserList);

          // console.log('Userr list', this.getUserList);
        } else {
          // this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        // this.notifyService.showError(message, this.title);
      }
    );
  }

  onSubmit(formValue: any, isPartial: boolean) {
    // const data = formValue;
    // const latitude = formValue.latitude;
    // const longitude = formValue.longitude;
  }

  overviewReset() {
    this.overViewForm.reset();
  }

  openDialog(flag: any): void {
    const dialogRef = this.dialog.open(NearMissDialogComponent, {
      width: '400px',
      data: { stopCardFlag: flag },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.immidiateAction = result;
      this.radioFlag = true;
      if (flag) {
        if (this.immidiateAction) {
          this.actionForm.patchValue({ action: [{ natureAction: this.immidiateAction }] });
          this.actionForm.patchValue({ action: [{ typeId: 1 }] });
        } else {
          this.overViewForm.patchValue({ StopCardId: '2' });
        }
      }
    });
  }

  ngAfterViewInit() {
    this.getNearMissFormFeeder('near miss');
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
