import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, Sort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Logger } from '@core';
import { NearMissService } from './../../share/near-miss.service';
import { LocalStorageService, NotificationService } from '@shared/common-service';
import { SidenavStoreService } from '@app/shell/sidenav/sidenav-store.service';
import { environment } from '@env/environment';
import * as moment from 'moment';
import { BehaviorSubject, Subscription } from 'rxjs';
import { CustomizedCellComponent } from '@shared/component/customized-cell/customized-cell.component';
import { VesselSidebarComponent } from '@shared/component/vessel-sidebar/vessel-sidebar.component';
import { Title } from '@angular/platform-browser';
import { FormlyFieldConfig } from '@formly';

const log = new Logger('NearMissTableComponent');

export interface NewrMissInter {
  reportId: string;
  reportType: string;
  portName: string;
  vesselName: string;
  dateOfReport: string;
  task: string;
  description: string;
}

/**
 * @title Table with sorting
 */
export interface RouteParams {
  type: string;
}

@Component({
  selector: 'app-near-miss-table',
  templateUrl: './near-miss-table.component.html',
  styleUrls: ['./near-miss-table.component.scss'],
})
export class NearMissTableComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSubject = new BehaviorSubject<Element[]>([]);
  displayedColumns: string[] = [
    'reportId',
    'reportType',
    'portName',
    'vesselName',
    'dateOfReport',
    'task',
    'description',
    'actions',
  ];
  public title = '';
  public indexing: any = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'];

  public nearMissData: any = [];
  public dataSource = new MatTableDataSource([]);
  public data: NewrMissInter[] = [];
  public isLoading = false;
  public isdataSource = false;
  public isTableView = true;
  public portList: any = [];
  public nearmissData: any = [];
  public reportType: any;
  public nearmissId: any;
  error: string | undefined;
  public isShow = false;
  public vesselTypeData: any = [];
  public vesselsData: any = [];
  public states: string[] = ['None', 'Alaska'];
  public deleteData: any;
  public pageEvent: PageEvent;
  public taskList: any;
  public vesselFilterData: any;
  public API_URL = environment.filePath;
  public sortedData: any;
  public sub: Subscription;
  public nearmissArray: any = [];
  public impactList: any = [];
  public impactValue2: any = [];
  public severityLevel: any = [];
  public probability: any = [];
  public impactLabel: any;
  public severityArray: any = [];
  public stageArray: any = [];
  public rowData: any;
  public icons: any;
  public id: any;
  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  public array: any;
  public length = 100;
  public sideBar: any;
  public checkedVessels: any = [];
  public checkedVesselIdArray: any = [];
  editType = '';
  paginationPageSize = 20;

  columnDefs = [
    {
      headerName: 'Report  Id',
      field: 'reportId',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true,
    },
    {
      headerName: 'Report Type',
      field: 'reportType',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Port Name',
      field: 'portName',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Vessel Name',
      field: 'vesselName',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Date Of Report Entry',
      field: 'dateOfReport',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Task',
      field: 'task',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
      editable: true,
    },
    {
      headerName: 'Description',
      field: 'description',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'Actions',
      field: 'actions',
      menuTabs: ['generalMenuTab', 'filterMenuTab'],
      sortable: true,
      cellRenderer: 'customizedBtnCell',
      cellRendererParams: {
        onClick: this.deleteNearmiss.bind(this),
        onClickEdit: this.editItems.bind(this),
        onClickDetails: this.getNearMissDetail.bind(this),
      },
      filter: 'agTextColumnFilter',
    },
  ];

  frameworkComponents = {
    customizedBtnCell: CustomizedCellComponent,
    customStatsToolPanel: VesselSidebarComponent,
  };
  defaultColDef = {
    flex: 1,
    resizable: true,
    enableValue: true,
    sortable: true,
    filter: true,
  };
  filterForm: FormGroup = new FormGroup({});
  options: FormGroup;
  colorControl = new FormControl('primary');
  fontSizeControl = new FormControl(16, Validators.min(10));
  setTimeControl = new FormControl();

  model = {};
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'reportId',
          type: 'input',
          className: 'flex-4',
          templateOptions: {
            label: 'Report ID',
            placeholder: 'REPORT ID',
            required: false,
          },
        },
        {
          key: 'portName',
          type: 'select',
          className: 'flex-4',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Port Name',
            options: this.nearMissService.getPortList(),
            valueProp: 'portId',
            labelProp: 'name',
            required: false,
          },
        },
        {
          key: 'dateOfReport',
          type: 'datepicker',
          className: 'flex-4',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Report Date',
            placeholder: 'Report Date',
            required: false,
          },
        },
        {
          key: 'taskId',
          type: 'select',
          className: 'flex-4',
          templateOptions: {
            appearance: 'outline',
            floatLabel: 'never',
            label: 'Select Task',
            options: this.nearMissService.getTaskList(),
            valueProp: 'taskId',
            labelProp: 'task',
            required: false,
          },
        },
      ],
    },
  ];

  @Input() isDisabled = false;
  @Output() btnClick = new EventEmitter();

  constructor(
    private route: ActivatedRoute,
    private nearMissService: NearMissService,
    private notifyService: NotificationService,
    private router: Router,
    private localStorageService: LocalStorageService,
    private leftSidenavStore: SidenavStoreService,
    private titleService: Title
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.titleService.setTitle('Near Miss');
  }

  onAddForm() {
    this.router.navigate(['form'], { relativeTo: this.route }).then((e) => {
      if (e) {
        // Navigation is successful!
      } else {
        // Navigation has failed!
      }
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    this.icons = {
      'custom-stats': '<span class="ag-icon ag-icon-custom-stats"></span>',
    };

    const paramValue = 'value';
    this.leftSidenavStore.hasSideNav = true;
    if (this.route.params[paramValue].type) {
      const type = this.route.params[paramValue].type;
      this.leftSidenavStore.hasMyVessels = true;
      this.reportType = type;
      this.isTableView = true;
      this.getFilterNearMiss(type);
    } else if (this.route.params[paramValue].nearmissId) {
      this.leftSidenavStore.hasSideNav = false;
      this.leftSidenavStore.hasMyVessels = false;
      this.isTableView = false;
      this.nearmissId = this.route.params[paramValue].nearmissId;
      this.getNearMissDetails(this.nearmissId);
    }

    /* this.leftSidenavStore.myVessels$.subscribe((vesselData: any) => {
      if (vesselData) {
        log.debug('vesselData: ', vesselData);
        this.vesselFilter(vesselData);
      }
    });
    */
    this.getVesselType();
    this.getVessels();
    this.getPortData();
    this.getNearMissTastList();
    this.getAllTypeList();
    this.getSeverityLevel('Severity Level');
    this.getProbability('Probablity Of Happening');
    this.sideBar = {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'columns',
          iconKey: 'columns',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
          },
        },
        {
          id: 'customStats',
          labelDefault: 'Vessels',
          labelKey: 'customStats',
          iconKey: 'custom-stats',
          filter: true,
          toolPanel: 'customStatsToolPanel',
        },
      ],
      filter: true,
      position: 'right',
    };
  }

  handleImgError(ev: any) {
    const source = ev.srcElement;
    const imgSrc = `assets/file.png`;
    source.src = imgSrc;
  }

  getNearMiss() {
    this.isLoading = true;
    this.nearMissService.getAllNearMiss().subscribe(
      (response) => {
        this.isLoading = false;
        log.debug('response getNearMiss: ', response);
        if (!response) {
          this.isdataSource = true;
          const message = 'No Record Found!';
          this.notifyService.showError(message, this.title);
        } else {
          this.nearMissData = response;
          this.nearmissArray = [];
          this.rowData = this.nearMissData;
          this.dataSource = new MatTableDataSource(this.rowData);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  onGridReady(params: any) {
    log.debug('reached');
    const paramValue = 'value';
    if (this.route.params[paramValue].type) {
      const type = this.route.params[paramValue].type;
      log.debug('type of report', type);
      this.nearMissService.getFilterNearMiss(type).subscribe(
        (response) => {
          log.debug('Users Data', response);
          if (!response) {
            this.isdataSource = true;
          }
          this.rowData = response;
        },
        (error) => {
          this.notifyService.showError(error);
        }
      );
    }
    this.leftSidenavStore.myVessels$.subscribe((vessels) => {
      log.debug('checked vessels', vessels);
      this.checkedVessels = vessels;
      this.checkedVesselIdArray = [];

      let stringd = '';
      vessels.forEach((vessel: any, index) => {
        stringd = stringd + 'vesselId=' + vessel.vesselId + '&';
      });
      this.nearMissService.getFilterVessel(stringd).subscribe((response: any) => {
        this.rowData = response;
      });

      if (vessels.length === 0) {
        this.nearMissService.getAllNearMiss().subscribe(
          (response) => {
            log.debug('Users Data', response);
            if (!response) {
              this.isdataSource = true;
            }
            this.rowData = response;
          },
          (error) => {
            this.notifyService.showError(error);
          }
        );
      }
    });
  }

  getFilterNearMiss(type: any) {
    this.isLoading = true;
    this.nearMissService.getFilterNearMiss(type).subscribe(
      (response) => {
        this.dataSource = new MatTableDataSource([]);
        this.isLoading = false;
        this.nearMissData = response;
        if (!response) {
          this.isdataSource = true;
          const message = 'No Record Found!';
          this.notifyService.showError(message, this.title);
        } else {
          this.isdataSource = false;
          this.nearmissArray = [];
          this.data = this.nearMissData;
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getFilter(type: any) {
    this.isLoading = true;
    const vesselList = this.vesselFilterData;
    this.nearMissService.getFilter(type, vesselList).subscribe(
      (response) => {
        this.dataSource = new MatTableDataSource([]);
        this.isLoading = false;
        this.nearMissData = response;
        if (!response) {
          this.isdataSource = true;
          const message = 'No Record Found!';
          this.notifyService.showError(message, this.title);
        } else {
          this.isdataSource = false;
          this.nearmissArray = [];
          this.data = this.nearMissData;
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },

      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  vesselFilter(vessels: any) {
    let vessel = '';
    let index = 0;
    if (vessels.length > 0) {
      vessels.forEach((vesselValue: any) => {
        if (index === 0) {
          vessel = 'vesselId=' + vesselValue;
        } else {
          vessel = vessel + '&vesselId=' + vesselValue;
        }
        index++;
      });
      this.vesselFilterData = vessel;
      this.onSubmit(1);
    } else {
      this.getNearMiss();
    }
  }

  onSubmit(type: any) {
    this.isLoading = true;
    log.debug('filter data on submit');
    if (!type) {
      if (this.filterForm.value.dateOfReport) {
        const observeTime = this.filterForm.value.dateOfReport;
        const convertedDate = moment(observeTime).startOf('day');
        const dateOfExpiry = moment(convertedDate).format('YYYY-MM-DD');
        this.filterForm.value.dateOfReport = dateOfExpiry;
      }

      if (this.reportType) {
        this.filterForm.value.reportType = this.reportType;
      }
    }
    if (this.reportType) {
      this.filterForm.value.reportType = this.reportType;
    }
    const vesselList = this.vesselFilterData;

    this.nearMissService.getFilter(this.filterForm.value, vesselList).subscribe(
      (response) => {
        this.dataSource = new MatTableDataSource([]);
        this.isLoading = false;
        this.nearMissData = response;
        log.debug('filter data response', response);
        if (!response) {
          this.isdataSource = true;
          const message = 'No Record Found!';
          this.notifyService.showError(message, this.title);
        } else {
          this.nearmissArray = [];
          this.nearMissData.forEach((element: any) => {
            const elementData = {
              reportId: element.reportId,
              reportType: element.reportType,
              portName: element.portName,
              vesselName: element.vesselName,
              dateOfReport: element.dateOfReport,
              task: element.task,
              description: element.description,
            };
            this.nearmissArray.push(elementData);
          });
          this.rowData = this.nearMissData;
          this.dataSource = new MatTableDataSource(this.rowData);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  resetFilter() {
    this.filterForm.reset();
    this.getNearMiss();
  }

  onChangeHour(event: any) {}

  getFontSize() {
    return Math.max(10, this.fontSizeControl.value);
  }

  getPadding() {}

  toggleDisplay() {
    this.isShow = !this.isShow;
  }

  setImpactType(type: any, impactList: any) {
    if (impactList) {
      const impactOn = impactList;
      impactOn.forEach((element: any) => {
        this.impactValue2.push(element.eventImpact);
      });

      const availableType = this.impactValue2.indexOf(type);
      if (availableType > -1) {
        return true;
      } else {
        return false;
      }
    }
  }

  getImpactActiveClass(resultantRisk: any) {
    if (resultantRisk < 5) {
      this.impactLabel = 'Low';
      return 'normal';
    } else if (resultantRisk > 4 && resultantRisk < 10) {
      this.impactLabel = 'Medium';
      return 'medium';
    } else {
      this.impactLabel = 'High';
      return 'extreme';
    }
  }

  getVesselType() {
    this.nearMissService.getVesselType().subscribe(
      (response) => {
        this.vesselTypeData = response;
        this.localStorageService.setVessels(response);
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getVessels() {
    this.nearMissService.getVessels().subscribe(
      (response) => {
        this.vesselsData = response;
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getPortData() {
    this.nearMissService.getPortLocation().subscribe(
      (response) => {
        this.portList = response;
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  getNearMissDetails(nearmissId: any) {
    this.isTableView = false;
    this.nearMissService.getNearmissDetails(nearmissId).subscribe(
      (response) => {
        this.nearmissData = response;
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Task Involved list data
  getNearMissTastList() {
    this.nearMissService.getTaskList().subscribe(
      (response) => {
        if (response) {
          this.taskList = response;
        } else {
          const message = 'Task list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get All Type List data
  getAllTypeList() {
    this.nearMissService.getAllTypeList().subscribe(
      (response) => {
        if (response) {
          this.impactList = response.impactList;
        } else {
          const message = 'all type list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Report Type List data
  getSeverityLevel(type: any) {
    this.nearMissService.getFormFeed(type).subscribe(
      (response) => {
        if (response) {
          this.severityArray = response;
          this.stageArray = [];
          this.severityArray.forEach((element: any) => {
            if (element.module === 'near miss') {
              this.stageArray.push(element);
            }
          });
          this.severityLevel = this.stageArray;
        } else {
          const message = 'severity level not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  // Get Report Type List data
  getProbability(type: any) {
    this.nearMissService.getFormFeed(type).subscribe(
      (response) => {
        if (response) {
          this.probability = response;
        } else {
          const message = 'probability list not found';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  deleteNearmiss(params: any) {
    this.nearMissService.deleteNearmiss(params.rowData.nearmissId).subscribe(
      (response) => {
        this.deleteData = response;
        if (this.deleteData.statusCode === 200) {
          const message = this.deleteData.message;
          this.notifyService.showSuccess(message, this.title);
          this.fetchData();
        } else {
          const message = 'Record No Deleted';
          this.notifyService.showError(message, this.title);
        }
      },
      (error) => {
        this.error = error;
        const message = error;
        this.notifyService.showError(message, this.title);
      }
    );
  }

  fetchData() {
    this.nearMissService.getAllNearMiss().subscribe((data) => {
      this.rowData = data;
    });
  }

  onSortData(sort: Sort) {
    // let data = this.dataSource;
    // if (sort.active && sort.direction !== '') {
    //   data = data.sort((a: NewrMissInter, b: NewrMissInter) => {
    //     const isAsc = sort.direction === 'asc';
    //     switch (sort.active) {
    //       case 'reportId':
    //         return this.compare(a.reportId, b.reportId, isAsc);
    //       case 'reportType':
    //         return this.compare(+a.reportType, +b.reportType, isAsc);
    //       case 'portName':
    //         return this.compare(+a.portName, +b.portName, isAsc);
    //       case 'vesselName':
    //         return this.compare(+a.vesselName, +b.vesselName, isAsc);
    //       case 'dateOfReport':
    //         return this.compare(+a.dateOfReport, +b.dateOfReport, isAsc);
    //       case 'task':
    //         return this.compare(+a.task, +b.task, isAsc);
    //       case 'description':
    //         return this.compare(+a.description, +b.description, isAsc);
    //       default:
    //         return 0;
    //     }
    //   });
    // }
    // this.dataSubject.next(data);
  }

  editItems(params: any) {
    log.debug('router', params.rowData.nearmissId);
    log.debug('link', this.router);
    this.id = params.rowData.nearmissId;
    this.router.navigate([`/incident/near-miss/${this.id}/edit`]);
  }

  getNearMissDetail(params: any) {
    log.debug('router', params.rowData.nearmissId);
    log.debug('link', this.router);
    this.id = params.rowData.nearmissId;
    this.router.navigate([`/incident/near-miss/${this.id}/details`]);
  }
}
