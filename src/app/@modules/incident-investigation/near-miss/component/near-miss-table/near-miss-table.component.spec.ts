import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearMissTableComponent } from './near-miss-table.component';

describe('NearMissTableComponent', () => {
  let component: NearMissTableComponent;
  let fixture: ComponentFixture<NearMissTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NearMissTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearMissTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
