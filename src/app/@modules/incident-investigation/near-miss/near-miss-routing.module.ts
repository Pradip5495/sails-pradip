import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NearMissComponent } from './near-miss.component';
import { extract } from '@app/i18n';
import { NearMissDetailsComponent } from './component/near-miss-details/near-miss-details.component';
import { NearMissFormComponent } from './component/near-miss-form/near-miss-form.component';
import { NearMissUpdateFormComponent } from './component/near-miss-update-form/near-miss-update-form.component';
import { NearMissTableComponent } from './component/near-miss-table/near-miss-table.component';

const nearMissModule: Routes = [
  {
    path: '',
    component: NearMissComponent,
    children: [
      { path: '', component: NearMissTableComponent },
      { path: 'filter/:type', component: NearMissComponent, data: { title: extract('near-miss') } },
      { path: 'form', component: NearMissFormComponent, data: { title: extract('near-miss') } },
      { path: ':id/edit', component: NearMissUpdateFormComponent, data: { title: extract('near-miss') } },
      { path: ':nearmissId/details', component: NearMissDetailsComponent, data: { title: extract('near-miss') } },
    ],
    data: { title: extract('Near Miss') },
  },
];

@NgModule({
  imports: [RouterModule.forChild(nearMissModule)],
  exports: [RouterModule],
})
export class NearMissRoutingModule {}
