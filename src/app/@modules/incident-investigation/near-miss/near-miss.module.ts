import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { RouterModule } from '@angular/router';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FormlyModule } from '@formly';
import { NearMissComponent } from './near-miss.component';
import { NearMissTableComponent } from './component/near-miss-table/near-miss-table.component';
import { NearMissFilterComponent } from './component/near-miss-filter/near-miss-filter.component';
import { NearMissDialogComponent } from './component/near-miss-dialog/near-miss-dialog.component';
import { NearMissFormComponent } from './component/near-miss-form/near-miss-form.component';
import { NearMissDetailsComponent } from './component/near-miss-details/near-miss-details.component';
import { NearMissUpdateFormComponent } from './component/near-miss-update-form/near-miss-update-form.component';
import { NearMissRoutingModule } from './near-miss-routing.module';
import { CauseTreeModule } from '../share/cause-tree/cause-tree.module';

@NgModule({
  declarations: [
    NearMissComponent,
    NearMissTableComponent,
    NearMissFilterComponent,
    NearMissDialogComponent,
    NearMissFormComponent,
    NearMissDetailsComponent,
    NearMissUpdateFormComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    RouterModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormlyModule,
    NearMissRoutingModule,
    CauseTreeModule,
  ],
})
export class NearMissModule {}
