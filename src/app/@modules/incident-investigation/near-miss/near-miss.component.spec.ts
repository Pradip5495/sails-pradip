import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearMissComponent } from './near-miss.component';

describe('NearMissComponent', () => {
  let component: NearMissComponent;
  let fixture: ComponentFixture<NearMissComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NearMissComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearMissComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
