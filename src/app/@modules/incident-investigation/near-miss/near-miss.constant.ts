export const NEAR_MISS_SEVERITY_LEVEL_POINTS = {
  Extreme: 5,
  Significant: 4,
  Moderate: 3,
  Minor: 2,
  Negligible: 1,
};

export const NEAR_MISS_LIKELIHOOD_POINTS = {
  Rare: 1,
  Unlikely: 2,
  Possible: 3,
  Likely: 4,
  'Almost Certain': 5,
};
