import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Logger } from '@core';

const log = new Logger('NearMissComponent');

@Component({
  selector: 'app-near-miss',
  templateUrl: './near-miss.component.html',
  styleUrls: ['./near-miss.component.scss'],
})
export class NearMissComponent implements OnInit {
  isLoading = false;
  isNewNearMiss = false;
  isUpdateNearMiss = false;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.isLoading = true;
    const paramValue = 'value';
    log.debug('Init');
    log.debug('Params', this.route.params[paramValue]);
    if (this.route.params[paramValue].form === 'create') {
      this.isNewNearMiss = true;
    } else if (this.route.params[paramValue].id) {
      this.isNewNearMiss = false;
      this.isUpdateNearMiss = true;
    }
  }
}
