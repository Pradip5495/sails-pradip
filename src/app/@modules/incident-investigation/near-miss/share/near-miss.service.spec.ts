import { TestBed } from '@angular/core/testing';

import { NearMissService } from './near-miss.service';

describe('NearMissService', () => {
  let service: NearMissService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NearMissService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
