import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';

// API Routes Definition
const routes = {
  nearmiss: {
    getAll: () => `/nearmiss`,
  },

  vessel: {
    get: () => `/vessel?myvessel=true`,
  },

  reporttype: {
    get: (id: string) => `/form-feeder?type=${id}`,
    getAll: () => `/form-feeder/list`,
  },

  crossreference: {
    getAll: () => `/crttsma`,
  },

  element: {
    getAll: () => `/crttsma/element`,
  },

  stage: {
    getAll: () => `/crttsma/stage`,
  },

  specifickpi: {
    get: (id: string) => `cross-referencing-to-tsma/stage/${id}`,
  },

  port: {
    getAll: () => `/vessel/port`,
  },
};
@Injectable({
  providedIn: 'root',
})
export class NearMissService {
  constructor(private httpClient: HttpClient, private readonly httpDispatcher: HttpDispatcherService) {}

  /**
   *  Get User Profile Details
   */
  getFilterNearMiss(type: string): Observable<Response> {
    const API_URL = `/nearmiss?reportType=${type}`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Filter Near Miss Details
   */
  getFilter(formData: any, vesselData: any): Observable<Response> {
    let formValue = '';
    let isFirstFlag = 0;
    // Query Binding
    if (vesselData) {
      if (isFirstFlag) {
        formValue = formValue + '&' + vesselData;
      } else {
        formValue = vesselData;
        isFirstFlag = 1;
      }
    }

    if (formData.reportId !== '' && formData.reportId) {
      if (isFirstFlag) {
        formValue = formValue + '&reportId=' + formData.reportId;
      } else {
        formValue = 'reportId=' + formData.reportId + formValue;
        isFirstFlag = 1;
      }
    }

    if (formData.portName !== '' && formData.portName) {
      if (isFirstFlag) {
        formValue = formValue + '&portId=' + formData.portName;
      } else {
        formValue = 'portId=' + formData.portName + formValue;
        isFirstFlag = 1;
      }
    }

    if (formData.dateOfReport !== '' && formData.dateOfReport) {
      if (isFirstFlag) {
        formValue = formValue + '&dateOfReport=' + formData.dateOfReport;
      } else {
        formValue = 'dateOfReport=' + formData.dateOfReport + formValue;
        isFirstFlag = 1;
      }
    }

    if (formData.reportType !== '' && formData.reportType) {
      if (isFirstFlag) {
        formValue = formValue + '&reportType=' + formData.reportType;
      } else {
        formValue = 'reportType=' + formData.reportType + formValue;
        isFirstFlag = 1;
      }
    }

    if (formData.taskId !== '' && formData.taskId) {
      if (isFirstFlag) {
        formValue = formValue + '&taskId=' + formData.taskId;
      } else {
        formValue = 'taskId=' + formData.taskId + formValue;
        isFirstFlag = 1;
      }
    }

    const API_URL = `/nearmiss?${formValue}`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Filter Vessel Near Miss Details
   */
  getFilterVessel(vesselId: any): Observable<Response> {
    const API_URL = `/nearmiss?${vesselId}`;
    // const  filters = vesselId ? `/nearmiss?${vesselId.toSanitizedURLFilters()}` : `/nearmiss`;
    return this.httpDispatcher.get(API_URL);
    /* return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );*/
  }

  /**
   *  Get contact or event
   */
  getContactOrEvent(): Observable<Response> {
    const API_URL = `/cause/typeOfContact`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get contact or event
   */
  getBasicImmediate(): Observable<Response> {
    const API_URL = `/cause/basicimmediatecause`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Root Cause
   */
  getRootCause(): Observable<Response> {
    const API_URL = `/cause/basicrootcause`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Corrective Action Cause
   */
  getCorrectiveCause(): Observable<Response> {
    const API_URL = `/cause/correctiveaction`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Corrective Action Cause
   */
  getCauseTree(): Observable<Response> {
    const API_URL = `/cause/causetree`;
    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Port List
   */
  getPortList<T = Response>(): Observable<T> {
    return this.httpDispatcher.get(routes.port.getAll());
  }

  /**
   *  Get Task Involved List
   */
  getTaskList(): Observable<any> {
    const API_URL = `/task`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Task Involved List
   */
  getLocationList(): Observable<Response> {
    const API_URL = `/location`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   * Submit Near Miss Data
   */
  createNearMiss(data: any): Observable<Response> {
    return this.httpClient.post('/nearmiss', data).pipe(
      tap((responseData: any) => {
        const statusCode = 'statusCode';
        if (responseData[statusCode] === 201) {
          return responseData;
        } else {
          return responseData;
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }

  /**
   * Update Near Miss Data
   */
  editNearMiss(data: any, nearMissId: any): Observable<Response> {
    const API_URL = `/nearmiss/${nearMissId}`;
    return this.httpClient.put(API_URL, data).pipe(
      tap((responseData: any) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        if (responseData[statusCode] === 200) {
          return responseData[dataParam];
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }

  /**
   *  Get All Vessel Details
   */

  getVessels(): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.vessel.get());
  }

  /**
   *  Get All Vessel Details
   */

  getVesselType(): Observable<Response> {
    const API_URL = `/vessel/type`;
    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All Designation Details
   */

  getDesignation(): Observable<Response> {
    const API_URL = `/user/designation`;
    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get All Designation Details
   *  @deprecated use `VesselService.getAllPorts()`
   */
  getPortLocation(): Observable<any> {
    const API_URL = `/vessel/port`;
    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Near miss Details
   */

  getNearmissDetails(nuid: any): Observable<Response> {
    const API_URL = `/nearmiss/${nuid}`;
    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get Action List
   */

  getAction(): Observable<Response> {
    const API_URL = `/action`;
    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get all reportreviwed by users for investigation team
   */
  getAllUsers(): Observable<Response> {
    const API_URL = `/user`;

    return this.httpClient.get(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  /**
   *  Get all cross reference Element
   */
  getElements(): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.element.getAll());
  }

  /**
   *  Get all cross reference Stage
   */
  getStage(): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.stage.getAll());
  }

  /**
   *  Get all cross reference Stage
   */
  getSpecificKPI(suid: any): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.specifickpi.get(suid));
  }

  /**
   *  Get all cross reference relation data
   */
  getCrossReferenceTMSA(): Observable<Response> {
    return this.httpDispatcher.get<any>(routes.crossreference.getAll());
  }

  deleteNearmiss(nearmissId: any) {
    const API_URL = `/nearmiss/${nearmissId}`;

    return this.httpClient.delete(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  deleteNearmissImage(actionAttchmentId: any) {
    const API_URL = `/nearmiss/attachment/${actionAttchmentId}`;

    return this.httpClient.delete(API_URL).pipe(
      map((responseData) => {
        const statusCode = 'statusCode';
        return responseData[statusCode] && responseData[statusCode] === 200 ? responseData : false;
      }),
      tap((item) => {
        if (item) {
          return item;
        }
      }), // when success, add the item to the local service
      catchError((err) => {
        return of(false);
      })
    );
  }

  getFormFeed(type: string): Observable<any> {
    return this.httpDispatcher.get<any>(routes.reporttype.get(type));
  }

  /**
   * Form Feeder for fields (Impact Type, status, Proposed By)
   */
  getAllTypeList(): Observable<any> {
    return this.httpDispatcher.get<any>(routes.reporttype.getAll());
  }

  /**
   * get All Near Miss Report
   */
  getAllNearMiss(): Observable<any> {
    return this.httpDispatcher.get<any>(routes.nearmiss.getAll());
  }
}
