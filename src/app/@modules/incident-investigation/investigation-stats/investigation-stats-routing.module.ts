import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InvestigationStatsComponent } from './investigation-stats.component';
import { extract } from '@app/i18n';

const investigationStatsRoutes: Routes = [
  { path: '', component: InvestigationStatsComponent, data: { title: extract('Statistics') } },
];

@NgModule({
  imports: [RouterModule.forChild(investigationStatsRoutes)],
  exports: [RouterModule],
})
export class InvestigationStatsRoutingModule {}
