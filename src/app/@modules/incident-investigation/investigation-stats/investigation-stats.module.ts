import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvestigationStatsComponent } from './investigation-stats.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { InvestigationStatsRoutingModule } from './investigation-stats-routing.module';

@NgModule({
  declarations: [InvestigationStatsComponent],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    InvestigationStatsRoutingModule,
  ],
})
export class InvestigationStatsModule {}
