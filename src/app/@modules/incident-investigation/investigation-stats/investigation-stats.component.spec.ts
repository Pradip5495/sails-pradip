import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestigationStatsComponent } from './investigation-stats.component';

describe('InvestigationStatsComponent', () => {
  let component: InvestigationStatsComponent;
  let fixture: ComponentFixture<InvestigationStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvestigationStatsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestigationStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
