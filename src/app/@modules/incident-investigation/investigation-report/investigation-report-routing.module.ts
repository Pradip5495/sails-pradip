import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InvestigationReportComponent } from './investigation-report.component';
import { extract } from '@app/i18n';

const investigationReportRoutes: Routes = [
  { path: '', component: InvestigationReportComponent, data: { title: extract('Report') } },
];

@NgModule({
  imports: [RouterModule.forChild(investigationReportRoutes)],
  exports: [RouterModule],
})
export class InvestigationReportRoutingModule {}
