import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvestigationReportComponent } from './investigation-report.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { InvestigationReportRoutingModule } from './investigation-report-routing.module';

@NgModule({
  declarations: [InvestigationReportComponent],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    InvestigationReportRoutingModule,
  ],
})
export class InvestigationReportModule {}
