export class PlanningModel {
  inspectionMaster: {
    vesselId: string;
    inspectionTypeId: string;
    inspectionType: string;
    portId?: string;
    dateOfInspection?: string;
    state: string;

    [key: string]: string;
  };
  comments?: string;
  interval?: number;
  isApproved?: boolean;
  approvedById?: string;
}
