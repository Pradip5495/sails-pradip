import { UserModel, UpdateUser } from './user-model.model';

describe('UserModel', () => {
  it('should create an instance', () => {
    expect(new UserModel()).toBeTruthy();
    expect(new UpdateUser()).toBeTruthy();
  });
});
