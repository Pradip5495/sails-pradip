export class LessonType {
  'lessonTypeId': string;
  'lessonType': string;
  'isActive': boolean;
}

export class ThirdPartyType {
  'thirdPartyId': string;
  'type': string;
}

export class ImpactType {
  'eventImpactTypeId': string;
  'eventImpactType': string;
  'isActive': boolean;
}

export class Nationality {
  'nationalityId': string;
  'countryName': string;
  'isActive': boolean;
}

export class LessonlearntType {
  'lessonsLearntTypeId': string;
  'lessonType': string;
}
