export class Injury {
  'injuryOccuredById': string;
  'cause': string;
  'isActive': boolean;
}

export class BodyPartAffectedModel {
  'bodyPartAffectedId': string;
  'typeOfPart': string;
  'isActive': boolean;
}

export class InjuryCategoryModel {
  'injuryCategoryId': string;
  'category': string;
  'isActive': boolean;
}

export class BackgroundConditionModel {
  'backgroundConditionId': string;
  'background': string;
}

export class AnalysisOfSimilarIncidents {
  'analysisOfSimilarIncidentId': string;
  'questions': string;
}
