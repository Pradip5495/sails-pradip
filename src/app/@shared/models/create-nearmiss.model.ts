export class CreateNearMissModel {
  nearmiss: string;
  vesselTypeId: string;
  latitudeId: string;
  longitudeId: string;
  taskId: string;
  locationId: string;
}
