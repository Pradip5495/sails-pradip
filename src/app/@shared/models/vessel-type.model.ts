export class VesselType {
  vesselTypeId: string;
  vesselType: string;
  isActive: boolean;
}
