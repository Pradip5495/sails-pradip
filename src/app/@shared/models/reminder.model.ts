export class ReminderModel {
  reminder: string;
  inspectionType: string;
  vesselId: string;
  oilMajorId: string;
  recurring: number;
}
