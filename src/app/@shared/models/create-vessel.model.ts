export class CreateVesselModel {
  vessel: string;
  lengthOverall: string;
  extremeBreadth: string;
  netRegisterTonnage: string;
  grossTonnage: string;
  deadWeightSummer: string;
  draftSummer: string;
  masterEmail: string;
  secondaryEmail: string;
  deliveryDate: Date;
  portId: string;
  fleetId: string;
  vesselTypeId: string;
  vesselOwnerId: string;
  vesselUserId: string;
  country: string;
}
