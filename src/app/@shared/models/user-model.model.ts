export class UserModel {
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  phone: string;
  countryCode: string;
  addressLine1: string;
  addressLine2: string;
  addressLine3: string;
  city: string;
  state: string;
  country: string;
  zipcode: string;
  roleId: string;
  designationId: string;
}

export class UpdateUser {
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  phone: string;
  countryCode: string;
  addressLine1: string;
  addressLine2: string;
  addressLine3: string;
  city: string;
  state: string;
  country: string;
  zipcode: string;
  roleId: string;
  designationId: string;
  designation: string;
  role: string;
  profilePic: string;
  // userId: string;
}
