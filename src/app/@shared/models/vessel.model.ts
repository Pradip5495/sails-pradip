export class VesselModel {
  portId?: string;
  vessel: string;
  country?: string;
  fleetId: string;
  isActive: number;
  portName?: string;
  vesselId: string;
  fleetName: string;
  vesselType: string;
  draftSummer?: string;
  masterEmail?: string;
  userDetails?: VesselUserDetail[];
  deliveryDate: string;
  grossTonnage?: string;
  vesselTypeId: string;
  lengthOverall?: string;
  vesselOwnerId: string;
  extremeBreadth?: string;
  fleetManagerId: string;
  secondaryEmail?: string;
  vesselOwnerName: string;
  deadWeightSummer?: string;
  fleetManagerName: string;
  netRegisterTonnage?: string;
}

export class VesselUserDetail {
  userId: string;
  userFirstName: string;
}
