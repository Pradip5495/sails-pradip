export class AdditionalGroup {
  'name': string;
  'userId': string;
  'vesselId': string;
}

export class SetKpi {
  'year': string;
  'inspectionTypeId': string;
  'alertValue': number;
  'targetValue': number;
  'includeInWatchList': boolean;
}
