export class PreparationModel {
  vesselId: string;
  inspectionTypeId: string;
  inspectionType: string;
  portId: string;
  dateOfInspection: string;
  [key: string]: string;
}
