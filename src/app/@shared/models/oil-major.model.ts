export class OilMajorModel {
  oilMajorId: 'string';
  fullName: 'string';
  category: 'string';
  shortName: 'string';
  type: 'string';
  email: 'string';
  website: 'string';
  isFileContent: boolean;
  emailContent: 'string';
  fileContent: 'string';
}
