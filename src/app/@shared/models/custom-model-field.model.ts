export class CustomModelField {
  'taskId': string;
  'task': string;
  'isActive': boolean;
}
export class LocationModel {
  'locationId': string;
  'location': string;
  'isActive': boolean;
}
export class RankModel {
  'rankId': string;
  'rank': string;
  'isActive': boolean;
}
export class EquipmentModel {
  'equipmentId': string;
  'equipment': string;
  'isActive': boolean;
}
export class EquipmentCategoryModel {
  'equipmentCategoryId': string;
  'categoryName': string;
  'isActive': boolean;
}
export class CompanyModel {
  'companyId': string;
  'companyName': string;
  'isActive': boolean;
}
