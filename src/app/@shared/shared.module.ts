import { NewRowButtonsComponent } from '@shared/component/new-row-buttons/new-row-buttons.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import {
  LoaderComponent,
  AgGridComponent,
  VesselSidebarComponent,
  CustomizedCellComponent,
  StatusRendererComponent,
  ToolbarComponent,
  ButtonComponent,
  DialogComponent,
  ProgressBarCellRendererComponent,
  ButtonCellRendererComponent,
  FormlyFieldTemplateComponent,
} from '@shared/component';
import { AgGridModule } from '@ag-grid-community/angular';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormlyModule } from '@formly';
import { AgCellButtonComponent } from './component/ag-cell-button/ag-cell-button.component';
import { CustomHeaderSelectComponent } from './component/custom-header-select/custom-header-select.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonModule } from '@angular/material/button';
import { FormlyImagePreviewComponent } from './component/formly-image-preview/formly-image-preview.component';
import { AttachmentComponent } from './component';
import { VesselStatsComponent } from './component/dashboard/vessel-stats/vessel-stats.component';
import { GraphCardComponent } from './component/dashboard/graph-card/graph-card.component';
import { TwoProgressBarCellComponent } from './component/ag-grid/two-progress-bar-cell/two-progress-bar-cell.component';
import { MatButtonLoadingDirective } from './directives/mat-button-loading.directive';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { DateToLocalPipe } from './pipes/date-to-local.pipe';

@NgModule({
  imports: [
    FlexLayoutModule,
    MaterialModule,
    CommonModule,
    FormsModule,
    Ng2SearchPipeModule,
    AgGridModule.withComponents([
      CustomizedCellComponent,
      StatusRendererComponent,
      NewRowButtonsComponent,
      AgCellButtonComponent,
      CustomHeaderSelectComponent,
    ]),
    FormlyModule,
    MatProgressBarModule,
    MatButtonModule,
  ],
  declarations: [
    LoaderComponent,
    AgGridComponent,
    CustomizedCellComponent,
    VesselSidebarComponent,
    NewRowButtonsComponent,
    StatusRendererComponent,
    ToolbarComponent,
    ButtonComponent,
    DialogComponent,
    AgCellButtonComponent,
    CustomHeaderSelectComponent,
    ProgressBarCellRendererComponent,
    ButtonCellRendererComponent,
    FormlyFieldTemplateComponent,
    FormlyImagePreviewComponent,
    AttachmentComponent,
    VesselStatsComponent,
    GraphCardComponent,
    TwoProgressBarCellComponent,
    MatButtonLoadingDirective,
    DateToLocalPipe,
  ],
  exports: [
    LoaderComponent,
    AgGridComponent,
    VesselSidebarComponent,
    NewRowButtonsComponent,
    StatusRendererComponent,
    ToolbarComponent,
    ButtonComponent,
    DialogComponent,
    CustomHeaderSelectComponent,
    ProgressBarCellRendererComponent,
    ButtonCellRendererComponent,
    FormlyFieldTemplateComponent,
    FormlyImagePreviewComponent,
    AttachmentComponent,
    VesselStatsComponent,
    GraphCardComponent,
    TwoProgressBarCellComponent,
    MatButtonLoadingDirective,
    DateToLocalPipe,
  ],
  entryComponents: [DialogComponent, MatProgressSpinner],
})
export class SharedModule {}
