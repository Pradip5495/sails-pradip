import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

/**
 * TODO: Update this pipe in future to make format configurable
 */
@Pipe({
  name: 'localTimezone',
})
export class DateToLocalPipe implements PipeTransform {
  defaultFormat = 'DD MMM yyy';

  transform(date: string, ...args: unknown[]): string {
    return date ? moment.utc(date).local().format(this.defaultFormat) : date;
  }
}
