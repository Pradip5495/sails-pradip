import { Injectable } from '@angular/core';
import { IJson } from '@types';

const vesselsTypeKey = 'vesselTypes';

export enum StorageType {
  LOCAL_STORAGE = 'localStorage',
}

// TODO: Refactor this service later on
@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  /**
   * Gets the vessel type.
   * @return The vessel type or null if the user is not authenticated.
   */
  get vessels(): any | null {
    return this.localStorage;
  }

  localStorage: any;
  private storageType: StorageType = StorageType.LOCAL_STORAGE;

  constructor() {
    const savedVessels = sessionStorage.getItem(vesselsTypeKey) || localStorage.getItem(vesselsTypeKey);
    if (savedVessels) {
      this.localStorage = JSON.parse(savedVessels);
    }
  }

  /**
   * Sets the vessel types.
   * The vessel types may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the vessels are only persisted for the current session.
   * @param vesselTypes vessel type list.
   * @param remember True to remember vessel type across sessions.
   */
  setVessels(vesselTypes?: any, remember?: boolean) {
    this.localStorage = vesselTypes || null;

    if (vesselTypes) {
      const storage = remember ? localStorage : sessionStorage;
      storage.setItem(vesselsTypeKey, JSON.stringify(this.localStorage));
    } else {
      sessionStorage.removeItem(vesselsTypeKey);
      localStorage.removeItem(vesselsTypeKey);
    }
  }

  setStorageType(type: StorageType) {
    this.storageType = type;
    return this;
  }

  get(key: string) {
    const item = localStorage.getItem(key);
    if (item) {
      return JSON.parse(item);
    }
    return item;
  }

  set(key: string, value: IJson) {
    if (this.storageType === StorageType.LOCAL_STORAGE) {
      localStorage.setItem(key, JSON.stringify(value));
    }
  }

  clear(key: string) {
    if (this.storageType === StorageType.LOCAL_STORAGE) {
      localStorage.removeItem(key);
    }
  }

  /**
   * @deprecated use `LocalStorageService.set()`
   */
  setFormData(data: any) {
    localStorage.setItem('formData', JSON.stringify(data));
  }

  /**
   * @deprecated use `LocalStorageService.get()`
   */
  getFormData() {
    return localStorage.getItem('formData');
  }

  /**
   * @deprecated use `LocalStorageService.set()`
   */
  deleteFormData() {
    return localStorage.removeItem('formData');
  }
}
