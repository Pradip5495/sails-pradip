import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const routes = {
  quote: `/vessel/type`,
};

@Injectable({
  providedIn: 'root',
})
export class RightMenuService {
  constructor(private httpClient: HttpClient) {}

  /**
   *  Get Vessel Type Details
   *  @deprecated
   */

  getVesselType(): Observable<Response> {
    return this.httpClient
      .cache()
      .get(routes.quote)
      .pipe(
        map((responseData) => {
          const statusCode = 'statusCode';
          const dataParam = 'data';
          return responseData[statusCode] && responseData[statusCode] === 200 ? responseData[dataParam] : false;
        }),
        tap((item) => {
          if (item) {
            return item;
          }
        }),
        catchError((err) => {
          return of(false);
        })
      );
  }

  /**
   *  Get All Vessel Details
   *  @deprecated
   */

  getVessels(): Observable<Response> {
    return this.httpClient.get('/vessel').pipe(
      tap((responseData) => {
        // const statusCode = 'statusCode';
        const dataParam = 'data';
        if (responseData === 200) {
          return responseData[dataParam];
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }
}
