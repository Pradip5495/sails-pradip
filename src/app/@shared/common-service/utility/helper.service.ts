import { Injectable } from '@angular/core';
import { Logger } from '@core';
import * as moment from 'moment';

const log = new Logger('helper service');
@Injectable({
  providedIn: 'root',
})
export class HelperService {
  constructor() {}

  getFullDate(date: any) {
    const convertedDate = moment(date).startOf('day');
    const observationYear = moment(convertedDate).format('YYYY-MM-DD');
    if (observationYear !== 'Invalid date') {
      return observationYear;
    } else {
      return '';
    }
  }

  getLocalTime(dateTime: any) {
    const gmtDateTime = moment.utc(dateTime, 'YYYY-MM-DD HH:mm');
    const localDateTime = gmtDateTime.local().format('YYYY-MM-DD HH:mm');
    log.debug('localDateTime: ', localDateTime);
    // const convertedDate = moment(dateTime).startOf('day');
    // const observationYear = moment(convertedDate).format('YYYY-MM-DD');
    if (localDateTime !== 'Invalid date') {
      return localDateTime;
    } else {
      return '';
    }
  }
}
