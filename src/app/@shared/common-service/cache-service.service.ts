import { Injectable } from '@angular/core';

const typeOfContactKey = 'typeOfContact';
const basicImmediateCauseKey = 'basicImmediateCause';
const basicRootCauseKey = 'basicRootCause';
const basicCorrectiveCauseKey = 'basicCorrectiveCause';
const causeTreeKey = 'causeTree';
const organizationKey = 'organizationData';

@Injectable({
  providedIn: 'root',
})
export class CacheServiceService {
  typeOfContactStorage: any;
  basicImmediateCauseStorage: any;
  basicRootCauseStorage: any;
  basicCorrectiveCauseStorage: any;
  causeTreeStorage: any;
  organizationStorage: any;

  constructor() {
    const savedTypeOfContact = sessionStorage.getItem(typeOfContactKey) || localStorage.getItem(typeOfContactKey);
    if (savedTypeOfContact) {
      this.typeOfContactStorage = JSON.parse(savedTypeOfContact);
    }

    const savedBasicImmediateCause =
      sessionStorage.getItem(basicImmediateCauseKey) || localStorage.getItem(basicImmediateCauseKey);
    if (savedBasicImmediateCause) {
      this.basicImmediateCauseStorage = JSON.parse(savedBasicImmediateCause);
    }

    const savedBasicRootCause = sessionStorage.getItem(basicRootCauseKey) || localStorage.getItem(basicRootCauseKey);
    if (savedBasicRootCause) {
      this.basicRootCauseStorage = JSON.parse(savedBasicRootCause);
    }

    const savedBasicCorrectiveCause =
      sessionStorage.getItem(basicCorrectiveCauseKey) || localStorage.getItem(basicCorrectiveCauseKey);
    if (savedBasicCorrectiveCause) {
      this.basicCorrectiveCauseStorage = JSON.parse(savedBasicCorrectiveCause);
    }

    const savedCauseTree = sessionStorage.getItem(causeTreeKey) || localStorage.getItem(causeTreeKey);
    if (savedCauseTree) {
      this.causeTreeStorage = JSON.parse(savedCauseTree);
    }
    const savedOrganizationTree = sessionStorage.getItem(organizationKey) || localStorage.getItem(organizationKey);
    if (savedOrganizationTree) {
      this.organizationStorage = JSON.parse(savedOrganizationTree);
    }
  }

  /**
   * Get the type of Contact.
   * @return The type of Contact or null if the user is not authenticated.
   */
  get typeOfContact(): any | null {
    return this.typeOfContactStorage;
  }

  /**
   * Get the Basic Immediate Cause.
   * @return The Basic Immediate Cause or null if the user is not authenticated.
   */
  get basicImmediateCause(): any | null {
    return this.basicImmediateCauseStorage;
  }

  /**
   * Get the Basic Root Cause.
   * @return The Basic Root Cause or null if the user is not authenticated.
   */
  get basicRootCause(): any | null {
    return this.basicRootCauseStorage;
  }

  /**
   * Get the Basic Corrective Cause.
   * @return The Basic Corrective Cause or null if the user is not authenticated.
   */
  get basicCorrectiveCause(): any | null {
    return this.basicCorrectiveCauseStorage;
  }

  /**
   * Get the Cause Tree.
   * @return The Cause Tree or null if the user is not authenticated.
   */
  get causeTree(): any | null {
    return this.causeTreeStorage;
  }

  /**
   * Get the Organization Tree.
   * @return The Organization Tree or null if the user is not authenticated.
   */
  get oragizationTree(): any | null {
    return this.organizationStorage;
  }

  /**
   * Sets the vessel types.
   * The vessel types may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the vessels are only persisted for the current session.
   * @param vesselTypes vessel type list.
   * @param remember True to remember vessel type across sessions.
   */
  setTypeOfContact(typeOfContact?: any, remember?: boolean) {
    this.typeOfContactStorage = typeOfContact || null;
    if (typeOfContact) {
      const storage = typeOfContact ? localStorage : sessionStorage;
      storage.setItem(typeOfContactKey, JSON.stringify(this.typeOfContactStorage));
    } else {
      sessionStorage.removeItem(typeOfContactKey);
      localStorage.removeItem(typeOfContactKey);
    }
  }

  setBasicImmediateCause(basicImmediateCause?: any) {
    this.basicImmediateCauseStorage = basicImmediateCause || null;

    if (basicImmediateCause) {
      const storage = basicImmediateCause ? localStorage : sessionStorage;
      storage.setItem(basicImmediateCauseKey, JSON.stringify(this.basicImmediateCauseStorage));
    } else {
      sessionStorage.removeItem(basicImmediateCauseKey);
      localStorage.removeItem(basicImmediateCauseKey);
    }
  }

  setBasicRootCause(basicRootCause?: any) {
    this.basicRootCauseStorage = basicRootCause || null;

    if (basicRootCause) {
      const storage = basicRootCause ? localStorage : sessionStorage;
      storage.setItem(basicRootCauseKey, JSON.stringify(this.basicRootCauseStorage));
    } else {
      sessionStorage.removeItem(basicRootCauseKey);
      localStorage.removeItem(basicRootCauseKey);
    }
  }

  setBasicCorrectiveCause(basicCorrectiveCause?: any) {
    this.basicCorrectiveCauseStorage = basicCorrectiveCause || null;

    if (basicCorrectiveCause) {
      const storage = basicCorrectiveCause ? localStorage : sessionStorage;
      storage.setItem(basicCorrectiveCauseKey, JSON.stringify(this.basicCorrectiveCauseStorage));
    } else {
      sessionStorage.removeItem(basicCorrectiveCauseKey);
      localStorage.removeItem(basicCorrectiveCauseKey);
    }
  }

  setCauseTree(causeTree?: any) {
    this.causeTreeStorage = causeTree || null;
    if (causeTree) {
      const storage = causeTree ? localStorage : sessionStorage;
      storage.setItem(causeTreeKey, JSON.stringify(this.causeTreeStorage));
    } else {
      sessionStorage.removeItem(causeTreeKey);
      localStorage.removeItem(causeTreeKey);
    }
  }

  setOrganizationTree(oragizationTree?: any) {
    this.organizationStorage = oragizationTree || null;
    if (oragizationTree) {
      const storage = oragizationTree ? localStorage : sessionStorage;
      storage.setItem(organizationKey, JSON.stringify(this.organizationStorage));
    } else {
      sessionStorage.removeItem(organizationKey);
      localStorage.removeItem(organizationKey);
    }
  }
}
