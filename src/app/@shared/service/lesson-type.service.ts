import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';
import { LessonlearntType } from '../models/lesson-type.model';

const routes = {
  lessonlearntType: {
    get: () => `/lessonslearnttype`,
    create: () => `/lessonslearnttype`,
    update: (id: string) => `/lessonslearnttype/${id}`,
    delete: (id: string) => `/lessonslearnttype/${id}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class LessonTypeService {
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /* Lesson Type CRUD */

  getLessonlearntType(): Observable<LessonlearntType[]> {
    return this.httpDispatcher.get<LessonlearntType[]>(routes.lessonlearntType.get());
  }

  createLessonlearntType(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.lessonlearntType.create(), data);
  }

  updateLessonlearntType(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.lessonlearntType.update(id), data);
  }

  deleteLessonlearntType(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.lessonlearntType.delete(id));
  }
}
