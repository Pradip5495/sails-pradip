import { TestBed } from '@angular/core/testing';

import { AdditionalGroupService } from './additional-group.service';

describe('AdditionalGroupService', () => {
  let service: AdditionalGroupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdditionalGroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
