import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateVesselModel, VesselModel, VesselType } from '@shared/models';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';

// API Routes Definition
const routes = {
  vessel: {
    get: (id: string) => `/vessel/${id}`,
    getAll: (filters?: Map<string, string>) => (filters ? `/vessel?${filters.toSanitizedURLFilters()}` : `/vessel`),
    create: () => `/vessel`,
    update: (id: string) => `/vessel/${id}`,
    delete: (id: string) => `/vessel/${id}`,

    // Nested Routes
    owner: {
      get: (id: string) => `/vessel/owner/${id}`,
      getAll: (filters?: Map<string, string>) =>
        filters ? `/vessel/owner?${filters.toSanitizedURLFilters()}` : `/vessel/owner`,
      create: () => `/vessel/owner`,
      update: (id: string) => `/vessel/owner/${id}`,
      delete: (id: string) => `/vessel/owner/${id}`,
    },
    type: {
      get: (id: string) => `/vessel/type/${id}`,
      getAll: (filters?: Map<string, string>) =>
        filters ? `/vessel/type?${filters.toSanitizedURLFilters()}` : `/vessel/type`,
      create: () => `/vessel/type`,
      update: (id: string) => `/vessel/type/${id}`,
      delete: (id: string) => `/vessel/type/${id}`,
    },
    port: {
      get: (id: string) => `/vessel/port/${id}`,
      getAll: (filters?: Map<string, string>) =>
        filters ? `/vessel/port?${filters.toSanitizedURLFilters()}` : `/vessel/port`,
      create: () => `/vessel/port`,
      update: (id: string) => `/vessel/port/${id}`,
      delete: (id: string) => `/vessel/port/${id}`,
    },
    fleet: {
      get: (id: string) => `/vessel/fleet/${id}`,
      getAll: (filters?: Map<string, string>) =>
        filters ? `/vessel/fleet?${filters.toSanitizedURLFilters()}` : `/vessel/fleet`,
      create: () => `/vessel/fleet`,
      update: (id: string) => `/vessel/fleet/${id}`,
      delete: (id: string) => `/vessel/fleet/${id}`,
    },
  },
};

@Injectable({
  providedIn: 'root',
})
export class VesselService {
  gridApi: any;
  columnApi: any;
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /* Vessels CRUD */
  createVessel(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.vessel.create(), data);
  }

  getVessel(id: string): Observable<VesselModel> {
    return this.httpDispatcher.get<VesselModel>(routes.vessel.get(id));
  }

  getVessels(filters?: Map<string, string>): Observable<VesselModel[]> {
    return this.httpDispatcher.get<VesselModel[]>(routes.vessel.getAll(filters));
  }

  updateVessel(id: string, vessel: CreateVesselModel): Observable<any> {
    return this.httpDispatcher.put(routes.vessel.update(id), vessel);
  }

  deleteVessel(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.vessel.delete(id));
  }

  /* Vessel Owner CRUD */
  createVesselOwner(owner: any): Observable<any> {
    return this.httpDispatcher.post(routes.vessel.owner.create(), owner);
  }

  getVesselOwner(id: string): Observable<any> {
    return this.httpDispatcher.get(routes.vessel.owner.get(id));
  }

  getVesselOwners(filters?: Map<string, string>): Observable<any> {
    return this.httpDispatcher.get(routes.vessel.owner.getAll(filters));
  }

  updateVesselOwner(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.vessel.owner.update(id), data);
  }

  deleteVesselOwner(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.vessel.owner.delete(id));
  }

  /* Vessel Type CRUD */
  createVesselType(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.vessel.type.create(), data);
  }

  getVesselType(id: string): Observable<any> {
    return this.httpDispatcher.get(routes.vessel.type.get(id));
  }

  getVesselTypes(filters?: Map<string, string>): Observable<any> {
    return this.httpDispatcher.get(routes.vessel.type.getAll(filters));
  }

  updateVesselType(id: string, data: VesselType): Observable<any> {
    return this.httpDispatcher.put(routes.vessel.type.update(id), data);
  }

  deleteVesselType(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.vessel.type.delete(id));
  }

  /* Vessel Port CRUD */
  createVesselPort(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.vessel.port.create(), data);
  }

  getVesselPort(id: string): Observable<any> {
    return this.httpDispatcher.get(routes.vessel.port.get(id));
  }

  getVesselPorts(filters?: Map<string, string>): Observable<any> {
    return this.httpDispatcher.get(routes.vessel.port.getAll(filters));
  }

  updateVesselPort(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.vessel.port.update(id), data);
  }

  deleteVesselPort(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.vessel.port.delete(id));
  }

  /* Vessel Fleet CRUD */
  createVesselFleet(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.vessel.fleet.create(), data);
  }

  getVesselFleet(id: string): Observable<any> {
    return this.httpDispatcher.get(routes.vessel.fleet.get(id));
  }

  getVesselFleets(filters?: Map<string, string>): Observable<any> {
    return this.httpDispatcher.get(routes.vessel.fleet.getAll(filters));
  }

  updateVesselFleet(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.vessel.fleet.update(id), data);
  }

  deleteVesselFleet(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.vessel.fleet.delete(id));
  }
}
