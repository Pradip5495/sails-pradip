import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';
import { ImpactType } from '../models/lesson-type.model';

const routes = {
  event: {
    get: (id: string) => `/eventimpacttype/${id}`,
    getAll: () => `/eventimpacttype`,
    create: () => `/eventimpacttype`,
    update: (id: string) => `/eventimpacttype/${id}`,
    delete: (id: string) => `/eventimpacttype/${id}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class EventImpactTypeService {
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /* Event Impact Type CRUD */

  getEventImpact(): Observable<ImpactType[]> {
    return this.httpDispatcher.get<ImpactType[]>(routes.event.getAll());
  }

  createEventImpact(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.event.create(), data);
  }

  updateEventImpact(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.event.update(id), data);
  }

  deleteEventImpact(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.event.delete(id));
  }
}
