import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  CustomModelField,
  LocationModel,
  RankModel,
  EquipmentModel,
  EquipmentCategoryModel,
  CompanyModel,
} from '@shared/models';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';

const routes = {
  task: {
    get: (id: string) => `/task/${id}`,
    getAll: () => `/task`,
    create: () => `/task`,
    update: (id: string) => `/task/${id}`,
    delete: (id: string) => `/task/${id}`,
  },
  location: {
    get: (id: string) => `/location/${id}`,
    getAll: () => `/location`,
    create: () => `/location`,
    update: (id: string) => `/location/${id}`,
    delete: (id: string) => `/location/${id}`,
  },
  rank: {
    get: (id: string) => `/rank/${id}`,
    getAll: () => `/rank`,
    create: () => `/rank`,
    update: (id: string) => `/rank/${id}`,
    delete: (id: string) => `/rank/${id}`,
  },
  company: {
    get: (id: string) => `/company/${id}`,
    getAll: () => `/company`,
    create: () => `/company`,
    update: (id: string) => `/company/${id}`,
    delete: (id: string) => `/company/${id}`,
  },
  equipment: {
    get: (id: string) => `/equipment/${id}`,
    getAll: () => `/equipment`,
    create: () => `/equipment`,
    update: (id: string) => `/equipment/${id}`,
    delete: (id: string) => `/equipment/${id}`,

    category: {
      get: (id: string) => `/equipment/category/${id}`,
      getAll: () => `/equipment/category`,
      create: () => `/equipment/category`,
      update: (id: string) => `/equipment/category/${id}`,
      delete: (id: string) => `/equipment/category/${id}`,
    },
  },
};

@Injectable({
  providedIn: 'root',
})
export class CustomDataFieldService {
  gridApi: any;
  columnApi: any;
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /* Task CRUD */
  createTask(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.task.create(), data);
  }

  getTask(id: string): Observable<CustomModelField> {
    return this.httpDispatcher.get<CustomModelField>(routes.task.get(id));
  }

  getTasks(): Observable<CustomModelField[]> {
    return this.httpDispatcher.get<CustomModelField[]>(routes.task.getAll());
  }

  updatetask(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.task.update(id), data);
  }

  deletetask(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.task.delete(id));
  }

  /* Location CRUD */
  createLocation(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.location.create(), data);
  }

  getLocation(id: string): Observable<LocationModel> {
    return this.httpDispatcher.get<LocationModel>(routes.location.get(id));
  }

  getLocations(): Observable<LocationModel[]> {
    return this.httpDispatcher.get<LocationModel[]>(routes.location.getAll());
  }

  updateLocation(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.location.update(id), data);
  }

  deleteLocation(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.location.delete(id));
  }

  /* Rank CRUD */
  createRank(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.rank.create(), data);
  }

  getRank(id: string): Observable<RankModel> {
    return this.httpDispatcher.get<RankModel>(routes.rank.get(id));
  }

  getRanks(): Observable<RankModel[]> {
    return this.httpDispatcher.get<RankModel[]>(routes.rank.getAll());
  }

  updateRank(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.rank.update(id), data);
  }

  deleteRank(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.rank.delete(id));
  }

  /* Equipment CRUD */
  createEquipment(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.equipment.create(), data);
  }

  getEquipment(id: string): Observable<EquipmentModel> {
    return this.httpDispatcher.get<EquipmentModel>(routes.equipment.get(id));
  }

  getEquipments(): Observable<EquipmentModel[]> {
    return this.httpDispatcher.get<EquipmentModel[]>(routes.equipment.getAll());
  }

  updateEquipment(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.equipment.update(id), data);
  }

  deleteEquipment(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.equipment.delete(id));
  }

  /* Equipment Category CRUD */
  createCategory(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.equipment.category.create(), data);
  }

  getCategory(id: string): Observable<EquipmentCategoryModel> {
    return this.httpDispatcher.get<EquipmentCategoryModel>(routes.equipment.category.get(id));
  }

  getCategorys(): Observable<EquipmentCategoryModel[]> {
    return this.httpDispatcher.get<EquipmentCategoryModel[]>(routes.equipment.category.getAll());
  }

  updateCategory(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.equipment.category.update(id), data);
  }

  deleteCategory(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.equipment.category.delete(id));
  }
  /* Company CRUD */
  createCompany(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.company.create(), data);
  }

  getCompany(id: string): Observable<CompanyModel> {
    return this.httpDispatcher.get<CompanyModel>(routes.company.get(id));
  }

  getCompanys(): Observable<CompanyModel[]> {
    return this.httpDispatcher.get<CompanyModel[]>(routes.company.getAll());
  }

  updateCompany(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.company.update(id), data);
  }

  deleteCompany(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.company.delete(id));
  }
}
