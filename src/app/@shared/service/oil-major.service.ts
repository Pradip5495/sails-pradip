import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OilMajorModel } from '@shared/models';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';

const routes = {
  oilmajor: {
    getAll: (filters?: Map<string, string>) => (filters ? `/oilmajor?${filters.toSanitizedURLFilters()}` : `/oilmajor`),
    create: () => `/oilmajor`,
    update: (id: string) => `/oilmajor/${id}`,
    delete: (id: string) => `/oilmajor/${id}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class OilMajorService {
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /* Create OilMajor */
  createOilMajor(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.oilmajor.create(), data);
  }

  /* Get all OilMajors */
  getOilMajors(filters?: Map<string, string>): Observable<OilMajorModel[]> {
    return this.httpDispatcher.get<OilMajorModel[]>(routes.oilmajor.getAll(filters));
  }

  /* Update OilMajor */
  updateOilMajor(id: string, oilmajor: OilMajorModel): Observable<any> {
    return this.httpDispatcher.put(routes.oilmajor.update(id), oilmajor);
  }

  /* Delete OilMajor */
  deleteOilMajor(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.oilmajor.delete(id));
  }
}
