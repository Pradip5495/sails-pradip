import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';

const routes = {
  investigators: {
    get: (id: string) => `/investigators/${id}`,
    getAll: (filters?: Map<string, string>) =>
      filters ? `/investigators?${filters.toSanitizedURLFilters()}` : `/investigators`,
    create: () => `/investigators`,
    update: (id: string) => `/investigators/${id}`,
    delete: (id: string) => `/investigators/${id}`,
    deleteAttachment: (id: string) => `/attachment/${id}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class InvestigatorsService {
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  createInvestigators(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.investigators.create(), data);
  }

  getListOfInvestigators(filters?: Map<string, string>): Observable<any> {
    return this.httpDispatcher.get(routes.investigators.getAll(filters));
  }

  getInvestigatorsById(id: any): Observable<any> {
    const investigatorByIdFilter = new Map<string, string>().set('investigatorsId', id);
    return this.getListOfInvestigators(investigatorByIdFilter);
  }

  updateInvestigators(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.investigators.update(id), data);
  }

  deleteInvestigators(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.investigators.delete(id));
  }

  deleteAttachmentInvestigators(id: string) {
    return this.httpDispatcher.delete(routes.investigators.deleteAttachment(id));
  }
}
