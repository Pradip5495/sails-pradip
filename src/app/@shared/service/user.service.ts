import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpDispatcherService } from '@core';
import { NavItem } from '@app/shell/header/nav-item/nav-item.interface';

// API Routes Definition
const routes = {
  user: {
    get: (id: string) => `/user/${id}`,
    getAll: (filters: Map<string, string>) => (filters ? `/user?${filters.toSanitizedURLFilters()}` : `/user`),
    getByDesignation: (designation: string) => `/user/designation/${designation}`,
    getUserProfile: () => `/profile`,
    create: () => `/user`,
    update: (id: string) => `/user/${id}`,
    delete: (id: string) => `/user/${id}`,

    // Nested Routes
    designation: {
      get: (id: string) => `/user/designation/${id}`,
      getAll: (filters: Map<string, string>) =>
        filters ? `/user/designation?${filters.toSanitizedURLFilters()}` : `/user/designation`,
      create: () => `/user/designation`,
      update: (id: string) => `/user/designation/${id}`,
      delete: (id: string) => `/user/designation/${id}`,
    },
  },
};

export interface UserProfiles {
  firstname: string;
  lastname: string;
  userId: string;
  designationCode?: string;
  myMenu: {
    incidentRoutes: NavItem[];
    auditRoutes: NavItem[];
    adminRoutes: NavItem[];
    organizationRoutes: NavItem[];
  };
}

const userProfileKey = 'userProfile';
const moduleKey = 'selected_module';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  /**
   * Gets the user credentials.
   * @return The user credentials or null if the user is not authenticated.
   */
  get currentUser(): UserProfiles | null {
    return JSON.parse(this._userProfile);
  }
  firstTimePageLoaded = true;
  private _userProfile: any;
  private _selectedModule: string;

  constructor(private readonly httpDispatcher: HttpDispatcherService) {
    const savedUserProfile = sessionStorage.getItem(userProfileKey) || localStorage.getItem(userProfileKey);
    const savedModuleName = sessionStorage.getItem(moduleKey) || localStorage.getItem(moduleKey);
    if (savedUserProfile) {
      this._userProfile = JSON.parse(savedUserProfile);
      this._selectedModule = savedModuleName;
    }
  }

  getSelectedModule() {
    return this._selectedModule;
  }

  setProfile(currentUser: any) {
    this._userProfile = currentUser || null;
    if (currentUser) {
      const storage = currentUser ? localStorage : sessionStorage;

      // TODO: Refactor set profile logic later on

      if (typeof JSON.parse(this._userProfile).designationCode === 'undefined') {
        const _userProfile = JSON.parse(this._userProfile);
        _userProfile.designationCode = 'M';
        this._userProfile = JSON.stringify(_userProfile);
      }
      storage.setItem(userProfileKey, JSON.stringify(this._userProfile));
    } else {
      sessionStorage.removeItem(userProfileKey);
      localStorage.removeItem(userProfileKey);
      this.setModule(null);
    }
  }

  setModule(moduleName: string) {
    if (moduleName) {
      this._selectedModule = moduleName;
      const storage = moduleName ? localStorage : sessionStorage;
      storage.setItem(moduleKey, moduleName);
    } else {
      sessionStorage.removeItem(moduleKey);
      localStorage.removeItem(moduleKey);
    }
  }

  /* User CRUD */
  getProfile(): Observable<Response> {
    return this.httpDispatcher.get(routes.user.getUserProfile());
  }

  createUser(data: any): Observable<Response> {
    return this.httpDispatcher.post(routes.user.create(), data);
  }

  getUser(id: string): Observable<any> {
    return this.httpDispatcher.get(routes.user.get(id));
  }

  getUsers(filters?: Map<string, string>): Observable<any> {
    return this.httpDispatcher.get(routes.user.getAll(filters));
  }

  getUsersByDesignation(designation: string): Observable<any> {
    return this.httpDispatcher.get(routes.user.getByDesignation(designation));
  }

  updateUser(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.user.update(id), data);
  }

  deleteUser(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.user.delete(id));
  }

  /* User Designation CRUD */

  createDesignation(data: any): Observable<Response> {
    return this.httpDispatcher.post(routes.user.designation.create(), data);
  }

  getDesignation(id: string): Observable<any> {
    return this.httpDispatcher.get(routes.user.designation.get(id));
  }

  getDesignations(filters?: Map<string, string>): Observable<any> {
    return this.httpDispatcher.get(routes.user.designation.getAll(filters));
  }

  updateDesignation(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.user.designation.update(id), data);
  }

  deleteDesignation(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.user.designation.delete(id));
  }
}
