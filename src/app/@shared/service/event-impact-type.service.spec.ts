import { TestBed } from '@angular/core/testing';

import { EventImpactTypeService } from './event-impact-type.service';

describe('EventImpactTypeService', () => {
  let service: EventImpactTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventImpactTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
