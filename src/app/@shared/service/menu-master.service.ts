import { Injectable } from '@angular/core';
import { HttpDispatcherService } from '@core';
import { Observable } from 'rxjs';

// API Routes Definition
const routes = {
  menu: {
    getAll: (filters: Map<string, string>) => (filters ? `/menu?${filters.toSanitizedURLFilters()}` : `/menu`),
    create: () => `/menu`,
    update: (id: string) => `/menu/${id}`,
    delete: (id: string) => `/menu/${id}`,

    // Nested Routes
    subMenu: {
      getAll: (filters: Map<string, string>) =>
        filters ? `/menu/sub?${filters.toSanitizedURLFilters()}` : `/menu/sub`,
    },
    access: {
      getAll: (filters: Map<string, string>) =>
        filters ? `/menu/access?${filters.toSanitizedURLFilters()}` : `/menu/access`,
    },
  },
};

@Injectable({
  providedIn: 'root',
})
export class MenuMasterService {
  constructor(private readonly httpDispacher: HttpDispatcherService) {}

  /* Menu CRUD */
  createMenu(data: any): Observable<Response> {
    return this.httpDispacher.post(routes.menu.create(), data);
  }

  getMenus(filters?: Map<string, string>): Observable<any> {
    return this.httpDispacher.get(routes.menu.getAll(filters));
  }

  updateMenu(id: string, data: any): Observable<any> {
    return this.httpDispacher.put(routes.menu.update(id), data);
  }

  deleteMenu(id: string): Observable<any> {
    return this.httpDispacher.delete(routes.menu.delete(id));
  }

  /*Sub Menu */
  getSubMenus(filters?: Map<string, string>): Observable<any> {
    return this.httpDispacher.get(routes.menu.subMenu.getAll(filters));
  }

  /*Sub Menu */
  getMenuAccess(filters?: Map<string, string>): Observable<any> {
    return this.httpDispacher.get(routes.menu.access.getAll(filters));
  }
}
