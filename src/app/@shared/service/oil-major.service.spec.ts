import { TestBed } from '@angular/core/testing';

import { OilMajorService } from './oil-major.service';

describe('OilMajorService', () => {
  let service: OilMajorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OilMajorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
