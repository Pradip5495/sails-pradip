import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ActivationService {
  constructor(private httpClient: HttpClient) {}

  /**
   * Submit Activation Data
   */

  createActivation(data: any): Observable<Response> {
    return this.httpClient.post('/activation', data).pipe(
      tap((responseData: any) => {
        const statusCode = 'statusCode';
        const dataParam = 'data';
        if (responseData[statusCode] === 200) {
          return responseData[dataParam];
        }
      }),
      catchError((err) => {
        return of(err);
      })
    );
  }
}
