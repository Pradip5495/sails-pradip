import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

export enum Icons {
  CLOSE = 'close',
  NEAR_MISS = 'near-miss',
  INCIDENT = 'incident',
  CHECKLIST = 'check-list',
  FILTER_BLACK = 'filter-black',
  FILTER_WHITE = 'filter-white',
  GRID_EMPTY = 'grid-empty',
  INVESTIGATOR = 'investigator',
  LESSON_LEARNT = 'lesson-learnt',
  MENU = 'menu-24px',
  NOTIFICATION = 'notification',
  PDF_BLACK = 'pdf-black',
  REPORTS = 'reports',
  STATS = 'stats',
  STOP = 'stop',
  VESSEL = 'vessel',
  VISIBILITY = 'visibility',
  APPS_WHITE = 'apps-white',
  APPS_BLACK = 'apps-black',
  WARNING_WHITE = 'warning-white',
  WARNING_BLACK = 'warning-black',
  PAN_TOOL = 'pan-tool',
  ORGANIZATION = 'organization',
  SHARING = 'sharing',
  REPAIR_TOOL = 'repair_tool',
  INJURIES_ACCIDENT = 'injuries_accident',
  SEA_SHIP_WITH_CONTAINERS = 'sea-ship-with-containers',
  PENCIL = 'pencil',
  DATA_ENTRY = 'data-entry',
  TIMELINE = 'timeline',
  CALENDAR = 'calendar',
  CONTRACT = 'contract',
  REMINDER = 'reminder',
  RETURN_ARROW = 'return_arrow',
}

@Injectable({
  providedIn: 'root',
})
export class IconService {
  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {}

  public registerIcons(): void {
    this.loadIcons(Object.values(Icons), '/assets/svg/icons');
  }

  private loadIcons(iconKeys: string[], iconUrl: string): void {
    iconKeys.forEach((key) => {
      this.matIconRegistry.addSvgIcon(key, this.domSanitizer.bypassSecurityTrustResourceUrl(`${iconUrl}/${key}.svg`));
    });
  }
}
