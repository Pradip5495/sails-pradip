import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';
import { AdditionalGroup, SetKpi } from '@shared/models/additional-group.model';

const routes = {
  AdditionalGroup: {
    get: (id: string) => `/additionalgroup/${id}`,
    getAll: () => `/additionalgroup`,
    create: () => `/additionalgroup`,
    update: (id: string) => `/additionalgroup/${id}`,
    delete: (id: string) => `/additionalgroup/${id}`,
  },

  SetKpi: {
    get: (id: string) => `/additionalgroup/${id}`,
    getAll: () => `/additionalgroup`,
    create: () => `/additionalgroup`,
    update: (id: string) => `/additionalgroup/${id}`,
    delete: (id: string) => `/additionalgroup/${id}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class AdditionalGroupService {
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /* Additional Group CRUD */

  getAdditionalGroup(): Observable<AdditionalGroup[]> {
    return this.httpDispatcher.get<AdditionalGroup[]>(routes.AdditionalGroup.getAll());
  }

  createAdditionalGroup(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.AdditionalGroup.create(), data);
  }

  updateAdditionalGroup(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.AdditionalGroup.update(id), data);
  }

  deleteAdditionalGroup(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.AdditionalGroup.delete(id));
  }

  /* kpi Configuration CRUD */

  getSetKpi(): Observable<SetKpi[]> {
    return this.httpDispatcher.get<SetKpi[]>(routes.SetKpi.getAll());
  }

  createSetKpi(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.SetKpi.create(), data);
  }

  updateSetKpi(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.SetKpi.update(id), data);
  }

  deleteSetKpi(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.SetKpi.delete(id));
  }
}
