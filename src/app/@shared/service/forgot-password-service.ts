import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpDispatcherService } from '@core';

// API Routes Definition
const routes = {
  password: {
    forgetPassword: () => `/forgot-password`,
    resetPassword: () => `/reset-password`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class ForgotPasswordService {
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /**
   * Submit Data
   */

  forgotPassword(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.password.forgetPassword(), data);
  }

  resetPassword(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.password.resetPassword(), data);
  }
}
