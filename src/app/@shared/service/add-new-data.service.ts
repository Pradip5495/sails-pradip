import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AddNewDataService {
  private subjectTask = new Subject<any>();
  private subjectRank = new Subject<any>();
  private subjectLocation = new Subject<any>();
  private subjectEquipment = new Subject<any>();
  private subjectCategory = new Subject<any>();
  private subjectCompany = new Subject<any>();
  private subjectVesselType = new Subject<any>();
  private subjectVesselOwner = new Subject<any>();
  private subjectFleet = new Subject<any>();
  private subjectPort = new Subject<any>();
  private subjectEventImpactType = new Subject<any>();
  private subjectBodyPartAffected = new Subject<any>();
  private subjectInjuryOccuredBy = new Subject<any>();
  private subjectBackgroundCondition = new Subject<any>();
  private subjectAnalysisSimilarCondition = new Subject<any>();
  private subjectInjuryCategory = new Subject<any>();
  private subjectNationality = new Subject<any>();
  private subjectThirdPartyType = new Subject<any>();
  private subjectDeficiencyGroup = new Subject<any>();
  private subjectTargetGroup = new Subject<any>();
  private subjectLessonType = new Subject<any>();
  private subjectLessonLearntType = new Subject<any>();

  constructor() {}

  sendClickEventTask() {
    this.subjectTask.next();
  }
  sendClickEventRank() {
    this.subjectRank.next();
  }
  sendClickEventLocation() {
    this.subjectLocation.next();
  }
  sendClickEventEquipment() {
    this.subjectEquipment.next();
  }
  sendClickEventCategory() {
    this.subjectCategory.next();
  }
  sendClickEventCompany() {
    this.subjectCompany.next();
  }
  sendClickEventVesselType() {
    this.subjectVesselType.next();
  }
  sendClickEventVesselOwner() {
    this.subjectVesselOwner.next();
  }
  sendClickEventFleet() {
    this.subjectFleet.next();
  }
  sendClickEventPort() {
    this.subjectPort.next();
  }
  sendClickEventImpactType() {
    this.subjectEventImpactType.next();
  }
  sendClickEventBodyPartAffected() {
    this.subjectBodyPartAffected.next();
  }
  sendClickEventInjuryOccuredBy() {
    this.subjectInjuryOccuredBy.next();
  }
  sendClickEventBackgroundCondition() {
    this.subjectBackgroundCondition.next();
  }
  sendClickEventAnalysisSimilarIncident() {
    this.subjectAnalysisSimilarCondition.next();
  }
  sendClickEventInjuryCategory() {
    this.subjectInjuryCategory.next();
  }
  sendClickEventNationality() {
    this.subjectNationality.next();
  }
  sendClickEventThirdPartyType() {
    this.subjectThirdPartyType.next();
  }
  sendClickEventDeficiencyGroup() {
    this.subjectDeficiencyGroup.next();
  }
  sendClickEventTargetGroup() {
    this.subjectTargetGroup.next();
  }
  sendClickEventLessonType() {
    this.subjectLessonType.next();
  }
  sendClickEventLearntLessonType() {
    this.subjectLessonLearntType.next();
  }

  getClickEventTask(): Observable<any> {
    return this.subjectTask.asObservable();
  }
  getClickEventRank(): Observable<any> {
    return this.subjectRank.asObservable();
  }
  getClickEventLocation(): Observable<any> {
    return this.subjectLocation.asObservable();
  }
  getClickEventEquipment(): Observable<any> {
    return this.subjectEquipment.asObservable();
  }
  getClickEventCategory(): Observable<any> {
    return this.subjectCategory.asObservable();
  }
  getClickEventCompany(): Observable<any> {
    return this.subjectCompany.asObservable();
  }
  getClickEventVesselType() {
    return this.subjectVesselType.asObservable();
  }
  getClickEventVesselOwner() {
    return this.subjectVesselOwner.asObservable();
  }
  getClickEventFleet() {
    return this.subjectFleet.asObservable();
  }
  getClickEventPort() {
    return this.subjectPort.asObservable();
  }
  getClickEventImpactType() {
    return this.subjectEventImpactType.asObservable();
  }
  getClickEventBodyPartAffected() {
    return this.subjectBodyPartAffected.asObservable();
  }
  getClickEventInjuryOccuredBy() {
    return this.subjectInjuryOccuredBy.asObservable();
  }
  getClickEventBackgroundCondition() {
    return this.subjectBackgroundCondition.asObservable();
  }
  getClickEventAnalysisSimilarIncident() {
    return this.subjectAnalysisSimilarCondition.asObservable();
  }
  getClickEventInjuryCategory() {
    return this.subjectInjuryCategory.asObservable();
  }
  getClickEventNationality() {
    return this.subjectNationality.asObservable();
  }
  getClickEventThirdPartyType() {
    return this.subjectThirdPartyType.asObservable();
  }
  getClickEventDeficiencyGroup() {
    return this.subjectDeficiencyGroup.asObservable();
  }
  getClickEventTargetGroup() {
    return this.subjectTargetGroup.asObservable();
  }
  getClickEventLessonType() {
    return this.subjectLessonType.asObservable();
  }
  getClickEventLessonLearntType() {
    return this.subjectLessonLearntType.asObservable();
  }
}
