import { TestBed } from '@angular/core/testing';

import { AddNewDataService } from './add-new-data.service';

describe('AddNewDataService', () => {
  let service: AddNewDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddNewDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
