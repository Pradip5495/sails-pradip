import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';
import { ThirdPartyType } from '../models/lesson-type.model';

const routes = {
  thirdPartyType: {
    getAll: () => `/thirdpartytype`,
    create: () => `/thirdpartytype`,
    update: (id: string) => `/thirdpartytype/${id}`,
    delete: (id: string) => `/thirdpartytype/${id}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class ThirdPartyService {
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /* Third Party CRUD */

  getThirdPartyType(): Observable<ThirdPartyType[]> {
    return this.httpDispatcher.get<ThirdPartyType[]>(routes.thirdPartyType.getAll());
  }

  createThirdPartyType(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.thirdPartyType.create(), data);
  }

  updateThirdPartyType(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.thirdPartyType.update(id), data);
  }

  deleteThirdPartyType(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.thirdPartyType.delete(id));
  }
}
