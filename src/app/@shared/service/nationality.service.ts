import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';
import { Nationality } from '../models/lesson-type.model';

const routes = {
  nationality: {
    get: (id: string) => `/nationality/${id}`,
    getAll: () => `/nationality`,
    create: () => `/nationality`,
    update: (id: string) => `/nationality/${id}`,
    delete: (id: string) => `/nationality/${id}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class NationalityService {
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /* Nationality CRUD */

  getNationality(): Observable<Nationality[]> {
    return this.httpDispatcher.get<Nationality[]>(routes.nationality.getAll());
  }

  createNationality(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.nationality.create(), data);
  }

  updateNationality(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.nationality.update(id), data);
  }

  deleteNationality(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.nationality.delete(id));
  }
}
