import {
  Injury,
  InjuryCategoryModel,
  BackgroundConditionModel,
  AnalysisOfSimilarIncidents,
  BodyPartAffectedModel,
} from './../models/injury.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';

const routes = {
  bodyPartAffected: {
    get: (id: string) => `/bodypartaffected/${id}`,
    getAll: () => `/bodypartaffected`,
    create: () => `/bodypartaffected`,
    update: (id: string) => `/bodypartaffected/${id}`,
    delete: (id: string) => `/bodypartaffected/${id}`,
  },

  injuryOcured: {
    get: (id: string) => `/injuryoccuredby/${id}`,
    getAll: () => `/injuryoccuredby`,
    create: () => `/injuryoccuredby`,
    update: (id: string) => `/injuryoccuredby/${id}`,
    delete: (id: string) => `/injuryoccuredby/${id}`,
  },

  injuryCategory: {
    get: (id: string) => `/injurycategory/${id}`,
    getAll: () => `/injurycategory`,
    create: () => `/injurycategory`,
    update: (id: string) => `/injurycategory/${id}`,
    delete: (id: string) => `/injurycategory/${id}`,
  },

  backgroundCondition: {
    getAll: () => `/background/condition`,
    create: () => `/background/condition`,
    update: (id: string) => `/background/condition/${id}`,
    delete: (id: string) => `/background/condition/${id}`,
  },

  analysisOfSimilarIncidents: {
    getAll: () => `/analysisofsimilarincident`,
    create: () => `/analysisofsimilarincident`,
    update: (id: string) => `/analysisofsimilarincident/${id}`,
    delete: (id: string) => `/analysisofsimilarincident/${id}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class InjuryService {
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /* Body Part Affected CRUD */

  getBodyPartAffected(): Observable<BodyPartAffectedModel[]> {
    return this.httpDispatcher.get<BodyPartAffectedModel[]>(routes.bodyPartAffected.getAll());
  }

  createBodyPartAffected(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.bodyPartAffected.create(), data);
  }

  updateBodyPartAffected(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.bodyPartAffected.update(id), data);
  }

  deleteBodyPartAffected(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.bodyPartAffected.delete(id));
  }
  /* Injury Occured By CRUD */

  getInjuryOccured(): Observable<Injury[]> {
    return this.httpDispatcher.get<Injury[]>(routes.injuryOcured.getAll());
  }

  createInjuryOccured(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.injuryOcured.create(), data);
  }

  updateInjuryOccured(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.injuryOcured.update(id), data);
  }

  deleteInjuryOccured(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.injuryOcured.delete(id));
  }

  /* Injury Category CRUD */

  getInjuryCategory(): Observable<InjuryCategoryModel[]> {
    return this.httpDispatcher.get<InjuryCategoryModel[]>(routes.injuryCategory.getAll());
  }

  createInjuryCategory(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.injuryCategory.create(), data);
  }

  updateInjuryCategory(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.injuryCategory.update(id), data);
  }

  deleteInjuryCategory(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.injuryCategory.delete(id));
  }

  /* Background Condition CRUD */

  getbackgroundCondition(): Observable<BackgroundConditionModel[]> {
    return this.httpDispatcher.get<BackgroundConditionModel[]>(routes.backgroundCondition.getAll());
  }

  createbackgroundCondition(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.backgroundCondition.create(), data);
  }

  updatebackgroundCondition(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.backgroundCondition.update(id), data);
  }

  deletebackgroundCondition(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.backgroundCondition.delete(id));
  }

  /* Analysis Of Similar Incident CRUD */

  getAnalysisOfSimilarIncidents(): Observable<AnalysisOfSimilarIncidents[]> {
    return this.httpDispatcher.get<AnalysisOfSimilarIncidents[]>(routes.analysisOfSimilarIncidents.getAll());
  }

  createAnalysisOfSimilarIncidents(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.analysisOfSimilarIncidents.create(), data);
  }

  updateAnalysisOfSimilarIncidents(id: string, data: any): Observable<any> {
    return this.httpDispatcher.put(routes.analysisOfSimilarIncidents.update(id), data);
  }

  deleteAnalysisOfSimilarIncidents(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.analysisOfSimilarIncidents.delete(id));
  }
}
