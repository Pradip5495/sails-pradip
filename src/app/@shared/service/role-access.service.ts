import { Injectable } from '@angular/core';
import { HttpDispatcherService } from '@core';
import { Observable } from 'rxjs';

// API Routes Definition
const routes = {
  role: {
    get: (id: string) => `/role/${id}`,
    getAll: (filters: Map<string, string>) => (filters ? `/role?${filters.toSanitizedURLFilters()}` : `/role`),
    create: () => `/role`,
    update: (id: string) => `/role/${id}`,
    delete: (id: string) => `/role/${id}`,

    // Nested Routes
    access: {
      get: (id: string) => `/role/access/${id}`,
      getAll: (filters: Map<string, string>) =>
        filters ? `/role/access?${filters.toSanitizedURLFilters()}` : `/role/access`,
      create: () => `/role/access`,
      update: (id: string) => `/role/access/${id}`,
      delete: (id: string) => `/role/access/${id}`,
    },
  },
};

@Injectable({
  providedIn: 'root',
})
export class RoleAccessService {
  constructor(private readonly httpDispacher: HttpDispatcherService) {}

  /* Role CRUD */
  createRole(data: any): Observable<Response> {
    return this.httpDispacher.post(routes.role.create(), data);
  }

  getRole(id: string): Observable<any> {
    return this.httpDispacher.get(routes.role.get(id));
  }

  getRoles(filters?: Map<string, string>): Observable<any> {
    return this.httpDispacher.get(routes.role.getAll(filters));
  }

  updateRole(id: string, data: any): Observable<any> {
    return this.httpDispacher.put(routes.role.update(id), data);
  }

  deleteRole(id: string): Observable<any> {
    return this.httpDispacher.delete(routes.role.delete(id));
  }

  /* User Access CRUD */
  createAccess(data: any): Observable<Response> {
    return this.httpDispacher.post(routes.role.access.create(), data);
  }

  getAccess(id: string): Observable<any> {
    return this.httpDispacher.get(routes.role.access.get(id));
  }

  getAccesses(filters?: Map<string, string>): Observable<any> {
    return this.httpDispacher.get(routes.role.access.getAll(filters));
  }
}
