import { TestBed } from '@angular/core/testing';

import { CustomDataFieldService } from './custom-data-field.service';

describe('CustomDataFieldService', () => {
  let service: CustomDataFieldService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomDataFieldService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
