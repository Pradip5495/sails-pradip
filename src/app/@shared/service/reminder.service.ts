import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ReminderModel } from '@shared/models';
import { HttpDispatcherService } from '@core/http/http-dispatcher.service';

const routes = {
  reminder: {
    getAll: (filters?: Map<string, string>) => (filters ? `/reminder?${filters.toSanitizedURLFilters()}` : `/reminder`),
    getPreviousInspection: (vesselId: string, oilMajorId: string) =>
      `/reminder/previousinspectiondate/${vesselId}/${oilMajorId}`,
    create: () => `/reminder`,
    update: (id: string) => `/reminder/${id}`,
    delete: (id: string) => `/reminder/${id}`,
  },
};

@Injectable({
  providedIn: 'root',
})
export class ReminderService {
  constructor(private readonly httpDispatcher: HttpDispatcherService) {}

  /* Create Reminder */
  createReminder(data: any): Observable<any> {
    return this.httpDispatcher.post(routes.reminder.create(), data);
  }

  /* Get all Reminders */
  getReminders(filters?: Map<string, string>): Observable<ReminderModel[]> {
    return this.httpDispatcher.get<ReminderModel[]>(routes.reminder.getAll(filters));
  }

  /* Update Reminder */
  updateReminder(id: string, reminder: ReminderModel): Observable<any> {
    return this.httpDispatcher.put(routes.reminder.update(id), reminder);
  }

  /* Delete Reminder */
  deleteReminder(id: string): Observable<any> {
    return this.httpDispatcher.delete(routes.reminder.delete(id));
  }

  getPreviousInspection(vesselId: string, oilMajorId: string) {
    return this.httpDispatcher.get(routes.reminder.getPreviousInspection(vesselId, oilMajorId), {
      acceptNullResponse: true,
    });
  }
}
