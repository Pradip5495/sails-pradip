import { Component, OnInit, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-graph-card',
  templateUrl: './graph-card.component.html',
  styleUrls: ['./graph-card.component.scss'],
})
export class GraphCardComponent implements OnInit {
  @Input() header: TemplateRef<any>;
  @Input() content: TemplateRef<any>;
  @Input() footer: TemplateRef<any>;
  constructor() {}
  ngOnInit() {}
}
