import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-vessel-stats',
  templateUrl: './vessel-stats.component.html',
  styleUrls: ['./vessel-stats.component.scss'],
})
export class VesselStatsComponent implements OnInit {
  @Input() vesselsData: Array<any>;

  constructor() {}

  ngOnInit() {
    this.vesselsData = [
      { vesselName: 'Vessel 1', vesselValue: 3.3, indication: 'arrow_upward', sign: 'red' },
      { vesselName: 'Vessel 1', vesselValue: 3.3, indication: 'keyboard_backspace', sign: 'green' },
      { vesselName: 'Vessel 1', vesselValue: 3.3, indication: 'arrow_downward', sign: 'yellow' },
      { vesselName: 'Vessel 1', vesselValue: 3.3, indication: 'arrow_forward', sign: 'red' },
    ];
  }

  getClass(vesselSign: string) {
    switch (vesselSign) {
      case 'red':
        break;

      case 'green':
        break;

      case 'yellow':
        break;

      case 'red':
        break;

      default:
        break;
    }
  }
}
