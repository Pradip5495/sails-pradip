import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselStatsComponent } from './vessel-stats.component';

describe('GraphCardComponent', () => {
  let component: VesselStatsComponent;
  let fixture: ComponentFixture<VesselStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VesselStatsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VesselStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
