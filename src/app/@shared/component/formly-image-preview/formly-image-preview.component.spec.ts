import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormlyImagePreviewComponent } from './formly-image-preview.component';

describe('FormlyImagePreviewComponent', () => {
  let component: FormlyImagePreviewComponent;
  let fixture: ComponentFixture<FormlyImagePreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormlyImagePreviewComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyImagePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
