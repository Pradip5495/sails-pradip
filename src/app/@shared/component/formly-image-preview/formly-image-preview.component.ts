import { Component, OnInit, Input } from '@angular/core';
import { environment } from '@env/environment';
import { IJson } from '@types';

@Component({
  selector: 'app-formly-image-preview',
  templateUrl: './formly-image-preview.component.html',
  styleUrls: ['./formly-image-preview.component.scss'],
})
export class FormlyImagePreviewComponent implements OnInit {
  API_URL = environment.filePath;

  @Input()
  model: IJson = {};

  @Input()
  formState: IJson = {};

  @Input()
  props: IJson = {};

  ngOnInit(): void {}

  getFilterFile(receivedImageData: any) {
    const imageTypeAssetMapping = {
      png: this.API_URL + '/' + receivedImageData.path,
      jpg: this.API_URL + '/' + receivedImageData.path,
      jpeg: this.API_URL + '/' + receivedImageData.path,
      zip: '/assets/zip.png',
      ' ': '/assets/zip.png',
      pdf: '/assets/pdf.png',
    };
    return imageTypeAssetMapping[receivedImageData.fileType] || 'assets/file.png';
  }

  /**
   * return image class based on file type
   */
  getImageStyleClass(imageType: any) {
    const imageTypeStyleMapping = {
      png: 'upload-images',
      jpg: 'upload-images',
      jpeg: 'upload-images',
    };

    return imageTypeStyleMapping[imageType] || 'pdfimages';
  }

  deleteImage($event: any, image: any) {
    this.props.delete(image);
  }
}
