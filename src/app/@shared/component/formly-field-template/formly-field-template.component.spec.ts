import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormlyFieldTemplateComponent } from './formly-field-template.component';

describe('FormlyFieldTemplateComponent', () => {
  let component: FormlyFieldTemplateComponent;
  let fixture: ComponentFixture<FormlyFieldTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormlyFieldTemplateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyFieldTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
