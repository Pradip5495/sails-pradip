import { Component, OnInit, Input } from '@angular/core';
import { IJson } from '@types';

@Component({
  selector: 'app-formly-field-template',
  templateUrl: './formly-field-template.component.html',
  styleUrls: ['./formly-field-template.component.scss'],
})
export class FormlyFieldTemplateComponent implements OnInit {
  @Input()
  model: IJson = {};

  @Input()
  formState: IJson = {};

  @Input()
  props: IJson = {};

  isSeverityValue: boolean;

  severityClass: string;

  constructor() {}

  ngOnInit(): void {
    this.isSeverityValue = 'isSeverityValue' in this.props;
  }

  getSeverityClass() {
    const severityValue: string = this.formState[this.props.key] || '';
    switch (severityValue.toLowerCase()) {
      case 'trival':
        this.severityClass = 'severity-trival-level';
        break;
      case 'minor':
        this.severityClass = 'severity-minor-level';
        break;
      case 'moderate':
        this.severityClass = 'severity-moderate-level';
        break;
      case 'major':
        this.severityClass = 'severity-major-level';
        break;
      case 'extreme':
        this.severityClass = 'severity-extreme-level';
        break;
      default:
        this.severityClass = 'test';
        break;
    }
    return this.severityClass;
  }
}
