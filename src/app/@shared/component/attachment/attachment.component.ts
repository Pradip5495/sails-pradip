import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IJson } from '@types';

@Component({
  selector: 'app-attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss'],
})
export class AttachmentComponent {
  images: any[] = [];
  allFiles: File[] = [];

  @Input()
  emptyLabel?: boolean;

  @Input()
  attachmentLabel?: string;

  @Input()
  preview = false;

  @Input()
  id = 'input-file-id';

  @Input()
  className = '';

  @Input()
  multiple = false;

  @Input()
  accept: string;

  @Input()
  attachIcon = 'attach_file';

  @Output()
  OnFileChange: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  OnDeleteImage = new EventEmitter<IJson>();

  get label() {
    if (this.emptyLabel) {
      return '';
    }
    return this.attachmentLabel || 'ATTACHMENT';
  }

  /**
   * return filter image URL based on file type
   */
  getFilterFile(imageType: any) {
    const imageTypeAssetMapping = {
      'image/png': imageType.url,
      'image/jpg': imageType.url,
      'image/jpeg': imageType.url,
      'application/x-zip-compressed': '/assets/zip.png',
      ' ': '/assets/zip.png',
      'application/pdf': '/assets/pdf.png',
    };
    return imageTypeAssetMapping[imageType.type] || 'assets/file.png';
  }

  /**
   * return image class based on file type
   */
  getImageStyleClass(imageType: any) {
    const imageTypeStyleMapping = {
      'image/png': 'upload-images',
      'image/jpg': 'upload-images',
      'image/jpeg': 'upload-images',
    };

    return imageTypeStyleMapping[imageType.type] || 'pdfimages';
  }

  deleteImage($event: any, image: any) {
    $event.preventDefault();
    const index = this.images.indexOf(image);

    this.OnDeleteImage.emit({ deletedImage: this.images[index], index });

    this.images.splice(index, 1);
    this.allFiles.splice(index, 1);
    this.OnFileChange.emit(this.allFiles);
  }

  onChange(event: any) {
    this.updatePreview(event);
    this.OnFileChange.emit(event.target.files);
  }

  updatePreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const { name, type, size } = event.target.files[i] as File;
        const image = { name, type, size, url: '' };

        this.allFiles.push(event.target.files[i] as File);

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.images.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }
}
