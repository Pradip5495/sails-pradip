import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'app-customized-cell',
  template: `
    <a *ngIf="this.params.showCustomIcon" (click)="onClickCustomIcon($event)">
      <mat-icon class="mat-icon material-icons mat-icon-no-color">{{ customIcon }}</mat-icon>
    </a>

    <a *ngIf="!this.params.hideDetailsButton" (click)="onClickDetails($event)"
      ><mat-icon class="mat-icon material-icons mat-icon-no-color">visibility</mat-icon></a
    >
    <a (click)="onClickEdit($event)" *ngIf="!this.params.hideEditIcon"
      ><mat-icon class="mat-icon material-icons mat-icon-no-color">create</mat-icon></a
    >
    <a (click)="onClick($event)" *ngIf="!this.params.hideDeleteIcon"
      ><mat-icon class="mat-icon material-icons mat-icon-no-color">delete</mat-icon></a
    >
  `,
  styleUrls: ['./customized-cell.component.css'],
})
export class CustomizedCellComponent implements OnInit, ICellRendererAngularComp {
  error: string | undefined;
  params: any;
  values: any;
  label: string;
  customIcon: string;

  constructor() {}

  ngOnInit(): void {}

  agInit(params: any) {
    this.params = params;
    this.label = this.params.label || null;
    this.customIcon = this.params.customIconName || 'list';
  }

  refresh(params: any): boolean {
    return true;
  }

  onClick($event: any) {
    if (this.params.onClick instanceof Function) {
      const params = {
        event: $event,
        rowData: this.params.node.data,
      };
      this.params.onClick(params);
    }
  }

  onClickEdit($event: any) {
    if (this.params.onClickEdit instanceof Function) {
      const params = {
        event: $event,
        rowData: this.params.node.data,
        api: this.params.api,
        rowIndex: this.params.rowIndex,
      };
      this.params.onClickEdit(params);
    }
  }

  onClickDetails($event: any) {
    if (this.params.onClickDetails instanceof Function) {
      const params = {
        event: $event,
        rowData: this.params.node.data,
        api: this.params.api,
        rowIndex: this.params.rowIndex,
      };
      this.params.onClickDetails(params);
    }
  }

  onClickCustomIcon(event: any) {
    if (this.params.onCustomIconClick instanceof Function) {
      const params = {
        event,
        rowData: this.params.node.data,
      };
      this.params.onCustomIconClick(params);
    }
  }
}
