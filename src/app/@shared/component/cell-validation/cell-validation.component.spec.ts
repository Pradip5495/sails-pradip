import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CellValidationComponent } from './cell-validation.component';

describe('CellValidationComponent', () => {
  let component: CellValidationComponent;
  let fixture: ComponentFixture<CellValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CellValidationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CellValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
