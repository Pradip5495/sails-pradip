import { isEmpty } from 'lodash';
import { Component, ViewChild, ViewContainerRef, AfterViewInit } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { Logger } from '@core/logger.service';

const log = new Logger('customized-cell');

const KEY_BACKSPACE = 8;
const KEY_DELETE = 46;
const KEY_F2 = 113;

@Component({
  selector: 'app-cell-validation',
  template: `
    <div class="input-container">
      <input
        matInput
        #input
        (keydown)="onKeyDown($event)"
        [(ngModel)]="value"
        [className]="iserror ? 'input2' : 'input1'"
        style="width: 100%"
        (keyup)="onKey($event)"
        required
      />
    </div>
    <p *ngIf="iserror">This field is required</p>
    <p *ngIf="isInvalidMail">Invalid Email Address</p>
  `,
  styleUrls: ['./cell-validation.component.css'],
})
export class CellValidationComponent implements ICellRendererAngularComp, AfterViewInit {
  public params: any;
  public iserror: any = false;
  public isInvalidMail: any = false;
  public value: any;
  public highlightAllOnFocus: any = true;
  public inputValue: any = '';

  @ViewChild('input', { read: ViewContainerRef }) public input: any;

  agInit(params: any): void {
    log.debug('inputValue:', this.inputValue);
    log.debug('input', this.input);
    log.debug('parama', params);
    this.params = params;
    this.setInitialState(this.params);
  }

  refresh(params: any): boolean {
    return true;
  }

  onKey(event: any) {
    // without type info
    this.inputValue = event.target.value;
  }

  setInitialState(params: any) {
    let startValue;
    let highlightAllOnFocus = true;

    log.debug('param', params);

    if (params.keyPress === KEY_BACKSPACE || params.keyPress === KEY_DELETE) {
      log.debug('startValue1', startValue);
      // if backspace or delete pressed, we clear the cell
      startValue = '';
    } else if (params.charPress) {
      log.debug('param4', params);
      startValue = params.charPress;
      highlightAllOnFocus = false;
      log.debug('startValue4', startValue);
    } else {
      // otherwise we start with the current value
      startValue = params.value;
      log.debug('startValue6', startValue);
      if (params.keyPress === KEY_F2) {
        highlightAllOnFocus = false;
      }
    }

    this.value = startValue;
    log.debug('value', this.value);
    log.debug('startValue7', startValue);
    this.highlightAllOnFocus = highlightAllOnFocus;
  }

  getValue(): any {
    log.debug('getvalue', this.value);
    if (this.isEmpty(this.value)) {
      this.iserror = true;
      log.debug('field is required');
    }
    return this.value;
  }

  isEmpty(val: any) {
    return val === undefined || val === null || val === '';
  }

  ValidateEmail(val: any) {
    log.debug('val:', val);
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val);
  }

  // will reject the number if it greater than 1,000,000
  // not very practical, but demonstrates the method.
  isCancelAfterEnd(): boolean {
    return isEmpty(this.value);
  }

  onKeyDown(event: any): void {
    const val = event.target.value;
    if (this.isEmpty(val)) {
      this.iserror = true;
      this.isInvalidMail = false;
    } else {
      if (this.params.colDef.field === 'email' && !this.ValidateEmail(val)) {
        log.debug('email wrong');
        this.iserror = false;
        this.isInvalidMail = true;
        event.target.classList.add('input2'); // To ADD
        event.target.classList.remove('input1'); // To Remove
      } else {
        this.iserror = false;
        this.isInvalidMail = false;
        event.target.classList.add('input1'); // To ADD
        event.target.classList.remove('input2'); // To Remove
      }
    }
  }

  // dont use afterGuiAttached for post gui events - hook into ngAfterViewInit instead for this
  ngAfterViewInit() {
    window.setTimeout(() => {
      this.input.element.nativeElement.focus();
      if (this.highlightAllOnFocus) {
        this.input.element.nativeElement.select();

        this.highlightAllOnFocus = false;
      } else {
        // when we started editing, we want the carot at the end, not the start.
        // this comes into play in two scenarios: a) when user hits F2 and b)
        // when user hits a printable character, then on IE (and only IE) the carot
        // was placed after the first character, thus 'apply' would end up as 'pplea'
        const length = this.input.element.nativeElement.value ? this.input.element.nativeElement.value.length : 0;
        if (length > 0) {
          this.input.element.nativeElement.setSelectionRange(length, length);
        }
      }

      this.input.element.nativeElement.focus();
    });
  }
}
