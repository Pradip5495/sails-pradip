import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'app-ag-cell-button',
  template: `
    <button
      *ngIf="buttonProp.type === 'raised'"
      mat-raised-button
      (click)="onClick()"
      [class]="styleButton(buttonProp.color)"
      class="no-padding"
    >
      <mat-icon *ngIf="buttonProp.icon"> {{ buttonProp.icon }} </mat-icon> {{ buttonProp.label }}
    </button>
    <button
      *ngIf="buttonProp.type === 'flat'"
      mat-flat-button
      (click)="onClick()"
      [class]="styleButton(buttonProp.color)"
      class="no-padding"
    >
      <mat-icon *ngIf="buttonProp.icon"> {{ buttonProp.icon }} </mat-icon> {{ buttonProp.label }}
    </button>
    <button
      *ngIf="buttonProp.type === 'stroked'"
      mat-stroked-button
      (click)="onClick()"
      [class]="styleButton(buttonProp.color)"
      class="no-padding"
    >
      <mat-icon *ngIf="buttonProp.icon"> {{ buttonProp.icon }} </mat-icon> {{ buttonProp.label }}
    </button>
  `,
  styles: [
    `
      .no-padding: {
        padding: 0px !important;
      }

      .success-button {
        background: #33b84c !important;
        width: 100% !important;
        color: white !important;
      }

      .danger-button {
        background: #ea7180 !important;
        width: 100% !important;
        color: white !important;
      }

      .warning-button {
        background: #f5a52d !important;
        width: 100% !important;
        color: white !important;
      }

      .plain-button {
        width: fit-content;
        color: #16569e !important;
      }
    `,
  ],
})
export class AgCellButtonComponent implements OnInit, ICellRendererAngularComp {
  buttonProp: { icon: string; label: string; color: string; type: string } = {
    icon: 'favorite',
    label: 'Default',
    color: '',
    type: 'raised',
  };

  params: any;

  constructor() {}

  ngOnInit(): void {}

  agInit(params: any) {
    this.params = params;
    this.buttonProp = {
      icon: params.icon ? params.icon : '',
      label: params.label ? params.label : 'Default',
      color: params.buttonColor ? params.buttonColor : '',
      type: params.buttonType ? params.buttonType : 'raised',
    };
  }

  refresh(params: any): boolean {
    return true;
  }

  onClick() {
    if (this.params.onClick instanceof Function) {
      this.params.onClick(this.params);
    }
  }

  styleButton(buttonColor: string) {
    if (!buttonColor) {
      return { 'plain-button': true };
    }

    switch (buttonColor) {
      case 'success':
        return { 'success-button': true };

      case 'danger':
        return { 'danger-button': true };

      case 'warning':
        return { 'warning-button': true };

      default:
        return { 'plain-button': true };
    }
  }
}
