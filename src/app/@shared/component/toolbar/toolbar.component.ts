import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@formly';
import { ICustomFormlyConfig, IJson } from '@types';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  public showFilter = false;
  public showGrid = true;
  public showColumn = false;
  selectWidth = 9;
  @Input()
  title: string;

  @Input()
  subtitle: string;

  @Input()
  selectOptions: Array<IJson>;

  @Input()
  selectValue: string;

  @Input()
  selectOptionValue = 'value';

  @Input()
  selectOptionLabel = 'label';

  @Input()
  navigateButtonTitle: string;

  @Input()
  buttonIcon: string;

  @Input()
  svgIcon: string;

  @Input()
  form: FormGroup;

  @Input()
  model: any;

  @Input()
  options: FormlyFormOptions;

  @Input()
  fields: FormlyFieldConfig[];

  @Input()
  newFormLink: string;

  @Input()
  gridColumnShowHide: boolean;

  @Input()
  navigationButtonDisabled = false;

  @Output()
  ngFilter: EventEmitter<any> = new EventEmitter();

  @Output()
  ngClear: EventEmitter<any> = new EventEmitter();

  @Output()
  ngNavigateClick: EventEmitter<any> = new EventEmitter();

  @Output()
  ngShowYear: EventEmitter<any> = new EventEmitter();

  @Output()
  ngShowMonths: EventEmitter<any> = new EventEmitter();

  @Output()
  ClickGrid: EventEmitter<any> = new EventEmitter();

  @Output()
  ClickColumn: EventEmitter<any> = new EventEmitter();

  @Output()
  headerSelectionChanged: EventEmitter<any> = new EventEmitter();

  formlyConfig: ICustomFormlyConfig[];

  get isDataReceived() {
    return !!this.model && !!this.fields && !!this.form && !!this.formlyConfig;
  }

  constructor() {}

  toggleFilter() {
    this.showFilter = !this.showFilter;
    if (this.showFilter) {
      this.formlyConfig[0].config.fields = this.fields;
    }
  }

  onFilter(model: object) {
    this.ngFilter.emit(); // Notify Parent for further actions
  }

  onClear() {
    this.ngClear.emit(); // Notify Parent for further actions
  }

  onNavigateButton() {
    this.ngNavigateClick.emit(); // Notify Parent for further actions
  }

  setSelectFieldWidth(selectedObject: IJson) {
    if (this.selectValue && selectedObject) {
      const selectValue = selectedObject[this.selectOptionLabel].toLowerCase();
      const selectValueLength = selectValue.length;
      this.selectWidth = selectValueLength > 9 ? selectValueLength / 1.5 + 1 : selectValueLength;
    }
  }

  onSelectionChange(event: any) {
    if (event) {
      const selectedObject: IJson = this.selectOptions.filter(
        (option) => option[this.selectOptionValue] === event.value
      );
      if (selectedObject.length > 0) {
        this.headerSelectionChanged.emit(selectedObject[0]);
        this.setSelectFieldWidth(selectedObject[0]);
      }
    }
  }

  getColumn() {
    this.showColumn = true;
    this.showGrid = false;
    this.ClickColumn.emit(this.showColumn);
  }
  getList() {
    this.showGrid = true;
    this.showColumn = false;
    this.ClickGrid.emit(this.showGrid);
  }

  ngOnInit(): void {
    this.formlyConfig = [
      {
        config: {
          model: this.model,
          fields: this.fields,
          options: {},
          form: this.form,
        },
        legacyForm: true,
      },
    ];
  }
}
