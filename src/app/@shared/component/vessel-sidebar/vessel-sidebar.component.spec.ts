import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselSidebarComponent } from './vessel-sidebar.component';

describe('VesselSidebarComponent', () => {
  let component: VesselSidebarComponent;
  let fixture: ComponentFixture<VesselSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VesselSidebarComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VesselSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
