import { Component, OnInit } from '@angular/core';
import { IToolPanel, IToolPanelParams } from '@ag-grid-community/core';
import { FormlyFieldConfig } from '@formly';
import { FormGroup } from '@angular/forms';
import { VesselService } from '@shared/service';
import { Logger } from '@core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { SidenavStoreService } from '@app/shell/sidenav/sidenav-store.service';

const log = new Logger('vessel-datalist');

@Component({
  selector: 'app-vessel-sidebar',
  templateUrl: './vessel-sidebar.component.html',
  styleUrls: ['./vessel-sidebar.component.css'],
})
export class VesselSidebarComponent implements OnInit, IToolPanel {
  form = new FormGroup({});
  model: any = {};
  fields: FormlyFieldConfig[] = [
    {
      key: 'Input',
      type: 'input',
      templateOptions: {
        label: 'Input',
        placeholder: 'Placeholder',
        description: 'Description',
        required: true,
      },
    },
  ];

  selected3: any[] = [];
  vesselIdArray: any[] = [];
  public vessels: any;
  public searchText: string;
  public vessel: any;

  constructor(private readonly vesselService: VesselService, private leftSideNavStore: SidenavStoreService) {}

  ngOnInit(): void {}

  agInit(params: IToolPanelParams): void {
    const myVesselFilter = new Map<string, string>().set('myvessel', 'true');
    this.vesselService.getVessels(myVesselFilter).subscribe((vessels) => {
      log.debug('vesselid:', vessels);
      this.vessels = vessels;
    });
  }

  refresh(): void {
    throw new Error('Method not implemented.');
  }

  toggle(vessel: string, event: MatCheckboxChange) {
    if (event.checked) {
      this.selected3.push(vessel);
    } else {
      const index = this.selected3.indexOf(vessel);
      if (index >= 0) {
        this.selected3.splice(index, 1);
      }
    }

    /*  for(const i of this.selected3)
    {
      this.vesselIdArray=i.vesselId ;
     log.debug('vesselIdArray',this.vesselIdArray);
    }*/
    this.leftSideNavStore.myVessels = this.selected3;
  }
  exists(vessel: any) {
    return this.selected3.indexOf(vessel) > -1;
  }

  isIndeterminate() {
    return this.selected3.length > 0 && !this.isChecked();
  }

  isChecked() {
    return this.selected3.length === this.vessels.length;
  }

  toggleAll(event: MatCheckboxChange) {
    if (event.checked) {
      this.vessels.forEach((row: any) => {
        // console.log('checked row', row);
        this.selected3.push(row);
      });

      // console.log('checked here');
    } else {
      // console.log('checked false');
      this.selected3.length = 0;
    }
    /*  for(const i of this.selected3)
    {
      this.vesselIdArray=i.vesselId ;
     log.debug('vesselIdArray',this.vesselIdArray);
    }
    this.leftSideNavStore.myVessels=this.vesselIdArray;
  }*/
    this.leftSideNavStore.myVessels = this.selected3;
  }
}
