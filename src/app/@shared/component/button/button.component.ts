import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent {
  @Input()
  icon?: string;

  @Input()
  label: string;

  @Output()
  OnClick = new EventEmitter();

  onButtonClick(event: any) {
    this.OnClick.emit(event);
  }
}
