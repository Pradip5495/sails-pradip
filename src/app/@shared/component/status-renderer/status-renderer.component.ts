import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'app-status-renderer',
  template: ` <mat-slide-toggle [checked]="check" (change)="toggle($event)"></mat-slide-toggle> `,
  styleUrls: ['./status-renderer.component.css'],
})
export class StatusRendererComponent implements OnInit, ICellRendererAngularComp {
  error: string | undefined;
  params: any;
  label: string;
  public check: any;

  constructor() {}

  ngOnInit(): void {}

  agInit(params: any) {
    this.params = params;
    this.label = this.params.label || null;
    if (params.data.isActive === true || params.data.isActive === 1) {
      this.check = true;
    } else {
      this.check = false;
    }
  }
  refresh(params: any): boolean {
    return true;
  }

  toggle($event: any) {
    if (this.params.toggle instanceof Function) {
      // put anything into params u want pass into parents component
      const params = {
        event: $event,
        rowData: this.params.node.data,
        api: this.params.api,
        rowIndex: this.params.rowIndex,
        // ...something
      };
      this.params.toggle(params);
    }
  }
}
