import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRowButtonsComponent } from './new-row-buttons.component';

describe('NewRowButtonsComponent', () => {
  let component: NewRowButtonsComponent;
  let fixture: ComponentFixture<NewRowButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewRowButtonsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRowButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
