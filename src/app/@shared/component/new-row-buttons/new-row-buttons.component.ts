import { Component, OnInit } from '@angular/core';
import { Logger } from '@core/logger.service';

const log = new Logger('new-row-buttons');

@Component({
  selector: 'app-new-row-buttons',
  template: `
    <a (click)="onSaveClick($event)" type="submit" mat-raised-button>Save</a>
    <button (click)="onCancelClick($event)" type="button" mat-raised-button>Cancel</button>
  `,
  styleUrls: ['./new-row-buttons.component.css'],
})
export class NewRowButtonsComponent implements OnInit {
  error: string | undefined;
  params: any;
  label: string;

  ngOnInit(): void {}
  agInit(params: any) {
    this.params = params;
    this.label = this.params.label || null;
  }
  refresh(params: any): boolean {
    return true;
  }

  onSaveClick($event: any) {
    if (this.params.onSaveClick instanceof Function) {
      // put anything into params u want pass into parents component
      log.debug('paramvalues:', this.params);
      const params = {
        event: $event,
        rowData: this.params.node.data,
      };
      this.params.onSaveClick(params);
    }
  }

  onCancelClick($event: any) {
    if (this.params.onCancelClick instanceof Function) {
      // put anything into params u want pass into parents component
      const params = {
        event: $event,
        rowData: this.params.node.data,
        api: this.params.api,
        rowIndex: this.params.rowIndex,
      };
      this.params.onCancelClick(params);
    }
  }
}
