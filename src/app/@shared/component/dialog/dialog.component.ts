import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogComponent implements OnChanges, OnDestroy {
  @ViewChild('dialog', { static: true }) _dialogContent: TemplateRef<any>;

  @Input()
  dialog?: boolean;

  @Output()
  beforeClosed: BehaviorSubject<MatDialogRef<any, any>> = new BehaviorSubject<MatDialogRef<any, any>>(null);

  @Output()
  afterClosed: BehaviorSubject<MatDialogRef<any, any>> = new BehaviorSubject<MatDialogRef<any, any>>(null);

  @Output()
  afterOpened: BehaviorSubject<MatDialogRef<any, any>> = new BehaviorSubject<MatDialogRef<any, any>>(null);

  @Input()
  header: TemplateRef<any>;

  @Input()
  content: TemplateRef<any>;

  @Input()
  footer: TemplateRef<any>;

  @Input()
  width: number;

  @Input()
  height: number;

  @Input()
  headerClass = '';

  @Input()
  panelClass: string;

  _opened = false;

  private ngUnsubscribe = new Subject();
  private _dialogRef: MatDialogRef<any>;

  constructor(private _dialog: MatDialog) {}

  open() {
    const dialogProps: any = {};

    if (this.width) {
      dialogProps.width = this.width;
    }

    if (this.height) {
      dialogProps.height = this.height;
    }

    if (this.panelClass) {
      dialogProps.panelClass = this.panelClass;
    }

    this._dialogRef = this._dialog.open(this._dialogContent, dialogProps);

    this._dialogRef.beforeClosed().pipe(takeUntil(this.ngUnsubscribe)).subscribe(this.onBeforeClosed.bind(this));
    this._dialogRef.afterClosed().pipe(takeUntil(this.ngUnsubscribe)).subscribe(this.onAfterClosed.bind(this));
    this._dialogRef.afterOpened().pipe(takeUntil(this.ngUnsubscribe)).subscribe(this.onAfterOpened.bind(this));
  }

  close() {
    if (this._dialogRef) {
      this._dialogRef.close();
    }
  }

  onBeforeClosed(value: any) {
    this.beforeClosed.next(value);
  }

  onAfterClosed(value: any) {
    this._opened = false;
    this.afterClosed.next(value);
  }

  onAfterOpened(value: any) {
    this.afterOpened.next(value);
  }

  ngOnChanges() {
    if (this.dialog && !this._opened) {
      this._opened = true;
      this.open();
    }

    if (!this.dialog && this._opened) {
      this.close();
    }
  }

  ngOnDestroy() {
    if (this.ngUnsubscribe) {
      this.ngUnsubscribe.next();
      this.ngUnsubscribe.complete();
    }
  }
}
