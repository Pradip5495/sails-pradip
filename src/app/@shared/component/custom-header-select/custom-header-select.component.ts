import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-loading-overlay',
  template: `
    <div>
      <div class="customHeaderLabel">
        <mat-form-field class="heading-style">
          <mat-select [value]="defaultValue" (selectionChange)="onSelectChange($event)">
            <mat-option [value]="90"> +3 MONTHS </mat-option>
            <mat-option [value]="180"> +6 MONTHS </mat-option>
            <mat-option [value]="270"> +9 MONTHS </mat-option>
            <mat-option [value]="360"> +12 MONTHS </mat-option>
          </mat-select>
        </mat-form-field>
      </div>
    </div>
  `,
  styles: [
    `
      .customHeaderMenuButton,
      .customHeaderLabel,
      .customSortDownLabel,
      .customSortUpLabel,
      .customSortRemoveLabel {
        float: left;
        margin: 0 0 0 3px;
      }

      .customSortUpLabel {
        margin: 0;
      }

      .customSortRemoveLabel {
        font-size: 11px;
      }

      .active {
        color: cornflowerblue;
      }
      :host ::ng-deep .mat-form-field-appearance-legacy .mat-form-field-wrapper {
        padding-bottom: 0.25rem !important;
      }

      :host ::ng-deep .mat-form-field-appearance-legacy .mat-form-field-underline {
        height: 0px !important;
        display: none;
      }

      .heading-style {
        color: #fff;
        font-weight: 500;
        font-size: 14px;
      }

      :host ::ng-deep .mat-select-value {
        width: 86%;
      }

      :host ::ng-deep .mat-select-arrow {
        margin: 0px 4px 0px 0px;
        color: white !important;
      }

      :host ::ng-deep .mat-select-value {
        text-align: center;
        color: white !important;
      }
    `,
  ],
})
export class CustomHeaderSelectComponent {
  params: any;
  defaultValue: number;

  ascSort: string;
  descSort: string;
  noSort: string;

  @ViewChild('menuButton', { read: ElementRef }) public menuButton: ElementRef;

  agInit(params: any): void {
    this.params = params;
    const $defaultValue = params.onDefaultValue();
    $defaultValue.subscribe((value: any) => {
      this.defaultValue = value > 0 ? value : 180;
    });

    params.column.addEventListener('sortChanged', this.onSortChanged.bind(this));
    this.onSortChanged();
  }

  onMenuClicked(event: any) {
    this.params.showColumnMenu(this.menuButton.nativeElement);
  }

  onSortChanged() {
    this.ascSort = this.descSort = this.noSort = 'inactive';
    if (this.params.column.isSortAscending()) {
      this.ascSort = 'active';
    } else if (this.params.column.isSortDescending()) {
      this.descSort = 'active';
    } else {
      this.noSort = 'active';
    }
  }

  onSelectChange(event: any) {
    this.params.onChange(event);
  }

  onSortRequested(order: any, event: any) {
    this.params.setSort(order, event.shiftKey);
  }
}
