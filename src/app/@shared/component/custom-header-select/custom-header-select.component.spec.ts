import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomHeaderSelectComponent } from './custom-header-select.component';

describe('CustomHeaderSelectComponent', () => {
  let component: CustomHeaderSelectComponent;
  let fixture: ComponentFixture<CustomHeaderSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomHeaderSelectComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomHeaderSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
