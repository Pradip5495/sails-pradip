import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'app-two-progress-bar-cell.component',
  template: `
    <div *ngIf="tasksCompleted !== 100 && timeElapsed !== 100">
      <mat-progress-bar
        mode="determinate"
        [value]="timeElapsed"
        [style.height]="timeBarHeight"
        color="success"
      ></mat-progress-bar>
      <section *ngIf="tasksCompleted > 0" class="progress-bar-section-2" title="{{ tasksCompleted + '%' }}">
        <mat-progress-bar
          #progress
          [style.height]="tasksBarHeight"
          [color]="color"
          [mode]="'determinate'"
          [value]="tasksCompleted"
        >
        </mat-progress-bar>
        <div *ngIf="!hidePercentageLabel" class="percent-value">
          {{ tasksCompleted == 100 ? 'READY' : tasksCompleted + '%' }}
        </div>
      </section>
    </div>
    <div *ngIf="tasksCompleted === 100 && timeElapsed === 100" class="ready-content">Ready</div>
  `,
  styleUrls: ['./two-progress-bar-cell.component.scss', '../progress-bar-cell/progress-bar-cell.component.scss'],
})
export class TwoProgressBarCellComponent implements ICellRendererAngularComp {
  tasksCompleted = 0;
  timeElapsed = 0;
  color = 'primary';
  hidePercentageLabel = false;
  tasksBarHeight = '22px';
  timeBarHeight = '6px';

  agInit(params: any) {
    this.setValues(params.data);
    this.setColor(this.tasksCompleted);
  }

  setValues(paramsData: any) {
    if (paramsData) {
      this.tasksCompleted = paramsData.tasksCompleted ? paramsData.tasksCompleted : this.tasksCompleted;
      this.timeElapsed = paramsData.timeElapsed ? paramsData.timeElapsed : this.timeElapsed;
    }
  }

  setColor(percentage: number) {
    if (percentage <= 25) {
      this.color = 'danger';
    }

    if (percentage > 25 && percentage <= 50) {
      this.color = 'warn';
    }

    if (percentage > 50 && percentage <= 75) {
      this.color = 'primary';
    }

    if (percentage > 75 && percentage <= 100) {
      this.color = 'success';
    }
  }

  refresh(params: any) {
    return true;
  }
}
