import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'cell-progress-bar',
  template: `
    <section *ngIf="percentage > 0" class="progress-bar-section" title="{{ percentage + '%' }}">
      <mat-progress-bar
        #progress
        class="progress-bar"
        [style.height]="height"
        [color]="color"
        [mode]="'determinate'"
        [value]="percentage"
      >
      </mat-progress-bar>
      <div *ngIf="!hidePercentageLabel" class="percent-value">{{ percentage == 100 ? 'READY' : percentage + '%' }}</div>
    </section>
  `,
  styleUrls: ['./progress-bar-cell.component.scss'],
})
export class ProgressBarCellRendererComponent implements ICellRendererAngularComp {
  percentage = 0;
  color = 'primary';
  height: number = null;
  hidePercentageLabel = false;

  agInit(params: any) {
    this.setValues(params);
    this.setColor(this.percentage);
  }

  setColor(percentage: number) {
    if (percentage <= 25) {
      this.color = 'danger';
    }

    if (percentage > 25 && percentage <= 50) {
      this.color = 'warn';
    }

    if (percentage > 50 && percentage <= 75) {
      this.color = 'primary';
    }

    if (percentage > 75 && percentage <= 100) {
      this.color = 'success';
    }
  }

  setValues(params: any) {
    if (typeof params.value?.value === 'undefined') {
      this.percentage = params.value;
      return;
    }

    const data = params.value;
    if (data && data.value) {
      this.percentage = data.value;
    }

    if (data && data.height) {
      this.height = data.height;
    }

    if (data.hidePercentageLabel) {
      this.hidePercentageLabel = data.hidePercentageLabel;
    }
  }

  refresh(params: any) {
    return true;
  }
}
