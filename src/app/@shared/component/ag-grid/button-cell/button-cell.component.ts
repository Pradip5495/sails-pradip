import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { Component } from '@angular/core';

@Component({
  selector: 'cell-button',
  template: `
    <button
      *ngIf="label"
      mat-raised-button
      [color]="color"
      [class]="className"
      [disabled]="disabled"
      (click)="buttonClicked()"
    >
      <mat-icon class="material-icons" *ngIf="matIcon || svgIcon" [svgIcon]="svgIcon || ''">{{
        matIcon || ''
      }}</mat-icon>
      {{ label }}
    </button>
  `,
})
export class ButtonCellRendererComponent implements ICellRendererAngularComp {
  label = '';
  color = 'primary';
  matIcon = '';
  svgIcon = '';
  className = '';
  disabled = false;
  params: any;

  agInit(params: any) {
    this.params = params;
    if (params.label) {
      this.label = params.label;
    }
    if (params.color) {
      this.color = params.color;
    }

    if (params.className) {
      this.className = params.className;
    }

    if (params.matIcon) {
      this.matIcon = params.matIcon;
    }

    if (params.svgIcon) {
      this.svgIcon = params.svgIcon;
    }

    if (params.value?.disabled) {
      this.disabled = params.value.disabled;
    }
  }

  refresh(params: any) {
    return true;
  }

  buttonClicked() {
    this.params.onClick(this.params);
  }
}
