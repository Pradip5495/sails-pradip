import { AgGridAngular } from '@ag-grid-community/angular';
import { GridOptions } from '@ag-grid-community/core';
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { IJson } from '@types';

@Component({
  selector: 'app-ag-grid',
  templateUrl: './ag-grid.component.html',
  styleUrls: ['./ag-grid.component.scss'],
})
export class AgGridComponent {
  @Input()
  gridOptions: GridOptions;

  @Input()
  rowSelection: 'single' | 'multiple' = 'multiple';

  @Input()
  pagination = true;

  @Input()
  animateRows: any = true;

  @Input()
  allowContextMenuWithControlKey: any = true;

  @Input()
  rowMultiSelectWithClick = true;

  @Input()
  columnDefs: any;

  @Input()
  getRowHeight: any;

  @Input()
  rowData: any = [];

  @Input()
  sideBar: any;

  @Input()
  defaultColDef: any;

  @Input()
  frameworkComponents: any;

  @Input()
  icons: any;

  @Input()
  editType: any;

  @Input()
  paginationPageSize: any;

  @Input()
  domLayout: any;

  @Input()
  aGrid: AgGridAngular;

  @Input()
  headerHeight: number;

  @Input()
  masterDetail = false;

  @Input()
  modules: any[] = [];

  @Input()
  detailCellRendererParams: any;

  @Input()
  className = '';

  @Input()
  suppressRowTransform: boolean;

  @Input()
  groupHeaderHeight: number;

  @Input()
  suppressRowClickSelection = false;

  @Input()
  detailCellRenderer: string;

  @Input()
  detailRowHeight: number;

  @Input()
  components?: IJson;

  @Input()
  suppressPaginationPanel?: boolean;

  @Output()
  gridReady: EventEmitter<any> = new EventEmitter();

  @Output()
  selectionChanged: EventEmitter<any> = new EventEmitter();

  @Output()
  rowSelected: EventEmitter<any> = new EventEmitter();

  @Output()
  cellValueChanged: EventEmitter<any> = new EventEmitter();

  @Output()
  firstDataRendered: EventEmitter<any> = new EventEmitter();

  @ViewChild('agGrid') agGrid: AgGridAngular;

  onGridReady(params: IJson) {
    this.gridReady.emit(params);
  }

  onSelectionChanged(params: any) {
    this.selectionChanged.emit(params);
  }

  onCellValueChanged(params: any) {
    this.cellValueChanged.emit(params);
  }

  onfirstDataRendered(params: IJson) {
    this.firstDataRendered.emit(params);
  }

  onRowSelected(event: any) {
    this.rowSelected.emit(event);
  }
}
