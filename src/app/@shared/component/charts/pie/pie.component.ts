import { AfterViewInit, Component, Inject, Input, NgZone, OnDestroy, PLATFORM_ID } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { isPlatformBrowser } from '@angular/common';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.scss'],
})
export class PieComponent implements OnDestroy, AfterViewInit {
  @Input()
  id = 'pie';

  @Input()
  addLegend = true;

  @Input()
  chartData: Subject<any[]>;

  @Input()
  pieChartSize = 90;

  @Input()
  pieChartHeight = '20rem';

  @Input()
  disableLegend = false;

  @Input()
  addLegendScroll = false;

  @Input()
  legendHeight = 80;

  private chart: am4charts.XYChart;

  constructor(@Inject(PLATFORM_ID) private platformId: any, private zone: NgZone) {}

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      if (this.chartData) {
        this.chartData.subscribe((data: any[]) => {
          if (data) {
            am4core.useTheme(am4themes_animated);
            this.createChart(data);
          } else {
            this.createChart();
          }
        });
      } else {
        this.createChart();
      }
    });
  }

  createChart(chartData?: any[]) {
    // Create chart instance
    const chart = am4core.create(this.id, am4charts.PieChart);

    // Add data
    if (chartData) {
      chartData.map((singleData: any) => {
        singleData.color = am4core.color(singleData.color);
      });
      chart.data = chartData;
    } else {
      chart.data = [
        {
          country: 'Lithuania',
          litres: 501.9,
          color: am4core.color('#fff000'),
        },
        {
          country: 'Czechia',
          litres: 301.9,
          color: am4core.color('#eee'),
        },
        {
          country: 'Ireland',
          litres: 201.1,
          color: am4core.color('#ddd'),
        },
      ];
    }

    // chart.legend = new am4charts.Legend();

    // Add and configure Series
    const pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = 'litres';
    pieSeries.dataFields.category = 'country';
    pieSeries.slices.template.propertyFields.fill = 'color';
    pieSeries.slices.template.stroke = am4core.color('#fff');
    pieSeries.slices.template.strokeOpacity = 1;

    // This creates initial animation
    pieSeries.hiddenState.properties.opacity = 1;
    pieSeries.hiddenState.properties.endAngle = -90;
    pieSeries.hiddenState.properties.startAngle = -90;
    pieSeries.labels.template.text = '';

    if (this.addLegend) {
      chart.legend = new am4charts.Legend();
      // chart.legend.width = 250;
      chart.legend.labels.template.maxWidth = 100;
      chart.legend.valueLabels.template.width = 80;
      chart.legend.labels.template.truncate = false;
      chart.legend.labels.template.wrap = true;

      if (this.addLegendScroll) {
        chart.legend.maxHeight = this.legendHeight;
        chart.legend.scrollable = true;
      }

      // chart.legend.itemContainers.template.tooltipText = "{country}";
      // chart.legend.valueLabels.template.text = "{value.country}";

      // chart.legend.useDefaultMarker = true;
      // chart.legend.width = 600;
      // chart.legend.scrollable = true;
      // chart.legend.labels.template.maxWidth = 95;

      // chart.legend.labels.template.width = 60;
      // chart.legend.labels.template.truncate = false;
      // chart.legend.labels.template.wrap = true;
      const markerTemplate = chart.legend.markers.template;
      markerTemplate.width = 15;
      markerTemplate.height = 15;
    }

    chart.radius = am4core.percent(this.pieChartSize);

    chart.hiddenState.properties.radius = am4core.percent(0);
  }
}
