import { AfterViewInit, Component, Inject, Input, NgZone, OnDestroy, PLATFORM_ID } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { isPlatformBrowser } from '@angular/common';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-multi-color-step-count',
  templateUrl: './multi-color-step-count.component.html',
  styleUrls: ['./multi-color-step-count.component.scss'],
})
export class MultiColorStepCountComponent implements OnDestroy, AfterViewInit {
  @Input()
  id = 'multi-color-step-count';

  @Input()
  columnWidth = 10;

  @Input()
  columnWithCurvedBorder = true;

  @Input()
  borderCurve = 0;

  @Input()
  legendIconWidth = 10;

  @Input()
  legendIconHeight = 10;

  @Input()
  legendPosition: any = 'bottom';

  @Input()
  chartHeight = '25rem';

  @Input()
  minGridDistance = 70;

  @Input()
  yAxisLabel = 'Expenditure (s)';

  @Input()
  disableyAxisLabel = false;

  @Input()
  disableXAxisGrid = true;

  @Input()
  disableYAxisGrid = false;

  @Input()
  disableLegend = false;

  @Input()
  chartData: Subject<any[]>;

  @Input()
  chartConfig: Array<{ columnField: string; fieldDescription: string; columnColor: string }> = [
    { columnField: 'europe', fieldDescription: 'North America', columnColor: '#e54f60' },
    { columnField: 'namerica', fieldDescription: 'Asia-Pacific', columnColor: '#f5a52e' },
    { columnField: 'namerica', fieldDescription: 'Asia-Pacific', columnColor: '#f1cd1c' },
    { columnField: 'lamerica', fieldDescription: 'Latin America', columnColor: '#2ec43f' },
  ];

  private chart: am4charts.XYChart;

  constructor(@Inject(PLATFORM_ID) private platformId: any, private zone: NgZone) {}

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      this.browserOnly(() => {
        if (this.chartData) {
          this.chartData.subscribe((data: any[]) => {
            if (data) {
              am4core.useTheme(am4themes_animated);
              this.createChart(data);
            } else {
              this.createChart();
            }
          });
        } else {
          this.createChart();
        }
      });
    });
  }

  createChart(chartData?: any) {
    // Create chart instance
    const chart = am4core.create(this.id, am4charts.XYChart);

    // Add data
    if (chartData) {
      chart.data = chartData;
    } else {
      chart.data = [
        {
          year: 'Grounding Stand',
          europe: 2.5,
          namerica: 2.5,
          asia: 2.1,
          lamerica: 0.3,
          meast: 0.2,
          africa: 0.1,
        },
        {
          year: 'Fire Explosion',
          europe: 2.6,
          namerica: 2.7,
          asia: 2.2,
          lamerica: 0.3,
          meast: 0.3,
          africa: 0.1,
        },
        {
          year: 'Detection',
          europe: 2.8,
          namerica: 2.9,
          asia: 2.4,
          lamerica: 0.3,
          meast: 0.3,
          africa: 0.1,
        },
        {
          year: 'Cargo Incident',
          europe: 2.6,
          namerica: 2.7,
          asia: 2.2,
          lamerica: 0.3,
          meast: 0.3,
          africa: 0.1,
        },
        {
          year: 'Security',
          europe: 2.8,
          namerica: 2.9,
          asia: 2.4,
          lamerica: 0.3,
          meast: 0.3,
          africa: 0.1,
        },
      ];
    }

    // Create axes
    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'year';
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.labels.template.wrap = true;
    categoryAxis.renderer.grid.template.disabled = this.disableXAxisGrid;

    chart.legend = new am4charts.Legend();
    chart.legend.position = this.legendPosition;
    chart.legend.paddingBottom = 20;
    chart.legend.disabled = this.disableLegend;
    chart.legend.labels.template.maxWidth = undefined;

    const markerTemplate = chart.legend.markers.template;
    markerTemplate.width = this.legendIconWidth;
    markerTemplate.height = this.legendIconHeight;

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.inside = false;
    valueAxis.title.text = this.yAxisLabel;
    valueAxis.renderer.labels.template.disabled = this.disableyAxisLabel;
    valueAxis.renderer.minGridDistance = this.minGridDistance;
    valueAxis.renderer.grid.template.disabled = this.disableYAxisGrid;
    valueAxis.min = 0;

    this.chart = chart;

    this.chartConfig.forEach((data) => {
      this.createSeries(data.columnField, data.fieldDescription, data.columnColor);
    });
  }

  // Create series
  createSeries(field: any, name: string, color: string) {
    // Set up series
    const series = this.chart.series.push(new am4charts.ColumnSeries());
    series.name = name;
    series.dataFields.valueY = field;
    series.dataFields.categoryX = 'year';
    series.sequencedInterpolation = true;
    series.columns.template.fill = am4core.color(color);
    series.columns.template.marginBottom = -10;
    series.columns.template.stroke = am4core.color('#ffffff00');
    // series.columns.template.width = am4core.percent(this.columnWidth);
    series.columns.template.width = this.columnWidth;

    // Bar Radius
    if (this.columnWithCurvedBorder) {
      series.columns.template.column.adapter.add('cornerRadiusTopLeft', this.topRadius.bind(this));
      series.columns.template.column.adapter.add('cornerRadiusTopRight', this.topRadius.bind(this));
      series.columns.template.column.adapter.add('cornerRadiusBottomLeft', this.bottomRadius.bind(this));
      series.columns.template.column.adapter.add('cornerRadiusBottomRight', this.bottomRadius.bind(this));
    }

    // Make it stacked
    series.stacked = true;

    // Configure columns
    series.columns.template.tooltipText = '[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}';

    // Add label
    const labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = '{valueY}';
    labelBullet.locationY = 0.5;
    labelBullet.label.hideOversized = true;

    return series;
  }

  bottomRadius(radius: any, target: any) {
    return target.dataItem && target.dataItem.valueY < 0 ? 0 : this.borderCurve;
  }

  topRadius(radius: any, target: any) {
    return target.dataItem && target.dataItem.valueY < 0 ? 0 : this.borderCurve;
  }
}
