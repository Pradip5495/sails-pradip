import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiColorStepCountComponent } from './multi-color-step-count.component';

describe('MultiColorStepCountComponent', () => {
  let component: MultiColorStepCountComponent;
  let fixture: ComponentFixture<MultiColorStepCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MultiColorStepCountComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiColorStepCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
