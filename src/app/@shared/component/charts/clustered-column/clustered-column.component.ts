import { AfterViewInit, Component, Inject, Input, NgZone, OnDestroy, PLATFORM_ID } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { isPlatformBrowser } from '@angular/common';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-clustered-column',
  templateUrl: './clustered-column.component.html',
  styleUrls: ['./clustered-column.component.scss'],
})
export class ClusteredColumnComponent implements OnDestroy, AfterViewInit {
  @Input()
  id = 'clustered-column';

  @Input()
  columnWithCurvedBorder = true;

  @Input()
  borderCurve = 7;

  @Input()
  categoryFieldName = 'category';

  @Input()
  columnWidth = 20;

  @Input()
  legendIconWidth = 10;

  @Input()
  legendIconHeight = 10;

  @Input()
  cellStartLocation = 0.2;

  @Input()
  cellEndLocation = 0.8;

  @Input()
  legendPosition: any = 'bottom';

  @Input()
  disableLegend = false;

  @Input()
  chartHeight = '40rem';

  @Input()
  minGridDistance = 40;

  @Input()
  disableXAxisGrid = true;

  @Input()
  disableYAxisGrid = false;

  @Input()
  chartConfig: Array<{ columnField: string; fieldDescription: string; columnColor: string }> = [
    { columnField: 'first', fieldDescription: 'No. of Incident', columnColor: '#20c43f' },
    { columnField: 'second', fieldDescription: 'Incident Rating', columnColor: '#0081c8' },
  ];

  @Input()
  chartData: Subject<any[]>;

  @Input()
  addLegendScroll = false;

  @Input()
  legendHeight = 80;

  private chart: am4charts.XYChart;
  private xAxis: am4charts.CategoryAxis<am4charts.AxisRenderer>;

  constructor(@Inject(PLATFORM_ID) private platformId: any, private zone: NgZone) {}

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngOnDestroy() {}

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      if (this.chartData) {
        this.chartData.subscribe((data: any[]) => {
          if (data) {
            am4core.useTheme(am4themes_animated);
            this.createChart(data);
          } else {
            this.createChart();
          }
        });
      } else {
        this.createChart();
      }
    });
  }

  createChart(chartData?: any) {
    const chart = am4core.create(this.id, am4charts.XYChart);
    chart.colors.step = 2;

    if (!this.disableLegend) {
      chart.legend = new am4charts.Legend();
      chart.legend.position = this.legendPosition;
      // chart.legend.paddingBottom = 20;
      chart.legend.labels.template.maxWidth = undefined;

      if (this.addLegendScroll) {
        chart.legend.maxHeight = this.legendHeight;
        chart.legend.scrollable = true;
      }

      const markerTemplate = chart.legend.markers.template;
      markerTemplate.width = this.legendIconWidth;
      markerTemplate.height = this.legendIconHeight;
    }

    const xAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    xAxis.dataFields.category = this.categoryFieldName;
    xAxis.renderer.cellStartLocation = this.cellStartLocation;
    xAxis.renderer.cellEndLocation = this.cellEndLocation;
    xAxis.renderer.grid.template.location = 0;
    xAxis.renderer.grid.template.disabled = this.disableXAxisGrid;
    this.xAxis = xAxis;

    const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
    yAxis.renderer.grid.template.disabled = this.disableYAxisGrid;
    yAxis.renderer.minGridDistance = this.minGridDistance;
    yAxis.min = 0;

    if (chartData) {
      chart.data = chartData;
    } else {
      chart.data = [
        {
          category: 'Vessel 1',
          first: 40,
          second: 55,
          third: 60,
        },
        {
          category: 'Vessel 2',
          first: 30,
          second: 78,
          third: 69,
        },
        {
          category: 'Vessel 3',
          first: 27,
          second: 40,
          third: 45,
        },
        {
          category: 'Vessel 4',
          first: 50,
          second: 33,
          third: 22,
        },
        {
          category: 'Vessel 5',
          first: 50,
          second: 33,
          third: 22,
        },
        {
          category: 'Vessel 6',
          first: 50,
          second: 33,
          third: 22,
        },
      ];
    }

    this.chart = chart;

    this.chartConfig.forEach((data) => {
      this.createSeries(data.columnField, data.fieldDescription, data.columnColor);
    });
  }

  createSeries(value: any, name: string, color: string) {
    const series = this.chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = value;
    series.dataFields.categoryX = this.categoryFieldName;
    series.name = name;
    series.columns.template.tooltipText = '{name}: {categoryX}: {valueY}';
    series.columns.template.fill = am4core.color(color);
    series.columns.template.stroke = am4core.color('#ffffff00');

    // Bar width
    // series.columns.template.width = this.columnWidth;
    series.columns.template.width = am4core.percent(this.columnWidth);

    if (this.columnWithCurvedBorder) {
      // Bar Radius
      series.columns.template.column.adapter.add('cornerRadiusTopLeft', this.topRadius.bind(this));
      series.columns.template.column.adapter.add('cornerRadiusTopRight', this.topRadius.bind(this));
      series.columns.template.column.adapter.add('cornerRadiusBottomLeft', this.bottomRadius.bind(this));
      series.columns.template.column.adapter.add('cornerRadiusBottomRight', this.bottomRadius.bind(this));
    }

    series.events.on('hidden', this.arrangeColumns.bind(this));
    series.events.on('shown', this.arrangeColumns.bind(this));

    const bullet = series.bullets.push(new am4charts.LabelBullet());
    bullet.interactionsEnabled = false;
    bullet.dy = 30;
    bullet.label.text = '';
    bullet.label.fill = am4core.color('#ffffff');

    return series;
  }

  arrangeColumns() {
    const series = this.chart.series.getIndex(0);

    const w = 1 - this.xAxis.renderer.cellStartLocation - (1 - this.xAxis.renderer.cellEndLocation);
    if (series.dataItems.length > 1) {
      const x0 = this.xAxis.getX(series.dataItems.getIndex(0), 'categoryX');
      const x1 = this.xAxis.getX(series.dataItems.getIndex(1), 'categoryX');
      const delta = ((x1 - x0) / this.chart.series.length) * w;
      if (am4core.isNumber(delta)) {
        const middle = this.chart.series.length / 2;

        let newIndex = 0;
        this.chart.series.each((_series) => {
          if (!_series.isHidden && !_series.isHiding) {
            _series.dummyData = newIndex;
            newIndex++;
          } else {
            _series.dummyData = this.chart.series.indexOf(_series);
          }
        });
        const visibleCount = newIndex;
        const newMiddle = visibleCount / 2;

        this.chart.series.each((_series) => {
          const trueIndex = this.chart.series.indexOf(_series);
          const _newIndex = _series.dummyData;

          const dx = (_newIndex - trueIndex + middle - newMiddle) * delta;

          _series.animate({ property: 'dx', to: dx }, _series.interpolationDuration, _series.interpolationEasing);
          _series.bulletsContainer.animate(
            { property: 'dx', to: dx },
            _series.interpolationDuration,
            _series.interpolationEasing
          );
        });
      }
    }
  }

  bottomRadius(radius: any, target: any) {
    return target.dataItem && target.dataItem.valueY < 0 ? 0 : this.borderCurve;
  }

  topRadius(radius: any, target: any) {
    return target.dataItem && target.dataItem.valueY < 0 ? 0 : this.borderCurve;
  }
}
