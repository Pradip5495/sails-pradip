import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleColorStepCountComponent } from './single-color-step-count.component';

describe('SingleColorStepCountComponent', () => {
  let component: SingleColorStepCountComponent;
  let fixture: ComponentFixture<SingleColorStepCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SingleColorStepCountComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleColorStepCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
