import { AfterViewInit, Component, Input, NgZone, OnDestroy } from '@angular/core';

// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_material from '@amcharts/amcharts4/themes/material';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

@Component({
  selector: 'app-bullet-chart',
  templateUrl: './bullet-chart.component.html',
})
export class BulletChartComponent implements AfterViewInit, OnDestroy {
  @Input() chartData: any = {};

  // Do you want to allow chart animation? -> true or false
  @Input() isAnimate = true;

  // Which color type to be used? -> 'solid' or 'gradient'
  @Input() colorType = 'solid';

  // Which colors you want to use in the background? -> Array required
  @Input() colors: any = ['#19d228', '#b4dd1e', '#f4fb16', '#f6d32b', '#fb7116'];

  // Which symbol you want to use for X axis values? -> Ex. '$', '%', 'INR' etc.
  @Input() numberFormat = '%';

  @Input() id = 'bullet-chart-container';

  container: am4core.Container;

  constructor(private zone: NgZone) {}

  // Run the function only in the browser
  browserOnly(f: () => void) {
    this.zone.runOutsideAngular(() => {
      f();
    });
  }

  ngAfterViewInit() {
    this.browserOnly(() => {
      am4core.useTheme(am4themes_material);

      if (this.isAnimate) {
        am4core.useTheme(am4themes_animated);
      }

      // Find and intialize bullet chart container in view
      this.container = am4core.create(this.id, am4core.Container);

      // Set width
      this.container.width = am4core.percent(100);

      // Set height
      this.container.height = am4core.percent(100);

      // Set layout
      this.container.layout = 'vertical';

      this.createBulletChart(this.container, this.colorType);
    });
  }

  /* Create ranges */
  createRange(axis: any, from: any, to: any, color: any) {
    const range = axis.axisRanges.create();
    range.value = from;
    range.endValue = to;
    range.axisFill.fill = color;
    range.axisFill.fillOpacity = 0.8;
    range.label.disabled = true;
    range.grid.disabled = true;
  }

  /* Create bullet chart with specified color type for background */
  createBulletChart(parent: any, colorType: any) {
    const colors = this.colors;

    /* Create chart instance */
    const chart = this.container.createChild(am4charts.XYChart);
    chart.paddingRight = 25;

    /* Assign received chart data */
    chart.data = [this.chartData];

    /* Create axes */
    const categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'category';
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.grid.template.disabled = true;

    const valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
    // valueAxis.renderer.minGridDistance = 30;
    // valueAxis.renderer.grid.template.disabled = true;
    valueAxis.min = 0;
    valueAxis.max = 100;
    valueAxis.strictMinMax = true;
    valueAxis.numberFormatter.numberFormat = '#\'' + this.numberFormat + '\'';
    valueAxis.renderer.baseGrid.disabled = true;

    /*
      In order to create separate background colors for each range of values,
      you have to create multiple axisRange objects each with their own fill color
    */
    if (colorType === 'solid') {
      let start = 0;
      let end = 20;
      for (const i of colors.length) {
        this.createRange(valueAxis, start, end, am4core.color(colors[i]));
        start += 20;
        end += 20;
      }
    } else if (colorType === 'gradient') {
      /*
      In order to create a gradient background, only one axisRange object is needed
      and a gradient object can be assigned to the axisRange's fill property.
    */
      const gradient = new am4core.LinearGradient();
      for (const i of colors.length) {
        // add each color that makes up the gradient
        gradient.addColor(am4core.color(colors[i]));
      }
      this.createRange(valueAxis, 0, 100, gradient);
    }

    /* Create series */
    const series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueX = 'value';
    series.dataFields.categoryY = 'category';
    series.columns.template.fill = am4core.color('#000');
    series.columns.template.stroke = am4core.color('#fff');
    series.columns.template.strokeWidth = 1;
    series.columns.template.strokeOpacity = 0.5;
    series.columns.template.height = am4core.percent(25);
    series.tooltipText = '{value}';

    const series2 = chart.series.push(new am4charts.StepLineSeries());
    series2.dataFields.valueX = 'target';
    series2.dataFields.categoryY = 'category';
    series2.strokeWidth = 3;
    series2.noRisers = true;
    series2.startLocation = 0.15;
    series2.endLocation = 0.85;
    series2.tooltipText = '{valueX}';
    series2.stroke = am4core.color('#000');

    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineX.disabled = true;
    chart.cursor.lineY.disabled = true;

    valueAxis.cursorTooltipEnabled = false;
    chart.arrangeTooltips = false;
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.container) {
        this.container.dispose();
      }
    });
  }
}
