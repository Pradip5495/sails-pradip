import { AfterViewInit, Component, Inject, Input, NgZone, OnDestroy, PLATFORM_ID } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { isPlatformBrowser } from '@angular/common';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-angular-gauge',
  templateUrl: './angular-gauge.component.html',
  styleUrls: ['./angular-gauge.component.scss'],
})
export class AngularGaugeComponent implements AfterViewInit, OnDestroy {
  @Input()
  id = 'angular-gauge';

  @Input()
  chartData: Subject<any[]>;

  @Input()
  chartSize = 90;

  @Input()
  chartHeight = '20rem';

  gaugeLabel: boolean;

  @Input()
  handValue: number;

  private chart: am4charts.XYChart;

  constructor(@Inject(PLATFORM_ID) private platformId: any, private zone: NgZone) {}

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      am4core.useTheme(am4themes_animated);
      this.createChart();
    });
  }

  createChart() {
    // create chart
    const chart = am4core.create(this.id, am4charts.GaugeChart);
    chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect

    chart.innerRadius = am4core.percent(75);
    chart.startAngle = -195;
    chart.endAngle = 15;

    const axis = chart.xAxes.push(new am4charts.ValueAxis() as any);
    axis.min = 0;
    axis.max = 280;
    axis.strictMinMax = true;
    axis.renderer.grid.template.stroke = new am4core.InterfaceColorSet().getFor('background');
    axis.renderer.grid.template.strokeOpacity = 0.25;
    axis.renderer.minGridDistance = 60;

    const range0 = axis.axisRanges.create();
    range0.value = 0;
    range0.endValue = 165;
    range0.axisFill.fillOpacity = 1;
    range0.axisFill.fill = '#52baf3';
    range0.axisFill.zIndex = -1;

    const range1 = axis.axisRanges.create();
    range1.value = 165;
    range1.endValue = 225;
    range1.axisFill.fillOpacity = 1;
    range1.axisFill.fill = '#03a9f4';
    range1.axisFill.zIndex = -1;

    const range2 = axis.axisRanges.create();
    range2.value = 225;
    range2.endValue = 280;
    range2.axisFill.fillOpacity = 1;
    range2.axisFill.fill = '#16569e';
    range2.axisFill.zIndex = -1;

    const hand = chart.hands.push(new am4charts.ClockHand());
    hand.showValue(this.handValue || 30);
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
