import { NgModule } from '@angular/core';

import { GaugeComponent } from './gauge/gauge.component';
import { ClusteredColumnComponent } from './clustered-column/clustered-column.component';
import { VariableRadiusPieChartComponent } from './variable-radius-pie/variable-radius-pie.component';
import { SingleColorStepCountComponent } from './single-color-step-count/single-color-step-count.component';
import { MultiColorStepCountComponent } from './multi-color-step-count/multi-color-step-count.component';
import { AnimatedGaugeComponent } from './animated-gauge/animated-gauge.component';
import { HighlightingLineLegendHoverComponent } from './highlighting-line-legend-hover/highlighting-line-legend-hover.component';
import { TestingComponent } from './testing/testing.component';
import { PieComponent } from './pie/pie.component';
import { StackedBarComponent } from './stacked-bar/stacked-bar.component';
import { CommonModule } from '@angular/common';
import { BulletChartComponent } from './bullet-chart/bullet-chart.component';
import { ZoomableRadarComponent } from './zoomable-radar/zoomable-radar.component';
import { LineWithColorsComponent } from './line-with-colors/line-with-colors.component';
import { AngularGaugeComponent } from './angular-gauge/angular-gauge.component';

@NgModule({
  declarations: [
    GaugeComponent,
    ClusteredColumnComponent,
    VariableRadiusPieChartComponent,
    SingleColorStepCountComponent,
    MultiColorStepCountComponent,
    AnimatedGaugeComponent,
    HighlightingLineLegendHoverComponent,
    TestingComponent,
    PieComponent,
    StackedBarComponent,
    BulletChartComponent,
    ZoomableRadarComponent,
    LineWithColorsComponent,
    AngularGaugeComponent,
  ],
  imports: [CommonModule],
  exports: [
    GaugeComponent,
    ClusteredColumnComponent,
    VariableRadiusPieChartComponent,
    SingleColorStepCountComponent,
    MultiColorStepCountComponent,
    AnimatedGaugeComponent,
    HighlightingLineLegendHoverComponent,
    TestingComponent,
    PieComponent,
    StackedBarComponent,
    AngularGaugeComponent,
  ],
})
export class ChartsModule {}
