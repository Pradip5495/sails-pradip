import { AfterViewInit, Component, Inject, Input, NgZone, OnDestroy, PLATFORM_ID } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-highlighting-line-legend-hover',
  templateUrl: './highlighting-line-legend-hover.component.html',
  styleUrls: ['./highlighting-line-legend-hover.component.scss'],
})
export class HighlightingLineLegendHoverComponent implements OnDestroy, AfterViewInit {
  @Input()
  id = 'highlight-line-legend-hover';

  private chart: am4charts.XYChart;

  constructor(@Inject(PLATFORM_ID) private platformId: any, private zone: NgZone) {}

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      am4core.useTheme(am4themes_animated);
      this.createChart();
    });
  }

  createChart() {
    // Create chart instance
    const chart = am4core.create(this.id, am4charts.XYChart);

    this.chart = chart;
    // Create axes
    chart.xAxes.push(new am4charts.DateAxis());
    chart.yAxes.push(new am4charts.ValueAxis());

    for (let i = 0; i < 10; i++) {
      this.createSeries('value' + i, 'Series #' + i);
    }

    chart.legend = new am4charts.Legend();
    chart.legend.position = 'right';
    chart.legend.scrollable = true;
    chart.legend.itemContainers.template.events.on('over', (event) => {
      this.processOver(event.target.dataItem.dataContext);
    });

    chart.legend.itemContainers.template.events.on('out', (event) => {
      this.processOut(event.target.dataItem.dataContext);
    });

    this.chart = chart;
  }

  // Create series
  createSeries(s: any, name: string) {
    const series = this.chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = 'value' + s;
    series.dataFields.dateX = 'date';
    series.name = name;

    const segment = series.segments.template;
    segment.interactionsEnabled = true;

    const hoverState = segment.states.create('hover');
    hoverState.properties.strokeWidth = 3;

    const dimmed = segment.states.create('dimmed');
    dimmed.properties.stroke = am4core.color('#dadada');

    segment.events.on('over', (event) => {
      this.processOver(event.target.parent.parent.parent);
    });

    segment.events.on('out', (event) => {
      this.processOut(event.target.parent.parent.parent);
    });

    const data = [];
    let value = Math.round(Math.random() * 100) + 100;
    for (let i = 1; i < 100; i++) {
      value += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 30 + i / 5);
      const dataItem = { date: new Date(2018, 0, i) };
      dataItem['value' + s] = value;
      data.push(dataItem);
    }

    series.data = data;
    return series;
  }

  processOver(hoveredSeries: any) {
    hoveredSeries.toFront();

    hoveredSeries.segments.each((segment: any) => {
      segment.setState('hover');
    });

    this.chart.series.each((series: any) => {
      if (series !== hoveredSeries) {
        series.segments.each((segment: any) => {
          segment.setState('dimmed');
        });
        series.bulletsContainer.setState('dimmed');
      }
    });
  }

  processOut(hoveredSeries: any) {
    this.chart.series.each((series: any) => {
      series.segments.each((segment: any) => {
        segment.setState('default');
      });
      series.bulletsContainer.setState('default');
    });
  }
}
