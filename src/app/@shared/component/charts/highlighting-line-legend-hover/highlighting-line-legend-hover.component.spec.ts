import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighlightingLineLegendHoverComponent } from './highlighting-line-legend-hover.component';

describe('HighlightingLineLegendHoverComponent', () => {
  let component: HighlightingLineLegendHoverComponent;
  let fixture: ComponentFixture<HighlightingLineLegendHoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HighlightingLineLegendHoverComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighlightingLineLegendHoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
