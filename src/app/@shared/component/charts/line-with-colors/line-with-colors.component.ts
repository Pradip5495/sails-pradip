import { AfterViewInit, Component, Input, NgZone, OnDestroy } from '@angular/core';

// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_material from '@amcharts/amcharts4/themes/material';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

@Component({
  selector: 'app-line-with-colors',
  templateUrl: './line-with-colors.component.html',
  styleUrls: ['./line-with-colors.component.scss'],
})
export class LineWithColorsComponent implements AfterViewInit, OnDestroy {
  @Input() chartData: any = [];

  // Do you want to allow chart animation? -> true or false
  @Input() isAnimate = true;

  @Input()
  id = 'line-with-color-chart';

  chart: am4charts.XYChart;

  constructor(private zone: NgZone) {}

  // Run the function only in the browser
  browserOnly(f: () => void) {
    this.zone.runOutsideAngular(() => {
      f();
    });
  }

  ngAfterViewInit() {
    this.browserOnly(() => {
      am4core.useTheme(am4themes_material);

      if (this.isAnimate) {
        am4core.useTheme(am4themes_animated);
      }

      // Find and intialize bullet chart in view
      this.chart = am4core.create(this.id, am4charts.XYChart);

      this.createBulletChart();
    });
  }

  /* Create line chart with specified color */
  createBulletChart() {
    this.chart.paddingRight = 20;

    const data = this.chartData;
    let visits = 10;
    let previousValue;

    for (let i = 0; i < 100; i++) {
      visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);

      if (i > 0) {
        // add color to previous data item depending on whether current value is less or more than previous value
        if (previousValue <= visits) {
          data[i - 1].color = this.chart.colors.getIndex(0);
        } else {
          data[i - 1].color = this.chart.colors.getIndex(5);
        }
      }

      data.push({ date: new Date(2018, 0, i + 1), value: visits });
      previousValue = visits;
    }

    this.chart.data = data;

    const dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.grid.template.location = 0;
    dateAxis.renderer.axisFills.template.disabled = true;
    dateAxis.renderer.ticks.template.disabled = true;

    const valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.minWidth = 35;
    valueAxis.renderer.axisFills.template.disabled = true;
    valueAxis.renderer.ticks.template.disabled = true;

    const series = this.chart.series.push(new am4charts.LineSeries());
    series.dataFields.dateX = 'date';
    series.dataFields.valueY = 'value';
    series.strokeWidth = 2;
    series.tooltipText = 'value: {valueY}, day change: {valueY.previousChange}';
    // set stroke property field
    series.propertyFields.stroke = 'color';

    this.chart.cursor = new am4charts.XYCursor();

    const scrollbarX = new am4core.Scrollbar();
    this.chart.scrollbarX = scrollbarX;

    dateAxis.start = 0.7;
    dateAxis.keepSelection = true;
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
