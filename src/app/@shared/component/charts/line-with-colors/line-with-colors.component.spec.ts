import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineWithColorsComponent } from './line-with-colors.component';

describe('LineWithColorsComponent', () => {
  let component: LineWithColorsComponent;
  let fixture: ComponentFixture<LineWithColorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LineWithColorsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineWithColorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
