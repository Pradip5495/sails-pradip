import { AfterViewInit, Component, Input, NgZone, OnDestroy } from '@angular/core';

// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_material from '@amcharts/amcharts4/themes/material';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

@Component({
  selector: 'app-zoomable-radar',
  templateUrl: './zoomable-radar.component.html',
  styleUrls: ['./zoomable-radar.component.scss'],
})
export class ZoomableRadarComponent implements AfterViewInit, OnDestroy {
  @Input() chartData: any = {};

  // Do you want to allow chart animation? -> true or false
  @Input() isAnimate = true;

  @Input()
  id = 'zoomable-chart';

  chart: am4charts.RadarChart;

  constructor(private zone: NgZone) {}

  // Run the function only in the browser
  browserOnly(f: () => void) {
    this.zone.runOutsideAngular(() => {
      f();
    });
  }

  ngAfterViewInit() {
    this.browserOnly(() => {
      am4core.useTheme(am4themes_material);

      if (this.isAnimate) {
        am4core.useTheme(am4themes_animated);
      }

      // Find and intialize zoomable chart in view
      this.chart = am4core.create(this.id, am4charts.RadarChart);
      this.chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

      // Set width
      this.chart.width = am4core.percent(100);

      // Set height
      this.chart.height = am4core.percent(100);

      this.createChart();
    });
  }

  /* Create zoomable chart with specified color type for background */
  createChart() {
    this.chart.padding(20, 20, 20, 20);

    this.chart.data = this.chartData;

    const categoryAxis = this.chart.xAxes.push(new am4charts.CategoryAxis() as any);
    categoryAxis.dataFields.category = 'category';
    categoryAxis.renderer.labels.template.location = 0.5;
    categoryAxis.renderer.tooltipLocation = 0.5;

    const valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis() as any);
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.labels.template.horizontalCenter = 'left';
    valueAxis.min = 0;

    const series1 = this.chart.series.push(new am4charts.RadarColumnSeries());
    series1.columns.template.tooltipText = '{name}: {valueY.value}';
    series1.columns.template.width = am4core.percent(80);
    series1.name = 'Series 1';
    series1.dataFields.categoryX = 'category';
    series1.dataFields.valueY = 'value1';
    series1.stacked = true;

    const series2 = this.chart.series.push(new am4charts.RadarColumnSeries());
    series2.columns.template.width = am4core.percent(80);
    series2.columns.template.tooltipText = '{name}: {valueY.value}';
    series2.name = 'Series 2';
    series2.dataFields.categoryX = 'category';
    series2.dataFields.valueY = 'value2';
    series2.stacked = true;

    const series3 = this.chart.series.push(new am4charts.RadarColumnSeries());
    series3.columns.template.width = am4core.percent(80);
    series3.columns.template.tooltipText = '{name}: {valueY.value}';
    series3.name = 'Series 3';
    series3.dataFields.categoryX = 'category';
    series3.dataFields.valueY = 'value3';
    series3.stacked = true;

    const series4 = this.chart.series.push(new am4charts.RadarColumnSeries());
    series4.columns.template.width = am4core.percent(80);
    series4.columns.template.tooltipText = '{name}: {valueY.value}';
    series4.name = 'Series 4';
    series4.dataFields.categoryX = 'category';
    series4.dataFields.valueY = 'value4';
    series4.stacked = true;

    this.chart.seriesContainer.zIndex = -1;

    this.chart.scrollbarX = new am4core.Scrollbar();
    this.chart.scrollbarX.exportable = false;
    this.chart.scrollbarY = new am4core.Scrollbar();
    this.chart.scrollbarY.exportable = false;

    this.chart.cursor = new am4charts.RadarCursor();
    this.chart.cursor.xAxis = categoryAxis;
    this.chart.cursor.fullWidthLineX = true;
    this.chart.cursor.lineX.strokeOpacity = 0;
    this.chart.cursor.lineX.fillOpacity = 0.1;
    this.chart.cursor.lineX.fill = am4core.color('#000000');
  }

  ngOnDestroy(): void {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
