import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomableRadarComponent } from './zoomable-radar.component';

describe('ZoomableRadarComponent', () => {
  let component: ZoomableRadarComponent;
  let fixture: ComponentFixture<ZoomableRadarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ZoomableRadarComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomableRadarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
