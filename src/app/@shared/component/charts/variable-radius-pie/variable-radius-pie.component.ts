import { AfterViewInit, Component, Inject, Input, NgZone, OnDestroy, PLATFORM_ID } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { isPlatformBrowser } from '@angular/common';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-variable-radius-pie-chart',
  templateUrl: './variable-radius-pie.component.html',
  styleUrls: ['./variable-radius-pie.component.scss'],
})
export class VariableRadiusPieChartComponent implements OnDestroy, AfterViewInit {
  @Input()
  id = 'variable-radius-pie';

  @Input()
  addLegend = true;

  @Input()
  chartData: Subject<any[]>;

  @Input()
  pieChartSize = 90;

  @Input()
  pieChartHeight = '20rem';

  @Input()
  valueFieldName = 'value';

  @Input()
  categoryFieldName = 'country';

  @Input()
  addLegendScroll = false;

  @Input()
  legendHeight = 80;

  private chart: am4charts.PieChart;

  constructor(@Inject(PLATFORM_ID) private platformId: any, private zone: NgZone) {}

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    setTimeout(() => {
      this.browserOnly(() => {
        if (this.chart) {
          this.chart.dispose();
        }
      });
    }, 100);
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      if (this.chartData) {
        this.chartData.subscribe((data: any[]) => {
          if (data) {
            am4core.useTheme(am4themes_animated);
            this.createChart(data);
          } else {
            this.createChart();
          }
        });
      } else {
        this.createChart();
      }
    });
    // this.browserOnly(() => {
    //   am4core.useTheme(am4themes_animated);
    //   this.createChart();
    // });
  }

  createChart(chartData?: any[]) {
    // Create chart
    const chart = am4core.create(this.id, am4charts.PieChart);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

    // Add data
    if (chartData) {
      chartData.map((singleData: any) => {
        singleData.color = am4core.color(singleData.color);
      });
      chart.data = chartData;
    } else {
      chart.data = [
        {
          country: 'Lithuania',
          value: 260,
        },
        {
          country: 'Czechia',
          value: 230,
        },
        {
          country: 'Ireland',
          value: 200,
        },
        {
          country: 'Germany',
          value: 165,
        },
        {
          country: 'Australia',
          value: 139,
        },
        {
          country: 'Austria',
          value: 128,
        },
      ];
    }

    const series = chart.series.push(new am4charts.PieSeries());
    series.dataFields.value = this.valueFieldName;
    series.dataFields.radiusValue = this.valueFieldName;
    series.dataFields.category = this.categoryFieldName;
    series.slices.template.cornerRadius = 6;
    series.colors.step = 3;
    series.labels.template.text = '';

    // if (this.addLegend) {
    //   chart.legend = new am4charts.Legend();
    // }

    if (this.addLegend) {
      chart.legend = new am4charts.Legend();
      chart.legend.valueLabels.template.text = `{value${[this.categoryFieldName]}}`;
      chart.legend.useDefaultMarker = true;
      // chart.legend.width = 240;
      if (this.addLegendScroll) {
        chart.legend.maxHeight = this.legendHeight;
        chart.legend.scrollable = true;
      }

      const markerTemplate = chart.legend.markers.template;
      markerTemplate.width = 15;
      markerTemplate.height = 15;
    }

    series.hiddenState.properties.endAngle = -90;

    this.chart = chart;
  }
}
