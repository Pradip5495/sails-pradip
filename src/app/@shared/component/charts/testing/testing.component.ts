import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.scss'],
})
export class TestingComponent implements OnInit {
  @HostBinding('class.chart-container') containerClass = true;

  constructor() {}

  ngOnInit(): void {}
}
