import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@formly';

export type IJson<V = any> = { [key in string | number]: V };
export type IFormlyOnModelChange<T = any> = (value: IJson<T>) => void;

export interface IFormlyConfig {
  model: any;
  fields: FormlyFieldConfig[];
  options: FormlyFormOptions;
  form: FormGroup;
  onModelChange?: IFormlyOnModelChange;
}

export interface ICustomFormlyConfig {
  id?: string; // To detect uniqueness if required
  legacyForm?: boolean;
  shortName?: string;
  title?: string;
  subTitle?: string;
  config: IFormlyConfig;
  discard?: boolean;
  maskUntouchedAsInvalid?: boolean;
  showErrorIfInvalid?: boolean;
  cache?: boolean;
  onDiscard?: () => void;
  onSave?: (value: IJson) => void;

  // TODO: this function can be refactored in future
  onPartialSave?: (num: number) => void;
}
