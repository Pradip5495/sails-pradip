import { FormControl, ValidationErrors } from '@angular/forms';

export function multipleEmailValidator(control: FormControl): ValidationErrors {
  if (!control.value) {
    return null;
  }

  let isNotValid = false;
  const emailRegxp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const emailsArray: string[] = control.value ? control.value.split(',') : '';
  const cleanedEmailsArray: string[] = [];
  emailsArray.forEach((email: string) => {
    if (email) {
      cleanedEmailsArray.push(email.trim());
    }
  });

  cleanedEmailsArray.forEach((element: string) => {
    if (!emailRegxp.test(element)) {
      return (isNotValid = true);
    }
  });
  return !isNotValid ? null : { multipleEmails: true };
}
