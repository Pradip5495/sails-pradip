import { FieldType } from '@ngx-formly/material/form-field';
import { Component, ComponentFactoryResolver, ViewContainerRef, DoCheck, ComponentRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-tmpl-cmp-type',
  template: ``,
})
export class TemplateComponentTypeComponent extends FieldType implements DoCheck, OnInit {
  componentRef: ComponentRef<any>;

  constructor(public viewContainerRef: ViewContainerRef, private componentFactoryResolver: ComponentFactoryResolver) {
    super();
  }

  ngOnInit() {
    this.viewContainerRef.clear();
    if (this.to.component) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.to.component);
      this.componentRef = this.viewContainerRef.createComponent(componentFactory);
      this.componentRef.instance.model = this.model || {};
      this.componentRef.instance.formState = this.formState || {};
      this.componentRef.instance.props = this.to.props || {};
    }
  }

  ngDoCheck() {
    if (this.componentRef) {
      this.componentRef.instance.model = this.model;
      this.componentRef.instance.formState = this.formState;
    }
  }
}
