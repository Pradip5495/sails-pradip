import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
  selector: 'app-button-type',
  template: `
    <button
      *ngIf="to.appearance === 'default'"
      mat-button
      [type]="to.type"
      [ngClass]="to.className || ''"
      [color]="to.color || 'primary'"
      (click)="onClick($event)"
      [disabled]="to.disabled"
    >
      {{ to.label }}
    </button>
    <button
      *ngIf="to.appearance === 'raised'"
      mat-raised-button
      [type]="to.type"
      [ngClass]="to.className || ''"
      [color]="to.color || 'primary'"
      (click)="onClick($event)"
      [disabled]="to.disabled"
    >
      {{ to.label }}
    </button>
    <button
      *ngIf="to.appearance === 'stroked'"
      mat-stroked-button
      [type]="to.type"
      [ngClass]="to.className || ''"
      [color]="to.color || 'primary'"
      (click)="onClick($event)"
      [disabled]="to.disabled"
    >
      {{ to.label }}
    </button>
    <button
      *ngIf="to.appearance === 'flat'"
      mat-flat-button
      [type]="to.type"
      [ngClass]="to.className || ''"
      [color]="to.color || 'primary'"
      (click)="onClick($event)"
      [disabled]="to.disabled"
    >
      {{ to.label }}
    </button>
    <button
      *ngIf="to.appearance === 'icon'"
      mat-icon-button
      [type]="to.type"
      [ngClass]="to.className || ''"
      [color]="to.color || 'primary'"
      (click)="onClick($event)"
      [disabled]="to.disabled"
    >
      <mat-icon [svgIcon]="to.svgIcon || ''">{{ to.icon || '' }}</mat-icon>
    </button>
    <button
      *ngIf="to.appearance === 'fab'"
      mat-fab
      [type]="to.type"
      [ngClass]="to.className || ''"
      [color]="to.color || 'primary'"
      (click)="onClick($event)"
      [disabled]="to.disabled"
    >
      <mat-icon [svgIcon]="to.svgIcon || ''">{{ to.icon }}</mat-icon>
    </button>
    <button
      *ngIf="to.appearance === 'minifab'"
      mat-mini-fab
      [type]="to.type"
      [ngClass]="to.className || ''"
      [color]="to.color || 'primary'"
      (click)="onClick($event)"
      [disabled]="to.disabled"
    >
      <mat-icon [svgIcon]="to.svgIcon || ''">{{ to.icon }}</mat-icon>
    </button>
  `,

  styles: [
    `
      .mat-raised-button.mat-primary {
        background-color: #16569e;
      }

      .mat-raised-button[disabled]:not([class*='mat-elevation-z']) {
        color: #b5b5b5;
      }

      .transparent {
        opacity: 0;
      }
    `,
  ],
})
export class ButtonTypeComponent extends FieldType {
  onClick($event: any) {
    if (this.to.onClick) {
      this.to.onClick($event, this.field);
    }
  }
}
