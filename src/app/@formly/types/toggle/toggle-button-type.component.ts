import { FieldType } from '@ngx-formly/material/form-field';
import { Component, DoCheck } from '@angular/core';

@Component({
  selector: 'app-toggle-button-type',
  template: `
    <mat-button-toggle-group
      [formControl]="formControl"
      [formlyAttributes]="field"
      #group="matButtonToggleGroup"
      (change)="onChange(group.value)"
    >
      <mat-button-toggle value="0" [ngClass]="!to.value ? 'active' : ''">
        <span *ngIf="!to.value"> &nbsp; &nbsp; </span>
        <span *ngIf="to.value">
          {{ to.label1 }}
        </span>
      </mat-button-toggle>
      <mat-button-toggle value="1" [ngClass]="to.value ? 'active' : ''">
        <span *ngIf="to.value"> &nbsp; &nbsp; </span>
        <span *ngIf="!to.value">
          {{ to.label2 }}
        </span>
      </mat-button-toggle>
    </mat-button-toggle-group>
  `,
  styleUrls: ['./toggle-button-type.component.scss'],
})
export class ToggleButtonTypeComponent extends FieldType implements DoCheck {
  onChange(value: any) {
    this.to.value = !this.to.value;
    if (this.to.onChange) {
      this.to.onChange(value);
    }
  }

  ngDoCheck() {
    if (this.model.portSea === '1' && !this.to.value) {
      this.to.value = true;
    }

    if (this.model.portSea === '0' && this.to.value) {
      this.to.value = false;
    }
  }
}
