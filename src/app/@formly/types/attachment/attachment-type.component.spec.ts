import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachmentTypeComponent } from './attachment-type.component';

describe('AttachmentTypeComponent', () => {
  let component: AttachmentTypeComponent;
  let fixture: ComponentFixture<AttachmentTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AttachmentTypeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
