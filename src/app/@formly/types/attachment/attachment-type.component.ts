import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/material';

@Component({
  selector: 'app-attachment-type',
  template: `
    <div class="attachment-section">
      <ng-container *ngIf="to.preview">
        <div *ngFor="let image of images" class="image-box">
          <img [src]="getFilterFile(image)" [class]="getImageStyleClass(image)" />
          <a (click)="deleteImage($event, image)" href="#">
            <mat-icon class="clear-icon">clear</mat-icon>
          </a>
          <p class="image-name">{{ image.name }}</p>
        </div>
      </ng-container>
      <div class="attachment-button">
        <input
          [class]="to.class || 'ng-hide upload-btn'"
          type="file"
          [formControl]="formControl"
          [formlyAttributes]="field"
          [id]="to.id || 'input-file-id'"
          (change)="onChange($event)"
          hidden="hidden"
          [multiple]="!!to.multiple"
          [accept]="to.accept"
        />
        <label [for]="to.id || 'input-file-id'" class="md-button md-raised md-primary file-button">
          <mat-icon>{{ to.attachIcon || 'attach_file' }}</mat-icon>
          <span class="icon-text">{{ label }}</span>
        </label>
      </div>
    </div>
  `,
  styleUrls: ['./attachment-type.component.scss'],
})
export class AttachmentTypeComponent extends FieldType {
  images: any[] = [];
  allFiles: File[] = [];

  /**
   * The nullish coalescing operator (??) is a logical operator that returns
   * its right-hand side operand when its left-hand side operand is null or undefined,
   * and otherwise returns its left-hand side operand.
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator
   */
  get label() {
    if (this.to.emptyLabel) {
      return '';
    }
    return this.to.label || 'ATTACHMENT';
  }

  /**
   * return filter image URL based on file type
   */
  getFilterFile(imageType: any) {
    const imageTypeAssetMapping = {
      'image/png': imageType.url,
      'image/jpg': imageType.url,
      'image/jpeg': imageType.url,
      'application/x-zip-compressed': '/assets/zip.png',
      ' ': '/assets/zip.png',
      'application/pdf': '/assets/pdf.png',
    };
    return imageTypeAssetMapping[imageType.type] || 'assets/file.png';
  }

  /**
   * return image class based on file type
   */
  getImageStyleClass(imageType: any) {
    const imageTypeStyleMapping = {
      'image/png': 'upload-images',
      'image/jpg': 'upload-images',
      'image/jpeg': 'upload-images',
    };

    return imageTypeStyleMapping[imageType.type] || 'pdfimages';
  }

  deleteImage($event: any, image: any) {
    $event.preventDefault();
    const index = this.images.indexOf(image);
    this.images.splice(index, 1);
    this.allFiles.splice(index, 1);
    if (this.to.onFileChange) {
      this.to.onFileChange(this.allFiles);
    }
  }

  onChange(event: any) {
    this.updatePreview(event);
    if (this.to.unsetModel && !(this.key instanceof Array)) {
      delete this.model[this.key];
    }
    if (this.to.onFileChange) {
      this.to.onFileChange(this.allFiles);
    }
  }

  updatePreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const { name, type, size } = event.target.files[i] as File;
        const image = { name, type, size, url: '' };

        this.allFiles.push(event.target.files[i] as File);

        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.images.push(image);
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    event.srcElement.value = null;
  }
}
