import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/material';

@Component({
  selector: 'app-tel-input-type',
  template: `
    <input
      type="text"
      [class]="to.className"
      matInput
      ng2TelInput
      [readonly]="to.readonly"
      [required]="to.required"
      [errorStateMatcher]="errorStateMatcher"
      (hasError)="onError($event)"
      (ng2TelOutput)="getNumber($event)"
      (intlTelInputObject)="telInputObject($event)"
      (countryChange)="onCountryChange($event)"
      [formControl]="formControl"
      [formlyAttributes]="field"
      [tabindex]="to.tabindex"
      [placeholder]="to.placeholder"
    />
  `,
})
export class TelInputTypeComponent extends FieldType {
  onError($event: any) {
    if (this.to.onError) {
      this.to.onError($event);
    }
  }

  getNumber($event: any) {
    if (this.to.getNumber) {
      this.to.getNumber($event);
    }
  }

  telInputObject($event: any) {
    if (this.to.telInputObject) {
      this.to.telInputObject($event);
    }
  }

  onCountryChange($event: any) {
    if (this.to.onCountryChange) {
      this.to.onCountryChange($event);
    }
  }
}
