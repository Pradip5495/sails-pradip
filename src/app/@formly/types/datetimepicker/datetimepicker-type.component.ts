import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/material';

@Component({
  selector: 'formly-repeat-section',
  template: `
    <input
      [id]="id"
      [readonly]="to.readonly"
      [required]="to.required"
      [owlDateTimeTrigger]="datetime"
      [owlDateTime]="datetime"
      [formControl]="formControl"
      [min]="to.timeMin"
      [max]="to.timeMax"
      matInput
      [placeholder]="to.placeholder"
    />
    <owl-date-time [pickerType]="to.pickerType || 'timer'" #datetime></owl-date-time>
  `,
})
export class DateTimePickerTypeComponent extends FieldType {
  get pickerType() {
    return this.to.pickerType || 'timer';
  }
}
