import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgSelectTypeComponent } from './ng-select-type.component';

describe('NgSelectTypeComponent', () => {
  let component: NgSelectTypeComponent;
  let fixture: ComponentFixture<NgSelectTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NgSelectTypeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgSelectTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
