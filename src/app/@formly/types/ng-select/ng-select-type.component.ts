import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/material';

@Component({
  selector: 'app-ng-select-type',
  template: `
    <ng-select
      [items]="to.items ? (hasLength ? to.items : (to.items | async)) : []"
      [bindLabel]="to.bindLabel"
      [class]="field.className"
      [bindValue]="to.bindValue || ''"
      [(ngModel)]="to.model"
      [addTag]="to.addTag || false"
      addTagText="{{ to.addTagText || 'Add item' }}"
      appearance="{{ to.appearance || 'outline' }}"
      appendTo="{{ to.appendTo || null }}"
      [closeOnSelect]="to.closeOnSelect || true"
      clearAllText="{{ to.clearAllText || 'Clear all' }}"
      [clearable]="to.clearable || true"
      [clearOnBackspace]="to.clearOnBackspace || true"
      dropdownPosition="to.dropdownPosition || 'auto'"
      [groupBy]="to.groupBy || null"
      [formControl]="formControl"
      [formlyAttributes]="field"
      [selectableGroup]="to.selectableGroup || false"
      [selectableGroupAsModel]="to.selectableGroupAsModel || true"
      [loading]="to.loading || false"
      loadingText="{{ to.loadingText || 'Loading...' }}"
      labelForId="{{ to.labelForId }}"
      [markFirst]="to.markFirst || true"
      maxSelectedItems="{{ to.maxSelectedItems || 3 }}"
      [hideSelected]="to.hideSelected || false"
      [multiple]="to.multiple || false"
      notFoundText="{{ to.notFoundText || 'No items found' }}"
      placeholder="{{ placeholder }}"
      [searchable]="to.searchable || true"
      [readonly]="to.readonly || false"
      [searchFn]="to.searchFn || null"
      [searchWhileComposing]="to.searchWhileComposing || true"
      [trackByFn]="to.trackByFn || null"
      [clearSearchOnAdd]="to.clearSearchOnAdd || true"
      [editableSearchTerm]="to.editableSearchTerm || false"
      [selectOnTab]="to.selectOnTab || false"
      [openOnEnter]="to.openOnEnter || true"
      [typeahead]="to.typeahead || null"
      [minTermLength]="to.minTermLength || 0"
      typeToSearchText="{{ to.typeToSearchText || 'Type to search' }}"
      [virtualScroll]="to.virtualScroll || false"
      [inputAttrs]="to.inputAttrs || {}"
      [tabIndex]="to.tabIndex || '-'"
      [keyDownFn]="onKeyDown"
      (add)="onAdd($event)"
      (blur)="onBlur($event)"
      (change)="onChange($event)"
      (close)="onClose()"
      (clear)="onClear()"
      (focus)="onFocus($event)"
      (search)="onSearch($event)"
      (open)="onOpen()"
      (remove)="onRemove($event)"
      (scrollToEnd)="onScrollToEnd($event)"
      (scroll)="onScroll($event)"
    >
    </ng-select>
    <mat-error class="mat-error" *ngIf="validator.required"> {{ key }} is required </mat-error>
  `,

  styleUrls: ['./ng-select-type.component.css'],
})
export class NgSelectTypeComponent extends FieldType {
  get hasLength() {
    return typeof this.to.items.length !== 'undefined';
  }

  get validator() {
    return {
      required: this?.formControl.errors?.required && this?.formControl.invalid && this?.formControl.touched,
    };
  }

  get placeholder() {
    let _placeholder = this.to.placeholder ? this.to.placeholder : 'Select Item';
    if (this.to.required) {
      _placeholder += ' *';
    }
    return _placeholder;
  }

  compareWith = (a: any, b: any) => a === b;

  onKeyDown($event: any) {
    if (this.to?.onKeyDown) {
      this.to.onKeyDown($event);
    }
  }

  onScroll($event: any) {
    if (this.to.onScroll) {
      this.to.onScroll($event);
    }
  }

  onScrollToEnd($event: any) {
    if (this.to.onScrollToEnd) {
      this.to.onScrollToEnd($event);
    }
  }

  onRemove($event: any) {
    if (this.to.onRemove) {
      this.to.onRemove($event);
    }
  }

  onOpen() {
    if (this.to.onOpen) {
      this.to.onOpen();
    }
  }

  onSearch($event: any) {
    if (this.to.onSearch) {
      this.to.onSearch($event);
    }
  }

  onFocus($event: any) {
    if (this.to.onFocus) {
      this.to.onFocus($event);
    }
  }

  onClear() {
    if (this.to.onClose) {
      this.to.onClear();
    }
  }

  onClose() {
    if (this.to.onClose) {
      this.to.onClose();
    }
  }

  onAdd($event: any) {
    if (this.to.onAdd) {
      this.to.onAdd($event);
    }
  }

  onBlur($event: any) {
    if (this.to.onBlur) {
      this.to.onBlur($event);
    }
  }

  onChange($event: any[]) {
    if (this.to.onChange) {
      this.to.onChange($event, this.field);
    }
  }
}
