import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeverityLevelTypeComponent } from './severity-level-type.component';

describe('SeverityLevelTypeComponent', () => {
  let component: SeverityLevelTypeComponent;
  let fixture: ComponentFixture<SeverityLevelTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SeverityLevelTypeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeverityLevelTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
