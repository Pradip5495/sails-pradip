import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/material';

@Component({
  selector: 'app-severity-level-type',
  templateUrl: './severity-level-type.component.html',
  styleUrls: ['./severity-level-type.component.css'],
})
export class SeverityLevelTypeComponent extends FieldType {
  //#region Default and Init Options

  severityValue = '';

  get Environment() {
    return this.formState.severityDescriptions.Environment;
  }

  get People() {
    return this.formState.severityDescriptions.People;
  }

  get Property() {
    return this.formState.severityDescriptions['Assets/Property/Financial'];
  }

  get Reputation() {
    return this.formState.severityDescriptions['Reputation/Business'];
  }

  //#endregion

  onSeverityLevelChange(event: any) {
    if (this.to.change) {
      this.to.change(event);
    }
  }
}
