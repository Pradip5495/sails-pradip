import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatDatePickerTypeComponent } from './mat-date-picker-type.component';

describe('MatDatePickerTypeComponent', () => {
  let component: MatDatePickerTypeComponent;
  let fixture: ComponentFixture<MatDatePickerTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MatDatePickerTypeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatDatePickerTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
