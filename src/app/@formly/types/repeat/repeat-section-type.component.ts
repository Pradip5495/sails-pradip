import { Component, OnInit, OnDestroy } from '@angular/core';
import { FieldArrayType, FormlyFieldConfig, FormlyTemplateOptions } from '@ngx-formly/core';

@Component({
  selector: 'formly-repeat-section',
  template: `
    <ng-container *ngIf="to.layout === 'legacy'; else modern">
      <div *ngFor="let field of field.fieldGroup; let i = index" class="row">
        <formly-field class="col" [field]="field"></formly-field>
      </div>
      <div *ngIf="!to.inlineAdd" [class]="to.addBtnParentClass || ''">
        <button [class]="to.addTextClass || ''" mat-raised-button type="button" (click)="add()">
          {{ to.addText }}
        </button>
      </div>
    </ng-container>

    <ng-template #modern>
      <div fxLayout="row" fxLayoutAlign="start start">
        <div fxLayout="column" fxFlex="80%">
          <ul class="inline-list">
            <li
              *ngFor="let field of field.fieldGroup; let i = index"
              [ngClass]="{ active: i === activeFieldIndex }"
              (click)="activeFieldIndex = i"
            >
              {{ i + 1 }}
            </li>
          </ul>
        </div>
        <div fxLayout="column" fxFlex="auto" fxLayoutAlign="start end">
          <div class="repeat-actions">
            <button
              mat-icon-button
              color="warn"
              class="flex-0-auto"
              type="button"
              *ngIf="field.fieldGroup.length > 0 && activeFieldIndex !== to.nonRemovableIndex"
              (click)="removeSection(activeFieldIndex)"
            >
              <mat-icon>delete</mat-icon>
            </button>
            <button mat-stroked-button [class]="to.addTextClass || ''" type="button" (click)="addSection()">
              {{ to.addText }}
            </button>
          </div>
        </div>
      </div>

      <div fxLayout="row" fxLayoutAlign="start start">
        <div fxFlex="100%">
          <formly-field class="col" [field]="field.fieldGroup[activeFieldIndex]"></formly-field>
        </div>
      </div>
    </ng-template>
  `,

  styles: [
    `
      ::ng-deep .action-btn-delete {
        margin-top: 5px;
        padding: 3px 0;
      }

      :host ::ng-deep .adjust-margin {
        width: 0px;
      }

      :host ::ng-deep .pl-0 {
        padding-left: 0px;
      }

      :host ::ng-deep ul.inline-list {
        list-style: none;
        padding-left: 0px;
      }

      :host ::ng-deep ul.inline-list li {
        display: inline-block;
        padding: 10px 15px;
        border-radius: 5px;
        margin-right: 10px;
        border: 1px solid grey;
        color: gray;
        cursor: pointer;
      }

      :host ::ng-deep ul.inline-list li.active {
        background: #52baf3;
        border: none;
        color: white;
      }

      :host ::ng-deep .repeat-actions {
        padding: 12px;
      }
    `,
  ],
})
export class RepeatSectionTypeComponent extends FieldArrayType implements OnInit, OnDestroy {
  set field(field: FormlyFieldConfig) {
    this._field = field;
    this._field.templateOptions.componentRef = this;
  }

  get field() {
    return this._field;
  }

  activeFieldIndex = 0;
  private readonly delBtnIdPrefix = '_frmly_del_';
  private _field: FormlyFieldConfig;

  ngOnInit() {
    if (this.to.addOnInit) {
      this.add();
    }
  }

  /**
   * Cleanup componentRef object
   */
  ngOnDestroy() {
    if (this.field && this.field.templateOptions.componentRef) {
      delete this.field.templateOptions.componentRef;
    }
  }

  removeSection(i: number) {
    this.remove(i);
    if (this.field.fieldGroup.length > 0) {
      this.activeFieldIndex = this.field.fieldGroup.length - 1;
    }
  }

  addSection() {
    this.add();
    this.activeFieldIndex = this.field.fieldGroup.length - 1;
  }

  add(i?: number, initialModel?: any, { markAsDirty }: { markAsDirty: boolean } = { markAsDirty: true }) {
    super.add(i, initialModel, { markAsDirty });
    const { nonRemovableIndex } = this.to;
    const latestIndex = i || this.field.fieldGroup.length - 1;
    if (typeof nonRemovableIndex === 'undefined' || nonRemovableIndex !== latestIndex) {
      this.addDeleteButton();
    }

    if (this.to.onRepeatSectionAdd) {
      this.to.onRepeatSectionAdd(this.field, latestIndex);
    }
  }

  remove(i: number, { markAsDirty }: { markAsDirty: boolean } = { markAsDirty: true }) {
    super.remove(i, { markAsDirty });
    this.refreshDeleteButton();
    if (this.to.onDelete) {
      this.to.onDelete(this.field.fieldGroup, this.model, this.formState);
    }
  }

  hasDeleteButton(fieldGroup: FormlyFieldConfig[]) {
    return (
      fieldGroup &&
      fieldGroup.filter((fieldConfig) => fieldConfig.id && fieldConfig.id.indexOf(this.delBtnIdPrefix) > -1).length > 0
    );
  }

  getButtonType(id: string, groupIndex: number, nonInteractive: boolean = false) {
    const templateOptions: FormlyTemplateOptions = {
      className: 'action-btn-delete remove-btn warn',
      label: 'Delete',
      color: 'default',
      type: 'button',
      appearance: 'raised',
      onClick: ($event: any) => {
        this.remove(groupIndex);
      },
    };

    const options = {
      className: 'flex-0-auto',
      id,
      type: 'button',
      key: 'delete',
      templateOptions,
      options: {},
    };

    if (nonInteractive) {
      templateOptions.disabled = true;
      templateOptions.className += ' transparent';
    }

    if (this.to.deleteBtnClass && nonInteractive) {
      options.className += ` ${this.to.deleteBtnClass}`;
    }

    return options;
  }

  /**
   * Refresh delete button groupIndex after deleting any section
   */
  refreshDeleteButton() {
    this.field.fieldGroup.map((group, groupIndex: number) => {
      if (group.fieldGroup) {
        group.fieldGroup.map((_innerGroup, index: number) => {
          _innerGroup.fieldGroup.map((__innerGroup) => {
            if (__innerGroup.id && __innerGroup.id.indexOf(this.delBtnIdPrefix) > -1) {
              __innerGroup.templateOptions.onClick = ($event: any) => {
                this.remove(groupIndex);
              };
            }
          });
        });
      }
    });
  }

  addDeleteButton() {
    if (!this.to.allowDelete || this.to.layout === 'modern') {
      return;
    }
    const { nonRemovableIndex } = this.to;
    this.field.fieldGroup.map((group, groupIndex: number) => {
      if (group.fieldGroup) {
        const lastArrayIndex = group.fieldGroup.length - 1;
        group.fieldGroup.map((_innerGroup, index: number) => {
          const delBtnId = this.delBtnIdPrefix + groupIndex + '_' + index;
          const hasDeleteBtn = this.hasDeleteButton(_innerGroup.fieldGroup);
          if (lastArrayIndex === index && _innerGroup.fieldGroup && !hasDeleteBtn) {
            if (typeof nonRemovableIndex === 'undefined' || nonRemovableIndex !== groupIndex) {
              _innerGroup.fieldGroup.push(this.getButtonType(delBtnId, groupIndex));
            } else if (nonRemovableIndex === groupIndex) {
              _innerGroup.fieldGroup.push(this.getButtonType(delBtnId, groupIndex, true));
            }
          }
        });
      }
    });
  }
}
