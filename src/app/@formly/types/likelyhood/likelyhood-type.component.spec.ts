import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LikelyhoodTypeComponent } from './likelyhood-type.component';

describe('LikelyhoodTypeComponent', () => {
  let component: LikelyhoodTypeComponent;
  let fixture: ComponentFixture<LikelyhoodTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LikelyhoodTypeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LikelyhoodTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
