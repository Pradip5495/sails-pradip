import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/material';

@Component({
  selector: 'app-likelyhood-type',
  templateUrl: './likelyhood-type.component.html',
  styleUrls: ['./likelyhood-type.component.scss'],
})
export class LikelyhoodTypeComponent extends FieldType {
  defaultOptions = {
    templateOptions: {
      label: 'PROBABILITY OF RECURRENCE IF NO ACTIONS TAKEN',
    },
  };

  impactLabel = 'Low';

  onChangeprobability(event: any) {}

  getImpactActiveClass() {
    if (this.to.impact < 5) {
      this.impactLabel = 'Low';
      return 'normal';
    } else if (this.to.impact > 4 && this.to.impact < 10) {
      this.impactLabel = 'Medium';
      return 'medium';
    } else {
      this.impactLabel = 'High';
      return 'extreme';
    }
  }
}
