import { FormlyFieldConfig } from '@ngx-formly/core';
import { IJson } from '@types';

export interface CustomFormlyFieldConfig extends FormlyFieldConfig {
  fieldObject: IJson;
}
