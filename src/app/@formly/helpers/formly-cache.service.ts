import { Injectable } from '@angular/core';
import { LocalStorageService } from '@shared/common-service';
import { IJson } from '@types';

@Injectable({
  providedIn: 'root',
})
export class FormlyCacheService {
  // Keep track of formly cache keys
  formlyCacheKeys: string[] = [];

  constructor(private localStorage: LocalStorageService) {}

  set(key: string, value: IJson) {
    this.localStorage.set(key, value);

    if (this.formlyCacheKeys.indexOf(key) === -1) {
      this.formlyCacheKeys.push(key);
    }
  }

  get(key: string) {
    return this.localStorage.get(key);
  }
}
