import { Pipe, PipeTransform } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Pipe({
  name: 'status',
  pure: false,
})
export class StepperStatusPipe implements PipeTransform {
  transform(form: FormGroup) {
    if (form.valid) {
      return 'primary';
    }

    if (form.touched && form.invalid) {
      return 'warn';
    }

    return '';
  }
}
