import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { ICustomFormlyConfig } from '@types';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;

  activeIndex = 1;
  isLinear = false;
  title: string;
  subTitle: string;

  @Input()
  containerClass = 'mat-card-form-content';

  _config: ICustomFormlyConfig[] = [];

  @Input()
  autoHeight = false;

  @Input()
  set config(_config: ICustomFormlyConfig[]) {
    this._config = _config;
  }

  get config(): ICustomFormlyConfig[] {
    if (this._config) {
      if (typeof this._config.length !== 'undefined') {
        return this._config;
      }
      return [this._config as any];
    }
  }

  /**
   * @deprecated
   * This Output event will be deprecated in future release
   */
  @Output()
  ngSubmit: EventEmitter<any> = new EventEmitter();

  isFirstStep() {
    return this.stepper && this.stepper.selectedIndex === 0;
  }

  isLastStep() {
    if (this.stepper && this.stepper.steps) {
      return this.stepper.selectedIndex === this.stepper.steps.length - 1;
    }
    return false;
  }

  move(index: number) {
    this.stepper.selectedIndex = index;
    this.activeIndex = index + 1;
    // Update Step Title & SubTitle
    this.title = this.config[index].title;
    this.subTitle = this.config[index].subTitle;
  }

  get isStepperAvailable() {
    return this.config.length > 1;
  }

  onFormSave(formData: any) {
    const { stepper } = this;
    if (stepper && stepper.selectedIndex + 1 < stepper.steps.length) {
      this.move(stepper.selectedIndex + 1);
    }
  }

  ngOnInit(): void {
    if (this.config.length > 1) {
      this.title = this.config[this.activeIndex].title;
      this.subTitle = this.config[this.activeIndex].subTitle;
    }
  }
}
