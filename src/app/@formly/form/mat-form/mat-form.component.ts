import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@formly';
import { IFormlyOnModelChange, IJson } from '@types';
import { NotificationService } from '@shared/common-service';
import { FormlyCacheService } from '@formly/helpers/formly-cache.service';

@Component({
  selector: 'app-mat-formly-form',
  templateUrl: './mat-form.component.html',
  styleUrls: ['./mat-form.component.scss'],
})
export class MatFormlyFormComponent {
  isPartial = false;

  @Input()
  discard = true;

  @Input()
  legacyForm? = false;

  @Input()
  containerClass = 'mat-card-form-content';

  @Input()
  model: any;

  @Input()
  fields: FormlyFieldConfig[];

  @Input()
  options: FormlyFormOptions;

  @Input()
  form: FormGroup;

  @Input()
  onModelChange?: IFormlyOnModelChange;

  @Input()
  stepperIndex?: number;

  @Input()
  autoHeight = false;

  @Input()
  maskUntouchedAsInvalid = true;

  @Input()
  showErrorIfInvalid = true;

  @Input()
  cache = true;

  @Input()
  id?: string;

  @Input()
  onDiscard?: () => void;

  @Input()
  onSave?: (modelValue: any, isPartial: boolean, stepId?: string) => void;

  @Input()
  onPartialSubmit?: (modelValue: any, isPartial: boolean) => void;

  @Output()
  save: EventEmitter<IJson> = new EventEmitter<IJson>();

  constructor(private notificationService: NotificationService, private formlyCacheService: FormlyCacheService) {}

  onReset() {
    if (this.onDiscard) {
      this.onDiscard();
    } else {
      this.form.reset();
    }
  }

  onSubmit(modelValue: IJson) {
    if (this.form.invalid) {
      return this.handleInvalidForm();
    }
    this.submitForm(modelValue);
  }

  submitForm(formData: IJson) {
    if (!this.isPartial) {
      if (this.onSave) {
        this.onSave(formData, this.isPartial, this.id);
        this.save.emit(formData);
      }

      if (this.cache && this.id) {
        this.formlyCacheService.set(this.id, this.model);
      }
    } else {
      this.onPartialSubmit(formData, this.isPartial);
      this.isPartial = true;
    }
  }

  handleInvalidForm() {
    if (this.maskUntouchedAsInvalid) {
      this.form.markAllAsTouched();
    }
    if (this.showErrorIfInvalid) {
      this.notificationService.showError('Fill all the required fields!!');
    }
  }

  modelChange(value: IJson) {
    if (this.onModelChange) {
      this.onModelChange(value);
    }
  }
}
