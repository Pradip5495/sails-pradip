import { FieldWrapper } from '@ngx-formly/core';
import { Component } from '@angular/core';

@Component({
  selector: 'formly-wrapper-card',
  template: `
    <mat-card class="card-wrapper">
      <mat-card-header>
        <mat-card-title>{{ to.label }}</mat-card-title>
      </mat-card-header>
      <mat-card-content>
        <ng-container #fieldComponent></ng-container>
      </mat-card-content>
    </mat-card>
  `,
  styles: [
    `
      .mat-card {
        margin-right: 10px;
      }

      ::ng-deep .card-wrapper .mat-card-title {
        font-size: 15px;
      }

      ::ng-deep .card-wrapper .mat-card-header-text {
        margin-left: 0px;
      }
    `,
  ],
})
export class CardWrapperComponent extends FieldWrapper {}
