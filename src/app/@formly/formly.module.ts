import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FormlyModule as NgxFormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormlyMatDatepickerModule } from '@ngx-formly/material/datepicker';
import { FormlyMatToggleModule } from '@ngx-formly/material/toggle';
import { FormComponent } from '@formly/form/form.component';
import { MaterialModule } from '@app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { InputTypeComponent } from './types/input/input-type.component';
import { ToggleButtonTypeComponent } from './types/toggle/toggle-button-type.component';
import { RepeatSectionTypeComponent } from './types/repeat/repeat-section-type.component';
import { DateTimePickerTypeComponent } from './types/datetimepicker/datetimepicker-type.component';

import { FormlyMatFormFieldModule } from '@ngx-formly/material/form-field';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FormlyWrapperAddonsComponent } from './wrapper/addons-wrapper.component';
import { MatDatePickerTypeComponent } from './types/matdatepicker/mat-date-picker-type.component';
import { AttachmentTypeComponent } from './types/attachment/attachment-type.component';
import { CardWrapperComponent } from './wrapper/card-wrapper.component';

import { MatFormlyFormComponent } from './form/mat-form/mat-form.component';
import formlyConfig from './formly.config';
import { NgSelectTypeComponent } from './types/ng-select/ng-select-type.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ButtonTypeComponent } from './types/button/button-type.component';
import { TelInputTypeComponent } from './types/tel-input/tel-input-type.component';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { AutocompleteTypeComponent } from './types/autocomplete/autocomplete.component';
import { TemplateComponentTypeComponent } from './types/template-component/template-component-type.component';
import { SeverityLevelTypeComponent } from './types/severity-level/severity-level-type.component';
import { LikelyhoodTypeComponent } from './types/likelyhood/likelyhood-type.component';
import { StepperStatusPipe } from './helpers/stepper-status.pipe';
import { FormlySelectModule } from '@ngx-formly/core/select';
import { MatFormlySelectComponent } from './types/mat-select/mat-select.component';

@NgModule({
  declarations: [
    FormComponent,
    InputTypeComponent,
    ToggleButtonTypeComponent,
    RepeatSectionTypeComponent,
    DateTimePickerTypeComponent,
    FormlyWrapperAddonsComponent,
    MatDatePickerTypeComponent,
    AttachmentTypeComponent,
    CardWrapperComponent,
    MatFormlyFormComponent,
    NgSelectTypeComponent,
    ButtonTypeComponent,
    TelInputTypeComponent,
    AutocompleteTypeComponent,
    TemplateComponentTypeComponent,
    SeverityLevelTypeComponent,
    LikelyhoodTypeComponent,
    StepperStatusPipe,
    MatFormlySelectComponent,
  ],
  exports: [FormComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    FormlyMatFormFieldModule,
    NgxFormlyModule.forRoot(formlyConfig),
    FormlyMaterialModule,
    FormlyMatDatepickerModule,
    FormlyMatToggleModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgSelectModule,
    FormsModule,
    Ng2TelInputModule,
    FormlySelectModule,
  ],
})
export class FormlyModule {}
