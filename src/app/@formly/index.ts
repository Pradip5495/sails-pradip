export * from './formly.module';

export { FormlyFieldConfig, FormlyFormOptions, FieldType, FieldArrayType, FieldWrapper } from '@ngx-formly/core';
