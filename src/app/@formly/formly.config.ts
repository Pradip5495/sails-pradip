import { FormlyWrapperAddonsComponent } from './wrapper/addons-wrapper.component';
import { CardWrapperComponent } from './wrapper/card-wrapper.component';
import { addonsExtension } from './extension/addon';
import { FormlyFieldConfig } from '@formly';
import { InputTypeComponent } from './types/input/input-type.component';
import { ToggleButtonTypeComponent } from './types/toggle/toggle-button-type.component';
import { RepeatSectionTypeComponent } from './types/repeat/repeat-section-type.component';
import { DateTimePickerTypeComponent } from './types/datetimepicker/datetimepicker-type.component';
import { MatDatePickerTypeComponent } from './types/matdatepicker/mat-date-picker-type.component';
import { AttachmentTypeComponent } from './types/attachment/attachment-type.component';
import { NgSelectTypeComponent } from './types/ng-select/ng-select-type.component';
import { ButtonTypeComponent } from './types/button/button-type.component';
import { FormlyFieldSelect } from '@ngx-formly/material/select';
import { FormlyFieldTextArea } from '@ngx-formly/material/textarea';
import { FormlyFieldMultiCheckbox } from '@ngx-formly/material/multicheckbox';
import { TelInputTypeComponent } from './types/tel-input/tel-input-type.component';
import { AutocompleteTypeComponent } from './types/autocomplete/autocomplete.component';
import { multipleEmailValidator } from './formly.validators';
import { TemplateComponentTypeComponent } from './types/template-component/template-component-type.component';
import { SeverityLevelTypeComponent } from './types/severity-level/severity-level-type.component';
import { LikelyhoodTypeComponent } from './types/likelyhood/likelyhood-type.component';
import { ImagePreviewTypeComponent } from './types/image-preview/image-preview.component';
import { MatFormlySelectComponent } from './types/mat-select/mat-select.component';

export default {
  wrappers: [
    {
      name: 'addons',
      component: FormlyWrapperAddonsComponent,
    },
    {
      name: 'card',
      component: CardWrapperComponent,
    },
  ],
  extensions: [{ name: 'addons', extension: { onPopulate: addonsExtension } }],
  validators: [{ name: 'multipleEmails', validation: multipleEmailValidator }],
  validationMessages: [
    {
      name: 'required',
      message: (error: any, config: FormlyFieldConfig) => {
        if (config.templateOptions.label) {
          return `${config.templateOptions.label} is required`;
        }
        return 'This field is required';
      },
    },
  ],
  types: [
    {
      name: 'input',
      component: InputTypeComponent,
      defaultOptions: {
        templateOptions: {
          appearance: 'outline',
          floatLabel: 'never',
        },
      },
    },
    {
      name: 'autocomplete',
      component: AutocompleteTypeComponent,
      wrappers: ['form-field'],
    },
    {
      name: 'toggle-button',
      component: ToggleButtonTypeComponent,
    },
    {
      name: 'repeat',
      component: RepeatSectionTypeComponent,
      defaultOptions: {
        templateOptions: {
          allowDelete: true,
          addOnInit: true,
          layout: 'legacy',
        },
      },
    },
    {
      name: 'datetimepicker',
      component: DateTimePickerTypeComponent,
      wrappers: ['form-field'],
      defaultOptions: {
        templateOptions: {
          appearance: 'outline',
          addonRight: {
            icon: 'access_time',
          },
        },
      },
    },
    {
      name: 'matdatepicker',
      component: MatDatePickerTypeComponent,
      wrappers: ['form-field'],
      defaultOptions: {
        templateOptions: {
          appearance: 'outline',
        },
      },
    },
    {
      name: 'sailfile',
      component: AttachmentTypeComponent,
    },
    {
      name: 'ng-select',
      component: NgSelectTypeComponent,
    },
    {
      name: 'select',
      component: FormlyFieldSelect,
      defaultOptions: {
        className: 'flex-1',
        templateOptions: {
          appearance: 'outline',
        },
      },
    },
    {
      name: 'textarea',
      component: FormlyFieldTextArea,
      defaultOptions: {
        className: 'flex-1',
        templateOptions: {
          appearance: 'outline',
        },
      },
    },
    {
      name: 'button',
      component: ButtonTypeComponent,
      defaultOptions: {
        templateOptions: {
          type: 'button',
          appearance: 'raised',
        },
      },
    },
    {
      name: 'tel-input',
      component: TelInputTypeComponent,
      wrappers: ['form-field'],
      defaultOptions: {
        templateOptions: {
          appearance: 'outline',
          className: 'btn-field-bt-mg',
          floatLabel: 'always',
        },
      },
    },
    {
      name: 'template-component',
      component: TemplateComponentTypeComponent,
    },
    {
      name: 'image-template',
      component: ImagePreviewTypeComponent,
    },
    {
      name: 'severity-level',
      component: SeverityLevelTypeComponent,
    },
    {
      name: 'likelyhood',
      component: LikelyhoodTypeComponent,
    },
    {
      name: 'multicheckbox',
      component: FormlyFieldMultiCheckbox,
      defaultOptions: {
        templateOptions: {
          color: 'primary',
        },
      },
    },
    {
      name: 'matselect',
      component: MatFormlySelectComponent,
      wrappers: ['form-field'],
      defaultOptions: {
        templateOptions: {
          appearance: 'outline',
        },
      },
    },
  ],
};
