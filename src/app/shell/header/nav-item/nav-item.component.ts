import { Component, Input } from '@angular/core';
import { NavItem } from '@app/shell/header/nav-item/nav-item.interface';
import { Logger } from '@core';
import { SidenavComponent } from '@app/shell/sidenav/sidenav.component';

const log = new Logger('NavItemComponent');

@Component({
  selector: 'app-nav-item',
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.scss'],
})
export class NavItemComponent {
  @Input() lSideNav!: SidenavComponent;
  @Input() navItem: NavItem;

  constructor() {}

  onNavClick() {
    log.debug('Navigate to', this.navItem.displayName);
    /* This code to be removed once after review */
    // this.leftSidenavStore.navItems = this.navItem.children;
  }
}
