import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HeaderService {
  private readonly _hasHeader = new BehaviorSubject<boolean>(true);

  get hasHeader$() {
    return this._hasHeader.asObservable();
  }

  set hasHeader(value: boolean) {
    this._hasHeader.next(value);
  }
}
