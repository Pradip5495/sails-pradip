import { Title } from '@angular/platform-browser';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '@core/auth';
import { Logger, untilDestroyed } from '@core';
import { NavItem } from '@app/shell/header/nav-item/nav-item.interface';
import { SidenavComponent } from '@app/shell/sidenav/sidenav.component';
import { UserService } from '@shared';
import { HeaderService } from './header.service';

const log = new Logger('HeaderComponent');

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  selectedModule = 'Module Switcher';

  @Input() sidenav!: SidenavComponent;
  // TODO SET true if user have a profile pic
  isProfilePic = false;
  hasHeader = true;

  // TODO get this from Service
  navItems: NavItem[];

  navItemsProfile!: NavItem[];
  profileId: any;

  constructor(
    private router: Router,
    private titleService: Title,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private headerService: HeaderService
  ) {
    this.headerService.hasHeader$.pipe(untilDestroyed(this)).subscribe((value) => {
      this.hasHeader = value;
    });
  }

  ngOnInit() {
    let storedModuleName = this.userService.getSelectedModule();

    if (!storedModuleName) {
      const urlArray = this.router.url.split('/');
      if (urlArray[1] === 'incident' || urlArray[1] === 'admin') {
        storedModuleName = 'Incident';
      } else if (urlArray[1] === 'audit') {
        storedModuleName = 'Audit';
      } else if (urlArray[1] === 'admin') {
        storedModuleName = 'Admin';
      } else if (urlArray[1] === 'home') {
        storedModuleName = 'Home';
      } else if (urlArray[1] === 'organization') {
        storedModuleName = 'Organization';
      }
    }
    const moduleName = storedModuleName || 'Home';
    this.setTopNavigationItems(moduleName);
  }

  ngOnDestroy() {
    // Needed for automatic destruction of subscriptions
  }

  setTopNavigationItems(switchingModule: string) {
    if (!switchingModule) {
      return;
    }

    this.selectedModule = switchingModule;
    // Fetching the stored user data for Menuitems
    const loggerUserData = this.userService.currentUser;

    if (switchingModule === 'Audit') {
      this.navItems = loggerUserData.myMenu.auditRoutes;
    } else if (switchingModule === 'Incident') {
      this.navItems = loggerUserData.myMenu.incidentRoutes;
    } else if (switchingModule === 'Admin') {
      this.navItems = loggerUserData.myMenu.adminRoutes;
    } else if (switchingModule === 'Home') {
      this.navItems = [];
    } else if (switchingModule === 'Organization') {
      this.navItems = loggerUserData.myMenu.organizationRoutes;
    }

    this.userService.setModule(switchingModule);
  }

  appSwitcherFunction(switchingModule: string) {
    if (!switchingModule) {
      return;
    }

    this.setTopNavigationItems(switchingModule);

    if (switchingModule === 'Audit') {
      this.router.navigate(['audit']);
    } else if (switchingModule === 'Incident') {
      this.router.navigate(['incident/investigation-stats']);
    } else if (switchingModule === 'Admin') {
      this.router.navigate(['admin']);
    } else if (switchingModule === 'Home') {
      this.router.navigate(['home']);
    } else if (switchingModule === 'Organization') {
      this.router.navigate(['organization']);
    }
  }

  userData() {
    this.userService.getProfile().subscribe((response: any) => {
      log.debug('UserData', response);
      log.debug('UserId', response.userId);
      this.profileId = response.userId;

      log.debug('profileId', this.profileId);

      this.router.navigate([`/profile/${this.profileId}/edit`]);
    });
  }

  get username(): string | null {
    const currentUser = this.userService.currentUser;
    return currentUser ? currentUser.firstname : null;
  }

  get getUserNameInitials(): string {
    const currentUser = this.userService.currentUser;
    const name = currentUser.firstname + ' ' + currentUser.lastname;
    const names = name.split(' ');
    let initials = names[0].substring(0, 1).toUpperCase();
    if (names.length > 1) {
      initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    return initials;
  }

  get title(): string {
    return this.titleService.getTitle();
  }

  logout() {
    this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
  }
}
