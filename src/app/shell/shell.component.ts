import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { filter } from 'rxjs/operators';

import { Logger, untilDestroyed } from '@core';
import { SidenavComponent } from '@app/shell/sidenav/sidenav.component';

const log = new Logger('ShellComponent');

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
})
export class ShellComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav', { static: false }) sidenav!: SidenavComponent;

  constructor(private media: MediaObserver) {
    log.debug('Construct');
  }

  async ngOnInit() {
    log.debug('Init');
    // Automatically close side menu on screens > sm breakpoint
    this.media
      .asObservable()
      .pipe(
        filter((changes: MediaChange[]) =>
          changes.some((change) => change.mqAlias !== 'xs' && change.mqAlias !== 'sm')
        ),
        untilDestroyed(this)
      )
      .subscribe(() => this.sidenav.close());

    // this.functionToMaintainSideNavState()
  }

  ngOnDestroy() {
    // Needed for automatic unsubscribe with untilDestroyed
  }
}
