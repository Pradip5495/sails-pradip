import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { untilDestroyed } from '@core';
import { SidenavStoreService } from '@app/shell/sidenav/sidenav-store.service';
import { VesselModel } from '@shared/models';
import { HeaderService } from '../header/header.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit, AfterViewInit, OnDestroy {
  isSideNavOpened: boolean;
  isSideMyVesselsOpened: boolean;
  isHeaderHidden = false;

  vessels: VesselModel[] = [];
  isMiniMyVesselsSidenav = false;

  constructor(
    public readonly sidenavStore: SidenavStoreService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly headerService: HeaderService
  ) {
    this.headerService.hasHeader$.pipe(untilDestroyed(this)).subscribe((value) => {
      this.isHeaderHidden = value === false;
    });
  }

  ngOnInit(): void {
    this.sidenavStore.hasSideNav$.pipe(untilDestroyed(this)).subscribe((value) => {
      this.isSideNavOpened = value;
      this.changeDetectorRef.detectChanges();
    });

    /* TODO: This code Belong to the Right side Navigation which must be removed after sometime */
    /* this.sidenavStore.hasMyVessels$.pipe(untilDestroyed(this)).subscribe((value) => {
      this.isSideMyVesselsOpened = value;
      this.changeDetectorRef.detectChanges();
    }); */
  }

  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges();
  }

  ngOnDestroy() {
    // Needed for automatic unsubscribe with untilDestroyed
  }

  close() {}

  public highlightActiveItem(event: any) {
    event.target.closest('button').classList.add('active');
  }

  /* TODO: This code Belong to the Right side Navigation which must be removed after sometime */
  /* toggle() {
    this.isMiniMyVesselsSidenav = !this.isMiniMyVesselsSidenav;
  } */
}
