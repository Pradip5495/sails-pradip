import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NavItem } from '@app/shell/header/nav-item/nav-item.interface';

@Injectable({
  providedIn: 'root',
})
export class SidenavStoreService {
  get myVessels(): string[] {
    return this._myVessels.getValue();
  }

  set myVessels(value: string[]) {
    this._myVessels.next(value);
  }

  // Expose the observable$ part of the _myVessels
  get myVessels$() {
    return this._myVessels.asObservable();
  }

  get navItems(): NavItem[] {
    return this._navItems.getValue();
  }

  set navItems(value: NavItem[]) {
    this.hasSideNav = !!value.length;
    this._navItems.next(value);
  }

  // Expose the observable$ part of the _navItems
  get navItems$() {
    return this._navItems.asObservable();
  }

  get hasSideNav(): boolean {
    return this._hasSideNav.getValue();
  }

  set hasSideNav(value: boolean) {
    this._hasSideNav.next(value);
    this._hasMyVessels.next(value);
  }

  // Expose the observable$ part of the _hadLeftSideNav
  get hasSideNav$() {
    return this._hasSideNav.asObservable();
  }

  get hasMyVessels(): boolean {
    return this._hasMyVessels.getValue();
  }

  set hasMyVessels(value: boolean) {
    this._hasMyVessels.next(value);
  }

  // Expose the observable$ part of the _hasMyVessels
  get hasMyVessels$() {
    return this._hasMyVessels.asObservable();
  }

  private readonly _navItems = new BehaviorSubject<NavItem[]>([]);
  private readonly _hasSideNav = new BehaviorSubject<boolean>(false);
  private readonly _myVessels = new BehaviorSubject<string[]>([]);
  private readonly _hasMyVessels = new BehaviorSubject<boolean>(false);

  isSelectedVessel(vessel: string): boolean {
    const foundVessel = this.myVessels.find((myVessel) => myVessel === vessel);
    return !!foundVessel;
  }

  toggleVesselSelection(vessel: string) {
    if (this.isSelectedVessel(vessel)) {
      this.removeFromMyVessels(vessel);
    } else {
      this.addToMyVessels(vessel);
    }
  }

  private addToMyVessels(vessel: string) {
    this.myVessels = [...this.myVessels, vessel];
  }

  private removeFromMyVessels(myVessel: string) {
    this.myVessels = this.myVessels.filter((vessel) => myVessel !== vessel);
  }
}
