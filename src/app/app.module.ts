import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';

import { environment } from '@env/environment';
import { CoreModule } from '@core';
import { FormlyModule } from '@formly';
import { SharedModule } from '@shared';
import { ShellModule } from './shell/shell.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ToastrModule } from 'ngx-toastr';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { DateFnsDateTimeAdapter } from '@shared/date';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { MatInputModule } from '@angular/material/input';
import { AgGridModule } from '@ag-grid-community/angular';
import { MatDialogModule } from '@angular/material/dialog';
import { NgApexchartsModule } from 'ng-apexcharts';

const DATEFNS_FORMATS_EN_LOCALE = {
  parseInput: 'dd/MM/yyyy HH:mm || dd/MM/yyyy', // multiple date input types separated by ||
  fullPickerInput: 'dd MMM yyyy HH:mm',
  datePickerInput: 'dd MMM yyyy',
  timePickerInput: 'HH:mm',
  monthYearLabel: 'MMM yyyy',
  dateA11yLabel: 'dd/MM/yyyy',
  monthYearA11yLabel: 'MMMM yyyy',
};

@NgModule({
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    CoreModule,
    FormlyModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ToastrModule.forRoot(),
    ShellModule,
    MatInputModule,
    NgxIntlTelInputModule,
    AgGridModule.withComponents([]),
    Ng2TelInputModule,
    FormsModule,
    AppRoutingModule,
    MatDialogModule,
    NgApexchartsModule,
  ],
  declarations: [AppComponent],
  providers: [
    { provide: DateTimeAdapter, useClass: DateFnsDateTimeAdapter },
    { provide: OWL_DATE_TIME_FORMATS, useValue: DATEFNS_FORMATS_EN_LOCALE },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
