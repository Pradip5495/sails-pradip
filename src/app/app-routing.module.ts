import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { LoginModule } from '@modules';
import { TestingComponent } from '@shared/component/charts/testing/testing.component';

const routes: Routes = [
  { path: 'chart-testing', component: TestingComponent, pathMatch: 'full' },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  { path: 'login', loadChildren: () => LoginModule },
  {
    path: '',
    loadChildren: () => import('@modules').then((m) => m.ModulesModule),
  },
  Shell.childRoutes([{ path: 'about', loadChildren: () => import('@modules').then((m) => m.AboutModule) }]),
  // Fallback when no prior route is matched
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
