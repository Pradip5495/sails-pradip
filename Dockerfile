# Package: copy build to Container
FROM nginx:1.17.1-alpine
COPY ./dist /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/

ARG PORT=5000
ENV PORT=${PORT}

EXPOSE ${PORT}
CMD ["nginx", "-g", "daemon off;"]